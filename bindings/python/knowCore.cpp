#include <pybind11/chrono.h>
#include <pybind11/stl.h>

#include "knowCorePython.h"

#include <knowCore/ConstrainedValue.h>
#include <knowCore/Image.h>
#include <knowCore/Messages.h>
#include <knowCore/Timestamp.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/Uri.h>
#include <knowCore/UriManager.h>
#include <knowCore/UrisRegistry.h>

namespace py = pybind11;

namespace
{
  std::string get_buffer_format(knowCore::Image::Type _type)
  {
    using kit = knowCore::Image::Type;
    switch(_type)
    {
    case kit::UnsignedInteger8:
      return py::format_descriptor<uint8_t>::format();
    case kit::UnsignedInteger16:
      return py::format_descriptor<uint16_t>::format();
    case kit::UnsignedInteger32:
      return py::format_descriptor<uint32_t>::format();
    // case kit::UnsignedInteger64:
    //   return py::format_descriptor<uint64_t>::format();
    case kit::Integer8:
      return py::format_descriptor<int8_t>::format();
    case kit::Integer16:
      return py::format_descriptor<int16_t>::format();
    case kit::Integer32:
      return py::format_descriptor<int32_t>::format();
    // case kit::Integer64:
    //   return py::format_descriptor<int64_t>::format();
    case kit::Float32:
      return py::format_descriptor<float>::format();
    case kit::Float64:
      return py::format_descriptor<double>::format();
    };
    throw std::runtime_error(std::format("Invalid knowCore::Image::Type '{}'!", int(_type)));
  }
  knowCore::Image::Type get_scalar_type(const std::string& _format)
  {
    using kit = knowCore::Image::Type;
    if(_format == py::format_descriptor<uint8_t>::format())
    {
      return kit::UnsignedInteger8;
    }
    else if(_format == py::format_descriptor<uint16_t>::format())
    {
      return kit::UnsignedInteger16;
    }
    else if(_format == py::format_descriptor<uint32_t>::format())
    {
      return kit::UnsignedInteger32;
      // } else if(_format == py::format_descriptor<uint64_t>::format())
      // {
      //   return kit::UnsignedInteger64;
    }
    else if(_format == py::format_descriptor<int8_t>::format())
    {
      return kit::Integer8;
    }
    else if(_format == py::format_descriptor<int16_t>::format())
    {
      return kit::Integer16;
    }
    else if(_format == py::format_descriptor<int32_t>::format())
    {
      return kit::Integer32;
      // } else if(_format == py::format_descriptor<int64_t>::format())
      // {
      //   return kit::Integer64;
    }
    else if(_format == py::format_descriptor<float>::format())
    {
      return kit::Float32;
    }
    else if(_format == py::format_descriptor<double>::format())
    {
      return kit::Float64;
    }
    throw std::runtime_error(std::format("Unsupported format '{}'!", _format));
  }
} // namespace

PYBIND11_MODULE(knowCore, m)
{
  m.doc() = R"pbdoc(
    knowCore
    -----------

    .. currentmodule:: knowCore

    .. autosummary::
      :toctree: _generate
  )pbdoc";

  m.def("urisRegistry", []() { return knowCore::UrisRegistry::manager(); });

  py::class_<knowCore::Image> image(m, "Image", py::buffer_protocol());

  py::enum_<knowCore::Image::ColorSpace>(image, "ColorSpace")
    .value("BGR", knowCore::Image::ColorSpace::BGR)
    .value("RGB", knowCore::Image::ColorSpace::RGB)
    .value("Unknown", knowCore::Image::ColorSpace::Unknown);

  image
    .def(py::init(
           [](py::buffer b, knowCore::Image::ColorSpace _cs)
           {
             py::buffer_info info = b.request();

             quint64 channels = 0;

             switch(info.shape.size())
             {
             case 2:
               channels = 1;
               break;
             case 3:
               channels = info.shape[2];
               break;
             default:
               throw std::runtime_error("knowCore::Image can only be two or three dimensions.");
             }
             quint64 width = info.shape[1];
             quint64 height = info.shape[0];
             knowCore::Image img;
             switch(_cs)
             {
             case knowCore::Image::ColorSpace::Unknown:
               img = knowCore::Image(width, height, channels, get_scalar_type(info.format));
               break;
             case knowCore::Image::ColorSpace::BGR:
             case knowCore::Image::ColorSpace::RGB:
               if(channels == 3)
               {
                 img = knowCore::Image(width, height, _cs, get_scalar_type(info.format));
                 break;
               }
               else
               {
                 throw std::runtime_error(
                   std::format("RGB/BGR images are 3 channels images, got {} channels", channels));
               }
             default:
               throw std::runtime_error(std::format("Unsupported colorspace: {}", int(_cs)));
               break;
             }
             if(img.size() != quint64(info.size))
             {
               throw std::runtime_error(std::format(
                 "knowCore::Image expect {} bytes but got {} bytes", img.size(), info.size));
             }
             const quint8* info_ptr = reinterpret_cast<const quint8*>(info.ptr);
             std::copy(info_ptr, info_ptr + img.size(), img.dataPtr());
             return img;
           }),
         py::arg("buffer"), py::arg("cs") = knowCore::Image::ColorSpace::Unknown)
    .def_buffer(
      [](knowCore::Image& _img)
      {
        std::vector<std::size_t> dimensions, strides;
        // height and width are reverse in python buffer
        dimensions.push_back(_img.height());
        dimensions.push_back(_img.width());

        // Strides
        quint64 scalar_size = knowCore::Image::scalarSize(_img.type());
        strides.push_back(_img.width() * scalar_size);

        // Handle multiple channels
        if(_img.channels() > 1)
        {
          strides[0] *= _img.channels();
          strides.push_back(_img.channels() * scalar_size);
          dimensions.push_back(_img.channels());
        }
        // If channel = 1, second stride is one pixel size, aka scalar size, if more channel,
        // the stride between element is scalar_size
        strides.push_back(scalar_size);

        return py::buffer_info(
          _img.dataPtr(),                           /* Pointer to buffer */
          knowCore::Image::scalarSize(_img.type()), /* Size of one scalar */
          get_buffer_format(_img.type()),           /* Python struct-style format descriptor */
          dimensions.size(),                        /* Number of dimensions */
          dimensions,                               /* Buffer dimensions */
          strides                                   /* Strides (in bytes) for each index */
        );
      })
    .def_static("fromRawData",
                pybind11_qt::overload<const QByteArray&, quint64, quint64, const QString&>(
                  &knowCore::Image::fromRawData));

  py::class_<knowCore::ConstrainedValue> constrained_value(m, "ConstrainedValue");
  constrained_value.def(py::init())
    .def("check",
         pybind11_qt::overload_r_c<cres_qresult<bool>, knowCore::ConstrainedValue,
                                   const knowCore::Value&>(&knowCore::ConstrainedValue::check))
    .def("apply",
         pybind11_qt::overload_r_c<knowCore::ConstrainedValue&, knowCore::ConstrainedValue,
                                   const knowCore::Value&, knowCore::ConstrainedValue::Type>(
           &knowCore::ConstrainedValue::apply));

  py::enum_<knowCore::ConstrainedValue::Type>(constrained_value, "Type")
    .value("Equal", knowCore::ConstrainedValue::Type::Equal)
    .value("Different", knowCore::ConstrainedValue::Type::Different)
    .value("Inferior", knowCore::ConstrainedValue::Type::Inferior)
    .value("Superior", knowCore::ConstrainedValue::Type::Superior)
    .value("InferiorEqual", knowCore::ConstrainedValue::Type::InferiorEqual)
    .value("SuperiorEqual", knowCore::ConstrainedValue::Type::SuperiorEqual)
    .value("GeoOverlaps", knowCore::ConstrainedValue::Type::GeoOverlaps)
    .value("GeoWithin", knowCore::ConstrainedValue::Type::GeoWithin)
    .value("GeoContains", knowCore::ConstrainedValue::Type::GeoContains)
    .value("GeoIntersects", knowCore::ConstrainedValue::Type::GeoIntersects)
    .value("GeoTouches", knowCore::ConstrainedValue::Type::GeoTouches)
    .value("GeoDisjoint", knowCore::ConstrainedValue::Type::GeoDisjoint)
    .value("Contains", knowCore::ConstrainedValue::Type::Contains)
    .value("NotContains", knowCore::ConstrainedValue::Type::NotContains)
    .value("In", knowCore::ConstrainedValue::Type::In)
    .value("NotIn", knowCore::ConstrainedValue::Type::NotIn);

  py::class_<knowCore::Messages>(m, "Messages").def("toString", &knowCore::Messages::toString);

  py::class_<knowCore::Timestamp>(m, "Timestamp")
    .def_static("originOfTime", &knowCore::Timestamp::originOfTime)
    .def_static("now", &knowCore::Timestamp::now)
    .def_static(
      "fromDateTime",
      [](const std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>& _time)
      {
        return knowCore::Timestamp::fromEpoch(
          _time
          - std::chrono::time_point<std::chrono::system_clock,
                                    std::chrono::nanoseconds>()); // a bit ugly but Timestamp
                                                                  // works with duration
      })
    .def_static("fromTimeDelta", [](const std::chrono::nanoseconds& _time)
                { return knowCore::Timestamp::fromEpoch(_time); })
    .def("toDateTime",
         [](const knowCore::Timestamp& _self)
         {
           return std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>(
             std::chrono::nanoseconds(
               _self.toEpoch<knowCore::NanoSeconds>().count().toInt64().expect_success()));
         });
  ;

  py::class_<knowCore::Uri>(m, "Uri")
    .def(py::init())
    .def(py::init<const QString&>())
    .def("isEmpty", &knowCore::Uri::isEmpty)
    .def("isAbsolute", &knowCore::Uri::isAbsolute)
    .def("isUrn", &knowCore::Uri::isUrn)
    .def("isUrl", &knowCore::Uri::isUrl)
    .def("resolved", pybind11_qt::overload<const knowCore::Uri&>(&knowCore::Uri::resolved))
    .def("resolved", pybind11_qt::overload<const QString&>(&knowCore::Uri::resolved))
    .def("toString", [](const knowCore::Uri& _uri) -> QString { return _uri; })
    .def_static("createUnique", &knowCore::Uri::createUnique);

  py::class_<knowCore::UriManager>(m, "UriManager")
    .def(py::init())
    .def("addPrefix",
         pybind11_qt::overload<const QString&, const QString&>(&knowCore::UriManager::addPrefix))
    .def("hasPrefix", &knowCore::UriManager::hasPrefix)
    .def("base", &knowCore::UriManager::base)
    .def("setBase", &knowCore::UriManager::setBase)
    .def("uri", pybind11_qt::overload<const QString&>(&knowCore::UriManager::uri))
    .def("uri", pybind11_qt::overload<const QString&, const QString&>(&knowCore::UriManager::uri));

  // Default value converters
  knowCore::pybind11::register_value_converter<knowCore::ValueHash>();
  knowCore::pybind11::register_value_converter<knowCore::ValueList>();
  knowCore::pybind11::register_value_converter<QString>();
  knowCore::pybind11::register_value_converter<QByteArray>();
  knowCore::pybind11::register_value_converter<knowCore::BigNumber>();
  knowCore::pybind11::register_value_converter<knowCore::Timestamp>();
  knowCore::pybind11::register_value_converter<knowCore::Uri>();
  knowCore::pybind11::register_value_converter<bool>();
  knowCore::pybind11::register_value_converter<quint8>();
  knowCore::pybind11::register_value_converter<quint16>();
  knowCore::pybind11::register_value_converter<quint32>();
  knowCore::pybind11::register_value_converter<quint64>();
  knowCore::pybind11::register_value_converter<qint8>();
  knowCore::pybind11::register_value_converter<qint16>();
  knowCore::pybind11::register_value_converter<qint32>();
  knowCore::pybind11::register_value_converter<qint64>();
  knowCore::pybind11::register_value_converter<float>();
  knowCore::pybind11::register_value_converter<double>();
}
