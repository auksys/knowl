#include "knowCorePython.h"

namespace knowCore::pybind11
{
  static QList<const detail::abstract_value_converter*> converters_;
}

void knowCore::pybind11::detail::value_converter_interface::register_value_converter(
  abstract_value_converter* _converter)
{
  converters_.append(_converter);
}

QList<const knowCore::pybind11::detail::abstract_value_converter*>
  knowCore::pybind11::detail::value_converter_interface::converters()
{
  return converters_;
}
