#include <pybind11-qt/core.h>

#include <knowCore/BigNumber.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/ValueHash.h>
#include <knowCore/ValueList.h>

namespace knowCore::pybind11
{
  template<typename _T_>
  _T_ handle_result(const cres_qresult<_T_>& _result)
  {
    if(_result.is_successful())
    {
      return _result.get_value();
    }
    else
    {
      throw std::runtime_error(_result.get_error().get_message().toStdString());
    }
  }
  template<>
  void handle_result(const cres_qresult<void>& _result)
  {
    if(not _result.is_successful())
    {
      throw std::runtime_error(_result.get_error().get_message().toStdString());
    }
  }

  template<typename _T_>
  void register_value_converter();
  namespace detail
  {
    struct abstract_value_converter;
    struct value_converter_interface
    {
      template<typename _T_>
      friend void knowCore::pybind11::register_value_converter();
      friend struct ::pybind11::detail::type_caster<knowCore::Value>;
    private:
      static void register_value_converter(abstract_value_converter* _converter);
      static QList<const detail::abstract_value_converter*> converters();
      static ::pybind11::handle to_python(const knowCore::Value& _variant,
                                          ::pybind11::return_value_policy _policy,
                                          ::pybind11::handle _parent);
      static knowCore::Value to_value(::pybind11::handle _handle);
    };
    struct abstract_value_converter
    {
      virtual bool can_cast(const knowCore::Value& _variant, bool _strict) const = 0;
      virtual ::pybind11::handle to_python(const knowCore::Value& _variant,
                                           ::pybind11::return_value_policy _policy,
                                           ::pybind11::handle _parent) const
        = 0;
      virtual knowCore::Value to_value(::pybind11::handle _handle) const = 0;
    };
    inline ::pybind11::handle
      value_converter_interface::to_python(const knowCore::Value& _variant,
                                           ::pybind11::return_value_policy _policy,
                                           ::pybind11::handle _parent)
    {
      for(const abstract_value_converter* conv : converters())
      {
        if(conv->can_cast(_variant, true))
        {
          return conv->to_python(_variant, _policy, _parent);
        }
      }
      for(const abstract_value_converter* conv : converters())
      {
        if(conv->can_cast(_variant, false))
        {
          return conv->to_python(_variant, _policy, _parent);
        }
      }
      throw ::pybind11::type_error("No converter for type: "
                                   + QString(_variant.datatype()).toStdString()
                                   + " from knowCore::Value.");
    }
    inline knowCore::Value value_converter_interface::to_value(::pybind11::handle _handle)
    {
      for(const abstract_value_converter* conv : converters())
      {
        try
        {
          return conv->to_value(_handle);
        }
        catch(const std::exception&)
        {
        }
      }
      throw ::pybind11::type_error("No converter for python value '"
                                   + std::string(::pybind11::repr(_handle))
                                   + "' to knowCore::Value.");
    }

    template<typename _T_>
    struct value_converter : public abstract_value_converter
    {
      bool can_cast(const knowCore::Value& _variant, bool _strict) const override
      {
        return (_strict and _variant.is<_T_>()) or (not _strict and _variant.canConvert<_T_>());
      }
      ::pybind11::handle to_python(const knowCore::Value& _value,
                                   ::pybind11::return_value_policy _policy,
                                   ::pybind11::handle _parent) const override
      {
        return ::pybind11::cast(_value.value<_T_>(), _policy, _parent).release();
      }
      knowCore::Value to_value(::pybind11::handle _handle) const
      {
        return knowCore::Value::fromValue(::pybind11::cast<_T_>(_handle));
      }
    };

  } // namespace detail
  template<typename _T_>
  void register_value_converter()
  {
    detail::value_converter_interface::register_value_converter(new detail::value_converter<_T_>());
  }
} // namespace knowCore::pybind11

namespace pybind11::detail
{
  template<>
  struct type_caster<knowCore::BigNumber>
  {
  public:
    PYBIND11_TYPE_CASTER(knowCore::BigNumber, const_name("knowCore::BigNumber"));

    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle src, bool)
    {
      if(int_::check_(src))
      {
        value = pybind11::cast<long>(src);
        return true;
      }
      else if(float_::check_(src))
      {
        value = pybind11::cast<double>(src);
        return true;
      }
      return false;
    }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(knowCore::BigNumber src, return_value_policy policy, handle parent)
    {
      if(src.isFloating())
      {
        return pybind11::cast(src.toDouble(), policy, parent);
      }
      else
      {
        return pybind11::cast(knowCore::pybind11::handle_result(src.toInt64()), policy, parent);
      }
    }
  };
  template<>
  struct type_caster<knowCore::Value>
  {
  public:
    PYBIND11_TYPE_CASTER(knowCore::Value, const_name("knowCore::Value"));

    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle src, bool)
    {
      value = knowCore::pybind11::detail::value_converter_interface::to_value(src);
      return true;
    }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(knowCore::Value src, return_value_policy policy, handle parent)
    {
      return knowCore::pybind11::detail::value_converter_interface::to_python(src, policy, parent);
    }
  };
  template<>
  struct type_caster<knowCore::ValueHash>
  {
  public:
    PYBIND11_TYPE_CASTER(knowCore::ValueHash, const_name("knowCore::ValueHash"));

    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle src, bool)
    {
      value = pybind11::cast<QHash<QString, knowCore::Value>>(src);
      return true;
    }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(knowCore::ValueHash src, return_value_policy policy, handle parent)
    {
      return pybind11::cast(src.hash(), policy, parent).release();
    }
  };
  template<>
  struct type_caster<knowCore::ValueList>
  {
  public:
    PYBIND11_TYPE_CASTER(knowCore::ValueList, const_name("knowCore::ValueList"));

    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle src, bool)
    {
      value = pybind11::cast<QList<knowCore::Value>>(src);
      return true;
    }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(knowCore::ValueList src, return_value_policy policy, handle parent)
    {
      return pybind11::cast(src.values(), policy, parent).release();
    }
  };
  template<>
  struct type_caster<cres_qresult<void>>
  {
  public:
    PYBIND11_TYPE_CASTER(cres_qresult<void>, const_name("cres_qresult<void>"));

    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle, bool) { return false; }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(cres_qresult<void> src, return_value_policy, handle)
    {
      if(src.is_successful())
      {
        return pybind11::none();
      }
      else
      {
        throw std::runtime_error(src.get_error().get_message().toStdString());
      }
    }
  };
  template<typename _T_>
  struct type_caster<cres_qresult<_T_>>
  {
  public:
    using t_conv = make_caster<_T_>;
    PYBIND11_TYPE_CASTER(cres_qresult<_T_>,
                         const_name("cres_qresult<") + t_conv::name + const_name(">"));

    /**
     * Conversion part 1 (Python->C++).
     */
    bool load(handle src, bool) { return false; }

    /**
     * Conversion part 2 (C++ -> Python).
     */
    static handle cast(cres_qresult<_T_> src, return_value_policy policy, handle parent)
    {
      return pybind11::cast(knowCore::pybind11::handle_result(src), policy, parent).release();
    }
  };

} // namespace pybind11::detail
