#include "knowCorePython.h"

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

namespace py = pybind11;

PYBIND11_MODULE(py_knowDBC, m)
{
  py::class_<knowDBC::Query>(m, "Query")
    .def("setQuery", pybind11_qt::overload<const QString&, bool>(&knowDBC::Query::setQuery),
         py::arg("query"), py::arg("keepBindings") = false)
    .def("bindValue",
         pybind11_qt::overload_r_c<void, knowDBC::Query, const QString&, const knowCore::Value&>(
           &knowDBC::Query::bindValue))
    .def("bindValues", pybind11_qt::overload_r_c<void, knowDBC::Query, const knowCore::ValueHash&>(
                         &knowDBC::Query::bindValues))
    .def("execute", &knowDBC::Query::execute);
  py::class_<knowDBC::Result>(m, "Result")
    .def("tuples", &knowDBC::Result::tuples)
    .def("fields", &knowDBC::Result::fields)
    .def("fieldName", &knowDBC::Result::fieldName)
    .def("fieldIndex", &knowDBC::Result::fieldIndex)
    .def("query", &knowDBC::Result::query)
    .def("error", &knowDBC::Result::error)
    .def("isSuccessful", [](const knowDBC::Result& _self) { return (bool)_self; })
    .def("value", pybind11_qt::overload_r_c<knowCore::Value, knowDBC::Result, int, int>(
                    &knowDBC::Result::value))
    .def("value",
         pybind11_qt::overload_r_c<knowCore::Value, knowDBC::Result, int, const QByteArray&>(
           &knowDBC::Result::value))
    .def("value", pybind11_qt::overload_r_c<knowCore::Value, knowDBC::Result, int, const char*>(
                    &knowDBC::Result::value));
}
