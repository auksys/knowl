import knowCore

from py_knowDBC import Query, Result

def print_result(res):
  import knowCore
  import knowRDF
  for i in range(0, res.tuples()):
    for j in range(0, res.fields()):
      v = res.value(i,j)
      if type(v) is knowRDF.Literal:
        if len(v.lang()) == 0:
          v = "{}({})".format(v.datatype(), v.value())
        else:
          v = "{}({})@{}".format(v.datatype(), v.value(), v.lang())
      elif type(v) is knowCore.Uri:
        v = v.toString()
      elif type(v) is knowRDF.BlankNode:
        v = "BlankNode"
      print("{}.{}: {}".format(i, res.fieldName(j), v))
  
