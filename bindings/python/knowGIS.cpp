#include "knowCorePython.h"

#include <knowRDF/Literal.h>

#include <knowGIS/GeoPoint.h>
#include <knowGIS/GeometryObject.h>
#include <knowGIS/Point.h>

namespace py = pybind11;

PYBIND11_MODULE(py_knowGIS, m)
{
  py::class_<knowGIS::GeometryObject, Cartography::GeometryObject>(m, "GeometryObject")
    .def(py::init())
    .def(py::init<const knowGIS::GeometryObject&>())
    .def_static("load", knowGIS::GeometryObject::load)
    .def_static("fromWKT", knowGIS::GeometryObject::fromWKT)
    .def("toLiteral", [](const knowGIS::GeometryObject& _self)
         { return knowRDF::Literal::fromVariant(QVariant::fromValue(_self)); });
  py::class_<knowGIS::GeoPoint, Cartography::GeoPoint>(m, "GeoPoint")
    .def(py::init())
    .def(py::init(
           [](double _longitude, double _latitude, double _altitude)
           {
             return new knowGIS::GeoPoint(Cartography::Longitude(_longitude),
                                          Cartography::Latitude(_latitude),
                                          Cartography::Altitude(_altitude));
           }),
         py::arg("longitude"), py::arg("latitude"), py::arg("altitude") = NAN);
  py::class_<knowGIS::Point, Cartography::Point>(m, "Point")
    .def(py::init())
    .def(py::init<double, double, double, Cartography::CoordinateSystem>());

  knowCore::pybind11::register_value_converter<knowGIS::GeometryObject>();
}
