#include "knowCorePython.h"

#include <QBuffer>
#include <QFile>

#include <knowCore/Messages.h>

#include <knowRDF/Literal.h>
#include <knowRDF/Object.h>
#include <knowRDF/Serialiser.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>
#include <knowRDF/TripleStream.h>
#include <knowRDF/TripleStreamListener.h>
#include <knowRDF/TriplesAccumulator.h>

namespace py = pybind11;

class PyTripleStreamListener : public knowRDF::TripleStreamListener
{
public:
  void triple(const knowRDF::Triple& _triple) override
  {
    PYBIND11_OVERRIDE_PURE(void, knowRDF::TripleStreamListener, triple, _triple);
  }
};

PYBIND11_MODULE(py_knowRDF, m)
{
  py::class_<knowRDF::BlankNode>(m, "BlankNode").def(py::init());
  py::class_<knowRDF::Literal>(m, "Literal")
    .def(py::init())
    .def("value", [](const knowRDF::Literal& _self) -> knowCore::Value { return _self; })
    .def("lang", &knowRDF::Literal::lang)
    .def("datatype", &knowRDF::Literal::datatype)
    .def("__repr__", [](const knowRDF::Literal& _literal) { return clog_qt::to_qstring(_literal); })
    .def_static("fromRdfLiteral", &knowRDF::Literal::fromRdfLiteral, py::arg("datatype"),
                py::arg("literal"), py::arg("lang") = QString());
  py::class_<knowRDF::Object>(m, "Object")
    .def(py::init())
    .def("type", &knowRDF::Object::type)
    .def("uri", &knowRDF::Object::uri)
    .def("blankNode", &knowRDF::Object::blankNode)
    .def("literal", &knowRDF::Object::literal)
    .def("variableName", &knowRDF::Object::variableName)
    .def("__repr__", [](const knowRDF::Object& _self) { return clog_qt::to_qstring(_self); })
    .def_static("fromValue", [](const knowCore::Value& _value)
                { return knowRDF::Object(knowRDF::Literal::fromValue(_value)); });
  py::class_<knowRDF::Subject>(m, "Subject")
    .def(py::init())
    .def(py::init<knowCore::Uri>())
    .def("type", &knowRDF::Subject::type)
    .def("uri", &knowRDF::Subject::uri)
    .def("blankNode", &knowRDF::Subject::blankNode)
    .def("variableName", &knowRDF::Subject::variableName)
    .def("__repr__", [](const knowRDF::Subject& _self) { return clog_qt::to_qstring(_self); });
  py::class_<knowRDF::Triple>(m, "Triple")
    .def(py::init())
    .def("subject", &knowRDF::Triple::subject)
    .def("predicate", &knowRDF::Triple::predicate)
    .def("object", &knowRDF::Triple::object)
    .def("__repr__", [](const knowRDF::Triple& _self) { return clog_qt::to_qstring(_self); });
  py::class_<knowRDF::TriplesAccumulator>(m, "TriplesAccumulator")
    .def(py::init())
    .def("triples", &knowRDF::TriplesAccumulator::triples)
    .def("triple", &knowRDF::TriplesAccumulator::triple);
  py::class_<knowRDF::TripleStream>(m, "TripleStream")
    .def(py::init())
    .def("setBase", &knowRDF::TripleStream::setBase)
    .def("addListener", &knowRDF::TripleStream::addListener)
    .def("start", &knowRDF::TripleStream::start)
    .def(
      "start_from_string",
      [](knowRDF::TripleStream* _stream, const QString& _content, knowCore::Messages* _messages,
         const QString& _format)
      {
        QBuffer content_buffer;
        content_buffer.setData(_content.toUtf8());
        knowCore::pybind11::handle_result(_stream->start(&content_buffer, _messages, _format));
      },
      py::arg("content"), py::arg("messages") = nullptr,
      py::arg("format") = knowCore::FileFormat::Turtle)
    .def(
      "start_from_file",
      [](knowRDF::TripleStream* _stream, const QString& _filename, knowCore::Messages* _messages,
         const QString& _format)
      {
        QFile data_ttl_file("data.ttl");
        if(not data_ttl_file.open(QIODevice::ReadOnly))
        {
          throw std::runtime_error("Failed to open file: " + _filename.toStdString());
        }
        knowCore::pybind11::handle_result(_stream->start(&data_ttl_file, _messages, _format));
      },
      py::arg("content"), py::arg("messages") = nullptr,
      py::arg("format") = knowCore::FileFormat::Turtle);
  py::class_<knowRDF::TripleStreamListener, PyTripleStreamListener>(m, "TripleStreamListener")
    .def("triple", &knowRDF::TripleStreamListener::triple);
  py::class_<knowRDF::Serialiser>(m, "Serialiser")
    .def_static(
      "create_to_file",
      [](const QString& _filename, const knowCore::UriManager& _uriManager, const QString& _format)
      {
        QFile* f = new QFile(_filename);
        if(not f->open(QIODevice::WriteOnly))
        {
          throw std::runtime_error("Failed to open file: " + _filename.toStdString());
        }
        return new knowRDF::Serialiser(f, _uriManager, true, _format);
      })
    .def("serialise",
         pybind11_qt::overload<const knowRDF::Triple&>(&knowRDF::Serialiser::serialise))
    .def("serialise",
         pybind11_qt::overload<const QList<knowRDF::Triple>&>(&knowRDF::Serialiser::serialise));

  knowCore::pybind11::register_value_converter<knowRDF::Literal>();
}
