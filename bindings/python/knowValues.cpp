#include <pybind11/stl.h>

#include "knowCorePython.h"

#include <knowValues/Values.h>

namespace py = pybind11;

PYBIND11_MODULE(knowValues, m)
{
  py::class_<knowValues::Values::SalientRegion>(m, "SalientRegion")
    .def(py::init())
    .def("uri", [](const knowValues::Values::SalientRegion& _self) { return _self->uri(); })
    .def("classes", [](const knowValues::Values::SalientRegion& _self) { return _self->classes(); })
    .def("geometry",
         [](const knowValues::Values::SalientRegion& _self) { return _self->geometry(); })
    .def("properties",
         [](const knowValues::Values::SalientRegion& _self) { return _self->properties(); })
    .def("timestamp",
         [](const knowValues::Values::SalientRegion& _self) { return _self->timestamp(); });

  knowCore::pybind11::register_value_converter<knowValues::Values::SalientRegion>();
}
