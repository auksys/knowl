#!/usr/bin/env python3

import datetime
import unittest

import knowCore


class TimeStamp(unittest.TestCase):
    def test_timestamp(self):
        dt0 = datetime.datetime.now() - datetime.timedelta(minutes=15)
        dt1 = datetime.datetime.now() - datetime.timedelta(minutes=15)
        ts = knowCore.Timestamp.fromDateTime(dt0)
        self.assertEqual(ts.toDateTime(), dt0)
        self.assertNotEqual(ts.toDateTime(), dt1)

class ConstrainedValue(unittest.TestCase):
    def test_check_for_values(self):
        cv = knowCore.ConstrainedValue() \
            .apply(20, knowCore.ConstrainedValue.Type.Superior) \
            .apply(30, knowCore.ConstrainedValue.Type.Inferior)

        self.assertFalse(cv.check(10))
        self.assertFalse(cv.check(20))
        self.assertTrue(cv.check(25))
        self.assertFalse(cv.check(30))
        self.assertFalse(cv.check(40))

if __name__ == '__main__':
    unittest.main()
