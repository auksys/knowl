#!/usr/bin/env python3

import unittest

import knowGIS

class GeometryObject(unittest.TestCase):
  def test_wkt(self):
    go = knowGIS.GeometryObject.fromWKT("POLYGON ((35 10, 45 45, 15 40, 10 20, 35 10), (20 30, 35 35, 30 20, 20 30))")
    self.assertEqual(go.toGeoVariant(), {u'type': u'Polygon', u'coordinates': [[[35.0, 10.0], [45.0, 45.0], [15.0, 40.0], [10.0, 20.0], [35.0, 10.0]], [[20.0, 30.0], [35.0, 35.0], [30.0, 20.0], [20.0, 30.0]]]})

if __name__ == '__main__':
    unittest.main()
