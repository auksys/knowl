#include "Context.h"

#include <QVariantHash>

#include "State.h"

using namespace knowBookQuick;

struct Context::Private
{
  State* state;
};

Context::Context(QObject* _parent) : QObject(_parent), d(new Private)
{
  d->state = new State;
  d->state->insert("store", QVariant());
  d->state->insert("connection", QVariant());
}

Context::~Context() { delete d; }

State* Context::state() const { return d->state; }

#include "moc_Context.cpp"
