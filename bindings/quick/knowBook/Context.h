#pragma once

#include <QObject>

namespace knowBookQuick
{
  class State;
  class Context : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(knowBookQuick::State* state READ state CONSTANT)
  public:
    Context(QObject* _parent = nullptr);
    virtual ~Context();
    State* state() const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowBookQuick
