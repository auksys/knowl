#include "Plugin.h"

#include <QtQml>

#include "Context.h"
#include "PluginsManager.h"
#include "State.h"

using namespace knowBookQuick;

void Plugin::registerTypes(const char* /*uri*/)
{
  const char* uri_knowBook = "knowBook";
  qmlRegisterType<Context>(uri_knowBook, 3, 0, "Context");
  qmlRegisterType<PluginsManager>(uri_knowBook, 3, 0, "PluginsManager");
  qmlRegisterType<State>(uri_knowBook, 3, 0, "State");
}

#include "moc_Plugin.cpp"
