#pragma once

#include <QQmlExtensionPlugin>

namespace knowBookQuick
{
  class Plugin : public QQmlExtensionPlugin
  {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "knowBook/3.0")
  public:
    void registerTypes(const char* uri) override;
  };
} // namespace knowBookQuick
