#include "PluginsManager.h"

#include <QDebug>
#include <QDir>
#include <QQmlComponent>
#include <QTimer>

#include <clog_qt>

#include "config.h"

using namespace knowBookQuick;

struct PluginsManager::Private
{
  QObjectList operationPlugins, connectionPlugins;
};

PluginsManager::PluginsManager() : d(new Private)
{
  // delay the call to load plugins, since QmlEngine is not set yet
  QTimer::singleShot(0, this, [this]() { loadPlugins(KNOWBOOK_INSTALL_PLUGINS_DIR); });
}

PluginsManager::~PluginsManager() { delete d; }

QObjectList PluginsManager::operationPlugins() const { return d->operationPlugins; }

QObjectList PluginsManager::connectionPlugins() const { return d->connectionPlugins; }

void PluginsManager::loadPlugins(const QString& _directory)
{
  clog_info("Loading knowBook plugins from {}", _directory);
  QDir directory(_directory);
  for(const QString& file : directory.entryList(QStringList() << "*.qml"))
  {
    clog_info("Attempt at loading plugin '{}'", directory.absoluteFilePath(file));
    clog_assert(qmlEngine(this));
    QQmlComponent component(qmlEngine(this), QUrl::fromLocalFile(directory.absoluteFilePath(file)));
    if(component.isError())
    {
      clog_error("Failed to load '{}' with error: {}", directory.absoluteFilePath(file),
                 component.errorString());
    }
    else
    {
      QObject* childItem = component.create();
      if(childItem)
      {
        childItem->setParent(this);

        QVariant pluginType = childItem->property("pluginType");
        if(pluginType.isValid())
        {
          switch(pluginType.toInt())
          {
          case(int)PluginType::OperationPlugin:
          {
            d->operationPlugins.append(childItem);
            break;
          }
          case(int)PluginType::ConnectionPlugin:
          {
            d->connectionPlugins.append(childItem);
            break;
          }
          default:
            clog_error("Plugin '{}' has invalid plugin type: {}", directory.absoluteFilePath(file),
                       pluginType);
          }
        }
        else
        {
          clog_error("Plugin '{}' has invalid plugin type.", directory.absoluteFilePath(file));
        }
      }
      else
      {
        qWarning().nospace() << "Failed to create item from '" << directory.absoluteFilePath(file)
                             << "'.";
      }
    }
  }
  for(const QString& file : directory.entryList(QDir::Dirs | QDir::NoDotAndDotDot))
  {
    loadPlugins(directory.absoluteFilePath(file));
  }

  emit(pluginsChanged());
}

QObject* PluginsManager::operationPluginFor(const QString& _type)
{
  for(QObject* object : d->operationPlugins)
  {
    if(object->property("name").toString() == _type)
    {
      return object;
    }
  }
  clog_warning("No plugin for type '{}'.", _type);
  return nullptr;
}

QObject* PluginsManager::connectionPluginFor(const QString& _type)
{
  for(QObject* object : d->connectionPlugins)
  {
    if(object->property("name").toString() == _type)
    {
      return object;
    }
  }
  return nullptr;
}

#include "moc_PluginsManager.cpp"
