#pragma once

#include <QObject>

namespace knowBookQuick
{

  class PluginsManager : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QList<QObject*> operationPlugins READ operationPlugins NOTIFY pluginsChanged)
    Q_PROPERTY(QList<QObject*> connectionPlugins READ connectionPlugins NOTIFY pluginsChanged)
  public:
    enum class PluginType
    {
      OperationPlugin = 12,
      ConnectionPlugin = 14
    };
    Q_ENUM(PluginType)
  public:
    PluginsManager();
    virtual ~PluginsManager();
    Q_INVOKABLE void loadPlugins(const QString& _directory);
    QObjectList operationPlugins() const;
    QObjectList connectionPlugins() const;
    Q_INVOKABLE QObject* operationPluginFor(const QString& _type);
    Q_INVOKABLE QObject* connectionPluginFor(const QString& _type);
  signals:
    void pluginsChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowBookQuick
