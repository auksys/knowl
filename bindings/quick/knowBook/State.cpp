#include "State.h"

#include <QVariantHash>

using namespace knowBookQuick;

struct State::Private
{
};

State::State(QObject* _parent) : QQmlPropertyMap(this, _parent), d(new Private) {}

State::~State() { delete d; }

bool State::has(const QString& _key) const { return contains(_key); }

void State::set(const QString& _key, const QVariant& _value)
{
  this->insert(_key, _value);
  emit(stateChanged());
  if(_value.canConvert<QObject*>())
  {
    QObject* o = _value.value<QObject*>();
    if(o and not o->parent())
      o->setParent(this);
  }
}

QVariant State::get(const QString& _key) { return value(_key); }

#include "moc_State.cpp"
