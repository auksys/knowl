#pragma once

#include <QQmlPropertyMap>

namespace knowBookQuick
{
  class State : public QQmlPropertyMap
  {
    Q_OBJECT
  public:
    State(QObject* _parent = nullptr);
    virtual ~State();
    Q_INVOKABLE bool has(const QString& _key) const;
    Q_INVOKABLE void set(const QString& _key, const QVariant& _value);
    Q_INVOKABLE QVariant get(const QString& _key);
  signals:
    void stateChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowBookQuick
