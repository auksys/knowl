import QtQuick
import QtQuick.Controls

import Cartography.Mapnik

import kDBGIS.Controls
import kDB.Repository

MapView
{
  id: _map_view_
  anchors.fill: parent
  zoom: 3.0
  map: Map
  {
    srs: '+proj=merc +a=6378137 +b=6378137 +lat_ts=0 +lon_0=0 +x_0=0 +y_0=0 +k=1 +units=m +nadgrids=@null +wktext +no_defs +type=crs'
    backgroundColor: "#aad3df"
    styles: [
      Style
      {
        name: "raster"
        Rule
        {
          RasterSymbolizer
          {
            ScalingKey { value: Mapnik.SCALING_BILINEAR }
          }
        }
      }
    ]
    Layer
    {
      name: "OSM"
      // Projection has to be web mercator
      srs: '+proj=merc +a=6378137 +b=6378137 +lat_ts=0 +lon_0=0 +x_0=0 +y_0=0 +k=1 +units=m +nadgrids=@null +wktext +no_defs +type=crs'
      styles: ["raster"]
      datasource: GeoTileDatasource {
        // Pick from https://wiki.openstreetmap.org/wiki/Raster_tile_providers
        sources: ["https://tile.openstreetmap.org/{z}/{x}/{y}.png"]
        userAgent: "knowBook/1.0 (auKsys; mkc)"
        onTilesUpdated: _map_view_.updateMap(true)
      }
    }
  }
  MouseArea
  {
    anchors.fill: parent
    property real zoomFactor: 1.05
    property real panFactor: 0.1

    property int __isPanning: 0 // 0 no panning 1 mouse pressed 2 panning in progress
    property int  __lastX: -1
    property int  __lastY: -1
    property int __isWheeling: 0 // 0 no wheeling 1 wheeling in progress
    
    preventStealing: true

    onPressed: mouse =>
    {
      __isPanning = 1
      __lastX = mouse.x
      __lastY = mouse.y
    }

    onReleased: mouse =>
    {
      if(__isPanning != 2)
      {
        _map_view_.centerTo(mouse.x, mouse.y)
      }
      __isPanning = 0
    }

    onPositionChanged: mouse =>
    {
      if (__isPanning > 0)
      {
        __isPanning = 2
        var dx = mouse.x - __lastX
        var dy = mouse.y - __lastY
        _map_view_.panX -= dx
        _map_view_.panY -= dy
        __lastX = mouse.x
        __lastY = mouse.y
      }
      if(__isWheeling == 1)
      { // If we are zooming then we should stop
        __isWheeling = 0
        hoverEnabled = false
      }
    }

    onWheel: wheel =>
    {
      if(wheel.angleDelta.y > 0)
      {
        // First time we zoom in? Then we should record mouse pointer position
        if(__isWheeling == 0)
        {
          __isWheeling = 1
          __lastX = wheel.x
          __lastY = wheel.y
          hoverEnabled = true
        }
        
        // Zoom to point
        _map_view_.zoomTo(__lastX, __lastY, _map_view_.zoom * zoomFactor, panFactor)
        
        // Adjust zooming point
        __lastX = (1-panFactor)* zoomFactor*(__lastX - 0.5*_map_view_.width) + 0.5*_map_view_.width
        __lastY = (1-panFactor)* zoomFactor*(__lastY - 0.5*_map_view_.height) + 0.5*_map_view_.height
      } else {
        _map_view_.zoomOut(zoomFactor)
      }
    }
  }
  Label
  {
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    text: "© <a href='https://www.openstreetmap.org/copyright'>OpenStreetMap</a> contributors."
    onLinkActivated: link => Qt.openUrlExternally(link)
  }
}

// Map
// {
//   id: root
//   zoomLevel: 14
//   center: QtPositioning.coordinate(58.4948444444, 15.1022444444)
//   plugin: Plugin { name: 'osm'; }
//   property ListModel layers: _layers
//   property variant mapRectangle
//   property Connection connection
//   function __update_mapRectangle()
//   {
//     mapRectangle = QtPositioning.rectangle(root.toCoordinate(Qt.point(0, 0)), root.toCoordinate(Qt.point(root.width - 1, root.height - 1), false))
//   }
  
//   onCenterChanged: root.__update_mapRectangle()
//   onZoomLevelChanged: root.__update_mapRectangle()
//   onWidthChanged: root.__update_mapRectangle()
//   onHeightChanged: root.__update_mapRectangle()
  
//   Component
//   {
//     id: test_component
//     MapCircle
//     {
//       property variant layerData
//       color: "blue"
//       radius: 50
//       center: QtPositioning.coordinate(58.4948444444, 15.1022444444)
//     }
//   }
//   Component
//   {
//     id: images_component
//     MapQuickItem
//     {
//       property variant layerData
//       anchorPoint.x: images_view.width * 0.5
//       anchorPoint.y: images_view.height * 0.5
//       coordinate: root.center
//       sourceItem: ImagesMapView {
//         id: images_view
//         width: root.width
//         height: root.height
//         mapRectangle: root.mapRectangle
//         source: layerData.source
//         connection: root.connection
//       }
//       zoomLevel: root.zoomLevel
//     }
//   }
//   ListModel
//   {
//     id: _layers
//     ListElement
//     {
//       name: "Orthoimages"
//       visible: true
//       delegateName: "images_component"
//       source: "orthoimages(id,rast)"
//     }
//   }
  
//   function __get_component(name)
//   {
//     if(name == "images_component")
//     {
//       return images_component;
//     } else if(name == "test_component")
//     {
//       return test_component;
//     } else {
//       console.log("In MapView.qml (__get_component): unknwon component: " + name)
//       return null
//     }
//   }
//   property variant __layers: []
//   function __reloadLayers()
//   {
//     console.log(root.visibleRegion.topLeft)
    
//     __layers.forEach(function(elt) {
//       root.removeMapItem(elt)
//     })
//     var new_layers = []
    
//     for(var i = 0; i < layers.count; ++i)
//     {
//       var layer = layers.get(i)
//       var item = root.__get_component(layer.delegateName).createObject(root, { "layerData": layer })
//       item.layerData = layer
//       item.visible = Qt.binding(function() { return item.layerData.visible; })
//       new_layers.push(item)
//       root.addMapItem(item)
//     }
//     __layers = new_layers
//   }
  
//   onLayersChanged: root.__reloadLayers()
//   Component.onCompleted:
//   {
//     root.__reloadLayers()
//     root.__update_mapRectangle()
//   }
// }
