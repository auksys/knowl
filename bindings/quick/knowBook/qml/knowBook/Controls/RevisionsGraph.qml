import Cyqlops.Graphviz
import QtQuick
import QtQuick.Controls

import knowCore

ScrollView
{
  id: root
  property alias model: graph_view.model
  property var revision
  
  horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOn
  verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn
  Rectangle {
    color: "white"
    width: graph_view.width
    height: graph_view.height
    GraphView
    {
      id: graph_view
      nodeDelegate: Rectangle {
        function __color(revision, node)
        {
          if(revision == node)
          {
            return "lightgreen"
          } else if(node.deltas.length > 1)
          {
            return "lightsalmon"
          } else {
            return "lightblue"
          }
        }
        color: __color(revision, node)
        width: 300
        height: 75
        Label
        {
          anchors.centerIn: parent
          text: "Hash: " + KnowCore.toHex(node.hash).substring(0, 10) + "\nContent-Hash: " + KnowCore.toHex(node.contentHash).substring(0, 10) + "\nHistoricity: " + node.historicity
          color: "black"
        }
        MouseArea
        {
          anchors.fill: parent
          onClicked: root.revision = node
        }
      }
    }
  }
}
