import QtQuick
import QtQuick.Controls

import knowCore

import knowBook.NotebookStyle

// Duplicated from Cyqlops.TableView to be able to import knowBook.NotebookStyle
// See README.MD in knowBook/NotebookStyle for discussion on the rational.

Control
{
  id: _root_
  property alias model: _table_view_.model
  property alias delegate: _table_view_.delegate

  property alias horizontalHeaderModel: _horizontal_header_.model
  property alias horizontalHeaderVisible: _horizontal_header_.visible

  property alias veritcalHeaderModel: _vertical_header_.model
  property alias verticalHeaderVisible: _vertical_header_.visible

  property alias columnSpacing: _table_view_.columnSpacing
  property alias rowSpacing: _table_view_.rowSpacing

  property int contentHeight: _table_view_.contentHeight + (_horizontal_header_.visible ? _horizontal_header_.height : 0)

  HorizontalHeaderView
  {
    id: _horizontal_header_
    anchors.left: _table_view_.left
    anchors.top: parent.top
    syncView: _table_view_
    clip: true
    visible: false
  }

  VerticalHeaderView
  {
    id: _vertical_header_
    anchors.top: _table_view_.top
    anchors.left: parent.left
    syncView: _table_view_
    visible: false
    clip: true
  }

  TableView
  {
    id: _table_view_
    anchors.left: _vertical_header_.right
    anchors.top: _horizontal_header_.bottom
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    clip: true

    columnSpacing: 1
    rowSpacing: 1

    delegate: Rectangle
    {
      implicitWidth: _text_.width + 6
      implicitHeight: _text_.height + 6
      Text
      {
        id: _text_
        text: KnowCore.displayString(model.display)
        anchors.left: parent.left
        anchors.leftMargin: 3
        anchors.verticalCenter: parent.verticalCenter
      }
      clip: true
      color: palette.base
    }
  }
}
