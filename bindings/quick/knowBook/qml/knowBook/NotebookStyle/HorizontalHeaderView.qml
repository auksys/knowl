import QtQuick
import QtQuick.Templates as T

T.HorizontalHeaderView {
  id: control

  implicitWidth: syncView ? syncView.width : 0
  implicitHeight: contentHeight

  delegate: Rectangle {
    // Qt6: add cellPadding (and font etc) as public API in headerview
    readonly property real cellPadding: 8

    implicitWidth: text.implicitWidth + (cellPadding * 2)
    implicitHeight: Math.max(control.height, text.implicitHeight + (cellPadding * 2))
    color: "#B0BEC5"

    Text {
      id: text
      text: control.textRole ? (Array.isArray(control.model) ? modelData[control.textRole]
                                  : model[control.textRole])
                              : modelData
      width: parent.width
      height: parent.height
      horizontalAlignment: Text.AlignHCenter
      verticalAlignment: Text.AlignVCenter
      color: "#ff26282a"
    }
  }
}
