import QtQuick
import QtQuick.Templates as T
import QtQuick.Controls.impl

T.ToolButton {
  id: control

  implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                 implicitContentWidth + leftPadding + rightPadding)
  implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                  implicitContentHeight + topPadding + bottomPadding)

  padding: 6
  spacing: 6

  icon.width: 16
  icon.height: 16

  contentItem: IconLabel {
    spacing: control.spacing
    mirrored: control.mirrored
    display: control.display

    icon: control.icon
    text: control.text
    font: control.font
    color: control.palette.buttonText
  }

  background: Rectangle {
    implicitWidth: 20
    implicitHeight: 20
    color: control.down ? "#FF9800" : control.enabled ? "#B0BEC5" : "#EEEEEE"
    radius: 4
  }
}
