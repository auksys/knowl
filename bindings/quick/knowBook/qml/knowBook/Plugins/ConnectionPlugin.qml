import QtQuick
import knowBook

QtObject
{
  readonly property int pluginType: PluginsManager.ConnectionPlugin
  property string name
  property Component storeComponent: null
  property Component connectionComponent: null
  property Component configurationComponent
}
