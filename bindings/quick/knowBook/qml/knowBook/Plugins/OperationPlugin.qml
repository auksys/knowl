import QtQuick
import knowBook

QtObject
{
  readonly property int pluginType: PluginsManager.OperationPlugin
  property string name
  property Component operationComponent
  function visible(context) { return false; }
}

