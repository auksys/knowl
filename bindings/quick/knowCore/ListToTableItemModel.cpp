#include "ListToTableItemModel.h"

#include <QDateTime>

using namespace knowCore::Quick;

struct ListToTableItemModel::Private
{
  QAbstractItemModel* sourceModel = nullptr;
  QList<int> columnToRole;
  QStringList columnNames;

  void updateColumnToRole();
  ListToTableItemModel* self;
};

void ListToTableItemModel::Private::updateColumnToRole()
{
  columnNames.clear();
  columnToRole.clear();
  if(sourceModel)
  {
    QList<int> roles = sourceModel->roleNames().keys();
    std::sort(roles.begin(), roles.end());
    for(int role : roles)
    {
      columnToRole.append(role);
      columnNames.append(sourceModel->roleNames()[role]);
    }
    self->columnNamesChanged();
  }
}

ListToTableItemModel::ListToTableItemModel() : d(new Private) { d->self = this; }

ListToTableItemModel::~ListToTableItemModel() {}

void ListToTableItemModel::setSourceModel(QAbstractItemModel* sourceModel)
{
  beginResetModel();
  d->sourceModel = sourceModel;
  d->updateColumnToRole();
  if(d->sourceModel)
  {
    connect(d->sourceModel, &QAbstractItemModel::modelAboutToBeReset,
            [this]() { beginResetModel(); });
    connect(d->sourceModel, &QAbstractItemModel::modelReset,
            [this]()
            {
              d->updateColumnToRole();
              endResetModel();
            });
  }
  endResetModel();
  emit(sourceModelChanged());
}

QAbstractItemModel* ListToTableItemModel::sourceModel() const { return d->sourceModel; }

QStringList ListToTableItemModel::columnNames() const { return d->columnNames; }

namespace
{
  bool isVariantLessThan(const QVariant& left, const QVariant& right)
  {
    if(left.userType() == QVariant::Invalid)
      return false;
    if(right.userType() == QVariant::Invalid)
      return true;
    switch(left.userType())
    {
    case QVariant::Int:
      return left.toInt() < right.toInt();
    case QVariant::UInt:
      return left.toUInt() < right.toUInt();
    case QVariant::LongLong:
      return left.toLongLong() < right.toLongLong();
    case QVariant::ULongLong:
      return left.toULongLong() < right.toULongLong();
    case QMetaType::Float:
      return left.toFloat() < right.toFloat();
    case QVariant::Double:
      return left.toDouble() < right.toDouble();
    case QVariant::Char:
      return left.toChar() < right.toChar();
    case QVariant::Date:
      return left.toDate() < right.toDate();
    case QVariant::Time:
      return left.toTime() < right.toTime();
    case QVariant::DateTime:
      return left.toDateTime() < right.toDateTime();
    case QVariant::String:
    default:
      return left.toString().compare(right.toString()) < 0;
    }
  }
} // namespace

QVariantList ListToTableItemModel::calculateColumnMinMax(int _column) const
{
  int rows = rowCount();
  if(rows == 0)
    return QVariantList() << QVariant() << QVariant();

  int role = d->columnToRole[_column];

  QVariant min, max;

  for(int row = 0; row < rows; ++row)
  {
    QVariant v = d->sourceModel->data(d->sourceModel->index(row, 0), role);
    if(v.isValid())
    {
      if(min.isValid())
      {
        if(isVariantLessThan(v, min))
          min = v;
        if(isVariantLessThan(max, v))
          max = v;
      }
      else
      {
        min = v;
        max = v;
      }
    }
  }
  return QVariantList() << min << max;
}

int ListToTableItemModel::columnCount(const QModelIndex& parent) const
{
  Q_UNUSED(parent);
  return d->columnToRole.size();
}

int ListToTableItemModel::rowCount(const QModelIndex& parent) const
{
  if(d->sourceModel)
  {
    return d->sourceModel->rowCount(parent);
  }
  else
  {
    return 0;
  }
}

QVariant ListToTableItemModel::data(const QModelIndex& index, int role) const
{
  Q_UNUSED(role);
  if(d->sourceModel)
  {
    return d->sourceModel->data(d->sourceModel->index(index.row(), 0, index.parent()),
                                d->columnToRole[index.column()]);
  }
  else
  {
    return QVariant();
  }
}

#include "moc_ListToTableItemModel.cpp"
