#ifndef _KNOW_CORE_LISTTOTABLEITEMMODEL_H_
#define _KNOW_CORE_LISTTOTABLEITEMMODEL_H_

#include <QAbstractItemModel>

namespace knowCore::Quick
{
  class ListToTableItemModel : public QAbstractTableModel
  {
    Q_OBJECT
    Q_PROPERTY(QAbstractItemModel* sourceModel READ sourceModel WRITE setSourceModel NOTIFY
                 sourceModelChanged)
    Q_PROPERTY(QStringList columnNames READ columnNames NOTIFY columnNamesChanged)
  public:
    ListToTableItemModel();
    ~ListToTableItemModel();
    void setSourceModel(QAbstractItemModel* sourceModel);
    QAbstractItemModel* sourceModel() const;
  public:
    QStringList columnNames() const;
    Q_INVOKABLE QVariantList calculateColumnMinMax(int _column) const;
  public:
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
  signals:
    void sourceModelChanged();
    void columnNamesChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowCore::Quick

#endif
