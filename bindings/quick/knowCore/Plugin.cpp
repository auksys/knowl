#include "Plugin.h"

#include <QtQml>

#include <knowCore/Uri.h>
#include <knowCore/UriList.h>

#include "ListToTableItemModel.h"
#include "UtilsSingleton.h"

using namespace knowCore::Quick;

void Plugin::registerTypes(const char* /*uri*/)
{
  const char* uri_knowCore = "knowCore";

  qRegisterMetaType<knowCore::UriList>("::knowCore::UriList");
  qRegisterMetaType<knowCore::UriList>("knowCore::UriList");

  qmlRegisterAnonymousType<QAbstractItemModel>(uri_knowCore, 1);
  qmlRegisterSingletonType<UtilsSingleton>(uri_knowCore, 1, 0, "KnowCore",
                                           [](QQmlEngine*, QJSEngine*) -> QObject*
                                           { return new UtilsSingleton; });
  qmlRegisterType<ListToTableItemModel>(uri_knowCore, 1, 0, "ListToTableItemModel");
}

#include "moc_Plugin.cpp"
