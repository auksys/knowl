#pragma once

#include <QQmlExtensionPlugin>

namespace knowCore::Quick
{
  class Plugin : public QQmlExtensionPlugin
  {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "knowCore::Quick/3.0")
  public:
    void registerTypes(const char* uri) override;
  };
} // namespace knowCore::Quick
