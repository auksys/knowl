set(KNOWCOREQUICK_SRCS
    Object.cpp
    )

add_library(knowCoreQuick SHARED ${KNOWCOREQUICK_SRCS})
target_link_libraries(knowCoreQuick PUBLIC Qt6::Quick knowCore)
install(TARGETS knowCoreQuick EXPORT knowLTargets ${INSTALL_TARGETS_DEFAULT_ARGS} )
set(PROJECT_EXPORTED_TARGETS knowCoreQuick ${PROJECT_EXPORTED_TARGETS} CACHE INTERNAL "")

install( FILES
  Object.h
  Error.h
  DESTINATION ${INSTALL_INCLUDE_DIR}/knowCore/Quick )
