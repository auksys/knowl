#include <QtQml>

#include <clog_qt>
#include <cres_qt>

namespace knowCore::Quick
{
  QQmlEngine* getEngine(const QObject* _object)
  {
    QQmlEngine* e = qmlEngine(_object);
    if(e)
      return e;
    if(_object->parent())
      return getEngine(_object->parent());
    return nullptr;
  }

  template<typename... _Args>
  void throwError(const QObject* _object, std::format_string<_Args...> __fmt, _Args&&... __args)
  {
    QString msg
      = QString::fromStdString(std::vformat(__fmt.get(), std::make_format_args(__args...)));
    QQmlEngine* e = getEngine(_object);
    if(e)
    {
      e->throwError(msg);
    }
    else
    {
      clog_error("Failed to throw error: {}", msg);
    }
  }

  template<typename _T_>
  bool handleError(const QObject* _object, const cres_qresult<_T_>& _t)
  {
    if(_t.is_successful())
    {
      return true;
    }
    else
    {
      throwError(_object, "{}", _t.get_error());
      return false;
    }
  }

} // namespace knowCore::Quick
