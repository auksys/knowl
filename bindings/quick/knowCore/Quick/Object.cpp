#include "Object.h"

using namespace knowCore::Quick;

struct Object::Private
{
  QString lastError;
};

Object::Object(QObject* _parent) : QObject(_parent), d(new Private) {}

Object::~Object() { delete d; }

QString Object::lastError() const { return d->lastError; }

bool Object::updateLastError(const cres_qresult<void>& _rv)
{
  if(_rv.is_successful())
  {
    d->lastError.clear();
  }
  else
  {
    d->lastError = _rv.get_error().get_message();
  }
  emit(lastErrorChanged());
  return _rv.is_successful();
}

void Object::setLastError(const QString& _error)
{
  d->lastError = _error;
  emit(lastErrorChanged());
}
