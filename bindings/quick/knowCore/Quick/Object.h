#pragma once

#include <QObject>

#include <clog_qt>
#include <cres_qt>

namespace knowCore::Quick
{
  class Object : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QString lastError READ lastError NOTIFY lastErrorChanged)
  public:
    Object(QObject* _parent = nullptr);
    ~Object();
    QString lastError() const;
  protected:
    bool updateLastError(const cres_qresult<void>& _rv);
    void setLastError(const QString& _error);
  signals:
    void lastErrorChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowCore::Quick
