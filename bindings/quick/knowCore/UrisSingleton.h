
#pragma once
#include <QObject>

#include <knowCore/Forward.h>
#include <knowCore/Uris/askcore_db.h>
#include <knowCore/Uris/askcore_graph.h>

namespace knowCore::Quick
{
#define DECLARE_URI(_name_)                                                                        \
  static inline QString _name_() { return Original::_name_; }                                      \
  Q_PROPERTY(QString _name_ READ _name_ CONSTANT)
  struct AskCoreQueries
  {
    using Original = knowCore::Uris::askcore_db_query_language;
    Q_GADGET
    DECLARE_URI(kdQL)
    DECLARE_URI(krQL)
    DECLARE_URI(SQL)
    DECLARE_URI(SPARQL)
  };
  struct AskCoreGraphs
  {
    using Original = knowCore::Uris::askcore_graph;
    Q_GADGET
    DECLARE_URI(all_agents)
    DECLARE_URI(all_datasets)
  };
  class UrisSingleton : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(knowCore::Quick::AskCoreQueries askCoreQueries MEMBER m_ask_core_queries CONSTANT)
    Q_PROPERTY(knowCore::Quick::AskCoreGraphs askCoreGraphs MEMBER m_ask_core_graphs CONSTANT)
  public:
    UrisSingleton(QObject* _parent = nullptr);
    ~UrisSingleton();
  private:
    AskCoreQueries m_ask_core_queries;
    AskCoreGraphs m_ask_core_graphs;
  };
} // namespace knowCore::Quick
