#include "UtilsSingleton.h"

#include <chrono>

#include <QtQml>

#include <QImage>
#include <QStandardPaths>
#include <QVariant>

#include <knowRDF/Literal.h>

#include <knowCore/Cast.h>
#include <knowCore/UriList.h>
#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Uris/xsd.h>

#include "UrisSingleton.h"

#include "Quick/Error.h"

using namespace knowCore::Quick;

UtilsSingleton::UtilsSingleton(QObject* _parent) : QObject(_parent)
{
  m_uris_singleton = new UrisSingleton(this);
}

UtilsSingleton::~UtilsSingleton() {}

QString UtilsSingleton::sparqlVariantToString(const QVariant& _variant)
{
  if(_variant.canConvert<QString>())
  {
    return _variant.value<QString>();
  }
  else if(_variant.canConvert<knowRDF::Literal>())
  {
    knowRDF::Literal lit = _variant.value<knowRDF::Literal>();
    QString str = lit.datatype() + "(" + lit.value<QString>().expect_success() + ")";
    if(not lit.lang().isEmpty())
    {
      str += "@" + lit.lang();
    }
    return str;
  }
  return QString("[Not convertible to string]");
}

qreal UtilsSingleton::now() const
{
  auto duration = std::chrono::system_clock::now().time_since_epoch();
  auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
  return millis;
}

QString UtilsSingleton::toHex(const QByteArray& _data) { return _data.toHex(); }

QString UtilsSingleton::toUtf8(const QByteArray& _data) { return QString::fromUtf8(_data); }

QString UtilsSingleton::getDatatype(const knowCore::Value& _value) { return _value.datatype(); }

QString UtilsSingleton::getDatatype(const QVariant& _value)
{
  auto const& [success, value, errMsg] = knowCore::Value::fromVariant(_value);
  if(success)
  {
    return getDatatype(value.value());
  }
  else
  {
    knowCore::Quick::throwError(
      this, "Variant cannot be converted to knowCore::Value with error: {}", errMsg.value());
    return QString();
  }
}

QString UtilsSingleton::displayString(const knowCore::Value& _value)
{
  auto const& [s, l, m] = _value.printable();
  if(s)
  {
    if(_value.datatype() == knowCore::Uris::xsd::anyURI)
    {
      return clog_qt::qformat("<{}>", l.value());
    }
    else if(_value.datatype() == knowCore::Uris::xsd::string)
    {
      return clog_qt::qformat("\"{}\"", l.value());
    }
    else if(_value.datatype() == knowCore::Uris::askcore_datatype::literal)
    {
      return l.value();
    }
    else
    {
      return clog_qt::qformat("{}({})", _value.datatype(), l.value());
    }
  }
  else
  {
    return clog_qt::qformat("Could not convert to literal with error: {}", m.value());
  }
}

QString UtilsSingleton::displayString(const QVariant& _value)
{
  auto const& [success, value, errMsg] = knowCore::Value::fromVariant(_value);
  if(success)
  {
    return displayString(value.value());
  }
  else
  {
    return clog_qt::to_qstring(_value);
  }
}

QVariant UtilsSingleton::valueToVariant(const knowCore::Value& _value)
{
  return _value.toVariant();
}

QImage UtilsSingleton::toImage(const QByteArray& _data, int _width, int _height,
                               const QString& _encoding, const QString& _compression)
{
  QImage img;

  if(_compression == "none")
  {
    QImage::Format format;
    if(_encoding == "rgb8")
    {
      format = QImage::Format_RGB888;
    }
    else if(_encoding == "bgr8")
    {
      format = QImage::Format_BGR888;
    }
    else
    {
      clog_error("Unsupported format '{}'", _encoding);
      return QImage();
    }
    img = QImage(_width, _height, format);
    if(img.sizeInBytes() != _data.size())
    {
      clog_error("QImage size {} does not match data size {}", img.sizeInBytes(), _data.size());
    }
    else
    {
      std::copy(_data.begin(), _data.end(), img.scanLine(0));
    }
  }
  else
  {
    if(not img.loadFromData(_data))
    {
      clog_error("Failed to uncompress image with encoding '{}'", _compression);
    }
  }

  return img;
}

QList<QString> UtilsSingleton::fromUriList(const knowCore::UriList& _list)
{
  return _list.toStringList();
}

UrisSingleton* UtilsSingleton::uris() const { return m_uris_singleton; }

#include "moc_UtilsSingleton.cpp"
