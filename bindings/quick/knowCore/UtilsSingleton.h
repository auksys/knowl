#ifndef _KNOWCORE_UTILS_ATTACHED_PROPERTY_H_
#define _KNOWCORE_UTILS_ATTACHED_PROPERTY_H_

#include <QObject>

#include <knowCore/Forward.h>

namespace knowCore::Quick
{
  class UrisSingleton;
  class UtilsSingleton : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(knowCore::Quick::UrisSingleton* Uris READ uris CONSTANT)
  public:
    UtilsSingleton(QObject* _parent = nullptr);
    ~UtilsSingleton();

    Q_INVOKABLE QString sparqlVariantToString(const QVariant& _variant);
    Q_INVOKABLE qreal now() const;
    Q_INVOKABLE QString toHex(const QByteArray& _data);
    Q_INVOKABLE QString toUtf8(const QByteArray& _data);
    Q_INVOKABLE QString getDatatype(const knowCore::Value& _value);
    Q_INVOKABLE QString getDatatype(const QVariant& _value);
    Q_INVOKABLE QString displayString(const knowCore::Value& _value);
    Q_INVOKABLE QString displayString(const QVariant& _value);
    Q_INVOKABLE QVariant valueToVariant(const knowCore::Value& _value);
    Q_INVOKABLE QImage toImage(const QByteArray& _data, int _width, int _height,
                               const QString& _encoding, const QString& _compression);
    Q_INVOKABLE QList<QString> fromUriList(const knowCore::UriList& _list);
  protected:
    UrisSingleton* uris() const;
  private:
    UrisSingleton* m_uris_singleton;
  };
} // namespace knowCore::Quick

#endif
