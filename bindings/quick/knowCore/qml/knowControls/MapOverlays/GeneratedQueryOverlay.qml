import QtLocation
import knowCore
import kDBQueries
import kDB.Repository

MapItemView
{
  id: root
  property alias queries: __query__.queries
  property alias queryName: __query__.queryName
  property alias connection: __query__.connection
  property alias bindings: __query__.bindings
  property var __query: GeneratedQuery
  {
    id: __query__
    queries: root.queries
    queryName: root.queryName
    connection: root.connection
    autoExecute: true
  }
  model: __query.result
}

