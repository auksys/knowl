import QtLocation
import knowCore
import kDB.Repository

MapItemView
{
  id: root
  property string query
  property Connection connection
  property var __query: SPARQLQuery
  {
    query: root.query
    connection: root.connection
    autoExecute: true
  }
  model: __query.result
}
