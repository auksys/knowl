#include "Plugin.h"

#include <QtQml>

#include <knowCore/Forward.h>

#include "Query.h"

using namespace knowDBC::Quick;

void Plugin::registerTypes(const char* /*uri*/)
{
  const char* uri_knowDBC = "knowDBC";
  qmlRegisterType<knowDBC::Quick::Query>(uri_knowDBC, 1, 0, "Query");
}

#include "moc_Plugin.cpp"
