#pragma once

#include <QQmlExtensionPlugin>

namespace knowDBC::Quick
{
  class Plugin : public QQmlExtensionPlugin
  {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "knowDBC/3.0")
  public:
    void registerTypes(const char* uri) override;
  };
} // namespace knowDBC::Quick
