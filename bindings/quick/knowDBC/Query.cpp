#include "Query.h"

#include <knowCore/TypeDefinitions.h>
#include <knowCore/ValueHash.h>

#include <knowDBC/Connection.h>
#include <knowDBC/Result.h>

#include "QueryResultModel.h"

using namespace knowDBC::Quick;

struct Query::Private
{
  knowDBC::Connection connection;
  QString query, lastError;
  QueryResultModel* result = nullptr;
  bool autoExecute = false;
  QVariantMap bindings;
  bool isMultipleQueries = false;
  QVariantMap environment;
  knowCore::Uri queryType;
};

Query::Query(QObject* _parent) : QObject(_parent), d(new Private)
{
  d->result = new QueryResultModel(this);
}

Query::~Query() { delete d; }

knowDBC::Connection Query::connection() const { return d->connection; }

void Query::setConnection(const knowDBC::Connection& _connection)
{
  d->connection = _connection;
  emit(connectionChanged());
  executeIfNeeded();
}

bool Query::isAutoExecute() const { return d->autoExecute; }

void Query::setAutoExecute(bool _exec)
{
  if(d->autoExecute != _exec)
  {
    d->autoExecute = _exec;
    emit(autoExecuteChanged());
    executeIfNeeded();
  }
}

QString Query::lastError() const { return d->lastError; }

QAbstractItemModel* Query::result() const { return d->result; }

QString Query::query() { return d->query; }

void Query::setQuery(const QString& _query)
{
  if(d->query != _query)
  {
    d->query = _query;
    emit(queryChanged());
    executeIfNeeded();
  }
}

QString Query::queryType() const { return d->queryType; }

void Query::setQueryType(const QString& _queryType)
{
  knowCore::Uri qTu = _queryType;
  if(d->queryType != qTu)
  {
    d->queryType = qTu;
    emit(queryTypeChanged());
    executeIfNeeded();
  }
}

void Query::executeIfNeeded()
{
  if(d->autoExecute and d->connection.isValid())
  {
    execute();
  }
}

QVariant Query::bindings() const { return d->bindings; }

void Query::setBindings(const QVariant& _bindings)
{
  QVariantMap nb = _bindings.toMap();
  if(nb != d->bindings)
  {
    d->bindings = nb;
    emit(bindingsChanged());
    executeIfNeeded();
  }
}

QVariant Query::environment() const { return d->environment; }

void Query::setEnvironment(const QVariant& _environment)
{
  QVariantMap nb = _environment.toMap();
  if(nb != d->environment)
  {
    d->environment = nb;
    emit(environmentChanged());
    executeIfNeeded();
  }
}

void Query::execute()
{
  if(not d->connection.isValid() or not d->connection.isConnected())
  {
    d->lastError = "Not connected!";
    emit(lastErrorChanged());
    return;
  }
  knowCore::ValueHash env;
  env.insert(d->environment);
  cres_qresult<knowDBC::Query> q_rv = d->connection.createQuery(d->queryType, env);
  if(not q_rv.is_successful())
  {
    d->lastError = clog_qt::qformat("Failure to create query of type {} with error {}.",
                                    d->queryType, q_rv.get_error());
    emit(lastErrorChanged());
    return;
  }
  knowDBC::Query q = q_rv.get_value();
  q.setQuery(d->query);
  knowCore::ValueHash bb;
  bb.insert(d->bindings);
  q.bindValues(bb);
  if(d->isMultipleQueries)
  {
    q.setOption(knowDBC::Query::OptionsKeys::MultiQueries, true);
  }
  knowDBC::Result r = q.execute();
  if(r)
  {
    d->result->setResult(r);
    emit(executionFinished());
  }
  else
  {
    d->result->setResult(r);
    d->lastError = r.error();
    emit(lastErrorChanged());
  }
}

bool Query::isMultipleQueries() const { return d->isMultipleQueries; }

void Query::setMultipleQueries(bool _v)
{
  if(d->isMultipleQueries != _v)
  {
    d->isMultipleQueries = _v;
    emit(multipleQueriesChanged());
    executeIfNeeded();
  }
}

#include "moc_Query.cpp"
