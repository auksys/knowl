#pragma once

#include <QObject>

#include <knowDBC/Forward.h>

class QAbstractItemModel;

namespace knowDBC::Quick
{
  class Query : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(
      knowDBC::Connection connection READ connection WRITE setConnection NOTIFY connectionChanged);
    Q_PROPERTY(QString query READ query WRITE setQuery NOTIFY queryChanged);
    Q_PROPERTY(QAbstractItemModel* result READ result CONSTANT);
    Q_PROPERTY(bool autoExecute READ isAutoExecute WRITE setAutoExecute NOTIFY autoExecuteChanged);
    Q_PROPERTY(QString lastError READ lastError NOTIFY lastErrorChanged);
    Q_PROPERTY(QVariant bindings READ bindings WRITE setBindings NOTIFY bindingsChanged);
    Q_PROPERTY(bool multipleQueries READ isMultipleQueries WRITE setMultipleQueries NOTIFY
                 multipleQueriesChanged);
    Q_PROPERTY(
      QVariant environment READ environment WRITE setEnvironment NOTIFY environmentChanged);
    Q_PROPERTY(QString queryType READ queryType WRITE setQueryType NOTIFY queryTypeChanged);
  public:
    Query(QObject* _parent = nullptr);
    ~Query();
    Q_INVOKABLE void execute();
    knowDBC::Connection connection() const;
    void setConnection(const knowDBC::Connection& _connection);
    QVariant bindings() const;
    void setBindings(const QVariant& _bindings);
    QVariant environment() const;
    void setEnvironment(const QVariant& _environment);
    QString query();
    void setQuery(const QString& _query);
    QAbstractItemModel* result() const;
    bool isAutoExecute() const;
    void setAutoExecute(bool _exec);
    bool isMultipleQueries() const;
    void setMultipleQueries(bool _v);
    QString lastError() const;
    QString queryType() const;
    void setQueryType(const QString& _queryType);
  protected:
    void executeIfNeeded();
  signals:
    /**
     * This signal is emited when the execution of the query is finished
     */
    void executionFinished();
    void connectionChanged();
    void queryChanged();
    void autoExecuteChanged();
    void lastErrorChanged();
    void bindingsChanged();
    void multipleQueriesChanged();
    void queryTypeChanged();
    void environmentChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowDBC::Quick
