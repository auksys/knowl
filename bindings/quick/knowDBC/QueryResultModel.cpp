#include "QueryResultModel.h"

#include <QBuffer>
#include <QFile>

#include <knowRDF/BlankNode.h>
#include <knowRDF/Literal.h>

#include <knowDBC/Result.h>

#include <knowCore/Quick/Error.h>

using namespace knowDBC::Quick;

struct QueryResultModel::Private
{
  knowDBC::Result result;
};

QueryResultModel::QueryResultModel(QObject* parent) : QAbstractListModel(parent), d(new Private) {}

QueryResultModel::QueryResultModel(const knowDBC::Result& _result, QObject* parent)
    : QueryResultModel(parent)
{
  d->result = _result;
}

QueryResultModel::~QueryResultModel() { delete d; }

int QueryResultModel::roleIndex(const QString& _role) const
{
  return Qt::UserRole + d->result.fieldIndex(_role);
}

QStringList QueryResultModel::columnNames() const
{
  QStringList column_names;
  if(d->result.isValid())
  {
    for(int i = 0; i < d->result.fields(); ++i)
    {
      column_names.append(d->result.fieldName(i));
    }
  }
  return column_names;
}

QHash<int, QByteArray> QueryResultModel::roleNames() const
{
  QHash<int, QByteArray> roles;
  if(d->result.isValid())
  {
    for(int i = 0; i < d->result.fields(); ++i)
    {
      roles[Qt::UserRole + i] = d->result.fieldName(i).toUtf8();
    }
  }
  return roles;
}

int QueryResultModel::rowCount(const QModelIndex& parent) const
{
  Q_UNUSED(parent);
  return d->result.isValid() ? d->result.tuples() : 0;
}

QVariant QueryResultModel::data(const QModelIndex& index, int role) const
{
  if(index.isValid() and role >= Qt::UserRole and role < Qt::UserRole + d->result.fields())
  {
    return QVariant::fromValue(d->result.value(index.row(), role - Qt::UserRole));
  }
  else
  {
    return QVariant();
  }
}

knowDBC::Result QueryResultModel::result() const { return d->result; }

void QueryResultModel::setResult(const knowDBC::Result& _t)
{
  beginResetModel();
  d->result = _t;
  endResetModel();
  emit(columnNamesChanged());
}

bool QueryResultModel::saveTo(const QUrl& _filename, Type _type) const
{
  QFile file(_filename.toLocalFile());
  file.open(QIODevice::WriteOnly);
  knowCore::Uri fileformat;
  switch(_type)
  {
  case Type::CSV:
    fileformat = knowCore::FileFormat::CSV;
    break;
  case Type::JSON:
    fileformat = knowCore::FileFormat::JSON;
    break;
  case Type::SRX:
    fileformat = knowCore::FileFormat::SRX;
    break;
  }

  auto const [r, errMessage] = result().write(&file, fileformat);
  if(not r)
  {
    clog_warning("Failed to save to: '{}' with error: {}", _filename.toLocalFile(),
                 errMessage.value());
  }
  return r;
}

QByteArray QueryResultModel::serialise(const QString& _format)
{
  QBuffer buffer;
  cres_qresult<void> r_s = result().write(&buffer, _format);
  if(knowCore::Quick::handleError(this, r_s))
  {
    return buffer.buffer();
  }
  else
  {
    return QByteArray();
  }
}

#include "moc_QueryResultModel.cpp"
