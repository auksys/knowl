#pragma once

#include <QAbstractListModel>

#include <knowDBC/Forward.h>

namespace knowDBC::Quick
{
  class QueryResultModel : public QAbstractListModel
  {
    Q_OBJECT
    Q_PROPERTY(QStringList columnNames READ columnNames NOTIFY columnNamesChanged)
  public:
    enum class Type
    {
      CSV,
      JSON,
      SRX
    };
    Q_ENUM(Type)
  public:
    explicit QueryResultModel(QObject* parent = nullptr);
    QueryResultModel(const knowDBC::Result& _result, QObject* parent = nullptr);
    virtual ~QueryResultModel();
    QStringList columnNames() const;
    Q_INVOKABLE int roleIndex(const QString& _role) const;
    Q_INVOKABLE bool saveTo(const QUrl& _filename, Type _type) const;
    Q_INVOKABLE QByteArray serialise(const QString& _format);
  protected:
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;
  public:
    knowDBC::Result result() const;
    void setResult(const knowDBC::Result& _t);
  signals:
    void columnNamesChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowDBC::Quick
