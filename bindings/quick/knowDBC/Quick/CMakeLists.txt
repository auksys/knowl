set(KNOWDBCQUICK_SRCS
    Connection.cpp
    Store.cpp
    )

add_library(knowDBCQuick SHARED ${KNOWDBCQUICK_SRCS})
target_link_libraries(knowDBCQuick PUBLIC Qt6::Quick knowDBC knowCoreQuick)
install(TARGETS knowDBCQuick EXPORT knowLTargets ${INSTALL_TARGETS_DEFAULT_ARGS} )
set(PROJECT_EXPORTED_TARGETS knowDBCQuick ${PROJECT_EXPORTED_TARGETS} CACHE INTERNAL "")

install( FILES
  Connection.h
  Forward.h
  Store.h
  DESTINATION ${INSTALL_INCLUDE_DIR}/knowDBC/Quick )
