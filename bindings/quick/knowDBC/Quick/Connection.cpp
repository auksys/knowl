#include "Connection.h"

#include <knowDBC/Connection.h>

using namespace knowDBC::Quick;

struct Connection::Private
{
  bool auto_connect = false;
};

Connection::Connection(QObject* _parent) : knowCore::Quick::Object(_parent), d(new Private) {}

Connection::~Connection() {}

bool Connection::isAutoConnect() const { return d->auto_connect; }

void Connection::setAutoConnect(bool _v)
{
  if(d->auto_connect != _v)
  {
    d->auto_connect = _v;
    emit(autoConnectChanged());
    if(d->auto_connect)
    {
      disconnect();
      connect();
    }
  }
}

bool Connection::supportQuery(const QString& _type) const
{
  knowDBC::Connection c = connection();
  return c.isValid() and c.supportQuery(_type);
}

#include "moc_Connection.cpp"
