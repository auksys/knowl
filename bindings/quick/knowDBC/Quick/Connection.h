#pragma once

#include <knowCore/Quick/Object.h>

#include <knowDBC/Forward.h>

namespace knowDBC::Quick
{
  class Connection : public knowCore::Quick::Object
  {
    Q_OBJECT
    Q_PROPERTY(bool connected READ isConnected NOTIFY connectionStatusChanged)
    Q_PROPERTY(bool autoConnect READ isAutoConnect WRITE setAutoConnect NOTIFY autoConnectChanged)
    Q_PROPERTY(knowDBC::Connection connection READ connection NOTIFY connectionStatusChanged)
  public:
    Connection(QObject* _parent = nullptr);
    ~Connection();
    bool isAutoConnect() const;
    void setAutoConnect(bool _v);
    virtual bool isConnected() const = 0;
    Q_INVOKABLE bool supportQuery(const QString& _type) const;
    virtual knowDBC::Connection connection() const = 0;
    Q_INVOKABLE virtual bool connect() = 0;
    Q_INVOKABLE virtual bool disconnect() = 0;
  signals:
    void connectionStatusChanged();
    void autoConnectChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowDBC::Quick
