#include "Store.h"

#include "Connection.h"

using namespace knowDBC::Quick;

struct Store::Private
{
  Connection* connection;
};

Store::Store(Connection* _connection, QObject* _parent)
    : knowCore::Quick::Object(_parent), d(new Private)
{
  d->connection = _connection;
  d->connection->setParent(this);
  QObject::connect(d->connection, SIGNAL(autoConnectChanged()), this, SIGNAL(autoConnectChanged()));
}

Store::~Store() { delete d; }

bool Store::isAutoConnect() const { return d->connection->isAutoConnect(); }

void Store::setAutoConnect(bool _v) { d->connection->setAutoConnect(_v); }

Connection* Store::connection() const { return d->connection; }

#include "moc_Store.cpp"
