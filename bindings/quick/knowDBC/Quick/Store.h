#pragma once

#include <knowCore/Quick/Object.h>

#include "Forward.h"

namespace knowDBC::Quick
{
  class Store : public knowCore::Quick::Object
  {
    Q_OBJECT
    Q_PROPERTY(bool storeRunning READ isStoreRunning NOTIFY storeRunningChanged)
    Q_PROPERTY(bool autoConnect READ isAutoConnect WRITE setAutoConnect NOTIFY autoConnectChanged)
    Q_PROPERTY(knowDBC::Quick::Connection* connection READ connection CONSTANT)
  public:
    Store(Connection* _connection, QObject* _parent = nullptr);
    ~Store();
    Q_INVOKABLE virtual bool start() = 0;
    Q_INVOKABLE virtual bool stop() = 0;
    Q_INVOKABLE virtual bool erase() = 0;
    virtual bool isStoreRunning() const = 0;
    bool isAutoConnect() const;
    void setAutoConnect(bool _v);
    Connection* connection() const;
  signals:
    void autoConnectChanged();
    void storeRunningChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowDBC::Quick
