#include "Plugin.h"

#include <QtQml>

#include <knowCore/ListToTableItemModel.h>

#include <knowGIS/GeometryObject.h>

#include "UtilsSingleton.h"

#if(QT_VERSION < QT_VERSION_CHECK(5, 14, 0))
template<typename _T_>
void qmlRegisterAnonymousType(const char*, int)
{
  qmlRegisterType<_T_>();
}
#endif

using namespace knowGISQuick;

void Plugin::registerTypes(const char* /*uri*/)
{
  qRegisterMetaType<knowGIS::GeometryObject>("::knowGIS::GeometryObject");
  qRegisterMetaType<knowGIS::GeometryObject>("knowGIS::GeometryObject");
  QMetaType::registerConverter<knowGIS::GeometryObject, Cartography::GeometryObject>();

  const char* uri_knowGIS = "knowGIS";
  qmlRegisterSingletonType<UtilsSingleton>(uri_knowGIS, 1, 0, "KnowGIS",
                                           [](QQmlEngine*, QJSEngine*) -> QObject*
                                           { return new UtilsSingleton; });
}

#include "moc_Plugin.cpp"
