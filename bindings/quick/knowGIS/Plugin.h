#pragma once

#include <QQmlExtensionPlugin>

namespace knowGISQuick
{
  class Plugin : public QQmlExtensionPlugin
  {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "knowGISQuick/3.0")
  public:
    void registerTypes(const char* uri) override;
  };
} // namespace knowGISQuick
