#include "UtilsSingleton.h"

#include <chrono>

#include <QtQml>

#include <QVariant>

#include <knowCore/Value.h>
#include <knowGIS/GeoPoint.h>
#include <knowGIS/GeometryObject.h>
#include <knowGIS/Point.h>
#include <knowRDF/Literal.h>

#include <knowCore/Quick/Error.h>

using namespace knowGISQuick;

UtilsSingleton::UtilsSingleton(QObject* _parent) : QObject(_parent) {}

UtilsSingleton::~UtilsSingleton() {}

QString UtilsSingleton::toWkt(const QVariant& _variant)
{
  if(_variant.canConvert<knowGIS::GeometryObject>())
  {
    knowGIS::GeometryObject go = _variant.value<knowGIS::GeometryObject>();
    return go.toWKT();
  }
  knowCore::Quick::throwError(this, "Variant {} is not a knowGIS::GeometryObject.", _variant);
  return QString();
}

QVariant UtilsSingleton::fromWkt(const QString& _wkt)
{
  cres_qresult<knowGIS::GeometryObject> res = knowGIS::GeometryObject::fromWKT(_wkt);
  if(res.is_successful())
  {
    return QVariant::fromValue(res.get_value());
  }
  knowCore::Quick::throwError(this, "Failed to parse WKT string {} with error {}.", _wkt,
                              res.get_error());
  return QVariant();
}

QVariant UtilsSingleton::getGeoPoint(const knowCore::Value& _value)
{
  if(knowCore::ValueCast<knowGIS::GeometryObject> go = _value)
  {
    knowGIS::Point pt = go->toPoint();
    if(pt.isValid())
    {
      knowGIS::GeoPoint gp = knowGIS::GeoPoint::from(pt);
      QVariantMap m;
      m["longitude"] = gp.longitude().get_value();
      m["latitude"] = gp.latitude().get_value();
      return m;
    }
    else
    {
      clog_warning("{} is not a geo point.", _value);
      return QVariant();
    }
  }
  else
  {
    clog_warning("{} is not a GeometryObject", _value);
    return QVariant();
  }
}

QVariant UtilsSingleton::getGeoPoint(const QVariant& _variant)
{
  if(_variant.isValid())
  {
    if(_variant.canConvert<QVariantMap>())
    {
      QVariantMap map = _variant.toMap();
      if(map.contains("type"))
      {
        // TODO this should be somehow integrated in knowRDF::Literal::fromVariant or something, it
        // is also duplicated in ROS kDB and possibly a few other places
        auto const [success, lit, message] = knowRDF::Literal::fromRdfLiteral(
          map["type"].toString(), map["value"].toString(), map["lang"].toString());
        if(success)
        {
          return getGeoPoint(lit.value());
        }
        else
        {
          clog_warning("Failed to convert geopoint from {} to literal with error {}", _variant,
                       message.value());
          return QVariant();
        }
      }
      else
      {
        auto const [success, lit, message]
          = knowRDF::Literal::fromVariant(map["value"], map["lang"].toString());
        if(success)
        {
          return getGeoPoint(lit.value());
        }
        else
        {
          clog_warning("Failed to convert geopoint from {} to literal with error {}", _variant,
                       message.value());
          return QVariant();
        }
      }
    }
    else
    {
      auto const [success, value, message] = knowCore::Value::fromVariant(_variant);
      if(success)
      {
        return getGeoPoint(value.value());
      }
      else
      {
        clog_warning("Failed to convert '{}' to knowCore::Value with error: {}", _variant,
                     message.value());
        return QVariant();
      }
    }
  }
  else
  {
    clog_warning("Invalid variant cannot be converted to geo point.");
    return QVariant();
  }
}

#include "moc_UtilsSingleton.cpp"
