#ifndef _KNOWCORE_UTILS_ATTACHED_PROPERTY_H_
#define _KNOWCORE_UTILS_ATTACHED_PROPERTY_H_

#include <QObject>

#include <knowCore/Forward.h>

namespace knowGISQuick
{
  class UtilsSingleton : public QObject
  {
    Q_OBJECT
  public:
    UtilsSingleton(QObject* _parent = nullptr);
    ~UtilsSingleton();

    Q_INVOKABLE QString toWkt(const QVariant& _variant);
    Q_INVOKABLE QVariant fromWkt(const QString& _wkt);
    Q_INVOKABLE QVariant getGeoPoint(const QVariant& _variant);
  private:
    QVariant getGeoPoint(const knowCore::Value& _variant);
  };
} // namespace knowGISQuick

#endif
