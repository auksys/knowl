#include "Plugin.h"

#include <QtQml>

#include "UtilsSingleton.h"

using namespace knowValuesQuick;

void Plugin::registerTypes(const char* /*uri*/)
{
  const char* uri_knowValues = "knowValues";
  qmlRegisterSingletonType<UtilsSingleton>(uri_knowValues, 1, 0, "Utils",
                                           [](QQmlEngine*, QJSEngine*) -> QObject*
                                           { return new UtilsSingleton; });
}

#include "moc_Plugin.cpp"
