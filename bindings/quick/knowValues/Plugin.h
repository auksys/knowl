#pragma once

#include <QQmlExtensionPlugin>

namespace knowValuesQuick
{
  class Plugin : public QQmlExtensionPlugin
  {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "knowValuesQuick/3.0")
  public:
    void registerTypes(const char* uri) override;
  };
} // namespace knowValuesQuick
