#include "UtilsSingleton.h"

#include <chrono>

#include <QtQml>

#include <QStandardPaths>
#include <QVariant>

#include <knowValues/Values.h>

using namespace knowValuesQuick;

UtilsSingleton::UtilsSingleton(QObject* _parent) : QObject(_parent) {}

UtilsSingleton::~UtilsSingleton() {}

knowValues::Values::PointCloud UtilsSingleton::getPointCloud(const knowCore::Value& _value)
{
  auto const [success, pc, errorMessage] = _value.value<knowValues::Values::PointCloud>();
  if(success)
  {
    return pc.value();
  }
  else
  {
    qmlEngine(this)->throwError(clog_qt::qformat("Failed to get point cloud from: {} with error {}",
                                                 _value, errorMessage.value()));
    return knowValues::Values::PointCloud();
  }
}

QObject* UtilsSingleton::getObject(const knowCore::Value& _value)
{
  auto const [success, val, errorMessage] = _value.value<knowValues::Values::Value>();
  if(success)
  {
    return val.value()->clone();
  }
  else
  {
    qmlEngine(this)->throwError(clog_qt::qformat("Failed to get object from: {} with error {}",
                                                 _value, errorMessage.value()));
    return nullptr;
  }
}
#include "moc_UtilsSingleton.cpp"
