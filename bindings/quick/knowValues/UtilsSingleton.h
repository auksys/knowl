#pragma once

#include <QObject>

#include <knowValues/Forward.h>

namespace knowValuesQuick
{
  class UtilsSingleton : public QObject
  {
    Q_OBJECT
  public:
    UtilsSingleton(QObject* _parent = nullptr);
    ~UtilsSingleton();

    Q_INVOKABLE knowValues::Values::PointCloud getPointCloud(const knowCore::Value& _value);
    Q_INVOKABLE QObject* getObject(const knowCore::Value& _value);
  };
} // namespace knowValuesQuick
