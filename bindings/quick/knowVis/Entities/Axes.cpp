#include "Axes.h"

#include <Qt3DCore/QTransform>

#include <knowVis/GeoTransform.h>
#include <knowVis/Resources.h>

using namespace knowVis::Entities;

Axes::Axes(Qt3DCore::QNode* _node) : knowVis::Entity(_node)
{
  m_root = new Qt3DCore::QEntity(this);
  m_xAxis = new Qt3DCore::QEntity(m_root);
  Resources::addXAxe(m_xAxis);
  m_yAxis = new Qt3DCore::QEntity(m_root);
  Resources::addYAxe(m_yAxis);
  m_zAxis = new Qt3DCore::QEntity(m_root);
  Resources::addZAxe(m_zAxis);
  m_scaleTransform = new Qt3DCore::QTransform(m_root);
  m_root->addComponent(m_scaleTransform);
  setupTransform();
}

Axes::~Axes() {}

void Axes::setScale(qreal _s) { m_scaleTransform->setScale(_s); }

knowVis::Pose* Axes::pose() const { return transform()->pose(); }

#include <knowVis/Pose.h>

#include "moc_Axes.cpp"
