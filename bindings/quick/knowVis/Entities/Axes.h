#pragma once

#include <knowVis/Entity.h>

namespace knowVis::Entities
{
  class Axes : public knowVis::Entity
  {
    Q_OBJECT
    Q_PROPERTY(knowVis::Pose* pose READ pose CONSTANT);
    Q_PROPERTY(qreal scale WRITE setScale)
  public:
    Axes(Qt3DCore::QNode* _parent = nullptr);
    ~Axes();
    void setScale(qreal _s);
    knowVis::Pose* pose() const;
  private:
    Qt3DCore::QEntity* m_root;
    Qt3DCore::QEntity* m_xAxis;
    Qt3DCore::QEntity* m_yAxis;
    Qt3DCore::QEntity* m_zAxis;
    Qt3DCore::QTransform* m_scaleTransform;
  };
} // namespace knowVis::Entities
