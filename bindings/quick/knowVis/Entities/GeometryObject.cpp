#include "GeometryObject.h"

#include <QVector3D>

#include <Qt3DExtras/QPhongMaterial>

#include <knowGIS/GeoPose.h>

#include <knowVis/Context.h>
#include <knowVis/GeoTransform.h>
#include <knowVis/Pose.h>

#include "../Materials/GeometryObjectMaterial.h"
#include "../Renderers/PolylineRenderer.h"

using namespace knowVis::Entities;

GeometryObject::GeometryObject(Qt3DCore::QNode* _parent) : knowVis::Entity(_parent)
{
  m_polylineRenderer = new Renderers::PolylineRenderer(this);
  addComponent(m_polylineRenderer);
  m_material = new Materials::GeometryObjectMaterial(this);

  addComponent(m_material);
  setupTransform();

  transform()->setUsePose(false);

  connect(this, &knowVis::Entity::contextChanged,
          [this]()
          {
            if(context())
            {
              connect(context()->origin(), &Pose::poseChanged, this,
                      &GeometryObject::updateRenderer);
              updateRenderer();
            }
          });
}

GeometryObject::~GeometryObject() {}

namespace
{
  inline float safeValue(double _v) { return std::isnan(_v) ? 0.0 : _v; }
} // namespace

void GeometryObject::updateRenderer()
{
  if(m_object.isValid())
  {
    knowGIS::GeometryObject go = m_object;
    if(context())
    {
      knowGIS::GeoPose gp = context()->origin()->geoPose();
      Cartography::CoordinateSystem utm = context()->coordinateSystem();
      go = go.transform(utm);
    }

    using polygon = Cartography::EuclidSystem::polygon;
    using linear_ring = Cartography::EuclidSystem::linear_ring;
    Cartography::EuclidSystem::geometry geometry = go.geometry();
    if(geometry.is<polygon>())
    {
      polygon poly = geometry.cast<polygon>();
      if(poly.interior_rings().size() > 0)
      {
        clog_error("Polygons with interior rings are currently not supported, got: {}", m_object);
        m_polylineRenderer->setPolyline({});
      }
      else
      {
        QList<QVector3D> points;
        linear_ring poly_er = poly.exterior_ring();
        for(euclid::geos::point_iterator<linear_ring> it = poly_er.points().begin();
            it != poly_er.points().end(); ++it)
        {
          // TODO rotate
          euclid::simple::point pt = *it;
          points.append({safeValue(pt.x()), safeValue(pt.y()), safeValue(pt.z())});
        }
        qDebug() << points;
        m_polylineRenderer->setPolyline(points);
      }
    }
    else
    {
      clog_error("Only polygon are currently supported, got: {}", m_object);
      m_polylineRenderer->setPolyline({});
    }
  }
  else
  {
    m_polylineRenderer->setPolyline({});
  }
}

void GeometryObject::setGeometryObject(const knowGIS::GeometryObject& _cw)
{
  m_object = _cw;
  emit(geometryObjectChanged());
  updateRenderer();
}

knowGIS::GeometryObject GeometryObject::geometryObject() const { return m_object; }

#include "moc_GeometryObject.cpp"
