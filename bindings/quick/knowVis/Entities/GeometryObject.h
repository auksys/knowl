#pragma once

#include <knowGIS/GeometryObject.h>
#include <knowVis/Entity.h>

namespace knowVis::Renderers
{
  class PolylineRenderer;
}

namespace knowVis::Materials
{
  class GeometryObjectMaterial;
}

namespace Qt3DExtras
{
  class QPhongMaterial;
}

namespace knowVis::Entities
{

  class GeometryObject : public knowVis::Entity
  {
    Q_OBJECT
    Q_PROPERTY(knowGIS::GeometryObject geometryObject READ geometryObject WRITE setGeometryObject
                 NOTIFY geometryObjectChanged)
    Q_PROPERTY(knowVis::Materials::GeometryObjectMaterial* material READ material)
  public:
    GeometryObject(Qt3DCore::QNode* parent = nullptr);
    ~GeometryObject();
  public:
    void setGeometryObject(const knowGIS::GeometryObject& _cw);
    knowGIS::GeometryObject geometryObject() const;
    knowVis::Materials::GeometryObjectMaterial* material() const { return m_material; }
  signals:
    void geometryObjectChanged();
  private:
    void updateRenderer();
  private:
    knowGIS::GeometryObject m_object;
    knowVis::Renderers::PolylineRenderer* m_polylineRenderer;
    knowVis::Materials::GeometryObjectMaterial* m_material;
  };
} // namespace knowVis::Entities
