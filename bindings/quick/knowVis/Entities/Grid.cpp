#include "Grid.h"

#include <Qt3DExtras/QPhongMaterial>

#include "../Renderers/GridRenderer.h"

using namespace knowVis::Entities;

Grid::Grid(Qt3DCore::QNode* _parent) : knowVis::Entity(_parent)
{
  m_gridRenderer = new Renderers::GridRenderer(this);
  addComponent(m_gridRenderer);
  m_phongMaterial = new Qt3DExtras::QPhongMaterial(this);
  m_phongMaterial->setAmbient(Qt::white);
  addComponent(m_phongMaterial);
  setupTransform();
}

Grid::~Grid() {}

void Grid::setCellWidth(qreal _cw)
{
  m_gridRenderer->setCellWidth(_cw);
  emit(cellWidthChanged());
}

qreal Grid::cellWidth() const { return m_gridRenderer->cellWidth(); }
void Grid::setCellHeight(qreal _ch)
{
  m_gridRenderer->setCellHeight(_ch);
  emit(cellHeightChanged());
}

qreal Grid::cellHeight() const { return m_gridRenderer->cellHeight(); }

void Grid::setColumns(int _columns)
{
  return m_gridRenderer->setColumns(_columns);
  emit(columnsChanged());
}

int Grid::columns() const { return m_gridRenderer->columns(); }

void Grid::setRows(int _rows)
{
  return m_gridRenderer->setRows(_rows);
  emit(rowsChanged());
}

int Grid::rows() const { return m_gridRenderer->rows(); }

#include "moc_Grid.cpp"
