#pragma once

#include <knowVis/Entity.h>

namespace knowVis::Renderers
{
  class GridRenderer;
}

namespace Qt3DExtras
{
  class QPhongMaterial;
}

namespace knowVis::Entities
{

  class Grid : public knowVis::Entity
  {
    Q_OBJECT
    Q_PROPERTY(qreal cellWidth READ cellWidth WRITE setCellWidth NOTIFY cellWidthChanged)
    Q_PROPERTY(qreal cellHeight READ cellHeight WRITE setCellHeight NOTIFY cellHeightChanged)
    Q_PROPERTY(int columns READ columns WRITE setColumns NOTIFY columnsChanged)
    Q_PROPERTY(int rows READ rows WRITE setRows NOTIFY rowsChanged)
  public:
    Grid(Qt3DCore::QNode* parent = nullptr);
    ~Grid();
  public:
    void setCellWidth(qreal _cw);
    qreal cellWidth() const;
    void setCellHeight(qreal _ch);
    qreal cellHeight() const;
    void setColumns(int _columns);
    int columns() const;
    void setRows(int _rows);
    int rows() const;
  signals:
    void cellWidthChanged();
    void cellHeightChanged();
    void columnsChanged();
    void rowsChanged();
  private:
    knowVis::Renderers::GridRenderer* m_gridRenderer;
    Qt3DExtras::QPhongMaterial* m_phongMaterial;
  };
} // namespace knowVis::Entities
