#include "LidarScan.h"

#include <knowVis/GeoTransform.h>
#include <knowVis/PointCloudGeometry.h>
#include <knowVis/PointCloudMaterial.h>
#include <knowVis/PointCloudRenderer.h>
#include <knowVis/Pose.h>

using namespace knowVis::Entities;

LidarScan::LidarScan(Qt3DCore::QNode* _parent) : knowVis::Entity(_parent)
{
  m_pointCloudGeometry = new knowVis::PointCloudRenderer(this);
  addComponent(m_pointCloudGeometry);
  m_pointCloudMaterial = new knowVis::PointCloudMaterial(this);
  addComponent(m_pointCloudMaterial);
  setupTransform();
}

LidarScan::~LidarScan() {}

knowValues::Values::LidarScan LidarScan::frame() const { return m_frame; }

void LidarScan::setFrame(const knowValues::Values::LidarScan& _frame)
{
  m_frame = _frame;
  emit(frameChanged());

  knowVis::PointCloud pc;
  pc.resize(_frame->ranges().size());
  for(int i = 0; i < _frame->ranges().size(); ++i)
  {
    qreal angle = _frame->angleMin() + i * _frame->angleIncrement();
    qreal cos_a = std::cos(angle);
    qreal sin_a = std::sin(angle);
    float range = _frame->ranges()[i];
    pc.setValue(i, cos_a * range, sin_a * range, 0.0);
  }
  transform()->pose()->setPose(_frame->pose());
  m_pointCloudGeometry->setPointCloud(pc);
}

#include "moc_LidarScan.cpp"
