#pragma once

#include <knowValues/Values.h>

#include <knowVis/Entity.h>

namespace knowVis::Entities
{
  class LidarScan : public knowVis::Entity
  {
    Q_OBJECT
    Q_PROPERTY(knowValues::Values::LidarScan frame READ frame WRITE setFrame NOTIFY frameChanged);
    Q_PROPERTY(knowVis::PointCloudMaterial* material READ material)
  public:
    LidarScan(Qt3DCore::QNode* _parent = nullptr);
    ~LidarScan();
    knowValues::Values::LidarScan frame() const;
    void setFrame(const knowValues::Values::LidarScan& _frame);
    knowVis::PointCloudMaterial* material() const { return m_pointCloudMaterial; }
  signals:
    void frameChanged();
  private:
    knowVis::PointCloudRenderer* m_pointCloudGeometry;
    knowVis::PointCloudMaterial* m_pointCloudMaterial;
    knowValues::Values::LidarScan m_frame;
    knowVis::GeoTransform* m_transform;
  };
} // namespace knowVis::Entities
