#include "OrthoImage.h"

#include <Qt3DExtras/QPlaneMesh>
#include <Qt3DExtras/QTextureMaterial>
// #include <Qt3DExtras/QPhongMaterial>
#include <Qt3DRender/QAbstractTextureImage>
#include <Qt3DRender/QTexture>

#include <knowCore/toQImage.h>
#include <knowGIS/Pose.h>
#include <knowGIS/Raster.h>

#include <knowVis/GeoTransform.h>
#include <knowVis/Pose.h>
#include <knowVis/Resources.h>

using namespace knowVis::Entities;

class knowVis::Entities::OrthoImageTexture : public Qt3DRender::QAbstractTextureImage
{
public:
  OrthoImageTexture(Qt3DCore::QNode* parent = nullptr) : Qt3DRender::QAbstractTextureImage(parent)
  {
  }
public:
  void setRaster(const knowGIS::Raster& _url)
  {
    m_raster = _url;
    notifyDataGeneratorChanged();
  }
private:
  struct TextureImageDataGenerator : public Qt3DRender::QTextureImageDataGenerator
  {
    Qt3DRender::QTextureImageDataPtr operator()() override
    {
      if(raster.isValid())
      {
        auto imgData = Qt3DRender::QTextureImageDataPtr::create();
        knowCore::Image img = raster.toImage();
        imgData->setImage(knowCore::toQImage(img));
        return imgData;
      }
      else
      {
        return nullptr;
      }
    }
    bool operator==(const QTextureImageDataGenerator& _rhs) const override
    {
      const TextureImageDataGenerator* otherfunctor
        = functor_cast<TextureImageDataGenerator>(&_rhs);
      return raster == otherfunctor->raster;
    }
    QT3D_FUNCTOR(TextureImageDataGenerator)
    knowGIS::Raster raster;
  };
protected:
  Qt3DRender::QTextureImageDataGeneratorPtr dataGenerator() const
  {
    TextureImageDataGenerator* tidg = new TextureImageDataGenerator;
    tidg->raster = m_raster;
    return Qt3DRender::QTextureImageDataGeneratorPtr(tidg);
  }
private:
  knowGIS::Raster m_raster;
};

OrthoImage::OrthoImage(Qt3DCore::QNode* _node) : knowVis::Entity(_node)
{
  m_planeMesh = new Qt3DExtras::QPlaneMesh(this);
  m_planeMesh->setWidth(2);
  m_planeMesh->setHeight(2);
  m_texture = new OrthoImageTexture(this);
  m_texture2d = new Qt3DRender::QTexture2D(this);
  m_texture2d->addTextureImage(m_texture);
  m_planeMaterial = new Qt3DExtras::QTextureMaterial(this);
  m_planeMaterial->setTexture(m_texture2d);

  // auto p = new Qt3DExtras::QPhongMaterial;
  // p->setAmbient(Qt::red);

  addComponent(m_planeMesh);
  // addComponent(p);
  addComponent(m_planeMaterial);
  setupTransform();
}

OrthoImage::~OrthoImage() {}

void OrthoImage::setSource(const QUrl& _url)
{
  m_source = _url;
  knowGIS::Raster raster = knowGIS::Raster::load(m_source.toLocalFile());

  if(raster.isValid())
  {
    m_planeMesh->setWidth(raster.horizontalSize());
    m_planeMesh->setHeight(raster.verticalSize());
    m_texture->setRaster(raster);
    knowGIS::Point cp = raster.center();
    knowGIS::Pose pose(knowGIS::Point(cp.x(), cp.y(), 0.0, cp.coordinateSystem()),
                       knowGIS::Quaternion(0.7071067811865475, 0, 0, 0.7071067811865475));
    transform()->pose()->setPose(pose);
  }
  else
  {
    clog_warning("Failed to load raster: {}", _url);
  }
  emit(sourceChanged());
}

QUrl OrthoImage::source() const { return m_source; }

void OrthoImage::setAltitude(double alt)
{
  m_altitude = alt;
  if(transform()->pose()->isValid())
  {
    knowGIS::Pose po = transform()->pose()->pose();
    transform()->pose()->setPose(
      knowGIS::Pose(knowGIS::Point(po.position().x(), po.position().y(), m_altitude,
                                   po.position().coordinateSystem()),
                    knowGIS::Quaternion(0.7071067811865475, 0, 0, 0.7071067811865475)));
  }
  emit(altitudeChanged());
}
double OrthoImage::altitude() const { return m_altitude; }
