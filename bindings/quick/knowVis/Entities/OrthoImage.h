#pragma once

#include <knowVis/Entity.h>

#include <QUrl>

namespace Qt3DRender
{
  class QTexture2D;
}

namespace Qt3DExtras
{
  class QPlaneMesh;
  class QTextureMaterial;
} // namespace Qt3DExtras

namespace knowVis::Entities
{
  class OrthoImageTexture;
  class OrthoImage : public knowVis::Entity
  {
    Q_OBJECT
    Q_PROPERTY(QUrl source WRITE setSource READ source NOTIFY sourceChanged);
    Q_PROPERTY(double altitude WRITE setAltitude READ altitude NOTIFY altitudeChanged);
  public:
    OrthoImage(Qt3DCore::QNode* _parent = nullptr);
    ~OrthoImage();
    void setSource(const QUrl& _url);
    QUrl source() const;
    void setAltitude(double alt);
    double altitude() const;
  signals:
    void sourceChanged();
    void altitudeChanged();
  private:
    Qt3DExtras::QPlaneMesh* m_planeMesh;
    Qt3DRender::QTexture2D* m_texture2d;
    Qt3DExtras::QTextureMaterial* m_planeMaterial;
    OrthoImageTexture* m_texture;
    QUrl m_source;
    double m_altitude;
  };
} // namespace knowVis::Entities
