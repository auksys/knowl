#include "PointCloud.h"

#include <knowVis/GeoTransform.h>
#include <knowVis/PointCloudGeometry.h>
#include <knowVis/PointCloudMaterial.h>
#include <knowVis/PointCloudRenderer.h>
#include <knowVis/Pose.h>

using namespace knowVis::Entities;

PointCloud::PointCloud(Qt3DCore::QNode* _parent) : knowVis::Entity(_parent)
{
  m_pointCloudRenderer = new knowVis::PointCloudRenderer(this);
  addComponent(m_pointCloudRenderer);
  m_pointCloudMaterial = new knowVis::PointCloudMaterial(this);
  addComponent(m_pointCloudMaterial);
  setupTransform();
}

PointCloud::~PointCloud() {}

knowValues::Values::PointCloud PointCloud::pointCloud() const { return m_frame; }

namespace
{
  using reader_function_pcf
    = std::function<double(const char*, const knowValues::Values::Definitions::PointCloudField*)>;
  using reader_function = std::function<double(const char*)>;
  template<typename _T_>
  double pc_reader(const char* _data,
                   const knowValues::Values::Definitions::PointCloudField* _field)
  {
    return *reinterpret_cast<const _T_*>(_data + _field->offset()) * _field->scale();
  }
  static QMap<knowValues::Values::Definitions::PointCloudField::DataType, reader_function_pcf>
    reader_functions
    = {{knowValues::Values::Definitions::PointCloudField::DataType::UnsignedInteger8,
        pc_reader<quint8>},
       {knowValues::Values::Definitions::PointCloudField::DataType::Integer8, pc_reader<qint8>},
       {knowValues::Values::Definitions::PointCloudField::DataType::UnsignedInteger16,
        pc_reader<quint16>},
       {knowValues::Values::Definitions::PointCloudField::DataType::Integer16, pc_reader<qint16>},
       {knowValues::Values::Definitions::PointCloudField::DataType::UnsignedInteger32,
        pc_reader<quint32>},
       {knowValues::Values::Definitions::PointCloudField::DataType::Integer32, pc_reader<quint32>},
       {knowValues::Values::Definitions::PointCloudField::DataType::UnsignedInteger64,
        pc_reader<quint64>},
       {knowValues::Values::Definitions::PointCloudField::DataType::Integer64, pc_reader<qint64>},
       {knowValues::Values::Definitions::PointCloudField::DataType::Float32, pc_reader<float>},
       {knowValues::Values::Definitions::PointCloudField::DataType::Float64, pc_reader<double>}};
  reader_function make_reader(const knowValues::Values::Definitions::PointCloudField* _pcf)
  {
    reader_function_pcf func = reader_functions.value(_pcf->dataType());
    return [_pcf, func](const char* _data) { return func(_data, _pcf); };
  }
  reader_function make_reader(const QList<knowValues::Values::Definitions::PointCloudField*>& _pcf,
                              const char* _fieldname)
  {
    for(const knowValues::Values::Definitions::PointCloudField* pcf : _pcf)
    {
      if(pcf->name() == _fieldname)
      {
        return make_reader(pcf);
      }
    }
    clog_fatal("Missing point cloud field: {}", _fieldname);
  }
} // namespace

void PointCloud::setPointCloud(const knowValues::Values::PointCloud& _frame)
{
  m_frame = _frame;
  emit(pointCloudChanged());

  if(_frame)
  {
    knowVis::PointCloud pc;
    pc.resize(_frame->pointsCount());

    std::size_t pointSize = 0;
    for(const knowValues::Values::Definitions::PointCloudField* pcf : _frame->fields())
    {
      pointSize += pcf->size();
    }

    reader_function x_reader = make_reader(_frame->fields(), "x");
    reader_function y_reader = make_reader(_frame->fields(), "y");
    reader_function z_reader = make_reader(_frame->fields(), "z");
    const char* data_origin = _frame->data().constData();
    for(int i = 0; i < _frame->pointsCount(); ++i)
    {
      const char* data = data_origin + i * pointSize;
      pc.setValue(i, x_reader(data), y_reader(data), z_reader(data));
    }
    transform()->pose()->setPose(_frame->transformation());
    m_pointCloudRenderer->setPointCloud(pc);
  }
  else
  {
    m_pointCloudRenderer->setPointCloud(knowVis::PointCloud());
  }
}

Qt3DCore::QGeometry* PointCloud::geometry() const { return m_pointCloudRenderer->geometry(); }

#include "moc_PointCloud.cpp"
