#pragma once

#include <knowValues/Values.h>
#include <knowVis/Entity.h>

namespace Qt3DCore
{
  class QGeometry;
}

namespace knowVis::Entities
{
  class PointCloud : public knowVis::Entity
  {
    Q_OBJECT
    Q_PROPERTY(knowValues::Values::PointCloud pointCloud READ pointCloud WRITE setPointCloud NOTIFY
                 pointCloudChanged);
    Q_PROPERTY(knowVis::PointCloudMaterial* material READ material)
    Q_PROPERTY(Qt3DCore::QGeometry* geometry READ geometry CONSTANT)
  public:
    PointCloud(Qt3DCore::QNode* _parent = nullptr);
    ~PointCloud();
    knowValues::Values::PointCloud pointCloud() const;
    void setPointCloud(const knowValues::Values::PointCloud& _frame);
    knowVis::PointCloudMaterial* material() const { return m_pointCloudMaterial; }
    Qt3DCore::QGeometry* geometry() const;
  signals:
    void pointCloudChanged();
  private:
    knowVis::PointCloudRenderer* m_pointCloudRenderer;
    knowVis::PointCloudMaterial* m_pointCloudMaterial;
    knowValues::Values::PointCloud m_frame;
    knowVis::GeoTransform* m_transform;
  };
} // namespace knowVis::Entities
