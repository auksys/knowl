#include "GeometryObjectMaterial.h"

#include <Qt3DRender/QEffect>
#include <Qt3DRender/QLineWidth>
#include <Qt3DRender/QTechnique>

using namespace knowVis::Materials;

GeometryObjectMaterial::GeometryObjectMaterial(Qt3DCore::QNode* parent)
    : Qt3DExtras::QPhongMaterial(parent)
{
  setAmbient(Qt::red);
  m_lineWidth = new Qt3DRender::QLineWidth();
  m_lineWidth->setValue(5);
  effect()->techniques().first()->renderPasses().first()->addRenderState(m_lineWidth);
}

GeometryObjectMaterial::~GeometryObjectMaterial() {}

QColor GeometryObjectMaterial::color() const { return ambient(); }

void GeometryObjectMaterial::setColor(const QColor& _color)
{
  setAmbient(_color);
  emit(colorChanged());
}

qreal GeometryObjectMaterial::lineWidth() const { return m_lineWidth->value(); }

void GeometryObjectMaterial::setLineWidth(qreal _pointSize)
{
  m_lineWidth->setValue(_pointSize);
  emit(lineWidthChanged());
}

#include "moc_GeometryObjectMaterial.cpp"
