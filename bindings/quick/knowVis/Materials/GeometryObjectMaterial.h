#pragma once

#include <Qt3DExtras/QPhongMaterial>

namespace Qt3DRender
{
  class QLineWidth;
}

namespace knowVis::Materials
{
  class GeometryObjectMaterial : public Qt3DExtras::QPhongMaterial
  {
    Q_OBJECT
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(qreal lineWidth READ lineWidth WRITE setLineWidth NOTIFY lineWidthChanged)
  public:
    GeometryObjectMaterial(Qt3DCore::QNode* parent = nullptr);
    ~GeometryObjectMaterial();
  public:
    QColor color() const;
    void setColor(const QColor& _color);
    qreal lineWidth() const;
    void setLineWidth(qreal _pointSize);
  signals:
    void colorChanged();
    void lineWidthChanged();
  private:
    Qt3DRender::QLineWidth* m_lineWidth;
  };
} // namespace knowVis::Materials

Q_DECLARE_METATYPE(knowVis::Materials::GeometryObjectMaterial*)
