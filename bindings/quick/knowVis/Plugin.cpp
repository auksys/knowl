#include "Plugin.h"

#include <QtQml>

#include <knowVis/Context.h>
#include <knowVis/GeoTransform.h>
#include <knowVis/PointCloudMaterial.h>
#include <knowVis/Pose.h>

#include "Entities/Axes.h"
#include "Entities/GeometryObject.h"
#include "Entities/Grid.h"
#include "Entities/LidarScan.h"
#include "Entities/OrthoImage.h"
#include "Entities/PointCloud.h"
#include "Materials/GeometryObjectMaterial.h"

#include "TestSingleton.h"

#if(QT_VERSION < QT_VERSION_CHECK(5, 14, 0))
template<typename _T_>
void qmlRegisterAnonymousType(const char*, int)
{
  qmlRegisterType<_T_>();
}
#endif

using namespace knowVisQuick;

void Plugin::registerTypes(const char* /*uri*/)
{

  qRegisterMetaType<knowVis::PointCloudMaterial*>("knowVis::PointCloudMaterial*");

  const char* uri_knowVis = "knowVis";
  qmlRegisterAnonymousType<knowVis::Pose>(uri_knowVis, 1);
  qmlRegisterUncreatableType<knowVis::PointCloudMaterial>(uri_knowVis, 1, 0, "PointCloudMaterial",
                                                          "Just for enums");
  qmlRegisterUncreatableType<knowVis::Materials::GeometryObjectMaterial>(
    uri_knowVis, 1, 0, "GeometryObjectMaterial", "Just for value type property");
  qmlRegisterType<knowVis::Context>(uri_knowVis, 1, 0, "Context");
  qmlRegisterType<knowVis::GeoTransform>(uri_knowVis, 1, 0, "GeoTransform");
  qmlRegisterSingletonType<TestSingleton>(uri_knowVis, 1, 0, "Test",
                                          [](QQmlEngine*, QJSEngine*) -> QObject*
                                          { return new TestSingleton; });

  const char* uri_knowVis_Entities = "knowVis.Entities";
  qmlRegisterType<knowVis::Entities::Axes>(uri_knowVis_Entities, 1, 0, "Axes");
  qmlRegisterType<knowVis::Entities::GeometryObject>(uri_knowVis_Entities, 1, 0, "GeometryObject");
  qmlRegisterType<knowVis::Entities::Grid>(uri_knowVis_Entities, 1, 0, "Grid");
  qmlRegisterType<knowVis::Entities::LidarScan>(uri_knowVis_Entities, 1, 0, "LidarScan");
  qmlRegisterType<knowVis::Entities::OrthoImage>(uri_knowVis_Entities, 1, 0, "OrthoImage");
  qmlRegisterType<knowVis::Entities::PointCloud>(uri_knowVis_Entities, 1, 0, "PointCloud");
}

#include "moc_Plugin.cpp"
