#pragma once

#include <QQmlExtensionPlugin>

namespace knowVisQuick
{
  class Plugin : public QQmlExtensionPlugin
  {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "knowVis/3.0")
  public:
    void registerTypes(const char* uri) override;
  };
} // namespace knowVisQuick
