#include "GridRenderer.h"

#include <QPointF>

#include <Qt3DCore/QAttribute>
#include <Qt3DCore/QBuffer>

#include <clog_qt>

using namespace knowVis::Renderers;

class GridGeometry : public Qt3DCore::QGeometry
{
public:
  explicit GridGeometry(QNode* parent = nullptr);
  ~GridGeometry();
  void setCellWidth(qreal _cw)
  {
    m_cellWidth = _cw;
    updateVertices();
  }
  qreal cellWidth() const { return m_cellWidth; }
  void setCellHeight(qreal _ch)
  {
    m_cellHeight = _ch;
    updateVertices();
  }
  qreal cellHeight() const { return m_cellHeight; }
  void setColumns(int _columns)
  {
    m_columns = _columns;
    updateVertices();
    updateIndices();
  }
  int columns() const { return m_columns; }
  void setRows(int _rows)
  {
    m_rows = _rows;
    updateVertices();
    updateIndices();
  }
  int rows() const { return m_rows; }
private:
  void updateVertices();
  void updateIndices();
private:
  qreal m_cellWidth = 10.0;
  qreal m_cellHeight = 10.0;
  int m_columns = 10;
  int m_rows = 10;

  Qt3DCore::QAttribute* m_positionAttribute;
  Qt3DCore::QAttribute* m_indexAttribute;
  Qt3DCore::QBuffer* m_vertexBuffer;
  Qt3DCore::QBuffer* m_indexBuffer;
};

GridGeometry::GridGeometry(QNode* parent) : QGeometry(parent)
{
  m_positionAttribute = new Qt3DCore::QAttribute(this);
  m_indexAttribute = new Qt3DCore::QAttribute(this);
  m_vertexBuffer = new Qt3DCore::QBuffer(this);
  m_indexBuffer = new Qt3DCore::QBuffer(this);

  m_positionAttribute->setName(Qt3DCore::QAttribute::defaultPositionAttributeName());
  //   m_positionAttribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
  //   m_positionAttribute->setVertexSize(3);
  m_positionAttribute->setVertexBaseType(Qt3DCore::QAttribute::Float);
  m_positionAttribute->setVertexSize(3);
  m_positionAttribute->setAttributeType(Qt3DCore::QAttribute::VertexAttribute);
  m_positionAttribute->setBuffer(m_vertexBuffer);
  m_positionAttribute->setByteStride(3 * sizeof(float));

  m_indexAttribute->setAttributeType(Qt3DCore::QAttribute::IndexAttribute);
  m_indexAttribute->setVertexBaseType(Qt3DCore::QAttribute::UnsignedShort);
  m_indexAttribute->setBuffer(m_indexBuffer);

  addAttribute(m_positionAttribute);
  addAttribute(m_indexAttribute);

  updateVertices();
  updateIndices();
}

GridGeometry::~GridGeometry() {}

void GridGeometry::updateVertices()
{
  const int verticesCount = 2 * ((m_columns + 1) + (m_rows + 1));
  m_positionAttribute->setCount(verticesCount);

  // vec3 pos, vec2 texCoord, vec3 normal
  const quint32 vertexSize = (3 * 2) * sizeof(float);

  QByteArray verticesData;
  verticesData.resize(vertexSize * verticesCount);
  float* verticesPtr = reinterpret_cast<float*>(verticesData.data());

  QPointF topLeft{-0.5 * m_columns * m_cellWidth, -0.5 * m_rows * m_cellHeight};
  QPointF bottomRight{0.5 * m_columns * m_cellWidth, 0.5 * m_rows * m_cellHeight};

  // Vertices coordinates
  for(int j = 0; j <= m_rows; ++j)
  {
    *verticesPtr++ = topLeft.x();
    *verticesPtr++ = topLeft.y() + j * m_cellHeight;
    *verticesPtr++ = 0;
    *verticesPtr++ = bottomRight.x();
    *verticesPtr++ = topLeft.y() + j * m_cellHeight;
    *verticesPtr++ = 0;
  }
  for(int i = 0; i <= m_columns; ++i)
  {
    *verticesPtr++ = topLeft.x() + i * m_cellWidth;
    *verticesPtr++ = topLeft.y();
    *verticesPtr++ = 0;
    *verticesPtr++ = topLeft.x() + i * m_cellWidth;
    *verticesPtr++ = bottomRight.y();
    *verticesPtr++ = 0;
  }

  m_vertexBuffer->setData(verticesData);
}

void GridGeometry::updateIndices()
{
  const int linesCount = 2 * (m_columns + 1 + m_rows + 1);
  const int indicesCount = linesCount * 2;
  m_indexAttribute->setCount(indicesCount);

  const int indexSize = sizeof(quint16);
  clog_assert(indicesCount < 65536);

  QByteArray indicesBytes;
  indicesBytes.resize(indicesCount * indexSize);
  quint16* indicesPtr = reinterpret_cast<quint16*>(indicesBytes.data());

  for(int i = 0; i <= linesCount; ++i)
  {
    *indicesPtr++ = 2 * i;
    *indicesPtr++ = 2 * i + 1;
  }

  m_indexBuffer->setData(indicesBytes);
}

struct GridRenderer::Private
{
  GridGeometry* geometry;
};

GridRenderer::GridRenderer(Qt3DCore::QNode* parent)
    : Qt3DRender::QGeometryRenderer(parent), d(new Private)
{
  d->geometry = new GridGeometry(this);
  setGeometry(d->geometry);
  setPrimitiveType(Qt3DRender::QGeometryRenderer::Lines);
}

GridRenderer::~GridRenderer() { delete d; }

void GridRenderer::setCellWidth(qreal _cw) { d->geometry->setCellWidth(_cw); }

qreal GridRenderer::cellWidth() const { return d->geometry->cellWidth(); }

void GridRenderer::setCellHeight(qreal _ch) { d->geometry->setCellHeight(_ch); }

qreal GridRenderer::cellHeight() const { return d->geometry->cellHeight(); }

void GridRenderer::setColumns(int _columns) { d->geometry->setColumns(_columns); }

int GridRenderer::columns() const { return d->geometry->columns(); }

void GridRenderer::setRows(int _rows) { d->geometry->setRows(_rows); }

int GridRenderer::rows() const { return d->geometry->rows(); }
