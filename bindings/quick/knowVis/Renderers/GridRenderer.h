#pragma once

#include <Qt3DRender/QGeometryRenderer>

#include <knowVis/Forward.h>

namespace knowVis::Renderers
{

  class GridRenderer : public Qt3DRender::QGeometryRenderer
  {
  public:
    GridRenderer(Qt3DCore::QNode* parent = nullptr);
    ~GridRenderer();
  public:
    void setCellWidth(qreal _cw);
    qreal cellWidth() const;
    void setCellHeight(qreal _ch);
    qreal cellHeight() const;
    void setColumns(int _columns);
    int columns() const;
    void setRows(int _rows);
    int rows() const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowVis::Renderers
