#include "PolylineRenderer.h"

#include <QVector3D>

#include <Qt3DCore/QAttribute>
#include <Qt3DCore/QBuffer>

#include <clog_qt>

using namespace knowVis::Renderers;

class PolylineGeometry : public Qt3DCore::QGeometry
{
public:
  explicit PolylineGeometry(QNode* parent = nullptr);
  ~PolylineGeometry();
  void setPolyline(const QList<QVector3D>& _ring)
  {
    m_polyline = _ring;
    updateVertices();
    updateIndices();
  }
  QList<QVector3D> polyline() const { return m_polyline; }
private:
  void updateVertices();
  void updateIndices();
private:
  QList<QVector3D> m_polyline;

  Qt3DCore::QAttribute* m_positionAttribute;
  Qt3DCore::QAttribute* m_indexAttribute;
  Qt3DCore::QBuffer* m_vertexBuffer;
  Qt3DCore::QBuffer* m_indexBuffer;
};

PolylineGeometry::PolylineGeometry(QNode* parent) : QGeometry(parent)
{
  m_positionAttribute = new Qt3DCore::QAttribute(this);
  m_indexAttribute = new Qt3DCore::QAttribute(this);
  m_vertexBuffer = new Qt3DCore::QBuffer(this);
  m_indexBuffer = new Qt3DCore::QBuffer(this);

  m_positionAttribute->setName(Qt3DCore::QAttribute::defaultPositionAttributeName());
  //   m_positionAttribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
  //   m_positionAttribute->setVertexSize(3);
  m_positionAttribute->setVertexBaseType(Qt3DCore::QAttribute::Float);
  m_positionAttribute->setVertexSize(3);
  m_positionAttribute->setAttributeType(Qt3DCore::QAttribute::VertexAttribute);
  m_positionAttribute->setBuffer(m_vertexBuffer);
  m_positionAttribute->setByteStride(3 * sizeof(float));

  m_indexAttribute->setAttributeType(Qt3DCore::QAttribute::IndexAttribute);
  m_indexAttribute->setVertexBaseType(Qt3DCore::QAttribute::UnsignedShort);
  m_indexAttribute->setBuffer(m_indexBuffer);

  addAttribute(m_positionAttribute);
  addAttribute(m_indexAttribute);

  updateVertices();
  updateIndices();
}

PolylineGeometry::~PolylineGeometry() {}

void PolylineGeometry::updateVertices()
{
  const int verticesCount = m_polyline.size();
  m_positionAttribute->setCount(verticesCount);

  // vec3 pos, vec2 texCoord, vec3 normal
  const quint32 vertexSize = 3 * sizeof(float);

  QByteArray verticesData;
  verticesData.resize(vertexSize * verticesCount);
  float* verticesPtr = reinterpret_cast<float*>(verticesData.data());

  // Vertices coordinates
  for(const QVector3D& v : m_polyline)
  {
    *verticesPtr++ = v.x();
    *verticesPtr++ = v.y();
    *verticesPtr++ = v.z();
  }

  m_vertexBuffer->setData(verticesData);
}

void PolylineGeometry::updateIndices()
{
  const int indicesCount = m_polyline.size();
  m_indexAttribute->setCount(indicesCount);

  const int indexSize = sizeof(quint16);
  clog_assert(indicesCount < 65536);

  QByteArray indicesBytes;
  indicesBytes.resize(indicesCount * indexSize);
  quint16* indicesPtr = reinterpret_cast<quint16*>(indicesBytes.data());

  for(int i = 0; i <= indicesCount; ++i)
  {
    *indicesPtr++ = i;
  }

  m_indexBuffer->setData(indicesBytes);
}

struct PolylineRenderer::Private
{
  PolylineGeometry* polyline;
};

PolylineRenderer::PolylineRenderer(Qt3DCore::QNode* parent)
    : Qt3DRender::QGeometryRenderer(parent), d(new Private)
{
  d->polyline = new PolylineGeometry(this);
  setGeometry(d->polyline);
  setPrimitiveType(Qt3DRender::QGeometryRenderer::LineStrip);
}

PolylineRenderer::~PolylineRenderer() { delete d; }

void PolylineRenderer::setPolyline(const QList<QVector3D>& _ring)
{
  d->polyline->setPolyline(_ring);
}

QList<QVector3D> PolylineRenderer::polyline() const { return d->polyline->polyline(); }
