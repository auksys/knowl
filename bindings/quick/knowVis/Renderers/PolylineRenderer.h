#pragma once

#include <Cartography/EuclidSystem.h>
#include <Qt3DRender/QGeometryRenderer>

#include <knowVis/Forward.h>

namespace knowVis::Renderers
{

  class PolylineRenderer : public Qt3DRender::QGeometryRenderer
  {
  public:
    PolylineRenderer(Qt3DCore::QNode* parent = nullptr);
    ~PolylineRenderer();
  public:
    void setPolyline(const QList<QVector3D>& _ring);
    QList<QVector3D> polyline() const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowVis::Renderers
