#include "TestSingleton.h"

#include <random>

#include <knowValues/Values.h>

using namespace knowVisQuick;

TestSingleton::TestSingleton(QObject* _parent) : QObject(_parent) {}

TestSingleton::~TestSingleton() {}

knowValues::Values::LidarScan TestSingleton::createLidarScanAt(qreal _longitude, qreal _latitude,
                                                               qreal _altitude)
{
  int values = 180;
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1.0, 10.0);
  QVector<float> ranges;
  float r = dis(gen);
  for(int i = 0; i < values; ++i)
  {
    r = 0.9 * r + 0.1 * dis(gen);
    ranges.push_back(r > 5.0 ? NAN : r);
  }
  return knowValues::Values::LidarScan::create()
    .setPose(::knowGIS::GeoPose(knowGIS::GeoPoint(Cartography::Longitude(_longitude),
                                                  Cartography::Latitude(_latitude),
                                                  Cartography::Altitude(_altitude)),
                                knowGIS::Quaternion(0, 1, 0, 1)))
    .setAngleMin(-M_PI_2)
    .setAngleMax(M_PI_2)
    .setAngleIncrement(M_PI / (values + 1))
    .setRanges(ranges);
}

knowValues::Values::PointCloud TestSingleton::createPointCloudAt(qreal _longitude, qreal _latitude,
                                                                 qreal _altitude)
{
  int values = 180;
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-10.0, 10.0);
  QByteArray data;
  data.resize(3 * sizeof(float) * values);
  float* data_data = reinterpret_cast<float*>(data.data());
  for(int i = 0; i < values; ++i)
  {
    data_data[0 + i * 3] = dis(gen);
    data_data[1 + i * 3] = dis(gen);
    data_data[2 + i * 3] = dis(gen);
  }
  QList<knowValues::Values::Definitions::PointCloudField*> fields;
  fields.append(knowValues::Values::Definitions::PointCloudField::create()
                  .setName("x")
                  .setDataType(knowValues::Values::Definitions::PointCloudField::DataType::Float32)
                  .setCount(1)
                  .setScale(1)
                  .setOffset(0));
  fields.append(knowValues::Values::Definitions::PointCloudField::create()
                  .setName("y")
                  .setDataType(knowValues::Values::Definitions::PointCloudField::DataType::Float32)
                  .setCount(1)
                  .setScale(1)
                  .setOffset(4));
  fields.append(knowValues::Values::Definitions::PointCloudField::create()
                  .setName("z")
                  .setDataType(knowValues::Values::Definitions::PointCloudField::DataType::Float32)
                  .setCount(1)
                  .setScale(1)
                  .setOffset(8));

  return knowValues::Values::PointCloud::create()
    .setTransformation(::knowGIS::GeoPose(knowGIS::GeoPoint(Cartography::Longitude(_longitude),
                                                            Cartography::Latitude(_latitude),
                                                            Cartography::Altitude(_altitude)),
                                          knowGIS::Quaternion(0, 0, 0, 1)))
    .setPointsCount(values)
    .setData(data)
    .setFields(fields);
}

#include "moc_TestSingleton.cpp"
