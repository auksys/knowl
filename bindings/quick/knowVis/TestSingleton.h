#pragma once
#include <QObject>

#include <knowValues/Forward.h>

namespace knowVisQuick
{
  class TestSingleton : public QObject
  {
    Q_OBJECT
  public:
    TestSingleton(QObject* _parent = nullptr);
    ~TestSingleton();

    Q_INVOKABLE knowValues::Values::LidarScan createLidarScanAt(qreal _longitude, qreal _latitude,
                                                                qreal _altitude);
    Q_INVOKABLE knowValues::Values::PointCloud createPointCloudAt(qreal _longitude, qreal _latitude,
                                                                  qreal _altitude);
  };
} // namespace knowVisQuick
