import QtQuick
import QtQuick.Window
import QtLocation
import QtPositioning

import QtQuick.Scene2D
import Qt3D.Core
import Qt3D.Render
import Qt3D.Extras

import knowVis

Entity
{
  id: root

  property Context context: Context {}
  components: [ cubeMesh, cubeMaterial, cubeTransform ]

  property var __extent
  property alias zoomLevel: map.zoomLevel

  CuboidMesh
  {
    id: cubeMesh
    xExtent: root.__extent
    yExtent: root.__extent
    zExtent: 0.001
  }
  Transform {
    id: cubeTransform
    translation: Qt.vector3d(0, 0, -0.001)
  }

  TextureMaterial {
    id: cubeMaterial
    texture: offscreenTexture
  }


  Scene2D {
    id: mapTexture
            output: RenderTargetOutput {
                attachmentPoint: RenderTargetOutput.Color0
                texture: Texture2D {
                    id: offscreenTexture
                    width: 2048
                    height: 2048
                    format: Texture.RGBA8_UNorm
                    generateMipMaps: true
                    magnificationFilter: Texture.Linear
                    minificationFilter: Texture.LinearMipMapLinear
                    wrapMode {
                        x: WrapMode.ClampToEdge
                        y: WrapMode.ClampToEdge
                    }
                }
            }
    Item
    {
      Plugin {
        id: mapPlugin
        name: "osm"
      }

      Map {
        id: map
        width: offscreenTexture.width
        height: offscreenTexture.height
        plugin: mapPlugin
        center: QtPositioning.coordinate(root.context.origin.latitude, root.context.origin.longitude)
        zoomLevel: 18

        onCenterChanged: map.__update_extent()
        onZoomLevelChanged: map.__update_extent()
        onWidthChanged: map.__update_extent()
        onHeightChanged: map.__update_extent()

        function  __update_extent()
        {
          root.__extent = map.toCoordinate(Qt.point(0, 0)).distanceTo(map.toCoordinate(Qt.point(map.width - 1, 0)))
        }
      }
    }
  }
}
