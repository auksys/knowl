import Qt3D.Core
import Qt3D.Render
import Qt3D.Input

Entity
{
  id: root
  property Camera camera
  property real linearSpeed: 0.05
  property real zoomInLimit: 1.0
  property alias mouseDevice: __mouseHandler__.sourceDevice
  MouseHandler {
    id: __mouseHandler__
    property point _lastPt;   // Last position of the mouse

    onPressed: {
      if(mouse.buttons == MouseEvent.RightButton)
      {
        if(mouse.modifiers & MouseEvent.ShiftModifier)
        {
          root.camera.translate(Qt.vector3d(0, 0, 1))
        } else {
          root.camera.translate(Qt.vector3d(0, 0, -1))
        }
      }
      _lastPt = Qt.point(mouse.x, mouse.y);
    }
    function __zoom(amount)
    {
      if(amount > 0 && camera.viewVector.length() - amount < root.zoomInLimit)
      {
        amount = camera.viewVector.length() - root.zoomInLimit
      }
      root.camera.translate(Qt.vector3d(0, 0, amount), Camera.DontTranslateViewCenter)
    }
    onPositionChanged:
    {
      if(mouse.buttons == MouseEvent.LeftButton)
      {
        if(mouse.modifiers & MouseEvent.ShiftModifier)
        {
          root.camera.translate(Qt.vector3d(-root.linearSpeed*(mouse.x - _lastPt.x), root.linearSpeed*(mouse.y - _lastPt.y), 0))
        } else {
          root.camera.panAboutViewCenter(-(mouse.x - _lastPt.x));
          root.camera.tiltAboutViewCenter(mouse.y - _lastPt.y);
        }
      } else if(mouse.buttons == MouseEvent.RightButton)
      {
        __zoom(-root.linearSpeed*(mouse.y - _lastPt.y))
      }
      _lastPt = Qt.point(mouse.x, mouse.y);
    }
    onWheel:
    {
      __zoom(root.linearSpeed*wheel.angleDelta.y)
    }
  }
}
