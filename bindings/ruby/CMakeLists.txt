add_subdirectory(knowRuby)

add_library(know_core_bindings_ruby SHARED knowCore_init.cpp)
target_link_libraries(know_core_bindings_ruby PRIVATE rice knowCore knowRuby)
set_target_properties(know_core_bindings_ruby PROPERTIES PREFIX "")
install(TARGETS know_core_bindings_ruby DESTINATION ${INSTALL_LIB_DIR}/ruby)

if(BUILD_KNOWGIS)
add_library(know_gis_bindings_ruby SHARED knowGIS_init.cpp)
target_link_libraries(know_gis_bindings_ruby PRIVATE rice knowGIS knowRuby)
set_target_properties(know_gis_bindings_ruby PROPERTIES PREFIX "")
install(TARGETS know_gis_bindings_ruby DESTINATION ${INSTALL_LIB_DIR}/ruby)
endif()

add_library(know_rdf_bindings_ruby SHARED knowRDF_init.cpp)
target_link_libraries(know_rdf_bindings_ruby PRIVATE rice knowRDF knowRuby)
set_target_properties(know_rdf_bindings_ruby PROPERTIES PREFIX "")
install(TARGETS know_rdf_bindings_ruby DESTINATION ${INSTALL_LIB_DIR}/ruby)

add_library(know_dbc_bindings_ruby SHARED knowDBC_init.cpp)
target_link_libraries(know_dbc_bindings_ruby PRIVATE rice knowDBC knowRuby)
set_target_properties(know_dbc_bindings_ruby PROPERTIES PREFIX "")
install(TARGETS know_dbc_bindings_ruby DESTINATION ${INSTALL_LIB_DIR}/ruby)

if(BUILD_KNOWVALUES)
add_library(know_values_bindings_ruby SHARED knowValues_init.cpp)
target_link_libraries(know_values_bindings_ruby PRIVATE rice knowValues knowRuby)
set_target_properties(know_values_bindings_ruby PROPERTIES PREFIX "")
install(TARGETS know_values_bindings_ruby DESTINATION ${INSTALL_LIB_DIR}/ruby)
install(FILES knowValues.rb DESTINATION ${INSTALL_LIB_DIR}/ruby)
endif()


install(FILES knowCore.rb knowRDF.rb knowGIS.rb knowDBC.rb DESTINATION ${INSTALL_LIB_DIR}/ruby)
install(FILES
  knowCore.h
  qt.h
  DESTINATION ${INSTALL_INCLUDE_DIR}/knowCore/ruby)

if(RUN_RSPEC_TESTS)
  add_subdirectory(tests)
endif()
