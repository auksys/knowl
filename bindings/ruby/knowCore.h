#include <knowCore/BigNumber.h>
#include <knowCore/UriList.h>
#include <knowCore/ValueHash.h>

#include <knowRuby/ConverterManager.h>
#include <knowRuby/HandleResult.h>

#include "qt.h"

namespace Rice::detail
{

  // cres_qresult
  template<typename _T_>
  struct Type<cres_qresult<_T_>>
  {
    static bool verify() { return Type<_T_>::verify(); }
  };
  template<typename _T_>
  struct To_Ruby<cres_qresult<_T_>>
  {
    VALUE convert(const cres_qresult<_T_>& x)
    {
      return To_Ruby<_T_>().convert(knowRuby::handle(x));
    }
  };
  template<>
  struct Type<cres_qresult<void>>
  {
    static bool verify() { return true; }
  };
  template<>
  struct To_Ruby<cres_qresult<void>>
  {
    VALUE convert(const cres_qresult<void>& x)
    {
      if(x.is_successful())
      {
        return Qnil;
      }
      else
      {
        throw std::runtime_error(x.get_error().get_message().toStdString());
      }
    }
  };

  // knowCore::BigNumber
  template<>
  struct Type<knowCore::BigNumber>
  {
    static bool verify() { return true; }
  };

  template<>
  class To_Ruby<knowCore::BigNumber>
  {
  public:
    VALUE convert(knowCore::BigNumber const& x)
    {
      if(x.isFloating())
      {
        return To_Ruby<double>().convert(x.toDouble());
      }
      else
      {
        return rb_cstr_to_inum(qPrintable(x.toString()), 10, 1);
      }
    }
  };

  template<>
  class To_Ruby<knowCore::BigNumber&> : To_Ruby<knowCore::BigNumber>
  {
  };

  template<>
  class From_Ruby<knowCore::BigNumber>
  {
  public:
    From_Ruby() = default;

    explicit From_Ruby(Arg* arg) : arg_(arg) {}
    Rice::detail::Convertible is_convertible(VALUE value)
    {
      ruby_value_type vt = rb_type(value);
      return (vt == RUBY_T_FLOAT or vt == RUBY_T_FIXNUM or vt == RUBY_T_BIGNUM)
               ? Rice::detail::Convertible::Exact
               : Rice::detail::Convertible::None;
    }
    knowCore::BigNumber convert(VALUE value)
    {
      if(value == Qnil && this->arg_ && this->arg_->hasDefaultValue())
      {
        return this->arg_->defaultValue<knowCore::BigNumber>();
      }
      else
      {
        switch(rb_type(value))
        {
        case RUBY_T_FLOAT:
          return knowCore::BigNumber(protect(rb_num2dbl, value));
        case RUBY_T_FIXNUM:
          return knowCore::BigNumber(protect(rb_num2short_inline, value));
        case RUBY_T_BIGNUM:
        {
          auto const& [success, VAL, errorMessage]
            = knowCore::BigNumber::fromString(From_Ruby<QString>().convert(rb_big2str(value, 10)));
          if(success)
          {
            return VAL.value();
          }
          else
          {
            throw std::runtime_error(errorMessage.value().get_message().toStdString());
          }
        }
        default:
        {
          throw Exception(rb_eTypeError, "wrong argument type %s (expected % s)",
                          detail::protect(rb_obj_classname, value), "number");
        }
        }
      }
    }
  private:
    Arg* arg_ = nullptr;
  };

  template<>
  struct From_Ruby<knowCore::BigNumber*> : public rice_qt::From_Ruby_Ptr<knowCore::BigNumber>
  {
    using From_Ruby_Ptr_T::From_Ruby_Ptr_T;
  };

  template<>
  struct From_Ruby<knowCore::BigNumber&> : public rice_qt::From_Ruby_Ref<knowCore::BigNumber>
  {
    using From_Ruby_Ref_T::From_Ruby_Ref_T;
  };
  // knowCore::Value
  template<>
  struct Type<knowCore::Value>
  {
    static bool verify() { return true; }
  };

  VALUE knowCoreValueToVALUE(knowCore::Value const& _value)
  {
    auto const& [success, VAL, error] = knowRuby::ConverterManager::toRubyValue(_value);
    if(success)
    {
      return VAL.value();
    }
    else
    {
      throw std::runtime_error(error.value().get_message().toStdString());
    }
  }

  template<>
  class To_Ruby<knowCore::Value>
  {
  public:
    VALUE convert(knowCore::Value const& x) { return knowCoreValueToVALUE(x); }
  };

  template<>
  class To_Ruby<knowCore::Value&>
  {
  public:
    VALUE convert(knowCore::Value const& x) { return knowCoreValueToVALUE(x); }
  };

  template<>
  class From_Ruby<knowCore::Value>
  {
  public:
    From_Ruby() = default;

    explicit From_Ruby(Arg* arg) : arg_(arg) {}
    Rice::detail::Convertible is_convertible(VALUE) { return Rice::detail::Convertible::Exact; }

    knowCore::Value convert(VALUE value)
    {
      if(value == Qnil && this->arg_ && this->arg_->hasDefaultValue())
      {
        return this->arg_->defaultValue<knowCore::Value>();
      }
      else
      {
        auto const& [success, VAL, error] = knowRuby::ConverterManager::toKnowCoreValue(value);
        if(success)
        {
          return VAL.value();
        }
        else
        {
          throw std::runtime_error(error.value().get_message().toStdString());
        }
      }
    }
  private:
    Arg* arg_ = nullptr;
  };

  template<>
  struct From_Ruby<knowCore::Value*> : public rice_qt::From_Ruby_Ptr<knowCore::Value>
  {
    using From_Ruby_Ptr_T::From_Ruby_Ptr;
  };

  template<>
  struct From_Ruby<knowCore::Value&> : public rice_qt::From_Ruby_Ref<knowCore::Value>
  {
    using From_Ruby_Ref_T::From_Ruby_Ref;
  };
  // knowCore::ValueHash
  template<>
  struct Type<knowCore::ValueHash>
  {
    static bool verify() { return true; }
  };

  template<>
  class To_Ruby<knowCore::ValueHash> : public To_Ruby<QHash<QString, knowCore::Value>>
  {
  };
  // template<>
  // class To_Ruby<knowCore::ValueHash&> : public To_Ruby<QHash<QString, knowCore::Value>&>{};
  template<>
  class From_Ruby<knowCore::ValueHash> : public From_Ruby<QHash<QString, knowCore::Value>>
  {
  };
  template<>
  struct From_Ruby<knowCore::ValueHash*> : public rice_qt::From_Ruby_Ptr<knowCore::ValueHash>
  {
    using From_Ruby_Ptr_T::From_Ruby_Ptr;
  };

  template<>
  struct From_Ruby<knowCore::ValueHash&> : public rice_qt::From_Ruby_Ref<knowCore::ValueHash>
  {
    using From_Ruby_Ref_T::From_Ruby_Ref;
  };
  // knowCore::UriList
  template<>
  struct Type<knowCore::UriList>
  {
    static bool verify() { return true; }
  };

  template<>
  class To_Ruby<knowCore::UriList> : public To_Ruby<QList<knowCore::Uri>>
  {
  };
  // template<>
  // class To_Ruby<knowCore::UriList&> : public To_Ruby<QList<knowCore::Uri>>{};
  template<>
  class From_Ruby<knowCore::UriList> : public From_Ruby<QList<knowCore::Uri>>
  {
  };
  template<>
  struct From_Ruby<knowCore::UriList*> : public rice_qt::From_Ruby_Ptr<knowCore::UriList>
  {
    using From_Ruby_Ptr_T::From_Ruby_Ptr;
  };

  template<>
  struct From_Ruby<knowCore::UriList&> : public rice_qt::From_Ruby_Ref<knowCore::UriList>
  {
    using From_Ruby_Ref_T::From_Ruby_Ref;
  };

} // namespace Rice::detail
