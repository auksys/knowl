require 'know_core_bindings_ruby'

def kCu(str)
  return KnowCore::Uri.new str
end

module KnowCore
  class Uri
    def pretty_print(pp)
      self.to_s.pretty_print(pp)
    end
  end
end
