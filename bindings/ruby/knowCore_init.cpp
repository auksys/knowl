#include "knowCore.h"

#include "overload.h"

// Cleanup some ruby defines that clashes with fmt
#undef isfinite
#undef int128_t
#undef uint128_t

#include <knowCore/ConstrainedValue.h>
#include <knowCore/Timestamp.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/UriManager.h>
#include <knowCore/Value.h>

KNOWRUBY_REGISTER_VALUE_TO_VALUE_NOT_POINTER(QString)
KNOWRUBY_REGISTER_VALUE_TO_VALUE_NOT_POINTER(knowCore::BigNumber)

KNOWRUBY_REGISTER_VALUE_TO_VALUE(QString)
KNOWRUBY_REGISTER_VALUE_TO_VALUE(knowCore::BigNumber)
KNOWRUBY_REGISTER_VALUE_TO_VALUE(knowCore::Timestamp)
KNOWRUBY_REGISTER_VALUE_TO_VALUE(quint8)
KNOWRUBY_REGISTER_VALUE_TO_VALUE(quint16)
KNOWRUBY_REGISTER_VALUE_TO_VALUE(quint32)
KNOWRUBY_REGISTER_VALUE_TO_VALUE(quint64)
KNOWRUBY_REGISTER_VALUE_TO_VALUE(qint8)
KNOWRUBY_REGISTER_VALUE_TO_VALUE(qint16)
KNOWRUBY_REGISTER_VALUE_TO_VALUE(qint32)
KNOWRUBY_REGISTER_VALUE_TO_VALUE(qint64)

extern "C" void Init_know_core_bindings_ruby()
{
  namespace kC = knowCore;

  Rice::Module rb_mknowCore = Rice::define_module("KnowCore");

  Rice::Data_Type<knowCore::ConstrainedValue> rb_cConstrainedValue
    = Rice::define_class_under<knowCore::ConstrainedValue>(rb_mknowCore, "ConstrainedValue");

  Rice::define_enum_under<knowCore::ConstrainedValue::Type>("Type", rb_cConstrainedValue)
    .define_value("Equal", knowCore::ConstrainedValue::Type::Equal)
    .define_value("Different", knowCore::ConstrainedValue::Type::Different)
    .define_value("Inferior", knowCore::ConstrainedValue::Type::Inferior)
    .define_value("Superior", knowCore::ConstrainedValue::Type::Superior)
    .define_value("InferiorEqual", knowCore::ConstrainedValue::Type::InferiorEqual)
    .define_value("SuperiorEqual", knowCore::ConstrainedValue::Type::SuperiorEqual)
    .define_value("GeoOverlaps", knowCore::ConstrainedValue::Type::GeoOverlaps)
    .define_value("GeoWithin", knowCore::ConstrainedValue::Type::GeoWithin)
    .define_value("GeoContains", knowCore::ConstrainedValue::Type::GeoContains)
    .define_value("GeoIntersects", knowCore::ConstrainedValue::Type::GeoIntersects)
    .define_value("GeoTouches", knowCore::ConstrainedValue::Type::GeoTouches)
    .define_value("GeoDisjoint", knowCore::ConstrainedValue::Type::GeoDisjoint)
    .define_value("Contains", knowCore::ConstrainedValue::Type::Contains)
    .define_value("NotContains", knowCore::ConstrainedValue::Type::NotContains)
    .define_value("In", knowCore::ConstrainedValue::Type::In)
    .define_value("NotIn", knowCore::ConstrainedValue::Type::NotIn);

  rb_cConstrainedValue.define_constructor(Rice::Constructor<knowCore::ConstrainedValue>())
    .define_method(
      "check", overload::overload_r_c<cres_qresult<bool>, knowCore::ConstrainedValue,
                                      const knowCore::Value&>(&knowCore::ConstrainedValue::check))
    .define_method("apply",
                   overload::overload_r_c<knowCore::ConstrainedValue&, knowCore::ConstrainedValue,
                                          const knowCore::Value&, knowCore::ConstrainedValue::Type>(
                     &knowCore::ConstrainedValue::apply));

  Rice::Data_Type<kC::Timestamp> rb_cTimestamp
    = Rice::define_class_under<kC::Timestamp>(rb_mknowCore, "Timestamp")
        .define_singleton_function("now", &kC::Timestamp::now)
        .define_singleton_function(
          "fromTime",
          [](Rice::Object _time)
          {
            VALUE class_of_time = rb_class_of(_time);
            if(class_of_time == rb_cTime)
            {
              timespec ts = rb_time_timespec(_time);
              int64_t ns = uint64_t(ts.tv_sec) * std::nano::den + ts.tv_nsec;
              return knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(ns);
            }
            else
            {
              std::stringstream stream;
              stream << "Cannot convert " << Rice::detail::protect(rb_obj_classname, _time)
                     << " (rb_type = 0x" << std::hex << rb_type(_time) << ") "
                     << " to knowCore::Timestamp";
              throw std::invalid_argument(stream.str().c_str());
            }
          })
        .define_method(
          "toTime",
          [](/*const*/ kC::Timestamp* _self)
          {
            timespec ts;
            int64_t ns_ep
              = _self->toEpoch<knowCore::NanoSeconds>().count().toInt64().expect_success();
            ts.tv_sec = ns_ep / std::nano::den;
            ts.tv_nsec = ns_ep - ts.tv_sec * std::nano::den;
            return Rice::Object(rb_time_timespec_new(&ts, INT_MAX));
          });
  Rice::Data_Type<kC::Uri> rb_cUri
    = Rice::define_class_under<kC::Uri>(rb_mknowCore, "Uri")
        .define_constructor(Rice::Constructor<kC::Uri, QString>(), Rice::Arg("_uri") = QString())
        .define_method("==", [](/*const*/ knowCore::Uri* _self, const knowCore::Uri* _other)
                       { return *_self == *_other; })
        .define_method("<=>", [](/*const*/ knowCore::Uri* _self, const knowCore::Uri* _other)
                       { return *_self < *_other ? -1 : (*_self == *_other ? 0 : 1); })
        .define_method("to_s", [](/*const*/ knowCore::Uri* _self) { return QString(*_self); });
  Rice::define_class_under<kC::UriManager>(rb_mknowCore, "UriManager")
    .define_constructor(Rice::Constructor<kC::UriManager>());
}
