#include "knowCore.h"

#include "overload.h"

// Cleanup some ruby defines that clashes with fmt
#undef isfinite
#undef int128_t
#undef uint128_t

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

extern "C" void Init_know_dbc_bindings_ruby()
{
  Rice::Module rb_mknowDBC = Rice::define_module("KnowDBC");

  Rice::Data_Type<knowDBC::Result> rb_cResult
    = Rice::define_class_under<knowDBC::Result>(rb_mknowDBC, "Result")
        .define_method("fields", &knowDBC::Result::fields)
        .define_method("fieldIndex", &knowDBC::Result::fieldIndex)
        .define_method("fieldNames", &knowDBC::Result::fieldNames)
        .define_method("tuples", &knowDBC::Result::tuples)
        .define_method("value", [](knowDBC::Result* _self, int _tuple, int _field)
                       { return _self->value(_tuple, _field); });

  Rice::Data_Type<knowDBC::Query> rb_cQuery
    = Rice::define_class_under<knowDBC::Query>(rb_mknowDBC, "Query")
        .define_method(
          "bindValue",
          overload::overload_r_c<void, knowDBC::Query, const QString&, const knowCore::Value&>(
            &knowDBC::Query::bindValue))
        .define_method("execute", &knowDBC::Query::execute);
}
