#include "knowCore.h"

// Cleanup some ruby defines that clashes with fmt
#undef isfinite
#undef int128_t
#undef uint128_t

#include <knowGIS/GeometryObject.h>
#include <knowGIS/Point.h>

KNOWRUBY_REGISTER_VALUE_TO_VALUE(knowGIS::GeometryObject)

extern "C" void Init_know_gis_bindings_ruby()
{
  Rice::Module rb_mknowGIS = Rice::define_module("KnowGIS");
  Rice::Data_Type<knowGIS::GeometryObject> rb_cGeometryObject
    = Rice::define_class_under<knowGIS::GeometryObject>(rb_mknowGIS, "GeometryObject")
        .define_constructor(Rice::Constructor<knowGIS::GeometryObject>())
        .define_singleton_function("fromWKT", &knowGIS::GeometryObject::fromWKT)
        .define_method("toWKT", *[](knowGIS::GeometryObject self) { return self.toWKT(); });
}
