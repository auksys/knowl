#include "knowCore.h"

#include "overload.h"

// Cleanup some ruby defines that clashes with fmt
#undef isfinite
#undef int128_t
#undef uint128_t

#include <QBuffer>
#include <QFile>

#include <knowCore/Messages.h>

#include <knowRDF/Literal.h>
#include <knowRDF/Object.h>
#include <knowRDF/Serialiser.h>
#include <knowRDF/Triple.h>
#include <knowRDF/TripleStream.h>
#include <knowRDF/TripleStreamListener.h>

namespace knowRuby
{
  template<>
  class ValueToValueImplementation<knowRDF::Literal> : public Interfaces::ValueToValue
  {
  public:
    virtual ~ValueToValueImplementation() {}
    bool canConvertToRubyValue(const knowCore::Value& _value) const override
    {
      return _value.is<knowRDF::Literal>();
    }
    bool canConvertToKnowCoreValue(VALUE _value) const override
    {
      return Rice::detail::From_Ruby<knowRDF::Literal>().is_convertible(_value)
             != Rice::detail::Convertible::None;
    }
    VALUE toRubyValue(const knowCore::Value& _value) const override
    {
      Rice::Return r;
      r.takeOwnership();
      return Rice::detail::To_Ruby<knowRDF::Literal*>(&r).convert(
        new knowRDF::Literal(_value.value<knowRDF::Literal>().expect_success()));
    }
    knowCore::Value toKnowCoreValue(VALUE _value) const override
    {
      return knowCore::Value::fromValue(
        Rice::detail::From_Ruby<knowRDF::Literal>().convert(_value));
    }
  };
} // namespace knowRuby

KNOWRUBY_REGISTER_VALUE_TO_VALUE(knowRDF::Literal)
KNOWRUBY_REGISTER_VALUE_TO_VALUE(knowRDF::BlankNode)

extern "C" void Init_know_rdf_bindings_ruby()
{
  Rice::Module rb_mknowRDF = Rice::define_module("KnowRDF");

  Rice::Data_Type<knowRDF::Literal> rb_cLiteral
    = Rice::define_class_under<knowRDF::Literal>(rb_mknowRDF, "Literal")
        .define_method("datatype", &knowRDF::Literal::datatype)
        .define_method("value",
                       [](/*const*/ knowRDF::Literal* _self) -> knowCore::Value { return *_self; })
        .define_method("to_s", [](/*const*/ knowRDF::Literal* _self)
                       { return clog_qt::to_qstring(*_self); });
  Rice::Data_Type<knowRDF::BlankNode> rb_cBlankNode
    = Rice::define_class_under<knowRDF::BlankNode>(rb_mknowRDF, "BlankNode")
        .define_method("uuid", [](/*const*/ knowRDF::BlankNode* _self) -> QString
                       { return _self->uuid().toString(); });
  Rice::Data_Type<knowRDF::Object> rb_cObject
    = Rice::define_class_under<knowRDF::Object>(rb_mknowRDF, "Object");
  Rice::define_enum_under<knowRDF::Object::Type>("Type", rb_cObject)
    .define_value("Undefined", knowRDF::Object::Type::Undefined)
    .define_value("Uri", knowRDF::Object::Type::Uri)
    .define_value("BlankNode", knowRDF::Object::Type::BlankNode)
    .define_value("Literal", knowRDF::Object::Type::Literal)
    .define_value("Variable", knowRDF::Object::Type::Variable);
  rb_cObject.define_method("type", &knowRDF::Object::type)
    .define_method("uri", &knowRDF::Object::uri)
    .define_method("blankNode", &knowRDF::Object::blankNode)
    .define_method("literal", &knowRDF::Object::literal)
    .define_method("variableName", &knowRDF::Object::variableName)
    .define_singleton_function("fromValue", [](const knowCore::Value& _value)
                               { return knowRDF::Object(knowRDF::Literal::fromValue(_value)); })
    .define_method("to_s",
                   [](/*const*/ knowRDF::Object* _self) { return clog_qt::to_qstring(*_self); });
  Rice::define_class_under<knowRDF::Triple>(rb_mknowRDF, "Triple");

  Rice::define_class_under<knowRDF::TripleStreamListener>(rb_mknowRDF, "TripleStreamListener")
    .define_method("triple", &knowRDF::TripleStreamListener::triple);
  Rice::define_class_under<knowRDF::TripleStream>(rb_mknowRDF, "TripleStream")
    .define_constructor(Rice::Constructor<knowRDF::TripleStream>())
    .define_method("setBase", &knowRDF::TripleStream::setBase)
    .define_method("addListener", &knowRDF::TripleStream::addListener)
    .define_method("start", &knowRDF::TripleStream::start)
    .define_method(
      "start_from_string",
      [](knowRDF::TripleStream* _stream, const QString& _content, knowCore::Messages* _messages,
         const QString& _format)
      {
        QBuffer content_buffer;
        content_buffer.setData(_content.toUtf8());
        cres_qresult<void> rv = _stream->start(&content_buffer, _messages, _format);
        if(not rv.is_successful())
        {
          throw std::runtime_error(rv.get_error().get_message().toStdString());
        }
      },
      Rice::Arg("content"), Rice::Arg("messages") = nullptr,
      Rice::Arg("format") = knowCore::FileFormat::Turtle)
    .define_method(
      "start_from_file",
      [](knowRDF::TripleStream* _stream, const QString& _filename, knowCore::Messages* _messages,
         const QString& _format)
      {
        QFile data_ttl_file("data.ttl");
        if(not data_ttl_file.open(QIODevice::ReadOnly))
        {
          throw std::runtime_error("Failed to open file: " + _filename.toStdString());
        }
        cres_qresult<void> rv = _stream->start(&data_ttl_file, _messages, _format);
        if(not rv.is_successful())
        {
          throw std::runtime_error(rv.get_error().get_message().toStdString());
        }
      },
      Rice::Arg("content"), Rice::Arg("messages") = nullptr,
      Rice::Arg("format") = knowCore::FileFormat::Turtle);

  Rice::define_class_under<knowRDF::Serialiser>(rb_mknowRDF, "Serialiser")
    .define_singleton_function(
      "create_to_file",
      [](const QString& _filename, const knowCore::UriManager& _uriManager, const QString& _format)
      {
        QFile* f = new QFile(_filename);
        if(not f->open(QIODevice::WriteOnly))
        {
          throw std::runtime_error("Failed to open file: " + _filename.toStdString());
        }
        return new knowRDF::Serialiser(f, _uriManager, true, _format);
      })
    // .define_method("serialise", overload::overload<const
    // knowRDF::Triple&>(&knowRDF::Serialiser::serialise))
    .define_method("serialise", overload::overload<const QList<knowRDF::Triple>&>(
                                  &knowRDF::Serialiser::serialise));
}
