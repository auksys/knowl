#include "ConverterManager.h"

using namespace knowRuby;

struct ConverterManager::Private
{
  QList<const Interfaces::ValueToValue*> v2v;
};

ConverterManager::Private* ConverterManager::d()
{
  static Private* p = new Private;
  return p;
}

cres_qresult<VALUE> ConverterManager::toRubyValue(const knowCore::Value& _value)
{
  if(_value.isEmpty())
  {
    return cres_success(Qnil);
  }
  for(const Interfaces::ValueToValue* v : d()->v2v)
  {
    if(v->canConvertToRubyValue(_value))
    {
      return cres_success(v->toRubyValue(_value));
    }
  }
  return cres_failure("Unknown converter for knowCore Value {}", _value);
}

cres_qresult<knowCore::Value> ConverterManager::toKnowCoreValue(VALUE _value)
{
  for(const Interfaces::ValueToValue* v : d()->v2v)
  {
    if(v->canConvertToKnowCoreValue(_value))
    {
      return cres_success(v->toKnowCoreValue(_value));
    }
  }
  return cres_failure("Unknown converter for ruby value '{}' of type '{}' ({}) ",
                      Rice::Object(_value).to_s().str(),
                      Rice::detail::protect(rb_obj_classname, _value), (int)rb_type(_value));
}

void ConverterManager::registerConverter(const Interfaces::ValueToValue* _value)
{
  d()->v2v.append(_value);
}
