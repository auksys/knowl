#include "ValueToValueImplementation.h"

namespace knowRuby
{
  class ConverterManager
  {
  public:
    static cres_qresult<VALUE> toRubyValue(const knowCore::Value& _value);
    static cres_qresult<knowCore::Value> toKnowCoreValue(VALUE _value);
    static void registerConverter(const Interfaces::ValueToValue* _value);
    template<typename _T_>
    static void registerConverter()
    {
      registerConverter(new ValueToValueImplementation<_T_>());
    }
  private:
    struct Private;
    static Private* d();
  };
} // namespace knowRuby

#define KNOWRUBY_REGISTER_VALUE_TO_VALUE_NOT_POINTER(_TYPE_)                                       \
  namespace knowRuby                                                                               \
  {                                                                                                \
    template<>                                                                                     \
    struct ValueToValueImplementationTraits<_TYPE_>                                                \
    {                                                                                              \
      static constexpr bool as_pointer = false;                                                    \
    };                                                                                             \
  }

#define __KNOWRUBY_REGISTER_VALUE_TO_VALUE(_FACTORY_NAME_, _TYPE_)                                 \
  namespace                                                                                        \
  {                                                                                                \
    class _FACTORY_NAME_                                                                           \
    {                                                                                              \
    public:                                                                                        \
      _FACTORY_NAME_() { knowRuby::ConverterManager::registerConverter<_TYPE_>(); }                \
      static _FACTORY_NAME_ s_instance;                                                            \
    };                                                                                             \
    _FACTORY_NAME_ _FACTORY_NAME_::s_instance;                                                     \
  }
#define KNOWRUBY_REGISTER_VALUE_TO_VALUE(_TYPE_)                                                   \
  __KNOWRUBY_REGISTER_VALUE_TO_VALUE(__KNOWCORE_UNIQUE_STATIC_NAME(ValueToValueRegistration),      \
                                     _TYPE_)
