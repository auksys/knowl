#pragma once

#include <clog_qt>
#include <cres_qt>

namespace knowRuby
{
  template<typename _T_>
  inline _T_ handle(const cres_qresult<_T_>& _value)
  {
    if(_value.is_successful())
    {
      return _value.get_value();
    }
    else
    {
      throw std::runtime_error(_value.get_error().get_message().toStdString());
    }
  }
} // namespace knowRuby
