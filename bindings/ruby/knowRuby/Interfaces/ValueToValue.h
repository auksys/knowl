#include <clog_qt>
#include <knowCore/Forward.h>

#include <ruby.h>

namespace knowRuby::Interfaces
{
  class ValueToValue
  {
  public:
    virtual ~ValueToValue();
    virtual bool canConvertToRubyValue(const knowCore::Value& _value) const = 0;
    virtual bool canConvertToKnowCoreValue(VALUE _value) const = 0;
    virtual VALUE toRubyValue(const knowCore::Value& _value) const = 0;
    virtual knowCore::Value toKnowCoreValue(VALUE _value) const = 0;
  };
} // namespace knowRuby::Interfaces