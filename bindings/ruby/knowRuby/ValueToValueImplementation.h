#include "Interfaces/ValueToValue.h"

#include <rice/rice.hpp>

#include <knowCore/Value.h>

#include "HandleResult.h"

namespace knowRuby
{
  template<typename _T_>
  struct ValueToValueImplementationTraits
  {
    static constexpr bool as_pointer = true;
  };
  template<typename _T_>
  class ValueToValueImplementation : public Interfaces::ValueToValue
  {
  public:
    virtual ~ValueToValueImplementation() {}
    bool canConvertToRubyValue(const knowCore::Value& _value) const override
    {
      return _value.canConvert<_T_>();
    }
    bool canConvertToKnowCoreValue(VALUE _value) const override
    {
      return Rice::detail::From_Ruby<_T_>().is_convertible(_value)
             != Rice::detail::Convertible::None;
    }
    VALUE toRubyValue(const knowCore::Value& _value) const override
    {
      if constexpr(std::is_trivially_constructible_v<_T_>
                   or not ValueToValueImplementationTraits<_T_>::as_pointer)
      {
        return Rice::detail::To_Ruby<_T_>().convert(handle(_value.value<_T_>()));
      }
      else
      {
        Rice::Return r;
        r.takeOwnership();
        return Rice::detail::To_Ruby<_T_*>(&r).convert(new _T_(handle(_value.value<_T_>())));
      }
    }
    knowCore::Value toKnowCoreValue(VALUE _value) const override
    {
      return knowCore::Value::fromValue(Rice::detail::From_Ruby<_T_>().convert(_value));
    }
  };
} // namespace knowRuby
