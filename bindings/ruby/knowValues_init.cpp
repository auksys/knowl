#include "knowCore.h"

// Cleanup some ruby defines that clashes with fmt
#undef isfinite
#undef int128_t
#undef uint128_t

#include <knowValues/Values.h>

KNOWRUBY_REGISTER_VALUE_TO_VALUE(knowValues::Values::SalientRegion)

#define DEFINE_GETTER(_NAME_, _KLASS_)                                                             \
  .define_method(#_NAME_, [](const kVV::_KLASS_& _self) { return _self->_NAME_(); })

extern "C" void Init_know_values_bindings_ruby()
{
  namespace kVV = knowValues::Values;
  Rice::Module rb_mknowValues = Rice::define_module("KnowValues");
  Rice::Data_Type<kVV::SalientRegion> rb_cSalientRegion
    = Rice::define_class_under<kVV::SalientRegion>(rb_mknowValues, "SalientRegion")
        .define_method("valid?", [](const kVV::SalientRegion& _self) { return _self.isValid(); })
          DEFINE_GETTER(uri, SalientRegion) DEFINE_GETTER(geometry, SalientRegion)
            DEFINE_GETTER(classes, SalientRegion) DEFINE_GETTER(timestamp, SalientRegion);
}
