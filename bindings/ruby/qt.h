#include <rice/rice.hpp>
#include <rice/stl.hpp>

namespace rice_qt
{

  template<typename _T_>
  class From_Ruby_Ptr
  {
  public:
    using From_Ruby_Ptr_T = From_Ruby_Ptr<_T_>;
    Rice::detail::Convertible is_convertible(VALUE value)
    {
      return Rice::detail::From_Ruby<_T_>().is_convertible(value);
    }
    _T_* convert(VALUE value)
    {
      this->converted_ = Rice::detail::From_Ruby<_T_>().convert(value);
      return &this->converted_;
    }
  private:
    _T_ converted_;
  };

  template<typename _T_>
  class From_Ruby_Ref
  {
  public:
    using From_Ruby_Ref_T = From_Ruby_Ref<_T_>;
    From_Ruby_Ref() = default;

    explicit From_Ruby_Ref(Rice::Arg* arg) : arg_(arg) {}

    Rice::detail::Convertible is_convertible(VALUE value)
    {
      return Rice::detail::From_Ruby<_T_>().is_convertible(value);
    }

    _T_& convert(VALUE value)
    {
      if(value == Qnil && this->arg_ && this->arg_->hasDefaultValue())
      {
        return this->arg_->defaultValue<_T_>();
      }
      else
      {
        this->converted_ = Rice::detail::From_Ruby<_T_>().convert(value);
        return this->converted_;
      }
    }
  private:
    Rice::Arg* arg_ = nullptr;
    _T_ converted_;
  };
} // namespace rice_qt

namespace Rice::detail
{

  // QString

  template<>
  struct Type<QString>
  {
    static bool verify() { return true; }
  };

  template<>
  class To_Ruby<QString>
  {
  public:
    VALUE convert(QString const& x) { return To_Ruby<std::string>().convert(x.toStdString()); }
  };

  template<>
  class To_Ruby<QString&>
  {
  public:
    VALUE convert(QString const& x) { return To_Ruby<std::string>().convert(x.toStdString()); }
  };

  template<>
  class From_Ruby<QString>
  {
  public:
    From_Ruby() = default;

    explicit From_Ruby(Arg* arg) : arg_(arg) {}

    Rice::detail::Convertible is_convertible(VALUE value)
    {
      return From_Ruby<std::string>().is_convertible(value);
    }

    QString convert(VALUE value)
    {
      if(value == Qnil && this->arg_ && this->arg_->hasDefaultValue())
      {
        return this->arg_->defaultValue<QString>();
      }
      else
      {
        return QString::fromStdString(From_Ruby<std::string>().convert(value));
      }
    }
  private:
    Arg* arg_ = nullptr;
  };

  template<>
  struct From_Ruby<QString*> : public rice_qt::From_Ruby_Ptr<QString>
  {
    using From_Ruby_Ptr_T::From_Ruby_Ptr_T;
  };

  template<>
  struct From_Ruby<QString&> : public rice_qt::From_Ruby_Ref<QString>
  {
    using From_Ruby_Ref_T::From_Ruby_Ref_T;
  };

  // QList

  template<typename T>
  struct Type<QList<T>>
  {
    static bool verify()
    {
      Type<T>::verify();
      return true;
    }
  };

  template<>
  struct Type<QStringList>
  {
    static bool verify() { return true; }
  };

  template<typename T>
  VALUE qlistToArray(const QList<T>& value)
  {
    VALUE r_arr = detail::protect(rb_ary_new);
    for(const T& t : value)
    {
      detail::protect(rb_ary_push, r_arr, detail::To_Ruby<T>().convert(t));
    }
    return r_arr;
  }

  template<typename T>
  class To_Ruby<QList<T>>
  {
  public:
    VALUE convert(QList<T> const& x) { return qlistToArray(x); }
  };

  template<typename T>
  class To_Ruby<QList<T>&>
  {
  public:
    VALUE convert(QList<T> const& x) { return qlistToArray(x); }
  };
  template<typename T>
  QList<T> qlistFromArray(VALUE value)
  {
    size_t length = protect(rb_array_len, value);
    QList<T> result;
    result.reserve(length);

    for(size_t i = 0; i < length; i++)
    {
      VALUE element = protect(rb_ary_entry, value, i);
      result.append(From_Ruby<T>().convert(element));
    }

    return result;
  }

  template<typename T>
  class From_Ruby<QList<T>>
  {
  public:
    From_Ruby() = default;

    explicit From_Ruby(Arg* arg) : arg_(arg) {}

    Convertible is_convertible(VALUE value)
    {
      switch(rb_type(value))
      {
      case RUBY_T_DATA:
        return Convertible::Exact;
        break;
      case RUBY_T_ARRAY:
        return Convertible::Cast;
        break;
      default:
        return Convertible::None;
      }
    }

    QList<T> convert(VALUE value)
    {
      switch(rb_type(value))
      {
      case T_DATA:
      {
        // This is a wrapped vector (hopefully!)
        return *Data_Object<QList<T>>::from_ruby(value);
      }
      case T_ARRAY:
      {
        // If this an Ruby array and the vector type is copyable
        if constexpr(std::is_default_constructible_v<T>)
        {
          return qlistFromArray<T>(value);
        }
      }
      case T_NIL:
      {
        if(this->arg_ && this->arg_->hasDefaultValue())
        {
          return this->arg_->template defaultValue<QList<T>>();
        }
        [[fallthrough]];
      }
      default:
      {
        throw Exception(rb_eTypeError, "wrong argument type %s (expected % s)",
                        detail::protect(rb_obj_classname, value), "QList");
      }
      }
    }
  private:
    Arg* arg_ = nullptr;
  };

  template<typename T>
  struct From_Ruby<QList<T>*> : public rice_qt::From_Ruby_Ptr<QList<T>>
  {
    using rice_qt::From_Ruby_Ptr<QList<T>>::From_Ruby_Ptr;
  };

  template<typename T>
  struct From_Ruby<QList<T>&> : public rice_qt::From_Ruby_Ref<QList<T>>
  {
    using rice_qt::From_Ruby_Ref<QList<T>>::From_Ruby_Ref_T;
  };

  // QHash

  template<typename K, typename V>
  struct Type<QHash<K, V>>
  {
    static bool verify() { return Type<K>::verify() and Type<V>::verify(); }
  };

  template<typename K, typename V>
  VALUE qhashToHash(const QHash<K, V>& value)
  {
    VALUE r_hash = detail::protect(rb_hash_new);
    for(auto it = value.begin(); it != value.end(); ++it)
    {
      detail::protect(rb_hash_aset, r_hash, detail::To_Ruby<K>().convert(it.key()),
                      detail::To_Ruby<V>().convert(it.value()));
    }
    return r_hash;
  }

  template<typename K, typename V>
  class To_Ruby<QHash<K, V>>
  {
  public:
    VALUE convert(QHash<K, V> const& x) { return qhashToHash(x); }
  };

  template<typename K, typename V>
  class To_Ruby<QHash<K, V>&>
  {
  public:
    VALUE convert(QHash<K, V> const& x) { return qhashToHash(x); }
  };

  template<typename K, typename V>
  int qhashFromHash_convertPair(VALUE key, VALUE value, VALUE user_data)
  {
    QHash<K, V>* result = (QHash<K, V>*)(user_data);
    return cpp_protect(
      [&]
      {
        result->operator[](From_Ruby<K>().convert(key)) = From_Ruby<V>().convert(value);
        return ST_CONTINUE;
      });
  }

  template<typename K, typename V>
  QHash<K, V> qhashFromHash(VALUE value)
  {
    QHash<K, V> result;
    VALUE user_data = (VALUE)(&result);

    // MSVC needs help here, but g++ does not
    using Rb_Hash_ForEach_T = void (*)(VALUE, int (*)(VALUE, VALUE, VALUE), VALUE);
    detail::protect<Rb_Hash_ForEach_T>(rb_hash_foreach, value, qhashFromHash_convertPair<K, V>,
                                       user_data);

    return result;
  }

  template<typename K, typename V>
  class From_Ruby<QHash<K, V>>
  {
  public:
    From_Ruby() = default;

    explicit From_Ruby(Arg* arg) : arg_(arg) {}

    Convertible is_convertible(VALUE value)
    {
      switch(rb_type(value))
      {
      case RUBY_T_DATA:
        return Convertible::Exact;
        break;
      case RUBY_T_NIL:
        return Convertible::Exact;
        break;
      case RUBY_T_HASH:
        return Convertible::Cast;
        break;
      default:
        return Convertible::None;
      }
    }

    QHash<K, V> convert(VALUE value)
    {
      switch(rb_type(value))
      {
      case T_DATA:
      {
        // This is a wrapped vector (hopefully!)
        return *Data_Object<QHash<K, V>>::from_ruby(value);
      }
      case T_HASH:
      {
        // If this an Ruby array and the vector type is copyable
        if constexpr(std::is_default_constructible_v<V>)
        {
          return qhashFromHash<K, V>(value);
        }
      }
      case T_NIL:
      {
        if(this->arg_ && this->arg_->hasDefaultValue())
        {
          return this->arg_->template defaultValue<QHash<K, V>>();
        }
      }
      default:
      {
        throw Exception(rb_eTypeError, "wrong argument type %s (expected % s)",
                        detail::protect(rb_obj_classname, value), "QHash");
      }
      }
    }
  private:
    Arg* arg_ = nullptr;
  };

  template<typename K, typename V>
  struct From_Ruby<QHash<K, V>*> : public rice_qt::From_Ruby_Ptr<QHash<K, V>>
  {
    using rice_qt::From_Ruby_Ptr<QHash<K, V>>::From_Ruby_Ptr;
  };

  template<typename K, typename V>
  struct From_Ruby<QHash<K, V>&> : public rice_qt::From_Ruby_Ref<QHash<K, V>>
  {
    using rice_qt::From_Ruby_Ref<QHash<K, V>>::From_Ruby_Ref;
  };

} // namespace Rice::detail
