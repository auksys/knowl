
function(knowl_add_rspec_test)
cmake_parse_arguments(knowl_add_rspec_test "" "NAME;COMMAND" "" ${ARGN})

add_test(NAME ${knowl_add_rspec_test_NAME} COMMAND ${CMAKE_COMMAND} -E env RUBYLIB=${CMAKE_SOURCE_DIR}/bindings/ruby:${CMAKE_BINARY_DIR}/bindings/ruby:$ENV{RUBYLIB} /usr/bin/env rspec ${knowl_add_rspec_test_COMMAND} WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

endfunction()

knowl_add_rspec_test(NAME test-ruby-knowcore COMMAND test_knowcore.rb)
knowl_add_rspec_test(NAME test-ruby-knowgis COMMAND test_knowgis.rb)
