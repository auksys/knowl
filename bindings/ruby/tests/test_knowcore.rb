require 'knowl-rspec'
require 'knowCore'


RSpec.describe "Timestamp" do
  it "can be created from time" do
    dt0 = Time.new - 30*60
    dt1 = Time.new - 30*60
    ts = KnowCore::Timestamp.fromTime(dt0)
    expect(ts.toTime).to eq(dt0)
    expect(ts.toTime).not_to eq(dt1)
end
end

RSpec.describe "ConstrainedValue" do
  it "can check for values" do
    cv = KnowCore::ConstrainedValue.new
    .apply(20, KnowCore::ConstrainedValue::Type::Superior)
    .apply(30, KnowCore::ConstrainedValue::Type::Inferior)

    expect(cv.check(10)).to be(false)
    expect(cv.check(20)).to be(false)
    expect(cv.check(25)).to be(true)
    expect(cv.check(30)).to be(false)
    expect(cv.check(40)).to be(false)
  end
end
