require 'knowl-rspec'
require 'knowGIS'

RSpec.describe "GeometryObject" do
  it "support WKT" do
    wkt_str = "POLYGON ((16.683875600036536 57.76189821045434, 16.681086102661048 57.760765003387945, 16.682416478332435 57.75976912541624, 16.684680262741004 57.760621919160826, 16.685205975707923 57.76117135950369, 16.683875600036536 57.76189821045434))"
    obj = KnowGIS::GeometryObject.fromWKT(wkt_str)
    expect(obj.toWKT()).to eq(wkt_str)
    expect { KnowGIS::GeometryObject.fromWKT("not a valid wkt") }.to raise_error(RuntimeError)
  end
end
