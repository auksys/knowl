# Look for GeographicLib
#
# Set
#  GEOGRAPHICLIB_FOUND = TRUE
#  GEOGRAPHICLIB_INCLUDE_DIR = /usr/local/include
#  GEOGRAPHICLIB_LIBRARIES = /usr/local/lib/libGeographic.so

find_path(GEOGRAPHICLIB_INCLUDE_DIR GeographicLib/Config.h)

find_library (GEOGRAPHICLIB_LIBRARIES Geographic
  PATHS "${CMAKE_INSTALL_PREFIX}/../GeographicLib/lib")

if (NOT GEOGRAPHICLIB_LIBRARIES)
  find_library (GEOGRAPHICLIB_LIBRARIES GeographicLib)
endif()

include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (GeographicLib DEFAULT_MSG GEOGRAPHICLIB_LIBRARIES GEOGRAPHICLIB_INCLUDE_DIR)
mark_as_advanced (GEOGRAPHICLIB_INCLUDE_DIR GEOGRAPHICLIB_LIBRARIES)

if (NOT TARGET GeographicLib)
  add_library(GeographicLib INTERFACE IMPORTED)
endif()
set_target_properties(GeographicLib PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${GEOGRAPHICLIB_INCLUDE_DIR}"
  INTERFACE_LINK_LIBRARIES "${GEOGRAPHICLIB_LIBRARIES}")
