// To hide Qt3D debug message: export QT_LOGGING_RULES="Qt3D.*=false;ShaderGenerator=false"

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Scene3D
import Qt3D.Core
import Qt3D.Render
import Qt3D.Input
import Qt3D.Extras
import Qt3D.Logic
import QtQuick as QQ2

import knowVis
import knowVis.Entities

ApplicationWindow
{
  id: root
  title: "knowVis Example"
  visible: true
  width: 800
  height: 600
  
  property var selectedFeature: null
  Context
  {
    id: _context_
    origin.longitude: 15.57065248
    origin.latitude: 58.3948809
    origin.altitude: 74
  }
  RowLayout
  {
    anchors.fill: parent
    ColumnLayout
    {
      RowLayout
      {
        Label
        {
          text: "Camera"
        }
        ComboBox
        {
          model: ["orbit", "top down orthographic"]
          onActivated: cameraState.state = cameraState.states[index].name
          Item {
            id: cameraState
            state: "orbit"
            states: [
            State {
              name: "orbit"
              PropertyChanges {
                target: camera
                projectionType: CameraLens.PerspectiveProjection
                position: Qt.vector3d( 0.0, 0.0, 5.0 )
                upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
                viewCenter: Qt.vector3d( 0.0, 0.0, 0.0 )
              }
            }, State {
              name: "topDownOrthographic"
              PropertyChanges {
                target: camera
                projectionType: CameraLens.OrthographicProjection
                left: -10.0
                right: 10.0
                top: 10.0
                bottom: -10.0
                position: Qt.vector3d( 0.0, 0.0, 40.0 )
                upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
                viewCenter: Qt.vector3d( 0.0, 0.0, 0.0 )
              }
            }
            ]
          }
        }
      }
      Label
      {
        text: "Selected feature: " + (root.selectedFeature ? root.selectedFeature.id : "none")
      }
      Repeater
      {
        model: root.selectedFeature ? root.selectedFeature.estimatedState : null
        Label {
          text: modelData
        }
      }
      Item
      {
        width: 1
        Layout.fillHeight: true
      }
    }
    Scene3D
    {
      id: scene3d
      focus: true
      aspects: ["input", "logic"]
      cameraAspectRatioMode: Scene3D.AutomaticAspectRatio
      Entity {
        id: sceneRoot
        
        Camera {
          id: camera
          fieldOfView: 45
          nearPlane : 0.1
          farPlane : 1000.0
        }

        MouseDevice
        {
          id: mouseDevice_
        }
        XYOrbitCameraController {
          camera: camera
          mouseDevice: mouseDevice_
        }
        
        components: [
          RenderSettings {
            activeFrameGraph: ForwardRenderer {
              camera: camera
              clearColor: "black"
            }
          },
          InputSettings { }
        ]
        
        Axes
        {
          id: origin
          scale: 5
        }
        Axes
        {
          context: _context_
          pose.longitude: 15.57029843
          pose.latitude: 58.39485841
          pose.altitude: 73
          scale: 5
        }
        LidarScan
        {
          context: _context_
          frame: Test.createLidarScanAt(15.57029843, 58.39485841, 73)
          material.color: "green"
        }
        Axes
        {
          context: _context_
          pose.longitude: 15.57089843
          pose.latitude: 58.39490841
          pose.altitude: 79
          scale: 5
        }
        PointCloud
        {
          context: _context_
          pointCloud: Test.createPointCloudAt(15.57089843, 58.39490841, 79)
          material {
            color: "red"
            secondaryColor: "blue"
            colorMode: PointCloudMaterial.ZGradient
            gradientMin: -10.0
            gradientMax: 10.0
            pointSize: 12
          }
        }
      }
      Layout.fillWidth: true
      Layout.fillHeight: true
    }
  }
}

