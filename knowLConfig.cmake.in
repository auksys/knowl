# - Config file for the knowL package
# It defines the following variables
#  KNOWL_INCLUDE_DIRS - include directories for knowL
#  KNOWL_LIBRARIES    - libraries to link against
#  KNOWL_EXECUTABLE   - the bar executable
 
@PACKAGE_INIT@

# Compute paths
get_filename_component(KNOWL_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
set(KNOWL_INCLUDE_DIRS "@PACKAGE_INSTALL_INCLUDE_DIR@")

# Our library dependencies (contains definitions for IMPORTED targets)
if(NOT TARGET knowCore AND NOT KNOWL_BINARY_DIR)
  include("${KNOWL_CMAKE_DIR}/knowLTargets.cmake")
endif()

# Dependencies

include(CMakeFindDependencyMacro)

find_dependency(Qt6Core REQUIRED)

# These are IMPORTED targets created by knowLTargets.cmake
set(KNOWL_LIBRARIES @PROJECT_EXPORTED_TARGETS@)
set(KNOWL_QUERIES_GENERATOR_COMMAND @CONF_BIN_DIR@/knowLQueriesGenerator)
# set(KNOWL_EXECUTABLE bar)

set(INSTALL_KNOWBOOK_PLUGINS_DIR @INSTALL_KNOWBOOK_PLUGINS_DIR@)

macro(__knowl_add_nice_target name libname)
  set(${name}_FOUND TRUE)
  if (NOT TARGET ${name})
    add_library(${name} INTERFACE IMPORTED)
  endif()
  set_target_properties(${name} PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${KNOWL_INCLUDE_DIRS}"
  )
  if( NOT ${name} STREQUAL ${libname})
  set_target_properties(${name} PROPERTIES
    INTERFACE_LINK_LIBRARIES ${libname}
  )
  endif()
endmacro()

macro(__knowl_add_nice_target_core_library CV KCF name libname )
  if(${CV})
    __knowl_add_nice_target(${name} ${libname})
    list(APPEND ${KCF} ${name})
  endif()
endmacro()

__knowl_add_nice_target_core_library(TRUE __KNOWL_COMPONENTS_FINDABLE__ knowCore knowCore)
__knowl_add_nice_target_core_library(@BUILD_KNOWL_QUICK@ __KNOWL_COMPONENTS_FINDABLE__ knowCore::Quick knowCoreQuick)
__knowl_add_nice_target_core_library(TRUE __KNOWL_COMPONENTS_FINDABLE__ knowDBC knowDBC)
__knowl_add_nice_target_core_library(@BUILD_KNOWL_QUICK@ __KNOWL_COMPONENTS_FINDABLE__ knowDBC::Quick knowDBCQuick)
__knowl_add_nice_target_core_library(TRUE __KNOWL_COMPONENTS_FINDABLE__ knowRDF knowRDF)
__knowl_add_nice_target_core_library(@BUILD_KNOWGIS@ __KNOWL_COMPONENTS_FINDABLE__ knowGIS knowGIS)
__knowl_add_nice_target_core_library(@BUILD_KNOWVALUES@ __KNOWL_COMPONENTS_FINDABLE__ knowValues knowValues)

__knowl_add_nice_target_core_library(@BUILD_KNOWL_PYTHON@ __KNOWL_COMPONENTS_FINDABLE__ knowCore::Python knowCorePython)

if(KNOWL_FIND_COMPONENTS)
  foreach(comp ${KNOWL_FIND_COMPONENTS})
    if(NOT ${comp} IN_LIST __KNOWL_COMPONENTS_FINDABLE__)
      message(FATAL_ERROR "know${comp} is not available!")
    endif()
  endforeach()
endif()

########### Check for C++20 ############
if(NOT CMAKE_CXX_STANDARD OR CMAKE_CXX_STANDARD LESS 20)
  message(FATAL_ERROR "C++20 is required for knowL")
endif()

set(KNOWL_FOUND true)

# If your package also provides CMake macros or functions, you might want
# to put them in a file FooBarUse.cmake (or similar), install it alongside
# FooBarConfig.cmake and define the variable FOOBAR_USE_FILE in above code
# and set it to the location of the FooBarUse.cmake file.

# Dependencies goes at the end to avoid messing variables such as PACKAGE_PREFIX_DIR

find_dependency(Cyqlops REQUIRED COMPONENTS Crypto)
find_dependency(clog REQUIRED)

if(@BUILD_KNOWGIS@)
  if(NOT KNOWL_FIND_COMPONENTS OR "knowGIS" IN_LIST "${KNOWL_FIND_COMPONENTS}")
    find_package(Cartography REQUIRED COMPONENTS Geometry Raster Algorithms GDAL)
  endif()
endif()

