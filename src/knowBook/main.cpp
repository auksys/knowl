// Need to be a QApplication to use QtCharts
#include <QApplication>

#include "config.h"

#include <QLoggingCategory>

#include <QAbstractItemModel>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int main(int argc, char* argv[])
{
  QApplication app(argc, argv);
  app.setOrganizationDomain("org.auksys");
  app.setApplicationName("knowBook");

  QQmlApplicationEngine engine;
  engine.addImportPath("qrc:/qml/");

  engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

  return app.exec();
}
