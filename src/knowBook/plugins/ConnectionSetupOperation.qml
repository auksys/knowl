import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import knowBook
import knowBook.Plugins
import knowBook.NotebookStyle

OperationPlugin
{
  name: "Connection Setup"
  function initialise(operation)
  {}
  operationComponent: ColumnLayout {
    id: _root_
    property QtObject operation
    property var pluginInstance: _root_.operation ? _root_.operation.pluginsManager.connectionPluginFor(_connection_selector_.currentText) : null
    property var __store: null
    property var __connection: null
    Connections
    {
      target: _root_.__store
      function onLastErrorChanged(errMsg) { operation.localState.set("errorMessage", _root_.__connection.lastError)}
    }
    Connections
    {
      target: _root_.__connection
      function onLastErrorChanged() {
        operation.localState.set("errorMessage", _root_.__connection.lastError)}
    }
    function __initialise()
    {
      if(operation && pluginInstance)
      {
        if(pluginInstance.name != operation.definition["connectionType"])
        {
          operation.definition["connectionType"] = pluginInstance.name
          pluginInstance.initialise(_root_.operation)
          if(pluginInstance.storeComponent)
          {
            _root_.__store = pluginInstance.storeComponent.createObject(null)
            _root_.__connection = _root_.__store.connection
          } else {
            _root_.__store = null
            _root_.__connection = pluginInstance.connectionComponent.createObject(null)
          }
          _root_.operation.context.state.set("store", _root_.__store)
          _root_.operation.context.state.set("connection", _root_.__connection)
          operation.localState.set("store", __store)
          operation.localState.set("connection", __connection)
          
        }
        
      }
    }
    onOperationChanged:
    {
      if(operation)
      {
        __store = operation.localState.store
        __connection = operation.localState.connection
        operation.context.state.set("store", __store)
        operation.context.state.set("connection", __connection)
        __initialise()
      }
    }
    onPluginInstanceChanged:
    {
      __initialise()
    }
    Loader
    {
      id: _loader_
      sourceComponent: _root_.pluginInstance ? _root_.pluginInstance.configurationComponent : null
      onLoaded: item.operation = _root_.operation
      Layout.fillWidth: true
    }
    RowLayout
    {
      Label
      {
        text: "Connection type:"
      }
      ComboBox
      {
        id: _connection_selector_
        model: _root_.operation.pluginsManager ? _root_.operation.pluginsManager.connectionPlugins.map(x => x.name) : null
        implicitContentWidthPolicy: ComboBox.WidestText
      }
      Button
      {
        text: (_root_.__store && !_root_.__store.storeRunning ) ? "Start store" : "Stop store"
        visible: _root_.__store
        onClicked: {
          if(_root_.__store.storeRunning)
          {
            _root_.__store.stop()
          } else {
            _root_.pluginInstance.update(_root_.operation, _root_.__store, _root_.__connection)
            _root_.__store.start()
            _root_.__connection.connect()
          }
        }
      }
      Button
      {
        text: (_root_.__connection && !_root_.__connection.connected) ? "Connect" : "Disconnect"
        enabled: _root_.__connection
        onClicked: {
          if(_root_.__connection.connected)
          {
            _root_.__connection.disconnect()
          } else {
            _root_.pluginInstance.update(_root_.operation, _root_.__store, _root_.__connection)
            _root_.__connection.connect()
          }
        }
      }
      Layout.alignment: Qt.AlignRight
      Layout.fillWidth: true
    }
  }
}
