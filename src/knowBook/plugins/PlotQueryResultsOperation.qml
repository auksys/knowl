import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Dialogs
import QtQuick.Dialogs
import QtCharts
import QtQml

import knowBook.Controls as KC
import knowCore
import knowDBC

import knowBook.Plugins

OperationPlugin
{
  name: "Plot Query Results"
  operationComponent: SplitView
  {
    id: root
    orientation: Qt.Vertical
    property real __query_time: 0
    property bool __is_sparql: queryMode.currentText == "SPARQL"
    property bool __is_sql: !root.__is_sparql
    property var connection
    Query
    {
      id: sparqlQuery
      query: queryEditor.text
      connection: root.connection
    }
    Query
    {
      id: sqlQuery
      query: queryEditor.text
      connection: root.connection
    }
    property variant activeQuery: root.__is_sparql ? sparqlQuery : sqlQuery
    ListToTableItemModel
    {
      id: listToTableModel
      sourceModel: activeQuery.result
    }
    ColumnLayout
    {
      TextArea
      {
        id: queryEditor
        Layout.fillWidth: true
        Layout.fillHeight: true
        text: "SELECT * FROM triples;"
      }
      RowLayout
      {
        Layout.fillWidth: true
        Label
        {
          text: (listToTableModel.sourceModel ? listToTableModel.sourceModel.rowCount() + " rows." : "No query result.") + /* (root.__query_time > 0) ?*/ "Executed in " + root.__query_time + "ms." /* : ""*/;
        }
        Item
        {
          Layout.fillWidth: true
          Layout.fillHeight: true
        }
        ComboBox
        {
          id: queryMode
          model: [ "SQL", "SPARQL" ]
        }
        Button
        {
          text: "Execute"
          Component
          {
            id: vxyModelMapperMaker
            VXYModelMapper
            {
              xColumn: 0
            }
          }
          onClicked: {
            var t1 = KnowCore.now()
            root.activeQuery.execute()
            var t2 = KnowCore.now()
            root.__query_time = t2 - t1
            
            chartView.removeAllSeries()
            var minmax_x = listToTableModel.calculateColumnMinMax(0)
            chartAxisX.min = minmax_x[0]
            chartAxisX.max = minmax_x[1]
            
            for(var col = 1; col < listToTableModel.columnCount(); ++col)
            {
              var s = chartView.createSeries(ChartView.SeriesTypeLine, listToTableModel.columnNames[col], chartAxisX, chartAxisY)
              var mm = vxyModelMapperMaker.createObject(s, { model: listToTableModel, series: s, yColumn: col })
              
              var minmax_y = listToTableModel.calculateColumnMinMax(col)

              if(col == 1)
              {
                chartAxisY.min = minmax_y[0]
                chartAxisY.max = minmax_y[1]
              } else {
                chartAxisY.min = Math.min(chartAxisY.min, minmax_y[0])
                chartAxisY.max = Math.min(chartAxisY.max, minmax_y[0])
              }
              
            }
            
          }
        }
      }
    }
    ChartView
    {
      id: chartView
      ValueAxis {
        id: chartAxisX
      }
      ValueAxis {
        id: chartAxisY
      }
    }
  }
}
