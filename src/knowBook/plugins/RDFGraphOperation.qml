import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import Cyqlops.Controls

import knowDBC
import knowBook.Controls

import knowBook.Plugins

OperationPlugin
{
  name: "RDF Graph"
  operationComponent: SplitView
  {
    id: root
    orientation: Qt.Horizontal
    property string centralNodeUri
    property var connection
    Query
    {
      id: sparqlNounQuery
      connection: root.connection
      query: "SELECT DISTINCT ?noun WHERE { ?noun ?y ?z . } ORDER BY ?noun;"
    }
    TableView
    {
      id: resultsTable
      function refresh()
      {
        sparqlNounQuery.execute()
      }
      model: sparqlNounQuery.result
      onVisibleChanged: {
        if(visible && root.connection.isConnected)
        {
          refresh()
        }
      }
      onClicked:
      {
        root.centralNodeUri = model.data(model.index(row, 0), 0x0100)
      }
    }
    Component
    {
      id: nodeView
      Rectangle
      {
        width: nodeViewText.width + 6
        height: nodeViewText.height + 6
        radius: 3
        color: "black"
        property alias label: nodeViewText.text
        Label
        {
          id: nodeViewText
          anchors.centerIn: parent
          color: "white"
        }
        MouseArea
        {
          anchors.fill: parent
          onClicked: root.centralNodeUri = label
        }
      }
    }
    Component
    {
      id: verbView
      Canvas
      {
        onWidthChanged: requestPaint()
        onHeightChanged: requestPaint()
        property real leftX: 0
        property real leftY
        property real rightX: width
        property real rightY
        property real middleX
        
        onPaint: {
          var ctx = getContext("2d");
          
          ctx.strokeStyle = "black"
          ctx.lineWidth = 1.0

          ctx.beginPath();
          ctx.moveTo(leftX, leftY)
          ctx.lineTo(middleX, leftY)
          ctx.lineTo(middleX, rightY)
          ctx.lineTo(rightX, rightY)
          ctx.stroke()
        }
      }
    }
    ScrollView
    {
      Rectangle
      {
        id: graph_view
        width: graph_view_row.width
        height: graph_view_row.height
        color: "white"
        
        Query
        {
          id: nodeNounQuery
          connection: root.connection
          query: "SELECT ?noun ?verb WHERE { ?noun ?verb <" + root.centralNodeUri + "> . } ORDER BY ?verb"
          autoExecute: true
        }
        Query
        {
          id: nodeObjectQuery
          connection: root.connection
          query: "SELECT ?verb ?object WHERE { <" + root.centralNodeUri + "> ?verb ?object . } ORDER BY ?verb"
          autoExecute: true
        }
        
        Row
        {
          id: graph_view_row
          spacing: 20
          Column
          {
            y: 0.5 * centralNodeView.height
            spacing: 5
            Repeater
            {
              id: nounNodes
              model: nodeNounQuery.result
              Loader
              {
                sourceComponent: nodeView
                onLoaded:
                {
                  item.label = Qt.binding(function () { return noun })
                }
              }
            }
          }
          Column
          {
            spacing: 5+6
            Repeater
            {
              model: nodeNounQuery.result
              Text
              {
                text: verb
              }
            }
          }
          Loader
          {
            id: centralNodeView
            anchors.verticalCenter: parent.verticalCenter
            sourceComponent: nodeView
            onLoaded:
            {
              item.label = Qt.binding(function () { return root.centralNodeUri })
            }
          }
          Column
          {
            spacing: 5+6
            Repeater
            {
              model: nodeObjectQuery.result
              Text
              {
                text: verb
              }
            }
          }
          Column
          {
            id: objectNodesColumn
            spacing: 5
            y: 0.5 * centralNodeView.height
            Repeater
            {
              id: objectNodes
              model: nodeObjectQuery.result
              Loader
              {
                sourceComponent: nodeView
                onLoaded:
                {
                  item.label = Qt.binding(function () { return KnowCore.sparqlVariantToString(object) })
                }
              }
            }
          }
        }
        Repeater
        {
          model: nounNodes.model
          Loader
          {
            sourceComponent: verbView
            onLoaded:
            {
              item.x      = Qt.binding(function() { return nounNodes.itemAt(index).x + nounNodes.itemAt(index).width })
              item.y      = 0
              item.width  = Qt.binding(function() { return centralNodeView.x - item.x })
              item.height = Qt.binding(function() { return graph_view_row.height })

              item.leftY = Qt.binding(function() {  return nounNodes.itemAt(index).y + nounNodes.itemAt(index).height })
              item.rightY  = Qt.binding(function() { return centralNodeView.y + 0.5 * centralNodeView.height })

              item.middleX = Qt.binding(function() { return item.width - 5 })
            }
          }
        }
        Repeater
        {
          model: objectNodes.model
          Loader
          {
            sourceComponent: verbView
            onLoaded:
            {
              item.x      = Qt.binding(function() { return centralNodeView.x + centralNodeView.width })
              item.y      = 0
              item.width  = Qt.binding(function() { return (objectNodes.itemAt(index) ? objectNodes.itemAt(index).x : 0) + objectNodesColumn.x - item.x })
              item.height = Qt.binding(function() { return graph_view_row.height })
              
              item.leftY  = Qt.binding(function() { return centralNodeView.y + 0.5 * centralNodeView.height })
              item.rightY = Qt.binding(function() { return (objectNodes.itemAt(index) ? objectNodes.itemAt(index).y + objectNodes.itemAt(index).height  : 0) })

              item.middleX = 5
            }
          }
        }
      }
    }
  }
}
