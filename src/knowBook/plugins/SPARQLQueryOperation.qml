import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import knowCore
import knowBook.Private.Plugins

BaseQueryOperation
{
  name: "SPARQL Query"
  queryType: KnowCore.Uris.askCoreQueries.SPARQL
  function initialise(operation)
  {
    if(operation.definition["query"] == null)
    {
      operation.definition["query"] = "SELECT ?x ?y ?z WHERE { ?x ?y ?z . };"
      operation.definition["bindings"] = "{}"
      operation.definition["default_dataset"] = "<None>"
    }
  }
  function prepareQuery(query, operation)
  {
    query.query = operation.definition["query"]
    var d_d = operation.definition["default_dataset"]
    query.environment = d_d == "<None>" ? {} : { "default_dataset": d_d }
  }
  optionsComponent: ColumnLayout
  {
    id: _root_
    property var operation
    property var __connection: operation.context.state.connection
    Label
    {
      text: "Default dataset:"
    }
    ComboBox
    {
      id: cb
      model: _root_.__connection ? ["<None>"].concat(_root_.__connection.rdfGraphs) : []
      currentIndex: operation ? model.indexOf(operation.definition["default_dataset"]) : 0
      onActivated: index => operation.definition["default_dataset"] = model[index]
      implicitContentWidthPolicy: ComboBox.WidestText
    }
  }
}
