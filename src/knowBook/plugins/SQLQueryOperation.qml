import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import knowCore
import knowBook.Private.Plugins

BaseQueryOperation
{
  name: "SQL Query"
  queryType: KnowCore.Uris.askCoreQueries.SQL
  function initialise(operation)
  {
    if(operation.definition["query"] == null)
    {
      operation.definition["query"] = "SELECT * FROM :TABLE:"
      operation.definition["bindings"] = "{}"
      operation.definition["table"] = ""
    }
  }
  function prepareQuery(query, operation)
  {
    var table = operation.definition["table"]
    query.query = operation.definition["query"].replace(":TABLE:", table)
  }
  optionsComponent: ColumnLayout
    {
      id: _root_
      property QtObject operation
      property var __connection: operation.context.state.connection

      Label
      {
        text: "Table:"
      }
      Frame
      {
        ListView
        {
          id: _sql_tables_
          anchors.fill: parent
          model: _root_.__connection ? _root_.__connection.sqlTables : []
          clip: true
          currentIndex: operation ? model.indexOf(operation.definition["table"]) : 0
          delegate: Rectangle
          {
            id: _sql_tables_delegate_
            width: _sql_tables_.width
            height: 4 + _sql_tables_delegate_text_.height
            color: _sql_tables_.currentItem == _sql_tables_delegate_ ? "#00BCD4" : "transparent"
            Text
            {
              id: _sql_tables_delegate_text_
              x: 2
              y: 2
              text: modelData
            }
            MouseArea
            {
              anchors.fill: parent
              onClicked:
              {
                _sql_tables_.currentIndex = index
                _root_.operation.definition["table"] = _sql_tables_.model[index]
              }
            }
          } 
        }
        Layout.preferredWidth: 200
        Layout.preferredHeight: 100
      }
    }

}
