import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Shapes

import knowCore
import knowBook
import knowBook.Plugins

import knowBook.NotebookStyle

GridLayout
{
  id: _root_
  columns: 2
  property var operation: {"definition": { "type": "Unknown" }}
  property var availableOperations: []
  signal addOperation(operation: var)
  property bool __collapsed: false
  Shape
  {
    id: _left_bar_
    property int margin: 4
    property int leftX: _operation_tools_.width * 0.5
    property int topY: margin
    property int rightX: width - margin
    property int bottomY: height - margin
    ShapePath
    {
      strokeColor: "black"
      fillColor: "transparent"
      startX: _left_bar_.rightX
      startY: _left_bar_.topY
      PathLine
      {
        x: _left_bar_.leftX
        y: _left_bar_.topY
      }
      PathLine
      {
        x: _left_bar_.leftX
        y: _left_bar_.bottomY
      }
      PathLine
      {
        x: _left_bar_.rightX
        y: _left_bar_.bottomY
      }
    }
    ColumnLayout
    {
      id: _operation_tools_
      y: 0.5 * _operation_tools_.width
      ToolButton
      {
        icon.source: _root_.__collapsed ? "qrc:/data/icons/expand.png" : "qrc:/data/icons/collapse.png"
        icon.color: "black"
        highlighted: true
        onClicked: _root_.__collapsed = !_root_.__collapsed
      }
      ToolButton
      {
        icon.source: "qrc:/data/icons/execute.png"
        icon.color: "black"
        visible: (!_root_.__collapsed && operation.plugin.execute != undefined)
        enabled: _operation_component_loader_.item.enabled
        highlighted: true
        onClicked:
        {
          var t1 = KnowCore.now()
          var t2 = KnowCore.now()
          operation.plugin.execute(_root_.operation)
          operation.localState.set("executionTime", t2 - t1)
        }
      }
      Repeater
      {
        model: _operation_component_loader_.item && _operation_component_loader_.item.actions ? _operation_component_loader_.item.actions : []
        ToolButton
        {
          action: modelData
          enabled: _operation_component_loader_.item.enabled
          highlighted: true
          display: AbstractButton.IconOnly
          visible: !_root_.__collapsed
        }
      }
      ToolButton
      {
        icon.source: "qrc:/data/icons/add.png"
        onClicked: _add_operation_menu_.open()
        enabled: _root_.availableOperations.length > 0
        highlighted: true
        Menu
        {
          x: parent.width
          y: 0
          id: _add_operation_menu_
          Instantiator {
            model: _root_.availableOperations
            MenuItem {
              text: modelData
              onTriggered: _root_.addOperation({ "type": modelData, "deletable": true })
            }
            onObjectAdded: (index, object) => _add_operation_menu_.insertItem(index, object)
            onObjectRemoved: object => _add_operation_menu_.removeItem(object)
          }
        }
      }
    }
    Layout.preferredWidth: _operation_tools_.x + _operation_tools_.width + margin
    Layout.minimumHeight: _operation_tools_.height + _operation_tools_.width
    Layout.fillHeight: true
    Layout.rowSpan: 3
  }
  Label
  {
    text: _root_.operation.definition["type"]
  }
  Loader
  {
    id: _operation_component_loader_
    visible: !_root_.__collapsed
    sourceComponent: _root_.operation.plugin.operationComponent
    onLoaded: {
      item.operation = _root_.operation
    }
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
  Label
  {
    id: _status_
    visible: text != ""
    wrapMode: Text.WordWrap
    function __make_status_text(errorMessage, statusMessage, executionTime)
    {
      color = "black"
      if(errorMessage && errorMessage != "")
      {
        color = "red"
        return errorMessage
      }
      var txt = ""
      if(statusMessage)
      {
        txt += statusMessage
      }
      if(executionTime)
      {
        txt += " Executed in " + executionTime + "ms."
      }
      return txt
    }
    text: __make_status_text(operation.localState.errorMessage, operation.localState.statusMessage, operation.localState.executionTime)
  }
}
