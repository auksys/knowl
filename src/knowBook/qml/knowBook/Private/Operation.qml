import QtQuick
import knowBook
import knowBook.Plugins

QtObject
{
  id: _operation_
  property var definition
  property State localState: State {}
  property Context context
  property PluginsManager pluginsManager
  property OperationPlugin plugin
  Component.onCompleted:
  {
    plugin = pluginsManager.operationPluginFor(definition["type"])
    if(plugin)
    {
      plugin.initialise(_operation_)
    } else {
      plugin = _unknown_type_
    }
    localState.set("statusMessage", null)
    localState.set("errorMessage", null)
    localState.set("executionTime", null)
  }
}
