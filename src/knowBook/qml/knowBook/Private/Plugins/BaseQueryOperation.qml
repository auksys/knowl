import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs
import QtQuick.Layouts

import Cyqlops.Controls

import knowCore
import knowDBC
import knowBook.Controls

import knowBook
import knowBook.Plugins

import knowBook.NotebookStyle

import Qt.labs.qmlmodels
OperationPlugin
{
  id: _root_plugin_
  property string queryType
  function visible(context) { return context.state.connection ? context.state.connection.supportQuery(_root_plugin_.queryType) : false; }
  function execute(operation)
  {
    var q = operation.localState.query
    if(!q)
    {
      q = __query_component.createObject(operation, { "operation": operation })
      operation.localState.set("query", q)
    }
    _root_plugin_.prepareQuery(q, operation)
    q.bindings = JSON.parse(operation.definition["bindings"])
    q.execute()
    var result = q.result 
    if(q.lastError == "")
    {
      operation.localState.set("statusMessage",
        result ? result.rowCount() + " rows." : "No query result.")
    } else {
      operation.localState.set("statusMessage", "<font color='red'><b>Error:</b> " + q.lastError + "</font>")
    }
  }
  property Component __query_component: Component
  {
    Query
    {
      id: _query_
      property QtObject operation
      queryType: _root_plugin_.queryType
      connection: operation.context.state.connection.connection
      onLastErrorChanged: root.operation.localState.set("errorMessage", lastError)
    }
  }
  property Component optionsComponent
  property bool supportBindings: true
  operationComponent: ColumnLayout
  {
    id: root
    property QtObject operation
    property var __connection: operation.context.state.connection
    enabled: __connection && __connection.connected
    property var actions: [_export_action_, _import_action_]
    MessageDialog
    {
      id: save_to_json_failed
      title: "Saving to JSON failed"
      text: "Saving to JSON has failed.";
      buttons: MessageDialog.Ok
    }
    FileDialog
    {
      id: saveJSONDialog
      title: "Save to JSON"
      nameFilters: [ "JSON files (*.json)" ]
      fileMode: FileDialog.SaveFile
      onAccepted: {
        if(!root.operation.localState.query.saveToJSON(selectedFile))
        {
          save_to_json_failed.open()
        }
      }
    }
    MessageDialog
    {
      id: load_from_json_failed
      title: "Loading from JSON failed"
      text: "Loading from JSON has failed.";
      buttons: MessageDialog.Ok
    }
    FileDialog
    {
      id: loadJSONDialog
      title: "Load from JSON"
      nameFilters: [ "JSON files (*.json)" ]
      onAccepted: {
        if(root.operation.localState.query.loadFromJSON(selectedFile))
        {
        } else
        {
          load_from_json_failed.open()
        }
      }
    }
    Action
    {
      id: _export_action_
      icon.source: "qrc:/data/icons/export.png"
      enabled: root.operation.localState.query
      text: "Save to JSON"
      onTriggered: saveJSONDialog.open()
    }
    Action
    {
      id: _import_action_
      icon.source: "qrc:/data/icons/import.png"
      enabled: root.operation.localState.query
      text: "Load from JSON"
      onTriggered: loadJSONDialog.open()
    }
    RowLayout
    {
      Layout.fillWidth: true
      Layout.fillHeight: true
      Loader
      {
        sourceComponent: _root_plugin_.optionsComponent
        Layout.fillHeight: true
        onLoaded: item.operation = Qt.binding(() => root.operation)
      }
      ColumnLayout
      {
        Label
        {
          text: "Query:"
        }
        TextArea
        {
          id: sqlQueryEditor
          Layout.fillWidth: true
          Layout.fillHeight: true
          text: operation.definition["query"]
          onTextChanged: operation.definition["query"] = text
          background: Rectangle { color: "#EEEEEE" }
        }
      }
      ColumnLayout
      {
        Label
        {
          text: "Bindings:"
        }
        TextArea
        {
          id: bindingsEditor
          text: operation.definition["bindings"]
          onTextChanged: operation.definition["bindings"] = text
          background: Rectangle { color: "#EEEEEE" }
          Layout.fillWidth: true
          Layout.fillHeight: true
        }
        visible: _root_plugin_.supportBindings
      }
    }
    TableView
    {
      id: resultsTable
      horizontalHeaderModel: listToTableModel.columnNames
      horizontalHeaderVisible: true
      model: ListToTableItemModel
      {
        id: listToTableModel
        sourceModel: root.operation.localState.query ? root.operation.localState.query.result : null
      }
      Layout.minimumHeight: resultsTable.contentHeight
      Layout.fillWidth: true
      Layout.fillHeight: true
    }
  }
}
