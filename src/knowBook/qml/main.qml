import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import knowBook
import knowBook.Plugins
import knowBook.Private
import knowBook.Private.Controls

ApplicationWindow {
  id: _root_
  title: qsTr("knowBook")
  width: 640
  height: 480
  visible: true

  background: Rectangle
  {
    color: "white"
  }

  property real __update_operations: 0
  property var __availableOperations: __f_availableOperations(_pluginsManager_, _context_, __update_operations)
  function __f_availableOperations(_plugins_manager, _context)
  {
    return _pluginsManager_.operationPlugins.filter(x => x.visible(_context)).map(x => x.name)
  }

  Connections
  {
    target: _context_.state
    function onStateChanged() { _root_.__update_operations += 1 }
  }
  Connections
  {
    target: _context_.state.connection ? _context_.state.connection : null
    ignoreUnknownSignals: true
    function onConnectionStatusChanged() { _root_.__update_operations += 1 }
  }

  OperationPlugin
  {
    id: _unknown_type_
    operationComponent: Text
    {
      property QtObject operation
      text: "Operation of type '" + operation.definition["type"] + "' is not supported."
    }
  }
  Component
  {
    id: _operation_object_
    Operation
    {
      pluginsManager: _pluginsManager_
      context: _context_
    }
  }
  function __create_operation(definition)
  {
    return _operation_object_.createObject(_root_, { "definition": definition })
  }
  PluginsManager
  {
    id: _pluginsManager_
    onOperationPluginsChanged: {
      if(_pluginsManager_.operationPluginFor("Connection Setup") != null && _root_.__operations.length == 0)
      {
       _root_.__operations = [__create_operation({"type": "Connection Setup", "deletable": false, "runnable": false })]
      }
    }
  }
  Context
  {
    id: _context_
  }
  property var __operations: []

  ScrollView
  {
    id: _scroll_view_
    anchors.fill: parent
    ColumnLayout
    {
      width: _scroll_view_.width
      Repeater
      {
        model: _root_.__operations
        OperationContainer
        {
          operation: modelData
          availableOperations: _root_.__availableOperations
          onAddOperation: operation => {
            var ops = _root_.__operations
            ops.push(_root_.__create_operation(operation))
            _root_.__operations = ops // .unshift(operation)
          }
        }
        // context: _context_
      }
    }
    ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
    ScrollBar.vertical.policy: ScrollBar.AlwaysOn
  }
}