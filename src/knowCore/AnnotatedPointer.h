#pragma once

#include <tuple>

namespace knowCore
{
  namespace details
  {
    template<typename _T_, typename... _TOther_>
    struct make_default
    {
      static std::tuple<typename _T_::Type, typename _TOther_::Type...> create()
      {
        return std::tuple_cat(std::tuple<typename _T_::Type>(_T_::Default),
                              make_default<_TOther_...>::create());
      }
    };
    template<typename _T_>
    struct make_default<_T_>
    {
      static std::tuple<typename _T_::Type> create()
      {
        return std::tuple<typename _T_::Type>(_T_::Default);
      }
    };
  } // namespace details
  /**
   * @ingroup knowCore
   *
   * This class is used to define an \ref AnnotatedPointer. So that \p _T_ is the a type of
   * annotation and \p _default_ is the default value.
   */
  template<typename _T_, _T_ _default_>
  struct PointerAnnotation
  {
    typedef _T_ Type;
    static constexpr _T_ Default = _default_;
  };
  /**
   * @ingroup knowCore
   *
   * Add annotation to a pointer.
   *
   * Example of use:
   *
   * The following create a new type of annotation to a pointer, which is a bool with a default true
   * value. The intent is that if the annotation is true the pointer ownership is transfer to the
   * called function.
   *
   * \code
   * template<typename _T_>
   * using TransferOwnershipAnnotation = knowCore::AnnotatedPointer<_T_,
   * knowCore::PointerAnnotation<bool, true>>; \endcode
   *
   * This annotation can be used like this:
   *
   * \code
   * class Monitor;
   * using AnnotatedMonitor = TransferOwnershipAnnotation<Monitor>
   * \endcode
   *
   * And then a new instance can be created:
   *
   * \code
   * AnnotatedMonitor am1(new Monitor, true);
   * AnnotatedMonitor am2(new Monitor, false);
   *
   * if(am1.get<0>()) delete *am1;
   * if(am2.get<0>()) delete *am2;
   * \endcode
   *
   */
  template<typename _T_, typename... _Annotation_>
  class AnnotatedPointer
  {
    typedef std::tuple<typename _Annotation_::Type...> AnnotationsT;
  public:
    AnnotatedPointer(_T_* _t, typename _Annotation_::Type... _annotations)
        : t(_t), m_annotations(_annotations...)
    {
    }
    AnnotatedPointer(_T_* _t)
        : t(_t), m_annotations(details::make_default<_Annotation_...>::create())
    {
    }
    _T_* pointer() { return t; }
    _T_* operator*() { return t; }
    const _T_* operator*() const { return t; }
    std::tuple<typename _Annotation_::Type...> annotations() const { return m_annotations; }
    template<int _index_>
    std::tuple_element_t<_index_, AnnotationsT> get() const
    {
      return std::get<_index_>(m_annotations);
    }
  public:
  private:
    _T_* t;
    AnnotationsT m_annotations;
  };
} // namespace knowCore
