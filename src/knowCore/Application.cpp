#include "Application.h"

#include <thread>

#include <QCoreApplication>
#include <QMutex>
#include <QWaitCondition>

#include <clog_qt>

using namespace knowCore;

struct Application::Private
{
  ~Private()
  {
    clog_assert(app);
    app->exit();
    app = nullptr;
    t.join();
  }
  QMutex m;
  QCoreApplication* app = nullptr;
  std::thread t;
};

void Application::start()
{
  static Private p;
  if(not p.app)
  {
    QMutexLocker l(&p.m);
    if(not p.app)
    {
      QMutex* mw = new QMutex;
      mw->lock();
      QWaitCondition* wc = new QWaitCondition;
      p.t = std::thread(
        [&mw, &wc]()
        {
          int argc = 0;
          p.app = new QCoreApplication(argc, nullptr);
          mw->lock();
          wc->wakeAll();
          mw->unlock();
          p.app->exec();
        });
      wc->wait(mw);
      mw->unlock();
      delete mw;
      delete wc;
      mw = nullptr;
      wc = nullptr;
    }
  }
}
