namespace knowCore
{
  /**
   * Start a QCoreApplication in a seperate thread.
   */
  class Application
  {
    Application() = delete;
    ~Application() = delete;
  public:
    static void start();
  private:
    struct Private;
  };
} // namespace knowCore
