#ifndef _KNOW_CORE_ARRAY_H_
#define _KNOW_CORE_ARRAY_H_

#include <clog_qt>
#include <cres_qt>

#include <QVector>

#include "Global.h"
#include "Vector.h"

namespace knowCore
{
  template<typename _T_>
  class Array
  {
  public:
    Array() {}
    explicit Array(const QList<qsizetype>& _dimensions) : m_dimensions(_dimensions)
    {
      m_data.resize(totalSize());
    }
    Array(const std::initializer_list<qsizetype>& _il) : Array(QList<qsizetype>(_il)) {}
    Array(const QVector<_T_>& _data, const QList<qsizetype>& _dimensions)
        : m_data(_data), m_dimensions(_dimensions)
    {
      clog_assert(m_data.size() == totalSize());
    }
    template<int _v_dimension_>
    cres_qresult<Vector<_T_, _v_dimension_>> toVector() const
    {
      if(m_dimensions.size() == 1 and m_dimensions[0] == _v_dimension_)
      {
        Vector<_T_, _v_dimension_> v;
        v.copyFrom(m_data);
        return cres_success(v);
      }
      else
      {
        return cres_failure(
          "Invalid conversion from array of dimensions {} to vector of dimension {}", m_dimensions,
          _v_dimension_);
      }
    }
    template<typename _RList_ = QList<_T_>>
    cres_qresult<_RList_> toList() const
    {
      switch(m_dimensions.size())
      {
      case 0:
        return cres_success(QList<_T_>());
      case 1:
        return cres_success(m_data.toList());
      default:
        return cres_failure("Invalid conversion from array of dimensions {} to list.",
                            m_dimensions);
      }
    }
    template<int _v_dimension_>
    cres_qresult<QList<Vector<_T_, _v_dimension_>>> toVectorList() const
    {
      if(m_dimensions.size() == 2)
      {
        if(m_dimensions[1] != _v_dimension_)
        {
          return cres_failure(
            "Invalid conversion from array of dimensions {} to a list of vectors of dimension {}",
            m_dimensions, _v_dimension_);
        }
        QList<Vector<_T_, _v_dimension_>> result;
        for(int i = 0; i < m_dimensions.size(); ++i)
        {
          Vector<_T_, _v_dimension_> v;
          for(int k = 0; k < _v_dimension_; ++k)
          {
            v[k] = m_data[i * _v_dimension_ + k];
          }
          result.append(v);
        }
        return cres_success(result);
      }
      else
      {
        return cres_failure(
          "Invalid conversion from array of dimensions {} to a list of vectors of dimension {}",
          m_dimensions, _v_dimension_);
      }
    }
    bool operator==(const Array<_T_>& _rhs) const
    {
      return m_dimensions == _rhs.m_dimensions and m_data == _rhs.m_data;
    }
    void fill(const _T_& _v) { m_data.fill(_v); }
    typename QVector<_T_>::iterator begin() { return m_data.begin(); }
    typename QVector<_T_>::iterator end() { return m_data.end(); }
    typename QVector<_T_>::const_iterator begin() const { return m_data.begin(); }
    typename QVector<_T_>::const_iterator end() const { return m_data.end(); }
    QVector<_T_> data() const { return m_data; }
    QList<qsizetype> dimensions() const { return m_dimensions; }
    template<std::convertible_to<int>... _Args_>
    _T_& operator()(_Args_... _indices)
    {
      clog_assert(sizeof...(_Args_) == m_dimensions.size());
      return m_data[make_index(0, _indices...)];
    }
  private:
    template<typename _Arg1_, typename _Arg2_, typename... _Args_>
    int make_index(int _level, _Arg1_ _i, _Arg2_ j, _Args_... _indices)
    {
      return make_index(_level + 1, j, _indices...) * m_dimensions[_level] + _i;
    }
    int make_index(int, int _i) { return _i; }
    qsizetype totalSize() const
    {
      int ts = 1;
      for(int d : m_dimensions)
      {
        ts *= d;
      }
      return ts;
    }
  public:
    QVector<_T_> m_data;
    QList<qsizetype> m_dimensions;
  };
} // namespace knowCore

#endif
