#pragma once

#include <functional>
#include <type_traits>

#include <QObject>

class QThread;

namespace knowCore
{
  namespace details
  {
    class AsynchronousFunctionCall : public QObject
    {
      Q_OBJECT
    public slots:
      void call(void* arg) { m_function(arg); }
      void setFunction(const std::function<void(void*)>& _function) { m_function = _function; }
    private:
      std::function<void(void*)> m_function;
    };
  } // namespace details
  /**
   * Execute a std::function inside a QThread using a QueuedConnection slot
   */
  template<typename _TRet_, typename... _Args_>
  class AsynchronousFunction
  {
    typedef std::tuple<std::decay_t<_Args_>...> ArgsTuple;
  public:
    AsynchronousFunction(const std::function<_TRet_(_Args_...)>& _function, QThread* _thread,
                         const std::function<void(const _TRet_&)>& _ret_handler)
        : m_call(new details::AsynchronousFunctionCall)
    {
      m_call->moveToThread(_thread);
      m_call->setFunction(
        [_function, _ret_handler](void* _args)
        {
          ArgsTuple* args = reinterpret_cast<ArgsTuple*>(_args);
          _ret_handler(std::apply(_function, *args));
          delete args;
        });
    }
    ~AsynchronousFunction() { delete m_call; }
    /**
     * Call the function asynchronously
     */
    void operator()(_Args_... _args)
    {
      ArgsTuple* t
        = new ArgsTuple(/*std::make_tuple<details::remove_cvref<_Args_...>>(*/ _args...) /*)*/;
      QMetaObject::invokeMethod(m_call, "call", Qt::QueuedConnection, Q_ARG(void*, t));
    }
  private:
    details::AsynchronousFunctionCall* m_call;
  };
  template<typename... _Args_>
  class AsynchronousFunction<void, _Args_...>
  {
    typedef std::tuple<std::decay_t<_Args_>...> ArgsTuple;
  public:
    AsynchronousFunction(const std::function<void(_Args_...)>& _function, QThread* _thread)
        : m_call(new details::AsynchronousFunctionCall)
    {
      m_call->moveToThread(_thread);
      m_call->setFunction(
        [_function](void* _args)
        {
          ArgsTuple* args = reinterpret_cast<ArgsTuple*>(_args);
          std::apply(_function, *args);
          delete args;
        });
    }
    ~AsynchronousFunction() { delete m_call; }
    /**
     * Call the function asynchronously
     */
    void operator()(_Args_... _args)
    {
      ArgsTuple* t
        = new ArgsTuple(/*std::make_tuple<details::remove_cvref<_Args_...>>(*/ _args...) /*)*/;
      QMetaObject::invokeMethod(m_call, "call", Qt::QueuedConnection, Q_ARG(void*, t));
    }
  private:
    details::AsynchronousFunctionCall* m_call;
  };
} // namespace knowCore
