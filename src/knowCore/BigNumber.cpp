#include "BigNumber.h"

#include <cfloat>
#include <cmath>

#include <QCryptographicHash>
#include <QVariant>

#include "Numeric_p.h"
#include "TypeDefinitions.h"
#include "Value.h"

using namespace knowCore;

BigNumber::BigNumber() : m_weight(0), m_dscale(0), m_sign(Sign::Positive) {}

BigNumber::BigNumber(const BigNumber& _rhs)
    : m_weight(_rhs.m_weight), m_dscale(_rhs.m_dscale), m_sign(_rhs.m_sign), m_digits(_rhs.m_digits)
{
}

BigNumber::BigNumber(BigNumber&& _rhs)
    : m_weight(_rhs.m_weight), m_dscale(_rhs.m_dscale), m_sign(_rhs.m_sign),
      m_digits(std::move(_rhs.m_digits))
{
}

BigNumber& BigNumber::operator=(const BigNumber& _rhs)
{
  m_weight = _rhs.m_weight;
  m_dscale = _rhs.m_dscale;
  m_sign = _rhs.m_sign;
  m_digits = _rhs.m_digits;
  return *this;
}

BigNumber::BigNumber(qint16 weight, BigNumber::Sign sign, qint16 dscale,
                     const QList<quint16>& digits)
    : m_weight(weight), m_dscale(dscale), m_sign(sign), m_digits(digits)
{
}

BigNumber::~BigNumber() {}

BigNumber::BigNumber(std::intmax_t _value) : BigNumber()
{
  Sign s = Sign::Positive;
  if(_value < 0)
  {
    s = Sign::Negative;
    _value = -_value;
  }
  *this = BigNumber(std::uintmax_t(_value));
  m_sign = s;
}

BigNumber::BigNumber(std::uintmax_t _value) : BigNumber()
{
  while(_value > 0)
  {
    m_digits.prepend(_value % 10000);
    _value = _value / 10000;
  }
  m_weight = m_digits.size() - 1;
  simplify();
}

double frexp10(double arg, int* exp)
{
  *exp = (arg == 0) ? 0 : (int)(1 + std::log10(std::fabs(arg)));
  return arg * pow(10, -(*exp));
}

BigNumber::BigNumber(double _value) : BigNumber()
{
  if(std::isnan(_value))
  {
    m_sign = Sign::NaN;
  }
  else if(std::isfinite(_value))
  {
    *this = BigNumber::fromString(QString::number(_value, 'g', DBL_DIG)).expect_success();
  }
  else if(_value > 0)
  {
    m_sign = Sign::PositiveInfinite;
  }
  else
  {
    m_sign = Sign::NegativeInfinite;
  }
}

cres_qresult<BigNumber> BigNumber::fromString(const QString& _value)
{
  BigNumber r;
  if(_value == "NaN")
  {
    r.m_sign = Sign::NaN;
  }
  else if(_value == "Inf" or _value == "+Inf")
  {
    r.m_sign = Sign::PositiveInfinite;
  }
  else if(_value == "-Inf")
  {
    r.m_sign = Sign::NegativeInfinite;
  }
  else
  {
    QStringView str = _value;
    const int exp_index = _value.indexOf("e");
    int exp = 0;
    if(exp_index >= 0)
    {
      exp = str.right(str.size() - exp_index - 1).toInt();
      str = str.left(exp_index);
    }

    if(str.at(0) == '-')
    {
      r.m_sign = Sign::Negative;
      str = str.right(str.size() - 1);
    }
    else if(str.at(0) == '+')
    {
      str = str.right(str.size() - 1);
    }
    const int point_index = str.indexOf('.');
    QStringView left_part, right_part;
    if(point_index < 0)
    {
      left_part = str;
      r.m_dscale = 0;
    }
    else
    {
      left_part = str.left(point_index);
      right_part = str.right(str.size() - point_index - 1);
      r.m_dscale = right_part.length();
    }
    // Generate left part of the number
    for(int i = 0; i < left_part.size(); i += 4)
    {
      if(i + 4 > left_part.size())
      {
        r.m_digits.prepend(left_part.left(left_part.size() - i).toUInt());
      }
      else
      {
        r.m_digits.prepend(left_part.mid(left_part.size() - i - 4, 4).toUInt());
      }
    }
    // Generate right part of the number
    r.m_weight = r.m_digits.size() - 1;
    for(int i = 0; i < right_part.size(); i += 4)
    {
      if(i + 4 > right_part.size())
      {
        static quint16 multipliers[3] = {1000, 100, 10};
        int s = right_part.size() - i;
        r.m_digits.append(right_part.right(s).toUInt() * multipliers[s - 1]);
      }
      else
      {
        r.m_digits.append(right_part.mid(i, 4).toUInt());
      }
    }
    r.exponenfy(exp);
  }
  r.simplify();
  return cres_success(r);
}

cres_qresult<BigNumber> BigNumber::fromVariant(const QVariant& _value)
{
  if(_value.userType() == MetaTypeInformation<BigNumber>::id())
  {
    return cres_success(_value.value<BigNumber>());
  }
  switch(_value.typeId())
  {
  case QVariant::Int:
    return cres_success(BigNumber(_value.toInt()));
  case QVariant::UInt:
    return cres_success(BigNumber(_value.toUInt()));
  case QVariant::LongLong:
    return cres_success(BigNumber(_value.toLongLong()));
  case QVariant::ULongLong:
    return cres_success(BigNumber(_value.toULongLong()));
  case QVariant::Double:
    return cres_success(BigNumber(_value.toDouble()));
  case QVariant::String:
    return fromString(_value.toString());
  default:
    return cres_failure("Variant is not a number {}", _value);
  }
}

cres_qresult<BigNumber> BigNumber::fromValue(const knowCore::Value& _value)
{
  if(ValueIs<QString> str = _value)
  {
    return fromString(str);
  }
  else if(ValueCast<BigNumber> bn = _value)
  {
    return cres_success(bn);
  }
  else
  {
    return cres_failure("Value is not a number {}", _value);
  }
}

BigNumber BigNumber::nan()
{
  BigNumber n;
  n.m_sign = Sign::NaN;
  return n;
}

BigNumber BigNumber::positiveInfinite()
{
  BigNumber n;
  n.m_sign = Sign::PositiveInfinite;
  return n;
}

BigNumber BigNumber::negativeInfinite()
{
  BigNumber n;
  n.m_sign = Sign::NegativeInfinite;
  return n;
}

BigNumber BigNumber::zero() { return BigNumber(); }

bool BigNumber::isNaN() const { return m_sign == Sign::NaN; }

bool BigNumber::isFinite() const { return m_sign == Sign::Positive or m_sign == Sign::Negative; }

bool BigNumber::isInfinite() const
{
  return m_sign == Sign::PositiveInfinite or m_sign == Sign::NegativeInfinite;
}

bool BigNumber::isFloating() const { return m_dscale > 0; }

double BigNumber::toDouble() const
{
  double r = 0.0;
  for(int i = 0; i < std::min<qint16>(m_digits.size(), m_weight + 1); ++i)
  {
    r = r * 10000 + m_digits[i];
  }
  int diff = m_weight - m_digits.size() + 1;
  while(diff > 0)
  {
    r *= 10000;
    --diff;
  }
  double power = 1e-4;
  for(int i = m_weight + 1; i < m_digits.size(); ++i)
  {
    if(i >= 0)
    {
      r = r + m_digits[i] * power;
    }
    power *= 1e-4;
  }
  switch(m_sign)
  {
  case Sign::NaN:
    return std::numeric_limits<double>::quiet_NaN();
  case Sign::NegativeInfinite:
    return -std::numeric_limits<double>::infinity();
  case Sign::PositiveInfinite:
    return std::numeric_limits<double>::infinity();
  case Sign::Negative:
    return -r;
  case Sign::Positive:
    return r;
  }
  qFatal("should not happen");
}

cres_qresult<qint64> BigNumber::toInt64(bool _truncate) const
{
  if(m_dscale > 0 and not _truncate)
  {
    return cres_failure("Floating point number");
  }
  auto [overflow, value] = toUInt64_ignore_sign();
  if(overflow)
  {
    return cres_failure("overflow");
  }
  switch(m_sign)
  {
  case Sign::NaN:
    return cres_failure("Not a number");
  case Sign::NegativeInfinite:
    return cres_failure("Negative infinite number");
  case Sign::PositiveInfinite:
    return cres_failure("Positive infinite number");
  case Sign::Negative:
  {
    if(value > quint64(-(std::numeric_limits<qint64>::min() + 1)) + 1)
    {
      return cres_failure("overflow");
    }
    else
    {
      return cres_success(-(qint64)value);
    }
  }
  case Sign::Positive:
    if(value > quint64(std::numeric_limits<qint64>::max()))
    {
      return cres_failure("overflow");
    }
    else
    {
      return cres_success<qint64>(value);
    }
  }
  return cres_failure("undefined");
}

cres_qresult<quint64> BigNumber::toUInt64(bool _truncate) const
{
  if(m_dscale > 0 and not _truncate)
  {
    return cres_failure("Floating point number");
  }
  switch(m_sign)
  {
  case Sign::NaN:
    return cres_failure("Not a number");
  case Sign::Negative:
    return cres_failure("Negative number");
  case Sign::NegativeInfinite:
    return cres_failure("Negative infinite number");
  case Sign::PositiveInfinite:
    return cres_failure("Positive infinite number");
  case Sign::Positive:
  {
    auto [overflow, value] = toUInt64_ignore_sign();
    if(overflow)
    {
      return cres_failure("overflow");
    }
    else
    {
      return cres_success(value);
    }
  }
  }
  return cres_failure("undefined");
}

std::tuple<bool, quint64> BigNumber::toUInt64_ignore_sign() const
{
  quint64 r = 0;
  for(int i = 0; i < std::min<qint16>(m_digits.size(), m_weight + 1); ++i)
  {
    //     r = r * 10000 + m_digits[i];
    auto [overflow, value_mul] = mul_overflow(r, 10000);
    if(overflow)
    {
      return {true, 0};
    }
    else
    {
      auto [overflow, value_add] = add_overflow(value_mul, m_digits[i]);
      if(overflow)
      {
        return {true, 0};
      }
      else
      {
        r = value_add;
      }
    }
  }
  int diff = m_weight + 1 - m_digits.size();
  while(diff > 0)
  {
    //     r *= 10000;
    auto [overflow, value_mul] = mul_overflow(r, 10000);
    if(overflow)
    {
      return {true, 0};
    }
    else
    {
      r = value_mul;
    }
    --diff;
  }
  return {false, r};
}

QString BigNumber::toString() const
{
  QString string;
  switch(m_sign)
  {
  case Sign::NaN:
    string = "NaN";
    break;
  case Sign::PositiveInfinite:
    string = "+Inf";
    break;
  case Sign::NegativeInfinite:
    string = "-Inf";
    break;
  case Sign::Negative:
    string = '-';
    Q_FALLTHROUGH();
  case Sign::Positive:
  {
    int string_ndigits = 0; // digits after 0 added to string
    if(m_digits.size() == 0)
    {
      string += '0';
      if(m_dscale > 0)
      {
        string += ".";
      }
    }
    else
    {
      int cw = m_weight;
      if(cw < 0)
      {
        string += "0.";
        for(int i = -1; i > cw; --i)
        {
          string += "0000";
          string_ndigits += 4;
        }
      }
      bool first = true;
      for(quint16 dig : m_digits)
      {
        if(first and cw >= 0)
        {
          string += QString::number(dig);
        }
        else
        {
          string += clog_qt::qformat("{:04}", dig);
        }
        first = false;
        --cw;
        if(cw == -1 and m_dscale > 0)
        {
          string += ".";
        }
        else if(cw < -1)
        {
          string_ndigits += 4;
        }
      }
      while(cw >= 0)
      {
        string += "0000";
        --cw;
        if(cw == -1 and m_dscale > 0)
        {
          string += ".";
        }
      }
    }
    if(string_ndigits > m_dscale)
    {
      string.chop(string_ndigits - m_dscale);
    }
    else
    {
      for(int i = string_ndigits; i < m_dscale; ++i)
      {
        string += '0';
      }
    }
    break;
  }
  }

  return string;
}

template<typename _T_>
_T_ BigNumber::toVariantValue() const
{
  switch(m_sign)
  {
  case Sign::NaN:
    return _T_::fromValue(std::numeric_limits<double>::quiet_NaN());
  case Sign::NegativeInfinite:
    return _T_::fromValue(-std::numeric_limits<double>::infinity());
  case Sign::PositiveInfinite:
    return _T_::fromValue(std::numeric_limits<double>::infinity());
  case Sign::Negative:
  {
    const auto [int_valid, int_value, errorMessage] = toInt64();
    Q_UNUSED(errorMessage);
    if(int_valid)
    {
      return _T_::fromValue(int_value.value());
    }
    break;
  }
  case Sign::Positive:
  {
    const auto [uint_valid, uint_value, errorMessage] = toUInt64();
    Q_UNUSED(errorMessage);
    if(uint_valid)
    {
      return _T_::fromValue(uint_value.value());
    }
    break;
  }
  }
  if(m_digits.size() <= 4) // ie 16 significant digits
  {
    double dbl = toDouble();
    return _T_::fromValue(dbl);
  }
  else
  {
    return _T_::fromValue(*this);
  }
}

QVariant BigNumber::toVariant() const { return toVariantValue<QVariant>(); }

Value BigNumber::toValue() const { return toVariantValue<Value>(); }

bool BigNumber::operator==(const BigNumber& _rhs) const
{
  return ((m_sign == Sign::NaN or m_sign == Sign::PositiveInfinite
           or m_sign == Sign::NegativeInfinite)
          and m_sign == _rhs.m_sign)
         or (m_weight == _rhs.m_weight and
             /* m_dscale == _rhs.m_dscale and */ m_sign == _rhs.m_sign
             and m_digits == _rhs.m_digits); // dscale is purely for display purposes and indicate
                                             // the number of 0 to add at the end of the string
}

QByteArray BigNumber::md5() const
{
  QCryptographicHash hash_md5(QCryptographicHash::Md5);
  hash(&hash_md5);
  return hash_md5.result();
}

void BigNumber::hash(QCryptographicHash* _hash) const
{
  _hash->addData(QByteArrayView(reinterpret_cast<const char*>(&m_weight), sizeof(quint16)));
  _hash->addData(QByteArrayView(reinterpret_cast<const char*>(&m_dscale), sizeof(quint16)));
  char s = char(m_sign);
  _hash->addData(QByteArrayView(&s, sizeof(quint8)));
  for(quint16 d : m_digits)
  {
    _hash->addData(QByteArrayView(reinterpret_cast<const char*>(&d), sizeof(quint16)));
  }
}

void BigNumber::simplify()
{
  while(not m_digits.isEmpty())
  {
    if(m_digits.front() != 0)
    {
      break;
    }
    else
    {
      m_digits.pop_front();
      --m_weight;
    }
  }
  while(not m_digits.isEmpty())
  {
    if(m_digits.back() != 0)
    {
      break;
    }
    else
    {
      m_digits.pop_back();
    }
  }
  if(m_digits.isEmpty())
  {
    m_weight = 0;
    m_dscale = 0;
  }
}

void BigNumber::exponenfy(int exp)
{
  // update dscale
  m_dscale -= exp;
  if(m_dscale < 0)
  {
    m_dscale = 0;
  }
  // update weight
  auto d = std::div(exp, 4);
  m_weight += d.quot;
  // shift numbers
  if(d.rem != 0)
  {
    qint32 coefs[3] = {10, 100, 1000};
    if(exp > 0)
    {
      qint32 coef = coefs[d.rem - 1];
      qint32 r = 0;
      for(int i = m_digits.size() - 1; i >= 0; --i)
      {
        qint32 v = m_digits[i] * coef + r;
        auto d = std::div(v, 1000);
        m_digits[i] = d.rem;
        r = d.quot;
      }
      if(r > 0)
      {
        m_digits.prepend(r);
        m_weight += 1;
      }
    }
    else
    {
      qint32 coef = coefs[-d.rem - 1];
      qint32 anticoef = coefs[3 + d.rem];
      qint32 r = 0;
      for(int i = 0; i < m_digits.size(); ++i)
      {
        auto d = std::div(m_digits[i], coef);
        m_digits[i] = d.quot + anticoef * r;
        r = d.rem;
      }
      if(r > 0)
      {
        m_digits.append(r * anticoef);
      }
    }
  }
}

#include "BigNumberMetaTypeImplementation_p.h"
#include "Uris/xsd.h"

KNOWCORE_DEFINE_CONVERT(BigNumber, _from, QString, _to, Safe)
{
  *_to = _from->toString();
  return cres_success();
}

KNOWCORE_DEFINE_CONVERT(QString, _from, BigNumber, _to, Safe)
{
  cres_try(*_to, BigNumber::fromString(*_from));
  return cres_success();
}

KNOWCORE_DEFINE_METATYPE(knowCore::BigNumber, knowCore::Uris::xsd::decimal,
                         knowCore::MetaTypeTraits::DebugStreamOperator
                           | knowCore::MetaTypeTraits::Comparable
                           | knowCore::MetaTypeTraits::ToString
                           | knowCore::MetaTypeTraits::FromString
                           | knowCore::MetaTypeTraits::NumericType)
KNOWCORE_DEFINE_METATYPE_ALIASES(knowCore::Uris::xsd::integer,
                                 knowCore::Uris::xsd::nonNegativeInteger,
                                 knowCore::Uris::xsd::decimal)

KNOWCORE_REGISTER_COMPARATORS((Equal, Inferior), BigNumber)
KNOWCORE_REGISTER_ARITHMETIC_OPERATORS((Addition, Substraction, Multiplication, Division),
                                       BigNumber, BigNumber)
