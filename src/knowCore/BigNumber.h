#ifndef _KNOW_CORE_BIGNUMBER_H_
#define _KNOW_CORE_BIGNUMBER_H_

#include <clog_qt>
#include <cres_qt>

#include <QList>

#include "Forward.h"

#include <Cyqlops/Crypto/Hash.h>

class QVariant;

namespace std
{
  template<>
  struct is_floating_point<knowCore::BigNumber> : public true_type
  {
  };
  template<>
  struct is_signed<knowCore::BigNumber> : public true_type
  {
  };
  template<>
  struct is_unsigned<knowCore::BigNumber> : public false_type
  {
  };
  template<>
  struct is_integral<knowCore::BigNumber> : public false_type
  {
  };
} // namespace std

namespace knowCore
{
  /*! \brief Class that can contains large numeric value.
   *
   * The representation of number in this class is modeled after the one used by postgres (see
   * postgres/src/backend/utils/adt/numeric.c source code for the "specification").
   *
   * In BigNumber the number are stored in base-1000:
   * - m_digits stores the significant digits, each element of the list is a quint16 between 0->9999
   * - m_weight stores the 10000-exponent of the first number in m_digits
   * - m_dscale stores the number of decimal values (10.02 has dscale of 2, 10 0f 0, 10.00005
   * of 5...). This number is purely for display purposes of the number of 0 after the ".".
   *
   * The number is given by m_digits[0]*10000^m_weight + m_digits[1]*10000^(m_weight-1) + ....
   */
  class BigNumber
  {
  public:
    enum class Sign
    {
      Positive,
      Negative,
      NaN,
      PositiveInfinite,
      NegativeInfinite
    };
  public:
    BigNumber();
    BigNumber(const BigNumber& _rhs);
    BigNumber(BigNumber&& _rhs);
    BigNumber& operator=(const BigNumber& _rhs);
    BigNumber(qint16 weight, Sign sign, qint16 dscale, const QList<quint16>& digits);
    ~BigNumber();

    BigNumber(std::intmax_t _value);
    BigNumber(std::uintmax_t _value);
    template<typename _T_>
      requires(std::is_integral_v<_T_> and std::is_unsigned_v<_T_>)
    BigNumber(_T_ _value) : BigNumber(std::uintmax_t(_value))
    {
    }
    template<typename _T_>
      requires(std::is_integral_v<_T_> and std::is_signed_v<_T_>)
    BigNumber(_T_ _value) : BigNumber(std::intmax_t(_value))
    {
    }
    BigNumber(double _value);
    static cres_qresult<BigNumber> fromString(const QString& _value);
    static cres_qresult<BigNumber> fromString(const char* _value)
    {
      return fromString(QString(_value));
    }
    static cres_qresult<BigNumber> fromVariant(const QVariant& _value);
    static cres_qresult<BigNumber> fromValue(const knowCore::Value& _value);
    static BigNumber zero();
    static BigNumber nan();
    static BigNumber positiveInfinite();
    static BigNumber negativeInfinite();
    bool isNaN() const;
    bool isFinite() const;
    bool isInfinite() const;
    /**
     * @return true if is floating point number
     */
    bool isFloating() const;
    QString toString() const;
    /** \brief return an optimal variant representation
     * This function try to return a variant with an optimal representation:
     *  - first try to store the number if a (u)int64 (depending on sign)
     *  - if it cannot be represented by (u)int64, then return a QVariant countaining a BigNumber
     */
    QVariant toVariant() const;
    /** \brief return an optimal value representation
     * This function try to return a variant with an optimal representation:
     *  - first try to store the number if a (u)int64 (depending on sign)
     *  - if it cannot be represented by (u)int64, then return a QVariant countaining a BigNumber
     */
    knowCore::Value toValue() const;
    double toDouble() const;
    /**
     * Attempt to convert to int64, in case of overflow, return an error.
     *
     * @p _truncate if set to true truncate a floating point number, otherwise return an error if
     * big number is a floating point
     */
    cres_qresult<qint64> toInt64(bool _truncate = false) const;
    cres_qresult<quint64> toUInt64(bool _truncate = false) const;
    bool operator==(const BigNumber& _rhs) const;
    bool operator!=(const BigNumber& _rhs) const { return not(*this == _rhs); }
    QByteArray md5() const;
    /**
     * Add the big number to a cryptographic hash calculation.
     */
    void hash(QCryptographicHash* _hash) const;

    BigNumber operator+(const BigNumber& _rhs) const;
    BigNumber operator-(const BigNumber& _rhs) const;
    BigNumber operator*(const BigNumber& _rhs) const;
    BigNumber operator/(const BigNumber& _rhs) const;
    BigNumber operator-() const;
    bool operator<(const BigNumber& _rhs) const;
    bool operator<=(const BigNumber& _rhs) const;
    bool operator>(const BigNumber& _rhs) const;
    bool operator>=(const BigNumber& _rhs) const;

    template<typename _T_>
      requires(std::is_arithmetic_v<_T_>)
    BigNumber operator+(const _T_& _rhs) const
    {
      return operator+(BigNumber(_rhs));
    }
    template<typename _T_>
      requires(std::is_arithmetic_v<_T_>)
    BigNumber operator-(const _T_& _rhs) const
    {
      return operator-(BigNumber(_rhs));
    }
    template<typename _T_>
      requires(std::is_arithmetic_v<_T_>)
    BigNumber operator*(const _T_& _rhs) const
    {
      return operator*(BigNumber(_rhs));
    }
    template<typename _T_>
      requires(std::is_arithmetic_v<_T_>)
    BigNumber operator/(const _T_& _rhs) const
    {
      return operator/(BigNumber(_rhs));
    }
    template<typename _T_>
      requires(std::is_arithmetic_v<_T_>)
    bool operator<(const _T_& _rhs) const
    {
      return operator<(BigNumber(_rhs));
    }
    template<typename _T_>
      requires(std::is_arithmetic_v<_T_>)
    bool operator<=(const _T_& _rhs) const
    {
      return operator<=(BigNumber(_rhs));
    }
    template<typename _T_>
      requires(std::is_arithmetic_v<_T_>)
    bool operator>(const _T_& _rhs) const
    {
      return operator>(BigNumber(_rhs));
    }
    template<typename _T_>
      requires(std::is_arithmetic_v<_T_>)
    bool operator>=(const _T_& _rhs) const
    {
      return operator>=(BigNumber(_rhs));
    }
  public:
    quint16 weight() const { return m_weight; }
    Sign sign() const { return m_sign; }
    quint16 dscale() const { return m_dscale; }
    QList<quint16> digits() const { return m_digits; }
  private:
    void simplify();
    void roundVar();
    void exponenfy(int exp);
    BigNumber add_abs(const BigNumber& _rhs) const;
    BigNumber sub_abs(const BigNumber& _rhs) const;
    enum class ComparisonResult
    {
      Bigger /* this < _rhs */,
      Smaller /* this > _rhs */,
      Equal
    };
    ComparisonResult compare_abs(const BigNumber& _rhs) const;
    ComparisonResult compare(const BigNumber& _rhs) const;
    /**
     * return true if overflow, otherwise false and the value
     */
    std::tuple<bool, quint64> toUInt64_ignore_sign() const;
    template<typename _T_>
    _T_ toVariantValue() const;
  private:
    qint16 m_weight, m_dscale;
    Sign m_sign;
    QList<quint16> m_digits;
  };
  template<typename _T_>
    requires(std::is_arithmetic_v<_T_>)
  static inline BigNumber operator+(const _T_& _lhs, const BigNumber& _rhs)
  {
    return BigNumber(_lhs) + _rhs;
  }
  template<typename _T_>
    requires(std::is_arithmetic_v<_T_>)
  static inline BigNumber operator-(const _T_& _lhs, const BigNumber& _rhs)
  {
    return BigNumber(_lhs) - _rhs;
  }
  template<typename _T_>
    requires(std::is_arithmetic_v<_T_>)
  static inline BigNumber operator*(const _T_& _lhs, const BigNumber& _rhs)
  {
    return BigNumber(_lhs) * _rhs;
  }
  template<typename _T_>
    requires(std::is_arithmetic_v<_T_>)
  static inline BigNumber operator/(const _T_& _lhs, const BigNumber& _rhs)
  {
    return BigNumber(_lhs) / _rhs;
  }
  template<typename _T_>
    requires(std::is_arithmetic_v<_T_>)
  static inline bool operator<(const _T_& _lhs, const BigNumber& _rhs)
  {
    return BigNumber(_lhs) < _rhs;
  }
  template<typename _T_>
    requires(std::is_arithmetic_v<_T_>)
  static inline bool operator<=(const _T_& _lhs, const BigNumber& _rhs)
  {
    return BigNumber(_lhs) <= _rhs;
  }
  template<typename _T_>
    requires(std::is_arithmetic_v<_T_>)
  static inline bool operator>(const _T_& _lhs, const BigNumber& _rhs)
  {
    return BigNumber(_lhs) > _rhs;
  }
  template<typename _T_>
    requires(std::is_arithmetic_v<_T_>)
  static inline bool operator>=(const _T_& _lhs, const BigNumber& _rhs)
  {
    return BigNumber(_lhs) >= _rhs;
  }
  template<typename _T_>
    requires(std::is_arithmetic_v<_T_>)
  static inline bool operator==(const _T_& _lhs, const BigNumber& _rhs)
  {
    return BigNumber(_lhs) == _rhs;
  }
} // namespace knowCore

#include "MetaType.h"
KNOWCORE_DECLARE_FULL_METATYPE(knowCore, BigNumber);

#include "Formatter.h"

clog_format_declare_formatter(knowCore::BigNumber)
{
  return std::format_to(ctx.out(), "{}", p.toString());
}

namespace Cyqlops::Crypto::Hash
{
  template<>
  struct Compute<knowCore::BigNumber>
  {
    Compute(QCryptographicHash& _algorithm, const knowCore::BigNumber& _v) { _v.hash(&_algorithm); }
  };
} // namespace Cyqlops::Crypto::Hash

#endif
