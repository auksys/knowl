#include "BigNumberList.h"

#include <QVariant>

#include "Uri.h"
#include "ValueList.h"

using namespace knowCore;

// template<>
QVariant QListSpecialMethods<BigNumber>::toVariant() const
{
  QVariantList l;
  for(const BigNumber& bn : *self())
  {
    l.append(bn.toVariant());
  }
  return l;
}

// template<>
knowCore::Value QListSpecialMethods<BigNumber>::toValue() const
{
  QList<Value> l;
  for(const BigNumber& bn : *self())
  {
    l.append(bn.toValue());
  }
  return Value::fromValue<ValueList>(l);
}

cres_qresult<BigNumberList> QListSpecialMethods<BigNumber>::fromVariant(const QVariant& _variant)
{
  if(_variant.canConvert<BigNumberList>())
  {
    return cres_success(_variant.value<BigNumberList>());
  }
  else if(_variant.canConvert<QVariantList>())
  {
    BigNumberList bnl;
    for(const QVariant& v : _variant.toList())
    {
      cres_try(BigNumber n, BigNumber::fromVariant(v));
      bnl.append(n);
    }
    return cres_success(bnl);
  }
  else
  {
    return cres_failure("Not a list of numbers");
  }
}

cres_qresult<BigNumberList>
  QListSpecialMethods<BigNumber>::fromValue(const knowCore::Value& _variant)
{
  if(ValueCast<BigNumberList> bnl = _variant)
  {
    return cres_success(bnl.value());
  }
  else if(ValueCast<knowCore::ValueList> vnl = _variant)
  {
    cres_try(QList<BigNumber> list, vnl->values<BigNumber>());
    return cres_success(list);
  }
  else
  {
    return cres_failure("Cannot convert list of values to numbe fromt type {}",
                        _variant.datatype());
  }
}

#include "Uris/askcore_datatype.h"

#include "BigNumberMetaTypeImplementation_p.h"

KNOWCORE_DEFINE_METATYPE(knowCore::BigNumberList, Uris::askcore_datatype::decimallist,
                         MetaTypeTraits::DebugStreamOperator | MetaTypeTraits::OnlyEqualComparable
                           | MetaTypeTraits::FromValueList | MetaTypeTraits::ToValueList)
