#pragma once

#include "BigNumber.h"
#include "Global.h"

namespace knowCore
{
  using BigNumberList = QList<BigNumber>;
}

QT_BEGIN_NAMESPACE

/*! \brief convenient class for handling list of BigNumber
 *
 */
template<>
struct QListSpecialMethods<knowCore::BigNumber> : QListSpecialMethodsBase<knowCore::BigNumber>
{
public:
  QVariant toVariant() const;
  knowCore::Value toValue() const;
  static cres_qresult<knowCore::BigNumberList> fromVariant(const QVariant& _variant);
  static cres_qresult<knowCore::BigNumberList> fromValue(const knowCore::Value& _variant);
};

QT_END_NAMESPACE

KNOWCORE_DECLARE_FULL_METATYPE(knowCore, BigNumberList);
