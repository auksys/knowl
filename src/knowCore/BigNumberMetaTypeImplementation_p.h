#include <knowCore/MetaTypeImplementation.h>

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(knowCore::BigNumber)
cres_qresult<QByteArray> md5(const knowCore::BigNumber& _value) const override
{
  return cres_success(_value.md5());
}
cres_qresult<QJsonValue> toJsonValue(const knowCore::BigNumber& _value,
                                     const SerialisationContexts&) const override
{
  const auto [success, value, errorMessage] = _value.toInt64();
  Q_UNUSED(errorMessage);

  if(success and value > -9007199254740992
     and value < 9007199254740992) // outside of that range, there is a loss of precision, since
                                   // Json store value as double!
  {
    return cres_success(QJsonValue(value.value()));
  }
  else
  {
    return cres_success(QJsonValue(_value.toString()));
  }
}
cres_qresult<void> fromJsonValue(knowCore::BigNumber* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts&) const override
{
  if(_json_value.isDouble())
  {
    *_value = BigNumber(_json_value.toDouble());
    return cres_success();
  }
  else if(_json_value.isString())
  {
    cres_try(*_value, BigNumber::fromString(_json_value.toString()));
    return cres_success();
  }
  else
  {
    return expectedError("number", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const knowCore::BigNumber& _value,
                                     const SerialisationContexts&) const override
{
  const auto [success, value, errorMessage] = _value.toInt64();
  Q_UNUSED(errorMessage);
  if(success)
  {
    return cres_success(QCborValue(value.value()));
  }
  else
  {
    return cres_success(_value.toString());
  }
}
cres_qresult<void> fromCborValue(knowCore::BigNumber* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts&) const override
{
  if(_cbor_value.isInteger())
  {
    *_value = BigNumber(_cbor_value.toInteger());
    return cres_success();
  }
  else if(_cbor_value.isString())
  {
    cres_try(*_value, BigNumber::fromString(_cbor_value.toString()));
    return cres_success();
  }
  else
  {
    return expectedError("number", _cbor_value);
  }
}
cres_qresult<QString> printable(const knowCore::BigNumber& _value) const override
{
  return cres_success(_value.toString());
}
cres_qresult<QString> toRdfLiteral(const knowCore::BigNumber& _value,
                                   const SerialisationContexts&) const override
{
  return cres_success(_value.toString());
}
cres_qresult<void> fromRdfLiteral(knowCore::BigNumber* _value, const QString& _serialised,
                                  const DeserialisationContexts&) const override
{
  cres_try(*_value, BigNumber::fromString(_serialised));
  return cres_success();
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(knowCore::BigNumber)
