// Ported from postrgresql src/backend/utils/adt/numeric.c
// Original coding 1998, Jan Wieck.  Heavily revised 2003, Tom Lane.
//
// Many of the algorithmic ideas are borrowed from David M. Smith's "FM"
// multiple-precision math library, most recently published as Algorithm
// 786: Multiple-Precision Complex Arithmetic and Functions, ACM
// Transactions on Mathematical Software, Vol. 24, No. 4, December 1998,
// pages 359-367.
//
// Copyright (c) 1998-2020, PostgreSQL Global Development Group
//
// IDENTIFICATION
//    src/backend/utils/adt/numeric.c
//
// PostgreSQL Database Management System
// (formerly known as Postgres, then as Postgres95)
//
// Portions Copyright (c) 1996-2020, PostgreSQL Global Development Group
//
// Portions Copyright (c) 1994, The Regents of the University of California
//
// Permission to use, copy, modify, and distribute this software and its
// documentation for any purpose, without fee, and without a written agreement
// is hereby granted, provided that the above copyright notice and this
// paragraph and the following two paragraphs appear in all copies.
//
// IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
// DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING
// LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
// DOCUMENTATION, EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
// ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATIONS TO
// PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

#include "BigNumber.h"

using namespace knowCore;

#define NBASE 10000
#define HALF_NBASE 5000
#define DEC_DIGITS 4       /* decimal digits per NBASE digit */
#define MUL_GUARD_DIGITS 2 /* these are measured in NBASE digits */
#define DIV_GUARD_DIGITS 4

void BigNumber::roundVar() {}

BigNumber BigNumber::add_abs(const BigNumber& _rhs) const
{
  BigNumber res;

  res.m_weight = std::max(m_weight, _rhs.m_weight) + 1;

  res.m_dscale = std::max(m_dscale, _rhs.m_dscale);

  /* Note: here we are figuring rscale in base-NBASE digits */
  int rscale1 = m_digits.size() - m_weight - 1;
  int rscale2 = _rhs.m_digits.size() - _rhs.m_weight - 1;
  int res_rscale = std::max(rscale1, rscale2);
  int res_ndigits = res_rscale + res.m_weight + 1;
  if(res_ndigits <= 0)
    res_ndigits = 1;

  std::fill_n(std::back_inserter(res.m_digits), res_ndigits, 0);

  int carry = 0;
  int i1 = res_rscale + m_weight + 1;
  int i2 = res_rscale + _rhs.m_weight + 1;
  for(int i = res_ndigits - 1; i >= 0; i--)
  {
    i1--;
    i2--;
    if(i1 >= 0 && i1 < m_digits.size())
      carry += m_digits[i1];
    if(i2 >= 0 && i2 < _rhs.m_digits.size())
      carry += _rhs.m_digits[i2];

    if(carry >= NBASE)
    {
      res.m_digits[i] = carry - NBASE;
      carry = 1;
    }
    else
    {
      res.m_digits[i] = carry;
      carry = 0;
    }
  }

  clog_assert(carry == 0); /* else we failed to allow for carry out */
  /* Remove leading/trailing zeroes */
  res.simplify();
  return res;
}

BigNumber BigNumber::sub_abs(const BigNumber& _rhs) const
{
  BigNumber res;

  res.m_weight = m_weight;

  res.m_dscale = std::max(m_dscale, _rhs.m_dscale);

  /* Note: here we are figuring rscale in base-NBASE digits */
  int rscale1 = m_digits.size() - m_weight - 1;
  int rscale2 = _rhs.m_digits.size() - _rhs.m_weight - 1;
  int res_rscale = std::max(rscale1, rscale2);

  int res_ndigits = res_rscale + res.m_weight + 1;
  if(res_ndigits <= 0)
    res_ndigits = 1;

  std::fill_n(std::back_inserter(res.m_digits), res_ndigits, 0);

  int borrow = 0;
  int i1 = res_rscale + m_weight + 1;
  int i2 = res_rscale + _rhs.m_weight + 1;
  for(int i = res_ndigits - 1; i >= 0; i--)
  {
    i1--;
    i2--;
    if(i1 >= 0 && i1 < m_digits.size())
      borrow += m_digits[i1];
    if(i2 >= 0 && i2 < _rhs.m_digits.size())
      borrow -= _rhs.m_digits[i2];

    if(borrow < 0)
    {
      res.m_digits[i] = borrow + NBASE;
      borrow = -1;
    }
    else
    {
      res.m_digits[i] = borrow;
      borrow = 0;
    }
  }

  clog_assert(borrow == 0); /* else caller gave us var1 < var2 */

  /* Remove leading/trailing zeroes */
  res.simplify();
  return res;
}

BigNumber::ComparisonResult BigNumber::compare_abs(const BigNumber& _rhs) const
{
  int i1 = 0;
  int i2 = 0;

  int var1weight = m_weight;
  int var2weight = _rhs.m_weight;

  /* Check any digits before the first common digit */

  while(var1weight > var2weight && i1 < m_digits.size())
  {
    if(m_digits[i1++] != 0)
      return ComparisonResult::Smaller;
    var1weight--;
  }
  while(var2weight > var1weight && i2 < _rhs.m_digits.size())
  {
    if(_rhs.m_digits[i2++] != 0)
      return ComparisonResult::Bigger;
    var2weight--;
  }

  /* At this point, either w1 == w2 or we've run out of digits */

  if(var1weight == var2weight)
  {
    while(i1 < m_digits.size() && i2 < _rhs.m_digits.size())
    {
      int stat = m_digits[i1++] - _rhs.m_digits[i2++];

      if(stat)
      {
        if(stat > 0)
          return ComparisonResult::Smaller;
        return ComparisonResult::Bigger;
      }
    }
  }

  /*
   * At this point, we've run out of digits on one side or the other; so any
   * remaining nonzero digits imply that side is larger
   */
  while(i1 < m_digits.size())
  {
    if(m_digits[i1++] != 0)
      return ComparisonResult::Smaller;
  }
  while(i2 < _rhs.m_digits.size())
  {
    if(_rhs.m_digits[i2++] != 0)
      return ComparisonResult::Bigger;
  }

  return ComparisonResult::Equal;
}

BigNumber::ComparisonResult BigNumber::compare(const BigNumber& _rhs) const
{
  if(m_sign == Sign::NegativeInfinite)
  {
    return _rhs.sign() == Sign::NegativeInfinite ? ComparisonResult::Equal
                                                 : ComparisonResult::Bigger;
  }
  if(m_sign == Sign::PositiveInfinite)
  {
    return _rhs.sign() == Sign::NegativeInfinite ? ComparisonResult::Equal
                                                 : ComparisonResult::Smaller;
  }
  if(_rhs.sign() == Sign::NegativeInfinite)
  {
    return ComparisonResult::Smaller;
  }
  if(_rhs.sign() == Sign::PositiveInfinite)
  {
    return ComparisonResult::Bigger;
  }
  if(m_digits.size() == 0)
  {
    if(_rhs.m_digits.size() == 0)
      return ComparisonResult::Equal;
    if(_rhs.m_sign == Sign::Negative)
      return ComparisonResult::Smaller;
    return ComparisonResult::Bigger;
  }
  if(_rhs.m_digits.size() == 0)
  {
    if(m_sign == Sign::Positive)
      return ComparisonResult::Smaller;
    return ComparisonResult::Bigger;
  }

  if(m_sign == Sign::Positive)
  {
    if(_rhs.m_sign == Sign::Negative)
      return ComparisonResult::Smaller;
    return compare_abs(_rhs);
  }

  if(_rhs.m_sign == Sign::Positive)
    return ComparisonResult::Bigger;

  return _rhs.compare_abs(*this);
}

BigNumber BigNumber::operator+(const BigNumber& _rhs) const
{
  switch(m_sign)
  {
  case Sign::Positive:
    switch(_rhs.sign())
    {
    case Sign::Positive:
      return add_abs(_rhs);
    case Sign::Negative:
      switch(compare_abs(_rhs))
      {
      case ComparisonResult::Equal:
        return zero();
      case ComparisonResult::Smaller: /* |this| > |_rhs| */
        return sub_abs(_rhs);
      case ComparisonResult::Bigger: /* |this| < |_rhs| */
        return -_rhs.sub_abs(*this);
      }
      break;
    case Sign::NaN:
    case Sign::PositiveInfinite:
    case Sign::NegativeInfinite:
      return _rhs;
    }
    return nan();
  case Sign::Negative:
    switch(_rhs.sign())
    {
    case Sign::Positive:
      switch(compare_abs(_rhs))
      {
      case ComparisonResult::Equal:
        return zero();
      case ComparisonResult::Smaller: /* |this| > |_rhs| */
        return -sub_abs(_rhs);
      case ComparisonResult::Bigger: /* |this| < |_rhs| */
        return _rhs.sub_abs(*this);
      }
      break;
    case Sign::Negative:
      return -add_abs(_rhs);
    case Sign::NaN:
    case Sign::PositiveInfinite:
    case Sign::NegativeInfinite:
      return _rhs;
    }
    return nan();
  case Sign::NegativeInfinite:
    return _rhs.sign() == Sign::PositiveInfinite ? nan() : *this;
  case Sign::PositiveInfinite:
    return _rhs.sign() == Sign::NegativeInfinite ? nan() : *this;
  case Sign::NaN:
    return *this;
  }
  return nan();
}

BigNumber BigNumber::operator-(const BigNumber& _rhs) const
{
  switch(m_sign)
  {
  case Sign::Positive:
    switch(_rhs.sign())
    {
    case Sign::Positive:
      switch(compare_abs(_rhs))
      {
      case ComparisonResult::Equal:
        return zero();
      case ComparisonResult::Smaller: /* |this| > |_rhs| */
        return sub_abs(_rhs);
      case ComparisonResult::Bigger: /* |this| < |_rhs| */
        return -_rhs.sub_abs(*this);
      }
      break;
    case Sign::Negative:
      return add_abs(_rhs);
    case Sign::NaN:
    case Sign::PositiveInfinite:
    case Sign::NegativeInfinite:
      return _rhs;
    }
    return nan();
  case Sign::Negative:
    switch(_rhs.sign())
    {
    case Sign::Positive:
      return -add_abs(_rhs);
    case Sign::Negative:
      switch(compare_abs(_rhs))
      {
      case ComparisonResult::Equal:
        return zero();
      case ComparisonResult::Smaller: /* |this| > |_rhs| */
        return -sub_abs(_rhs);
      case ComparisonResult::Bigger: /* |this| < |_rhs| */
        return _rhs.sub_abs(*this);
      }
      break;
    case Sign::NaN:
    case Sign::PositiveInfinite:
    case Sign::NegativeInfinite:
      return _rhs;
    }
    return nan();
  case Sign::NegativeInfinite:
    return _rhs.sign() == Sign::NegativeInfinite ? nan() : *this;
  case Sign::PositiveInfinite:
    return _rhs.sign() == Sign::PositiveInfinite ? nan() : *this;
  case Sign::NaN:
    return *this;
  }
  return nan();
}

BigNumber BigNumber::operator*(const BigNumber& _rhs) const
{
  if(m_sign == Sign::NaN)
    return *this;
  if(_rhs.m_sign == Sign::NaN)
    return _rhs;
  if(m_sign == Sign::PositiveInfinite)
  {
    return (_rhs.sign() == Sign::Negative or _rhs.sign() == Sign::NegativeInfinite)
             ? negativeInfinite()
             : *this;
  }
  if(m_sign == Sign::NegativeInfinite)
  {
    return (_rhs.sign() == Sign::Negative or _rhs.sign() == Sign::NegativeInfinite)
             ? positiveInfinite()
             : *this;
  }
  if(_rhs.sign() == Sign::PositiveInfinite)
  {
    return m_sign == Sign::Negative ? negativeInfinite() : *this;
  }
  if(_rhs.sign() == Sign::NegativeInfinite)
  {
    return m_sign == Sign::Negative ? positiveInfinite() : *this;
  }

  BigNumber res;

  res.m_dscale = std::max(m_dscale, _rhs.m_dscale);

  /*
   * Arrange for var1 to be the shorter of the two numbers.  This improves
   * performance because the inner multiplication loop is much simpler than
   * the outer loop, so it's better to have a smaller number of iterations
   * of the outer loop.  This also reduces the number of times that the
   * accumulator array needs to be normalized.
   */
  if(m_digits.size() > _rhs.m_digits.size())
  {
    return _rhs * *this;
  }

  if(m_digits.size() == 0 || _rhs.m_digits.size() == 0)
  {
    /* one or both inputs is zero; so is result */
    return zero();
  }

  /* Determine result sign and (maximum possible) weight */
  if(m_sign == _rhs.m_sign)
    res.m_sign = Sign::Positive;
  else
    res.m_sign = Sign::Negative;
  res.m_weight = m_weight + _rhs.m_weight + 2;

  /*
   * Determine the number of result digits to compute.  If the exact result
   * would have more than rscale fractional digits, truncate the computation
   * with MUL_GUARD_DIGITS guard digits, i.e., ignore input digits that
   * would only contribute to the right of that.  (This will give the exact
   * rounded-to-rscale answer unless carries out of the ignored positions
   * would have propagated through more than MUL_GUARD_DIGITS digits.)
   *
   * Note: an exact computation could not produce more than m_digits.size() +
   * _rhs.m_digits.size() digits, but we allocate one extra output digit in case
   * rscale-driven rounding produces a carry out of the highest exact digit.
   */
  qsizetype res_ndigits = m_digits.size() + _rhs.m_digits.size() + 1;
  qsizetype maxdigits
    = res.m_weight + 1 + (res.m_dscale + DEC_DIGITS - 1) / DEC_DIGITS + MUL_GUARD_DIGITS;
  res_ndigits = std::min(res_ndigits, maxdigits);

  if(res_ndigits < 3)
  {
    /* All input digits will be ignored; so result is zero */
    return zero();
  }

  /*
   * We do the arithmetic in an array "dig[]" of signed int's.  Since
   * INT_MAX is noticeably larger than NBASE*NBASE, this gives us headroom
   * to avoid normalizing carries immediately.
   *
   * maxdig tracks the maximum possible value of any dig[] entry; when this
   * threatens to exceed INT_MAX, we take the time to propagate carries.
   * Furthermore, we need to ensure that overflow doesn't occur during the
   * carry propagation passes either.  The carry values could be as much as
   * INT_MAX/NBASE, so really we must normalize when digits threaten to
   * exceed INT_MAX - INT_MAX/NBASE.
   *
   * To avoid overflow in maxdig itself, it actually represents the max
   * possible value divided by NBASE-1, ie, at the top of the loop it is
   * known that no dig[] entry exceeds maxdig * (NBASE-1).
   */
  QVector<int> dig;
  dig.fill(0, res_ndigits);
  int maxdig = 0;

  /*
   * The least significant digits of var1 should be ignored if they don't
   * contribute directly to the first res_ndigits digits of the result that
   * we are computing.
   *
   * Digit i1 of var1 and digit i2 of var2 are multiplied and added to digit
   * i1+i2+2 of the accumulator array, so we need only consider digits of
   * var1 for which i1 <= res_ndigits - 3.
   */
  for(int i1 = std::min(m_digits.size() - 1, res_ndigits - 3); i1 >= 0; i1--)
  {
    int var1digit = m_digits[i1];

    if(var1digit == 0)
      continue;

    /* Time to normalize? */
    maxdig += var1digit;
    if(maxdig > (INT_MAX - INT_MAX / NBASE) / (NBASE - 1))
    {
      /* Yes, do it */
      int carry = 0;
      for(int i = res_ndigits - 1; i >= 0; i--)
      {
        int newdig = dig[i] + carry;
        if(newdig >= NBASE)
        {
          carry = newdig / NBASE;
          newdig -= carry * NBASE;
        }
        else
          carry = 0;
        dig[i] = newdig;
      }
      clog_assert(carry == 0);
      /* Reset maxdig to indicate new worst-case */
      maxdig = 1 + var1digit;
    }

    /*
     * Add the appropriate multiple of var2 into the accumulator.
     *
     * As above, digits of var2 can be ignored if they don't contribute,
     * so we only include digits for which i1+i2+2 < res_ndigits.
     *
     * This inner loop is the performance bottleneck for multiplication,
     * so we want to keep it simple enough so that it can be
     * auto-vectorized.  Accordingly, process the digits left-to-right
     * even though schoolbook multiplication would suggest right-to-left.
     * Since we aren't propagating carries in this loop, the order does
     * not matter.
     */
    {
      int i2limit = std::min(_rhs.m_digits.size(), res_ndigits - i1 - 2);
      int* dig_i1_2 = &dig[i1 + 2];

      for(int i2 = 0; i2 < i2limit; i2++)
        dig_i1_2[i2] += var1digit * _rhs.m_digits[i2];
    }
  }

  /*
   * Now we do a final carry propagation pass to normalize the result, which
   * we combine with storing the result digits into the output. Note that
   * this is still done at full precision w/guard digits.
   */
  std::fill_n(std::back_inserter(res.m_digits), res_ndigits, 0);
  int carry = 0;
  for(int i = res_ndigits - 1; i >= 0; i--)
  {
    int newdig = dig[i] + carry;
    if(newdig >= NBASE)
    {
      carry = newdig / NBASE;
      newdig -= carry * NBASE;
    }
    else
      carry = 0;
    res.m_digits[i] = newdig;
  }
  clog_assert(carry == 0);

  /* Round to target rscale (and set result->dscale) */
  res.roundVar();

  /* Strip leading and trailing zeroes */
  res.simplify();
  return res;
}

BigNumber BigNumber::operator/(const BigNumber& _rhs) const
{
  if((not isFinite() and not _rhs.isFinite()) or m_sign == Sign::NaN or _rhs.m_sign == Sign::NaN)
    return nan();

  if(m_sign == Sign::PositiveInfinite)
  {
    clog_assert(_rhs.isFinite());
    return _rhs.sign() == Sign::Negative ? negativeInfinite() : *this;
  }
  if(m_sign == Sign::NegativeInfinite)
  {
    clog_assert(_rhs.isFinite());
    return _rhs.sign() == Sign::Negative ? positiveInfinite() : *this;
  }
  if(_rhs.sign() == Sign::PositiveInfinite or _rhs.sign() == Sign::NegativeInfinite)
  {
    return BigNumber::zero();
  }

  BigNumber res;

  res.m_dscale = std::max(m_dscale, _rhs.m_dscale);

  /*
   * First of all division by zero check; we must not be handed an
   * unnormalized divisor.
   */
  if(_rhs.m_digits.size() == 0 || _rhs.m_digits[0] == 0)
  {
    return BigNumber::nan();
  }

  /*
   * Now result zero check
   */
  if(m_digits.size() == 0)
  {
    return zero();
  }

  /*
   * Determine the result sign, weight and number of digits to calculate.
   * The weight figured here is correct if the emitted quotient has no
   * leading zero digits; otherwise strip_var() will fix things up.
   */
  if(m_sign == _rhs.m_sign)
    res.m_sign = Sign::Positive;
  else
    res.m_sign = Sign::Negative;
  res.m_weight = m_weight - _rhs.m_weight;
  /* The number of accurate result digits we need to produce: */
  int res_ndigits = res.m_weight + 1 + (res.m_dscale + DEC_DIGITS - 1) / DEC_DIGITS;
  /* ... but always at least 1 */
  res_ndigits = std::max(res_ndigits, 1) + 1; // +1 for rounding

  /*
   * The working dividend normally requires res_ndigits + _rhs.m_digits.size()
   * digits, but make it at least m_digits.size() so we can load all of var1
   * into it.  (There will be an additional digit dividend[0] in the
   * dividend space, but for consistency with Knuth's notation we don't
   * count that in div_ndigits.)
   */
  qsizetype div_ndigits = res_ndigits + _rhs.m_digits.size();
  div_ndigits = std::max(div_ndigits, m_digits.size());

  /*
   * We need a workspace with room for the working dividend (div_ndigits+1
   * digits) plus room for the possibly-normalized divisor (_rhs.m_digits.size()
   * digits).  It is convenient also to have a zero at divisor[0] with the
   * actual divisor data in divisor[1 .. _rhs.m_digits.size()].  Transferring the
   * digits into the workspace also allows us to realloc the result (which
   * might be the same as either input var) before we begin the main loop.
   * Note that we use palloc0 to ensure that divisor[0], dividend[0], and
   * any additional dividend positions beyond m_digits.size(), start out 0.
   */
  QVector<int> storage(div_ndigits + _rhs.m_digits.size() + 2);
  int* dividend = storage.data();
  int* divisor = dividend + (div_ndigits + 1);
  std::copy(m_digits.begin(), m_digits.end(), dividend + 1);
  std::copy(_rhs.m_digits.begin(), _rhs.m_digits.end(), divisor + 1);

  /*
   * Now we can realloc the result to hold the generated quotient digits.
   */
  std::fill_n(std::back_inserter(res.m_digits), res_ndigits, 0);

  if(_rhs.m_digits.size() == 1)
  {
    /*
     * If there's only a single divisor digit, we can use a fast path (cf.
     * Knuth section 4.3.1 exercise 16).
     */
    int divisor1 = divisor[1];
    int carry = 0;
    for(int i = 0; i < res_ndigits; i++)
    {
      carry = carry * NBASE + dividend[i + 1];
      res.m_digits[i] = carry / divisor1;
      carry = carry % divisor1;
    }
  }
  else
  {
    /*
     * The full multiple-place algorithm is taken from Knuth volume 2,
     * Algorithm 4.3.1D.
     *
     * We need the first divisor digit to be >= NBASE/2.  If it isn't,
     * make it so by scaling up both the divisor and dividend by the
     * factor "d".  (The reason for allocating dividend[0] above is to
     * leave room for possible carry here.)
     */
    if(divisor[1] < HALF_NBASE)
    {
      int d = NBASE / (divisor[1] + 1);

      int carry = 0;
      for(int i = _rhs.m_digits.size(); i > 0; i--)
      {
        carry += divisor[i] * d;
        divisor[i] = carry % NBASE;
        carry = carry / NBASE;
      }
      clog_assert(carry == 0);
      carry = 0;
      /* at this point only m_digits.size() of dividend can be nonzero */
      for(int i = m_digits.size(); i >= 0; i--)
      {
        carry += dividend[i] * d;
        dividend[i] = carry % NBASE;
        carry = carry / NBASE;
      }
      clog_assert(carry == 0);
      clog_assert(divisor[1] >= HALF_NBASE);
    }
    /* First 2 divisor digits are used repeatedly in main loop */
    int divisor1 = divisor[1];
    int divisor2 = divisor[2];

    /*
     * Begin the main loop.  Each iteration of this loop produces the j'th
     * quotient digit by dividing dividend[j .. j + _rhs.m_digits.size()] by the
     * divisor; this is essentially the same as the common manual
     * procedure for long division.
     */
    for(int j = 0; j < res_ndigits; j++)
    {
      /* Estimate quotient digit from the first two dividend digits */
      int next2digits = dividend[j] * NBASE + dividend[j + 1];
      int qhat;

      /*
       * If next2digits are 0, then quotient digit must be 0 and there's
       * no need to adjust the working dividend.  It's worth testing
       * here to fall out ASAP when processing trailing zeroes in a
       * dividend.
       */
      if(next2digits == 0)
      {
        res.m_digits[j] = 0;
        continue;
      }

      if(dividend[j] == divisor1)
        qhat = NBASE - 1;
      else
        qhat = next2digits / divisor1;

      /*
       * Adjust quotient digit if it's too large.  Knuth proves that
       * after this step, the quotient digit will be either correct or
       * just one too large.  (Note: it's OK to use dividend[j+2] here
       * because we know the divisor length is at least 2.)
       */
      while(divisor2 * qhat > (next2digits - qhat * divisor1) * NBASE + dividend[j + 2])
        qhat--;

      /* As above, need do nothing more when quotient digit is 0 */
      if(qhat > 0)
      {
        /*
         * Multiply the divisor by qhat, and subtract that from the
         * working dividend.  "carry" tracks the multiplication,
         * "borrow" the subtraction (could we fold these together?)
         */
        int carry = 0;
        int borrow = 0;
        for(int i = _rhs.m_digits.size(); i >= 0; i--)
        {
          carry += divisor[i] * qhat;
          borrow -= carry % NBASE;
          carry = carry / NBASE;
          borrow += dividend[j + i];
          if(borrow < 0)
          {
            dividend[j + i] = borrow + NBASE;
            borrow = -1;
          }
          else
          {
            dividend[j + i] = borrow;
            borrow = 0;
          }
        }
        clog_assert(carry == 0);

        /*
         * If we got a borrow out of the top dividend digit, then
         * indeed qhat was one too large.  Fix it, and add back the
         * divisor to correct the working dividend.  (Knuth proves
         * that this will occur only about 3/NBASE of the time; hence,
         * it's a good idea to test this code with small NBASE to be
         * sure this section gets exercised.)
         */
        if(borrow)
        {
          qhat--;
          carry = 0;
          for(int i = _rhs.m_digits.size(); i >= 0; i--)
          {
            carry += dividend[j + i] + divisor[i];
            if(carry >= NBASE)
            {
              dividend[j + i] = carry - NBASE;
              carry = 1;
            }
            else
            {
              dividend[j + i] = carry;
              carry = 0;
            }
          }
          /* A carry should occur here to cancel the borrow above */
          clog_assert(carry == 1);
        }
      }

      /* And we're done with this quotient digit */
      res.m_digits[j] = qhat;
    }
  }

  res.roundVar();
  res.simplify();
  return res;
}

BigNumber BigNumber::operator-() const
{
  BigNumber r = *this;
  switch(r.sign())
  {
  case Sign::Positive:
    r.m_sign = Sign::Negative;
    break;
  case Sign::Negative:
    r.m_sign = Sign::Positive;
    break;
  case Sign::PositiveInfinite:
    r.m_sign = Sign::NegativeInfinite;
    break;
  case Sign::NegativeInfinite:
    r.m_sign = Sign::PositiveInfinite;
    break;
  case Sign::NaN:
    break;
  }
  return r;
}

bool BigNumber::operator<(const BigNumber& _rhs) const
{
  return compare(_rhs) == ComparisonResult::Bigger;
}

bool BigNumber::operator<=(const BigNumber& _rhs) const
{
  ComparisonResult cr = compare(_rhs);
  return cr == ComparisonResult::Bigger or cr == ComparisonResult::Equal;
}

bool BigNumber::operator>(const BigNumber& _rhs) const
{
  return compare(_rhs) == ComparisonResult::Smaller;
}

bool BigNumber::operator>=(const BigNumber& _rhs) const
{
  ComparisonResult cr = compare(_rhs);
  return cr == ComparisonResult::Smaller or cr == ComparisonResult::Equal;
}
