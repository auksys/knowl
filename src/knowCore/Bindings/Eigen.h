#include <Eigen/Core>
#include <knowCore/Vector.h>

namespace knowCore::Bindings
{
  template<typename _Scalar, std::size_t _Size>
  void convert(const Vector<_Scalar, _Size>& _src, Eigen::Matrix<_Scalar, (int)_Size, 1>& _dst)
  {
    _dst = Eigen::Map<const Eigen::Matrix<_Scalar, (int)_Size, 1>>(_src.data());
  }
  template<typename _Scalar, std::size_t _Size>
  Eigen::Matrix<_Scalar, (int)_Size, 1> convert(const Vector<_Scalar, _Size>& _src)
  {
    Eigen::Matrix<_Scalar, (int)_Size, 1> v;
    convert(_src, v);
    return v;
  }
} // namespace knowCore::Bindings
