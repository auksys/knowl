#include <ag/qt.h>
#include <knowCore/Value.h>

namespace ag
{
  template<typename _T_>
    requires(knowCore::HasMetaTypeInformationV<_T_> and not std::is_same_v<_T_, QString>)
  struct TypeSupport<_T_, false>
  {
    static void hash(QCryptographicHash* _hash, const _T_& _t)
    {
      _hash->addData(knowCore::Value::fromValue(_t).md5().expect_success());
    }
    static _T_ init() { return _T_(); }
    static void clean(_T_* _t) { *_t = _T_(); }
    static _T_ clone(const _T_ _t) { return _t; }
  };

  template<typename _T_>
    requires(knowCore::HasMetaTypeInformationV<_T_> and not std::is_same_v<_T_, QString>)
  struct SerialisationSupport<_T_, false>
  {
    static QJsonValue toJson(const _T_& _t)
    {
      return knowCore::Value::fromValue(_t).toJsonValue().expect_success();
    }
    static bool fromJson(_T_* _t, const QJsonValue& _value, QString* _errorMessage)
    {
      auto const [success, value, error]
        = knowCore::Value::fromJsonValue(knowCore::MetaTypeInformation<_T_>::uri(), _value);
      if(success)
      {
        auto const [success_value, value_t, error_value] = value.value().template value<_T_>();
        if(success_value)
        {
          *_t = value_t.value();
          return true;
        }
        else
        {
          *_errorMessage = error_value.value().get_message();
          return false;
        }
      }
      else
      {
        *_errorMessage = error.value().get_message();
        return false;
      }
    }
    static QCborValue toCbor(const _T_ _t)
    {
      return knowCore::Value::fromValue(_t).toCborValue().expect_success();
    }
    static bool fromCbor(_T_* _t, const QCborValue& _value, QString* _errorMessage)
    {
      auto const [success, value, error]
        = knowCore::Value::fromCborValue(knowCore::MetaTypeInformation<_T_>::uri(), _value);
      if(success)
      {
        auto const [success_value, value_t, error_value] = value.value().template value<_T_>();
        if(success_value)
        {
          *_t = value_t.value();
          return true;
        }
        else
        {
          *_errorMessage = error_value.value().get_message();
          return false;
        }
      }
      else
      {
        *_errorMessage = error.value().get_message();
        return false;
      }
    }
  };

} // namespace ag
