#include "CSVWriter.h"

#include <QTextStream>
#include <QVariant>

using namespace knowCore;

struct CSVWriter::Private
{
  QTextStream stream;
  QByteArray seperator;
  int fields;
  int current_field = 0;
  template<typename _T_>
  void write(const _T_& _value);
  void write(const QString& _value);
  void nextCell();
};

template<typename _T_>
void CSVWriter::Private::write(const _T_& _value)
{
  stream << _value;
  nextCell();
}

void knowCore::CSVWriter::Private::write(const QString& _value)
{
  stream << "\"";
  stream << _value;
  stream << "\"";
  nextCell();
}

void CSVWriter::Private::nextCell()
{
  ++current_field;
  if(fields == current_field)
  {
    current_field = 0;
    stream << '\n';
  }
  else
  {
    stream << seperator;
  }
}

CSVWriter::CSVWriter(QIODevice* _device, int _fields, const QByteArray& _seperator) : d(new Private)
{
  d->stream.setDevice(_device);
  d->seperator = _seperator;
  d->fields = _fields;
}

CSVWriter::~CSVWriter() { delete d; }

CSVWriter& CSVWriter::operator<<(int _value)
{
  d->write(_value);
  return *this;
}

CSVWriter& CSVWriter::operator<<(const QString& _value)
{
  d->write(_value);
  return *this;
}

CSVWriter& CSVWriter::operator<<(const QVariant& _value)
{
  switch(_value.typeId())
  {
  case QVariant::Bool:
    d->write(_value.value<bool>());
    break;
  case QVariant::Int:
    d->write(_value.value<int>());
    break;
  case QVariant::UInt:
    d->write(_value.value<uint>());
    break;
  case QVariant::LongLong:
    d->write(_value.value<long long>());
    break;
  case QVariant::ULongLong:
    d->write(_value.value<unsigned long long>());
    break;
  case QVariant::Double:
    d->write(_value.value<double>());
    break;
  case QVariant::String:
    d->write(_value.value<QString>());
    break;
  case QVariant::ByteArray:
    d->write(_value.value<QByteArray>());
    break;
  default:
    d->write(_value.toString());
    break;
  }
  return *this;
}
