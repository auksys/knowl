#include <QByteArray>

class QIODevice;
class QString;
class QVariant;

namespace knowCore
{
  class CSVWriter
  {
  public:
    CSVWriter(QIODevice* _device, int _fields, const QByteArray& _seperator = ",");
    ~CSVWriter();
    CSVWriter& operator<<(int _value);
    CSVWriter& operator<<(const QString& _value);
    CSVWriter& operator<<(const QVariant& _value);
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowCore
