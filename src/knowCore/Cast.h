#include <type_traits>

#include "ConstExplicitlySharedDataPointer.h"
#include "Global.h"

namespace knowCore
{
  template<typename _T2_, typename _T_>
    requires details::base_of<_T2_, _T_>
  QList<_T2_*> listCast(const QList<_T_*>& _list)
  {
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
    return *reinterpret_cast<const QList<_T2_*>*>(&_list);
#pragma GCC diagnostic error "-Wstrict-aliasing"
  }
  template<typename _T_>
  const QList<_T_>& listCast(const QList<_T_>& _list)
  {
    return _list;
  }
  template<typename _T2_, typename _T_>
    requires details::base_of<typename _T2_::Type, _T_>
  QList<_T2_> listCast(const QList<ConstExplicitlySharedDataPointer<_T_>>& _list)
  {
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
    return *reinterpret_cast<const QList<_T2_>*>(&_list);
#pragma GCC diagnostic error "-Wstrict-aliasing"
  }
  template<typename _T2_, typename _T_>
    requires(std::is_base_of_v<_T2_, _T_>)
  QList<const _T2_*> listCast(const QList<ConstExplicitlySharedDataPointer<_T_>>& _list)
  {
    QList<const _T2_*> transitions;
    for(const ConstExplicitlySharedDataPointer<_T_>& t : _list)
    {
      transitions.append(t.data());
    }
    return transitions;
  }
  template<typename _T2_, typename _T_>
    requires(not details::base_of<_T2_, _T_>)
            and std::convertible_to<_T_, _T2_> and (not std::same_as<_T_, _T2_>)
  QList<_T2_> listCast(const QList<_T_>& _list)
  {
    QList<_T2_> transitions;
    for(const _T_& t : _list)
    {
      transitions.append(t);
    }
    return transitions;
  }
  template<typename _T_>
  QVector<_T_> vectorCast(const QList<_T_>& _list)
  {
    QVector<_T_> transitions;
    std::copy(_list.begin(), _list.end(), std::back_inserter(transitions));
    return transitions;
  }
} // namespace knowCore
