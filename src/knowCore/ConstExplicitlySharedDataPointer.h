#ifndef _KNOW_CORE_CONSTEXPLICITLYSHAREDDATAPOINTER_H_
#define _KNOW_CORE_CONSTEXPLICITLYSHAREDDATAPOINTER_H_

#include <QSharedDataPointer>

namespace knowCore
{
  /**
   * @ingroup knowCore
   * This class can be used to hold a class that inherits from \ref QSharedData as a smart pointer.
   */
  template<class T>
  class ConstExplicitlySharedDataPointer
  {
  public:
    typedef T Type;
    typedef T* pointer;

    inline const T& operator*() const { return *d; }
    inline const T* operator->() const { return d; }
    inline operator bool() const { return d; }
    inline const T* data() const { return d; }
    inline const T* constData() const { return d; }

    inline bool operator==(const ConstExplicitlySharedDataPointer<T>& other) const
    {
      return d == other.d;
    }
    inline bool operator!=(const ConstExplicitlySharedDataPointer<T>& other) const
    {
      return d != other.d;
    }

    inline ConstExplicitlySharedDataPointer() { d = 0; }
    inline ~ConstExplicitlySharedDataPointer()
    {
      if(d && !d->ref.deref())
      {
        delete d;
      }
    }

    inline ConstExplicitlySharedDataPointer(const T* data) : d(const_cast<T*>(data))
    {
      if(d)
      {
        d->ref.ref();
      }
    }
    inline ConstExplicitlySharedDataPointer(const ConstExplicitlySharedDataPointer& o) : d(o.d)
    {
      if(d)
      {
        d->ref.ref();
      }
    }
    template<typename _T2_>
      requires(std::is_convertible_v<_T2_*, T*>)
    inline ConstExplicitlySharedDataPointer(const ConstExplicitlySharedDataPointer<_T2_>& o)
        : d(const_cast<_T2_*>(o.data()))
    {
      if(d)
      {
        d->ref.ref();
      }
    }
    inline ConstExplicitlySharedDataPointer(const QExplicitlySharedDataPointer<T>& o)
        : d(const_cast<T*>(o.data()))
    {
      if(d)
      {
        d->ref.ref();
      }
    }
    inline ConstExplicitlySharedDataPointer<T>&
      operator=(const ConstExplicitlySharedDataPointer<T>& o)
    {
      if(o.d != d)
      {
        if(o.d)
        {
          o.d->ref.ref();
        }

        if(d && !d->ref.deref())
        {
          delete d;
        }

        d = o.d;
      }

      return *this;
    }
    inline ConstExplicitlySharedDataPointer& operator=(T* o)
    {
      if(o != d)
      {
        if(o)
        {
          o->ref.ref();
        }

        if(d && !d->ref.deref())
        {
          delete d;
        }

        d = o;
      }

      return *this;
    }
    template<typename _T2_>
      requires(std::is_convertible_v<_T2_*, T*>)
    inline ConstExplicitlySharedDataPointer<_T2_> d_cast() const
    {
      return ConstExplicitlySharedDataPointer<_T2_>(dynamic_cast<_T2_*>(d));
    }
    template<typename _T2_>
      requires(std::is_convertible_v<_T2_*, T*>)
    inline ConstExplicitlySharedDataPointer<_T2_> s_cast() const
    {
      return ConstExplicitlySharedDataPointer<_T2_>(static_cast<_T2_*>(d));
    }

    inline bool operator!() const { return !d; }

    inline void swap(ConstExplicitlySharedDataPointer& other) { qSwap(d, other.d); }
  private:
    T* d;
  };

} // namespace knowCore

#endif
