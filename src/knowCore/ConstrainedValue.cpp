#include "ConstrainedValue.h"

#include "Range.h"

using namespace knowCore;

struct ConstrainedValue::Private : public QSharedData
{
  QList<Constraint> constraints;

  bool hasConstraint(Type _type1, Type _type2) const;
  bool hasConstraint(Type _type1) const { return hasConstraint(_type1, _type1); }
  bool hasNoConstraint() const { return constraints.isEmpty(); }

  /**
   * Inferior or within constraints need to be swaped
   */
  static ComparisonOperators comparisonOperators(Type _type);
};

inline bool ConstrainedValue::Private::hasConstraint(Type _type1, Type _type2) const
{
  for(const Constraint& c : constraints)
  {
    if(c.type == _type1 or c.type == _type2)
    {
      return true;
    }
  }
  return false;
}

inline ComparisonOperators ConstrainedValue::Private::comparisonOperators(Type _type)
{
  switch(_type)
  {
  case Type::Equal:
  case Type::Different:
    return ComparisonOperator::Equal;
  case Type::Inferior:
  case Type::Superior:
    return ComparisonOperator::Inferior;
  case Type::InferiorEqual:
  case Type::SuperiorEqual:
    return ComparisonOperator((int)ComparisonOperator::Inferior | (int)ComparisonOperator::Equal);
  case Type::GeoOverlaps:
    return ComparisonOperator::GeoOverlaps;
  case Type::GeoTouches:
    return ComparisonOperator::GeoTouches;
  case Type::GeoIntersects:
  case Type::GeoDisjoint:
    return ComparisonOperator::GeoIntersects;
  case Type::GeoContains:
    return ComparisonOperator::GeoContains;
  case Type::GeoWithin:
    return ComparisonOperator::GeoWithin;
  case Type::Contains:
  case Type::NotContains:
  case Type::In:
  case Type::NotIn:
    return ComparisonOperator::In;
  }
  return ComparisonOperators();
}

ConstrainedValue::ConstrainedValue() : d(new Private) {}

ConstrainedValue::ConstrainedValue(const ConstrainedValue& _rhs) : d(_rhs.d) {}

ConstrainedValue& ConstrainedValue::operator=(const ConstrainedValue& _rhs)
{
  d = _rhs.d;
  return *this;
}

ConstrainedValue::~ConstrainedValue() {}

bool ConstrainedValue::operator==(const ConstrainedValue& _rhs) const
{
  if(d == _rhs.d)
    return true;
  if(d->constraints.size() != _rhs.d->constraints.size())
    return false;
  for(int i = 0; i < d->constraints.size(); ++i)
  {
    if(d->constraints[i] != _rhs.d->constraints[i])
      return false;
  }
  return true;
}

bool ConstrainedValue::hasConstraints() const { return not d->constraints.isEmpty(); }

QList<ConstrainedValue::Constraint> ConstrainedValue::constraints() const { return d->constraints; }

ConstrainedValue& ConstrainedValue::apply(const ConstrainedValue::Constraint& _constraint)
{
  d->constraints.append(_constraint);
  return *this;
}

ConstrainedValue& ConstrainedValue::apply(const Value& _value, ConstrainedValue::Type _type)
{
  apply({_value, _type});
  return *this;
}

cres_qresult<ValueRange> ConstrainedValue::update(const ValueRange& _range)
{
  ValueRange range = _range;
  for(const Constraint& constraint : constraints())
  {
    using CT = knowCore::ConstrainedValue::Type;
    using RBT = RangeBoundType;
    switch(constraint.type)
    {
    case CT::Equal:
    {
      cres_try(bool c, range.contains(constraint.value));
      if(not c)
        return cres_failure("Constraint {} is not in range {}.", constraint.value, range);
      range = ValueRange(RBT::Inclusive, constraint.value, constraint.value, RBT::Inclusive);
      break;
    }
    case CT::Inferior:
    {
      cres_try(bool c, range.contains(constraint.value));
      if(not c)
        return cres_failure("Constraint {} is not in range {}.", constraint.value, range);
      range = ValueRange(range.lowerBoundType(), range.lower(), constraint.value, RBT::Exclusive);
      break;
    }
    case CT::Superior:
    {
      cres_try(bool c, range.contains(constraint.value));
      if(not c)
        return cres_failure("Constraint {} is not in range {}.", constraint.value, range);
      range = ValueRange(RBT::Exclusive, constraint.value, range.upper(), range.upperBoundType());
      break;
    }
    case CT::InferiorEqual:
    {
      cres_try(bool c, range.contains(constraint.value));
      if(not c)
        return cres_failure("Constraint {} is not in range {}.", constraint.value, range);
      range = ValueRange(range.lowerBoundType(), range.lower(), constraint.value, RBT::Inclusive);
      break;
    }
    case CT::SuperiorEqual:
    {
      cres_try(bool c, range.contains(constraint.value));
      if(not c)
        return cres_failure("Constraint {} is not in range {}.", constraint.value, range);
      range = ValueRange(RBT::Inclusive, constraint.value, range.upper(), range.upperBoundType());
      break;
    }
    case CT::Different:
    case CT::GeoOverlaps:
    case CT::GeoWithin:
    case CT::GeoContains:
    case CT::GeoIntersects:
    case CT::GeoTouches:
    case CT::GeoDisjoint:
    case CT::Contains:
    case CT::NotContains:
    case CT::In:
    case CT::NotIn:
      return cres_failure("Unsupported constraint {}", constraint.type, constraint.value);
    }
    clog_debug_vn(range, range.lower(), range.lowerBoundType(), range.upper(),
                  range.upperBoundType());
  }
  return cres_success(range);
}

cres_qresult<bool> ConstrainedValue::check(const Value& _value)
{
  for(const Constraint& constraint : d->constraints)
  {
    knowCore::Value a = _value;
    knowCore::Value b = constraint.value;
    switch(constraint.type)
    {
    case Type::Superior:
    case Type::SuperiorEqual:
    case Type::Contains:
    case Type::NotContains:
      std::swap(a, b);
      break;
    default:
      break;
    }
    cres_try(bool comparison, a.compare(b, Private::comparisonOperators(constraint.type)));
    switch(constraint.type)
    {
    case Type::Different:
    case Type::NotIn:
    case Type::NotContains:
      comparison = not comparison;
      break;
    default:
      break;
    }
    if(not comparison)
    {
      return cres_success(false);
    }
  }
  return cres_success(true);
}
