#pragma once

#include <QSharedDataPointer>

#include "Value.h"

namespace knowCore
{
  /**
   * @ingroup knowCore
   * This class allows to represent a \ref ConstrainedValue with lower, upper or equality
   * constraint.
   */
  class ConstrainedValue
  {
  public:
    enum class Type
    {
      Equal,
      Different,
      Inferior,      ///< such as value < constraint.value
      Superior,      ///< such as constraint.value < value
      InferiorEqual, ///< such as value <= constraint.value
      SuperiorEqual, ///< such as constraint.value <= value
      GeoOverlaps,   ///< such as value overlaps with constraint.value (but they cannot share a
                     ///< boundary)
      GeoWithin,     ///< such as value is within the constraint.value
      GeoContains,   ///< such as constraint.value is within the value
      GeoIntersects, ///< such as value intersects with constraint.value,
      GeoTouches,    ///< such as value touches with constraint.value,
      GeoDisjoint,   ///< such as value disjoint with constraint.value,
      Contains,      ///< such as list contains a value
      NotContains,   ///< such as list does not contains a value
      In,            ///< such as one value is part of a list
      NotIn          ///< such as one value is not part of a list
    };
    struct Constraint
    {
      Value value;
      Type type;
      bool operator==(const Constraint& _rhs) const
      {
        return value == _rhs.value and type == _rhs.type;
      }
      bool operator!=(const Constraint& _rhs) const { return not(*this == _rhs); }
    };
  public:
    ConstrainedValue();
    ConstrainedValue(const ConstrainedValue& _rhs);
    ConstrainedValue& operator=(const ConstrainedValue& _rhs);
    ~ConstrainedValue();

    bool operator==(const ConstrainedValue& _value) const;

    /**
     * @return true if this constrained value has constrained
     */
    bool hasConstraints() const;
    QList<Constraint> constraints() const;
    cres_qresult<bool> check(const Value& _value);
    /**
     * Update a range according to the constraint.
     */
    cres_qresult<ValueRange> update(const ValueRange& _range);
    template<typename _T_>
    cres_qresult<bool> check(const _T_& _value);
    /**
     * Apply a constraint on this value.
     * @return a reference to self
     */
    ConstrainedValue& apply(const Constraint& _constraint);
    /**
     * Apply a constraint on this value, with the given \ref _value and \ref _type.
     * @return a reference to self
     */
    ConstrainedValue& apply(const Value& _value, Type _type);
    template<typename _T_>
    ConstrainedValue& apply(const _T_& _value, Type _type);
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
  template<typename _T_>
  cres_qresult<bool> ConstrainedValue::check(const _T_& _value)
  {
    return check(Value::fromValue(_value));
  }
  template<typename _T_>
  ConstrainedValue& ConstrainedValue::apply(const _T_& _value, Type _type)
  {
    return apply(Value::fromValue(_value), _type);
  }
} // namespace knowCore

#include <knowCore/Formatter.h>

clog_format_declare_enum_formatter(knowCore::ConstrainedValue::Type, Equal, Different, Inferior,
                                   Superior, InferiorEqual, SuperiorEqual, GeoOverlaps, GeoWithin,
                                   GeoContains, GeoIntersects, Contains, In);

clog_format_declare_formatter(knowCore::ConstrainedValue::Constraint)
{
  return format_to(ctx.out(), "{} {}", p.type, p.value);
}

clog_format_declare_formatter(knowCore::ConstrainedValue)
{
  return format_to(ctx.out(), "{:s' and '}", p.constraints());
}
