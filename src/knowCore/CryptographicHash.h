#pragma once

#include <QCryptographicHash>
#include <QDateTime>
#include <QUuid>

namespace knowCore
{
  /**
   * This class provide convenient extensions to QCryptographicHash
   */
  class CryptographicHash : public QCryptographicHash
  {
  public:
    explicit CryptographicHash(Algorithm method) : QCryptographicHash(method) {}
    ~CryptographicHash() {}
    using QCryptographicHash::addData;

    void addData(const QUuid& _uuid) { addData(_uuid.toByteArray()); }
    void addData(const QDateTime& _time) { addData(_time.toMSecsSinceEpoch()); }
    template<typename _T_>
      requires(std::is_fundamental_v<_T_>)
    void addData(_T_ _t)
    {
      addData(reinterpret_cast<char*>(&_t), sizeof(_t));
    }
  };
} // namespace knowCore
