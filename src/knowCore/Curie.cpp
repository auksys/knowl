/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "Curie.h"

#include <QString>

#include "Uri.h"
#include "UriManager.h"

using namespace knowCore;

struct Curie::Private : public QSharedData
{
  QString prefix, reference;
};

Curie::Curie() : d(new Private) {}

Curie::Curie(const QString& _prefix, const QString& _reference) : d(new Private)
{
  d->prefix = _prefix;
  d->reference = _reference;
}

Curie::Curie(const knowCore::Curie& _curie) : d(_curie.d) {}

Curie& Curie::operator=(const Curie& _rhs)
{
  d = _rhs.d;
  return *this;
}

Curie::~Curie() {}

QString Curie::prefix() const { return d->prefix; }

QString Curie::suffix() const { return d->reference; }

bool Curie::canResolve(UriManager* _manager) const { return _manager->hasPrefix(d->prefix); }

Uri Curie::resolve(UriManager* _manager) { return _manager->uri(d->prefix, d->reference); }

bool Curie::operator==(const Curie& _rhs) const
{
  return d->prefix == _rhs.d->prefix and d->reference == _rhs.d->reference;
}

// #include <QDebug>
//
// QDebug operator<<(QDebug dbg, const knowCore::Curie & curie)
// {
//   dbg << curie.prefix() + ":" + curie.suffix();
//   return dbg;
// }
