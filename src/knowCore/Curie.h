/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#pragma once

#include <QSharedDataPointer>

namespace knowCore
{
  class Uri;
  class UriManager;
  /**
   * Represents a "Curie" as defined in @link http://www.w3.org/TR/curie/
   * A Curie is an URL written with the form "prefix:reference".
   *
   * For instance: "foaf:name" if foaf corresponds to the URL "\<http://xmlns.com/foaf/0.1/\>"
   *               then "foaf:name" get converted to "\<http://xmlns.com/foaf/0.1/name\>"
   */
  class Curie
  {
  public:
    /**
     * Create an empty Curie
     */
    Curie();
    Curie(const QString& _prefix, const QString& _suffix);
    Curie(const Curie& _rhs);
    Curie& operator=(const Curie& _rhs);
    ~Curie();
    QString prefix() const;
    QString suffix() const;
    bool canResolve(UriManager* _manager) const;
    /**
     * Convert the Curie into an url, ie, replace the prefix by its actual content.
     */
    Uri resolve(UriManager* _manager);
    bool operator==(const Curie& _rhs) const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
} // namespace knowCore

// QDebug operator<<(QDebug, const knowCore::Curie &);
