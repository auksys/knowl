#include "FileFormat.h"

#include <QFileInfo>
#include <QMimeDatabase>
#include <QUrl>

using namespace knowCore;

QString FileFormat::Turtle = "turtle";
QString FileFormat::JSON = "json";
QString FileFormat::XML = "xml";
QString FileFormat::SRX = "srx";
QString FileFormat::CBOR = "cbor";
QString FileFormat::CSV = "csv";
QString FileFormat::Auto = "auto";

cres_qresult<QString> knowCore::mimeToFormat(const QString& _mime)
{
  if(_mime == "text/turtle")
  {
    return cres_success(FileFormat::Turtle);
  }
  else if(_mime == "application/json")
  {
    return cres_success(FileFormat::JSON);
  }
  else if(_mime == "application/xml")
  {
    return cres_success(FileFormat::XML);
  }
  else if(_mime == "text/csv")
  {
    return cres_success(FileFormat::CSV);
  }
  else if(_mime == "application/cbor")
  {
    return cres_success(FileFormat::CBOR);
  }
  return cres_failure("Unknown file format for: {}", _mime);
}

cres_qresult<QString> knowCore::formatFor(const QByteArray& _data)
{
  QMimeType qmt = QMimeDatabase().mimeTypeForData(_data);
  return mimeToFormat(qmt.name());
}

cres_qresult<QString> knowCore::formatFor(const QFileInfo& _fileInfo)
{
  QMimeType qmt = QMimeDatabase().mimeTypeForFile(_fileInfo);
  cres_qresult<QString> format = mimeToFormat(qmt.name());
  if(format.is_successful())
  {
    return format;
  }
  else
  {
    if(_fileInfo.suffix().toLower() == "srx")
    {
      return cres_success(FileFormat::SRX);
    }
    else if(_fileInfo.suffix().toLower() == "cbor")
    {
      return cres_success(FileFormat::CBOR);
    }
    else if(_fileInfo.suffix().toLower() == "ttl" or _fileInfo.suffix().toLower() == "nt")
    {
      return cres_success(FileFormat::Turtle);
    }
    else
    {
      return cres_failure("{} for file {}", format.get_error(), _fileInfo.fileName());
    }
  }
}

cres_qresult<QString> knowCore::formatFor(const QString& _filename)
{
  return formatFor(QFileInfo(_filename));
}

cres_qresult<QString> knowCore::formatFor(const QUrl& _filename)
{
  return formatFor(_filename.toLocalFile());
}
