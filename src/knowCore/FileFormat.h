/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#pragma once

#include <clog_qt>
#include <cres_qt>

#include <QString>

class QFileInfo;
class QUrl;

namespace knowCore
{
  class FileFormat
  {
  public:
    static QString Turtle;
    static QString JSON;
    static QString XML;
    static QString SRX;
    static QString CBOR;
    static QString CSV;
    static QString Auto;
  };
  cres_qresult<QString> mimeToFormat(const QString& _mime);
  cres_qresult<QString> formatFor(const QByteArray& _data);
  cres_qresult<QString> formatFor(const QFileInfo& _fileInfo);
  cres_qresult<QString> formatFor(const QString& _filename);
  cres_qresult<QString> formatFor(const QUrl& _filename);
} // namespace knowCore
