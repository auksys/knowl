#pragma once

#include <clog_format>
#include <clog_qt>

#include "Global.h"

namespace knowCore
{
  template<typename _TYPE_>
  struct printable_formatter : public std::formatter<QString>
  {
    template<typename FormatContext>
    auto format(_TYPE_ const& p, FormatContext& ctx) const
    {
      auto const [success, value, message] = p.printable();
      if(success)
      {
        return std::formatter<QString>::format(value.value(), ctx);
      }
      else
      {
        return std::format_to(ctx.out(), "error: '{}'", message.value());
      }
    }
  };
} // namespace knowCore

#define KNOWCORE_CORE_DECLARE_FORMATTER_PRINTABLE(_TYPE_)                                          \
  template<>                                                                                       \
  struct std::formatter<_TYPE_> : public knowCore::printable_formatter<_TYPE_>                     \
  {                                                                                                \
  }
