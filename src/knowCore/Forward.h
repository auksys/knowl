#pragma once

#include <cres_qt_forward>

template<class T>
class QSharedPointer;

// Qt's containers
template<typename, typename>
class QHash;
template<typename, typename>
class QMap;
template<typename>
class QList;

// Qt's classes
class QByteArray;
class QCborValue;
class QIODevice;
class QJsonValue;
class QString;
using QStringList = QList<QString>;
class QUrl;
class QVariant;

namespace knowCore
{
  template<typename _T_, typename... _Annotation_>
  class AnnotatedPointer;
  class BigNumber;
  class ConstrainedValue;
  template<class T>
  class ConstExplicitlySharedDataPointer;
  class Curie;
  class Timestamp;
  class DeserialisationContexts;
  class Image;
  class LexerTextStream;
  class Messages;
  template<class T>
  class MetaType;
  template<class T, class _Enabled_ = void>
  class MetaTypeDefinition;
  template<typename _T_>
  class MonitoredValueController;
  template<typename _T_, _T_ _default_>
  struct PointerAnnotation;
  template<typename _T_>
  class QuantityValue;
  template<typename _T_>
  class Range;
  class SerialisationContexts;
  template<typename _T_>
  class TimeStamped;
  class Uri;
  class UriList;
  class UriManager;
  class Value;
  class ValueHash;
  class ValueList;
  template<typename _T_>
  class WeakReference;
  namespace Uris
  {
    template<typename _T_>
    struct IsUriDefinition;
    template<typename T>
    inline constexpr bool IsUriDefinitionV = IsUriDefinition<T>::value;
  } // namespace Uris
  namespace Interfaces
  {
    template<typename _T_>
    class Iterable;
    template<typename _T_>
    using IterableSP = QSharedPointer<Iterable<_T_>>;
  } // namespace Interfaces
  // QuantityValue defines
  using QuantityNumber = QuantityValue<BigNumber>;
  // Range defines
  using NumberRange = Range<BigNumber>;
  using QuantityNumberRange = Range<QuantityNumber>;
  using ValueRange = Range<Value>;
} // namespace knowCore
