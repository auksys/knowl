#include "Global.h"

#ifdef __GNU_LIBRARY__

#include <cxxabi.h>

QString knowCore::details::demangle(const char* _mangled_symbol)
{
  int status;
  char* realname = abi::__cxa_demangle(_mangled_symbol, 0, 0, &status);
  if(status == 0)
  {
    QString str = QString::fromLatin1(realname);
    free(realname);
    return str;
  }
  else
  {
    return QString::fromLatin1(_mangled_symbol);
  }
}

#else

QString knowCore::details::demangle(const char* _mangled_symbol)
{
  return QString::fromLatin1(_mangled_symbol);
}

#endif
