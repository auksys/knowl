#pragma once

#include <QList>
#include <QSharedPointer>
#include <QString>
#include <type_traits>

#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
#define KNOWCORE_QT_SKIP_EMPTY_PART Qt::SkipEmptyParts
#else
#define KNOWCORE_QT_SKIP_EMPTY_PART QString::SkipEmptyParts
#endif

#ifdef __GNU_LIBRARY__
#ifndef __clang__
#define KNOWCORE_GLIBC_CONSTEXPR constexpr
#endif
#endif

// BEGIN knowCore Macros

#ifndef KNOWCORE_GLIBC_CONSTEXPR
#define KNOWCORE_GLIBC_CONSTEXPR
#endif

#if defined __has_builtin
#define __KNOWCORE_HAS_BUILTIN(_X_) __has_builtin(_X_)
#else
#define __KNOWCORE_HAS_BUILTIN(_X_) 0
#endif

#define __KNOWCORE_STR_2(_X_) #_X_
#define __KNOWCORE_STR(_X_) __KNOWCORE_STR_2(_X_)

#define __KNOWCORE_CAT(a, b) __KNOWCORE_CAT_I(a, b)
#define __KNOWCORE_CAT_I(a, b) __KNOWCORE_CAT_II(~, a##b)
#define __KNOWCORE_CAT_II(p, res) res

#ifdef __COUNTER__
#define __KNOWCORE_UNIQUE_STATIC_NAME(_X_) __KNOWCORE_CAT(_X_, __COUNTER__)
#else
#define __KNOWCORE_UNIQUE_STATIC_NAME(_X_) __KNOWCORE_CAT(_X_, __LINE__)
#endif

#define __KNOWCORE_GET_2ND_ARG(s, arg1, arg2, ...) arg2

#define KNOWCORE_UNPACK(...) __VA_ARGS__
#define KNOWCORE_COMMA ,

// BEGIN knowCore Foreach

// The following macros are for internal use by knowCore, no warranty of behavior is guaranteed

#define __KNOWCORE_INC(I) __KNOWCORE_INC_##I
#define __KNOWCORE_INC_0 1
#define __KNOWCORE_INC_1 2
#define __KNOWCORE_INC_2 3
#define __KNOWCORE_INC_3 4
#define __KNOWCORE_INC_4 5
#define __KNOWCORE_INC_5 6
#define __KNOWCORE_INC_6 7
#define __KNOWCORE_INC_7 8
#define __KNOWCORE_INC_8 9
#define __KNOWCORE_INC_9 10
#define __KNOWCORE_INC_10 11
#define __KNOWCORE_INC_11 12
#define __KNOWCORE_INC_12 13
#define __KNOWCORE_INC_13 14
#define __KNOWCORE_INC_14 15
#define __KNOWCORE_INC_15 16
#define __KNOWCORE_INC_16 17
#define __KNOWCORE_INC_17 18
#define __KNOWCORE_INC_18 19
#define __KNOWCORE_INC_19 20
#define __KNOWCORE_INC_20 21

#define __KNOWCORE_FOREACH__0(WHAT, SEP, I, ...)
#define __KNOWCORE_FOREACH__1(WHAT, SEP, I, X, ...) WHAT(X, I)
#define __KNOWCORE_FOREACH__2(WHAT, SEP, I, X, ...)                                                \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__1(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__3(WHAT, SEP, I, X, ...)                                                \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__2(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__4(WHAT, SEP, I, X, ...)                                                \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__3(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__5(WHAT, SEP, I, X, ...)                                                \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__4(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__6(WHAT, SEP, I, X, ...)                                                \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__5(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__7(WHAT, SEP, I, X, ...)                                                \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__6(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__8(WHAT, SEP, I, X, ...)                                                \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__7(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__9(WHAT, SEP, I, X, ...)                                                \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__8(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__10(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__9(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__11(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__10(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__12(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__11(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__13(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__12(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__14(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__13(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__15(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__14(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__16(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__15(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__17(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__16(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__18(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__17(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__19(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__18(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__20(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__19(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__21(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__20(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__22(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__21(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__23(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__22(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__24(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__23(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__25(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__24(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__26(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__25(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__27(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__26(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__28(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__27(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__29(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__28(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)
#define __KNOWCORE_FOREACH__30(WHAT, SEP, I, X, ...)                                               \
  WHAT(X, I) KNOWCORE_UNPACK SEP __KNOWCORE_FOREACH__29(WHAT, SEP, __KNOWCORE_INC(I), __VA_ARGS__)

#define __KNOWCORE_FOREACH_GET_MACRO(_0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13,   \
                                     _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25,   \
                                     _26, _27, _28, _29, NAME, ...)                                \
  NAME

/**
 * Execute action(X, I) on each variadic arguments.
 * X will contains the argument and I the index.
 * And add sep between each element
 *
 * @code
 * #define PRINT(X, I) std::cout << " " # I ": " << X;
 * KNOWCORE_FOREACH(PRINT, on_failure();, a, b, c)
 * @endcode
 *
 * Will expand into:
 *
 * @code
 * std::cout << " " "0" ": " << a; on_failure();
 * std::cout << " " "1" ": " << b; on_failure();
 * std::cout << " " "2" ": " << c;
 * @endcode
 */
#define KNOWCORE_FOREACH_SEP(action, sep, ...)                                                     \
  __KNOWCORE_FOREACH_GET_MACRO(                                                                    \
    void, __VA_ARGS__ __VA_OPT__(, ) __KNOWCORE_FOREACH__29, __KNOWCORE_FOREACH__28,               \
    __KNOWCORE_FOREACH__27, __KNOWCORE_FOREACH__26, __KNOWCORE_FOREACH__25,                        \
    __KNOWCORE_FOREACH__24, __KNOWCORE_FOREACH__23, __KNOWCORE_FOREACH__22,                        \
    __KNOWCORE_FOREACH__21, __KNOWCORE_FOREACH__20, __KNOWCORE_FOREACH__19,                        \
    __KNOWCORE_FOREACH__18, __KNOWCORE_FOREACH__17, __KNOWCORE_FOREACH__16,                        \
    __KNOWCORE_FOREACH__15, __KNOWCORE_FOREACH__14, __KNOWCORE_FOREACH__13,                        \
    __KNOWCORE_FOREACH__12, __KNOWCORE_FOREACH__11, __KNOWCORE_FOREACH__10, __KNOWCORE_FOREACH__9, \
    __KNOWCORE_FOREACH__8, __KNOWCORE_FOREACH__7, __KNOWCORE_FOREACH__6, __KNOWCORE_FOREACH__5,    \
    __KNOWCORE_FOREACH__4, __KNOWCORE_FOREACH__3, __KNOWCORE_FOREACH__2, __KNOWCORE_FOREACH__1,    \
    __KNOWCORE_FOREACH__0)                                                                         \
  (action, (sep), 0, __VA_ARGS__)

/**
 * Execute action(X, I) on each variadic arguments.
 * X will contains the argument and I the index.
 *
 * @code
 * #define PRINT(X, I) std::cout << " " # I ": " << X;
 * KNOWCORE_FOREACH(PRINT, a, b, c)
 * @endcode
 *
 * Will expand into:
 *
 * @code
 * std::cout << " " "0" ": " << a;
 * std::cout << " " "1" ": " << b;
 * std::cout << " " "2" ": " << c;
 * @endcode
 */
#define KNOWCORE_FOREACH(action, ...) KNOWCORE_FOREACH_SEP(action, , __VA_ARGS__)

// END knowCore Foreach

#define __KNOWCORE_UNLIST_ACTION(__X__, __I__) __X__

/**
 * Remove the comma seperating __VA_ARGS__
 */
#define KNOWCORE_UNLIST(...) KNOWCORE_FOREACH(__KNOWCORE_UNLIST_ACTION, __VA_ARGS__)

// BEGIN knowCore List

#define __KNOWCORE_LIST_ACTION(__X__, __I__) , __X__

/**
 * Concatenate the arguments of the macro in a comma seperated list.
 * Usefull to enable multiple template parameters in a macro.
 *
 * @code
 * SOME_MACRO(some_template<KNOWCORE_LIST(A,B)>)
 * @endcode
 */
#define KNOWCORE_LIST(__A__, ...) __A__ KNOWCORE_FOREACH(__KNOWCORE_LIST_ACTION, __VA_ARGS__)

// END knowCore List

// END Macros

// BEGIN knowCore Literal Operators

#if __cplusplus > 201703L
inline QString operator"" _kCs(const char8_t* _text, std::size_t _size)
{
  return QString::fromUtf8((const char*)_text, _size);
}
#endif

inline QString operator"" _kCs(const char* _text, std::size_t _size)
{
  return QString::fromUtf8((const char*)_text, _size);
}

namespace knowCore::details
{
  // From https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p1423r3.html
  template<std::size_t N>
  struct char8_t_string_literal
  {
    static constexpr inline std::size_t size = N;
    template<std::size_t... I>
    constexpr char8_t_string_literal(const char8_t (&r)[N], std::index_sequence<I...>) : s{r[I]...}
    {
    }
    constexpr char8_t_string_literal(const char8_t (&r)[N])
        : char8_t_string_literal(r, std::make_index_sequence<N>())
    {
    }
    auto operator<=>(const char8_t_string_literal&) const = default;
    char8_t s[N];
  };

  template<char8_t_string_literal L, std::size_t... I>
  constexpr inline const char as_char_buffer[sizeof...(I)] = {static_cast<char>(L.s[I])...};

  template<char8_t_string_literal L, std::size_t... I>
  constexpr auto& make_as_char_buffer(std::index_sequence<I...>)
  {
    return as_char_buffer<L, I...>;
  }
} // namespace knowCore::details

constexpr char operator""_kC_as_char(char8_t c) { return c; }

/**
 *  Allow to create UTF8 string in combination with QByteArray:
 *
 * @code
 * QByteArray arr(u8R"(This a UTF-8 String.)"_kC_as_char);
 * @endcode
 */
template<knowCore::details::char8_t_string_literal L>
constexpr auto& operator""_kC_as_char()
{
  return knowCore::details::make_as_char_buffer<L>(std::make_index_sequence<decltype(L)::size>());
}

// END knowCore Literal Operators

// BEGIN knowCore Utils and C++ extensions

namespace knowCore
{
  // BEGIN demangle and prettyTypename
  namespace details
  {
    /**
     * Take a symmbol and demangle it
     */
    QString demangle(const char* _mangled_symbol);
  } // namespace details
  /**
   * @return the type name of \ref _T_ as a pretty string (platform dependent)
   */
  template<typename _T_>
  QString prettyTypename()
  {
    return details::demangle(typeid(_T_).name());
  }
  // END demangle and prettyTypename

  namespace details
  {

    template<typename _T_>
    struct DeleteAll
    {
      DeleteAll(const QList<_T_>&) {}
    };
    template<typename _T_>
    struct DeleteAll<_T_*>
    {
      DeleteAll(const QList<_T_*>& _list) { qDeleteAll(_list); }
    };
  } // namespace details
  template<typename _T_>
  void deleteAll(const QList<_T_>& _list)
  {
    (void)details::DeleteAll<_T_>(_list);
  }

  namespace details
  {
    /**
     * Define a trait that test if \ref T is in the set \ref Ts.
     *
     * \code
     * contains_v<int, QString, QByteArray> == false;
     * contains_v<QString, QString, QByteArray> == true;
     * \endcode
     */
    template<typename T, typename... Ts>
    struct contains : std::disjunction<std::is_same<T, Ts>...>
    {
    };
    template<typename T, typename... Ts>
    inline constexpr bool contains_v = contains<T, Ts...>::value;

    //
    // The following template can be used to detect if a class is fully implemented
    //
    template<typename T, class = void>
    struct is_complete : std::false_type
    {
    };

    template<typename T>
    struct is_complete<T, decltype(void(sizeof(T)))> : std::true_type
    {
    };

    template<typename... T>
    inline constexpr bool is_complete_v = is_complete<T...>::value;

    //
    // The following template can be used to detect if a class is an instance of atempolate
    //
    namespace
    {
      template<typename, template<typename...> typename>
      struct is_instance_impl : public std::false_type
      {
      };

      template<template<typename...> typename U, typename... Ts>
      struct is_instance_impl<U<Ts...>, U> : public std::true_type
      {
      };
    } // namespace

    template<typename T, template<typename...> typename U>
    using is_instance = is_instance_impl<std::decay_t<T>, U>;
    template<typename T, template<typename...> typename U>
    inline constexpr bool is_instance_v = is_instance<T, U>::value;

    template<template<typename...> class Template, typename T>
    struct is_specialisation_of : std::false_type
    {
    };

    template<template<typename...> class Template, typename... Args>
    struct is_specialisation_of<Template, Template<Args...>> : std::true_type
    {
    };

    template<template<typename...> class Template, typename... Args>
    inline constexpr bool is_specialisation_of_v = is_specialisation_of<Template, Args...>::value;
  } // namespace details
  /**
   * Use this class to define an overloaded visitor for a std::variant
   *
   * @code
   * std::variant<QString, knowCore::Uri> avariant;
   * std::visit(knowCore::Overloaded{
   *     [](const QString& _string) { return _string; },
   *     [](const knowCore::Uri& _uri) { return QString(_uri); }
   *   }, avariant)
   * @endcode
   */
  template<class... Ts>
  struct Overloaded : Ts...
  {
    using Ts::operator()...;
  };
} // namespace knowCore

// END knowCore Utils and C++ extensions

// BEGIN knowCore D-Pointer Utilities

namespace knowCore::Private
{
  template<typename _TD_, typename _TS_, class Enable = void>
  struct CastD
  {
    typedef _TD_* ReturnT;
    static ReturnT get(_TS_ _t) { return static_cast<_TD_*>(_t); }
  };
  template<typename _TD_, typename _TS_>
    requires(not std::is_const_v<_TD_>)
  struct CastD<_TD_, const _TS_>
  {
    typedef _TD_* ReturnT;
    static ReturnT get(_TS_ _t) { return const_cast<_TD_*>(static_cast<const _TD_*>(_t)); }
  };
  template<typename _TD_, typename _TS_>
  struct CastD<_TD_, QSharedPointer<_TS_>>
  {
    typedef std::remove_const_t<_TD_> TD;
    typedef QSharedPointer<TD> ReturnT;
    static ReturnT get(const QSharedPointer<_TS_> _t) { return _t.template staticCast<TD>(); }
  };
  template<typename _TD_, typename _TS_>
  struct CastD<_TD_, QExplicitlySharedDataPointer<_TS_>>
  {
    typedef std::remove_const_t<_TD_> TD;
    typedef QExplicitlySharedDataPointer<TD> ReturnT;
    static ReturnT get(const QExplicitlySharedDataPointer<_TS_> _t)
    {
      return ReturnT(const_cast<TD*>(static_cast<_TD_*>(_t.data())));
    }
  };
  template<typename _TD_, typename _TS_>
  typename CastD<_TD_, _TS_>::ReturnT castD(const _TS_& _t)
  {
    return CastD<_TD_, _TS_>::get(_t);
  };
} // namespace knowCore::Private

/**
 * @internal
 * Macro use to declare the D() functions for casting the private pointer
 */
#define __KNOWCORE_D_FUNC_DECL()                                                                   \
  inline decltype(knowCore::Private::castD<Private>(d)) D();                                       \
  inline decltype(knowCore::Private::castD<const Private>(d)) CD();                                \
  inline decltype(knowCore::Private::castD<Private>(d)) NCD() const;                               \
  inline decltype(knowCore::Private::castD<const Private>(d)) D() const

#define KNOWCORE_D_DECL()                                                                          \
  struct Private;                                                                                  \
  __KNOWCORE_D_FUNC_DECL()

#define KNOWCORE_D_FUNC_DEF(_KLASS_)                                                               \
  decltype(knowCore::Private::castD<_KLASS_::Private>(static_cast<_KLASS_*>(nullptr)->d))          \
    _KLASS_::D()                                                                                   \
  {                                                                                                \
    return knowCore::Private::castD<Private>(d);                                                   \
  }                                                                                                \
  decltype(knowCore::Private::castD<const _KLASS_::Private>(static_cast<_KLASS_*>(nullptr)->d))    \
    _KLASS_::CD()                                                                                  \
  {                                                                                                \
    return knowCore::Private::castD<const Private>(d);                                             \
  }                                                                                                \
  decltype(knowCore::Private::castD<_KLASS_::Private>(static_cast<_KLASS_*>(nullptr)->d))          \
    _KLASS_::NCD() const                                                                           \
  {                                                                                                \
    return knowCore::Private::castD<Private>(d);                                                   \
  }                                                                                                \
  decltype(knowCore::Private::castD<const _KLASS_::Private>(static_cast<_KLASS_*>(nullptr)->d))    \
    _KLASS_::D() const                                                                             \
  {                                                                                                \
    return knowCore::Private::castD<const Private>(d);                                             \
  }

// END knowCore D-Pointer Utilities

// BEGIN function forward

#define KNOWCORE_FORWARD_INTERFACE_DEFAULT interface()

/**
 * Allow to change how the forwarding interface is accessed, default to "interface()"
 */
#define KNOWCORE_FORWARD_INTERFACE KNOWCORE_FORWARD_INTERFACE_DEFAULT

#define __KNOWCORE_FORWARD_CONST_FUNCTION_ACTION(__NAME__, __I__)                                  \
  auto __NAME__() const -> decltype(this->KNOWCORE_FORWARD_INTERFACE->__NAME__()) override         \
  {                                                                                                \
    return this->KNOWCORE_FORWARD_INTERFACE->__NAME__();                                           \
  }

#define __KNOWCORE_FORWARD_FUNCTION_ACTION(__NAME__, __I__)                                        \
  auto __NAME__() -> decltype(this->KNOWCORE_FORWARD_INTERFACE->__NAME__()) override               \
  {                                                                                                \
    return this->KNOWCORE_FORWARD_INTERFACE->__NAME__();                                           \
  }

#define __KNOWCORE_FORWARD_FUNCTION_ARGUMENTS_ACTION(__TYPE__, __I__) __TYPE__ _arg##__I__

#define __KNOWCORE_FORWARD_FUNCTION_ARGUMENTS(...)                                                 \
  KNOWCORE_FOREACH_SEP(__KNOWCORE_FORWARD_FUNCTION_ARGUMENTS_ACTION, KNOWCORE_COMMA, __VA_ARGS__)

#define __KNOWCORE_FORWARD_FUNCTION_ARGUMENTS_NAMES_ACTION(__TYPE__, __I__) _arg##__I__

#define __KNOWCORE_FORWARD_FUNCTION_ARGUMENTS_NAMES(...)                                           \
  KNOWCORE_FOREACH_SEP(__KNOWCORE_FORWARD_FUNCTION_ARGUMENTS_NAMES_ACTION, KNOWCORE_COMMA,         \
                       __VA_ARGS__)

#define ___KNOWCORE_FORWARD_FUNCTION_IMPLEMENTATION(__NAME__, __MODIFIER__, ...)                   \
  auto __NAME__(__KNOWCORE_FORWARD_FUNCTION_ARGUMENTS(__VA_ARGS__))                                \
    __MODIFIER__ -> decltype(this->KNOWCORE_FORWARD_INTERFACE->__NAME__(                           \
                   __KNOWCORE_FORWARD_FUNCTION_ARGUMENTS_NAMES(__VA_ARGS__))) override             \
  {                                                                                                \
    return this->KNOWCORE_FORWARD_INTERFACE->__NAME__(                                             \
      __KNOWCORE_FORWARD_FUNCTION_ARGUMENTS_NAMES(__VA_ARGS__));                                   \
  }

/**
 * Forward definition when defining a reference \ref knowCore::Reference.
 *
 * @begincode
 * KNOWCORE_FORWARD_FUNCTION(name, TypeA, TypeB)
 * @end
 * is equivalent to write
 * @begincode
 * ReturnType name(TypeA _a, TypeB _b) override
 * {
 *    return interface()->name(_a, _b);
 * }
 * @end
 */
#define KNOWCORE_FORWARD_FUNCTION(__NAME__, ...)                                                   \
  ___KNOWCORE_FORWARD_FUNCTION_IMPLEMENTATION(__NAME__, , __VA_ARGS__)

/**
 * Forward definition of a const function when defining a reference \ref knowCore::Reference.
 *
 * @begincode
 * KNOWCORE_FORWARD_CONST_FUNCTION(name, TypeA, TypeB)
 * @end
 * is equivalent to write
 * @begincode
 * ReturnType name(TypeA _a, TypeB _b) const override
 * {
 *    return interface()->name(_a, _b);
 * }
 * @end
 */
#define KNOWCORE_FORWARD_CONST_FUNCTION(__NAME__, ...)                                             \
  ___KNOWCORE_FORWARD_FUNCTION_IMPLEMENTATION(__NAME__, const, __VA_ARGS__)

/**
 * Forward definition of a list of functions, without arguments, when defining a reference \ref
 * knowCore::Reference.
 *
 * @begincode
 * KNOWCORE_FORWARD_FUNCTIONS(name1, name2)
 * @end
 * is equivalent to write
 * @begincode
 * ReturnType1 name1() override
 * {
 *    return interface()->name1();
 * }
 * ReturnType2 name2() override
 * {
 *    return interface()->name2();
 * }
 * @end
 */

#define KNOWCORE_FORWARD_FUNCTIONS(...)                                                            \
public:                                                                                            \
  KNOWCORE_FOREACH(__KNOWCORE_FORWARD_FUNCTION_ACTION, __VA_ARGS__)

/**
 * Forward definition of a list functions, without arguments, when defining a reference \ref
 * knowCore::Reference.
 *
 * @begincode
 * KNOWCORE_FORWARD_CONST_FUNCTIONS(name1, name2)
 * @end
 * is equivalent to write
 * @begincode
 * ReturnType1 name1() const override
 * {
 *    return interface()->name1();
 * }
 * ReturnType2 name2() const override
 * {
 *    return interface()->name2();
 * }
 * @end
 */

#define KNOWCORE_FORWARD_CONST_FUNCTIONS(...)                                                      \
public:                                                                                            \
  KNOWCORE_FOREACH(__KNOWCORE_FORWARD_CONST_FUNCTION_ACTION, __VA_ARGS__)

// END function forward

// BEGIN concepts

namespace knowCore::details
{
  template<class Base, class Derived>
  concept base_of = std::is_base_of_v<Base, Derived>;
}

// END concepts

namespace knowCore
{
  template<typename _T_>
  std::remove_reference<_T_>::type dereference(_T_&& _t)
  {
    return _t;
  }
} // namespace knowCore

namespace knowCore
{
  template<size_t N>
  struct FixedString
  {
    constexpr FixedString(const char (&str)[N]) { std::copy_n(str, N, value); }

    char value[N];
  };
} // namespace knowCore
