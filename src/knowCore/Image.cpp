#include "Image.h"

#include <cfloat>
#include <limits>

#include <QByteArray>
#include <QCborMap>

#include <Cyqlops/Crypto/Hash.h>

#include "ImageCompression/jpgd.h"
#include "ImageCompression/jpge.h"

#include <knowCore/Uris/askcore_datatype.h>

using namespace knowCore;

struct Image::Private : public QSharedData
{
  QByteArray arr;
  quint64 width, height, channels;
  Image::Type type;
  ColorSpace colorSpace;
  static quint64 pixelSize(Image::Type _type, quint64 _channels);
  static quint64 channelsFor(ColorSpace _cs);

  struct ParsedEncoding
  {
    Type type;
    ColorSpace colorSpace;
    quint64 channels;
  };
  static cres_qresult<ParsedEncoding> parseEncoding(const QString& _encoding);
};

quint64 Image::Private::channelsFor(ColorSpace _cs)
{
  switch(_cs)
  {
  case ColorSpace::BGR:
  case ColorSpace::RGB:
    return 3;
  case ColorSpace::Unknown:
    clog_warning("Querying for number of channel for unknown colorspace.");
    return 0;
  }
  clog_fatal("Unknown colorspace.");
}

cres_qresult<Image::Private::ParsedEncoding> Image::Private::parseEncoding(const QString& _encoding)
{
  ParsedEncoding pe;
  if(_encoding.startsWith("rgb") or _encoding.startsWith("bgr"))
  {
    pe.channels = 3;
    if(_encoding.startsWith("rgb"))
    {
      pe.colorSpace = ColorSpace::RGB;
    }
    else if(_encoding.startsWith("bgr"))
    {
      pe.colorSpace = ColorSpace::BGR;
    }
    else
    {
      return cres_failure("Invalid enconding '{}'", _encoding);
    }
    QRegularExpression reg("\\w+(\\d+)");
    QRegularExpressionMatch em = reg.match(_encoding);
    if(em.hasMatch())
    {
      switch(em.captured(1).toInt())
      {
      default:
        return cres_failure("Uknown image depth {} in encoding {}", em.captured(1), _encoding);
      case 8:
        pe.type = Image::Type::UnsignedInteger8;
        break;
      case 16:
        pe.type = Image::Type::UnsignedInteger16;
        break;
      case 32:
        pe.type = Image::Type::UnsignedInteger32;
        break;
      }
    }
    else
    {
      return cres_failure("Failed to parse image encoding '{}'", _encoding);
    }
  }
  else
  {
    QRegularExpression reg("(\\d+)(\\w)C(\\d+)");
    QRegularExpressionMatch em = reg.match(_encoding);
    if(em.hasMatch())
    {
      pe.channels = em.captured(3).toInt();
      pe.colorSpace = Image::ColorSpace::Unknown;
    }
    else
    {
      return cres_failure("Failed to parse image encoding '{}'", _encoding);
    }
    if(em.captured(2) == "U")
    {
      switch(em.captured(1).toInt())
      {
      default:
        return cres_failure("Uknown image depth {} in encoding {}", em.captured(3), _encoding);
      case 8:
        pe.type = Image::Type::UnsignedInteger8;
        break;
      case 16:
        pe.type = Image::Type::UnsignedInteger16;
        break;
      case 32:
        pe.type = Image::Type::UnsignedInteger32;
        break;
      }
    }
    else if(em.captured(2) == "S")
    {
      switch(em.captured(1).toInt())
      {
      default:
        return cres_failure("Uknown image depth {} in encoding {}", em.captured(3), _encoding);
      case 8:
        pe.type = Image::Type::Integer8;
        break;
      case 16:
        pe.type = Image::Type::Integer16;
        break;
      case 32:
        pe.type = Image::Type::Integer32;
        break;
      }
    }
    else if(em.captured(2) == "F")
    {
      switch(em.captured(1).toInt())
      {
      default:
        return cres_failure("Uknown image depth {} in encoding {}", em.captured(3), _encoding);
      case 32:
        pe.type = Image::Type::Float32;
        break;
      case 64:
        pe.type = Image::Type::Float64;
        break;
      }
    }
    else
    {
      return cres_failure("Uknown image channel type {} in encoding {}", em.captured(2), _encoding);
    }
  }
  return cres_success(pe);
}

Image::Image() : Image(0, 0, 0, Type::UnsignedInteger8) {}

Image::Image(const QByteArray& _data, quint64 _width, quint64 _height, quint64 _channels,
             Type _type)
    : d(new Private)
{
  d->arr = _data;
  d->width = _width;
  d->height = _height;
  d->channels = _channels;
  d->type = _type;
  d->colorSpace = ColorSpace::Unknown;
  clog_assert(d->arr.size() == d->width * d->height * pixelSize());
}

Image::Image(const QByteArray& _data, quint64 _width, quint64 _height, ColorSpace _colorSpace,
             Type _type)
    : Image(_data, _width, _height, Private::channelsFor(_colorSpace), _type)
{
  d->colorSpace = _colorSpace;
}

Image::Image(quint64 _width, quint64 _height, quint64 _channels, Image::Type _type)
    : Image(QByteArray(_width * _height * Private::pixelSize(_type, _channels), 0), _width, _height,
            _channels, _type)
{
}

Image::Image(quint64 _width, quint64 _height, ColorSpace _colorSpace, Type _type)
    : Image(_width, _height, Private::channelsFor(_colorSpace), _type)
{
  d->colorSpace = _colorSpace;
}

Image::Image(const Image& _rhs) : d(_rhs.d) {}

Image& Image::operator=(const Image& _rhs)
{
  d = _rhs.d;
  return *this;
}

Image::~Image() {}

cres_qresult<Image> Image::createImage(quint64 _width, quint64 _height, const QString& _encoding)
{
  cres_try(Private::ParsedEncoding pe, Private::parseEncoding(_encoding));
  switch(pe.colorSpace)
  {
  case ColorSpace::Unknown:
    return cres_success(Image(_width, _height, pe.channels, pe.type));
  default:
    return cres_success(Image(_width, _height, pe.colorSpace, pe.type));
  }
}

cres_qresult<Image> Image::fromRawData(const QByteArray& _data, quint64 _width, quint64 _height,
                                       quint64 _channels, Type _type)
{
  int expected_size = _width * _height * Private::pixelSize(_type, _channels);
  if(_data.size() == expected_size)
  {
    return cres_success(Image(_data, _width, _height, _channels, _type));
  }
  else
  {
    return cres_failure("Cannot create image, expected array of size {} got {}", expected_size,
                        _data.size());
  }
}

cres_qresult<Image> Image::fromRawData(const QByteArray& _data, quint64 _width, quint64 _height,
                                       ColorSpace _colorSpace, Type _type)
{
  if(_colorSpace == ColorSpace::Unknown)
  {
    return cres_failure("Cannot create image for unknown colorspace.");
  }
  else
  {
    int expected_size
      = _width * _height * Private::pixelSize(_type, Private::channelsFor(_colorSpace));
    if(_data.size() == expected_size)
    {
      return cres_success(Image(_data, _width, _height, _colorSpace, _type));
    }
    else
    {
      return cres_failure("Cannot create image, expected array of size {} got {}", expected_size,
                          _data.size());
    }
  }
}

cres_qresult<Image> Image::fromRawData(const QByteArray& _data, quint64 _width, quint64 _height,
                                       const QString& _encoding)
{
  cres_try(Private::ParsedEncoding pe, Private::parseEncoding(_encoding));
  switch(pe.colorSpace)
  {
  case ColorSpace::Unknown:
    return fromRawData(_data, _width, _height, pe.channels, pe.type);
  default:
    return fromRawData(_data, _width, _height, pe.colorSpace, pe.type);
  }
}

cres_qresult<Image> Image::fromCompressedData(const QByteArray& _data, const QString& _compression)
{
  if(_compression == "jpeg" or _compression == "jpg")
  {
    int width, height, channels;
    unsigned char* decompressedImage = jpgd::decompress_jpeg_image_from_memory(
      (const jpge::uint8*)_data.data(), _data.size(), &width, &height, &channels, 3);
    QByteArray data((const char*)decompressedImage, width * height * channels);
    if(channels == 3)
    {
      return fromRawData(data, width, height, ColorSpace::RGB, Type::UnsignedInteger8);
    }
    else
    {
      return fromRawData(data, width, height, channels, Type::UnsignedInteger8);
    }
  }
  else
  {
    return cres_failure("Unsupported image compression method: {}", _compression);
  }
}

cres_qresult<QByteArray> Image::compress(const QString& _compression,
                                         const QHash<QString, QVariant>& _options) const
{
  if(_compression == "jpeg" or _compression == "jpg")
  {
    if(d->channels == 2 or d->channels > 4)
    {
      return cres_failure("Cannot compress to jpeg images with channels: {}", d->channels);
    }
    if(d->type != Type::UnsignedInteger8)
    {
      return cres_failure("Cannot compress to jpeg images with channel type: {}", d->type);
    }
    QByteArray output;
    output.resize(d->arr.size());
    jpge::params p;
    p.m_quality = _options.value("quality", 95).toInt();
    int resultBufSize = output.size();
    if(jpge::compress_image_to_jpeg_file_in_memory(output.data(), resultBufSize, d->width,
                                                   d->height, d->channels,
                                                   (jpge::uint8*)d->arr.data(), p))
    {
      output.truncate(resultBufSize);
      return cres_success(output);
    }
    else
    {
      return cres_failure("Failed to compress image with jpg.");
    }
  }
  else
  {
    return cres_failure("Unsupported image compression method: {}", _compression);
  }
}

const quint8* Image::dataPtr() const { return (const quint8*)d->arr.data(); }

quint8* Image::dataPtr() { return (quint8*)d->arr.data(); }

quint64 Image::height() const { return d->height; }

quint64 Image::width() const { return d->width; }

quint64 Image::channels() const { return d->channels; }

QByteArray Image::toArray() const { return d->arr; }

Image::Type Image::type() const { return d->type; }

bool Image::operator==(const Image& _rhs) const
{
  return d == _rhs.d
         or (d->width == _rhs.d->width and d->height == _rhs.d->height
             and d->channels == _rhs.d->channels and d->colorSpace == _rhs.d->colorSpace
             and d->arr == _rhs.d->arr);
}

namespace
{
  namespace math
  {
    template<typename _scalar_>
    _scalar_ round_div(_scalar_ dividend, _scalar_ divisor)
    {
      return (dividend + (divisor / 2)) / divisor;
    }
  } // namespace math

  template<typename _Scalar>
  struct channel_trait;
  template<>
  struct channel_trait<int8_t>
  {
    static constexpr int8_t zero = 0;
    static constexpr int8_t normal_range_min = std::numeric_limits<int8_t>::min();
    static constexpr int8_t normal_range_max = std::numeric_limits<int8_t>::max();
    static constexpr int8_t epsilon = 0;
    typedef int16_t computation_type;
  };
  template<typename _Scalar>
  struct channel_trait;
  template<>
  struct channel_trait<uint8_t>
  {
    static constexpr uint8_t zero = 0;
    static constexpr uint8_t normal_range_min = std::numeric_limits<uint8_t>::min();
    static constexpr uint8_t normal_range_max = std::numeric_limits<uint8_t>::max();
    static constexpr uint8_t epsilon = 0;
    typedef int16_t computation_type;
  };
  template<>
  struct channel_trait<int16_t>
  {
    static constexpr int16_t zero = 0;
    static constexpr int16_t normal_range_min = std::numeric_limits<int16_t>::min();
    static constexpr int16_t normal_range_max = std::numeric_limits<int16_t>::max();
    static constexpr int16_t epsilon = 0;
    typedef int32_t computation_type;
  };
  template<>
  struct channel_trait<uint16_t>
  {
    static constexpr uint16_t zero = 0;
    static constexpr uint16_t normal_range_min = std::numeric_limits<uint16_t>::min();
    static constexpr uint16_t normal_range_max = std::numeric_limits<uint16_t>::max();
    static constexpr uint16_t epsilon = 0;
    typedef int32_t computation_type;
  };
  template<>
  struct channel_trait<int32_t>
  {
    static constexpr int32_t zero = 0;
    static constexpr int32_t normal_range_min = std::numeric_limits<int32_t>::min();
    static constexpr int32_t normal_range_max = std::numeric_limits<int32_t>::max();
    static constexpr int32_t epsilon = 0;
    typedef int64_t computation_type;
  };
  template<>
  struct channel_trait<uint32_t>
  {
    static constexpr uint32_t zero = 0;
    static constexpr uint32_t normal_range_min = std::numeric_limits<uint32_t>::min();
    static constexpr uint32_t normal_range_max = std::numeric_limits<uint32_t>::max();
    static constexpr uint32_t epsilon = 0;
    typedef int64_t computation_type;
  };
  template<>
  struct channel_trait<float>
  {
    static constexpr float zero = 0.0f;
    static constexpr float normal_range_min = 0.0f;
    static constexpr float normal_range_max = 1.0f;
    static constexpr float epsilon = FLT_EPSILON;
    typedef float computation_type;
  };
  template<>
  struct channel_trait<double>
  {
    static constexpr double zero = 0.0;
    static constexpr double normal_range_min = 0.0;
    static constexpr double normal_range_max = 1.0;
    static constexpr double epsilon = DBL_EPSILON;
    typedef double computation_type;
  };
  template<>
  struct channel_trait<long double>
  {
    static constexpr long double zero = 0.0l;
    static constexpr long double normal_range_min = 0.0l;
    static constexpr long double normal_range_max = 1.0l;
    static constexpr long double epsilon = LDBL_EPSILON;
    typedef long double computation_type;
  };

  template<typename _scalar_output_, typename _scalar_input_>
  struct scalar_converter;

  template<typename _scalar_>
  struct scalar_converter<_scalar_, _scalar_>
  {
    static _scalar_ convert(_scalar_ _in) { return _in; }
  };
  template<typename _scalar_output_, typename _scalar_input_>
    requires(std::is_floating_point_v<_scalar_output_> and std::is_integral_v<_scalar_input_>)
  struct scalar_converter<_scalar_output_, _scalar_input_>
  {
    typedef typename std::conditional<std::is_floating_point<_scalar_output_>::value,
                                      _scalar_output_, _scalar_input_>::type _floating_point_type_;
    static _scalar_output_ convert(_scalar_input_ _in)
    {
      return (_floating_point_type_(_in) - channel_trait<_scalar_input_>::normal_range_min)
             / (_floating_point_type_(channel_trait<_scalar_input_>::normal_range_max)
                - channel_trait<_scalar_input_>::normal_range_min);
    }
  };
  template<typename _scalar_output_, typename _scalar_input_>
    requires(std::is_integral_v<_scalar_output_> and std::is_floating_point_v<_scalar_input_>)
  struct scalar_converter<_scalar_output_, _scalar_input_>
  {
    typedef typename std::conditional<std::is_floating_point<_scalar_output_>::value,
                                      _scalar_output_, _scalar_input_>::type _floating_point_type_;
    static _scalar_output_ convert(_scalar_input_ _in)
    {
      return _in
               * ((typename channel_trait<_scalar_output_>::computation_type)
                    channel_trait<_scalar_output_>::normal_range_max
                  - channel_trait<_scalar_output_>::normal_range_min)
             + channel_trait<_scalar_output_>::normal_range_min;
    }
  };
  template<typename _scalar_output_, typename _scalar_input_>
    requires(std::is_floating_point_v<_scalar_output_> and std::is_floating_point_v<_scalar_input_>)
  struct scalar_converter<_scalar_output_, _scalar_input_>
  {
    static _scalar_output_ convert(_scalar_input_ _in) { return _scalar_output_(_in); }
  };
  template<typename _scalar_output_, typename _scalar_input_>
    requires(std::is_integral_v<_scalar_output_> and std::is_integral_v<_scalar_input_>
             and sizeof(_scalar_output_) < sizeof(_scalar_input_))
  struct scalar_converter<_scalar_output_, _scalar_input_>
  {
    static constexpr _scalar_input_ mask
      = (1 << ((sizeof(_scalar_input_) - sizeof(_scalar_output_)) * 8)) + 1;
    static _scalar_output_ convert(_scalar_input_ _in) { return math::round_div(_in, mask); }
  };
  template<typename _scalar_output_, typename _scalar_input_>
    requires(std::is_integral_v<_scalar_output_> and std::is_integral_v<_scalar_input_>
             and (sizeof(_scalar_output_) > sizeof(_scalar_input_)))
  struct scalar_converter<_scalar_output_, _scalar_input_>
  {
    static constexpr _scalar_output_ mask
      = (1 << ((sizeof(_scalar_output_) - sizeof(_scalar_input_)) * 8)) + 1;
    static _scalar_output_ convert(_scalar_input_ _in) { return _in * mask; }
  };
  template<typename _scalar_output_, typename _scalar_input_>
    requires(std::is_integral_v<_scalar_output_> and std::is_integral_v<_scalar_input_>
             and sizeof(_scalar_output_) == sizeof(_scalar_input_))
  struct scalar_converter<_scalar_output_, _scalar_input_>
  {
    static _scalar_output_ convert(_scalar_input_ _in)
    {
      return (int64_t)_in + (int64_t)channel_trait<_scalar_input_>::normal_range_min
             - (int64_t)channel_trait<_scalar_output_>::normal_range_min;
    }
  };
  template<typename _scalar_output_, typename _scalar_input_>
  _scalar_output_ convert(_scalar_input_ _in)
  {
    return scalar_converter<_scalar_output_, _scalar_input_>::convert(_in);
  }

  template<typename _scalar_output_, typename _scalar_input_>
  void batch_convert(quint8* _output, const quint8* _input, quint64 _size)
  {
    _scalar_output_* output = reinterpret_cast<_scalar_output_*>(_output);
    const _scalar_input_* input = reinterpret_cast<const _scalar_input_*>(_input);
    for(quint64 i = 0; i < _size; ++i)
    {
      output[i] = convert<_scalar_output_, _scalar_input_>(input[i]);
    }
  }
} // namespace

#define DO_CASE_CONVERT(__TYPE__, __DATA_TYPE__)                                                   \
  case Type::__TYPE__:                                                                             \
    switch(d->type)                                                                                \
    {                                                                                              \
    case Type::UnsignedInteger8:                                                                   \
      batch_convert<uint8_t, __DATA_TYPE__>(r.dataPtr(), dataPtr(),                                \
                                            width() * height() * channels());                      \
      break;                                                                                       \
    case Type::Integer8:                                                                           \
      batch_convert<int8_t, __DATA_TYPE__>(r.dataPtr(), dataPtr(),                                 \
                                           width() * height() * channels());                       \
      break;                                                                                       \
    case Type::UnsignedInteger16:                                                                  \
      batch_convert<uint16_t, __DATA_TYPE__>(r.dataPtr(), dataPtr(),                               \
                                             width() * height() * channels());                     \
      break;                                                                                       \
    case Type::Integer16:                                                                          \
      batch_convert<int16_t, __DATA_TYPE__>(r.dataPtr(), dataPtr(),                                \
                                            width() * height() * channels());                      \
      break;                                                                                       \
    case Type::UnsignedInteger32:                                                                  \
      batch_convert<uint32_t, __DATA_TYPE__>(r.dataPtr(), dataPtr(),                               \
                                             width() * height() * channels());                     \
      break;                                                                                       \
    case Type::Integer32:                                                                          \
      batch_convert<int32_t, __DATA_TYPE__>(r.dataPtr(), dataPtr(),                                \
                                            width() * height() * channels());                      \
      break;                                                                                       \
    case Type::Float32:                                                                            \
      batch_convert<float, __DATA_TYPE__>(r.dataPtr(), dataPtr(),                                  \
                                          width() * height() * channels());                        \
      break;                                                                                       \
    case Type::Float64:                                                                            \
      batch_convert<double, __DATA_TYPE__>(r.dataPtr(), dataPtr(),                                 \
                                           width() * height() * channels());                       \
      break;                                                                                       \
    }                                                                                              \
    break;
#include <iostream>
Image Image::convert(Image::Type _type) const
{
  if(d->type == _type)
    return *this;
  Image r(d->width, d->height, d->channels, _type);
  switch(_type)
  {
    DO_CASE_CONVERT(UnsignedInteger8, uint8_t)
    DO_CASE_CONVERT(Integer8, int8_t)
    DO_CASE_CONVERT(UnsignedInteger16, uint16_t)
    DO_CASE_CONVERT(Integer16, int16_t)
    DO_CASE_CONVERT(UnsignedInteger32, uint32_t)
    DO_CASE_CONVERT(Integer32, int32_t)
    DO_CASE_CONVERT(Float32, float)
    DO_CASE_CONVERT(Float64, double)
  }
  return r;
}

quint64 Image::Private::pixelSize(Image::Type _type, quint64 _channels)
{
  return scalarSize(_type) * _channels;
}

quint64 Image::pixelSize() const { return Private::pixelSize(d->type, channels()); }

#define DO_CASE_PIXEL_SIZE(__TYPE__, __DATA_TYPE__)                                                \
  case Type::__TYPE__:                                                                             \
    return sizeof(__DATA_TYPE__);

quint64 Image::scalarSize(Image::Type _type)
{
  switch(_type)
  {
    DO_CASE_PIXEL_SIZE(UnsignedInteger8, uint8_t)
    DO_CASE_PIXEL_SIZE(Integer8, int8_t)
    DO_CASE_PIXEL_SIZE(UnsignedInteger16, uint16_t)
    DO_CASE_PIXEL_SIZE(Integer16, int16_t)
    DO_CASE_PIXEL_SIZE(UnsignedInteger32, uint32_t)
    DO_CASE_PIXEL_SIZE(Integer32, int32_t)
    DO_CASE_PIXEL_SIZE(Float32, float)
    DO_CASE_PIXEL_SIZE(Float64, double)
  }
  return -1;
}

quint64 Image::size() const { return d->width * d->height * pixelSize(); }

Image::ColorSpace Image::colorSpace() const { return d->colorSpace; }

namespace
{
  const char* channel_depth(Image::Type _type)
  {
    switch(_type)
    {
      using enum Image::Type;
    case UnsignedInteger8:
      return "8";
    case Integer8:
      return "s8";
    case UnsignedInteger16:
      return "16";
    case Integer16:
      return "s16";
    case UnsignedInteger32:
      return "32";
    case Integer32:
      return "s32";
    case Float32:
      return "f32";
    case Float64:
      return "f64";
    }
    clog_fatal("Unknown image type: {}", int(_type));
  }
} // namespace

QString Image::encoding() const
{

  switch(d->colorSpace)
  {
  case ColorSpace::Unknown:
    switch(d->type)
    {
      using enum Image::Type;
    case UnsignedInteger8:
      return clog_qt::qformat("8UC{}", d->channels);
    case Integer8:
      return clog_qt::qformat("8SC{}", d->channels);
    case UnsignedInteger16:
      return clog_qt::qformat("16UC{}", d->channels);
    case Integer16:
      return clog_qt::qformat("16SC{}", d->channels);
    case UnsignedInteger32:
      return clog_qt::qformat("32UC{}", d->channels);
    case Integer32:
      return clog_qt::qformat("32SC{}", d->channels);
    case Float32:
      return clog_qt::qformat("32FC{}", d->channels);
    case Float64:
      return clog_qt::qformat("64FC{}", d->channels);
    }
    clog_fatal("Unknown image type: {}", int(d->type));
    break;
  case ColorSpace::RGB:
    return clog_qt::qformat("rgb{}", channel_depth(d->type));
  case ColorSpace::BGR:
    return clog_qt::qformat("bgr{}", channel_depth(d->type));
  }
  clog_fatal("Unknown image colorspace: {}", int(d->colorSpace));
}

#include <knowCore/MetaTypeImplementation.h>

#define DATA_KEY QStringLiteral("type")
#define WIDTH_KEY QStringLiteral("width")
#define HEIGHT_KEY QStringLiteral("height")
#define CHANNELS_KEY QStringLiteral("channels")
#define CS_KEY QStringLiteral("colorspace")
#define TYPE_KEY QStringLiteral("channel_type")

namespace
{
  QString colorspaceToString(Image::ColorSpace _cs)
  {
    switch(_cs)
    {
    case Image::ColorSpace::BGR:
      return "bgr";
    case Image::ColorSpace::RGB:
      return "rgb";
    case Image::ColorSpace::Unknown:
      break;
    }
    return "unknown";
  }
  QString typeToString(Image::Type _t)
  {
    switch(_t)
    {
    case Image::Type::UnsignedInteger8:
      return "UnsignedInteger8";
    case Image::Type::UnsignedInteger16:
      return "UnsignedInteger16";
    case Image::Type::UnsignedInteger32:
      return "UnsignedInteger32";
    case Image::Type::Integer8:
      return "Integer8";
    case Image::Type::Integer16:
      return "Integer16";
    case Image::Type::Integer32:
      return "Integer32";
    case Image::Type::Float32:
      return "Float32";
    case Image::Type::Float64:
      return "Float64";
    }
    return "unknown";
  }
  Image::ColorSpace stringToColorspace(const QString& _cs)
  {
    static QHash<QString, Image::ColorSpace> stToCS = {{"bgr", Image::ColorSpace::BGR},
                                                       {"rgb", Image::ColorSpace::RGB},
                                                       {"unknown", Image::ColorSpace::Unknown}};
    return stToCS.value(_cs, Image::ColorSpace::Unknown);
  }
  cres_qresult<Image::Type> stringToType(const QString& _cs)
  {
    static QHash<QString, Image::Type> stToCS
      = {{"UnsignedInteger8", Image::Type::UnsignedInteger8},
         {"UnsignedInteger16", Image::Type::UnsignedInteger16},
         {"UnsignedInteger32", Image::Type::UnsignedInteger32},
         {"Integer8", Image::Type::Integer8},
         {"Integer16", Image::Type::Integer16},
         {"Integer32", Image::Type::Integer32},
         {"Float32", Image::Type::Float32},
         {"Float64", Image::Type::Float64}};
    if(stToCS.contains(_cs))
      return cres_success(stToCS.value(_cs));
    return cres_failure("Unknown type '{}'", _cs);
  }
} // namespace

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(Image)
cres_qresult<QByteArray> md5(const Image& _value) const override
{
  return cres_success(Cyqlops::Crypto::Hash::md5(_value.toArray()));
}
cres_qresult<QJsonValue> toJsonValue(const Image& _value,
                                     const SerialisationContexts&) const override
{
  QJsonObject obj;
  obj[DATA_KEY] = QString::fromLatin1(_value.toArray().toBase64());
  obj[WIDTH_KEY] = int(_value.width());
  obj[HEIGHT_KEY] = int(_value.height());
  obj[CHANNELS_KEY] = int(_value.channels());
  obj[CS_KEY] = colorspaceToString(_value.colorSpace());
  obj[TYPE_KEY] = typeToString(_value.type());
  return cres_success(obj);
}
cres_qresult<void> fromJsonValue(Image* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts&) const override
{
  if(_json_value.isObject())
  {
    QJsonObject obj = _json_value.toObject();
    QByteArray arr = QByteArray::fromBase64(obj[DATA_KEY].toString().toLatin1());
    int width = obj[WIDTH_KEY].toInt();
    int height = obj[HEIGHT_KEY].toInt();
    int channels = obj[CHANNELS_KEY].toInt();
    Image::ColorSpace cs = stringToColorspace(obj[CS_KEY].toString());
    QString ty_s = obj[TYPE_KEY].toString();
    cres_try(Image::Type ty, stringToType(ty_s));
    if(cs != Image::ColorSpace::Unknown)
    {
      cres_try(*_value, Image::fromRawData(arr, width, height, cs, ty));
    }
    else
    {
      cres_try(*_value, Image::fromRawData(arr, width, height, channels, ty));
    }
    return cres_success();
  }
  else
  {
    return expectedError("object", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const Image& _value,
                                     const SerialisationContexts&) const override
{
  QCborMap obj;
  obj[DATA_KEY] = _value.toArray();
  obj[WIDTH_KEY] = int(_value.width());
  obj[HEIGHT_KEY] = int(_value.height());
  obj[CHANNELS_KEY] = int(_value.channels());
  obj[CS_KEY] = colorspaceToString(_value.colorSpace());
  obj[TYPE_KEY] = typeToString(_value.type());
  return cres_success(obj);
}
cres_qresult<void> fromCborValue(Image* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts&) const override
{
  if(_cbor_value.isMap())
  {
    QCborMap obj = _cbor_value.toMap();
    QByteArray arr = obj[DATA_KEY].toByteArray();
    int width = obj[WIDTH_KEY].toInteger();
    int height = obj[HEIGHT_KEY].toInteger();
    int channels = obj[CHANNELS_KEY].toInteger();
    Image::ColorSpace cs = stringToColorspace(obj[CS_KEY].toString());
    QString ty_s = obj[TYPE_KEY].toString();
    cres_try(Image::Type ty, stringToType(ty_s));
    if(cs != Image::ColorSpace::Unknown)
    {
      cres_try(*_value, Image::fromRawData(arr, width, height, cs, ty));
    }
    else
    {
      cres_try(*_value, Image::fromRawData(arr, width, height, channels, ty));
    }
    return cres_success();
  }
  else
  {
    return expectedError("object", _cbor_value);
  }
}
cres_qresult<QString> printable(const Image& _value) const override
{
  return cres_success(clog_qt::qformat(
    "image({}x{}x{} {} {} {}bytes)", _value.width(), _value.height(), _value.channels(),
    colorspaceToString(_value.colorSpace()), typeToString(_value.type()), _value.toArray().size()));
}
cres_qresult<QString> toRdfLiteral(const Image& _value,
                                   const SerialisationContexts& _context) const override
{
  return toJsonString(_value, _context);
}
cres_qresult<void> fromRdfLiteral(Image* _value, const QString& _serialised,
                                  const DeserialisationContexts& _context) const override
{
  return fromJsonString(_value, _serialised, _context);
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(Image)

KNOWCORE_DEFINE_METATYPE(knowCore::Image, knowCore::Uris::askcore_datatype::image,
                         knowCore::MetaTypeTraits::None)
