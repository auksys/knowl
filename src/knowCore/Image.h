#include <QSharedDataPointer>

#include "Forward.h"

namespace knowCore
{
  class Image
  {
  public:
    enum class Type
    {
      UnsignedInteger8,
      Integer8,
      UnsignedInteger16,
      Integer16,
      UnsignedInteger32,
      Integer32,
      Float32,
      Float64
    };
    enum class ColorSpace
    {
      Unknown,
      RGB,
      BGR
    };
  private:
    Image(const QByteArray& _data, quint64 _width, quint64 _height, quint64 _channels, Type _type);
    Image(const QByteArray& _data, quint64 _width, quint64 _height, ColorSpace _colorSpace,
          Type _type);
  public:
    Image();
    Image(quint64 _width, quint64 _height, quint64 _channels, Type _type);
    Image(quint64 _width, quint64 _height, ColorSpace _colorSpace, Type _type);
    Image(const Image& _rhs);
    Image& operator=(const Image& _rhs);
    ~Image();
    QByteArray toArray() const;
    quint8* dataPtr();
    const quint8* dataPtr() const;
    quint64 width() const;
    quint64 height() const;
    quint64 channels() const;
    Type type() const;
    Image convert(Type _type) const;
    quint64 pixelSize() const;
    /**
     * @return the size of the image in bytes.
     */
    quint64 size() const;
    /**
     * @return the color space of this image.
     */
    ColorSpace colorSpace() const;
    /**
     * @return a string that represent the encoding of an image. It follows OpenCV/ROS convention.
     */
    QString encoding() const;
    bool operator==(const Image& _rhs) const;
    /**
     * Compress the image with the compression algorithm (@p _compression) and the given options.
     *
     * Currently only "jpeg" is supported with "quality" options (default to 95).
     */
    cres_qresult<QByteArray> compress(const QString& _compression,
                                      const QHash<QString, QVariant>& _options) const;
    static quint64 scalarSize(Type _type);
    /**
     * Create an image with the given encoding
     */
    static cres_qresult<Image> createImage(quint64 _width, quint64 _height,
                                           const QString& _encoding);
    /**
     * Attempt to create an image from a number of channels and channel type.
     */
    static cres_qresult<Image> fromRawData(const QByteArray& _data, quint64 _width, quint64 _height,
                                           quint64 _channels, Type _type);
    /**
     * Attempt to create an image from a colorspace and channel type.
     */
    static cres_qresult<Image> fromRawData(const QByteArray& _data, quint64 _width, quint64 _height,
                                           ColorSpace _colorSpace, Type _type);
    /**
     * Attempt to create an image from a byte array and encoding.
     */
    static cres_qresult<Image> fromRawData(const QByteArray& _data, quint64 _width, quint64 _height,
                                           const QString& _encoding);
    /**
     * Uncompress an image from \p _data. Using the specified \p _compression  method.
     */
    static cres_qresult<Image> fromCompressedData(const QByteArray& _data,
                                                  const QString& _compression);
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace knowCore

#include "Formatter.h"

clog_format_declare_enum_formatter(knowCore::Image::Type, UnsignedInteger8, Integer8,
                                   UnsignedInteger16, Integer16, UnsignedInteger32, Integer32,
                                   Float32, Float64);

#include "MetaType.h"
KNOWCORE_DECLARE_FULL_METATYPE(knowCore, Image);
