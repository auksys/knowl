#pragma once

namespace knowCore::Interfaces
{
  template<typename _T_>
  class Iterable
  {
  public:
    virtual ~Iterable() {}
    virtual _T_ next() = 0;
    virtual bool hasNext() const = 0;
  };
} // namespace knowCore::Interfaces
