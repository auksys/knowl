#include "IoEvent.h"

#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <string.h>
#include <unistd.h>

#include <clog_qt>

using namespace knowCore;

struct IoEvent::Private
{
  int m_pipe[2];
};

IoEvent::IoEvent() : d(new Private)
{
  if(pipe(d->m_pipe) < 0)
  {
    clog_error("Failed to create pipe: {} ({})", strerror(errno), errno);
    return;
  }

  if(fcntl(d->m_pipe[0], F_SETFL, O_NONBLOCK) < 0)
  {
    clog_error("Failed to set pipe non-blocking mode: {} ({})", strerror(errno), errno);
    return;
  }
}

IoEvent::~IoEvent()
{
  if(d->m_pipe[0] >= 0)
  {
    close(d->m_pipe[0]);
    d->m_pipe[0] = -1;
  }

  if(d->m_pipe[1] >= 0)
  {
    close(d->m_pipe[1]);
    d->m_pipe[1] = -1;
  }
  delete d;
}

void IoEvent::set()
{
  if(write(d->m_pipe[1], "x", 1) != 1)
  {
    clog_error("Error when writting to IoEvent pipe");
  }
}

void IoEvent::reset()
{
  uint8_t buf;
  while(read(d->m_pipe[0], &buf, 1) == 1)
    ;
}

int IoEvent::fd() const { return d->m_pipe[0]; }
