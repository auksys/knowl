namespace knowCore
{

  class IoEvent
  {
  public:
    IoEvent();
    ~IoEvent();

    /**
     * Set the event to signalled state.
     */
    void set();

    /**
     * Reset the event from signalled state.
     */
    void reset();

    int fd() const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowCore
