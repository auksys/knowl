#include "Interfaces/Iterable.h"

#include <knowCore/Forward.h>

namespace knowCore::Iterators
{

  /**
   * @ingroup knowCore
   * Aggregate several iterators into a single one
   */
  template<typename _T_>
  class Aggregator : public Interfaces::Iterable<_T_>
  {
  public:
    Aggregator(QList<Interfaces::IterableSP<_T_>> _iterators) : m_iterators(_iterators)
    {
      skipToNext();
    }
    ~Aggregator() {}
  public:
    _T_ next() override
    {
      _T_ t = m_iterators[m_index]->next();
      skipToNext();
      return t;
    }
    bool hasNext() const override
    {
      return m_index < m_iterators.size() and m_iterators[m_index]->hasNext();
    }
  private:
    void skipToNext()
    {
      while(m_index < m_iterators.size() and not m_iterators[m_index]->hasNext())
      {
        ++m_index;
      }
    }
  private:
    QList<Interfaces::IterableSP<_T_>> m_iterators;
    qsizetype m_index = 0;
  };
  template<typename _T_>
  Interfaces::IterableSP<_T_> makeAggregator(const QList<Interfaces::IterableSP<_T_>>& _iterators)
  {
    return Interfaces::IterableSP<_T_>(new Aggregator<_T_>(_iterators));
  }

} // namespace knowCore::Iterators