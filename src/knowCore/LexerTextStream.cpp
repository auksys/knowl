#include "LexerTextStream.h"

#include <clog_qt>

#include <QIODevice>
#include <QList>
#include <QTextStream>

using namespace knowCore;

struct LexerTextStream::Private
{
  int buffer_position = 0;
  int buffer_current_size = 0;
  QString buffer;
  QTextStream* textStream = nullptr;
  int col = 1;
  int line = 1;
  QList<Element> ungotten;
  std::size_t bufferSize = 1000;
  bool fillBuffer(int _minimumAvailableSize, bool _allow_reset);
  bool hasEnoughData(int _size) { return buffer_position + _size <= buffer_current_size; }
  QStringView read(int _size)
  {
    clog_assert(hasEnoughData(_size));
    QStringView r = QStringView(buffer).sliced(buffer_position, _size);
    buffer_position += _size;
    return r;
  }
  bool tryRead(int _size, QString* _r)
  {
    if(fillBuffer(_size, true))
    {
      *_r = read(_size).toString();
      return true;
    }
    else
    {
      return false;
    }
  }
};

bool LexerTextStream::Private::fillBuffer(int _minimumAvailableSize, bool _allow_reset)
{
  if(hasEnoughData(_minimumAvailableSize))
    return true;

  if(textStream and not textStream->atEnd())
  {
    if(_allow_reset)
    {
      std::size_t length = buffer_current_size - buffer_position;
      for(std::size_t i = 0; i < length; ++i)
      {
        buffer[uint(i)] = buffer[uint(i + buffer_position)];
      }
      buffer_position = 0;
      buffer_current_size = length;
    }
    if(_minimumAvailableSize > buffer.size() - buffer_position)
    {
      // Expand the buffer
      buffer
        = buffer + QString().fill(' ', _minimumAvailableSize - buffer.size() + buffer_current_size);
    }
    while(not textStream->atEnd() and not hasEnoughData(_minimumAvailableSize))
    {
      QChar c;
      *textStream >> c;
      buffer[buffer_current_size] = c;
      ++buffer_current_size;
    }
    return hasEnoughData(_minimumAvailableSize);
  }
  return false;
}

LexerTextStream::LexerTextStream() : d(new Private) {}

LexerTextStream::LexerTextStream(QIODevice* _sstream) : LexerTextStream() { setDevice(_sstream); }

LexerTextStream::LexerTextStream(const QString& _string) : LexerTextStream() { setString(_string); }

LexerTextStream::~LexerTextStream()
{
  delete d->textStream;
  delete d;
}

void LexerTextStream::setBufferSize(std::size_t _size) { d->bufferSize = _size; }

std::size_t LexerTextStream::bufferSize() { return d->bufferSize; }

std::size_t LexerTextStream::currentBufferSize() { return d->buffer.size(); }

void LexerTextStream::setDevice(QIODevice* _sstream)
{
  if(not _sstream->isOpen())
  {
    _sstream->open(QIODevice::ReadOnly);
  }
  d->textStream = new QTextStream(_sstream);
  d->buffer_current_size = 0;
  d->buffer_position = 0;
  d->col = 1;
  d->line = 1;
}

void knowCore::LexerTextStream::setString(const QString& _string)
{
  delete d->textStream;
  d->textStream = nullptr;
  d->buffer = _string;
  d->buffer_position = 0;
  d->buffer_current_size = _string.size();
  d->col = 1;
  d->line = 1;
}

LexerTextStream::Element LexerTextStream::getNextNonSeparatorChar()
{
  if(eof())
    return Element{QString(), d->line, d->col, true};
  Element lastElement;
  while((lastElement = getNextChar()).isSpace())
  { // Ignore space
    if(eof())
      return Element{QString(), d->line, d->col, true};
  }
  return lastElement;
}

LexerTextStream::Element LexerTextStream::getNextChar()
{
  if(d->ungotten.isEmpty())
  {
    Element nc{QString(), d->line, d->col, false};
    if(d->fillBuffer(1, true))
    {
      nc.content = d->read(1).toString();
      if(nc.content == '\n')
      {
        ++d->line;
        d->col = 1;
      }
      else if(nc.content[0] == '\\')
      {
        if(not d->fillBuffer(1, true))
        {
          d->col += 1;
          return nc;
        }
        QStringView fc = d->read(1);

        // Support for unicode escape string
        if(fc == 'u')
        {
          if(not d->fillBuffer(4, true))
          {
            return {QString(), d->line, d->col, true};
          }
          d->col += 5;
          nc.content = QChar(d->read(4).toUShort(nullptr, 16));
        }
        else if(fc == 'U')
        {
          if(not d->fillBuffer(8, true))
          {
            return {QString(), d->line, d->col, true};
          }
          d->col += 9;
          nc.content = QChar(d->read(4).toUShort(nullptr, 16));
          nc.content += QChar(d->read(4).toUShort(nullptr, 16));
        }
        else
        {
          d->col += 1;
          d->ungotten.append({fc.toString(), d->line, d->col, false});
        }
      }
      else
      {
        d->col += 1;
      }
    }
    else
    {
      if(not eof())
      {
        clog_error("Could not fill the buffer but not eof, returning eof regardless.");
      }
      nc.eof = true;
    }
    clog_assert(nc.content.size() > 0 or nc.eof);
    return nc;
  }
  else
  {
    return d->ungotten.takeLast();
  }
}

std::tuple<LexerTextStream::Element, bool> LexerTextStream::getString(const QString& terminator)
{
  LexerTextStream::Element string;
  string.eof = false;
  if(d->ungotten.isEmpty())
  {
    string.column = d->col - terminator.size();
    string.line = d->line;
  }
  else
  {
    string.column = d->ungotten.last().column - terminator.size();
    string.line = d->ungotten.last().line;
    while(not d->ungotten.isEmpty())
    {
      LexerTextStream::Element e = d->ungotten.takeLast();
      string.content += e.content;
      if(string.content.endsWith(terminator))
      {
        string.content = string.content.left(string.content.size() - terminator.size());
        return std::make_tuple(string, true);
      }
      else if(string.content.endsWith("\n") and terminator.size() == 1)
      {
        string.content.chop(1);
        return std::make_tuple(string, false);
      }
    }
    d->ungotten.clear();
  }

  bool escaping = false;
  bool singleTerminator = terminator.size() == 1;

  int current_index = d->buffer_position;

#define FINISH_STRING(_OFFSET_)                                                                    \
  string.content                                                                                   \
    += d->buffer.mid(d->buffer_position, current_index - d->buffer_position + _OFFSET_);           \
  d->buffer_position = current_index + 1

  while(d->fillBuffer(current_index - d->buffer_position + 1, false))
  {
    ++d->col;
    QChar c = d->buffer[current_index];
    ++current_index;

    if(c == '\n')
    {
      if(singleTerminator)
      {
        --current_index;
        FINISH_STRING(0);
        --d->buffer_position;
        return std::make_tuple(string, false);
      }
      else
      {
        ++d->line;
        d->col = 1;
      }
    }
    else if(c == terminator[0] and not escaping)
    {
      int k = 1;
      for(; k < terminator.length(); ++k)
      {
        if(d->fillBuffer(current_index - d->buffer_position + 1, false))
        {
          ++d->col;
          if(terminator[k] != d->buffer[current_index])
          {
            break;
          }
          ++current_index;
        }
      }
      if(k == terminator.length())
      {
        current_index -= terminator.length();
        FINISH_STRING(0);
        d->buffer_position += terminator.length() - 1;
        return std::make_tuple(string, true);
      }
      else
      {
        --d->col;
      }
    }
    else
    {
      if(escaping)
      {
        escaping = false;
      }
      else if(c == '\\')
      {
        FINISH_STRING(-1);
        d->buffer_position = current_index;
        escaping = true;
      }
    }
  }
  FINISH_STRING(0);
  return std::make_tuple(string, false);
}

std::tuple<LexerTextStream::Element, bool> LexerTextStream::getDigit(LexerTextStream::Element start)
{
  knowCore::LexerTextStream::Element number = start;
  bool integer = true;
  bool exponent = false;
  bool rightAfterExponent = false;
  knowCore::LexerTextStream::Element lastChar;
  bool not_eof;
  while((not_eof = d->tryRead(1, &lastChar.content))
        and (lastChar.isDigit() or lastChar == '.' or (lastChar == 'e' and not exponent)
             or (rightAfterExponent and lastChar == '-')
             or (rightAfterExponent and lastChar == '+')))
  {
    rightAfterExponent = false;
    number.content += lastChar.content;
    if(lastChar == '.')
    {
      integer = false;
    }
    if(lastChar == 'e')
    {
      integer = false;
      exponent = true;
      rightAfterExponent = true;
    }
  }
  if(not_eof)
  {
    lastChar.column = number.column + number.content.size() + 1;
    lastChar.line = number.line;
    lastChar.eof = false;
    unget(lastChar);
  }
  d->col = lastChar.column;
  return std::make_tuple(number, integer);
}

void LexerTextStream::unget(LexerTextStream::Element c)
{
  clog_assert(c.content.size() > 0 or c.eof);
  d->ungotten.append(c);
  for(int i = 0; i < d->ungotten.size() - 1; ++i)
  {
    clog_assert(d->ungotten[i].line > d->ungotten[i + 1].line
                or d->ungotten[i].column > d->ungotten[i + 1].column);
  }
}

bool LexerTextStream::eof() const
{
  return d->buffer_position >= d->buffer_current_size
         and (not d->textStream or d->textStream->atEnd()) and d->ungotten.isEmpty();
}
