#include <QString>

class QIODevice;

namespace knowCore
{
  /**
   * Implement convenient support for lexing SPARQL/Turtle-like languages.
   *
   * It allows:
   *  - to get the next character in the stream (\ref getNextChar),
   *  - to get the next non seperator character (\ref getNextNonSeparatorChar),
   *  - to push back any character (\ref unget)
   *  - to get a string (\ref getString)
   *  - to get a digit (\ref getDigit)
   */
  class LexerTextStream
  {
  public:
    LexerTextStream();
    /**
     * Construct a stream reading from a \ref QIODevice.
     * With this constructor, \ref LexerTextStream uses an internal buffer.
     */
    LexerTextStream(QIODevice* sstream);
    /**
     * Construct a stream reading from a \ref QString.
     * With this constructor, \ref LexerTextStream 's internal buffer is
     * equal to \ref string.
     */
    LexerTextStream(const QString& string);
    ~LexerTextStream();
    void setDevice(QIODevice* sstream);
    void setString(const QString& string);
    struct Element
    {
      QString content;
      int line;
      int column;
      bool eof;
      bool operator==(char c) const { return content.size() == 1 and content[0] == c; }
      bool operator!=(char c) const { return not(*this == c); }
#define KNOWCORE_CORE_LEXERBASE_CHAR_F(_F_)                                                        \
  bool _F_() const { return content.size() == 1 and content[0]._F_(); }
      KNOWCORE_CORE_LEXERBASE_CHAR_F(isSpace)
      KNOWCORE_CORE_LEXERBASE_CHAR_F(isLetter)
      KNOWCORE_CORE_LEXERBASE_CHAR_F(isDigit)
      bool isLetterOrDigit() const
      {
        return content.size() == 1 and (content[0].isLetter() or content[0].isDigit());
      }
    };
    /**
     * @return the next char and increment the column counter.
     */
    Element getNextChar();
    /**
     * @return the next char that is not a separator (space, tab, return...)
     */
    Element getNextNonSeparatorChar();
    /**
     * Cancel the reading of the previous char.
     */
    void unget(Element c);
    /**
     * Read a string, the boolean is true if the terminator was found, false otherwise
     */
    std::tuple<Element, bool> getString(const QString& terminator);
    /**
     * Read a digit, the boolean is true if integer
     */
    std::tuple<Element, bool> getDigit(Element start);
    bool eof() const;
    /**
     * Set how many bytes are read in the buffer to \ref _size.
     */
    void setBufferSize(std::size_t _size);
    /**
     * @return how many bytes are read in the buffer at once
     */
    std::size_t bufferSize();
    /**
     * @return the number of bytes currently held in the internal buffer
     *
     * This value might differs from \ref bufferSize for various reasons:
     *  - not enough data was available in the stream
     *  - if \ref LexerTextStream was initialised with a string, the buffer
     *    is equal to the string and the \ref currentBufferSize is equal to
     *    the length of the string
     *  - when reading a string, the entire string has to be contained in the
     *    buffer
     */
    std::size_t currentBufferSize();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowCore
