#pragma once

#include <QMutex>
#include <clog_qt>

namespace knowCore
{
  template<typename _T_>
  class Locked;
  template<typename _T_>
  class Unlocked
  {
    friend class Locked<_T_>;
    Unlocked(_T_* _d, QMutex* _m) : d(_d), m(_m)
    {
      m->lock();
      locked = true;
    }
  public:
    ~Unlocked() { m->unlock(); }
    _T_* operator->()
    {
      clog_assert(locked);
      return d;
    }
    _T_& operator*()
    {
      clog_assert(locked);
      return *d;
    }
    void unlock()
    {
      m->unlock();
      locked = false;
    }
    void relock()
    {
      m->lock();
      locked = true;
    }
  public:
    _T_* d;
    QMutex* m;
    bool locked = false;
  };
  /**
   * Allow to create an object which is protected by a mutex.
   * The only method to access the data is by first calling \ref unlocked.
   */
  template<typename _T_>
  class Locked
  {
  public:
    Locked(const _T_& _d = _T_()) : d(_d) {}
    Unlocked<_T_> unlocked() { return Unlocked<_T_>(&d, &m); }
  private:
    _T_ d;
    QMutex m;
  };
} // namespace knowCore
