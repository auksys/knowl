#include "MemoryLeakTracker.h"

#include <QDebug>
#include <QGlobalStatic>
#include <QMutex>

#include <clog_qt>

#include "config.h"

// Only linux support the memory leak tracker
#ifndef Q_OS_LINUX
#undef KNOWCORE_ENABLE_MEMORY_LEAK_TRACKER
#endif

// Disable the memory leak tracker on release build
#ifdef NDEBUG
#undef KNOWCORE_ENABLE_MEMORY_LEAK_TRACKER
#endif

using namespace knowCore;

Q_GLOBAL_STATIC(MemoryLeakTracker, s_instance)

// Common function
MemoryLeakTracker* MemoryLeakTracker::instance() { return s_instance; }

#ifdef KNOWCORE_ENABLE_MEMORY_LEAK_TRACKER

#include <QHash>

#ifdef Q_OS_LINUX

struct BacktraceInfo
{
  BacktraceInfo() : trace(0), size(0) {}
  ~BacktraceInfo() { delete[] trace; }
  void** trace;
  int size;
};

#define BACKTRACE_SIZE 20

#include <execinfo.h>

#ifdef KNOWCORE_ENABLE_MEMORY_LEAK_TRACKER_BACKTRACE_SUPPORT
#define MAKE_BACKTRACEINFO                                                                         \
  BacktraceInfo* info = new BacktraceInfo;                                                         \
  info->trace = new void*[BACKTRACE_SIZE];                                                         \
  int n = backtrace(info->trace, BACKTRACE_SIZE);                                                  \
  info->size = n;
#else
#define MAKE_BACKTRACEINFO BacktraceInfo* info = 0;
#endif

struct WhatInfo
{
  QHash<const void*, BacktraceInfo*> infos;
  QString name;
};

struct MemoryLeakTracker::Private
{
  QHash<const void*, WhatInfo> whatWhoWhen;
  template<typename _T_>
  void dumpReferencedObjectsAndDelete(QHash<const _T_*, WhatInfo>&, bool _delete);
  QMutex m;
};

template<typename _T_>
void MemoryLeakTracker::Private::dumpReferencedObjectsAndDelete(QHash<const _T_*, WhatInfo>& map,
                                                                bool _delete)
{
  QMutexLocker l(&m);

  for(typename QHash<const _T_*, WhatInfo>::iterator it = map.begin(); it != map.end(); ++it)
  {
    qWarning() << "Object " << it.key() << "(" << it.value().name << ") is still referenced by "
               << it.value().infos.size() << " objects:";

    for(QHash<const void*, BacktraceInfo*>::iterator it2 = it.value().infos.begin();
        it2 != it.value().infos.end(); ++it2)
    {
      qWarning() << "Referenced by " << it2.key() << " at:";
#ifdef KNOWCORE_ENABLE_MEMORY_LEAK_TRACKER_BACKTRACE_SUPPORT
      BacktraceInfo* info = it2.value();
      char** strings = backtrace_symbols(info->trace, info->size);

      for(int i = 0; i < info->size; ++i)
      {
        qWarning() << strings[i];
      }

      if(_delete)
      {
        delete info;
        it2.value() = 0;
      }

#else
      Q_UNUSED(_delete);
      qWarning() << "Enable backtrace support by running 'cmake "
                    "-DKNOWCORE_ENABLE_MEMORY_LEAK_TRACKER_BACKTRACE_SUPPORT=ON'";
#endif
    }

    qWarning() << "=====";
  }
}

MemoryLeakTracker::MemoryLeakTracker() : d(new Private) {}

MemoryLeakTracker::~MemoryLeakTracker()
{
  if(d->whatWhoWhen.isEmpty())
  {
    qInfo() << "No leak detected.";
  }
  else
  {
    qWarning() << "****************************************";
    qWarning() << (d->whatWhoWhen.size()) << " leaks have been detected";
    d->dumpReferencedObjectsAndDelete(d->whatWhoWhen, true);
    qWarning() << "****************************************";
#ifndef NDEBUG
    qFatal("Leaks have been detected... fix your software.");
#endif
  }

  delete d;
}

void MemoryLeakTracker::reference(const void* what, const void* bywho, const char* whatName)
{
  if(what == 0x0)
    return;

  QMutexLocker l(&d->m);

  if(whatName == 0 || (strcmp(whatName, "PK13KisSharedData") != 0))
  {
    MAKE_BACKTRACEINFO
    d->whatWhoWhen[what].infos[bywho] = info;

    if(whatName)
    {
      d->whatWhoWhen[what].name = whatName;
    }
  }
}

void MemoryLeakTracker::dereference(const void* what, const void* bywho)
{
  QMutexLocker l(&d->m);

  if(d->whatWhoWhen.contains(what))
  {
    QHash<const void*, BacktraceInfo*>& whoWhen = d->whatWhoWhen[what].infos;
    delete whoWhen[bywho];
    whoWhen.remove(bywho);

    if(whoWhen.isEmpty())
    {
      d->whatWhoWhen.remove(what);
    }
  }
}

void MemoryLeakTracker::dumpReferences()
{
  qWarning() << "****************************************";
  qWarning() << (d->whatWhoWhen.size()) << " objects are currently referenced";
  d->dumpReferencedObjectsAndDelete(d->whatWhoWhen, false);
  qWarning() << "****************************************";
}

void MemoryLeakTracker::dumpReferences(const void* what)
{
  QMutexLocker l(&d->m);

  if(!d->whatWhoWhen.contains(what))
  {
    qWarning() << "Object " << what << " is not tracked";
    return;
  }

  WhatInfo& info = d->whatWhoWhen[what];
  qInfo() << "Object " << what << "(" << info.name << ") is still referenced by "
          << info.infos.size() << " objects:";

  for(QHash<const void*, BacktraceInfo*>::iterator it2 = info.infos.begin();
      it2 != info.infos.end(); ++it2)
  {
    qInfo() << "Referenced by " << it2.key() << " at:";
#ifdef KNOWCORE_ENABLE_MEMORY_LEAK_TRACKER_BACKTRACE_SUPPORT
    BacktraceInfo* info = it2.value();
    char** strings = backtrace_symbols(info->trace, info->size);

    for(int i = 0; i < info->size; ++i)
    {
      qInfo() << strings[i];
    }

#else
    qInfo() << "Enable backtrace support in MemoryLeakTracker.cpp";
#endif
  }

  qInfo() << "=====";
}

#else
#error "Hum, no memory leak tracker for your platform"
#endif

#else

MemoryLeakTracker::MemoryLeakTracker() : d(0) {}

MemoryLeakTracker::~MemoryLeakTracker() {}

void MemoryLeakTracker::reference(const void* what, const void* bywho, const char* whatName)
{
  Q_UNUSED(what);
  Q_UNUSED(bywho);
  Q_UNUSED(whatName);
}

void MemoryLeakTracker::dereference(const void* what, const void* bywho)
{
  Q_UNUSED(what);
  Q_UNUSED(bywho);
}

void MemoryLeakTracker::dumpReferences() {}

void MemoryLeakTracker::dumpReferences(const void* what) { Q_UNUSED(what); }

#endif
