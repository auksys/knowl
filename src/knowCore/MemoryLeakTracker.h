#pragma once

#include "Global.h"

namespace knowCore
{

  /**
   * This class tracks what pointer is reference by who. It is used by
   * the smart pointers to detect leaks.
   *
   * Note that the KisMemoryLeakTracker is currently only available on Linux,
   * and translate to NOOP on other platforms. It is also just a debug tool,
   * and should not be used in a production build of krita.
   */
  class MemoryLeakTracker
  {
  public:
    MemoryLeakTracker();
    ~MemoryLeakTracker();
    static MemoryLeakTracker* instance();
    void reference(const void* what, const void* bywho, const char* whatName = 0);
    void dereference(const void* what, const void* bywho);
    void dumpReferences();
    void dumpReferences(const void* what);
  public:
    template<typename _T_>
    void reference(const _T_* what, const void* bywho);
    template<typename _T_>
    void dereference(const _T_* what, const void* bywho);
  private:
    struct Private;
    Private* const d;
  };

#include <typeinfo>

  template<typename _T_>
  void MemoryLeakTracker::reference(const _T_* what, const void* bywho)
  {
    reference((void*)what, bywho, qPrintable(prettyTypename<_T_>()));
  }

  template<typename _T_>
  void MemoryLeakTracker::dereference(const _T_* what, const void* bywho)
  {
    dereference((void*)what, bywho);
  }
} // namespace knowCore
