#ifndef _KNOW_CORE_MESSAGE_H_
#define _KNOW_CORE_MESSAGE_H_

#include <QSharedDataPointer>
#include <QString>

namespace knowCore
{
  /**
   * This class contains the information about a compilation message, such as the text
   * of the message, the line of occurance, and the name of the file.
   *
   * @ingroup knowCore
   */
  class Message
  {
  public:
    enum MessageType
    {
      ERROR,
      WARNING
    };
  public:
    /**
     * Construct a new \ref CompilationMessage
     */
    Message(MessageType _type, const QString& errorMessage, int line = -1,
            const QString& fileName = "");
    Message(const Message&);
    Message& operator=(const Message& rhs);
  public:
    ~Message();
    /**
     * @return the line where the message occurs
     */
    int line() const;
    /**
     * @return the text of the message
     */
    QString message() const;
    /**
     * @return the file name
     */
    QString fileName() const;
    /**
     * @return the type of message
     */
    MessageType type() const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
} // namespace knowCore

#endif
