#include "Messages.h"

#include <sstream>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include "Message.h"

using namespace knowCore;

struct Messages::Private
{
  QList<Message> errors;
  QList<Message> warnings;
  QList<Message> messages;
  void appendMessage(const Message& _message);
};

Messages::Messages() : d(new Private) {}

Messages::Messages(const Messages& _rhs) : d(new Private) { *this = _rhs; }

Messages& Messages::operator=(const Messages& _rhs)
{
  *d = *_rhs.d;
  return *this;
}

Messages::~Messages() { delete d; }

bool Messages::hasErrors() const { return not d->errors.isEmpty(); }

bool Messages::hasMessages() const { return not d->messages.isEmpty(); }

bool Messages::hasWarnings() const { return not d->warnings.isEmpty(); }

QList<Message> Messages::errors() const { return d->errors; }

QList<Message> Messages::warnings() const { return d->warnings; }

QList<Message> Messages::messages() const { return d->messages; }

void Messages::reportError(const QString& errorMessage, int line, const QString& fileName)
{
  d->appendMessage(Message(knowCore::Message::ERROR, errorMessage, line, fileName));
}

void Messages::reportWarning(const QString& warningMessage, int line, const QString& fileName)
{
  d->appendMessage(Message(knowCore::Message::WARNING, warningMessage, line, fileName));
}

void Messages::merge(const Messages& _messages)
{
  for(const Message& m : _messages.messages())
  {
    d->appendMessage(m);
  }
}

void Messages::Private::appendMessage(const Message& _message)
{
  messages.push_back(_message);
  switch(_message.type())
  {
  case Message::ERROR:
    errors.push_back(_message);
    break;
  case Message::WARNING:
    warnings.push_back(_message);
    break;
  }
}

QString Messages::toString() const
{
  std::ostringstream os;
  for(Message msg : d->messages)
  {
    switch(msg.type())
    {
    case Message::ERROR:
      os << "Error";
      break;
    case Message::WARNING:
      os << "Warning";
      break;
    }
    if(not msg.fileName().isEmpty())
    {
      os << ": " << qPrintable(msg.fileName());
    }
    if(msg.line() > 0)
    {
      os << ":" << msg.line();
    }
    os << ": " << qPrintable(msg.message()) << std::endl;
  }
  return QString::fromStdString(os.str());
}

QByteArray Messages::toJsonString() const
{
  QJsonArray array;
  for(Message msg : d->messages)
  {
    QJsonObject obj;
    switch(msg.type())
    {
    case Message::ERROR:
      obj["type"] = "Error";
      break;
    case Message::WARNING:
      obj["type"] = "Warning";
      break;
    }
    obj["filename"] = msg.fileName();
    obj["line"] = msg.line();
    obj["message"] = msg.message();
    array.append(obj);
  }
  QJsonDocument doc;
  doc.setArray(array);
  return doc.toJson();
}

cres_qresult<Messages> Messages::fromJsonString(const QByteArray& _messages)
{
  QJsonParseError error;
  QJsonDocument doc = QJsonDocument::fromJson(_messages, &error);
  if(doc.isNull())
  {
    return cres_failure(error.errorString());
  }
  if(doc.isArray())
  {
    Messages ms;
    QJsonArray array = doc.array();
    for(int i = 0; i < array.size(); ++i)
    {
      QJsonValue array_i_val = array[i];
      if(array_i_val.isObject())
      {
        QJsonObject obj = array_i_val.toObject();
        QString type = obj["type"].toString();
        QString filename = obj["filename"].toString();
        int line = obj["line"].toInt();
        QString message = obj["message"].toString();
        if(type == "Error")
        {
          ms.reportError(message, line, filename);
        }
        else if(type == "Warning")
        {
          ms.reportWarning(message, line, filename);
        }
        else
        {
          return cres_failure("Unknown type of message '{}'", type);
        }
      }
      else
      {
        return cres_failure("Expected an object got '{}'", array_i_val);
      }
    }
    return cres_success(ms);
  }
  else
  {
    return cres_failure("Expected an array got '{}'", QString::fromUtf8(_messages));
  }
}

void Messages::clear()
{
  d->errors.clear();
  d->warnings.clear();
  d->messages.clear();
}
