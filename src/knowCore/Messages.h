#ifndef _KNOW_CORE_MESSAGES_H_
#define _KNOW_CORE_MESSAGES_H_

#include <QList>
#include <QString>

#include <clog_qt>
#include <cres_qt>

namespace knowCore
{
  class Message;
  /**
   * @ingroup knowCore
   * This class countains the different compilation messages, errors and warnings.
   */
  class Messages
  {
  public:
    Messages();
    Messages& operator=(const Messages&);
    Messages(const Messages&);
    ~Messages();
    void reportError(const QString& errorMessage, int line = -1, const QString& fileName = "");
    void reportWarning(const QString& warningMessage, int line = -1, const QString& fileName = "");
    void merge(const Messages& _messages);
    bool hasErrors() const;
    bool hasWarnings() const;
    bool hasMessages() const;
    /**
     * @return the list of errors messages
     */
    QList<Message> errors() const;
    /**
     * @return the list of warnings messages
     */
    QList<Message> warnings() const;
    /**
     * @return the list of all messages
     */
    QList<Message> messages() const;
    /**
     * @return a string with all the messages, suitable for display
     */
    QString toString() const;
    void clear();
    QByteArray toJsonString() const;
    static cres_qresult<Messages> fromJsonString(const QByteArray& _messages);
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowCore

#endif
