#include "MetaType_p.h"

#include "Uri.h"
#include <clog_qt>

using namespace knowCore;

Uri MetaTypes::uri(int _type)
{
  return MetaTypeRegistryPrivate::instance()->id_2_definitions.value(_type)->uri();
}

int MetaTypes::type(const Uri& _uri)
{
  return MetaTypeRegistryPrivate::instance()->uri_2_definitions.value(_uri)->qMetaTypeId();
}

bool MetaTypes::isNumericType(const Uri& _uri)
{
  return MetaTypeRegistryPrivate::instance()->numeric_types.contains(_uri);
}

bool MetaTypes::isDefined(const Uri& _uri)
{
  return MetaTypeRegistryPrivate::instance()->uri_2_definitions.contains(_uri);
}

bool MetaTypes::canConvert(const Uri& _from, const Uri& _to, TypeCheckingMode _checking)
{
  return MetaTypeRegistryPrivate::instance()->converter(_from, _to, _checking);
}

QList<Uri> MetaTypes::aliases(const knowCore::Uri& _uri)
{
  return MetaTypeRegistryPrivate::instance()->aliases_to_from.value(_uri);
}

MetaTypeRegistryPrivate* MetaTypeRegistryPrivate::instance()
{
  static MetaTypeRegistryPrivate* s = nullptr;
  if(Q_UNLIKELY(not s))
  {
    s = new MetaTypeRegistryPrivate();
  }
  return s;
}

AbstractDeserialisationContext::AbstractDeserialisationContext() {}

AbstractDeserialisationContext::~AbstractDeserialisationContext() {}

AbstractSerialisationContext::AbstractSerialisationContext() {}
AbstractSerialisationContext::~AbstractSerialisationContext() {}

struct DeserialisationContexts::Private : public QSharedData
{
  ~Private() { qDeleteAll(contexts.values()); }
  QHash<knowCore::Uri, AbstractDeserialisationContext*> contexts;
};

DeserialisationContexts::DeserialisationContexts() : d(new Private) {}

DeserialisationContexts::DeserialisationContexts(const DeserialisationContexts& _rhs) : d(_rhs.d) {}

DeserialisationContexts& DeserialisationContexts::operator=(const DeserialisationContexts& _rhs)
{
  d = _rhs.d;
  return *this;
}

DeserialisationContexts::~DeserialisationContexts() {}

cres_qresult<AbstractDeserialisationContext*>
  DeserialisationContexts::getContext(const knowCore::Uri& _uri) const
{
  if(d->contexts.contains(_uri))
  {
    return cres_success(d->contexts.value(_uri));
  }
  else
  {
    return cres_failure("No deserialisation context available for '{}'", _uri);
  }
}

void DeserialisationContexts::addContext(const knowCore::Uri& _uri,
                                         AbstractDeserialisationContext* _context)
{
  d->contexts[_uri] = _context;
}

struct SerialisationContexts::Private : public QSharedData
{
  ~Private() { qDeleteAll(contexts.values()); }
  QHash<knowCore::Uri, AbstractSerialisationContext*> contexts;
};

SerialisationContexts::SerialisationContexts() : d(new Private) {}

SerialisationContexts::SerialisationContexts(const SerialisationContexts& _rhs) : d(_rhs.d) {}

SerialisationContexts& SerialisationContexts::operator=(const SerialisationContexts& _rhs)
{
  d = _rhs.d;
  return *this;
}

SerialisationContexts::~SerialisationContexts() {}

cres_qresult<AbstractSerialisationContext*>
  SerialisationContexts::getContext(const knowCore::Uri& _uri) const
{
  if(d->contexts.contains(_uri))
  {
    return cres_success(d->contexts.value(_uri));
  }
  else
  {
    return cres_failure("No serialisation context available for '{}'", _uri);
  }
}

void SerialisationContexts::addContext(const knowCore::Uri& _uri,
                                       AbstractSerialisationContext* _context)
{
  d->contexts[_uri] = _context;
}

AbstractMetaTypeDefinition::~AbstractMetaTypeDefinition() {}

AbstractTypeConversion::~AbstractTypeConversion() {}

AbstractTypeComparator::~AbstractTypeComparator() {}

AbstractArithmeticOperator::~AbstractArithmeticOperator() {}

void MetaTypeRegistry::registerNumericType(const knowCore::Uri& _uri)
{
  return MetaTypeRegistryPrivate::instance()->numeric_types.append(_uri);
}

void MetaTypeRegistry::registerType(knowCore::AbstractMetaTypeDefinition* _definition)
{
  clog_assert(
    not MetaTypeRegistryPrivate::instance()->id_2_definitions.contains(_definition->qMetaTypeId()));
  clog_assert(
    not MetaTypeRegistryPrivate::instance()->uri_2_definitions.contains(_definition->uri()));

  MetaTypeRegistryPrivate::instance()->id_2_definitions[_definition->qMetaTypeId()] = _definition;
  MetaTypeRegistryPrivate::instance()->uri_2_definitions[_definition->uri()] = _definition;
}

void MetaTypeRegistry::registerComparator(AbstractTypeComparator* _comparator)
{
  MetaTypeRegistryPrivate::instance()
    ->comparators[(int)_comparator->comparisonOperator()][_comparator->left()][_comparator->right()]
    = _comparator;
}

void MetaTypeRegistry::registerArithmeticOperator(AbstractArithmeticOperator* _operator)
{
  MetaTypeRegistryPrivate::instance()
    ->operators[(int)_operator->arithmeticOperator()][_operator->left()][_operator->right()]
    = _operator;
}

void MetaTypeRegistry::registerConverter(TypeCheckingMode _mode,
                                         knowCore::AbstractTypeConversion* _conversion)
{
  const knowCore::AbstractTypeConversion* conv
    = MetaTypeRegistryPrivate::instance()->converter(_conversion->from(), _conversion->to(), _mode);
  if(conv)
  {
    clog_warning("Replacing converter between '{}' and '{}'", _conversion->from(),
                 _conversion->to());
    delete conv;
  }
  switch(_mode)
  {
  case TypeCheckingMode::Safe:
    MetaTypeRegistryPrivate::instance()->safe_converters[_conversion->from()][_conversion->to()]
      = _conversion;
    break;
  case TypeCheckingMode::Force:
    MetaTypeRegistryPrivate::instance()->force_converters[_conversion->from()][_conversion->to()]
      = _conversion;
    break;
  }
}

bool MetaTypeRegistry::canConvert(const knowCore::Uri& _from, const knowCore::Uri& _to,
                                  TypeCheckingMode _conversion)
{
  return MetaTypeRegistryPrivate::instance()->converter(_from, _to, _conversion);
}

void MetaTypeRegistry::registerMetaTypeAliases(const QList<Uri>& _aliases, const Uri& _to)
{
  AbstractMetaTypeDefinition* definition
    = MetaTypeRegistryPrivate::instance()->uri_2_definitions.value(_to);
  clog_assert(definition);
  for(const Uri& u : _aliases)
  {
    MetaTypeRegistryPrivate::instance()->uri_2_definitions[u] = definition;
    MetaTypeRegistryPrivate::instance()->aliases_from_to[u] = _to;
  }
  MetaTypeRegistryPrivate::instance()->aliases_to_from[_to] += _aliases;
}

void MetaTypeRegistry::registerToVariant(const Uri& _uri, AbstractToVariantMarshal* _marshal)
{
  MetaTypeRegistryPrivate::instance()->to_variant_marshals[_uri] = _marshal;
}

void knowCore::registerMetaTypeAliases(const QList<Uri>& _aliases, const Uri& _to)
{
  MetaTypeRegistry::registerMetaTypeAliases(_aliases, _to);
}

#include "MetaTypeDefinitionString_p.h"

KNOWCORE_DEFINE_METATYPE(QString, knowCore::Uris::xsd::string, MetaTypeTraits::None)
KNOWCORE_DEFINE_METATYPE(QStringList, knowCore::Uris::askcore_datatype::stringlist,
                         MetaTypeTraits::None)
KNOWCORE_DEFINE_METATYPE(QByteArray, knowCore::Uris::askcore_datatype::binarydata,
                         MetaTypeTraits::None)
KNOWCORE_DEFINE_METATYPE(QList<QByteArray>, knowCore::Uris::askcore_datatype::binarydatalist,
                         MetaTypeTraits::None)
KNOWCORE_DEFINE_METATYPE_ALIASES(knowCore::Uris::xsd::language, knowCore::Uris::xsd::string)
