#pragma once

#include <clog_qt>
#include <cres_qt>

#include <QCborValue>
#include <QJsonValue>

#include "Formatter.h"
#include "Uri.h"
#include <clog_qt>

namespace knowCore
{
  struct MetaTypeTraits
  {
    enum
    {
      None = 0,
      Comparable = 1 << 1,          ///< Has < and == operator
      OnlyEqualComparable = 1 << 2, ///< Has == operator
      ToString = 1 << 3,
      FromString = 1 << 4,
      ToByteArray = 1 << 5,
      FromByteArray = 1 << 6,
      ToValueList = 1 << 7,
      FromValueList = 1 << 8,
      DebugStreamOperator = 1 << 9,
      NumericType = 1 << 10 ///< indicates that the type is for a numeric value
    };
  };

  enum class ComparisonOperator
  {
    Equal = 1 << 1,
    AlmostEqual = 1 << 2,
    Inferior = 1 << 3,
    GeoOverlaps = 1 << 4,   ///< geographic operator
    GeoWithin = 1 << 5,     ///< geographic operator
    GeoIntersects = 1 << 6, ///< geographic operator
    GeoContains = 1 << 7,   ///< geographic operator
    GeoTouches = 1 << 8,    ///< geographic operator
    In = 1 << 9,            ///< element in container
  };
  enum class ArithmeticOperator
  {
    Addition,
    Substraction,
    Division,
    Multiplication
  };
  Q_DECLARE_FLAGS(ComparisonOperators, ComparisonOperator)
  enum class TypeCheckingMode
  {
    Safe, ///< Only allow safe conversion (ie from uint8 to uint16)
    Force ///< Allow unsafe conversion, datalosss might occurs
  };

  /**
   * Get access to the meta information of the type _T_.
   *  - MetaTypeInformation<_T_>::uri() gives the main uri for the type
   *  - MetaTypeInformation<_T_>::uris() gives the main uri for the type and all the aliases
   *  - MetaTypeInformation<_T_>::id() is equivalent to qMetaTypeId<_T_> but inline, so faster
   */
  template<typename _T_>
  class MetaTypeInformation;
  class MetaTypeRegistry;
  class Value;
  template<typename _T_>
  class HasMetaTypeInformation : public std::false_type
  {
  };
  template<typename _T_>
  inline constexpr bool HasMetaTypeInformationV = HasMetaTypeInformation<_T_>::value;

  class MetaTypes
  {
  public:
    static Uri uri(int _type);
    static int type(const Uri& _uri);
    static bool isNumericType(const Uri& _uri);
    /**
     * @return true if the type \ref _uri given in argument is registered to be used
     *         by \ref Value and has a defined \ref AbstractMetaTypeDefinition.
     */
    static bool isDefined(const Uri& _uri);
    /**
     * @return true if there is a conversion from \ref _from to \ref _to.
     */
    static bool canConvert(const Uri& _from, const Uri& _to,
                           TypeCheckingMode _checking = TypeCheckingMode::Safe);
    /**
     * @return the list of aliases for a given type
     */
    static QList<Uri> aliases(const knowCore::Uri& _uri);
  };

  /**
   * Base class for context class used during deserialisation.
   */
  class AbstractDeserialisationContext
  {
  public:
    AbstractDeserialisationContext();
    virtual ~AbstractDeserialisationContext();
  };

  /**
   * Base class for context class used during deserialisation.
   */
  class AbstractSerialisationContext
  {
  public:
    AbstractSerialisationContext();
    virtual ~AbstractSerialisationContext();
  };

  /**
   * Store \ref AbstractDeserialisationContext that are used by the meta-type system during
   * deserialisation of objects. This is mostly useful for deserialising objects whose
   * implementation vary depending on context.
   *
   * This class use shared data, and cannot be cloned.
   */
  class DeserialisationContexts
  {
  public:
    DeserialisationContexts();
    DeserialisationContexts(const DeserialisationContexts& _rhs);
    DeserialisationContexts& operator=(const DeserialisationContexts& _rhs);
    ~DeserialisationContexts();
    /**
     * @return the context referenced by \ref _uri.
     */
    cres_qresult<AbstractDeserialisationContext*> getContext(const knowCore::Uri& _uri) const;
    /**
     * @return the context referenced by \ref _uri.
     */
    template<typename _T_>
    cres_qresult<_T_*> getContext(const knowCore::Uri& _uri) const;
    /**
     * Add a context to the set of contexts. The ownership of \p _context is transfered to this \ref
     * DeserialisationContexts.
     */
    void addContext(const knowCore::Uri& _uri, AbstractDeserialisationContext* _context);
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };

  /**
   * Store \ref AbstractSerialisationContext that are used by the meta-type system during
   * deserialisation of objects. This is mostly useful for deserialising objects whose
   * implementation vary depending on context.
   *
   * This class use shared data, and cannot be cloned.
   */
  class SerialisationContexts
  {
  public:
    SerialisationContexts();
    SerialisationContexts(const SerialisationContexts& _rhs);
    SerialisationContexts& operator=(const SerialisationContexts& _rhs);
    ~SerialisationContexts();
    /**
     * @return the context referenced by \ref _uri.
     */
    cres_qresult<AbstractSerialisationContext*> getContext(const knowCore::Uri& _uri) const;
    /**
     * @return the context referenced by \ref _uri.
     */
    template<typename _T_>
    cres_qresult<_T_*> getContext(const knowCore::Uri& _uri) const;
    /**
     * Add a context to the set of contexts. The ownership of \p _context is transfered to this \ref
     * SerialisationContexts.
     */
    void addContext(const knowCore::Uri& _uri, AbstractSerialisationContext* _context);
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };

  inline static const DeserialisationContexts& defaultDeserialisationContext()
  {
    static DeserialisationContexts contexts;
    return contexts;
  }
  inline static const SerialisationContexts& defaultSerialisationContext()
  {
    static SerialisationContexts contexts;
    return contexts;
  }

  /**
   * Definition of a knowCore MetaType, with support function for allocation, md5 hashing, json,
   * cbor and RDF serialisation.
   */
  class AbstractMetaTypeDefinition
  {
    friend class MetaTypeRegistry;
    friend class MetaTypes;
    friend class Value;
    template<typename>
    friend class MetaTypeInformation;
  protected:
    virtual ~AbstractMetaTypeDefinition();
    virtual knowCore::Uri uri() const = 0;
    /**
     * The result of calling qMetaTypeId<_T_>
     */
    virtual int qMetaTypeId() const = 0;
    virtual void* allocate() const = 0;
    virtual void* duplicate(const void*) const = 0;
    virtual void release(void* _value) const = 0;
    virtual bool compareEquals(const void* _lhs, const void* _rhs) const = 0;
    /**
     * @return the md5 hash for the given value
     */
    virtual cres_qresult<QByteArray> md5(const void* _value) const = 0;
    /**
     * @return serialise to json
     */
    virtual cres_qresult<QJsonValue> toJsonValue(const void* _value,
                                                 const SerialisationContexts& _contexts) const
      = 0;
    /**
     * Deserialise from json
     */
    virtual cres_qresult<void> fromJsonValue(void* _value, const QJsonValue& _json_value,
                                             const DeserialisationContexts& _contexts) const
      = 0;
    /**
     * @return serialise to cbor
     */
    virtual cres_qresult<QCborValue> toCborValue(const void* _value,
                                                 const SerialisationContexts& _contexts) const
      = 0;
    /**
     * Deserialise from cbor
     */
    virtual cres_qresult<void> fromCborValue(void* _value, const QCborValue& _cbor_value,
                                             const DeserialisationContexts& _contexts) const
      = 0;
    /**
     * Convert to a string representation for display purposes
     */
    virtual cres_qresult<QString> printable(const void* _value) const = 0;
    /**
     * Serialise according to RDF.
     */
    virtual cres_qresult<QString> toRdfLiteral(const void* _value,
                                               const SerialisationContexts& _contexts) const
      = 0;
    /**
     * Serialise according to RDF.
     */
    virtual cres_qresult<void> fromRdfLiteral(void* _value, const QString& _serialised,
                                              const DeserialisationContexts& _contexts) const
      = 0;
  };

  class AbstractTypeConversion
  {
  public:
    virtual ~AbstractTypeConversion();
    virtual knowCore::Uri from() const = 0;
    virtual knowCore::Uri to() const = 0;
    virtual cres_qresult<void> convert(const void* _from, void* _to) const = 0;
  };
  class AbstractTypeComparator
  {
  public:
    virtual ~AbstractTypeComparator();
    virtual ComparisonOperator comparisonOperator() const = 0;
    virtual knowCore::Uri left() const = 0;
    virtual knowCore::Uri right() const = 0;
    virtual cres_qresult<bool> compare(const void* _left, const void* _right) const = 0;
  };
  class AbstractArithmeticOperator
  {
  public:
    virtual ~AbstractArithmeticOperator() = 0;
    virtual ArithmeticOperator arithmeticOperator() const = 0;
    virtual knowCore::Uri left() const = 0;
    virtual knowCore::Uri right() const = 0;
    virtual cres_qresult<knowCore::Value> compute(const void* _left, const void* _right) const = 0;
  };

  class AbstractToVariantMarshal;

  template<typename _T_>
  void registerMetaType();

  void registerMetaTypeAliases(const QList<Uri>& _aliases, const Uri& _to);

  template<typename _TFrom_, typename _TTo_>
  void
    registerConverter(TypeCheckingMode _conversionMode,
                      const std::function<cres_qresult<void>(const _TFrom_*, _TTo_*)>& _converter);
  template<typename _TFrom_, typename _TTo_>
  void registerConverter();
  template<ComparisonOperator _operator_, typename _TLeft_, typename _TRight_>
  void registerComparator(
    const std::function<cres_qresult<bool>(const _TLeft_&, const _TRight_&)>& _comparator);
  template<ComparisonOperator _operator_, typename _TLeft_, typename _TRight_>
  void registerComparator();
  template<ArithmeticOperator _operator_, typename _TLeft_, typename _TRight_>
  void registerArithmeticOperator();

  template<typename _T_>
  void registerNumericType();

  namespace Details
  {
    template<typename _T_, bool _has_converter_>
    struct RegisterFromListConverter;
    template<typename _T_, bool _has_converter_>
    struct RegisterToListConverter;
  } // namespace Details

  class MetaTypeRegistry
  {
    template<typename _T_>
    friend class MetaTypeInformation;
    template<typename _T_>
    friend void registerMetaType();
    template<typename _TTo_, typename _TFrom_>
    friend void registerConverter(TypeCheckingMode _mode,
                                  const std::function<_TTo_(const _TFrom_&)>& _converter);
    template<typename _TFrom_, typename _TTo_>
    friend void registerConverter();
    template<typename _T_>
    friend struct Details::RegisterFromListConverter;
    template<typename _T_>
    friend struct Details::RegisterToListConverter;
    template<ComparisonOperator _operator_, typename _TLeft_, typename _TRight_>
    friend void registerComparator(
      const std::function<cres_qresult<bool>(const _TLeft_&, const _TRight_&)>& _comparator);
    template<ComparisonOperator _operator_, typename _TLeft_, typename _TRight_>
    void friend registerComparator();
    friend void registerMetaTypeAliases(const QList<Uri>& _aliases, const Uri& _to);
    template<typename _T_>
    friend void registerNumericType();
    template<ArithmeticOperator _operator_, typename _TLeft_, typename _TRight_>
    friend void registerArithmeticOperator();
  private:
    static void registerType(AbstractMetaTypeDefinition* _definition);
    static void registerConverter(TypeCheckingMode _conversionMode,
                                  AbstractTypeConversion* _conversion);
    static void registerComparator(AbstractTypeComparator* _conversion);
    static void registerArithmeticOperator(AbstractArithmeticOperator* _operator);
    static bool canConvert(const knowCore::Uri& _from, const knowCore::Uri& _to,
                           TypeCheckingMode _conversion = TypeCheckingMode::Safe);
    static void registerMetaTypeAliases(const QList<Uri>& _aliases, const Uri& _to);
    static void registerNumericType(const knowCore::Uri& _uri);
    static void registerToVariant(const knowCore::Uri& _uri, AbstractToVariantMarshal* _marshal);
  };

  template<typename _T_, class _Enabled_>
  class MetaTypeDefinition;

} // namespace knowCore

#define __KNOWCORE_DECLARE_KNOWCORE_METATYPE_KLASS(__TYPE__, __DEFINITION__)                       \
  namespace knowCore                                                                               \
  {                                                                                                \
    template<>                                                                                     \
    class HasMetaTypeInformation<__TYPE__> : public std::true_type                                 \
    {                                                                                              \
    };                                                                                             \
    template<>                                                                                     \
    class MetaTypeInformation<__TYPE__>                                                            \
    {                                                                                              \
    public:                                                                                        \
      MetaTypeInformation();                                                                       \
      static knowCore::Uri uri();                                                                  \
      __DEFINITION__                                                                               \
      static QList<knowCore::Uri> aliases() { return knowCore::MetaTypes::aliases(uri()); }        \
      static bool isNumericType() { return MetaTypes::isNumericType(uri()); }                      \
      template<typename _TOther_>                                                                  \
      static bool canConvertTo()                                                                   \
      {                                                                                            \
        return knowCore::MetaTypeRegistry::canConvert(uri(),                                       \
                                                      MetaTypeInformation<_TOther_>::uri());       \
      }                                                                                            \
      template<typename _TOther_>                                                                  \
      static bool canConvertFrom()                                                                 \
      {                                                                                            \
        return knowCore::MetaTypeRegistry::canConvert(MetaTypeInformation<_TOther_>::uri(),        \
                                                      uri());                                      \
      }                                                                                            \
      inline static int id() { return s_instance.m_id; }                                           \
    private:                                                                                       \
      void init_knowCoreMetaType();                                                                \
      int m_id;                                                                                    \
      static MetaTypeInformation<__TYPE__> s_instance;                                             \
    };                                                                                             \
  }

#define __KNOWCORE_DECLARE_KNOWCORE_METATYPE_KLASS_DEFINITION(__TYPE__)                            \
  static AbstractMetaTypeDefinition* definition();                                                 \
  inline static cres_qresult<QByteArray> md5(const __TYPE__& _value)                               \
  {                                                                                                \
    return definition()->md5(&_value);                                                             \
  }                                                                                                \
  inline static cres_qresult<QJsonValue> toJsonValue(const __TYPE__& _value,                       \
                                                     const SerialisationContexts& _contexts        \
                                                     = defaultSerialisationContext())              \
  {                                                                                                \
    return definition()->toJsonValue(&_value, _contexts);                                          \
  }                                                                                                \
  inline static cres_qresult<__TYPE__> fromJsonValue(const QJsonValue& _json_value,                \
                                                     const DeserialisationContexts& _contexts      \
                                                     = defaultDeserialisationContext())            \
  {                                                                                                \
    __TYPE__ r;                                                                                    \
    cres_try(cres_ignore, definition()->fromJsonValue(&r, _json_value, _contexts));                \
    return cres_success(r);                                                                        \
  }                                                                                                \
  inline static cres_qresult<QCborValue> toCborValue(const __TYPE__& _value,                       \
                                                     const SerialisationContexts& _contexts        \
                                                     = defaultSerialisationContext())              \
  {                                                                                                \
    return definition()->toCborValue(&_value, _contexts);                                          \
  }                                                                                                \
  inline static cres_qresult<__TYPE__> fromCborValue(const QCborValue& _cbor_value,                \
                                                     const DeserialisationContexts& _contexts      \
                                                     = defaultDeserialisationContext())            \
  {                                                                                                \
    __TYPE__ r;                                                                                    \
    cres_try(cres_ignore, definition()->fromCborValue(&r, _cbor_value, _contexts));                \
    return cres_success(r);                                                                        \
  }                                                                                                \
  inline static cres_qresult<QString> printable(const __TYPE__& _value)                            \
  {                                                                                                \
    return definition()->printable(&_value);                                                       \
  }                                                                                                \
  inline static cres_qresult<QString> toRdfLiteral(const __TYPE__& _value,                         \
                                                   const SerialisationContexts& _contexts          \
                                                   = defaultSerialisationContext())                \
  {                                                                                                \
    return definition()->toRdfLiteral(&_value, _contexts);                                         \
  }                                                                                                \
  inline static cres_qresult<__TYPE__> fromRdfLiteral(const QString& _serialised,                  \
                                                      const DeserialisationContexts& _contexts     \
                                                      = defaultDeserialisationContext())           \
  {                                                                                                \
    __TYPE__ r;                                                                                    \
    cres_try(cres_ignore, definition()->fromRdfLiteral(&r, _serialised, _contexts));               \
    return cres_success(r);                                                                        \
  }

#define KNOWCORE_DECLARE_KNOWCORE_METATYPE_KLASS(__TYPE__)                                         \
  __KNOWCORE_DECLARE_KNOWCORE_METATYPE_KLASS(                                                      \
    __TYPE__, __KNOWCORE_DECLARE_KNOWCORE_METATYPE_KLASS_DEFINITION(__TYPE__))

#define KNOWCORE_DECLARE_KNOWCORE_METATYPE_KLASS_NO_DEFINITION(__TYPE__)                           \
  __KNOWCORE_DECLARE_KNOWCORE_METATYPE_KLASS(__TYPE__, )

#define KNOWCORE_DECLARE_KNOWCORE_METATYPE_NO_DEFINITION(_NS_, _TYPE_)                             \
  KNOWCORE_DECLARE_KNOWCORE_METATYPE_KLASS_NO_DEFINITION(_NS_::_TYPE_)                             \
  namespace _NS_                                                                                   \
  {                                                                                                \
    using _TYPE_##MetaTypeInformation = knowCore::MetaTypeInformation<_TYPE_>;                     \
  }

#define KNOWCORE_DECLARE_KNOWCORE_METATYPE(_NS_, _TYPE_)                                           \
  KNOWCORE_DECLARE_KNOWCORE_METATYPE_KLASS(_NS_::_TYPE_)                                           \
  namespace _NS_                                                                                   \
  {                                                                                                \
    using _TYPE_##MetaTypeInformation = knowCore::MetaTypeInformation<_TYPE_>;                     \
  }

#define KNOWCORE_DECLARE_FULL_METATYPE(_NS_, _TYPE_)                                               \
  Q_DECLARE_METATYPE(_NS_::_TYPE_)                                                                 \
  KNOWCORE_DECLARE_KNOWCORE_METATYPE(_NS_, _TYPE_)

#define KNOWCORE_DECLARE_FULL_METATYPE_NO_DEFINITION(_NS_, _TYPE_)                                 \
  Q_DECLARE_METATYPE(_NS_::_TYPE_)                                                                 \
  KNOWCORE_DECLARE_KNOWCORE_METATYPE_NO_DEFINITION(_NS_, _TYPE_)

KNOWCORE_DECLARE_KNOWCORE_METATYPE_KLASS(QString)
KNOWCORE_DECLARE_KNOWCORE_METATYPE_KLASS(QStringList)
KNOWCORE_DECLARE_KNOWCORE_METATYPE_KLASS(QByteArray)
KNOWCORE_DECLARE_KNOWCORE_METATYPE_KLASS(QList<QByteArray>)
KNOWCORE_DECLARE_KNOWCORE_METATYPE(knowCore,
                                   Uri) // <- this has to be here and not Uri.h, because of circular
                                        // dependenct between Uri.h and MetaType.h
clog_format_declare_enum_formatter(knowCore::ComparisonOperator, Equal, Inferior, GeoOverlaps,
                                   GeoIntersects, GeoWithin, GeoContains, GeoTouches, In);
clog_format_declare_enum_formatter(knowCore::ArithmeticOperator, Addition, Substraction,
                                   Multiplication, Division);
