#include "MetaTypeImplementation.h"
#include "Uris/askcore_datatype.h"
#include "Uris/xsd.h"

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(QString)
cres_qresult<QByteArray> md5(const QString& _value) const override
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  hash.addData(_value.toUtf8());
  return cres_success(hash.result());
}
cres_qresult<QJsonValue> toJsonValue(const QString& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(QJsonValue(_value));
}
cres_qresult<void> fromJsonValue(QString* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts&) const override
{
  if(_json_value.isString())
  {
    *_value = _json_value.toString();
    return cres_success();
  }
  else
  {
    return expectedError("string", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const QString& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(QCborValue(_value));
}
cres_qresult<void> fromCborValue(QString* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts&) const override
{
  if(_cbor_value.isString())
  {
    *_value = _cbor_value.toString();
    return cres_success();
  }
  else
  {
    return expectedError("string", _cbor_value);
  }
}
cres_qresult<QString> printable(const QString& _value) const override
{
  return cres_success(_value);
}
cres_qresult<QString> toRdfLiteral(const QString& _value,
                                   const SerialisationContexts&) const override
{
  return cres_success(_value);
}
cres_qresult<void> fromRdfLiteral(QString* _value, const QString& _serialised,
                                  const DeserialisationContexts&) const override
{
  *_value = _serialised;
  return cres_success();
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(QString)

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(QByteArray)
cres_qresult<QByteArray> md5(const QByteArray& _value) const override
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  hash.addData(_value);
  return cres_success(hash.result());
}
cres_qresult<QJsonValue> toJsonValue(const QByteArray& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(QJsonValue(QString::fromLatin1(_value.toBase64())));
}
cres_qresult<void> fromJsonValue(QByteArray* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts&) const override
{
  if(_json_value.isString())
  {
    QString b64 = _json_value.toString();
    *_value = QByteArray::fromBase64(b64.toLatin1());
    return cres_success();
  }
  else
  {
    return expectedError("string", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const QByteArray& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(QCborValue(_value));
}
cres_qresult<void> fromCborValue(QByteArray* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts&) const override
{
  if(_cbor_value.isByteArray())
  {
    *_value = _cbor_value.toByteArray();
    return cres_success();
  }
  else
  {
    return expectedError("bytearray", _cbor_value);
  }
}
cres_qresult<QString> printable(const QByteArray& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const QByteArray& _value,
                                   const SerialisationContexts&) const override
{
  return cres_success(QString::fromLatin1(_value.toBase64()));
}
cres_qresult<void> fromRdfLiteral(QByteArray* _value, const QString& _serialised,
                                  const DeserialisationContexts&) const override
{
  *_value = QByteArray::fromBase64(_serialised.toLatin1());
  return cres_success();
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(QByteArray)
