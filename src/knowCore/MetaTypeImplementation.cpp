#include "MetaTypeImplementation.h"

#include "ValueList.h"

QList<knowCore::Value> knowCore::details::MoreDetails::values(const void* _from)
{
  return reinterpret_cast<const knowCore::ValueList*>(_from)->values();
}

void knowCore::details::MoreDetails::assign(const QList<knowCore::Value>& _list, void* _to)
{
  clog_debug_vn(_list, _list.size());
  *reinterpret_cast<knowCore::ValueList*>(_to) = _list;
}
