#pragma once

#include <QCborArray>
#include <QCryptographicHash>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include "Value.h"

#include "Uris/askcore_datatype.h"

namespace knowCore
{
  class BigNumber;

  // BEGIN Serialisation/Deserialisation Contexts

  namespace details
  {
    template<typename _T_, typename _TC_>
    cres_qresult<_T_*> SeDeContexts_getContext(_TC_* _contexts, const knowCore::Uri& _uri)
    {
      cres_try(auto context, _contexts->getContext(_uri));
      _T_* casted = dynamic_cast<_T_*>(context);
      if(casted)
      {
        return cres_success(casted);
      }
      else
      {
        return cres_failure("Cannot cast context to '{}'", knowCore::prettyTypename<_T_>());
      }
    }
  } // namespace details

  template<typename _T_>
  cres_qresult<_T_*> DeserialisationContexts::getContext(const knowCore::Uri& _uri) const
  {
    return details::SeDeContexts_getContext<_T_>(this, _uri);
  }

  template<typename _T_>
  cres_qresult<_T_*> SerialisationContexts::getContext(const knowCore::Uri& _uri) const
  {
    return details::SeDeContexts_getContext<_T_>(this, _uri);
  }

  // END Serialisation/Deserialisation Contexts

  // BEGIN Converter API
  class Uri;
  template<typename _TFrom_, typename _TTo_, TypeCheckingMode _TypeCheckingMode_>
  struct Converter;
  template<ComparisonOperator _operator_, typename _TLeft_, typename _TRight_>
  struct Comparator;
  template<ArithmeticOperator _operator_, typename _TLeft_, typename _TRight_>
  struct ArithmeticOperation;

  template<typename _TFrom_, typename _TTo_>
    requires(std::is_convertible_v<_TFrom_, _TTo_> and not std::is_arithmetic_v<_TFrom_>)
  struct Converter<_TFrom_, _TTo_, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const _TFrom_* _from, _TTo_* _to)
    {
      *_to = *_from;
      return cres_success();
    }
  };
  template<typename _TFrom_, typename _TTo_,
           TypeCheckingMode _TypeCheckingMode_ = TypeCheckingMode::Safe>
  inline cres_qresult<void> convert(const _TFrom_* _from, _TTo_* _to)
  {
    return Converter<_TFrom_, _TTo_, _TypeCheckingMode_>::convert(_from, _to);
  }

  template<ComparisonOperator _operator_, typename _TFrom_, typename _TTo_>
  inline cres_qresult<bool> compare(const _TFrom_& _from, _TTo_& _to)
  {
    return Comparator<_operator_, _TFrom_, _TTo_>::compare(_from, _to);
  }
  // BEGIN Comparator
  namespace details
  {
    template<typename _TLeft_, typename _TRight_>
    concept EqualComparable = requires(_TLeft_ a, _TRight_ b) {
      { a == b } -> std::convertible_to<bool>;
    };
    template<typename _TLeft_, typename _TRight_>
    concept InferiorComparable = requires(_TLeft_ a, _TRight_ b) {
      { a < b } -> std::convertible_to<bool>;
    };
    template<typename _TLeft_, typename _TRight_>
    concept EqualComparableRV = requires(_TLeft_ a, _TRight_ b) {
      { a == b } -> std::convertible_to<cres_qresult<bool>>;
    };
    template<typename _TLeft_, typename _TRight_>
    concept InferiorComparableRV = requires(_TLeft_ a, _TRight_ b) {
      { a < b } -> std::convertible_to<cres_qresult<bool>>;
    };
    template<typename _Tp>
    concept is_integral = std::is_integral_v<_Tp>;

    template<typename _TLeft_, typename _TRight_>
    concept left_unsigned_right_signed
      = std::unsigned_integral<_TLeft_> and std::signed_integral<_TRight_>
        and std::integral<_TRight_>;
    template<typename _TLeft_, typename _TRight_>
    concept left_signed_right_unsigned
      = std::signed_integral<_TLeft_> and std::unsigned_integral<_TRight_>
        and std::integral<_TLeft_>;
  } // namespace details

  template<typename _TLeft_, typename _TRight_>
    requires details::InferiorComparable<_TLeft_, _TRight_>
             and (not details::left_unsigned_right_signed<_TLeft_, _TRight_>)
             and (not details::left_signed_right_unsigned<_TLeft_, _TRight_>)
  struct Comparator<ComparisonOperator::Inferior, _TLeft_, _TRight_>
  {
    static cres_qresult<bool> compare(const _TLeft_& _left, const _TRight_& _right)
    {
      return cres_success(_left < _right);
    }
  };
  template<typename _TLeft_, typename _TRight_>
    requires details::EqualComparable<_TLeft_, _TRight_>
             and (not details::left_unsigned_right_signed<_TLeft_, _TRight_>)
             and (not details::left_signed_right_unsigned<_TLeft_, _TRight_>)
  struct Comparator<ComparisonOperator::Equal, _TLeft_, _TRight_>
  {
    static inline cres_qresult<bool> compare(const _TLeft_& _left, const _TRight_& _right)
    {
      return cres_success(_left == _right);
    }
  };

  template<typename _TLeft_, typename _TRight_>
    requires details::InferiorComparableRV<_TLeft_, _TRight_>
  struct Comparator<ComparisonOperator::Inferior, _TLeft_, _TRight_>
  {
    static cres_qresult<bool> compare(const _TLeft_& _left, const _TRight_& _right)
    {
      return _left < _right;
    }
  };
  template<typename _TLeft_, typename _TRight_>
    requires details::EqualComparableRV<_TLeft_, _TRight_>
  struct Comparator<ComparisonOperator::Equal, _TLeft_, _TRight_>
  {
    static inline cres_qresult<bool> compare(const _TLeft_& _left, const _TRight_& _right)
    {
      return _left == _right;
    }
  };
  // END Comparator

  // BEGIN ArithmeticOperation
  namespace details
  {
    template<ArithmeticOperator _operator_, typename _TLeft_, typename _TRight_>
    struct op_trait;

    template<typename _TLeft_, typename _TRight_>
    struct op_trait<ArithmeticOperator::Addition, _TLeft_, _TRight_>
    {
      static auto compute(_TLeft_ _x, _TRight_ _y) { return _x + _y; }
    };
    template<typename _TLeft_, typename _TRight_>
    struct op_trait<ArithmeticOperator::Substraction, _TLeft_, _TRight_>
    {
      static auto compute(_TLeft_ _x, _TRight_ _y) { return _x - _y; }
    };
    template<typename _TLeft_, typename _TRight_>
    struct op_trait<ArithmeticOperator::Division, _TLeft_, _TRight_>
    {
      static auto compute(_TLeft_ _x, _TRight_ _y) { return _x / _y; }
    };
    template<typename _TLeft_, typename _TRight_>
    struct op_trait<ArithmeticOperator::Multiplication, _TLeft_, _TRight_>
    {
      static auto compute(_TLeft_ _x, _TRight_ _y) { return _x * _y; }
    };

    template<typename T>
    concept arithmetic = std::is_arithmetic_v<T>;
  } // namespace details
  template<ArithmeticOperator _operator_, typename _TLeft_, typename _TRight_>
    requires details::arithmetic<_TLeft_> and details::arithmetic<_TRight_>
  struct ArithmeticOperation<_operator_, _TLeft_, _TRight_>
  {
    using op_trait = details::op_trait<_operator_, _TLeft_, _TRight_>;
    static cres_qresult<knowCore::Value> compute(const _TLeft_& _left, const _TRight_& _right)
    {
      return cres_success(Value::fromValue(op_trait::compute(_left, _right)));
    }
  };

  // END ArithmeticOperation

  namespace details
  {
    class MoreDetails
    {
      template<typename _TTo_>
      friend struct ToValueListConversionImplementation;
      template<typename _TTo_>
      friend struct FromValueListConversionImplementation;
      static QList<knowCore::Value> values(const void* _from);
      static void assign(const QList<knowCore::Value>& _list, void* _to);
    };
    template<typename _TFrom_, typename _TTo_>
    struct TypeConversionImplementation : public AbstractTypeConversion
    {
      knowCore::Uri from() const override { return MetaTypeInformation<_TFrom_>::uri(); }
      knowCore::Uri to() const override { return MetaTypeInformation<_TTo_>::uri(); }
      cres_qresult<void> convert(const void* _from, void* _to) const override
      {
        return convert(reinterpret_cast<const _TFrom_*>(_from), reinterpret_cast<_TTo_*>(_to));
      }
      virtual cres_qresult<void> convert(const _TFrom_* _from, _TTo_* _to) const = 0;
    };
    /**
     * @internal
     *
     * Implementation of a converter from a knowCore::ValueList to a QList<_T_>.
     */
    template<typename _TTo_>
    struct FromValueListConversionImplementation : public AbstractTypeConversion
    {
      knowCore::Uri from() const override { return Uris::askcore_datatype::valuelist; }
      knowCore::Uri to() const override { return MetaTypeInformation<_TTo_>::uri(); }
      cres_qresult<void> convert(const void* _from, void* _to) const override
      {
        return convert(MoreDetails::values(_from), reinterpret_cast<_TTo_*>(_to));
      }
      virtual cres_qresult<void> convert(const QList<knowCore::Value>& _from, _TTo_* _to) const
      {
        clog_debug_vn(_from, _from.size());
        QList<typename _TTo_::Type> l;
        for(const knowCore::Value& val : _from)
        {
          cres_try(typename _TTo_::Type t, val.value<typename _TTo_::Type>());
          l.append(t);
        }
        *_to = l;
        clog_debug_vn(_to->size());
        return cres_success();
      }
    };
    template<typename _TFrom_>
    struct ToValueListConversionImplementation : public AbstractTypeConversion
    {
      knowCore::Uri from() const override { return MetaTypeInformation<_TFrom_>::uri(); }
      knowCore::Uri to() const override { return Uris::askcore_datatype::valuelist; }
      cres_qresult<void> convert(const void* _from, void* _to) const override
      {
        MoreDetails::assign(convert(reinterpret_cast<const _TFrom_*>(_from)), _to);
        return cres_success();
      }
      virtual QList<knowCore::Value> convert(const _TFrom_* _from) const
      {
        clog_debug_vn(_from->size());
        QList<knowCore::Value> l;
        for(const typename _TFrom_::Type& val : *_from)
        {
          l.append(Value::fromValue(val));
        }
        return l;
      }
    };

    template<typename _TFrom_, typename _TTo_, TypeCheckingMode _TypeCheckingMode_>
    struct ConvertConversionImplementation : public TypeConversionImplementation<_TFrom_, _TTo_>
    {
      cres_qresult<void> convert(const _TFrom_* _from, _TTo_* _to) const override
      {
        return Converter<_TFrom_, _TTo_, _TypeCheckingMode_>::convert(_from, _to);
      }
    };
    template<ComparisonOperator _operator_, typename _TLeft_, typename _TRight_>
    struct TypeComparatorImplementation : public AbstractTypeComparator
    {
      ComparisonOperator comparisonOperator() const override { return _operator_; }
      knowCore::Uri left() const override { return MetaTypeInformation<_TLeft_>::uri(); }
      knowCore::Uri right() const override { return MetaTypeInformation<_TRight_>::uri(); }
      cres_qresult<bool> compare(const void* _left, const void* _right) const override
      {
        return compare(*reinterpret_cast<const _TLeft_*>(_left),
                       *reinterpret_cast<const _TRight_*>(_right));
      }
      virtual cres_qresult<bool> compare(const _TLeft_& _left, const _TRight_& _right) const = 0;
    };

    template<ComparisonOperator _operator_, typename _TLeft_, typename _TRight_>
    struct ComparatorComparatorImplementation
        : public TypeComparatorImplementation<_operator_, _TLeft_, _TRight_>
    {
      cres_qresult<bool> compare(const _TLeft_& _left, const _TRight_& _right) const override
      {
        return Comparator<_operator_, _TLeft_, _TRight_>::compare(_left, _right);
      }
    };
    /*************************
     * Artithmetic Operators *
     *************************/
    template<ArithmeticOperator _operator_, typename _TLeft_, typename _TRight_>
    struct ArtithmeticOperatorImplementation : public AbstractArithmeticOperator
    {
      ArithmeticOperator arithmeticOperator() const override { return _operator_; }
      knowCore::Uri left() const override { return MetaTypeInformation<_TLeft_>::uri(); }
      knowCore::Uri right() const override { return MetaTypeInformation<_TRight_>::uri(); }
      cres_qresult<Value> compute(const void* _left, const void* _right) const override
      {
        return compute(*reinterpret_cast<const _TLeft_*>(_left),
                       *reinterpret_cast<const _TRight_*>(_right));
      }
      virtual cres_qresult<Value> compute(const _TLeft_& _left, const _TRight_& _right) const = 0;
    };

    template<ArithmeticOperator _operator_, typename _TLeft_, typename _TRight_>
    struct ArithmeticOperationArtithmeticOperatorImplementation
        : public ArtithmeticOperatorImplementation<_operator_, _TLeft_, _TRight_>
    {
      cres_qresult<Value> compute(const _TLeft_& _left, const _TRight_& _right) const override
      {
        return ArithmeticOperation<_operator_, _TLeft_, _TRight_>::compute(_left, _right);
      }
    };
  } // namespace details

/**
 * Allow to define a specialise version of \ref knowCore::convert
 *
 * Example of use:
 *
 * \code
 * KNOWCORE_DEFINE_CONVERT(bool, _from, QString, _to)
 * {
 *   *to = *_from ? "true" : "false";
 *   return cres_success();
 * }
 * \endcode
 */
#define KNOWCORE_DEFINE_GENERIC_CONVERT(_TFrom_, _From_Arg_, _TTo_, _To_Arg_, _TypeCheckingMode_)  \
  template<>                                                                                       \
  struct knowCore::Converter<_TFrom_, _TTo_, knowCore::TypeCheckingMode::_TypeCheckingMode_>       \
  {                                                                                                \
    static inline cres_qresult<void> convert(const _TFrom_* _From_Arg_, _TTo_* _To_Arg_);          \
  };                                                                                               \
  inline cres_qresult<void>                                                                        \
    knowCore::Converter<_TFrom_, _TTo_, knowCore::TypeCheckingMode::_TypeCheckingMode_>::convert(  \
      const _TFrom_* _from, _TTo_* _to)

/**
 * Allow to define a specialise version of \ref knowCore::convert
 *
 * Example of use:
 *
 * \code
 * KNOWCORE_DEFINE_CONVERT(bool, _from, QString, _to)
 * {
 *   *to = *_from ? "true" : "false";
 *   return cres_success();
 * }
 * \endcode
 */
#define KNOWCORE_DEFINE_CONVERT(_TFrom_, _From_Arg_, _TTo_, _To_Arg_, _TypeCheckingMode_)          \
  template<>                                                                                       \
  struct knowCore::Converter<_TFrom_, _TTo_, knowCore::TypeCheckingMode::_TypeCheckingMode_>       \
  {                                                                                                \
    static inline cres_qresult<void> convert(const _TFrom_* _From_Arg_, _TTo_* _To_Arg_);          \
  };                                                                                               \
  inline cres_qresult<void>                                                                        \
    knowCore::Converter<_TFrom_, _TTo_, knowCore::TypeCheckingMode::_TypeCheckingMode_>::convert(  \
      const _TFrom_* _from, _TTo_* _to)
  // END Converter API

  // BEGIN Variant Marshal API
  class AbstractToVariantMarshal
  {
  public:
    virtual QVariant toQVariant(const void* _data) = 0;
  };

  namespace details
  {
    template<typename A>
    concept HasToVariant = requires(A a) {
      { QVariant(a.toVariant()) };
    };

    template<typename _T_>
    class ToVariantMarshalImplementation : public AbstractToVariantMarshal
    {
    public:
      QVariant toQVariant(const void* _data) override
      {
        return reinterpret_cast<const _T_*>(_data)->toVariant();
      }
    };
  } // namespace details

  // BEGIN register functions
  template<typename _T_>
  void registerMetaType()
  {
    MetaTypeRegistry::registerType(MetaTypeInformation<_T_>::definition());
    if constexpr(details::HasToVariant<_T_>)
    {
      MetaTypeRegistry::registerToVariant(MetaTypeInformation<_T_>::uri(),
                                          new details::ToVariantMarshalImplementation<_T_>());
    }
  }

  template<typename _TFrom_, typename _TTo_>
  void
    registerConverter(TypeCheckingMode _mode,
                      const std::function<cres_qresult<void>(const _TFrom_*, _TTo_*)>& _converter)
  {
    struct ConversionImplementation : public details::TypeConversionImplementation<_TFrom_, _TTo_>
    {
      ConversionImplementation(
        const std::function<cres_qresult<void>(const _TFrom_*, _TTo_*)>& _converter)
          : m_converter(_converter)
      {
      }
      cres_qresult<void> convert(const _TFrom_* _from, const _TTo_* _to) const override
      {
        return m_converter(_from, _to);
      }
      std::function<cres_qresult<void>(const _TFrom_*, _TTo_*)> m_converter;
    };
    MetaTypeRegistry::registerConverter(new ConversionImplementation(_converter));
  }

  namespace details
  {

    template<typename _TFrom_, typename _TTo_, TypeCheckingMode _TypeCheckingMode_>
    concept HasConversionMode = requires(_TFrom_* a, _TTo_* b) {
      { Converter<_TFrom_, _TTo_, _TypeCheckingMode_>::convert(a, b) };
    };

  } // namespace details

  template<typename _TFrom_, typename _TTo_>
  void registerConverter()
  {
    constexpr bool has_safe = details::HasConversionMode<_TFrom_, _TTo_, TypeCheckingMode::Safe>;
    constexpr bool has_force = details::HasConversionMode<_TFrom_, _TTo_, TypeCheckingMode::Force>;
    static_assert(has_safe or has_force, "No converter was defined.");
    if constexpr(has_safe)
    {
      MetaTypeRegistry::registerConverter(
        TypeCheckingMode::Safe,
        new details::ConvertConversionImplementation<_TFrom_, _TTo_, TypeCheckingMode::Safe>());
    }
    else if constexpr(has_force)
    {
      MetaTypeRegistry::registerConverter(
        TypeCheckingMode::Force,
        new details::ConvertConversionImplementation<_TFrom_, _TTo_, TypeCheckingMode::Force>());
    }
  }
  template<ComparisonOperator _operator_, typename _TLeft_, typename _TRight_>
  void registerComparator(
    const std::function<cres_qresult<bool>(const _TLeft_&, _TRight_&)>& _comparator)
  {
    struct ComparatorImplementation
        : public details::TypeComparatorImplementation<_operator_, _TLeft_, _TRight_>
    {
      ComparatorImplementation(
        const std::function<cres_qresult<bool>(const _TLeft_&, _TRight_&)>& _comparator)
          : m_comparator(_comparator)
      {
      }
      cres_qresult<bool> compare(const _TLeft_& _from, const _TRight_& _to) const override
      {
        return m_comparator(_from, _to);
      }
      std::function<cres_qresult<bool>(const _TLeft_&, const _TRight_&)> m_comparator;
    };
    MetaTypeRegistry::registerComparator(new ComparatorImplementation(_comparator));
  }

  template<ComparisonOperator _operator_, typename _TLeft_, typename _TRight_>
  void registerComparator()
  {
    MetaTypeRegistry::registerComparator(
      new details::ComparatorComparatorImplementation<_operator_, _TLeft_, _TRight_>());
  }
  template<typename _TLeft_, typename _TRight_>
  void registerComparators()
  {
  }

  template<typename _TLeft_, typename _TRight_, ComparisonOperator _comparator_,
           ComparisonOperator... _comparator_others_>
  void registerComparators()
  {
    registerComparator<_comparator_, _TLeft_, _TRight_>();
    registerComparators<_TLeft_, _TRight_, _comparator_others_...>();
  }

  template<ArithmeticOperator _operator_, typename _TLeft_, typename _TRight_>
  void registerArithmeticOperator()
  {
    MetaTypeRegistry::registerArithmeticOperator(
      new details::ArithmeticOperationArtithmeticOperatorImplementation<_operator_, _TLeft_,
                                                                        _TRight_>());
  }

  template<typename _TLeft_, typename _TRight_>
  void registerArithmeticOperators()
  {
  }

  template<typename _TLeft_, typename _TRight_, ArithmeticOperator _operator_,
           ArithmeticOperator... _operator_others_>
  void registerArithmeticOperators()
  {
    registerArithmeticOperator<_operator_, _TLeft_, _TRight_>();
    registerArithmeticOperators<_TLeft_, _TRight_, _operator_others_...>();
  }

  template<typename _T_>
  void registerNumericType()
  {
    MetaTypeRegistry::registerNumericType(MetaTypeInformation<_T_>::uri());
    if constexpr(not std::is_same_v<_T_, BigNumber>)
    {
      registerConverter<_T_, BigNumber>();
      registerConverter<BigNumber, _T_>();
    }
  }

  // END register functions

  // BEGIN MetaTypeDefinitionImplementation
  template<typename _T_>
  class MetaTypeDefinitionImplementation : public AbstractMetaTypeDefinition
  {
  public:
    MetaTypeDefinitionImplementation() {}
  private:
    template<typename _Q_ = MetaTypeInformation<_T_>>
      requires(details::is_complete_v<_Q_>)
    knowCore::Uri get_uri() const
    {
      return MetaTypeInformation<_T_>::uri();
    }
    template<typename _Q_ = MetaTypeInformation<_T_>>
      requires(not details::is_complete_v<_Q_>)
    knowCore::Uri get_uri() const
    {
      clog_fatal("No metatype information!");
    }
    template<typename _Q_ = MetaTypeInformation<_T_>>
      requires(details::is_complete_v<_Q_>)
    int get_qMetaTypeId() const
    {
      return MetaTypeInformation<_T_>::id();
    }
    template<typename _Q_ = MetaTypeInformation<_T_>>
      requires(not details::is_complete_v<_Q_>)
    int get_qMetaTypeId() const
    {
      clog_fatal("No metatype information!");
    }
  protected:
    knowCore::Uri uri() const override { return get_uri(); }
    int qMetaTypeId() const override { return get_qMetaTypeId(); }
    virtual cres_qresult<QByteArray> md5(const _T_& _value) const = 0;
    virtual cres_qresult<QJsonValue> toJsonValue(const _T_& _value,
                                                 const SerialisationContexts& _contexts) const
      = 0;
    virtual cres_qresult<void>
      fromJsonValue(_T_* _value, const QJsonValue& _json_value,
                    const knowCore::DeserialisationContexts& _contexts) const
      = 0;
    virtual cres_qresult<QCborValue> toCborValue(const _T_& _value,
                                                 const SerialisationContexts& _contexts) const
      = 0;
    virtual cres_qresult<void>
      fromCborValue(_T_* _value, const QCborValue& _cbor_value,
                    const knowCore::DeserialisationContexts& _contexts) const
      = 0;
    virtual cres_qresult<QString> printable(const _T_& _value) const = 0;
    virtual cres_qresult<QString> toRdfLiteral(const _T_& _value,
                                               const SerialisationContexts& _contexts) const
      = 0;
    virtual cres_qresult<void> fromRdfLiteral(_T_* _value, const QString& _serialised,
                                              const DeserialisationContexts& _contexts) const
      = 0;
  protected:
    cres_qresult<QString> toJsonString(const _T_& _value,
                                       const SerialisationContexts& _contexts) const
    {
      QJsonDocument doc;
      cres_try(QJsonValue value, toJsonValue(_value, _contexts));
      if(value.isArray())
      {
        doc.setArray(value.toArray());
      }
      else if(value.isObject())
      {
        doc.setObject(value.toObject());
      }
      else
      {
        clog_fatal("toJsonString/fromJsonString only works on object or array");
        return expectedError("object or array", doc);
      }
      return cres_success(QString::fromUtf8(doc.toJson(QJsonDocument::Compact)));
    }
    cres_qresult<void> fromJsonString(_T_* _value, const QString& _json_value,
                                      const DeserialisationContexts& _contexts) const
    {
      QJsonParseError error;
      QJsonDocument doc = QJsonDocument::fromJson(_json_value.toUtf8(), &error);
      if(doc.isNull())
      {
        return cres_failure(error.errorString());
      }
      else
      {
        if(doc.isArray())
        {
          return fromJsonValue(_value, doc.array(), _contexts);
        }
        else if(doc.isObject())
        {
          return fromJsonValue(_value, doc.object(), _contexts);
        }
        else
        {
          return expectedError("object or array", doc);
        }
      }
    }
  protected:
    void* allocate() const override { return new _T_; }
    void* duplicate(const void* _value) const override
    {
      return new _T_(*reinterpret_cast<const _T_*>(_value));
    }
    void release(void* _value) const override { delete(reinterpret_cast<_T_*>(_value)); }
    bool compareEquals(const void* _lhs, const void* _rhs) const override
    {
      return *reinterpret_cast<const _T_*>(_lhs) == *reinterpret_cast<const _T_*>(_rhs);
    }
    cres_qresult<QByteArray> md5(const void* _value) const override
    {
      return md5(*reinterpret_cast<const _T_*>(_value));
    }
    cres_qresult<QJsonValue> toJsonValue(const void* _value,
                                         const SerialisationContexts& _contexts) const override
    {
      return toJsonValue(*reinterpret_cast<const _T_*>(_value), _contexts);
    }
    cres_qresult<void> fromJsonValue(void* _value, const QJsonValue& _json_value,
                                     const DeserialisationContexts& _contexts) const override
    {
      return fromJsonValue(reinterpret_cast<_T_*>(_value), _json_value, _contexts);
    }
    cres_qresult<QCborValue> toCborValue(const void* _value,
                                         const SerialisationContexts& _contexts) const override
    {
      return toCborValue(*reinterpret_cast<const _T_*>(_value), _contexts);
    }
    cres_qresult<void> fromCborValue(void* _value, const QCborValue& _cbor_value,
                                     const DeserialisationContexts& _contexts) const override
    {
      return fromCborValue(reinterpret_cast<_T_*>(_value), _cbor_value, _contexts);
    }
    cres_qresult<QString> printable(const void* _value) const override
    {
      return printable(*reinterpret_cast<const _T_*>(_value));
    }
    cres_qresult<QString> toRdfLiteral(const void* _value,
                                       const SerialisationContexts& _contexts) const override
    {
      return toRdfLiteral(*reinterpret_cast<const _T_*>(_value), _contexts);
    }
    cres_qresult<void> fromRdfLiteral(void* _value, const QString& _serialised,
                                      const DeserialisationContexts& _contexts) const override
    {
      return fromRdfLiteral(reinterpret_cast<_T_*>(_value), _serialised, _contexts);
    }
  protected:
    template<typename _TCoJ_>
    cres_qerror expectedError(const char* _what, const _TCoJ_& _got) const
    {
      return cres_failure("Expected '{}' got '{}'", _what, _got);
    }
    template<typename _TCoJ_>
    cres_qerror expectedArrayError(const _TCoJ_& _coj) const
    {
      return expectedError("array", _coj);
    }
    template<typename _TCoJ_>
    cres_qerror expectedObjectError(const _TCoJ_& _coj) const
    {
      return expectedError("object", _coj);
    }
  };
  // END MetaTypeDefinitionImplementation

  // BEGIN MetaTypeDefinition for QList
  template<typename _T_>
  class MetaTypeDefinition<QList<_T_>> : public MetaTypeDefinitionImplementation<QList<_T_>>
  {
    using knowCore::MetaTypeDefinitionImplementation<QList<_T_>>::expectedArrayError;
    using knowCore::MetaTypeDefinitionImplementation<QList<_T_>>::toJsonString;
    using knowCore::MetaTypeDefinitionImplementation<QList<_T_>>::fromJsonString;
  protected:
    cres_qresult<QByteArray> md5(const QList<_T_>& _value) const override
    {
      QCryptographicHash hash(QCryptographicHash::Md5);
      for(const _T_& v : _value)
      {
        cres_try(QByteArray value, m_nested.md5(v));
        hash.addData(value);
      }
      return cres_success(hash.result());
    }
    cres_qresult<QJsonValue> toJsonValue(const QList<_T_>& _value,
                                         const SerialisationContexts& _contexts) const override
    {
      QJsonArray array;
      for(const _T_& v : _value)
      {
        cres_try(QJsonValue value, m_nested.toJsonValue(v, _contexts));
        array.append(value);
      }
      return cres_success(array);
    }
    cres_qresult<void>
      fromJsonValue(QList<_T_>* _value, const QJsonValue& _json_value,
                    const knowCore::DeserialisationContexts& _contexts) const override
    {
      if(_json_value.isArray())
      {
        deleteAll(*_value);
        _value->clear();
        for(const QJsonValue& value : _json_value.toArray())
        {
          _T_ v{};
          cres_try(cres_ignore, m_nested.fromJsonValue(&v, value, _contexts));
          _value->append(v);
        }
        return cres_success();
      }
      else
      {
        return expectedArrayError(_json_value);
      }
    }
    cres_qresult<QCborValue> toCborValue(const QList<_T_>& _value,
                                         const SerialisationContexts& _contexts) const override
    {
      QCborArray array;
      for(const _T_& v : _value)
      {
        cres_try(QCborValue value, m_nested.toCborValue(v, _contexts));
        array.append(value);
      }
      return cres_success(array);
    }
    cres_qresult<void>
      fromCborValue(QList<_T_>* _value, const QCborValue& _cbor_value,
                    const knowCore::DeserialisationContexts& _contexts) const override
    {
      if(_cbor_value.isArray())
      {
        deleteAll(*_value);
        _value->clear();
        for(const QCborValue& value : _cbor_value.toArray())
        {
          _T_ v{};
          cres_try(cres_ignore, m_nested.fromCborValue(&v, value, _contexts));
          _value->append(v);
        }
        return cres_success();
      }
      else
      {
        return expectedArrayError(_cbor_value);
      }
    }
    cres_qresult<QString> printable(const QList<_T_>& _value) const override
    {
      QStringList strings;
      for(const _T_& v : _value)
      {
        cres_try(QString value, m_nested.printable(v));
        strings.append(value);
      }
      return cres_success(clog_qt::qformat("[{}]", strings));
    }
    cres_qresult<QString> toRdfLiteral(const QList<_T_>& _value,
                                       const SerialisationContexts& _contexts) const override
    {
      return toJsonString(_value, _contexts);
    }
    cres_qresult<void> fromRdfLiteral(QList<_T_>* _value, const QString& _serialised,
                                      const DeserialisationContexts& _contexts) const override
    {
      return fromJsonString(_value, _serialised, _contexts);
    }
  private:
    MetaTypeDefinition<_T_> m_nested;
  };
  // END MetaTypeDefinition for QList

  namespace Details
  {
    template<typename _TFrom_, typename _TTo_>
    inline _TTo_ qt_convert(const _TFrom_& _from)
    {
      _TTo_ to;
      convert(&_from, &to).expect_success();
      return to;
    }
    inline constexpr bool valid(const char* _t) { return _t; }
    inline constexpr bool valid(const knowCore::Uri&) { return true; }
    template<typename _T_, typename _T2_, bool _has_converter_>
    class RegisterConverter;
    template<typename _T_, typename _T2_>
    struct RegisterConverter<_T_, _T2_, true>
    {
      static void registerConverter()
      {
        QMetaType::registerConverter<_T_, _T2_>(qt_convert<_T_, _T2_>);
        knowCore::registerConverter<_T_, _T2_>();
      }
    };
    template<typename _T_, typename _T2_>
    struct RegisterConverter<_T_, _T2_, false>
    {
      static void registerConverter() {}
    };
    template<typename _T_, bool _has_converter_>
    struct RegisterFromListConverter;
    template<typename _T_>
    struct RegisterFromListConverter<_T_, true>
    {
      static void registerConverter()
      {
        MetaTypeRegistry::registerConverter(
          TypeCheckingMode::Safe, new details::FromValueListConversionImplementation<_T_>());
      }
    };
    template<typename _T_>
    struct RegisterFromListConverter<_T_, false>
    {
      static void registerConverter() {}
    };
    template<typename _T_, bool _has_converter_>
    struct RegisterToListConverter;
    template<typename _T_>
    struct RegisterToListConverter<_T_, true>
    {
      static void registerConverter()
      {
        MetaTypeRegistry::registerConverter(
          TypeCheckingMode::Safe, new details::ToValueListConversionImplementation<_T_>());
      }
    };
    template<typename _T_>
    struct RegisterToListConverter<_T_, false>
    {
      static void registerConverter() {}
    };

    template<typename _T_, bool _is_numeric>
    struct RegisterNumericDataType;
    template<typename _T_>
    struct RegisterNumericDataType<_T_, true>
    {
      static void registerNumericDataType() { registerNumericType<_T_>(); }
    };
    template<typename _T_>
    struct RegisterNumericDataType<_T_, false>
    {
      static void registerNumericDataType() {}
    };
    inline knowCore::Uri makeUri(std::nullptr_t) { clog_fatal("no uri defined for type!"); }
    template<typename _T_>
    knowCore::Uri makeUri(const _T_& _uri)
    {
      return knowCore::Uri(_uri);
    }
  } // namespace Details
} // namespace knowCore

#define __KNOWCORE_DEFINE_METATYPE__INTERNAL__(_TYPE_, _URI_, _TRAITS_)                            \
  knowCore::MetaTypeInformation<_TYPE_> knowCore::MetaTypeInformation<_TYPE_>::s_instance;         \
  knowCore::MetaTypeInformation<_TYPE_>::MetaTypeInformation()                                     \
  {                                                                                                \
    m_id = qRegisterMetaType<_TYPE_>(#_TYPE_);                                                     \
    knowCore::Details::RegisterFromListConverter<                                                  \
      _TYPE_, bool((_TRAITS_) & knowCore::MetaTypeTraits::FromValueList)>::registerConverter();    \
    knowCore::Details::RegisterToListConverter<                                                    \
      _TYPE_, bool((_TRAITS_) & knowCore::MetaTypeTraits::ToValueList)>::registerConverter();      \
    knowCore::Details::RegisterConverter<                                                          \
      _TYPE_, QString,                                                                             \
      bool((_TRAITS_) & knowCore::MetaTypeTraits::ToString)>::registerConverter();                 \
    knowCore::Details::RegisterConverter<                                                          \
      QString, _TYPE_,                                                                             \
      bool((_TRAITS_) & knowCore::MetaTypeTraits::FromString)>::registerConverter();               \
    knowCore::Details::RegisterConverter<                                                          \
      _TYPE_, QByteArray,                                                                          \
      bool((_TRAITS_) & knowCore::MetaTypeTraits::ToByteArray)>::registerConverter();              \
    knowCore::Details::RegisterConverter<                                                          \
      QByteArray, _TYPE_,                                                                          \
      bool((_TRAITS_) & knowCore::MetaTypeTraits::FromByteArray)>::registerConverter();            \
    knowCore::Details::RegisterNumericDataType<                                                    \
      _TYPE_,                                                                                      \
      bool((_TRAITS_) & knowCore::MetaTypeTraits::NumericType)>::registerNumericDataType();        \
    init_knowCoreMetaType();                                                                       \
  }

#define KNOWCORE_DEFINE_QT_METATYPE(_TYPE_, _TRAITS_)                                              \
  void knowCore::MetaTypeInformation<_TYPE_>::init_knowCoreMetaType() {}                           \
  __KNOWCORE_DEFINE_METATYPE__INTERNAL__(_TYPE_, nullptr, _TRAITS_)

/**
 *
 */
#define KNOWCORE_DEFINE_METATYPE_URI(_TYPE_, _URI_)                                                \
  knowCore::Uri knowCore::MetaTypeInformation<_TYPE_>::uri()                                       \
  {                                                                                                \
    return knowCore::Details::makeUri(_URI_);                                                      \
  }

/**
 * Define metatype given by \ref _TYPE_ (this should include the full namespace definition)
 * with the \ref _URI_ and a set of \ref _TRAITS_ (defined in \ref knowCore::MetaTypeTraits).
 *
 * \p _TRAITS_ can be set to ```knowCore::MetaTypeTraits::None``` by default.
 */
#define KNOWCORE_DEFINE_METATYPE(_TYPE_, _URI_, _TRAITS_)                                          \
  KNOWCORE_DEFINE_METATYPE_URI(_TYPE_, _URI_)                                                      \
  knowCore::AbstractMetaTypeDefinition* knowCore::MetaTypeInformation<_TYPE_>::definition()        \
  {                                                                                                \
    static knowCore::AbstractMetaTypeDefinition* definition                                        \
      = new knowCore::MetaTypeDefinition<_TYPE_>();                                                \
    return definition;                                                                             \
  }                                                                                                \
  void knowCore::MetaTypeInformation<_TYPE_>::init_knowCoreMetaType()                              \
  {                                                                                                \
    registerMetaType<_TYPE_>();                                                                    \
  }                                                                                                \
  __KNOWCORE_DEFINE_METATYPE__INTERNAL__(_TYPE_, _URI_, _TRAITS_)

#define __KNOWCORE_REGISTER_CONVERSION_FROM_ONE(_TYPE_, I)                                         \
  if constexpr(not std::is_same_v<TypeFrom, _TYPE_>)                                               \
  {                                                                                                \
    knowCore::registerConverter<TypeFrom, _TYPE_>();                                               \
  }

#define __KNOWCORE_REGISTER_CONVERSION_FROM(_FACTORY_NAME_, _TYPE_FROM_, ...)                      \
  namespace                                                                                        \
  {                                                                                                \
    class _FACTORY_NAME_                                                                           \
    {                                                                                              \
    public:                                                                                        \
      _FACTORY_NAME_()                                                                             \
      {                                                                                            \
        typedef _TYPE_FROM_ TypeFrom;                                                              \
        KNOWCORE_FOREACH(__KNOWCORE_REGISTER_CONVERSION_FROM_ONE, __VA_ARGS__)                     \
      }                                                                                            \
      static _FACTORY_NAME_ s_instance;                                                            \
    };                                                                                             \
    _FACTORY_NAME_ _FACTORY_NAME_::s_instance;                                                     \
  }

/**
 * Allow to define conversion from one type to many other.
 *
 * For instance the following define conversion from `double` to `int` and `float`:
 * \code
 *  KNOWCORE_REGISTER_CONVERSION_FROM(double, int, float)
 * \endcode
 */
#define KNOWCORE_REGISTER_CONVERSION_FROM(_TYPE_FROM_, ...)                                        \
  __KNOWCORE_REGISTER_CONVERSION_FROM(__KNOWCORE_UNIQUE_STATIC_NAME(MetaTypeRegisterConversion),   \
                                      _TYPE_FROM_, __VA_ARGS__)

#define __KNOWCORE_REGISTER_CONVERSION_TO_ONE(_TYPE_, I)                                           \
  if constexpr(not std::is_same_v<_TYPE_, TypeTo>)                                                 \
  {                                                                                                \
    knowCore::registerConverter<_TYPE_, TypeTo>();                                                 \
  }

#define __KNOWCORE_REGISTER_CONVERSION_TO(_FACTORY_NAME_, _TYPE_TO_, ...)                          \
  namespace                                                                                        \
  {                                                                                                \
    class _FACTORY_NAME_                                                                           \
    {                                                                                              \
    public:                                                                                        \
      _FACTORY_NAME_()                                                                             \
      {                                                                                            \
        typedef _TYPE_TO_ TypeTo;                                                                  \
        KNOWCORE_FOREACH(__KNOWCORE_REGISTER_CONVERSION_TO_ONE, __VA_ARGS__)                       \
      }                                                                                            \
      static _FACTORY_NAME_ s_instance;                                                            \
    };                                                                                             \
    _FACTORY_NAME_ _FACTORY_NAME_::s_instance;                                                     \
  }

/**
 * Allow to define conversion from many types to one type.
 *
 * For instance the following define conversion from `int` and `float` to `double`:
 * \code
 *  KNOWCORE_REGISTER_CONVERSION_TO(double, int, float)
 * \endcode
 */
#define KNOWCORE_REGISTER_CONVERSION_TO(_TYPE_TO_, ...)                                            \
  __KNOWCORE_REGISTER_CONVERSION_TO(__KNOWCORE_UNIQUE_STATIC_NAME(MetaTypeRegisterConversion),     \
                                    _TYPE_TO_, __VA_ARGS__)

// BEGIN KNOWCORE_REGISTER_COMPARATORS

#define __KNOWCORE_REGISTER_COMPARATORS_FROM_ONE(_TYPE_, I)                                        \
  knowCore::registerComparators<_LeftType_, _TYPE_, _comparators_...>();

#define __KNOWCORE_REGISTER_COMPARATORS_MAKE_COMPARATOR_ONE(_COMPARATOR_, I)                       \
  , knowCore::ComparisonOperator::_COMPARATOR_
#define __KNOWCORE_REGISTER_COMPARATOR_MAKE_COMPARATORS(...)                                       \
  KNOWCORE_FOREACH(__KNOWCORE_REGISTER_COMPARATORS_MAKE_COMPARATOR_ONE, __VA_ARGS__)

#define __KNOWCORE_REGISTER_COMPARATORS(_FACTORY_NAME_, _COMPARATORS_, _LEFT_, ...)                \
  namespace                                                                                        \
  {                                                                                                \
    class _FACTORY_NAME_                                                                           \
    {                                                                                              \
    public:                                                                                        \
      _FACTORY_NAME_()                                                                             \
      {                                                                                            \
        registerComparators<                                                                       \
          _LEFT_ __KNOWCORE_REGISTER_COMPARATOR_MAKE_COMPARATORS _COMPARATORS_>();                 \
      }                                                                                            \
      static _FACTORY_NAME_ s_instance;                                                            \
    private:                                                                                       \
      template<typename _LeftType_, knowCore::ComparisonOperator... _comparators_>                 \
      void registerComparators()                                                                   \
      {                                                                                            \
        KNOWCORE_FOREACH(__KNOWCORE_REGISTER_COMPARATORS_FROM_ONE, __VA_ARGS__)                    \
      }                                                                                            \
    };                                                                                             \
    _FACTORY_NAME_ _FACTORY_NAME_::s_instance;                                                     \
  }

#define KNOWCORE_REGISTER_COMPARATORS_FROM(_OPERATORS_, _LEFT_TYPE_, ...)                          \
  __KNOWCORE_REGISTER_COMPARATORS(__KNOWCORE_UNIQUE_STATIC_NAME(MetaTypeRegisterComparators),      \
                                  _OPERATORS_, _LEFT_TYPE_, __VA_ARGS__)

// END KNOWCORE_REGISTER_COMPARATOR

/**
 * Register comparators for a type
 */
#define KNOWCORE_REGISTER_COMPARATORS(_COMPARATORS_, _TYPE_)                                       \
  KNOWCORE_REGISTER_COMPARATORS_FROM(_COMPARATORS_, _TYPE_, _TYPE_)

// BEGIN KNOWCORE_REGISTER_ARITHMETIC_OPERATOR

#define __KNOWCORE_REGISTER_ARITHMETIC_OPERATOR_FROM_ONE(_TYPE_, I)                                \
  knowCore::registerArithmeticOperators<_LeftType_, _TYPE_, _operators_...>();

#define __KNOWCORE_REGISTER_ARITHMETIC_OPERATOR_MAKE_OPERATORS_ONE(_OPERATOR_, I)                  \
  , knowCore::ArithmeticOperator::_OPERATOR_
#define __KNOWCORE_REGISTER_ARITHMETIC_OPERATOR_MAKE_OPERATORS(...)                                \
  KNOWCORE_FOREACH(__KNOWCORE_REGISTER_ARITHMETIC_OPERATOR_MAKE_OPERATORS_ONE, __VA_ARGS__)

#define __KNOWCORE_REGISTER_ARITHMETIC_OPERATOR(_FACTORY_NAME_, _OPERATORS_, _LEFT_, ...)          \
  namespace                                                                                        \
  {                                                                                                \
    class _FACTORY_NAME_                                                                           \
    {                                                                                              \
    public:                                                                                        \
      _FACTORY_NAME_()                                                                             \
      {                                                                                            \
        registerArithmeticOperators<                                                               \
          _LEFT_ __KNOWCORE_REGISTER_ARITHMETIC_OPERATOR_MAKE_OPERATORS _OPERATORS_>();            \
      }                                                                                            \
      static _FACTORY_NAME_ s_instance;                                                            \
    private:                                                                                       \
      template<typename _LeftType_, ArithmeticOperator... _operators_>                             \
      void registerArithmeticOperators()                                                           \
      {                                                                                            \
        KNOWCORE_FOREACH(__KNOWCORE_REGISTER_ARITHMETIC_OPERATOR_FROM_ONE, __VA_ARGS__)            \
      }                                                                                            \
    };                                                                                             \
    _FACTORY_NAME_ _FACTORY_NAME_::s_instance;                                                     \
  }

#define KNOWCORE_REGISTER_ARITHMETIC_OPERATORS(_OPERATORS_, _LEFT_TYPE_, ...)                      \
  __KNOWCORE_REGISTER_ARITHMETIC_OPERATOR(                                                         \
    __KNOWCORE_UNIQUE_STATIC_NAME(MetaTypeRegisterArithmeticOperator), _OPERATORS_, _LEFT_TYPE_,   \
    __VA_ARGS__)

// END KNOWCORE_REGISTER_ARITHMETIC_OPERATOR

#define KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION_TEMPLATE(_TYPE_)                             \
  class knowCore::MetaTypeDefinition<_TYPE_>                                                       \
      : public knowCore::MetaTypeDefinitionImplementation<_TYPE_>                                  \
  {                                                                                                \
    template<typename _TT_, typename _ENABLED_>                                                    \
    friend class knowCore::MetaTypeDefinition;                                                     \
    using knowCore::MetaTypeDefinitionImplementation<_TYPE_>::expectedError;                       \
    using knowCore::MetaTypeDefinitionImplementation<_TYPE_>::expectedObjectError;                 \
    using knowCore::MetaTypeDefinitionImplementation<_TYPE_>::expectedArrayError;                  \
    using knowCore::MetaTypeDefinitionImplementation<_TYPE_>::toJsonString;                        \
    using knowCore::MetaTypeDefinitionImplementation<_TYPE_>::fromJsonString;                      \
  protected:

#define KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(_TYPE_)                                     \
  }                                                                                                \
  ;

#define KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(_TYPE_)                                      \
  template<>                                                                                       \
  KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION_TEMPLATE(_TYPE_)

#define __KNOWCORE_URIFY(_URI_, I) knowCore::Details::makeUri(_URI_),

#define __KNOWCORE_URIFY_LIST(...) KNOWCORE_FOREACH(__KNOWCORE_URIFY, __VA_ARGS__)

#define __KNOWCORE_DEFINE_METATYPE_ALIASES__INTERNAL__(_FACTORY_NAME_, ...)                        \
  namespace                                                                                        \
  {                                                                                                \
    class _FACTORY_NAME_                                                                           \
    {                                                                                              \
    public:                                                                                        \
      _FACTORY_NAME_()                                                                             \
      {                                                                                            \
        QList<knowCore::Uri> uris = {__KNOWCORE_URIFY_LIST(__VA_ARGS__)};                          \
        clog_assert(uris.size() > 0);                                                              \
        knowCore::Uri to = uris.takeLast();                                                        \
        knowCore::registerMetaTypeAliases(uris, to);                                               \
      }                                                                                            \
      static _FACTORY_NAME_ s_instance;                                                            \
    };                                                                                             \
    _FACTORY_NAME_ _FACTORY_NAME_::s_instance;                                                     \
  }

/**
 * Define a set of uris to be aliases of an other uri.
 * \code
 * KNOWCORE_DEFINE_METATYPE_ALIASES(uri0, uri1, uri2, ..., urin, main_uri)
 * \endcode
 *
 * uri0, uri1, uri2, ..., urin are aliases of main_uri. Meaning they use the same underlying
 * data structure for storage in \ref Value.
 */
#define KNOWCORE_DEFINE_METATYPE_ALIASES(...)                                                      \
  __KNOWCORE_DEFINE_METATYPE_ALIASES__INTERNAL__(__KNOWCORE_UNIQUE_STATIC_NAME(MetaTypeAlias),     \
                                                 __VA_ARGS__)

/**

The following snipet can be used to define an implementation of a MetaTypeDefinition for
a custom type.

\code{.cpp}
KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(_TYPE_)
  cres_qresult<QByteArray> md5(const _TYPE_& _value) const override
  {
    QCryptographicHash hash(QCryptographicHash::Md5);
    hash.addData(...);
    return cres_success(hash.result());
  }
  cres_qresult<QJsonValue> toJsonValue(const _TYPE_& _value) const override
  {
    return cres_success(...);
  }
  cres_qresult<void> fromJsonValue(_TYPE_* _value, const QJsonValue& _json_value) const override
  {
    if(_json_value.is...())
    {
      *_value = _json_value.to...();
      return cres_success();
    } else {
      return expectedError(..., _json_value);
    }
  }
  cres_qresult<QCborValue> toCborValue(const _TYPE_& _value) const override
  {
    return cres_success(...);
  }
  cres_qresult<void> fromCborValue(_TYPE_* _value, const QCborValue& _cbor_value) const override
  {
    if(_cbor_value.is...())
    {
      *_value = _cbor_value.to...();
      return cres_success();
    } else {
      return expectedError(..., _cbor_value);
    }
  }
  cres_qresult<QString> printable(const _TYPE_& _value) const override
  {
    return cres_success(clog_qt::to_qstring(_value));
  }
  cres_qresult<QString> toRdfLiteral(const _TYPE_& _value) const override
  {
    return cres_success(...);
  }
  cres_qresult<void> fromRdfLiteral(_TYPE_* _value, const QString& _serialised) const override
  {
    ...
    return cres_success();
  }
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(_TYPE_)
@endcode

Alternatively, a templated version can be used:

@code
template<typename _T_>
requires (...)
KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION_TEMPLATE(_T_)
...
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(_T_)
@endcode

*/
