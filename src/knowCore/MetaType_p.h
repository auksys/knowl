#include "MetaType.h"

namespace knowCore
{
  struct MetaTypeRegistryPrivate
  {
    QHash<Uri, AbstractMetaTypeDefinition*> uri_2_definitions;
    QHash<int, AbstractMetaTypeDefinition*> id_2_definitions;
    QHash<Uri, QHash<Uri, AbstractTypeConversion*>> safe_converters;
    QHash<Uri, QHash<Uri, AbstractTypeConversion*>> force_converters;
    QHash<int, QHash<Uri, QHash<Uri, AbstractTypeComparator*>>> comparators;
    QHash<int, QHash<Uri, QHash<Uri, AbstractArithmeticOperator*>>> operators;
    QHash<Uri, QList<Uri>> aliases_to_from;
    QHash<Uri, Uri> aliases_from_to;
    QList<knowCore::Uri> numeric_types;
    QHash<Uri, AbstractToVariantMarshal*> to_variant_marshals;

    knowCore::Uri unaliased(const Uri& _uri) { return aliases_from_to.value(_uri, _uri); }
    const AbstractTypeConversion* converter(const Uri& _from, const Uri& _to,
                                            TypeCheckingMode _conversionMode)
    {
      Uri from_u = unaliased(_from);
      Uri to_u = unaliased(_to);

      if(_conversionMode == TypeCheckingMode::Force)
      {
        const AbstractTypeConversion* conv = force_converters.value(from_u).value(to_u, nullptr);
        if(conv)
          return conv;
      }
      // If no weak conversion, fallback to strong
      return safe_converters.value(from_u).value(to_u, nullptr);
    }
    const AbstractTypeComparator* comparator(ComparisonOperator _operator, const Uri& _from,
                                             const Uri& _to)
    {
      return comparators.value((int)_operator)
        .value(unaliased(_from))
        .value(unaliased(_to), nullptr);
    }
    const AbstractArithmeticOperator* arithmeticOperator(ArithmeticOperator _operator,
                                                         const Uri& _from, const Uri& _to)
    {
      return operators.value((int)_operator).value(unaliased(_from)).value(unaliased(_to), nullptr);
    }
    static MetaTypeRegistryPrivate* instance();
  };
} // namespace knowCore
