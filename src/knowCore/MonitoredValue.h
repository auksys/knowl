#include <functional>

#include <QMutex>

namespace knowCore
{
  template<typename _T_>
  class MonitoredValueController;
  /**
   * @ingroup knowCore
   *
   * Represent a value that can be monitored when changed.
   */
  template<typename _T_>
  class MonitoredValue
  {
    friend class MonitoredValueController<_T_>;
    MonitoredValue(const _T_& _init) : d(new Private{{}, _init, 0, {}}) {}
  public:
    MonitoredValue(const MonitoredValue& _rhs);
    MonitoredValue& operator=(const MonitoredValue& _rhs);
    ~MonitoredValue();
    /// @return the value
    _T_ value() const;
    /// Add a monitor, which is called when the value is changed by the controller.
    std::size_t addMonitor(const std::function<void(const _T_& _value)>& _function);
    void removeMonitor(std::size_t _idx);
  private:
    struct Private
    {
      QMutex m;
      _T_ value;
      std::size_t nextId;
      QMap<std::size_t, std::function<void(const _T_& _value)>> monitors;
    };
    QSharedPointer<Private> d;
  };

  template<typename _T_>
  MonitoredValue<_T_>::MonitoredValue(const MonitoredValue& _rhs) : d(_rhs.d)
  {
  }

  template<typename _T_>
  MonitoredValue<_T_>& MonitoredValue<_T_>::operator=(const MonitoredValue& _rhs)
  {
    d = _rhs.d;
    return *this;
  }

  template<typename _T_>
  MonitoredValue<_T_>::~MonitoredValue()
  {
  }

  template<typename _T_>
  _T_ MonitoredValue<_T_>::value() const
  {
    return d->value;
  }

  template<typename _T_>
  std::size_t
    MonitoredValue<_T_>::addMonitor(const std::function<void(const _T_& _value)>& _function)
  {
    QMutexLocker l(&d->m);
    std::size_t id = d->nextId++;
    d->monitors[id] = _function;
    return id;
  }

  template<typename _T_>
  void MonitoredValue<_T_>::removeMonitor(std::size_t _idx)
  {
    QMutexLocker l(&d->m);
    d->monitors.remove(_idx);
  }

  /**
   * @ingroup knowCore
   *
   * Controller for the monitored value. The intention is to keep the controller private and to
   * expose the result of `value()`.
   */
  template<typename _T_>
  class MonitoredValueController
  {
  public:
    MonitoredValueController(const _T_& _initial_value);
    ~MonitoredValueController();
    /// Set the value and trigger the monitors
    void setValue(const _T_& _value);
    MonitoredValue<_T_> value() const;
  private:
    MonitoredValue<_T_> m_value;
  };

  template<typename _T_>
  MonitoredValueController<_T_>::MonitoredValueController(const _T_& _initial_value)
      : m_value(_initial_value)
  {
  }

  template<typename _T_>
  MonitoredValueController<_T_>::~MonitoredValueController()
  {
  }

  template<typename _T_>
  void MonitoredValueController<_T_>::setValue(const _T_& _value)
  {
    QMutexLocker l(&m_value.d->m);
    m_value.d->value = _value;
    for(const std::function<void(const _T_&)>& monitor : m_value.d->monitors)
    {
      monitor(_value);
    }
  }

  template<typename _T_>
  MonitoredValue<_T_> MonitoredValueController<_T_>::value() const
  {
    return m_value;
  }
} // namespace knowCore