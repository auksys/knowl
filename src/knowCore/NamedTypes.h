#pragma once

#include <cext_nt>

namespace knowCore
{
  using Symbol = cext_named_type<QString, struct SymbolTag>;
}

inline knowCore::Symbol operator"" _kCSymbol(const char* _text, std::size_t)
{
  return knowCore::Symbol(QString::fromUtf8(_text));
}
