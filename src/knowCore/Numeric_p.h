#include <tuple>

#include "Global.h"

#include <clog_qt>
#include <cres_qt>

namespace knowCore
{
  inline std::tuple<bool, quint64> mul_overflow(quint64 _a, quint64 _b)
  {
#if __KNOWCORE_HAS_BUILTIN(__builtin_mul_overflow)
    quint64 result;
    if(__builtin_mul_overflow(_a, _b, &result))
    {
      return {true, NAN};
    }
    else
    {
      return {false, result};
    }
#else
    quint64 result = _a * _b;
    if(_a != 0 and _b != result / _a)
    {
      return {true, NAN};
    }
    return {false, result};
#endif
  }
  std::tuple<bool, quint64> add_overflow(quint64 _a, quint64 _b)
  {
#if __KNOWCORE_HAS_BUILTIN(__builtin_add_overflow)
    quint64 result;
    if(__builtin_add_overflow(_a, _b, &result))
    {
      return {true, NAN};
    }
    else
    {
      return {false, result};
    }
#else
    quint64 result = _a + _b;
    if(result < _a)
    {
      return {true, NAN};
    }
    return {false, result};
#endif
  }
} // namespace knowCore
