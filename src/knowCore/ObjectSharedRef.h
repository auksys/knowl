#include "SharedRef.h"

#include <QObject>

namespace knowCore
{
  namespace details
  {
    struct ObjectSharedRefCheck
    {
      static bool canDelete(QObject* _object) { return _object and _object->parent() == nullptr; }
    };
  } // namespace details
  /**
   * \ref ObjectSharedRef is a variant of SharedRef for QObject, so that the QObject is only deleted
   * if it does not have a parent.
   */
  template<typename _T_>
  using ObjectSharedRef = SharedRef<_T_, details::ObjectSharedRefCheck>;
} // namespace knowCore
