#include "QuantityValue.h"

#include "Uris/askcore_datatype.h"

#include <knowCore/Uris/askcore_datatype.h>

#include <knowCore/MetaTypeImplementation.h>

using namespace knowCore;

template<typename _T_>
KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION_TEMPLATE(QuantityValue<_T_>)
cres_qresult<QByteArray> md5(const QuantityValue<_T_>& _value) const override
{
  return _value.md5();
}
cres_qresult<QJsonValue> toJsonValue(const QuantityValue<_T_>& _value,
                                     const SerialisationContexts& _contexts) const override
{
  return _value.toJsonValue(_contexts);
}
cres_qresult<void> fromJsonValue(QuantityValue<_T_>* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts& _contexts) const override
{
  cres_try(*_value, QuantityValue<_T_>::fromJsonValue(_json_value, _contexts));
  return cres_success();
}
cres_qresult<QCborValue> toCborValue(const QuantityValue<_T_>& _value,
                                     const SerialisationContexts& _contexts) const override
{
  return _value.toCborValue(_contexts);
}
cres_qresult<void> fromCborValue(QuantityValue<_T_>* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts& _contexts) const override
{
  cres_try(*_value, QuantityValue<_T_>::fromCborValue(_cbor_value, _contexts));
  return cres_success();
}
cres_qresult<QString> printable(const QuantityValue<_T_>& _value) const override
{
  return _value.printable();
}
cres_qresult<QString> toRdfLiteral(const QuantityValue<_T_>& _value,
                                   const SerialisationContexts& _contexts) const override
{
  return _value.toRdfLiteral(_contexts);
}
cres_qresult<void> fromRdfLiteral(QuantityValue<_T_>* _value, const QString& _serialised,
                                  const DeserialisationContexts& _contexts) const override
{
  cres_try(*_value, QuantityValue<_T_>::fromRdfLiteral(_serialised, _contexts));
  return cres_success();
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(QuantityValue<_T_>)

template<typename _T_>
struct Converter<QuantityValue<_T_>, QString, TypeCheckingMode::Safe>
{
  static inline cres_qresult<void> convert(const QuantityValue<_T_>* _from, QString* _to)
  {
    cres_try(*_to, _from->toRdfLiteral());
    return cres_success();
  }
};

template<typename _T_>
struct Converter<QString, QuantityValue<_T_>, TypeCheckingMode::Safe>
{
  static inline cres_qresult<void> convert(const QString* _from, QuantityValue<_T_>* _to)
  {
    cres_try(*_to, QuantityValue<_T_>::fromRdfLiteral(*_from));
    return cres_success();
  }
};

template<typename _T_>
struct Converter<QuantityValue<_T_>, _T_, TypeCheckingMode::Safe>
{
  static inline cres_qresult<void> convert(const QuantityValue<_T_>* _from, _T_* _to)
  {
    if(_from->unit() == Unit::empty())
    {
      *_to = _from->value();
      return cres_success();
    }
    else
    {
      return cres_failure(
        "Conversion from quantiy value with non-empty unit {} to base value type {} is not safe",
        _from->unit(), prettyTypename<_T_>());
    }
  }
};

template<typename _T_>
struct Converter<QuantityValue<_T_>, _T_, TypeCheckingMode::Force>
{
  static inline cres_qresult<void> convert(const QuantityValue<_T_>* _from, _T_* _to)
  {
    *_to = _from->value();
    return cres_success();
  }
};

template<typename _T_>
struct Converter<_T_, QuantityValue<_T_>, TypeCheckingMode::Safe>
{
  static inline cres_qresult<void> convert(const _T_* _from, QuantityValue<_T_>* _to)
  {
    *_to = QuantityValue<_T_>(*_from, Unit::empty());
    return cres_success();
  }
};

KNOWCORE_DEFINE_METATYPE(QuantityNumber, Uris::askcore_datatype::quantityDecimal,
                         MetaTypeTraits::ToString | MetaTypeTraits::FromString
                           | MetaTypeTraits::NumericType)
KNOWCORE_REGISTER_COMPARATORS((Equal, Inferior), QuantityValue<BigNumber>)

template<ArithmeticOperator _operator_, typename _T_>
struct ArithmeticOperation<_operator_, QuantityValue<_T_>, QuantityValue<_T_>>
{
  using op_trait = details::op_trait<_operator_, QuantityValue<_T_>, QuantityValue<_T_>>;
  static cres_qresult<knowCore::Value> compute(const QuantityValue<_T_>& _left,
                                               const QuantityValue<_T_>& _right)
  {
    cres_try(QuantityValue<BigNumber> result, op_trait::compute(_left, _right));
    return cres_success(Value::fromValue(result));
  }
};

template<ArithmeticOperator _operator_, typename _T_>
struct ArithmeticOperation<_operator_, QuantityValue<_T_>, _T_>
{
  using op_trait = details::op_trait<_operator_, QuantityValue<_T_>, _T_>;
  static cres_qresult<knowCore::Value> compute(const QuantityValue<_T_>& _left, const _T_& _right)
  {
    return cres_success(Value::fromValue(op_trait::compute(_left, _right)));
  }
};
template<ArithmeticOperator _operator_, typename _T_>
struct ArithmeticOperation<_operator_, _T_, QuantityValue<_T_>>
{
  using op_trait = details::op_trait<_operator_, _T_, QuantityValue<_T_>>;
  static cres_qresult<knowCore::Value> compute(const _T_& _left, const QuantityValue<_T_>& _right)
  {
    return cres_success(Value::fromValue(op_trait::compute(_left, _right)));
  }
};

KNOWCORE_REGISTER_ARITHMETIC_OPERATORS((Addition, Substraction, Multiplication, Division),
                                       QuantityValue<BigNumber>, QuantityValue<BigNumber>)
KNOWCORE_REGISTER_ARITHMETIC_OPERATORS((Multiplication, Division), QuantityValue<BigNumber>,
                                       BigNumber)
KNOWCORE_REGISTER_ARITHMETIC_OPERATORS((Multiplication), BigNumber, QuantityValue<BigNumber>)
