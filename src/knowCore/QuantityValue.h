#pragma once

#include <QCborMap>
#include <QCryptographicHash>
#include <QJsonObject>

#include "BigNumber.h"
#include "NamedTypes.h"
#include "Unit.h"
#include "Value.h"

namespace knowCore
{
  /**
   * @ingroup knowCore
   *
   * This class can be use to combine a number with a \ref Unit. Look at \ref UnitNumber for an
   * alias combining \ref BigNumber with \ref Unit.
   */
  template<typename _T_>
  class QuantityValue
  {
    template<typename _TOther_>
    struct Constructor;
  public:
    QuantityValue() {}
    QuantityValue(const _T_& _value, const Unit& _unit);
    ~QuantityValue() {}
    template<typename _TOther_, typename _TUri_>
      requires(not std::is_base_of_v<Value, _TOther_> and knowCore::Uris::IsUriDefinitionV<_TUri_>)
    static cres_qresult<QuantityValue> create(const _TOther_& _value, const _TUri_& _unit_uri)
    {
      return create(_value, knowCore::Uri(_unit_uri));
    }
    template<typename _TOther_>
      requires(not std::is_base_of_v<Value, _TOther_>)
    static cres_qresult<QuantityValue> create(const _TOther_& _value,
                                              const knowCore::Uri& _unit_uri);
    static cres_qresult<QuantityValue> create(const Value& _value, const knowCore::Uri& _unit_uri);
    template<typename _TOther_>
      requires(not std::is_base_of_v<Value, _TOther_>)
    static cres_qresult<QuantityValue> create(const _TOther_& _value, const QString& _unit_uri)
      = delete;
    static cres_qresult<QuantityValue> create(const Value& _value, const QString& _unit_uri)
      = delete;
    template<typename _TOther_>
      requires(not std::is_base_of_v<Value, _TOther_>)
    static cres_qresult<QuantityValue> create(const _TOther_& _value, const Symbol& _unit_symbol);
    static cres_qresult<QuantityValue> create(const Value& _value, const Symbol& _unit_symbol);

    /**
     * @return the \ref Unit associated with this number
     */
    Unit unit() const { return m_unit; }
    /**
     * @return the value
     */
    _T_ value() const { return m_value; }

    /**
     * Attempt an addition, return an error if units are not compatible
     */
    cres_qresult<QuantityValue> operator+(const QuantityValue& _rhs) const;
    /**
     * Attempt an substractionm, return an error if units are not compatible
     */
    cres_qresult<QuantityValue> operator-(const QuantityValue& _rhs) const;
    /**
     * Attempt a division, return an error if units are not compatible
     */
    cres_qresult<QuantityValue> operator/(const QuantityValue& _rhs) const;
    /**
     * Attempt a multiplication, return an error if units are not compatible
     */
    cres_qresult<QuantityValue> operator*(const QuantityValue& _rhs) const;
    /**
     * Division.
     */
    QuantityValue operator/(const _T_& _rhs) const;
    /**
     * Multiplication.
     */
    QuantityValue operator*(const _T_& _rhs) const;

    /**
     * Compare two values. Note, that comparing two numbers with different unit is acceptable and
     * will return false.
     */
    bool operator==(const QuantityValue& _rhs) const;
    /**
     * Compare two values for inferirority. It will return an error if the units are not compatible.
     */
    cres_qresult<bool> operator<(const QuantityValue& _rhs) const;

    /**
     * @return a md5 hash of the value
     */
    cres_qresult<QByteArray> md5() const;
    /**
     * Convert to json
     */
    cres_qresult<QJsonValue> toJsonValue(const SerialisationContexts& _contexts
                                         = defaultSerialisationContext()) const;
    /**
     * Convert from json
     */
    static cres_qresult<QuantityValue> fromJsonValue(const QJsonValue& _value,
                                                     const DeserialisationContexts& _context
                                                     = defaultDeserialisationContext());
    /**
     * Convert to cbor
     */
    cres_qresult<QCborValue> toCborValue(const SerialisationContexts& _contexts
                                         = defaultSerialisationContext()) const;
    /**
     * Convert from cbor
     */
    static cres_qresult<QuantityValue> fromCborValue(const QCborValue& _value,
                                                     const DeserialisationContexts& _context
                                                     = defaultDeserialisationContext());
    /**
     * Convert to a rdf literal (something like "12.0 m/s")
     */
    cres_qresult<QString> toRdfLiteral(const SerialisationContexts& _contexts
                                       = defaultSerialisationContext()) const;
    /**
     * Convert from a rdf literal (something like "12.0 m/s")
     */
    static cres_qresult<QuantityValue> fromRdfLiteral(const QString& _value,
                                                      const DeserialisationContexts& _context
                                                      = defaultDeserialisationContext());
    /**
     * Return a printable value
     */
    cres_qresult<QString> printable() const;
  private:
    cres_qresult<_T_> cast_rhs(const QuantityValue& _rhs) const;
    _T_ m_value;
    Unit m_unit;
  };
  /**
   * Multiplication.
   */
  template<typename _T_>
  QuantityValue<_T_> operator*(const _T_& _lhs, const QuantityValue<_T_>& _rhs);

  // BEGIN Specialized constructors
  template<>
  template<typename _TOther_>
  struct QuantityValue<BigNumber>::Constructor<_TOther_>
  {
    static cres_qresult<BigNumber> construct(const _TOther_& _v)
      requires(not std::is_same_v<_TOther_, QString>)
    {
      return cres_success(BigNumber(_v));
    }
    static cres_qresult<BigNumber> construct(const QString& _v)
      requires(std::is_same_v<_TOther_, QString>)
    {
      return BigNumber::fromString(_v);
    }
  };
  // END Specialized constructors

  // BEGIN implementation
  template<typename _T_>
  inline QuantityValue<_T_>::QuantityValue(const _T_& _value, const Unit& _unit)
      : m_value(_value), m_unit(_unit)
  {
  }

  template<typename _T_>
  template<typename _TOther_>
    requires(not std::is_base_of_v<Value, _TOther_>)
  inline cres_qresult<QuantityValue<_T_>> QuantityValue<_T_>::create(const _TOther_& _value,
                                                                     const knowCore::Uri& _unit_uri)
  {
    cres_try(Unit unit, Unit::byUri(_unit_uri));
    return cres_success(QuantityValue(_value, unit));
  }

  template<typename _T_>
  inline cres_qresult<QuantityValue<_T_>> QuantityValue<_T_>::create(const knowCore::Value& _value,
                                                                     const knowCore::Uri& _unit_uri)
  {
    cres_try(_T_ value, _value.value<_T_>());
    return create(value, _unit_uri);
  }

  template<typename _T_>
  template<typename _TOther_>
    requires(not std::is_base_of_v<Value, _TOther_>)
  inline cres_qresult<QuantityValue<_T_>> QuantityValue<_T_>::create(const _TOther_& _value,
                                                                     const Symbol& _unit_symbol)
  {
    cres_try(Unit unit, Unit::bySymbol(_unit_symbol));
    cres_try(_T_ value, Constructor<_TOther_>::construct(_value));
    return cres_success(QuantityValue(value, unit));
  }

  template<typename _T_>
  inline cres_qresult<QuantityValue<_T_>> QuantityValue<_T_>::create(const Value& _value,
                                                                     const Symbol& _unit_symbol)
  {
    cres_try(_T_ value, _value.value<_T_>());
    return create(value, _unit_symbol);
  }

  template<typename _T_>
  inline cres_qresult<_T_> QuantityValue<_T_>::cast_rhs(const QuantityValue& _rhs) const
  {
    if(_rhs.unit().base() == unit().base())
    {
      return cres_success(_rhs.value() * _rhs.unit().scale() / unit().scale());
    }
    else
    {
      return cres_failure("Cannot operate on incompatible units: {} is not compatible with {}",
                          unit(), _rhs.unit());
    }
  }

  template<typename _T_>
  inline cres_qresult<QuantityValue<_T_>>
    QuantityValue<_T_>::operator+(const QuantityValue& _rhs) const
  {
    cres_try(_T_ rhsv, cast_rhs(_rhs));
    return cres_success<QuantityValue<_T_>>({value() + rhsv, unit()});
  }
  template<typename _T_>
  inline cres_qresult<QuantityValue<_T_>>
    QuantityValue<_T_>::operator-(const QuantityValue& _rhs) const
  {
    cres_try(_T_ rhsv, cast_rhs(_rhs));
    return cres_success<QuantityValue<_T_>>({value() - rhsv, unit()});
  }
  template<typename _T_>
  inline cres_qresult<QuantityValue<_T_>>
    QuantityValue<_T_>::operator/(const QuantityValue& _rhs) const
  {
    if(unit().base() == _rhs.unit().base())
    {
      cres_try(_T_ rhsv, cast_rhs(_rhs));
      return cres_success<QuantityValue<_T_>>({value() / rhsv, Unit::empty()});
    }
    else
    {
      cres_try(Unit runit, unit() / _rhs.unit());
      return cres_success<QuantityValue<_T_>>({value() / _rhs.value(), runit});
    }
  }
  template<typename _T_>
  inline cres_qresult<QuantityValue<_T_>>
    QuantityValue<_T_>::operator*(const QuantityValue& _rhs) const
  {
    if(unit().base() == _rhs.unit().base())
    {
      cres_try(_T_ rhsv, cast_rhs(_rhs));
      cres_try(Unit runit, unit() * unit());
      return cres_success<QuantityValue<_T_>>({value() * rhsv, runit});
    }
    else
    {
      cres_try(Unit runit, unit() * _rhs.unit());
      return cres_success<QuantityValue<_T_>>({value() * _rhs.value(), runit});
    }
  }

  template<typename _T_>
  QuantityValue<_T_> QuantityValue<_T_>::operator/(const _T_& _rhs) const
  {
    return QuantityValue<_T_>{value() / _rhs, unit()};
  }

  template<typename _T_>
  QuantityValue<_T_> QuantityValue<_T_>::operator*(const _T_& _rhs) const
  {
    return QuantityValue<_T_>{value() * _rhs, unit()};
  }

  template<typename _T_>
  QuantityValue<_T_> operator*(const _T_& _lhs, const QuantityValue<_T_>& _rhs)
  {
    return _rhs * _lhs;
  }

  template<typename _T_>
  bool QuantityValue<_T_>::operator==(const QuantityValue& _rhs) const
  {
    return value() == _rhs.value() and unit() == _rhs.unit();
  }

  template<typename _T_>
  cres_qresult<bool> QuantityValue<_T_>::operator<(const QuantityValue& _rhs) const
  {
    cres_try(_T_ rhsv, cast_rhs(_rhs));
    return cres_success(value() < rhsv);
  }

  template<typename _T_>
  inline cres_qresult<QByteArray> QuantityValue<_T_>::md5() const
  {
    QCryptographicHash hash(QCryptographicHash::Md5);
    cres_try(QByteArray value_md5, MetaTypeInformation<_T_>::md5(m_value));
    hash.addData(value_md5);
    hash.addData(unit().symbol().toUtf8());
    return cres_success(hash.result());
  }

#define __KNOWCORE_VALUE_KEY QString("value")
#define __KNOWCORE_UNIT_KEY QString("unit")

  template<typename _T_>
  inline cres_qresult<QJsonValue>
    QuantityValue<_T_>::toJsonValue(const SerialisationContexts& _contexts) const
  {
    QJsonObject object;
    cres_try(object[__KNOWCORE_VALUE_KEY],
             MetaTypeInformation<_T_>::toJsonValue(value(), _contexts));
    object[__KNOWCORE_UNIT_KEY] = unit().symbol();
    return cres_success(object);
  }

  template<typename _T_>
  inline cres_qresult<QuantityValue<_T_>>
    QuantityValue<_T_>::fromJsonValue(const QJsonValue& _value,
                                      const DeserialisationContexts& _context)
  {
    if(_value.isObject())
    {
      QJsonObject object = _value.toObject();
      cres_try(_T_ value, MetaTypeInformation<_T_>::fromJsonValue(
                            object.value(__KNOWCORE_VALUE_KEY), _context));
      cres_try(Unit unit, Unit::bySymbol(object.value(__KNOWCORE_UNIT_KEY).toString()));
      return cres_success<QuantityValue<_T_>>({value, unit});
    }
    else
    {
      return cres_failure("Expected object got {}", _value);
    }
  }

  template<typename _T_>
  inline cres_qresult<QCborValue>
    QuantityValue<_T_>::toCborValue(const SerialisationContexts& _contexts) const
  {
    QCborMap object;
    cres_try(object[__KNOWCORE_VALUE_KEY],
             MetaTypeInformation<_T_>::toCborValue(value(), _contexts));
    object[__KNOWCORE_UNIT_KEY] = unit().symbol();
    return cres_success(object);
  }

  template<typename _T_>
  inline cres_qresult<QuantityValue<_T_>>
    QuantityValue<_T_>::fromCborValue(const QCborValue& _value,
                                      const DeserialisationContexts& _context)
  {
    if(_value.isMap())
    {
      QCborMap object = _value.toMap();
      cres_try(_T_ value, MetaTypeInformation<_T_>::fromCborValue(
                            object.value(__KNOWCORE_VALUE_KEY), _context));
      cres_try(Unit unit, Unit::bySymbol(object.value(__KNOWCORE_UNIT_KEY).toString()));
      return cres_success<QuantityValue<_T_>>({value, unit});
    }
    else
    {
      return cres_failure("Expected object got {}", _value);
    }
  }

  template<typename _T_>
  inline cres_qresult<QString>
    QuantityValue<_T_>::toRdfLiteral(const SerialisationContexts& _contexts) const
  {
    cres_try(QString str, MetaTypeInformation<_T_>::toRdfLiteral(value(), _contexts));
    return cres_success(str + " " + m_unit.symbol());
  }

  template<typename _T_>
  inline cres_qresult<QuantityValue<_T_>>
    QuantityValue<_T_>::fromRdfLiteral(const QString& _value,
                                       const DeserialisationContexts& _context)
  {
    QStringList splited = _value.split(" ", KNOWCORE_QT_SKIP_EMPTY_PART);
    if(splited.size() == 0)
    {
      return cres_failure("Empty literal");
    }
    Unit u = Unit::empty();
    if(splited.size() == 2)
    {
      cres_try(u, Unit::bySymbol(splited[1]));
    }
    cres_try(_T_ value, MetaTypeInformation<_T_>::fromRdfLiteral(splited[0], _context));
    return cres_success<QuantityValue<_T_>>({value, u});
  }

  template<typename _T_>
  inline cres_qresult<QString> QuantityValue<_T_>::printable() const
  {
    return toRdfLiteral();
  }

#undef __KNOWCORE_UNIT_KEY
#undef __KNOWCORE_VALUE_KEY

  // END implementation
} // namespace knowCore

#include "MetaType.h"
KNOWCORE_DECLARE_FULL_METATYPE(knowCore, QuantityNumber);

template<typename _T_>
struct std::formatter<knowCore::QuantityValue<_T_>> : public clog_format::base_formatter
{
  template<typename FormatContext>
  auto format(knowCore::QuantityValue<_T_> const& p,
              FormatContext& ctx) const -> decltype(ctx.out())
  {
    return format_to(ctx.out(), "({} {})", p.value(), p.unit());
  }
};