#include "Range.h"

#include "Uris/askcore_datatype.h"

#include <knowCore/Uris/askcore_datatype.h>

#include <knowCore/MetaTypeImplementation.h>

using namespace knowCore;

template<typename _T_>
KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION_TEMPLATE(Range<_T_>)
cres_qresult<QByteArray> md5(const Range<_T_>& _value) const override
{
  return _value.md5();
}
cres_qresult<QJsonValue> toJsonValue(const Range<_T_>& _value,
                                     const SerialisationContexts& _contexts) const override
{
  return _value.toJsonValue(_contexts);
}
cres_qresult<void> fromJsonValue(Range<_T_>* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts& _contexts) const override
{
  cres_try(*_value, Range<_T_>::fromJsonValue(_json_value, _contexts));
  return cres_success();
}
cres_qresult<QCborValue> toCborValue(const Range<_T_>& _value,
                                     const SerialisationContexts& _contexts) const override
{
  return _value.toCborValue(_contexts);
}
cres_qresult<void> fromCborValue(Range<_T_>* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts& _contexts) const override
{
  cres_try(*_value, Range<_T_>::fromCborValue(_cbor_value, _contexts));
  return cres_success();
}
cres_qresult<QString> printable(const Range<_T_>& _value) const override
{
  return _value.printable();
}
cres_qresult<QString> toRdfLiteral(const Range<_T_>& _value,
                                   const SerialisationContexts& _contexts) const override
{
  return _value.toRdfLiteral(_contexts);
}
cres_qresult<void> fromRdfLiteral(Range<_T_>* _value, const QString& _serialised,
                                  const DeserialisationContexts& _contexts) const override
{
  cres_try(*_value, Range<_T_>::fromRdfLiteral(_serialised, _contexts));
  return cres_success();
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(Range<_T_>)

template<typename _T_>
struct Converter<Range<_T_>, QString, TypeCheckingMode::Safe>
{
  static inline cres_qresult<void> convert(const Range<_T_>* _from, QString* _to)
  {
    cres_try(*_to, _from->toRdfLiteral());
    return cres_success();
  }
};

template<typename _T_>
struct Converter<QString, Range<_T_>, TypeCheckingMode::Safe>
{
  static inline cres_qresult<void> convert(const QString* _from, Range<_T_>* _to)
  {
    cres_try(*_to, Range<_T_>::fromRdfLiteral(*_from));
    return cres_success();
  }
};

KNOWCORE_DEFINE_METATYPE(NumberRange, Uris::askcore_datatype::decimalRange,
                         MetaTypeTraits::ToString | MetaTypeTraits::FromString)
KNOWCORE_DEFINE_METATYPE(QuantityNumberRange, Uris::askcore_datatype::quantityDecimalRange,
                         MetaTypeTraits::ToString | MetaTypeTraits::FromString)
KNOWCORE_DEFINE_METATYPE(ValueRange, Uris::askcore_datatype::range,
                         MetaTypeTraits::ToString | MetaTypeTraits::FromString)

KNOWCORE_REGISTER_COMPARATORS((Equal), Range<BigNumber>)
KNOWCORE_REGISTER_COMPARATORS((Equal), Range<QuantityNumber>)
KNOWCORE_REGISTER_COMPARATORS((Equal), Range<Value>)
