#pragma once

#include <Cyqlops/Crypto/Hash.h>

#include "QuantityValue.h"

namespace knowCore
{
  enum class RangeBoundType
  {
    Exclusive,
    Inclusive,
    Unset
  };
  namespace details
  {
    template<typename _T_>
    struct RangeComparator;
    template<>
    struct RangeComparator<Value>
    {
      using ReturnType = cres_qresult<bool>;
      static ReturnType isEqual(const Value& _lhs, const Value& _rhs)
      {
        return _lhs.compare(_rhs, knowCore::ComparisonOperator::Equal);
      }
      static ReturnType isStrictlyInferior(const Value& _lhs, const Value& _rhs)
      {
        return _lhs.compare(_rhs, knowCore::ComparisonOperator::Inferior);
      }
    };
    template<typename _T_>
    struct RangeComparator<QuantityValue<_T_>>
    {
      using ReturnType = cres_qresult<bool>;
      static ReturnType isEqual(const QuantityValue<_T_>& _lhs, const QuantityValue<_T_>& _rhs)
      {
        return _lhs == _rhs;
      }
      static ReturnType isStrictlyInferior(const QuantityValue<_T_>& _lhs,
                                           const QuantityValue<_T_>& _rhs)
      {
        return _lhs < _rhs;
      }
    };
    template<>
    struct RangeComparator<BigNumber>
    {
      using ReturnType = bool;
      static ReturnType isEqual(const BigNumber& _lhs, const BigNumber& _rhs)
      {
        return _lhs == _rhs;
      }
      static ReturnType isStrictlyInferior(const BigNumber& _lhs, const BigNumber& _rhs)
      {
        return _lhs < _rhs;
      }
    };
  } // namespace details
  /**
   * @ingroup knowCore
   * Allow to define a range
   */
  template<typename _T_>
  class Range
  {
  public:
    Range() : m_lowerType(RangeBoundType::Unset), m_upperType(RangeBoundType::Unset) {}
    Range(RangeBoundType _lower_type, const _T_& _lower, const _T_& _upper,
          RangeBoundType _upper_type)
        : m_lower(_lower), m_lowerType(_lower_type), m_upper(_upper), m_upperType(_upper_type)
    {
    }
    ~Range() {}

    bool operator==(const Range& _rhs) const;
    _T_ lower() const { return m_lower; }
    RangeBoundType lowerBoundType() const { return m_lowerType; }
    _T_ upper() const { return m_upper; }
    RangeBoundType upperBoundType() const { return m_upperType; }

    /**
     * @return true if the value @p _v is contained in the bound.
     */
    typename details::RangeComparator<_T_>::ReturnType contains(const _T_& _v) const
      requires(std::is_same_v<typename details::RangeComparator<_T_>::ReturnType, bool>);
    /**
     * @return true if the value @p _v is contained in the bound.
     */
    typename details::RangeComparator<_T_>::ReturnType contains(const _T_& _v) const
      requires(
        std::is_same_v<typename details::RangeComparator<_T_>::ReturnType, cres_qresult<bool>>);

    /**
     * @return a md5 hash of the value
     */
    cres_qresult<QByteArray> md5() const;
    /**
     * Convert to json
     */
    cres_qresult<QJsonValue> toJsonValue(const SerialisationContexts& _contexts
                                         = defaultSerialisationContext()) const;
    /**
     * Convert from json
     */
    static cres_qresult<Range> fromJsonValue(const QJsonValue& _value,
                                             const DeserialisationContexts& _context
                                             = defaultDeserialisationContext());
    /**
     * Convert to cbor
     */
    cres_qresult<QCborValue> toCborValue(const SerialisationContexts& _contexts
                                         = defaultSerialisationContext()) const;
    /**
     * Convert from cbor
     */
    static cres_qresult<Range> fromCborValue(const QCborValue& _value,
                                             const DeserialisationContexts& _context
                                             = defaultDeserialisationContext());
    /**
     * Convert to a rdf literal (something like "12.0 m/s")
     */
    cres_qresult<QString> toRdfLiteral(const SerialisationContexts& _contexts
                                       = defaultSerialisationContext()) const;
    /**
     * Convert from a rdf literal (something like "12.0 m/s")
     */
    static cres_qresult<Range> fromRdfLiteral(const QString& _value,
                                              const DeserialisationContexts& _context
                                              = defaultDeserialisationContext());
    /**
     * Return a printable value
     */
    cres_qresult<QString> printable() const;
  private:
    _T_ m_lower;
    RangeBoundType m_lowerType;
    _T_ m_upper;
    RangeBoundType m_upperType;
  };

  template<typename _T_>
  bool Range<_T_>::operator==(const Range& _rhs) const
  {
    return m_lowerType == _rhs.m_lowerType and m_upperType == _rhs.m_upperType
           and m_lower == _rhs.m_lower and m_upper == _rhs.m_upper;
  }

#define __KNOWCORE_DATATYPE_KEY QString("datatype")
#define __KNOWCORE_VALUE_KEY QString("value")

  namespace details
  {
    template<typename _T_>
    struct RangeRDFLiteral : public MetaTypeInformation<_T_>
    {
    };
    template<>
    struct RangeRDFLiteral<Value>
    { // TODO this should be generalized
      static auto md5(const Value& _value) { return _value.md5(); }
      static cres_qresult<QJsonValue> toJsonValue(const Value& _value,
                                                  const SerialisationContexts& _contexts)
      {
        QJsonObject obj;
        obj[__KNOWCORE_DATATYPE_KEY] = QString(_value.datatype());
        cres_try(QJsonValue value, _value.toJsonValue(_contexts));
        obj[__KNOWCORE_VALUE_KEY] = value;
        return cres_success(obj);
      }
      static cres_qresult<Value> fromJsonValue(const QJsonValue& _value,
                                               const DeserialisationContexts& _contexts)
      {
        QJsonObject obj = _value.toObject();
        cres_try(Value value, Value::fromJsonValue(obj[__KNOWCORE_DATATYPE_KEY].toString(),
                                                   obj[__KNOWCORE_VALUE_KEY], _contexts));
        return cres_success(value);
      }
      static cres_qresult<QCborValue> toCborValue(const Value& _value,
                                                  const SerialisationContexts& _contexts)
      {
        QCborMap obj;
        obj[__KNOWCORE_DATATYPE_KEY] = QString(_value.datatype());
        cres_try(QCborValue value, _value.toCborValue(_contexts));
        obj[__KNOWCORE_VALUE_KEY] = value;
        return cres_success(obj);
      }
      static cres_qresult<Value> fromCborValue(const QCborValue& _value,
                                               const DeserialisationContexts& _contexts)
      {
        QCborMap obj = _value.toMap();
        cres_try(Value value, Value::fromCborValue(obj[__KNOWCORE_DATATYPE_KEY].toString(),
                                                   obj[__KNOWCORE_VALUE_KEY], _contexts));
        return cres_success(value);
      }
      static cres_qresult<QString> toRdfLiteral(const Value& _value,
                                                const SerialisationContexts& _contexts)
      {
        cres_try(QString lit, _value.toRdfLiteral(_contexts));
        return cres_success(clog_qt::qformat("<{}>({})", _value.datatype(), lit));
      }
      static cres_qresult<Value> fromRdfLiteral(const QString& _value,
                                                const DeserialisationContexts& _contexts)
      {
        QRegularExpressionMatch match = QRegularExpression("<(.*)>(.*)").match(_value);
        if(match.hasMatch())
        {
          return Value::fromRdfLiteral(match.captured(1), match.captured(2), _contexts);
        }
        else
        {
          return cres_failure("Invalid range value literal.");
        }
      }
    };
  } // namespace details

  template<typename _T_>
  inline cres_qresult<QByteArray> Range<_T_>::md5() const
  {
    QCryptographicHash hash(QCryptographicHash::Md5);
    cres_try(QByteArray value_lower, details::RangeRDFLiteral<_T_>::md5(lower()));
    cres_try(QByteArray value_upper, details::RangeRDFLiteral<_T_>::md5(upper()));
    hash.addData(value_lower);
    Cyqlops::Crypto::Hash::compute(hash, lowerBoundType());
    hash.addData(value_upper);
    Cyqlops::Crypto::Hash::compute(hash, upperBoundType());
    // hash.addData(int(upperBoundType()));
    return cres_success(hash.result());
  }

  template<typename _T_>
  typename details::RangeComparator<_T_>::ReturnType Range<_T_>::contains(const _T_& _v) const
    requires(std::is_same_v<typename details::RangeComparator<_T_>::ReturnType, cres_qresult<bool>>)
  {
    switch(lowerBoundType())
    {
    case RangeBoundType::Inclusive:
    {
      cres_try(bool v, details::RangeComparator<_T_>::isEqual(_v, lower()));
      if(v)
      {
        break;
      }
      Q_FALLTHROUGH();
    }
    case RangeBoundType::Exclusive:
    {
      cres_try(bool v, details::RangeComparator<_T_>::isStrictlyInferior(_v, lower()));
      if(v)
      {
        return cres_success(false);
      }
    }
    case RangeBoundType::Unset:
      break;
    }
    switch(upperBoundType())
    {
    case RangeBoundType::Inclusive:
    {
      cres_try(bool v, details::RangeComparator<_T_>::isEqual(_v, upper()));
      if(v)
      {
        break;
      }
      Q_FALLTHROUGH();
    }
    case RangeBoundType::Exclusive:
    {
      cres_try(bool v, details::RangeComparator<_T_>::isStrictlyInferior(upper(), _v));
      if(v)
      {
        return cres_success(false);
      }
    }
    case RangeBoundType::Unset:
      break;
    }
    return cres_success(true);
  }

  template<typename _T_>
  typename details::RangeComparator<_T_>::ReturnType Range<_T_>::contains(const _T_& _v) const
    requires(std::is_same_v<typename details::RangeComparator<_T_>::ReturnType, bool>)
  {
    switch(lowerBoundType())
    {
    case RangeBoundType::Inclusive:
    {
      if(details::RangeComparator<_T_>::isEqual(_v, lower()))
      {
        break;
      }
      Q_FALLTHROUGH();
    }
    case RangeBoundType::Exclusive:
    {
      if(details::RangeComparator<_T_>::isStrictlyInferior(_v, lower()))
      {
        return cres_success(false);
      }
    }
    case RangeBoundType::Unset:
      break;
    }
    switch(upperBoundType())
    {
    case RangeBoundType::Inclusive:
    {
      if(details::RangeComparator<_T_>::isEqual(_v, upper()))
      {
        break;
      }
      Q_FALLTHROUGH();
    }
    case RangeBoundType::Exclusive:
    {
      if(details::RangeComparator<_T_>::isStrictlyInferior(upper(), _v))
      {
        return cres_success(false);
      }
    }
    case RangeBoundType::Unset:
      break;
    }
    return true;
  }

#define __KNOWCORE_LOWER_VALUE_KEY QString("lower_value")
#define __KNOWCORE_LOWER_TYPE_KEY QString("lower_type")
#define __KNOWCORE_UPPER_VALUE_KEY QString("upper_value")
#define __KNOWCORE_UPPER_TYPE_KEY QString("upper_value")

  template<typename _T_>
  inline cres_qresult<QJsonValue>
    Range<_T_>::toJsonValue(const SerialisationContexts& _contexts) const
  {
    QJsonObject object;
    cres_try(object[__KNOWCORE_LOWER_VALUE_KEY],
             details::RangeRDFLiteral<_T_>::toJsonValue(lower(), _contexts));
    cres_try(object[__KNOWCORE_UPPER_VALUE_KEY],
             details::RangeRDFLiteral<_T_>::toJsonValue(upper(), _contexts));
    object[__KNOWCORE_LOWER_TYPE_KEY] = (int)lowerBoundType();
    object[__KNOWCORE_UPPER_TYPE_KEY] = (int)upperBoundType();
    return cres_success(object);
  }

  template<typename _T_>
  inline cres_qresult<Range<_T_>> Range<_T_>::fromJsonValue(const QJsonValue& _value,
                                                            const DeserialisationContexts& _context)
  {
    if(_value.isObject())
    {
      QJsonObject object = _value.toObject();
      cres_try(_T_ lower, details::RangeRDFLiteral<_T_>::fromJsonValue(
                            object.value(__KNOWCORE_LOWER_VALUE_KEY), _context));
      cres_try(_T_ upper, details::RangeRDFLiteral<_T_>::fromJsonValue(
                            object.value(__KNOWCORE_UPPER_VALUE_KEY), _context));
      RangeBoundType lowerType = RangeBoundType(object.value(__KNOWCORE_LOWER_TYPE_KEY).toInt());
      RangeBoundType upperType = RangeBoundType(object.value(__KNOWCORE_UPPER_TYPE_KEY).toInt());
      return cres_success<Range<_T_>>({lowerType, lower, upper, upperType});
    }
    else
    {
      return cres_failure("Expected object got {}", _value);
    }
  }

  template<typename _T_>
  inline cres_qresult<QCborValue>
    Range<_T_>::toCborValue(const SerialisationContexts& _contexts) const
  {
    QCborMap object;
    cres_try(object[__KNOWCORE_LOWER_VALUE_KEY],
             details::RangeRDFLiteral<_T_>::toCborValue(lower(), _contexts));
    cres_try(object[__KNOWCORE_UPPER_VALUE_KEY],
             details::RangeRDFLiteral<_T_>::toCborValue(upper(), _contexts));
    object[__KNOWCORE_LOWER_TYPE_KEY] = (int)lowerBoundType();
    object[__KNOWCORE_UPPER_TYPE_KEY] = (int)upperBoundType();
    return cres_success(object);
  }

  template<typename _T_>
  inline cres_qresult<Range<_T_>> Range<_T_>::fromCborValue(const QCborValue& _value,
                                                            const DeserialisationContexts& _context)
  {
    if(_value.isMap())
    {
      QCborMap object = _value.toMap();
      cres_try(_T_ lower, details::RangeRDFLiteral<_T_>::fromCborValue(
                            object.value(__KNOWCORE_LOWER_VALUE_KEY), _context));
      cres_try(_T_ upper, details::RangeRDFLiteral<_T_>::fromCborValue(
                            object.value(__KNOWCORE_UPPER_VALUE_KEY), _context));
      RangeBoundType lowerType
        = RangeBoundType(object.value(__KNOWCORE_LOWER_TYPE_KEY).toInteger());
      RangeBoundType upperType
        = RangeBoundType(object.value(__KNOWCORE_UPPER_TYPE_KEY).toInteger());
      return cres_success<Range<_T_>>({lowerType, lower, upper, upperType});
    }
    else
    {
      return cres_failure("Expected object got {}", _value);
    }
  }

  template<typename _T_>
  inline cres_qresult<QString>
    Range<_T_>::toRdfLiteral(const SerialisationContexts& _contexts) const
  {
    QString v;
    switch(lowerBoundType())
    {
    case RangeBoundType::Exclusive:
    case RangeBoundType::Unset:
      v = "]]";
      break;
    case RangeBoundType::Inclusive:
      v = "[[";
      break;
    }
    QString str_lower, str_upper;
    if(lowerBoundType() == RangeBoundType::Unset)
    {
      str_lower = "inf";
    }
    else
    {
      cres_try(str_lower, details::RangeRDFLiteral<_T_>::toRdfLiteral(lower(), _contexts));
    }
    if(upperBoundType() == RangeBoundType::Unset)
    {
      str_upper = "inf";
    }
    else
    {
      cres_try(str_upper, details::RangeRDFLiteral<_T_>::toRdfLiteral(upper(), _contexts));
    }

    v += str_lower + ", " + str_upper;

    switch(upperBoundType())
    {
    case RangeBoundType::Exclusive:
    case RangeBoundType::Unset:
      v += "[[";
      break;
    case RangeBoundType::Inclusive:
      v += "]]";
      break;
    }

    return cres_success(v);
  }

  template<typename _T_>
  inline cres_qresult<Range<_T_>>
    Range<_T_>::fromRdfLiteral(const QString& _value, const DeserialisationContexts& _context)
  {
    if(_value.size() < 5)
      return cres_failure("Invalid range RDF Literal: '{}'", _value);

    QString lowerBoundString = _value.left(2);
    QString upperBoundString = _value.right(2);
    QStringList values = _value.mid(2, _value.size() - 4).split(",", KNOWCORE_QT_SKIP_EMPTY_PART);

    RangeBoundType lowerBoundType;
    if(lowerBoundString == "]]")
      lowerBoundType = RangeBoundType::Exclusive;
    else if(lowerBoundString == "[[")
      lowerBoundType = RangeBoundType::Inclusive;
    else
      return cres_failure("Invalid lower bound symbol: '{}'", lowerBoundString);

    RangeBoundType upperBoundType;
    if(upperBoundString == "[[")
      upperBoundType = RangeBoundType::Exclusive;
    else if(upperBoundString == "]]")
      upperBoundType = RangeBoundType::Inclusive;
    else
      return cres_failure("Invalid lower bound symbol: '{}'", lowerBoundString);

    if(values.size() != 2)
      return cres_failure("Got wrong amount of values in: '{}' got {} expected 2", _value,
                          values.size());

    _T_ lower, upper;
    if(values[0] == "inf")
    {
      lowerBoundType = RangeBoundType::Unset;
    }
    else
    {
      cres_try(lower, details::RangeRDFLiteral<_T_>::fromRdfLiteral(values[0], _context));
    }
    if(values[1] == "inf")
    {
      upperBoundType = RangeBoundType::Unset;
    }
    else
    {
      cres_try(upper, details::RangeRDFLiteral<_T_>::fromRdfLiteral(values[1], _context));
    }

    return cres_success<Range<_T_>>({lowerBoundType, lower, upper, upperBoundType});
  }

  template<typename _T_>
  inline cres_qresult<QString> Range<_T_>::printable() const
  {
    return toRdfLiteral();
  }

#undef __KNOWCORE_LOWER_VALUE_KEY
#undef __KNOWCORE_LOWER_TYPE_KEY
#undef __KNOWCORE_UPPER_VALUE_KEY
#undef __KNOWCORE_UPPER_TYPE_KEY

} // namespace knowCore

#include "MetaType.h"
KNOWCORE_DECLARE_FULL_METATYPE(knowCore, QuantityNumberRange);
KNOWCORE_DECLARE_FULL_METATYPE(knowCore, NumberRange);
KNOWCORE_DECLARE_FULL_METATYPE(knowCore, ValueRange);

#include "Formatter.h"
clog_format_declare_enum_formatter(knowCore::RangeBoundType, Exclusive, Inclusive, Unset);

template<typename _T_>
struct std::formatter<knowCore::Range<_T_>>
    : public knowCore::printable_formatter<knowCore::Range<_T_>>
{
};
