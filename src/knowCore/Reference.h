#pragma once

#include <QExplicitlySharedDataPointer>

#include <clog_qt>
#include <cres_qt>

#include "Global.h"

#define KNOWCORE_DEFINE_REFERENCE(_KLASS_, _INTERFACE_, ...)                                       \
  class _KLASS_ : public knowCore::Reference<_INTERFACE_>                                          \
  {                                                                                                \
    using knowCore::Reference<_INTERFACE_>::interface;                                             \
  public:                                                                                          \
    using InterfaceType = _INTERFACE_;                                                             \
  public:                                                                                          \
    _KLASS_(InterfaceType* _lf) : knowCore::Reference<InterfaceType>(_lf) {}                       \
    _KLASS_() = default;                                                                           \
    _KLASS_(const _KLASS_&) = default;                                                             \
    _KLASS_& operator=(const _KLASS_&) = default;                                                  \
    ~_KLASS_() = default;                                                                          \
    KNOWCORE_UNLIST(__VA_ARGS__)                                                                   \
  };

namespace knowCore
{
  template<typename _Interface_>
  class Reference : protected _Interface_
  {
    template<typename _OInterface_>
    friend class Reference;
  public:
    using InterfaceType = _Interface_;
  protected:
    Reference(InterfaceType* _interface) : d(new Private) { d->interface = _interface; }
  public:
    Reference() = default;
    Reference(const Reference&) = default;
    Reference& operator=(const Reference&) = default;
    ~Reference() = default;
    template<typename _T_>
    cres_qresult<_T_> cast() const
    {
      if(dynamic_cast<const typename _T_::InterfaceType*>(interface()))
      {
        _T_ r;
        *r.d
          = *reinterpret_cast<typename _T_::Private*>(d); // TODO not pretty, find a better solution
        return cres_success(r);
      }
      else
      {
        return cres_failure("Cannot cast from '{} to '{}'", prettyTypename<InterfaceType>(),
                            prettyTypename<_T_::InterfaceType>());
      }
    }
    /**
     * @return true if the reference is valid
     */
    bool isValid() const { return d and d->interface; }
  protected:
    const InterfaceType* interface() const { return d->interface; }
    InterfaceType* interface() { return d->interface; }
  private:
    struct Private : public QSharedData
    {
      ~Private() { delete interface; }
      InterfaceType* interface = nullptr;
    };
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace knowCore
