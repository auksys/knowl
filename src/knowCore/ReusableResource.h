#include <QExplicitlySharedDataPointer>
#include <QMutex>

#include <clog_qt>

namespace knowCore
{
  /**
   * @ingroup knowCore
   *
   * Allow to define a ressource that can only be used by one thread at a time but can be reused
   * between threads
   */
  template<typename _T_>
  class ReusableResource
  {
  public:
    struct Handle_ : public QSharedData
    {
      friend ReusableResource;
      ~Handle_()
      {
        QMutexLocker l(&m_owner->m_mutex);
        m_owner->m_available.append(m_t);
      }
      _T_* ressource() const { return m_t; }
    private:
      _T_* m_t;
      ReusableResource<_T_>* m_owner;
    };
    using Handle = QExplicitlySharedDataPointer<Handle_>;
  public:
    ReusableResource(bool _free_resources = true) : m_free_resources(false) {}
    ~ReusableResource()
    {
      QMutexLocker l(&m_mutex);
      clog_assert(m_available.size() == m_resources.size());
      if(m_free_resources)
      {
        qDeleteAll(m_resources);
      }
    }
    Handle get()
    {
      Handle h(new Handle_);
      h->m_owner = this;
      QMutexLocker l(&m_mutex);
      if(m_available.isEmpty())
      {
        h->m_t = new _T_;
        m_resources.append(h->m_t);
      }
      else
      {
        h->m_t = m_available.takeFirst();
      }
      return h;
    }
  public:
    bool m_free_resources;
    QMutex m_mutex;
    QList<_T_*> m_available;
    QList<_T_*> m_resources;
  };
} // namespace knowCore
