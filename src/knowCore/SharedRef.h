#include <atomic>

namespace knowCore
{
  namespace details
  {
    struct SharedRefDefaultCheck
    {
      template<typename _T_>
      static bool canDelete(_T_*)
      {
        return true;
      }
    };
  } // namespace details
  /**
   * @ingroup knowCore
   * The \ref SharedRef acts as an (optional) smart pointer, in the sense, that it can contains
   * objects that are owned by it or not.
   */
  template<typename _T_, typename _TDeleteCheck_ = details::SharedRefDefaultCheck>
  class SharedRef
  {
    template<typename _TOther_, typename _TDeleteCheckOther_>
    friend class SharedRef;
  public:
    typedef _T_ ElementType;
  public:
    SharedRef() : m_data(nullptr) {}
    /**
     * Create a new SharedRef from object _T_, specify if the pointer is owned and manadged by this
     * \ref SharedRef or not.
     */
    SharedRef(_T_* _t, bool _own) : m_data(new Data)
    {
      m_data->t = _t;
      ++m_data->count;
      m_data->own = _own;
    }
    SharedRef(const SharedRef<_T_, _TDeleteCheck_>& _rhs) : m_data(nullptr) { ref(_rhs); }
    SharedRef& operator=(const SharedRef<_T_, _TDeleteCheck_>& _rhs)
    {
      if(_rhs.m_data != m_data)
      {
        deref();
        ref(_rhs);
      }
      return *this;
    }
    ~SharedRef() { deref(); }
  public: // Casting operator
    /**
     * Perform a dynamic cast from \ref _T_ to \param _TCast_.
     */
    template<typename _TCast_>
    SharedRef<_TCast_, _TDeleteCheck_> d_cast() const
    {
      using CastSharedRef = SharedRef<_TCast_, _TDeleteCheck_>;
      CastSharedRef res;
      if(m_data and dynamic_cast<_TCast_*>(m_data->t))
      {
        res.m_data = reinterpret_cast<typename CastSharedRef::Data*>(
          m_data); // a bit ugly but all Data have the same layout
        ++m_data->count;
      }
      return res;
    }
    /**
     * Perform a static cast from \ref _T_ to \param _TCast_.
     */
    template<typename _TCast_>
      requires(std::is_base_of_v<_TCast_, _T_>)
    SharedRef<_TCast_, _TDeleteCheck_> s_cast() const
    {
      using CastSharedRef = SharedRef<_TCast_, _TDeleteCheck_>;
      CastSharedRef res;
      if(m_data)
      {
        res.m_data = reinterpret_cast<typename CastSharedRef::Data*>(
          m_data); // a bit ugly but all Data have the same layout
        ++m_data->count;
      }
      return res;
    }
  public: // Comparison operator
    bool operator==(const SharedRef<_T_>& _rhs) const
    {
      return m_data == _rhs.m_data or m_data->t == _rhs.m_data->t;
    }
    bool operator==(const _T_* _rhs) const
    {
      return (m_data and m_data->t == _rhs) or (not m_data and not _rhs);
    }
    operator bool() const { return m_data and m_data->t; }
  public: // Access operator
    bool isValid() const { return m_data and m_data->t; }
    _T_* operator->() { return m_data->t; }
    const _T_* operator->() const { return m_data->t; }
    /**
     * The SharedRef is not anymore responsible for deleting the object, but still points to it!
     */
    _T_* grab()
    {
      m_data->own = false;
      return m_data->t;
    }
    _T_* data() { return m_data->t; }
    const _T_* data() const { return m_data->t; }
  private:
    void deref()
    {
      if(m_data)
      {
        --m_data->count;
        if(m_data->count == 0)
        {
          if(m_data->own and _TDeleteCheck_::canDelete(m_data->t))
          {
            delete m_data->t;
          }
          delete m_data;
        }
        m_data = nullptr;
      }
    }
    void ref(const SharedRef<_T_, _TDeleteCheck_>& _rhs)
    {
      m_data = _rhs.m_data;
      if(m_data)
      {
        ++m_data->count;
      }
    }
  private:
    struct Data
    {
      Data() : count(0) {}
      Data(const Data& _rhs) = delete;
      _T_* t = nullptr;
      std::atomic<int> count;
      bool own = false;
    };
    Data* m_data;
  };
} // namespace knowCore
