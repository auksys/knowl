#pragma once

#include <QtTest/QtTest>

#include <clog_qt>
#include <cres_qt>
#include <cres_qtest>

#include "BigNumber.h"
#include "Timestamp.h"
#include "Unit.h"
#include "Uri.h"
#include "Value.h"

namespace QTest
{
  inline bool qCompare(knowCore::Uri const& t1, const char* const& t2, const char* actual,
                       const char* expected, const char* file, int line)
  {
    return qCompare((QString)t1, QString::fromUtf8(t2), actual, expected, file, line);
  }
} // namespace QTest

namespace knowCore
{
  inline char* toString(const knowCore::Uri& t) { return QTest::toString((QString)t); }
  inline char* toString(const knowCore::Timestamp& t)
  {
    return QTest::toString(t.toEpoch<knowCore::NanoSeconds>().count());
  }
  inline char* toString(const knowCore::BigNumber& t) { return QTest::toString(t.toString()); }
  inline char* toString(const knowCore::Value& t)
  {
    auto const [success, result, error] = t.printable();
    if(success)
    {
      return QTest::toString(result.value());
    }
    else
    {
      return QTest::toString(error.value());
    }
  }
  template<typename _T_>
  inline char* toString(const cres_qresult<_T_>& _t)
  {
    if(_t.is_successful())
    {
      return QTest::toString(_t.get_value());
    }
    else
    {
      return toString(_t.get_error().get_message());
    }
  }
  inline char* toString(const knowCore::Unit& t)
  {
    return t.isValid() ? QTest::toString(t.symbol()) : QTest::toString("invalid unit");
  }
} // namespace knowCore

#define KNOWCORE_TEST_WAIT_FOR_ACT(_CONDITION_, _ACT_)                                             \
  for(int i = 0; i < 2000; ++i)                                                                    \
  {                                                                                                \
    QThread::msleep(10);                                                                           \
    {                                                                                              \
      _ACT_                                                                                        \
    }                                                                                              \
    auto ___expr___ = clog_eval_expr(_CONDITION_);                                                 \
    if(___expr___.value())                                                                         \
      break;                                                                                       \
    if((i % 100) == 0)                                                                             \
    {                                                                                              \
      clog_info("Waiting for: {} since {}s", ___expr___, i / 100);                                 \
    }                                                                                              \
  }                                                                                                \
  (void)0

#define KNOWCORE_TEST_WAIT_FOR(_CONDITION_) KNOWCORE_TEST_WAIT_FOR_ACT(_CONDITION_, )
