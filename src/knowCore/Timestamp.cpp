#include "Timestamp.h"

#include <QDateTime>
#include <QRegularExpression>
#include <QTimeZone>

#include <Cyqlops/Crypto/Hash.h>

#include "QuantityValue.h"
#include "TypeDefinitions.h"

#include "Uris/unit.h"

using qudt_unit = knowCore::Uris::unit;

using namespace knowCore;

namespace std
{
  template<typename _T_>
  struct common_type<BigNumber, BigNumber, _T_>
  {
    using type = BigNumber;
  };
} // namespace std

namespace
{
  cres_qresult<QDateTime> toQDateTime(NanoSeconds _time)
  {
    cres_try(qint64 ms, std::chrono::duration_cast<MilliSeconds>(_time).count().toInt64(true));
    return cres_success(QDateTime::fromMSecsSinceEpoch(ms));
  }
  QString fill(int _value, int _count)
  {
    QString r = QString::number(_value);
    while(r.size() < _count)
    {
      r = "0" + r;
    }
    return r;
  }
} // namespace

Unit Timestamp::nanoSECUnit()
{
  static Unit ns = Unit::byUri(Uris::unit::NanoSEC).expect_success();
  return ns;
}

cres_qresult<QDateTime> Timestamp::toQDateTime() const
{
  return ::toQDateTime(m_ns_since_unix_epoch);
}

cres_qresult<QString> Timestamp::toRdfLiteral() const
{
  auto const [s, dtl, e] = toQDateTime();

  if(s)
  {
    QDateTime dt = dtl.value().toTimeZone(QTimeZone::utc());
    NanoSeconds remainns = m_ns_since_unix_epoch
                           - std::chrono::duration_cast<NanoSeconds>(
                             MilliSeconds(BigNumber(dt.toMSecsSinceEpoch() - dt.time().msec())));
    cres_try(qint64 r, remainns.count().toInt64(true));
    return cres_success(clog_qt::qformat("{}-{}-{}T{}:{}:{}.{}Z", dt.date().year(),
                                         fill(dt.date().month(), 2), fill(dt.date().day(), 2),
                                         fill(dt.time().hour(), 2), fill(dt.time().minute(), 2),
                                         fill(dt.time().second(), 2), fill(r, 6)));
  }
  else
  {
    return cres_success(clog_qt::qformat("{}ns", m_ns_since_unix_epoch.count()));
  }
}

cres_qresult<Timestamp> Timestamp::fromEpoch(const QuantityNumber& qv)
{
  if(qv.unit().uri() == qudt_unit::NanoSEC)
  {
    return cres_success(fromEpoch(NanoSeconds(qv.value())));
  }
  else if(qv.unit().base().uri() == qudt_unit::SEC)
  {
    return cres_success(fromEpoch(NanoSeconds(qv.value() * qv.unit().scale() * 1e9)));
  }
  else
  {
    return cres_failure("Quantity not expressed in time unit '{}'", qv);
  }
}

cres_qresult<Timestamp> Timestamp::fromRdfLiteral(const QString& _literal)
{
  //                         1       2        3       4        5        6
  QRegularExpression reg(
    "(\\d+)-(\\d\\d)-(\\d\\d)T(\\d\\d):(\\d\\d):(\\d\\d).(\\d\\d\\d\\d\\d\\d)Z");
  QRegularExpressionMatch reg_match = reg.match(_literal);
  if(reg_match.hasMatch())
  {
    NanoSeconds ns_part(BigNumber(reg_match.captured(7).toInt()));
    QDateTime dt(QDate(reg_match.captured(1).toInt(), reg_match.captured(2).toInt(),
                       reg_match.captured(3).toInt()),
                 QTime(reg_match.captured(4).toInt(), reg_match.captured(5).toInt(),
                       reg_match.captured(6).toInt()),
                 QTimeZone::utc());
    return cres_success(fromEpoch(
      std::chrono::duration_cast<NanoSeconds>(MilliSeconds(BigNumber(dt.toMSecsSinceEpoch())))
      + ns_part));
  }
  else
  {
    QDateTime dt = QDateTime::fromString(_literal, Qt::ISODate);
    if(dt.isValid())
    {
      return cres_success(Timestamp::from(dt));
    }
    else
    {
      cres_try(QuantityNumber qv, QuantityNumber::fromRdfLiteral(_literal),
               message("Could not parse datetime from '{}' with error: '{}'", _literal));
      return fromEpoch(qv);
    }
  }
}

Timestamp Timestamp::endOfTime()
{
  Timestamp ts;
  ts.m_ns_since_unix_epoch = NanoSeconds(BigNumber::positiveInfinite());
  return ts;
}

Timestamp Timestamp::originOfTime()
{
  Timestamp ts;
  ts.m_ns_since_unix_epoch = NanoSeconds(BigNumber::negativeInfinite());
  return ts;
}

Timestamp Timestamp::now()
{
  return fromEpoch(std::chrono::high_resolution_clock::now().time_since_epoch());
}

Timestamp Timestamp::from(const QDateTime& _time)
{
  return fromEpoch<MilliSeconds>(_time.toMSecsSinceEpoch());
}

cres_qresult<NanoSeconds> Timestamp::localToEpoch(NanoSeconds _time)
{
  cres_try(QDateTime qdt, ::toQDateTime(_time));
  return cres_success(_time
                      - std::chrono::duration_cast<NanoSeconds>(
                        MilliSeconds(BigNumber(QTimeZone::systemTimeZone().offsetFromUtc(qdt)))));
}
cres_qresult<NanoSeconds> Timestamp::epochToLocal(NanoSeconds _time)
{
  cres_try(QDateTime qdt, ::toQDateTime(_time));
  return cres_success(_time
                      + std::chrono::duration_cast<NanoSeconds>(
                        MilliSeconds(BigNumber(QTimeZone::systemTimeZone().offsetFromUtc(qdt)))));
}

#include "MetaTypeImplementation.h"
#include "Uris/xsd.h"

KNOWCORE_DEFINE_CONVERT(Timestamp, _from, QString, _to, Safe)
{
  cres_try(*_to, _from->toRdfLiteral());
  return cres_success();
}

KNOWCORE_DEFINE_CONVERT(QString, _from, Timestamp, _to, Safe)
{
  return cres_try_assign(_to, Timestamp::fromRdfLiteral(*_from));
}

KNOWCORE_DEFINE_CONVERT(BigNumber, _from, Timestamp, _to, Force)
{
  // This conversion is weak, because it assumes that from is NanoSeconds
  *_to = Timestamp::fromEpoch(NanoSeconds(*_from));
  return cres_success();
}

KNOWCORE_DEFINE_CONVERT(Timestamp, _from, BigNumber, _to, Force)
{
  // This conversion is weak, because it assumes that quint64 is NanoSeconds
  *_to = _from->toEpoch<NanoSeconds>().count();
  return cres_success();
}

KNOWCORE_DEFINE_CONVERT(quint64, _from, Timestamp, _to, Force)
{
  // This conversion is weak, because it assumes that from is NanoSeconds
  *_to = Timestamp::fromEpoch(NanoSeconds(BigNumber(*_from)));
  return cres_success();
}

KNOWCORE_DEFINE_CONVERT(Timestamp, _from, quint64, _to, Force)
{
  // This conversion is weak, because it assumes that quint64 is NanoSeconds
  cres_try(*_to, _from->toEpoch<NanoSeconds>().count().toUInt64());
  return cres_success();
}

KNOWCORE_DEFINE_CONVERT(qint64, _from, Timestamp, _to, Force)
{
  // This conversion is weak, because it assumes that from is NanoSeconds
  *_to = Timestamp::fromEpoch(NanoSeconds(BigNumber(*_from)));
  return cres_success();
}

KNOWCORE_DEFINE_CONVERT(Timestamp, _from, qint64, _to, Force)
{
  // This conversion is weak, because it assumes that qint64 is NanoSeconds
  cres_try(*_to, _from->toEpoch<NanoSeconds>().count().toInt64());
  return cres_success();
}

KNOWCORE_DEFINE_CONVERT(float, _from, Timestamp, _to, Force)
{
  // This conversion is weak, because it assumes that from is Seconds
  *_to = Timestamp::fromEpoch(Seconds(BigNumber(*_from)));
  return cres_success();
}

KNOWCORE_DEFINE_CONVERT(Timestamp, _from, float, _to, Force)
{
  // This conversion is weak, because it assumes that float is Seconds
  *_to = _from->toEpoch<Seconds>().count().toDouble();
  return cres_success();
}

KNOWCORE_DEFINE_CONVERT(double, _from, Timestamp, _to, Force)
{
  // This conversion is weak, because it assumes that from is Seconds
  *_to = Timestamp::fromEpoch(Seconds(BigNumber(*_from)));
  return cres_success();
}

KNOWCORE_DEFINE_CONVERT(Timestamp, _from, double, _to, Force)
{
  // This conversion is weak, because it assumes that double is Seconds
  *_to = _from->toEpoch<Seconds>().count().toDouble();
  return cres_success();
}

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(Timestamp)
cres_qresult<QByteArray> md5(const Timestamp& _value) const override
{
  return cres_success(_value.toEpoch<NanoSeconds>().count().md5());
}
cres_qresult<QJsonValue> toJsonValue(const Timestamp& _value,
                                     const SerialisationContexts& _contexts) const override
{
  return MetaTypeInformation<QuantityNumber>::toJsonValue(_value.toEpoch<QuantityNumber>(),
                                                          _contexts);
}
cres_qresult<void> fromJsonValue(Timestamp* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts& _contexts) const override
{
  cres_try(QuantityNumber count,
           MetaTypeInformation<QuantityNumber>::fromJsonValue(_json_value, _contexts));
  cres_try(*_value, Timestamp::fromEpoch(count));
  return cres_success();
}
cres_qresult<QCborValue> toCborValue(const Timestamp& _value,
                                     const SerialisationContexts& _contexts) const override
{
  return knowCore::MetaTypeInformation<QuantityNumber>::toCborValue(
    _value.toEpoch<QuantityNumber>(), _contexts);
}
cres_qresult<void> fromCborValue(Timestamp* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts& _contexts) const override
{
  cres_try(QuantityNumber count,
           MetaTypeInformation<QuantityNumber>::fromCborValue(_cbor_value, _contexts));
  cres_try(*_value, Timestamp::fromEpoch(count));
  return cres_success();
}
cres_qresult<QString> printable(const Timestamp& _value) const override
{
  return _value.toRdfLiteral();
}
cres_qresult<QString> toRdfLiteral(const Timestamp& _value,
                                   const SerialisationContexts&) const override
{
  return _value.toRdfLiteral();
}
cres_qresult<void> fromRdfLiteral(Timestamp* _value, const QString& _serialised,
                                  const DeserialisationContexts&) const override
{
  return cres_try_assign(_value, Timestamp::fromRdfLiteral(_serialised));
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(Timestamp)

KNOWCORE_DEFINE_METATYPE(knowCore::Timestamp, knowCore::Uris::xsd::dateTime,
                         knowCore::MetaTypeTraits::Comparable | knowCore::MetaTypeTraits::ToString
                           | knowCore::MetaTypeTraits::FromString)

KNOWCORE_REGISTER_CONVERSION_FROM(BigNumber, Timestamp)
KNOWCORE_REGISTER_CONVERSION_FROM(Timestamp, BigNumber)
KNOWCORE_REGISTER_CONVERSION_FROM(quint64, Timestamp)
KNOWCORE_REGISTER_CONVERSION_FROM(Timestamp, quint64)
KNOWCORE_REGISTER_CONVERSION_FROM(qint64, Timestamp)
KNOWCORE_REGISTER_CONVERSION_FROM(Timestamp, qint64)
KNOWCORE_REGISTER_CONVERSION_FROM(float, Timestamp)
KNOWCORE_REGISTER_CONVERSION_FROM(Timestamp, float)
KNOWCORE_REGISTER_CONVERSION_FROM(double, Timestamp)
KNOWCORE_REGISTER_CONVERSION_FROM(Timestamp, double)
