#pragma once

#include <QtGlobal>

#include <chrono>

#include <clog_qt>
#include <cres_qt>

class QDateTime;

#include "BigNumber.h"
#include "QuantityValue.h"

namespace knowCore
{
  using Years = std::chrono::duration<BigNumber, std::ratio<31556952>>;
  using Months = std::chrono::duration<BigNumber, std::ratio<2629746>>;
  using Weeks = std::chrono::duration<BigNumber, std::ratio<604800>>;
  using Days = std::chrono::duration<BigNumber, std::ratio<86400>>;
  using Hours = std::chrono::duration<BigNumber, std::ratio<3600>>;
  using Minutes = std::chrono::duration<BigNumber, std::ratio<60>>;
  using Seconds = std::chrono::duration<BigNumber>;
  using MilliSeconds = std::chrono::duration<BigNumber, std::milli>;
  using NanoSeconds = std::chrono::duration<BigNumber, std::nano>;
  namespace details
  {
    template<typename T>
    struct is_ratio : std::false_type
    {
    };

    template<intmax_t _Num, intmax_t _Den>
    struct is_ratio<std::ratio<_Num, _Den>> : std::true_type
    {
    };
  } // namespace details
  class Timestamp
  {
  public:
    Timestamp(NanoSeconds _ns) : m_ns_since_unix_epoch(_ns) {}
  public:
    Timestamp() : m_ns_since_unix_epoch(BigNumber::zero()) {}
    ~Timestamp() {}

    bool isValid() const { return m_ns_since_unix_epoch.count() > 0; }
    cres_qresult<QDateTime> toQDateTime() const;

    /**
     * Create a datetime from a quantity number. It will fails if \p _quantity is not expressed in a
     * multiple of seconds.
     */
    static cres_qresult<Timestamp> fromEpoch(const QuantityNumber& _quantity);

    cres_qresult<QString> toRdfLiteral() const;
    static cres_qresult<Timestamp> fromRdfLiteral(const QString& _literal);

    template<typename _unit_>
    cres_qresult<_unit_> toLocal() const;
    /**
     * @return the time stamp in \p _unit_, which can be \ref QuantityNumber or \ref NanoSeconds or
     * \ref MilliSeconds...
     */
    template<typename _unit_>
    _unit_ toEpoch() const;

    bool operator<(const Timestamp& _rhs) const;
    bool operator<=(const Timestamp& _rhs) const;
    bool operator==(const Timestamp& _rhs) const;
    bool operator!=(const Timestamp& _rhs) const;
    bool operator>(const Timestamp& _rhs) const;
    bool operator>=(const Timestamp& _rhs) const;

    Timestamp operator+(const Timestamp& _rhs) const;
    Timestamp& operator+=(const Timestamp& _rhs);
    Timestamp operator-(const Timestamp& _rhs) const;
    Timestamp operator*(const knowCore::BigNumber& _rhs) const;

    template<typename _unit_>
    Timestamp add(_unit_ _unit) const;
    /**
     * Compute the interval of time between the current time and one given in arguments \p _time.
     */
    template<typename _unit_>
    _unit_ intervalTo(const Timestamp& _time) const;

    /**
     * The end of time, aka infinite.
     */
    static Timestamp endOfTime();
    /**
     * The origin of time, aka -infinite.
     */
    static Timestamp originOfTime();
    static Timestamp now();
    template<typename _unit_>
    static cres_qresult<Timestamp> fromLocal(_unit_ _value);
    template<typename _Period_>
      requires details::is_ratio<_Period_>::value
    static Timestamp fromEpoch(const std::chrono::duration<BigNumber, _Period_>& _value);
    template<typename _Rep_, typename _Period_>
      requires(not std::is_same_v<_Rep_, knowCore::BigNumber>
               and details::is_ratio<_Period_>::value)
    static Timestamp fromEpoch(const std::chrono::duration<_Rep_, _Period_>& _value);
    template<typename _unit_>
    static Timestamp fromEpoch(qint64 _value);
    template<typename _unit_>
    static Timestamp fromEpoch(const knowCore::BigNumber& _value);
    static Timestamp from(const QDateTime& _time);
  private:
    static cres_qresult<NanoSeconds> localToEpoch(NanoSeconds _time);
    static cres_qresult<NanoSeconds> epochToLocal(NanoSeconds _time);
    static Unit nanoSECUnit();
  private:
    NanoSeconds m_ns_since_unix_epoch;
  };

  template<>
  inline knowCore::QuantityNumber Timestamp::toEpoch<knowCore::QuantityNumber>() const
  {
    return knowCore::QuantityNumber(m_ns_since_unix_epoch.count(), nanoSECUnit());
  }

  template<typename _unit_>
  inline cres_qresult<Timestamp> Timestamp::fromLocal(_unit_ _value)
  {
    cres_try(NanoSeconds lte,
             localToEpoch(std::chrono::duration_cast<NanoSeconds>(_unit_(_value))));
    return cres_success(fromEpoch(lte));
  }
  template<typename _Period_>
    requires details::is_ratio<_Period_>::value
  inline Timestamp Timestamp::fromEpoch(const std::chrono::duration<BigNumber, _Period_>& _value)
  {
    return Timestamp(std::chrono::duration_cast<NanoSeconds>(_value));
  }
  template<typename _Rep_, typename _Period_>
    requires(not std::is_same_v<_Rep_, knowCore::BigNumber> and details::is_ratio<_Period_>::value)
  inline Timestamp Timestamp::fromEpoch(const std::chrono::duration<_Rep_, _Period_>& _value)
  {
    return fromEpoch<_Period_>(
      std::chrono::duration<knowCore::BigNumber, _Period_>(_value.count()));
  }
  template<typename _unit_>
  inline Timestamp Timestamp::fromEpoch(qint64 _value)
  {
    return fromEpoch(_unit_(BigNumber(_value)));
  }
  template<typename _unit_>
  inline Timestamp Timestamp::fromEpoch(const knowCore::BigNumber& _value)
  {
    return fromEpoch(_unit_(_value));
  }
  template<typename _unit_>
  inline cres_qresult<_unit_> Timestamp::toLocal() const
  {
    cres_try(NanoSeconds etl, epochToLocal(m_ns_since_unix_epoch));
    return std::chrono::duration_cast<_unit_>(etl);
  }
  template<typename _unit_>
  inline _unit_ Timestamp::toEpoch() const
  {
    return std::chrono::duration_cast<_unit_>(m_ns_since_unix_epoch);
  }

  inline bool Timestamp::operator<(const Timestamp& _rhs) const
  {
    return m_ns_since_unix_epoch < _rhs.m_ns_since_unix_epoch;
  }
  inline bool Timestamp::operator<=(const Timestamp& _rhs) const
  {
    return m_ns_since_unix_epoch <= _rhs.m_ns_since_unix_epoch;
  }
  inline bool Timestamp::operator==(const Timestamp& _rhs) const
  {
    return m_ns_since_unix_epoch == _rhs.m_ns_since_unix_epoch;
  }
  inline bool Timestamp::operator!=(const Timestamp& _rhs) const
  {
    return m_ns_since_unix_epoch != _rhs.m_ns_since_unix_epoch;
  }
  inline bool Timestamp::operator>(const Timestamp& _rhs) const
  {
    return m_ns_since_unix_epoch > _rhs.m_ns_since_unix_epoch;
  }
  inline bool Timestamp::operator>=(const Timestamp& _rhs) const
  {
    return m_ns_since_unix_epoch >= _rhs.m_ns_since_unix_epoch;
  }

  inline Timestamp Timestamp::operator+(const Timestamp& _rhs) const
  {
    return Timestamp(m_ns_since_unix_epoch + _rhs.m_ns_since_unix_epoch);
  }
  inline Timestamp& Timestamp::operator+=(const Timestamp& _rhs)
  {
    m_ns_since_unix_epoch = m_ns_since_unix_epoch + _rhs.m_ns_since_unix_epoch;
    return *this;
  }

  inline Timestamp Timestamp::operator-(const Timestamp& _rhs) const
  {
    return Timestamp(m_ns_since_unix_epoch - _rhs.m_ns_since_unix_epoch);
  }
  inline Timestamp Timestamp::operator*(const knowCore::BigNumber& _rhs) const
  {
    return Timestamp(m_ns_since_unix_epoch * _rhs);
  }

  template<typename _unit_>
  Timestamp Timestamp::add(_unit_ _unit) const
  {
    return Timestamp(m_ns_since_unix_epoch + std::chrono::duration_cast<NanoSeconds>(_unit));
  }

  template<typename _unit_>
  _unit_ Timestamp::intervalTo(const Timestamp& _time) const
  {
    return _time.toEpoch<_unit_>() - toEpoch<_unit_>();
  }
} // namespace knowCore

#include "MetaType.h"
KNOWCORE_DECLARE_FULL_METATYPE(knowCore, Timestamp);

inline knowCore::Timestamp operator"" _kCdtns(unsigned long long int _value)
{
  return knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(_value);
}

#include "Formatter.h"
clog_format_declare_formatter(knowCore::Timestamp)
{
  auto const& [s, v, e] = p.toQDateTime();
  if(s)
  {
    return format_to(ctx.out(), "{}", v.value());
  }
  else
  {
    return format_to(ctx.out(), "{}ns", p.toEpoch<knowCore::NanoSeconds>().count());
  }
}

inline knowCore::Timestamp operator""_kCtns(unsigned long long int _v)
{
  return knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(_v);
}
