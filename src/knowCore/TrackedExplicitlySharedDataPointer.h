#include <QExplicitlySharedDataPointer>

#include "MemoryLeakTracker.h"

namespace knowCore
{
  template<class T, bool enable_tracking>
  class TrackedExplicitlySharedDataPointer
  {
  public:
    inline TrackedExplicitlySharedDataPointer() { track(); }
    inline ~TrackedExplicitlySharedDataPointer() { untrack(); }

    explicit TrackedExplicitlySharedDataPointer(T* data) Q_DECL_NOTHROW : d(data) { track(); }
    inline TrackedExplicitlySharedDataPointer(
      const TrackedExplicitlySharedDataPointer<T, enable_tracking>& o)
        : d(o.d)
    {
      track();
    }
    inline TrackedExplicitlySharedDataPointer<T, enable_tracking>&
      operator=(const TrackedExplicitlySharedDataPointer<T, enable_tracking>& o)
    {
      untrack();
      d = o.d;
      track();
      return *this;
    }

    inline bool operator==(const TrackedExplicitlySharedDataPointer<T, enable_tracking>& o) const
    {
      return data() == o.data();
    }

    inline T& operator*() const { return *data(); }
    inline T* operator->() { return data(); }
    inline T* operator->() const { return data(); }
    inline T* data() const { return d.data(); }
    inline const T* constData() const { return d.constData(); }
  private:
    QExplicitlySharedDataPointer<T> d;
    void track()
    {
      if(enable_tracking)
      {
        MemoryLeakTracker::instance()->reference(this->data(), this);
      }
    }
    void untrack()
    {
      if(enable_tracking)
      {
        MemoryLeakTracker::instance()->dereference(this->data(), this);
      }
    }
  };

} // namespace knowCore
