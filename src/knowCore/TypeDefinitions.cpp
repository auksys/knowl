#include "TypeDefinitions.h"

#include <type_traits>

#include <QByteArray>
#include <QCborMap>
#include <QJsonDocument>
#include <QJsonValue>

#include "BigNumber.h"
#include "Cast.h"
#include "MetaTypeImplementation.h"
#include <clog_qt>

#include "Uris/askcore_datatype.h"
#include "Uris/xsd.h"

using namespace knowCore;

// BEGIN type definitions for bool

KNOWCORE_DEFINE_CONVERT(bool, _from, BigNumber, _to, Safe)
{
  *_to = BigNumber(0, BigNumber::Sign::Positive, 0, {quint16(_from ? 1 : 0)});
  return cres_success();
}

KNOWCORE_DEFINE_CONVERT(BigNumber, _from, bool, _to, Safe)
{
  if(_from->sign() == BigNumber::Sign::NaN)
  {
    return cres_failure("not a number");
  }
  *_to = _from->weight() != 0 or _from->dscale() != 0
         or (_from->digits().size() == 1 and _from->digits()[0] != 0) or _from->digits().size() > 1;
  return cres_success();
}

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(bool)
cres_qresult<QByteArray> md5(const bool& _value) const override
{
  char v = _value;
  QCryptographicHash hash(QCryptographicHash::Md5);
  hash.addData(QByteArrayView(&v, 1));
  return cres_success(hash.result());
}
cres_qresult<QJsonValue> toJsonValue(const bool& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(QJsonValue(_value));
}
cres_qresult<void> fromJsonValue(bool* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts&) const override
{
  if(_json_value.isBool())
  {
    *_value = _json_value.toBool();
    return cres_success();
  }
  else
  {
    return expectedError("boolean", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const bool& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(QCborValue(_value));
}
cres_qresult<void> fromCborValue(bool* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts&) const override
{
  if(_cbor_value.isBool())
  {
    *_value = _cbor_value.toBool();
    return cres_success();
  }
  else
  {
    return expectedError("boolean", _cbor_value);
  }
}
cres_qresult<QString> printable(const bool& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const bool& _value, const SerialisationContexts&) const override
{
  return cres_success(_value ? QStringLiteral("true") : QStringLiteral("false"));
}
cres_qresult<void> fromRdfLiteral(bool* _value, const QString& _serialised,
                                  const DeserialisationContexts&) const override
{
  if(_serialised == "true" or _serialised == "1")
  {
    *_value = true;
    return cres_success();
  }
  else if(_serialised == "false" or _serialised == "0")
  {
    *_value = false;
    return cres_success();
  }
  else
  {
    return cres_failure("Unknown boolean value '{}'", _serialised);
  }
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(bool)

KNOWCORE_DEFINE_METATYPE(bool, Uris::xsd::boolean, knowCore::MetaTypeTraits::NumericType)

// END type definitions for bool

namespace knowCore
{
  namespace details
  {
    template<class T>
    struct bn_convert_type;
    template<class T>
      requires(std::is_integral_v<T> and std::is_signed_v<T>)
    struct bn_convert_type<T> : public std::type_identity<qint64>
    {
    };
    template<class T>
      requires(std::is_integral_v<T> and std::is_unsigned_v<T>)
    struct bn_convert_type<T> : public std::type_identity<quint64>
    {
    };
    template<class T>
      requires(std::is_floating_point_v<T>)
    struct bn_convert_type<T> : public std::type_identity<double>
    {
    };
  } // namespace details

  // BEGIN BigNumber converter
  template<typename _TFrom_>
  struct Converter<_TFrom_, knowCore::BigNumber, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const _TFrom_* _from, knowCore::BigNumber* _to)
    {
      *_to = BigNumber(typename details::bn_convert_type<_TFrom_>::type(*_from));
      return cres_success();
    }
  };
  template<typename _TTo_>
    requires(std::is_integral_v<_TTo_> and std::is_signed_v<_TTo_>)
  struct Converter<knowCore::BigNumber, _TTo_, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const knowCore::BigNumber* _from, _TTo_* _to)
    {
      cres_try(*_to, _from->toInt64());
      return cres_success();
    }
  };
  template<typename _TTo_>
    requires(std::is_integral_v<_TTo_> and std::is_unsigned_v<_TTo_>)
  struct Converter<knowCore::BigNumber, _TTo_, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const knowCore::BigNumber* _from, _TTo_* _to)
    {
      cres_try(*_to, _from->toUInt64());
      return cres_success();
    }
  };
  template<typename _TTo_>
    requires(std::is_floating_point_v<_TTo_>)
  struct Converter<knowCore::BigNumber, _TTo_, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const knowCore::BigNumber* _from, _TTo_* _to)
    {
      *_to = _from->toDouble();
      return cres_success();
    }
  };
  // END BigNumber converter

  // BEGIN QString -> number converters
  template<typename _TTo_>
    requires(std::is_integral_v<_TTo_> and std::is_signed_v<_TTo_>)
  struct Converter<QString, _TTo_, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const QString* _from, _TTo_* _to)
    {
      bool ok;
      qint64 v = _from->toLongLong(&ok);
      if(ok)
      {
        if(std::is_same_v<_TTo_, qint64>
           or (v >= std::numeric_limits<_TTo_>::min() and v <= std::numeric_limits<_TTo_>::max()))
        {
          *_to = v;
          return cres_success();
        }
        else
        {
          return cres_failure("'{}' is out of bounds for '{}'", v, prettyTypename<_TTo_>());
        }
      }
      else
      {
        return cres_failure("Could not parse signed integer from '{}'", *_from);
      }
    }
  };
  template<typename _TTo_>
    requires(std::is_integral_v<_TTo_> and std::is_unsigned_v<_TTo_>)
  struct Converter<QString, _TTo_, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const QString* _from, _TTo_* _to)
    {
      bool ok;
      quint64 v = _from->toULongLong(&ok);
      if(ok)
      {
        if(std::is_same_v<_TTo_, quint64> or v <= std::numeric_limits<_TTo_>::max())
        {
          *_to = v;
          return cres_success();
        }
        else
        {
          return cres_failure("'{}' is out of bounds for '{}'", v, prettyTypename<_TTo_>());
        }
      }
      else
      {
        return cres_failure("Could not parse unsigned integer from '{}'", *_from);
      }
    }
  };
  template<typename _TTo_>
    requires(std::is_floating_point_v<_TTo_>)
  struct Converter<QString, _TTo_, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const QString* _from, _TTo_* _to)
    {
      bool ok;
      quint64 v = _from->toDouble(&ok);
      if(ok)
      {
        *_to = v;
        return cres_success();
      }
      else
      {
        return cres_failure("Could not parse unsigned integer from '{}'", *_from);
      }
    }
  };
  // END QString -> number converters
  // BEGIN number -> number converters
  template<typename _TFrom_, typename _TTo_>
    requires(std::is_arithmetic_v<_TFrom_> and std::is_arithmetic_v<_TTo_>
             and not std::is_same_v<_TTo_, BigNumber> and not std::is_same_v<_TFrom_, BigNumber>)
  struct Converter<_TFrom_, _TTo_, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const _TFrom_* _from, _TTo_* _to)
    {
      *_to = *_from;
      if(_TFrom_(*_to) == *_from)
      {
        return cres_success();
      }
      else
      {
        return cres_failure("Cannot represent '{}' from '{}' to '{}'", *_from,
                            prettyTypename<_TFrom_>(), prettyTypename<_TTo_>());
      }
    }
  };
  // END number -> number converters

  // BEGIN number -> number equality comparison
  template<typename _TLeft_, typename _TRight_>
    requires(std::is_unsigned_v<_TLeft_> and std::is_signed_v<_TRight_>
             and std::is_integral_v<_TRight_>)
  struct Comparator<ComparisonOperator::Equal, _TLeft_, _TRight_>
  {
    static inline cres_qresult<bool> compare(const _TLeft_& _left, const _TRight_& _right)
    {
      if(_right < 0)
        return cres_success(false);
      return cres_success(_left == std::make_unsigned_t<_TRight_>(_right));
    }
  };
  template<typename _TLeft_, typename _TRight_>
    requires(std::is_signed_v<_TLeft_> and std::is_integral_v<_TLeft_>
             and std::is_unsigned_v<_TRight_>)
  struct Comparator<ComparisonOperator::Equal, _TLeft_, _TRight_>
  {
    static inline cres_qresult<bool> compare(const _TLeft_& _left, const _TRight_& _right)
    {
      if(_left < 0)
        return cres_success(false);
      return cres_success(std::make_unsigned_t<_TLeft_>(_left) == _right);
    }
  };

  // END number -> number equality comparison

  // BEGIN number -> number inferior comparison
  template<typename _TLeft_, typename _TRight_>
    requires details::left_unsigned_right_signed<_TLeft_, _TRight_>
  struct Comparator<ComparisonOperator::Inferior, _TLeft_, _TRight_>
  {
    static inline cres_qresult<bool> compare(const _TLeft_& _left, const _TRight_& _right)
    {
      if(_right < 0)
        return cres_success(false);
      return cres_success(_left < std::make_unsigned_t<_TRight_>(_right));
    }
  };
  template<typename _TLeft_, typename _TRight_>
    requires details::left_signed_right_unsigned<_TLeft_, _TRight_>
  struct Comparator<ComparisonOperator::Inferior, _TLeft_, _TRight_>
  {
    static inline cres_qresult<bool> compare(const _TLeft_& _left, const _TRight_& _right)
    {
      if(_left < 0)
        return cres_success(false);
      return cres_success(std::make_unsigned_t<_TLeft_>(_left) < _right);
    }
  };

  // END number -> number inferior comparison
} // namespace knowCore

// BEGIN type definitions for integers and floats

template<typename _T_>
  requires(std::is_integral_v<_T_>)
KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION_TEMPLATE(_T_)
cres_qresult<QByteArray> md5(const _T_& _value) const override
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  hash.addData(QByteArrayView(reinterpret_cast<const char*>(&_value), sizeof(_T_)));
  return cres_success(hash.result());
}
cres_qresult<QJsonValue> toJsonValue(const _T_& _value, const SerialisationContexts&) const override
{
  return cres_success(QJsonValue(qint64(_value)));
}
cres_qresult<void> fromJsonValue(_T_* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts&) const override
{
  if(_json_value.isDouble())
  {
    *_value = _T_(_json_value.toDouble());
    return cres_success();
  }
  else
  {
    return expectedError("number", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const _T_& _value, const SerialisationContexts&) const override
{
  return cres_success(QCborValue(qint64(_value)));
}
cres_qresult<void> fromCborValue(_T_* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts&) const override
{
  if(_cbor_value.isInteger())
  {
    *_value = _cbor_value.toInteger();
    return cres_success();
  }
  else
  {
    return expectedError("integer", _cbor_value);
  }
}
cres_qresult<QString> printable(const _T_& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const _T_& _value, const SerialisationContexts&) const override
{
  return cres_success(QString::number(_value));
}
cres_qresult<void> fromRdfLiteral(_T_* _value, const QString& _serialised,
                                  const DeserialisationContexts&) const override
{
  bool s;
  if constexpr(std::is_signed_v<_T_>)
  {
    *_value = _serialised.toLongLong(&s);
  }
  else
  {
    *_value = _serialised.toULongLong(&s);
  }
  if(s)
  {
    return cres_success();
  }
  else
  {
    return cres_failure("Failed to convert to double: '{}'", _serialised);
  }
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(_T_)

KNOWCORE_DEFINE_METATYPE(quint8, Uris::xsd::unsignedInteger8, knowCore::MetaTypeTraits::NumericType)
KNOWCORE_DEFINE_METATYPE(quint16, Uris::xsd::unsignedInteger16,
                         knowCore::MetaTypeTraits::NumericType)
KNOWCORE_DEFINE_METATYPE(quint32, Uris::xsd::unsignedInteger32,
                         knowCore::MetaTypeTraits::NumericType)
KNOWCORE_DEFINE_METATYPE(quint64, Uris::xsd::unsignedInteger64,
                         knowCore::MetaTypeTraits::NumericType)
KNOWCORE_DEFINE_METATYPE(qint8, Uris::xsd::integer8, knowCore::MetaTypeTraits::NumericType)
KNOWCORE_DEFINE_METATYPE(qint16, Uris::xsd::integer16, knowCore::MetaTypeTraits::NumericType)
KNOWCORE_DEFINE_METATYPE(qint32, Uris::xsd::integer32, knowCore::MetaTypeTraits::NumericType)
KNOWCORE_DEFINE_METATYPE(qint64, Uris::xsd::integer64, knowCore::MetaTypeTraits::NumericType)

#define KNOWCORE_REGISTER_CONVERSION_COMPARATOR_FROM_NUMBER(__TYPE__)                              \
  KNOWCORE_REGISTER_CONVERSION_FROM(__TYPE__, quint8, quint16, quint32, quint64, qint8, qint16,    \
                                    qint32, qint64, float, double)                                 \
  KNOWCORE_REGISTER_COMPARATORS_FROM((Equal, Inferior), __TYPE__, quint8, quint16, quint32,        \
                                     quint64, qint8, qint16, qint32, qint64, float, double,        \
                                     BigNumber)                                                    \
  KNOWCORE_REGISTER_ARITHMETIC_OPERATORS((Addition, Substraction, Multiplication, Division),       \
                                         __TYPE__, quint8, quint16, quint32, quint64, qint8,       \
                                         qint16, qint32, qint64, float, double, BigNumber)

KNOWCORE_REGISTER_CONVERSION_COMPARATOR_FROM_NUMBER(quint8)
KNOWCORE_REGISTER_CONVERSION_COMPARATOR_FROM_NUMBER(quint16)
KNOWCORE_REGISTER_CONVERSION_COMPARATOR_FROM_NUMBER(quint32)
KNOWCORE_REGISTER_CONVERSION_COMPARATOR_FROM_NUMBER(quint64)
KNOWCORE_REGISTER_CONVERSION_COMPARATOR_FROM_NUMBER(qint8)
KNOWCORE_REGISTER_CONVERSION_COMPARATOR_FROM_NUMBER(qint16)
KNOWCORE_REGISTER_CONVERSION_COMPARATOR_FROM_NUMBER(qint32)
KNOWCORE_REGISTER_CONVERSION_COMPARATOR_FROM_NUMBER(qint64)

KNOWCORE_REGISTER_COMPARATORS_FROM((Equal, Inferior), knowCore::BigNumber, quint8, quint16, quint32,
                                   quint64, qint8, qint16, qint32, qint64, float, double)
KNOWCORE_REGISTER_ARITHMETIC_OPERATORS((Addition, Substraction, Multiplication, Division),
                                       BigNumber, quint8, quint16, quint32, quint64, qint8, qint16,
                                       qint32, qint64, float, double)

template<typename _T_>
  requires(std::is_floating_point_v<_T_>)
KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION_TEMPLATE(_T_)
//   template<typename _TT_, std::size_t _size_>
//   friend class knowCore::MetaTypeDefinition;
cres_qresult<QByteArray> md5(const _T_& _value) const override
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  hash.addData(QByteArrayView(reinterpret_cast<const char*>(&_value), sizeof(_T_)));
  return cres_success(hash.result());
}
cres_qresult<QJsonValue> toJsonValue(const _T_& _value, const SerialisationContexts&) const override
{
  return cres_success(QJsonValue(_value));
}
cres_qresult<void> fromJsonValue(_T_* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts&) const override
{
  if(_json_value.isDouble())
  {
    *_value = _T_(_json_value.toDouble());
    return cres_success();
  }
  else
  {
    return expectedError("double", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const _T_& _value, const SerialisationContexts&) const override
{
  return cres_success(QCborValue(_value));
}
cres_qresult<void> fromCborValue(_T_* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts&) const override
{
  if(_cbor_value.isDouble())
  {
    *_value = _cbor_value.toDouble();
    return cres_success();
  }
  else
  {
    return expectedError("double", _cbor_value);
  }
}
cres_qresult<QString> printable(const _T_& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const _T_& _value, const SerialisationContexts&) const override
{
  return cres_success(QString::number(_value));
}
cres_qresult<void> fromRdfLiteral(_T_* _value, const QString& _serialised,
                                  const DeserialisationContexts&) const override
{
  bool s;
  *_value = _serialised.toDouble(&s);
  if(s)
  {
    return cres_success();
  }
  else
  {
    return cres_failure("Failed to convert to integer: '{}'", _serialised);
  }
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(_T_)

KNOWCORE_DEFINE_METATYPE(float, Uris::xsd::float32, knowCore::MetaTypeTraits::NumericType)
KNOWCORE_DEFINE_METATYPE(double, Uris::xsd::float64, knowCore::MetaTypeTraits::NumericType)

KNOWCORE_REGISTER_CONVERSION_COMPARATOR_FROM_NUMBER(float)
KNOWCORE_REGISTER_CONVERSION_COMPARATOR_FROM_NUMBER(double)

KNOWCORE_REGISTER_CONVERSION_FROM(QString, quint8, quint16, quint32, quint64, qint8, qint16, qint32,
                                  qint64, float, double)

// END type definitions for integers and floats

// BEGIN list comparison operator
namespace knowCore
{
  template<typename _T1_, typename _T2_>
  bool operator<(const QList<_T1_>& _l1, const QList<_T2_>& _l2)
  {
    if(_l1.size() < _l2.size())
      return true;
    if(_l1.size() > _l2.size())
      return false;

    for(int i = 0; i < _l1.size(); ++i)
    {
      if(_l1[i] < _l2[i])
        return true;
      if(_l1[i] > _l2[i])
        return false;
    }
    return false;
  }
} // namespace knowCore
// END list comparison operator

// BEGIN define metatype for number containers
#define KNOWCORE_DEFINE_METATYPE_NUMBER_CONTAINER(__MF__, __URI__, __NAME__)                       \
  __MF__(knowCore::__NAME__##List, __URI__, knowCore::MetaTypeTraits::Comparable)

#define KNOWCORE_DEFINE_METATYPE_INT_CONTAINERS(__SIZE__)                                          \
  KNOWCORE_DEFINE_METATYPE_NUMBER_CONTAINER(                                                       \
    KNOWCORE_DEFINE_METATYPE, knowCore::Uris::askcore_datatype::integer##__SIZE__##list,           \
    Int##__SIZE__)                                                                                 \
  KNOWCORE_DEFINE_METATYPE_NUMBER_CONTAINER(                                                       \
    KNOWCORE_DEFINE_METATYPE, knowCore::Uris::askcore_datatype::unsignedinteger##__SIZE__##list,   \
    UInt##__SIZE__)

KNOWCORE_DEFINE_METATYPE_INT_CONTAINERS(8)
KNOWCORE_DEFINE_METATYPE_INT_CONTAINERS(16)
KNOWCORE_DEFINE_METATYPE_INT_CONTAINERS(32)
KNOWCORE_DEFINE_METATYPE_INT_CONTAINERS(64)

KNOWCORE_DEFINE_METATYPE_NUMBER_CONTAINER(KNOWCORE_DEFINE_METATYPE,
                                          knowCore::Uris::askcore_datatype::floatlist, Float)
KNOWCORE_DEFINE_METATYPE_NUMBER_CONTAINER(KNOWCORE_DEFINE_METATYPE,
                                          knowCore::Uris::askcore_datatype::doublelist, Double)

// END define metatype for number containers

// BEGIN type definitions for vectors

#define __KNOWCORE_VECTOR__ knowCore::Vector<_T_, _size_>

template<typename _T_, std::size_t _size_>
KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION_TEMPLATE(__KNOWCORE_VECTOR__)
cres_qresult<QByteArray> md5(const knowCore::Vector<_T_, _size_>& _value) const override
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  for(std::size_t i = 0; i < _size_; ++i)
  {
    cres_try(QByteArray value, m_nested.md5(_value[i]));
    hash.addData(value);
  }
  return cres_success(hash.result());
}
cres_qresult<QJsonValue> toJsonValue(const knowCore::Vector<_T_, _size_>& _value,
                                     const SerialisationContexts& _contexts) const override
{
  QJsonArray array;
  for(std::size_t i = 0; i < _size_; ++i)
  {
    cres_try(QJsonValue value, m_nested.toJsonValue(_value[i], _contexts));
    array.append(value);
  }
  return cres_success(array);
}
cres_qresult<void> fromJsonValue(knowCore::Vector<_T_, _size_>* _value,
                                 const QJsonValue& _json_value,
                                 const DeserialisationContexts& _contexts) const override
{
  if(_json_value.isArray())
  {
    QJsonArray array = _json_value.toArray();
    if(array.size() != _size_)
    {
      return expecedArraySizeError(array);
    }
    for(std::size_t i = 0; i < _size_; ++i)
    {
      _T_ v = _T_();
      cres_try(cres_ignore, m_nested.fromJsonValue(&v, array[i], _contexts));
      (*_value)[i] = v;
    }
    return cres_success();
  }
  else
  {
    return expectedArrayError(_json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const knowCore::Vector<_T_, _size_>& _value,
                                     const SerialisationContexts& _contexts) const override
{
  QCborArray array;
  for(std::size_t i = 0; i < _size_; ++i)
  {
    cres_try(QCborValue value, m_nested.toCborValue(_value[i], _contexts));
    array.append(value);
  }
  return cres_success(array);
}
cres_qresult<void> fromCborValue(knowCore::Vector<_T_, _size_>* _value,
                                 const QCborValue& _cbor_value,
                                 const DeserialisationContexts& _contexts) const override
{
  if(_cbor_value.isArray())
  {
    QCborArray array = _cbor_value.toArray();
    if(array.size() != _size_)
    {
      return expecedArraySizeError(array);
    }
    for(std::size_t i = 0; i < _size_; ++i)
    {
      _T_ v = _T_();
      cres_try(cres_ignore, m_nested.fromCborValue(&v, array[i], _contexts));
      (*_value)[i] = v;
    }
    return cres_success();
  }
  else
  {
    return expectedArrayError(_cbor_value);
  }
}
cres_qresult<QString> printable(const knowCore::Vector<_T_, _size_>& _value) const override
{
  QStringList strings;
  for(std::size_t i = 0; i < _size_; ++i)
  {
    cres_try(QString value, m_nested.printable(_value[i]));
    strings.append(value);
  }
  return cres_success(clog_qt::qformat("[{}]", strings));
}
cres_qresult<QString> toRdfLiteral(const knowCore::Vector<_T_, _size_>& _value,
                                   const SerialisationContexts& _contexts) const override
{
  return toJsonString(_value, _contexts);
}
cres_qresult<void> fromRdfLiteral(knowCore::Vector<_T_, _size_>* _value, const QString& _serialised,
                                  const DeserialisationContexts& _contexts) const override
{
  return fromJsonString(_value, _serialised, _contexts);
}
private:
template<typename _TCoJ_>
cres_qerror expecedArraySizeError(const _TCoJ_& _coj) const
{
  return cres_qerror(this->expectedError("array of size " __KNOWCORE_STR(_size_), _coj.size()));
}
MetaTypeDefinition<_T_> m_nested;
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(__KNOWCORE_VECTOR__)

KNOWCORE_DEFINE_METATYPE(knowCore::Vector2d, knowCore::Uris::askcore_datatype::vector2d,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector3d, knowCore::Uris::askcore_datatype::vector3d,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector4d, knowCore::Uris::askcore_datatype::vector4d,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector5d, knowCore::Uris::askcore_datatype::vector5d,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector6d, knowCore::Uris::askcore_datatype::vector6d,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector7d, knowCore::Uris::askcore_datatype::vector7d,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector8d, knowCore::Uris::askcore_datatype::vector8d,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector9d, knowCore::Uris::askcore_datatype::vector9d,
                         knowCore::MetaTypeTraits::Comparable)

KNOWCORE_DEFINE_METATYPE(knowCore::Vector2dList, knowCore::Uris::askcore_datatype::vector2dlist,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector3dList, knowCore::Uris::askcore_datatype::vector3dlist,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector4dList, knowCore::Uris::askcore_datatype::vector4dlist,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector5dList, knowCore::Uris::askcore_datatype::vector5dlist,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector6dList, knowCore::Uris::askcore_datatype::vector6dlist,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector7dList, knowCore::Uris::askcore_datatype::vector7dlist,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector8dList, knowCore::Uris::askcore_datatype::vector8dlist,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector9dList, knowCore::Uris::askcore_datatype::vector9dlist,
                         knowCore::MetaTypeTraits::Comparable)

KNOWCORE_DEFINE_METATYPE(knowCore::Vector2f, knowCore::Uris::askcore_datatype::vector2f,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector3f, knowCore::Uris::askcore_datatype::vector3f,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector4f, knowCore::Uris::askcore_datatype::vector4f,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector5f, knowCore::Uris::askcore_datatype::vector5f,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector6f, knowCore::Uris::askcore_datatype::vector6f,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector7f, knowCore::Uris::askcore_datatype::vector7f,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector8f, knowCore::Uris::askcore_datatype::vector8f,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector9f, knowCore::Uris::askcore_datatype::vector9f,
                         knowCore::MetaTypeTraits::Comparable)

KNOWCORE_DEFINE_METATYPE(knowCore::Vector2fList, knowCore::Uris::askcore_datatype::vector2flist,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector3fList, knowCore::Uris::askcore_datatype::vector3flist,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector4fList, knowCore::Uris::askcore_datatype::vector4flist,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector5fList, knowCore::Uris::askcore_datatype::vector5flist,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector6fList, knowCore::Uris::askcore_datatype::vector6flist,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector7fList, knowCore::Uris::askcore_datatype::vector7flist,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector8fList, knowCore::Uris::askcore_datatype::vector8flist,
                         knowCore::MetaTypeTraits::Comparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Vector9fList, knowCore::Uris::askcore_datatype::vector9flist,
                         knowCore::MetaTypeTraits::Comparable)

// END type definitions for vectors

// BEGIN type definitions for arrays

#include "MetaTypeDefinitionString_p.h"

#define DIMENSIONS_KEY QStringLiteral("dimensions")
#define VALUES_KEY QStringLiteral("values")

namespace knowCore
{
  // BEGIN array <-> list converters
  template<typename _TFrom_, typename _TTo_>
  struct Converter<QList<_TFrom_>, Array<_TTo_>, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const QList<_TFrom_>* _from, Array<_TTo_>* _to)
    {
      *_to = Array<_TTo_>(vectorCast(listCast<_TTo_>(*_from)), {_from->size()});
      return cres_success();
    }
  };
  template<typename _TFrom_, typename _TTo_>
  struct Converter<Array<_TFrom_>, QList<_TTo_>, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const Array<_TFrom_>* _from, QList<_TTo_>* _to)
    {
      cres_try(QList<_TFrom_> list, _from->toList());
      *_to = listCast<_TTo_>(list);
      return cres_success();
    }
  };
  // END array <-> list converters
} // namespace knowCore

template<typename _T_>
KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION_TEMPLATE(Array<_T_>)
cres_qresult<QByteArray> md5(const knowCore::Array<_T_>& _value) const override
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  for(const _T_& val : _value)
  {
    cres_try(QByteArray value, m_nested.md5(val));
    hash.addData(value);
  }
  return cres_success(hash.result());
}
cres_qresult<QJsonValue> toJsonValue(const knowCore::Array<_T_>& _value,
                                     const SerialisationContexts& _contexts) const override
{
  QJsonObject obj;
  QJsonArray dimensions;
  for(int d : _value.dimensions())
  {
    dimensions.append(d);
  }
  obj[DIMENSIONS_KEY] = dimensions;
  QJsonArray array;
  for(const _T_& val : _value)
  {
    cres_try(QJsonValue value, m_nested.toJsonValue(val, _contexts));
    array.append(value);
  }
  obj[VALUES_KEY] = array;
  return cres_success(obj);
}
cres_qresult<void> fromJsonValue(knowCore::Array<_T_>* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts& _contexts) const override
{
  if(_json_value.isObject())
  {
    QJsonObject obj;
    QList<qsizetype> dimensions;
    for(const QJsonValue& val_d : obj.value(DIMENSIONS_KEY).toArray())
    {
      dimensions.append(val_d.toDouble());
    }
    QVector<_T_> data;
    for(const QJsonValue& val_d : obj.value(VALUES_KEY).toArray())
    {
      _T_ value{};
      cres_try(cres_ignore, m_nested.fromJsonValue(&value, val_d, _contexts));
      data.append(value);
    }
    *_value = knowCore::Array<_T_>(data, dimensions);
    return cres_success();
  }
  else
  {
    return expectedObjectError(_json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const knowCore::Array<_T_>& _value,
                                     const SerialisationContexts& _contexts) const override
{
  QCborMap obj;
  QCborArray dimensions;
  for(int d : _value.dimensions())
  {
    dimensions.append(d);
  }
  obj[DIMENSIONS_KEY] = dimensions;
  QCborArray array;
  for(const _T_& val : _value)
  {
    cres_try(QCborValue value, m_nested.toCborValue(val, _contexts));
    array.append(value);
  }
  obj[VALUES_KEY] = array;
  return cres_success(obj);
}
cres_qresult<void> fromCborValue(knowCore::Array<_T_>* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts& _contexts) const override
{
  if(_cbor_value.isMap())
  {
    QCborMap obj;
    QList<qsizetype> dimensions;
    for(const QCborValue& val_d : obj.value(DIMENSIONS_KEY).toArray())
    {
      dimensions.append(val_d.toInteger());
    }
    QVector<_T_> data;
    for(const QCborValue& val_d : obj.value(VALUES_KEY).toArray())
    {
      _T_ value{};
      cres_try(cres_ignore, m_nested.fromCborValue(&value, val_d, _contexts));
      data.append(value);
    }
    *_value = knowCore::Array<_T_>(data, dimensions);
    return cres_success();
  }
  else
  {
    return expectedObjectError(_cbor_value);
  }
}
cres_qresult<QString> printable(const knowCore::Array<_T_>& _value) const override
{
  QStringList strings;
  for(const _T_& val : _value)
  {
    cres_try(QString value, m_nested.printable(val));
    strings.append(value);
  }
  return cres_success(clog_qt::qformat("[{},{}]", _value.dimensions(), strings));
}
cres_qresult<QString> toRdfLiteral(const knowCore::Array<_T_>& _value,
                                   const SerialisationContexts& _contexts) const override
{
  return toJsonString(_value, _contexts);
}
cres_qresult<void> fromRdfLiteral(knowCore::Array<_T_>* _value, const QString& _serialised,
                                  const DeserialisationContexts& _contexts) const override
{
  return fromJsonString(_value, _serialised, _contexts);
}
private:
template<typename _TCoJ_>
cres_qerror expecedArraySizeError(const _TCoJ_& _coj) const
{
  return cres_qerror(this->expectedError("array of size " __KNOWCORE_STR(_size_), _coj.size()));
}
MetaTypeDefinition<_T_> m_nested; // Implementations are in MetaTypeDefinitionString_p.h
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(Array<_T_>)

KNOWCORE_DEFINE_METATYPE(knowCore::Array<quint8>,
                         knowCore::Uris::askcore_datatype::unsignedinteger8array,
                         knowCore::MetaTypeTraits::OnlyEqualComparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Array<quint16>,
                         knowCore::Uris::askcore_datatype::unsignedinteger16array,
                         knowCore::MetaTypeTraits::OnlyEqualComparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Array<quint32>,
                         knowCore::Uris::askcore_datatype::unsignedinteger32array,
                         knowCore::MetaTypeTraits::OnlyEqualComparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Array<quint64>,
                         knowCore::Uris::askcore_datatype::unsignedinteger64array,
                         knowCore::MetaTypeTraits::OnlyEqualComparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Array<qint8>, knowCore::Uris::askcore_datatype::integer8array,
                         knowCore::MetaTypeTraits::OnlyEqualComparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Array<qint16>, knowCore::Uris::askcore_datatype::integer16array,
                         knowCore::MetaTypeTraits::OnlyEqualComparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Array<qint32>, knowCore::Uris::askcore_datatype::integer32array,
                         knowCore::MetaTypeTraits::OnlyEqualComparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Array<qint64>, knowCore::Uris::askcore_datatype::integer64array,
                         knowCore::MetaTypeTraits::OnlyEqualComparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Array<float>, knowCore::Uris::askcore_datatype::floatarray,
                         knowCore::MetaTypeTraits::OnlyEqualComparable)
KNOWCORE_DEFINE_METATYPE(knowCore::Array<double>, knowCore::Uris::askcore_datatype::doublearray,
                         knowCore::MetaTypeTraits::OnlyEqualComparable)

KNOWCORE_DEFINE_METATYPE(knowCore::BytesArray, knowCore::Uris::askcore_datatype::bytesarray,
                         knowCore::MetaTypeTraits::OnlyEqualComparable)
KNOWCORE_DEFINE_METATYPE(knowCore::StringArray, knowCore::Uris::askcore_datatype::stringarray,
                         knowCore::MetaTypeTraits::OnlyEqualComparable)

#define KNOWCORE_REGISTER_CONVERSION_ARRAY_LIST(_TYPE_)                                            \
  KNOWCORE_REGISTER_CONVERSION_FROM(                                                               \
    QList<_TYPE_>, knowCore::Array<quint8>, knowCore::Array<quint16>, knowCore::Array<quint32>,    \
    knowCore::Array<quint64>, knowCore::Array<qint8>, knowCore::Array<qint16>,                     \
    knowCore::Array<qint32>, knowCore::Array<qint64>, knowCore::Array<float>,                      \
    knowCore::Array<double>)                                                                       \
  KNOWCORE_REGISTER_CONVERSION_FROM(knowCore::Array<_TYPE_>, QList<quint8>, QList<quint16>,        \
                                    QList<quint32>, QList<quint64>, QList<qint8>, QList<qint16>,   \
                                    QList<qint32>, QList<qint64>, QList<float>, QList<double>)

KNOWCORE_REGISTER_CONVERSION_ARRAY_LIST(quint8)
KNOWCORE_REGISTER_CONVERSION_ARRAY_LIST(quint16)
KNOWCORE_REGISTER_CONVERSION_ARRAY_LIST(quint32)
KNOWCORE_REGISTER_CONVERSION_ARRAY_LIST(quint64)
KNOWCORE_REGISTER_CONVERSION_ARRAY_LIST(qint8)
KNOWCORE_REGISTER_CONVERSION_ARRAY_LIST(qint16)
KNOWCORE_REGISTER_CONVERSION_ARRAY_LIST(qint32)
KNOWCORE_REGISTER_CONVERSION_ARRAY_LIST(qint64)
KNOWCORE_REGISTER_CONVERSION_ARRAY_LIST(float)
KNOWCORE_REGISTER_CONVERSION_ARRAY_LIST(double)

namespace knowCore
{
  template<typename _T_, std::size_t _dimension_>
  struct Converter<Array<_T_>, Vector<_T_, _dimension_>, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const Array<_T_>* _from, Vector<_T_, _dimension_>* _to)
    {
      cres_try(*_to, _from->template toVector<_dimension_>());
      return cres_success();
    }
  };
  template<typename _T_, std::size_t _dimension_>
  struct Converter<Array<_T_>, QList<Vector<_T_, _dimension_>>, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const Array<_T_>* _from,
                                             QList<Vector<_T_, _dimension_>>* _to)
    {
      cres_try(*_to, _from->template toVectorList<_dimension_>());
      return cres_success();
    }
  };

  // BEGIN number -> number converters
  template<>
  struct Converter<QStringList, Array<QString>, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const QStringList* _from, Array<QString>* _to)
    {
      *_to = Array<QString>(vectorCast(listCast<QString>(*_from)), {_from->size()});
      return cres_success();
    }
  };
  template<>
  struct Converter<Array<QString>, QStringList, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const Array<QString>* _from, QStringList* _to)
    {
      cres_try(*_to, _from->toList());
      return cres_success();
    }
  };
  // END number -> number converters
} // namespace knowCore

#define KNOWCORE_REGISTER_CONVERSION_ARRAY_VECTOR(_TYPE_, _D_TYPE_)                                \
  KNOWCORE_REGISTER_CONVERSION_FROM(                                                               \
    knowCore::Array<_TYPE_>, Vector2##_D_TYPE_, Vector2##_D_TYPE_##List, Vector3##_D_TYPE_,        \
    Vector3##_D_TYPE_##List, Vector4##_D_TYPE_, Vector4##_D_TYPE_##List, Vector5##_D_TYPE_,        \
    Vector5##_D_TYPE_##List, Vector6##_D_TYPE_, Vector6##_D_TYPE_##List, Vector7##_D_TYPE_,        \
    Vector7##_D_TYPE_##List, Vector8##_D_TYPE_, Vector8##_D_TYPE_##List, Vector9##_D_TYPE_,        \
    Vector9##_D_TYPE_##List)

// , QList<Vector<KNOWCORE_LIST(_TYPE_, 2)>>, Vector<KNOWCORE_LIST(_TYPE_, 3)>

// , QList<Vector<KNOWCORE_LIST(_TYPE_, 3)>>, Vector<KNOWCORE_LIST(_TYPE_, 4)>,
// QList<Vector<KNOWCORE_LIST(_TYPE_, 4)>>, Vector<KNOWCORE_LIST(_TYPE_, 5)>,
// QList<Vector<KNOWCORE_LIST(_TYPE_, 5)>>, Vector<KNOWCORE_LIST(_TYPE_, 6)>,
// QList<Vector<KNOWCORE_LIST(_TYPE_, 6)>>, Vector<KNOWCORE_LIST(_TYPE_, 7)>,
// QList<Vector<KNOWCORE_LIST(_TYPE_, 7)>>, Vector<KNOWCORE_LIST(_TYPE_, 8)>,
// QList<Vector<KNOWCORE_LIST(_TYPE_, 8)>>, Vector<KNOWCORE_LIST(_TYPE_, 9)>,
// QList<Vector<KNOWCORE_LIST(_TYPE_, 9)>>)

KNOWCORE_REGISTER_CONVERSION_ARRAY_VECTOR(float, f)
KNOWCORE_REGISTER_CONVERSION_ARRAY_VECTOR(double, d)
KNOWCORE_REGISTER_CONVERSION_FROM(QList<QByteArray>, knowCore::BytesArray)
KNOWCORE_REGISTER_CONVERSION_FROM(knowCore::BytesArray, QList<QByteArray>)
KNOWCORE_REGISTER_CONVERSION_FROM(QStringList, knowCore::StringArray)
KNOWCORE_REGISTER_CONVERSION_FROM(knowCore::StringArray, QStringList)

// END type definitions for arrays

// BEGIN type definitions for QJsonValue

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(QJsonValue)
cres_qresult<QByteArray> md5(const QJsonValue& _value) const override
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  hash.addData(toString(_value).toUtf8());
  return cres_success(hash.result());
}
cres_qresult<QJsonValue> toJsonValue(const QJsonValue& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(_value);
}
cres_qresult<void> fromJsonValue(QJsonValue* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts&) const override
{

  if(_json_value.isString())
  {
    *_value = _json_value.toString();
    return cres_success();
  }
  else
  {
    return expectedError("string", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const QJsonValue& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(QCborValue::fromVariant(_value.toVariant()));
}
cres_qresult<void> fromCborValue(QJsonValue* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts&) const override
{
  *_value = QJsonValue::fromVariant(_cbor_value.toVariant());
  return cres_success();
}
cres_qresult<QString> printable(const QJsonValue& _value) const override
{
  return cres_success(toString(_value));
}
cres_qresult<QString> toRdfLiteral(const QJsonValue& _value,
                                   const SerialisationContexts&) const override
{
  return cres_success(toString(_value));
}
cres_qresult<void> fromRdfLiteral(QJsonValue* _value, const QString& _serialised,
                                  const DeserialisationContexts&) const override
{
  QJsonParseError err;
  QJsonDocument doc = QJsonDocument::fromJson(_serialised.toUtf8(), &err);
  if(doc.isNull())
  {
    return cres_failure("Failed to parse JSON document: {}", err.errorString());
  }
  else
  {
    if(doc.isObject())
      *_value = doc.object();
    else if(doc.isArray())
      *_value = doc.array();
    else
      return cres_failure("JSON Document should be object or array.");
    return cres_success();
  }
}
private:
static QString toString(const QJsonValue& _value)
{
  if(_value.isObject())
  {
    return QString::fromUtf8(QJsonDocument(_value.toObject()).toJson(QJsonDocument::Compact));
  }
  else if(_value.isArray())
  {
    return QString::fromUtf8(QJsonDocument(_value.toArray()).toJson(QJsonDocument::Compact));
  }
  else
  {
    return _value.toString();
  }
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(QJsonValue)

namespace knowCore
{
  template<>
  struct Converter<QJsonValue, QString, TypeCheckingMode::Safe>
  {
    static inline cres_qresult<void> convert(const QJsonValue* _from, QString* _to)
    {
      switch(_from->type())
      {
      case QJsonValue::Null:
        *_to = "Null";
        return cres_success();
      case QJsonValue::Bool:
        *_to = _from->toBool() ? "true" : "false";
        return cres_success();
      case QJsonValue::Double:
        *_to = QString::number(_from->toDouble());
        return cres_success();
      case QJsonValue::String:
        *_to = _from->toString();
        return cres_success();
      case QJsonValue::Array:
      case QJsonValue::Object:
      {
        QJsonDocument doc;
        switch(_from->type())
        {
        case QJsonValue::Array:
          doc.setArray(_from->toArray());
          break;
        case QJsonValue::Object:
          doc.setObject(_from->toObject());
          break;
        case QJsonValue::Null:
        case QJsonValue::Bool:
        case QJsonValue::Double:
        case QJsonValue::String:
        case QJsonValue::Undefined:
          clog_fatal("Impossible.");
        }
        *_to = QString::fromUtf8(doc.toJson());
      }
      break;
      case QJsonValue::Undefined:
        *_to = "Undefined";
        return cres_success();
      }
      return cres_failure("Unknown type of Json value.");
    }
  };
} // namespace knowCore

KNOWCORE_REGISTER_CONVERSION_TO(QString, QJsonValue)

KNOWCORE_DEFINE_METATYPE(QJsonValue, Uris::askcore_datatype::json, MetaTypeTraits::None)

// END type definitions for QJsonValue
