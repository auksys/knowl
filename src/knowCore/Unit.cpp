#include "Unit.h"

#if FMT_VERSION >= 70000
#include <fmt/args.h>
#endif

#include "Uri.h"

#include <cres_qt>

#include "Uris/askcore_unit.h"
#include "Uris/unit.h"

using namespace knowCore;

struct Unit::Private : public QSharedData
{
  QString symbol, name;
  knowCore::Uri uri;
  Unit base;
  double scale;
  QList<Unit> derived;

  static QPair<QStringList, QStringList> decompose(const QString& _symbol);
  static QPair<QString, int> splitAtomicSymbol(const QString& _atomic_symbol);
  static QString multiply(const QString& _symbol1, const QString& _symbol2);
  static QString divide(const QString& _symbol1, const QString& _symbol2);
  static QStringList multiply(const QStringList& _symbols, const QString& _symbol);
  static QString recompose(const QStringList& _mul, const QStringList& _div);

  struct Registry
  {
    ~Registry();
    QList<Unit> baseUnits;
    QHash<QString, QExplicitlySharedDataPointer<const Private>> symbolsToPrivate;
    QHash<QString, QExplicitlySharedDataPointer<const Private>> namesToPrivate;
    QHash<knowCore::Uri, QExplicitlySharedDataPointer<const Private>> urisToPrivate;

    QList<std::function<void()>> delayed_init;

    Unit registerUnit(const QString& _symbol, const QString& _name, const knowCore::Uri& _uri,
                      const Unit& _base, double _scale);
  };
  static Registry registry;
};

Unit::Private::Registry Unit::Private::registry;

Unit::Private::Registry::~Registry()
{
  for(Unit& u : baseUnits)
  {
    const_cast<Private*>(u.d.data())
      ->derived.clear(); // make sure to remove circular dependency to allow freeing memory
  }
}

Unit Unit::Private::Registry::registerUnit(const QString& _symbol, const QString& _name,
                                           const knowCore::Uri& _uri, const Unit& _base,
                                           double _scale)
{
  Unit u;
  Unit::Private* pd = new Unit::Private;
  pd->symbol = _symbol;
  pd->name = _name;
  pd->uri = _uri;
  pd->base = _base;
  pd->scale = _scale;
  u.d = pd;

  symbolsToPrivate[_symbol] = u.d;
  namesToPrivate[_name] = u.d;
  urisToPrivate[_uri] = u.d;

  if(_base.isValid())
  {
    const_cast<Private*>(_base.d.data())->derived.append(u);
  }
  else
  {
    baseUnits.append(u);
  }

  return u;
}

QPair<QStringList, QStringList> Unit::Private::decompose(const QString& _symbol)
{
  QStringList splited_div = _symbol.split("/", KNOWCORE_QT_SKIP_EMPTY_PART);
  if(splited_div.isEmpty())
    return {};
  QString mul = splited_div[0];
  QString div = splited_div.size() > 1 ? splited_div[1] : QString();
  if(div.size() > 0 and div[0] == '(')
  {
    div = div.mid(1, div.size() - 2);
  }
  return {mul.split(".", KNOWCORE_QT_SKIP_EMPTY_PART), div.split(".", KNOWCORE_QT_SKIP_EMPTY_PART)};
}

QPair<QString, int> Unit::Private::splitAtomicSymbol(const QString& _atomic_symbol)
{
  QStringList splited_s = _atomic_symbol.split("^", KNOWCORE_QT_SKIP_EMPTY_PART);

  clog_assert(not splited_s.isEmpty());

  int number = 1;

  if(splited_s.size() > 1)
  {
    number = splited_s[1].toInt();
  }

  return {splited_s[0], number};
}

QString Unit::Private::multiply(const QString& _symbol1, const QString& _symbol2)
{
  auto const [mul1, div1] = decompose(_symbol1);
  auto const [mul2, div2] = decompose(_symbol2);

  QStringList omul = mul2;
  for(const QString& s1 : mul1)
  {
    omul = multiply(omul, s1);
  }
  QStringList odiv = div2;
  for(const QString& s1 : div1)
  {
    odiv = multiply(odiv, s1);
  }
  return recompose(omul, odiv);
}

QString Unit::Private::divide(const QString& _symbol1, const QString& _symbol2)
{
  auto const [mul1, div1] = decompose(_symbol1);
  auto const [mul2, div2] = decompose(_symbol2);

  QStringList omul = div2;
  for(const QString& s1 : mul1)
  {
    omul = multiply(omul, s1);
  }
  QStringList odiv = mul2;
  for(const QString& s1 : div1)
  {
    odiv = multiply(odiv, s1);
  }
  return recompose(omul, odiv);
}

QString Unit::Private::recompose(const QStringList& _mul, const QStringList& _div)
{
  QStringList mul = _mul;
  QStringList div = _div;

  for(QStringList::iterator it_mul = mul.begin(); it_mul != mul.end();)
  {
    bool has_match = false;
    for(QStringList::iterator it_div = div.begin(); it_div != div.end();)
    {
      if(*it_mul == *it_div)
      {
        has_match = true;
        div.erase(it_div);
        break;
      }
      else
      {
        ++it_div;
      }
    }
    if(has_match)
    {
      it_mul = mul.erase(it_mul);
    }
    else
    {
      ++it_mul;
    }
  }

  QString symbol;

  switch(mul.size())
  {
  case 0:
    break;
  default:
    symbol = mul.join(".");
    break;
  }

  switch(div.size())
  {
  case 0:
    return symbol;
  case 1:
    return symbol + "/" + div[0];
  default:
    return symbol + "/(" + div.join(".") + ")";
  }
}

QStringList Unit::Private::multiply(const QStringList& _symbols, const QString& _symbol)
{
  auto const [symbol_root, symbol_number] = splitAtomicSymbol(_symbol);
  QStringList output = _symbols;
  for(int i = 0; i < output.size(); ++i)
  {
    auto const [o_symbol_root, o_symbol_number] = splitAtomicSymbol(output[i]);
    if(symbol_root == o_symbol_root)
    {
      output[i] = o_symbol_root + "^" + QString::number(symbol_number + o_symbol_number);
      return output;
    }
  }
  output.append(_symbol);
  output.sort();
  return output;
}

Unit Unit::empty()
{
  static Unit u(
    Private::registry.registerUnit("", "empty", Uris::askcore_unit::no_unit, Unit(), 1.0));
  return u;
}

Unit::Unit(const QExplicitlySharedDataPointer<const Private>& _d) : d(_d) {}

Unit::Unit() : d(nullptr) {}

Unit::Unit(const Unit& _unit) : d(_unit.d) {}

Unit& Unit::operator=(const Unit& _unit)
{
  d = _unit.d;
  return *this;
}

Unit::~Unit() {}

QString Unit::symbol() const { return d->symbol; }

QString Unit::name() const { return d->name; }

knowCore::Uri Unit::uri() const { return d->uri; }

Unit Unit::base() const { return d->base.isValid() ? d->base : *this; }

QList<Unit> Unit::derivedUnits() const { return d->derived; }

qreal Unit::scale() const { return d->scale; }

bool Unit::operator==(const Unit& _unit) const { return d == _unit.d; }

bool Unit::isValid() const { return d; }

cres_qresult<Unit> Unit::operator/(const Unit& _unit) const
{
  return Unit::bySymbol(Private::divide(symbol(), _unit.symbol()));
}

cres_qresult<Unit> Unit::operator*(const Unit& _unit) const
{
  return Unit::bySymbol(Private::multiply(symbol(), _unit.symbol()));
}

#define KPN(_C_, _S_, _P_)                                                                         \
  case Prefix::_C_##_P_:                                                                           \
    return #_S_ #_P_

QString Unit::prefixName(Prefix p)
{
  switch(p)
  {
    KPN(Y, y, otta);
    KPN(Z, z, etta);
    KPN(E, e, xa);
    KPN(P, p, eta);
    KPN(T, t, era);
    KPN(G, g, iga);
    KPN(M, m, ega);
    KPN(K, k, ilo);
    KPN(H, h, ecto);
    KPN(D, d, eca);
    KPN(D, d, eci);
    KPN(C, c, enti);
    KPN(M, m, illi);
    KPN(M, m, icro);
    KPN(N, n, ano);
    KPN(P, p, ico);
    KPN(F, f, emto);
    KPN(A, a, tto);
    KPN(Z, z, epto);
    KPN(Y, y, octo);
  case Prefix::Base:
    return QString();
  }
  clog_fatal("Invalid prefix");
}

#define KPS(_P_, _S_)                                                                              \
  case Prefix::_P_:                                                                                \
    return _S_

QString Unit::prefixSymbol(Prefix p)
{
  switch(p)
  {
    KPS(Yotta, "Y");
    KPS(Zetta, "Z");
    KPS(Exa, "E");
    KPS(Peta, "P");
    KPS(Tera, "T");
    KPS(Giga, "G");
    KPS(Mega, "M");
    KPS(Kilo, "k");
    KPS(Hecto, "h");
    KPS(Deca, "da");
    KPS(Base, "");
    KPS(Deci, "d");
    KPS(Centi, "c");
    KPS(Milli, "m");
    KPS(Micro, "μ");
    KPS(Nano, "n");
    KPS(Pico, "p");
    KPS(Femto, "f");
    KPS(Atto, "a");
    KPS(Zepto, "z");
    KPS(Yocto, "y");
  }
  clog_fatal("Invalid prefix");
}

#define KPSc(_C_, _S_)                                                                             \
  case Prefix::_C_:                                                                                \
    return 1e##_S_

#define KPScm(_C_, _S_)                                                                            \
  case Prefix::_C_:                                                                                \
    return 1e-##_S_

qreal Unit::prefixScale(Prefix p)
{
  switch(p)
  {
    KPSc(Yotta, 24);
    KPSc(Zetta, 21);
    KPSc(Exa, 18);
    KPSc(Peta, 15);
    KPSc(Tera, 12);
    KPSc(Giga, 9);
    KPSc(Mega, 6);
    KPSc(Kilo, 3);
    KPSc(Hecto, 2);
    KPSc(Deca, 1);
    KPSc(Base, 0);
    KPScm(Deci, 1);
    KPScm(Centi, 2);
    KPScm(Milli, 3);
    KPScm(Micro, 6);
    KPScm(Nano, 9);
    KPScm(Pico, 12);
    KPScm(Femto, 15);
    KPScm(Atto, 18);
    KPScm(Zepto, 21);
    KPScm(Yocto, 24);
  }
  clog_fatal("Invalid prefix");
}

QList<Unit> Unit::baseUnits() { return Private::registry.baseUnits; }

cres_qresult<Unit> Unit::bySymbol(const QString& _symbol)
{
  if(Private::registry.symbolsToPrivate.contains(_symbol))
  {
    return cres_success<Unit>(Private::registry.symbolsToPrivate.value(_symbol));
  }
  else
  {
    return cres_failure("Unknown unit symbol '{}'", _symbol);
  }
}

cres_qresult<Unit> Unit::byName(const QString& _name)
{
  if(Private::registry.namesToPrivate.contains(_name))
  {
    return cres_success<Unit>(Private::registry.namesToPrivate.value(_name));
  }
  else
  {
    return cres_failure("Unknown unit name '{}'", _name);
  }
}

cres_qresult<Unit> Unit::byUri(const knowCore::Uri& _uri)
{
  if(Private::registry.urisToPrivate.contains(_uri))
  {
    return cres_success<Unit>(Private::registry.urisToPrivate.value(_uri));
  }
  else
  {
    return cres_failure("Unknown unit uri '{}'", _uri);
  }
}

namespace
{
#define QPS(_P_, _S_)                                                                              \
  case Unit::Prefix::_P_:                                                                          \
    return _S_

  QString qunitPrefixSymbol(Unit::Prefix p)
  {
    switch(p)
    {
      QPS(Yotta, "Yotta");
      QPS(Zetta, "Zetta");
      QPS(Exa, "Exa");
      QPS(Peta, "Peta");
      QPS(Tera, "Tera");
      QPS(Giga, "Giga");
      QPS(Mega, "Mega");
      QPS(Kilo, "Kilo");
      QPS(Hecto, "Hecto");
      QPS(Deca, "Deca");
      QPS(Base, "");
      QPS(Deci, "Deci");
      QPS(Centi, "Centi");
      QPS(Milli, "Milli");
      QPS(Micro, "Micro");
      QPS(Nano, "Nano");
      QPS(Pico, "Pico");
      QPS(Femto, "Femto");
      QPS(Atto, "Atto");
      QPS(Zepto, "Zepto");
      QPS(Yocto, "Yocto");
    }
    clog_fatal("Invalid prefix");
  }
  QString aunitPrefixSymbol(Unit::Prefix p)
  {
    switch(p)
    {
      QPS(Yotta, "yotta");
      QPS(Zetta, "zetta");
      QPS(Exa, "exa");
      QPS(Peta, "peta");
      QPS(Tera, "tera");
      QPS(Giga, "giga");
      QPS(Mega, "mega");
      QPS(Kilo, "kilo");
      QPS(Hecto, "hecto");
      QPS(Deca, "deca");
      QPS(Base, "");
      QPS(Deci, "deci");
      QPS(Centi, "centi");
      QPS(Milli, "milli");
      QPS(Micro, "micro");
      QPS(Nano, "nano");
      QPS(Pico, "pico");
      QPS(Femto, "femto");
      QPS(Atto, "atto");
      QPS(Zepto, "zepto");
      QPS(Yocto, "yocto");
    }
    clog_fatal("Invalid prefix");
  }

} // namespace

UnitRegistration::UnitRegistration(const QString& _symbol, const QString& _name,
                                   const QString& _uri, const QList<Unit::Prefix>& _prefixes,
                                   const PrefixToScale& _scale, const PrefixToString& _to_uri)
{
  if(Unit::Private::registry.symbolsToPrivate.contains(_symbol))
  {
    clog_warning("Unit '{}' with name '{}' and uri '{}' has already been registered");
    return;
  }
  Unit base = Unit::Private::registry.registerUnit(_symbol, clog_qt::qformat(_name, ""),
                                                   clog_qt::qformat(_uri, ""), Unit(), 1.0);

  for(Unit::Prefix p : _prefixes)
  {
    if(p != Unit::Prefix::Base)
    {
      Unit::Private::registry.registerUnit(Unit::prefixSymbol(p) + _symbol,
                                           clog_qt::qformat(_name, Unit::prefixName(p)),
                                           clog_qt::qformat(_uri, _to_uri(p)), base, _scale(p));
    }
  }
}

namespace
{
  QString formatList(const QString& _format, const QStringList& _args)
  {
    switch(_args.size())
    {
    case 1:
      return clog_qt::qformat(_format, _args[0]);
    case 2:
      return clog_qt::qformat(_format, _args[0], _args[1]);
    case 3:
      return clog_qt::qformat(_format, _args[0], _args[1], _args[2]);
    case 4:
      return clog_qt::qformat(_format, _args[0], _args[1], _args[2], _args[3]);
    case 5:
      return clog_qt::qformat(_format, _args[0], _args[1], _args[2], _args[3], _args[4]);
    case 6:
      return clog_qt::qformat(_format, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5]);
    case 7:
      return clog_qt::qformat(_format, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5],
                              _args[6]);
    default:
      clog_fatal("Not supported: {}", _args.size());
    }
  }
} // namespace

UnitRegistration::UnitRegistration(const QString& _symbol, const QString& _name,
                                   const QString& _uri, const QList<QList<Unit::Prefix>>& _prefixes,
                                   const PrefixesToScale& _scale, const PrefixToString& _to_uri)
{
  QStringList list_symbols, list_names, list_uris;

  for(int i = 0; i < _prefixes.size(); ++i)
  {
    list_symbols.push_back(QString());
    list_names.push_back(QString());
    list_uris.push_back(QString());
  }
  QString base_symbol = formatList(_symbol, list_symbols);
  QString base_name = formatList(_name, list_names);
  QString base_uri = formatList(_uri, list_uris);

  Unit base = Unit::Private::registry.registerUnit(base_symbol, base_name, base_uri, Unit(), 1.0);

  QList<int> indices;
  std::fill_n(std::back_inserter(indices), _prefixes.size(), 0);

  const int last_prefixes = _prefixes.size() - 1;
  while(indices[last_prefixes] < _prefixes[last_prefixes].size())
  {
    QList<Unit::Prefix> prefixes;
    QStringList list_symbols, list_names, list_uris;

    for(int i = 0; i < _prefixes.size(); ++i)
    {
      Unit::Prefix p = _prefixes[i][indices[i]];
      prefixes.append(p);
      list_symbols.push_back(Unit::prefixSymbol(p));
      list_names.push_back(Unit::prefixName(p));
      list_uris.push_back(_to_uri(p));
    }
    QString symbol = formatList(_symbol, list_symbols);
    QString name = formatList(_name, list_names);
    QString uri = formatList(_uri, list_uris);

    if(symbol != base_symbol)
    {
      Unit::Private::registry.registerUnit(symbol, name, uri, base, _scale(prefixes));
    }
    for(int i = 0; i < _prefixes.size(); ++i)
    {
      ++indices[i];
      if(indices[i] >= _prefixes[i].size() and i != last_prefixes)
      {
        indices[i] = 0;
      }
      else
      {
        break;
      }
    }
  }
}

UnitRegistration::~UnitRegistration() {}

namespace
{
  template<int n>
  double pow(double _v)
  {
    return pow<n - 1>(_v) * _v;
  }
  template<>
  double pow<1>(double _v)
  {
    return _v;
  }
  template<int n>
  double prefixScale(Unit::Prefix _p)
  {
    return pow<n>(Unit::prefixScale(_p));
  }
  template<int n0, int n1>
  double divisionScale(const QList<Unit::Prefix>& _p)
  {
    clog_assert(_p.size() == 2);
    return pow<n0>(Unit::prefixScale(_p[0])) / pow<n1>(Unit::prefixScale(_p[1]));
  }
} // namespace

namespace knowCore
{
  struct make_sure_empty_is_registered
  {
    make_sure_empty_is_registered() { Unit::empty(); }
  };
  make_sure_empty_is_registered __KNOWCORE_UNIQUE_STATIC_NAME(make_sure_empty_is_registered);
} // namespace knowCore

KNOWCORE_DEFINE_UNIT(m, "m", "{}metre", "http://qudt.org/vocab/unit/{}M", &Unit::prefixScale,
                     &qunitPrefixSymbol, Kilo, Hecto, Deca, Base, Deci, Centi, Milli, Micro, Nano,
                     Pico);
KNOWCORE_DEFINE_UNIT(m2, "m^2", "square {}metre", "http://qudt.org/vocab/unit/{}M2",
                     &prefixScale<2>, &qunitPrefixSymbol, Kilo, Hecto, Deca, Base, Deci, Centi,
                     Milli, Micro, Nano, Pico);
KNOWCORE_DEFINE_UNIT(g, "g", "{}grams", "http://qudt.org/vocab/unit/{}GM", &Unit::prefixScale,
                     &qunitPrefixSymbol, Mega, Kilo, Hecto, Deca, Base, Deci, Centi, Milli, Micro,
                     Nano, Pico);
KNOWCORE_DEFINE_UNIT(s, "s", "{}second", "http://qudt.org/vocab/unit/{}SEC", &Unit::prefixScale,
                     &qunitPrefixSymbol, Kilo, Base, Milli, Micro, Nano, Pico);
KNOWCORE_DEFINE_UNIT(point, "point", "point", Uris::askcore_unit::point, &Unit::prefixScale,
                     &aunitPrefixSymbol, Base);
KNOWCORE_DEFINE_UNIT(W, "W", "{}watt", "http://qudt.org/vocab/unit/{}W", &Unit::prefixScale,
                     &qunitPrefixSymbol, Mega, Kilo, Hecto, Deca, Base, Deci, Centi, Milli, Micro,
                     Nano, Pico);

KNOWCORE_DEFINE_COMPOSITE_UNIT(m_per_s, "{0}m/{1}s", "{0}metre per {1}second",
                               "http://qudt.org/vocab/unit/{0}M-PER-{1}SEC",
                               &divisionScale<KNOWCORE_LIST(1, 1)>, &qunitPrefixSymbol,
                               {KNOWCORE_UNIT_PREFIX_LIST(Kilo, Hecto, Deca, Base, Deci, Centi,
                                                          Milli, Micro, Nano, Pico)},
                               {KNOWCORE_UNIT_PREFIX_LIST(Base, Milli, Micro, Nano)});

KNOWCORE_DEFINE_COMPOSITE_UNIT(point_per_m2, "{0}point/{1}m^2", "{0}point per square {1}metre",
                               "http://askco.re/unit#{0}point_per_{1}m2",
                               &divisionScale<KNOWCORE_LIST(1, 2)>, &aunitPrefixSymbol,
                               {KNOWCORE_UNIT_PREFIX_LIST(Base)},
                               {KNOWCORE_UNIT_PREFIX_LIST(Kilo, Hecto, Deca, Base, Deci, Centi,
                                                          Milli, Micro, Nano, Pico)});
