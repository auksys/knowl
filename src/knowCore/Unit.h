#pragma once

#include <QExplicitlySharedDataPointer>

#include "Forward.h"

namespace knowCore
{
  class UnitRegistration;
  /**
   * @ingroup knowCore
   *
   * Define a Unit following the International System of Units and the
   * http://www.w3.org/2007/ont/unit# ontology.
   */
  class Unit
  {
    friend class UnitRegistration;
  public:
    enum class Prefix
    {
      Yotta,
      Zetta,
      Exa,
      Peta,
      Tera,
      Giga,
      Mega,
      Kilo,
      Hecto,
      Deca,
      Base,
      Deci,
      Centi,
      Milli,
      Micro,
      Nano,
      Pico,
      Femto,
      Atto,
      Zepto,
      Yocto
    };
  public:
    static Unit empty();
    Unit();
    Unit(const Unit& _unit);
    Unit& operator=(const Unit& _unit);
    ~Unit();
    /**
     * @return the symbol that correspond to this unit (i.e. m, mm, m/s, mm^2, km^2...)
     */
    QString symbol() const;
    /**
     * @return the name of the unit (i.e. metre, second...)
     */
    QString name() const;
    /**
     * @return the URI of this unit, either from the http://qudt.org/vocab/unit or from
     * http://askco.re/unit# ontologies).
     */
    knowCore::Uri uri() const;
    /**
     * @return if the base of the unit, i.e., for millimetre, return metre, for millimetre/second
     * return metre/second
     */
    Unit base() const;
    /**
     * @return all derived units
     */
    QList<Unit> derivedUnits() const;
    /**
     * @return true if two units are identical
     */
    bool operator==(const Unit& _unit) const;
    /**
     * @return true if valid
     */
    bool isValid() const;
    /**
     * @return the unit obtained from dividing two units, or an error if the unit does not exists
     */
    cres_qresult<Unit> operator/(const Unit& _unit) const;
    /**
     * @return the unit obtained from multiplying two units, or an error if the unit does not exists
     */
    cres_qresult<Unit> operator*(const Unit& _unit) const;
    /**
     * @return the scale of the unit in relation to the base, i.e. how much to multiply a value
     * express in this unit to obtain the value in the base unit ( value in this is equal to scale *
     * value in the base). For millimetre this will return 1e-3.
     */
    qreal scale() const;
  public:
    /**
     * The symbol corresponding to the prefix.
     */
    static QString prefixSymbol(Prefix p);
    /**
     * The name corresponding to the prefix.
     */
    static QString prefixName(Prefix p);
    /**
     * The scale corresponding to the prefix.
     */
    static qreal prefixScale(Prefix p);
  public:
    /**
     * @return all base units
     */
    static QList<Unit> baseUnits();
    /**
     * @return unit given a symbol
     */
    static cres_qresult<Unit> bySymbol(const QString& _symbol);
    /**
     * @return unit given a name
     */
    static cres_qresult<Unit> byName(const QString& _name);
    /**
     * @return unit given a uri
     */
    static cres_qresult<Unit> byUri(const knowCore::Uri& _symbol);
  private:
    struct Private;
    QExplicitlySharedDataPointer<const Private> d;
    Unit(const QExplicitlySharedDataPointer<const Private>& _d);
  };
  /**
   * @ingroup knowCore
   *
   * Allow to register a unit. Do not use directly, use KNOWCORE_DEFINE_UNIT or
   * KNOWCORE_DEFINE_COMPOSITE_UNIT instead.
   */
  class UnitRegistration
  {
  public:
    using PrefixToScale = std::function<double(Unit::Prefix)>;
    using PrefixesToScale = std::function<double(const QList<Unit::Prefix>&)>;
    using PrefixToString = std::function<QString(Unit::Prefix)>;
  public:
    UnitRegistration(const QString& _symbol, const QString& _name, const QString& _uri,
                     const QList<Unit::Prefix>& _prefixes, const PrefixToScale& _scale,
                     const PrefixToString& _to_uri);
    UnitRegistration(const QString& _symbol, const QString& _name, const QString& _uri,
                     const QList<QList<Unit::Prefix>>& _prefixes, const PrefixesToScale& _scale,
                     const PrefixToString& _to_uri);
    ~UnitRegistration();
  };

} // namespace knowCore

#define __KNOWCORE_UNIT_ADD_PREFIX(X, I) knowCore::Unit::Prefix::X,

#define KNOWCORE_UNIT_PREFIX_LIST(...) KNOWCORE_FOREACH(__KNOWCORE_UNIT_ADD_PREFIX, __VA_ARGS__)

/**
 * Define a simple unit.
 * @p _CPP_SYMBOL_ is used to define a unique name for the registration
 * @p _SYMBOL_ is a format string for the symbol
 * @p _NAME_ is a format string for the name
 * @p _URI_ is a format string for the uri
 * @p _SCALE_F_ is a function that return the scale for the prefix
 * @p _TO_URI_F_ is a function that return the uri element for the prefix
 *
 * Variable arguments are use to select the prefixes.
 *
 * For examples see the bottom of Unit.cpp.
 */
#define KNOWCORE_DEFINE_UNIT(_CPP_SYMBOL_, _SYMBOL_, _NAME_, _URI_, _SCALE_F_, _TO_URI_F_, ...)    \
  knowCore::UnitRegistration __KNOWCORE_UNIQUE_STATIC_NAME(_CPP_SYMBOL_)(                          \
    _SYMBOL_, _NAME_, _URI_, {KNOWCORE_UNIT_PREFIX_LIST(__VA_ARGS__)}, _SCALE_F_, _TO_URI_F_)

/**
 * Define a simple unit.
 * @p _CPP_SYMBOL_ is used to define a unique name for the registration
 * @p _SYMBOL_ is a format string for the symbol
 * @p _NAME_ is a format string for the name
 * @p _URI_ is a format string for the uri
 * @p _SCALE_F_ is a function that return the scale for the prefix
 * @p _TO_URI_F_ is a function that return the uri element for the prefix
 *
 * Variable arguments are use to select the prefixes.
 *
 * For examples see the bottom of Unit.cpp.
 */
#define KNOWCORE_DEFINE_COMPOSITE_UNIT(_CPP_SYMBOL_, _SYMBOL_, _NAME_, _URI_, _SCALE_F_,           \
                                       _TO_URI_F_, ...)                                            \
  knowCore::UnitRegistration __KNOWCORE_UNIQUE_STATIC_NAME(_CPP_SYMBOL_)(                          \
    _SYMBOL_, _NAME_, _URI_, {__VA_ARGS__}, _SCALE_F_, _TO_URI_F_)

#include "Formatter.h"

clog_format_declare_formatter(knowCore::Unit)
{
  return std::format_to(ctx.out(), "{}", p.symbol());
}
