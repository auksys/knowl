#include "Uri.h"

#include <QUrl>
#include <QUuid>

#include "UriManager.h"
#include "ValueList.h"

using namespace knowCore;

struct Uri::Private : public QSharedData
{
  Private() : is_urn(false) {}
  bool is_urn;
  QString uri;

  void parse(const QString& _string);

  static QHash<QString, std::function<QString(const QUrl& _url)>> resolvers;
};

QHash<QString, std::function<QString(const QUrl& _url)>> Uri::Private::resolvers;

void Uri::Private::parse(const QString& _string)
{
  is_urn = _string.startsWith(QStringLiteral("urn:"));
  uri = _string;
}

Uri::Uri() : d(new Private) {}

Uri::Uri(const QUrl& _url) : Uri(_url.toString()) {}

Uri::Uri(const QString& _uri) : d(new Private) { d->parse(_uri); }

Uri::Uri(const Uri& _base, const QString& _uri) { *this = _base.resolved(_uri); }

Uri::Uri(const Uri& _rhs) : d(_rhs.d) {}

Uri& Uri::operator=(const Uri& _rhs)
{
  d = _rhs.d;
  return *this;
}

Uri::~Uri() {}

Uri Uri::createUnique(const QStringList& _path)
{
  QString suffix;
  if(not _path.isEmpty())
  {
    suffix = ":" + _path.join(":");
  }
  QString us = QUuid::createUuid().toString();
  return "urn:uuid:" + us.mid(1, us.size() - 2) + suffix;
}

bool Uri::operator==(const Uri& _rhs) const { return d->uri == _rhs.d->uri; }

bool Uri::operator==(const char* _rhs) const { return d->uri == _rhs; }

bool Uri::operator<(const Uri& _rhs) const { return d->uri < _rhs.d->uri; }

Uri::operator QUrl() const { return QUrl(d->uri); }

Uri::operator QString() const { return d->uri; }

bool Uri::isEmpty() const { return d->uri.isEmpty(); }

bool Uri::isAbsolute() const
{
  if(d->is_urn)
    return false;
  QUrl qurl = d->uri;
  return not qurl.isRelative();
}

bool Uri::isUrl() const { return not isEmpty() and not d->is_urn; }

bool Uri::isUrn() const { return not isEmpty() and d->is_urn; }

Uri Uri::resolved(const Uri& _url) const
{
  if(d->is_urn)
  {
    if(d->uri.right(1) == ":")
    {
      return Uri(d->uri + _url.d->uri);
    }
    else
    {
      return Uri(d->uri + ":" + _url.d->uri);
    }
  }
  else if(_url.isAbsolute() or d->uri.isEmpty())
  {
    return _url;
  }
  else if(_url.isEmpty())
  {
    return *this;
  }
  else
  {
    return base() + _url.d->uri;
  }
}

Uri Uri::resolved(const QString& _url) const { return resolved(Uri(_url)); }

QString Uri::base() const
{
  if(d->uri.endsWith("/") or d->uri.endsWith("#"))
  {
    return d->uri;
  }
  int slash_last_index = d->uri.lastIndexOf("/");
  int sharp_last_index = d->uri.lastIndexOf("#");

  int cut_index = std::max(slash_last_index, sharp_last_index);
  if(cut_index == -1)
  {
    return QString();
  }
  else
  {
    return d->uri.left(cut_index + 1);
  }
}

void Uri::registerLocalFileResolver(const QString& _scheme,
                                    const std::function<QString(const QUrl& _url)>& _resolver)
{
  Uri::Private::resolvers[_scheme] = _resolver;
}

QString Uri::toLocalFile() const
{
  QUrl url((QString) * this);

  if(Uri::Private::resolvers.contains(url.scheme()))
  {
    return Uri::Private::resolvers.value(url.scheme())(url);
  }
  else
  {
    return url.toLocalFile();
  }
}

#include "MetaTypeImplementation.h"
#include "Uris/xsd.h"

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(Uri)
cres_qresult<QByteArray> md5(const Uri& _value) const override
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  hash.addData(((QString)_value).toUtf8());
  return cres_success(hash.result());
}
cres_qresult<QJsonValue> toJsonValue(const Uri& _value, const SerialisationContexts&) const override
{
  return cres_success(QJsonValue((QString)_value));
}
cres_qresult<void> fromJsonValue(Uri* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts&) const override
{
  if(_json_value.isString())
  {
    *_value = _json_value.toString();
    return cres_success();
  }
  else
  {
    return expectedError("string", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const Uri& _value, const SerialisationContexts&) const override
{
  return cres_success(QCborValue((QString)_value));
}
cres_qresult<void> fromCborValue(Uri* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts&) const override
{
  if(_cbor_value.isString())
  {
    *_value = _cbor_value.toString();
    return cres_success();
  }
  else
  {
    return expectedError("string", _cbor_value);
  }
}
cres_qresult<QString> printable(const Uri& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const Uri& _value, const SerialisationContexts&) const override
{
  return cres_success<QString>(_value);
}
cres_qresult<void> fromRdfLiteral(Uri* _value, const QString& _serialised,
                                  const DeserialisationContexts&) const override
{
  *_value = _serialised;
  return cres_success();
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(Uri)

KNOWCORE_DEFINE_METATYPE(Uri, Uris::xsd::anyURI,
                         MetaTypeTraits::Comparable | MetaTypeTraits::ToString
                           | MetaTypeTraits::FromString)

#include <knowCore/Uris/askcore_datatype.h>

// UriList MetaTypeDefinition needs access to the MetaTypeDefinition of Uri

#include "UriList.h"

template<>
struct knowCore::MetaTypeDefinition<UriList> : public MetaTypeDefinition<QList<Uri>>
{
  Uri uri() const override { return MetaTypeInformation<UriList>::uri(); }
  int qMetaTypeId() const override { return MetaTypeInformation<UriList>::id(); }
};

KNOWCORE_DEFINE_METATYPE(UriList, Uris::askcore_datatype::uriList,
                         MetaTypeTraits::Comparable | MetaTypeTraits::FromValueList
                           | MetaTypeTraits::ToValueList)

KNOWCORE_DEFINE_CONVERT(ValueList, _from, UriList, _to, Safe)
{
  for(const knowCore::Value& value : *_from)
  {
    cres_try(Uri uri, value.value<Uri>());
    _to->append(uri);
  }
  return cres_success();
}

KNOWCORE_REGISTER_CONVERSION_FROM(ValueList, UriList)

template<>
struct knowCore::Comparator<ComparisonOperator::In, Uri, UriList>
{
  static inline cres_qresult<bool> compare(const Uri& _left, const UriList& _right)
  {
    return cres_success(_right.contains(_left));
  }
};

KNOWCORE_REGISTER_COMPARATORS_FROM((In), Uri, UriList)

template<>
struct knowCore::Comparator<ComparisonOperator::Equal, QString, Uri>
{
  static inline cres_qresult<bool> compare(const QString& _left, const Uri& _right)
  {
    return cres_success(_left == QString(_right));
  }
};

template<>
struct knowCore::Comparator<ComparisonOperator::Equal, Uri, QString>
{
  static inline cres_qresult<bool> compare(const Uri& _left, const QString& _right)
  {
    return cres_success(QString(_left) == _right);
  }
};
KNOWCORE_REGISTER_COMPARATORS_FROM((Equal), QString, Uri)
KNOWCORE_REGISTER_COMPARATORS_FROM((Equal), Uri, QString)
