#ifndef _KNOW_CORE_URI_H_
#define _KNOW_CORE_URI_H_

#include <functional>

#include <QSharedDataPointer>
#include <QVariant>

#include "Forward.h"

namespace knowCore
{
  class Uri
  {
  public:
    Uri();
    Uri(const QUrl& _url);
    Uri(const QString& _uri);
    Uri(const Uri& _base, const QString& _uri);
    Uri(const Uri& _rhs);
    Uri& operator=(const Uri& _rhs);
    ~Uri();
    /**
     * Create a unique Uri based on a Uuid (according to RFC 4122
     * https://tools.ietf.org/html/rfc4122)
     */
    static Uri createUnique(const QStringList& _path = QStringList());

    bool operator==(const Uri& _rhs) const;
    bool operator==(const char* _rhs) const;
    bool operator<(const Uri& _rhs) const;
    bool operator!=(const Uri& _rhs) const { return !(*this == _rhs); }
    bool operator!=(const char* _rhs) const { return !(*this == _rhs); }

    operator QUrl() const;
    operator QString() const;

    bool isEmpty() const;
    bool isAbsolute() const;
    bool isUrn() const;
    bool isUrl() const;

    Uri resolved(const Uri& _url) const;
    Uri resolved(const QString& _url) const;

    static void
      registerLocalFileResolver(const QString& _scheme,
                                const std::function<QString(const QUrl& _url)>& _resolver);
    QString toLocalFile() const;
    /**
     * @return Return the base of the URI (ie without a filename)
     */
    QString base() const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
  inline uint qHash(const Uri& key, uint seed = 0) { return qHash((QString)key, seed); }
} // namespace knowCore

inline knowCore::Uri operator"" _kCu(const char* _text, std::size_t)
{
  return knowCore::Uri(_text);
}

Q_DECLARE_METATYPE(knowCore::Uri)

#include "Formatter.h"
clog_format_declare_cast_formatter(knowCore::Uri, QString);

#endif
