#include "UriList.h"

#include <knowCore/Uris/askcore_datatype.h>

using namespace knowCore;

QStringList UriList::toStringList() const
{
  QStringList result;
  std::copy(begin(), end(), std::back_inserter(result));
  return result;
}
