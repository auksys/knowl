#pragma once

#include "Uris/Uris.h"
#include <QStringList>

namespace knowCore
{
  class UriList : public QList<Uri>
  {
  public:
    inline UriList(std::initializer_list<QString> args) : UriList(QList<QString>(args)) {}
    template<typename _T_>
      requires(Uris::IsUriDefinitionV<_T_>)
    inline UriList(std::initializer_list<_T_> args) Q_DECL_NOTHROW
    {
      std::copy(args.begin(), args.end(), std::front_inserter(*this));
    }
    inline UriList(const QList<QString>& _other) Q_DECL_NOTHROW
    {
      std::copy(_other.begin(), _other.end(), std::front_inserter(*this));
    }
    inline UriList() Q_DECL_NOTHROW {}
    inline explicit UriList(const Uri& i) { append(i); }
    inline UriList(const QList<Uri>& l) : QList<Uri>(l) {}
#ifdef Q_COMPILER_RVALUE_REFS
    inline UriList(QList<Uri>&& l) Q_DECL_NOTHROW : QList<Uri>(std::move(l)) {}
#endif
#ifdef Q_COMPILER_INITIALIZER_LISTS
    inline UriList(std::initializer_list<Uri> args) : QList<Uri>(args) {}
#endif

    UriList& operator=(const QList<QString>& _other)
    {
      clear();
      std::copy(_other.begin(), _other.end(), std::front_inserter(*this));
      return *this;
    }
    UriList& operator=(const QList<Uri>& other)
    {
      QList<Uri>::operator=(other);
      return *this;
    }
#ifdef Q_COMPILER_RVALUE_REFS
    UriList& operator=(QList<Uri>&& other) Q_DECL_NOTHROW
    {
      QList<Uri>::operator=(std::move(other));
      return *this;
    }
#endif
    QStringList toStringList() const;
  };
} // namespace knowCore

#include "MetaType.h"
KNOWCORE_DECLARE_FULL_METATYPE(knowCore, UriList);

#include "Formatter.h"

// Use QList<knowCore::Uri> formatter

template<>
struct std::formatter<knowCore::UriList> : public std::formatter<QList<knowCore::Uri>>
{
};
