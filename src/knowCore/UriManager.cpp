/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "UriManager.h"

#include <QHash>
#include <QString>

#include "Curie.h"
#include "Uri.h"

using namespace knowCore;

struct UriManager::Private : public QSharedData
{
  Uri base;
  QHash<QString, Uri> prefixes;
};

UriManager::UriManager() : d(new Private) {}

UriManager::UriManager(const Uri& _base) : d(new Private) { d->base = _base; }

UriManager::UriManager(const UriManager& _rhs) : d(_rhs.d) {}

UriManager& UriManager::operator=(const UriManager& _rhs)
{
  d = _rhs.d;
  return *this;
}

UriManager::~UriManager() {}

void UriManager::addPrefix(const QString& _prefix, const Uri& _url) { d->prefixes[_prefix] = _url; }

void UriManager::addPrefix(const QString& _prefix, const QString& _url)
{
  addPrefix(_prefix, d->base.resolved(_url));
}

bool UriManager::hasPrefix(const QString& _prefix) const
{
  return d->prefixes.contains(_prefix) or _prefix.isEmpty();
}

Uri UriManager::uri(const QString& _prefix) const { return d->prefixes.value(_prefix, d->base); }

Uri UriManager::uri(const QString& _prefix, const QString& _suffix) const
{
  Uri u = uri(_prefix);
  return Uri((QString)u + _suffix);
}

void UriManager::setBase(const Uri& _url) { d->base = _url; }

Uri UriManager::base() const { return d->base; }

bool UriManager::curify(const Uri& _uri, Curie* _curie) const
{
  QString t = _uri;
  for(QHash<QString, Uri>::const_iterator it = d->prefixes.begin(); it != d->prefixes.end(); ++it)
  {
    QString n = it.value();
    if(t.startsWith(n))
    {
      QString l = t.right(t.length() - n.length());
      *_curie = Curie(it.key(), l);
      return true;
    }
  }
  return false;
}

QHash<QString, Uri> UriManager::prefixes() const { return d->prefixes; }
