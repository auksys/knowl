#pragma once

#include <QSharedDataPointer>

#include <knowCore/Forward.h>

namespace knowCore
{
  class UriManager
  {
  public:
    UriManager();
    UriManager(const Uri& _base);
    UriManager(const UriManager& _rhs);
    UriManager& operator=(const UriManager& _rhs);
    ~UriManager();
    void addPrefix(const QString& _prefix, const Uri& _url);
    void addPrefix(const QString& _prefix, const QString& _url);
    bool hasPrefix(const QString& _prefix) const;
    Uri uri(const QString& _prefix) const;
    Uri uri(const QString& _prefix, const QString& _suffix) const;
    Uri base() const;
    void setBase(const Uri& _url);
    bool curify(const Uri& _uri, Curie* _curie) const;
    QHash<QString, Uri> prefixes() const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
} // namespace knowCore
