#define KNOWCORE_ENABLE_ONTOLOGY_URIS_DEFINITION
#include "Uris.h"

#include <QString>

#include <knowCore/Uri.h>

#include "askcore_agent.h"
#include "askcore_dataset.h"
#include "askcore_datatype.h"
#include "askcore_db.h"
#include "askcore_declaration.h"
#include "askcore_focus_node.h"
#include "askcore_graph.h"
#include "askcore_thing.h"
#include "askcore_unit.h"
#include "dc.h"
#include "delta.h"
#include "dul.h"
#include "foaf.h"
#include "mf.h"
#include "qudt.h"
#include "rdf.h"
#include "rdfs.h"
#include "rs.h"
#include "ssn.h"
#include "time.h"
#include "un.h"
#include "unit.h"
#include "xsd.h"
#include "xsi.h"
