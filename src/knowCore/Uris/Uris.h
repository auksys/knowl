#pragma once

#include <knowCore/Uri.h>
#include <knowCore/UriManager.h>
#include <knowCore/UrisRegistry.h>
#include <knowCore/Value.h>

namespace knowCore::Uris
{
  template<typename _T_>
  struct IsUriDefinition : std::false_type
  {
  };

  template<typename _T1_, typename _T2_>
    requires(IsUriDefinitionV<_T1_> and IsUriDefinitionV<_T2_>)
  inline knowCore::Uri select(bool _cond, const _T1_& _t1, const _T2_& _t2)
  {
    return _cond ? knowCore::Uri(_t1) : knowCore::Uri(_t2);
  }
} // namespace knowCore::Uris

template<typename _T_, typename Char>
  requires knowCore::Uris::IsUriDefinition<_T_>::value
struct std::formatter<_T_, Char> : std::formatter<knowCore::Uri>
{
};

// On a side node, the operator from "UriDefinition" should be assumed to be "static".
// They might be called before the UriDefinition is initialised. Which works fine because they don't
// depend on anything. (even though it is clearly ugly)

#define __KNOWCORE_DECLARE_URI_C(_NAME_, _URL_)                                                    \
  class _NAME_##UriDefinition                                                                      \
  {                                                                                                \
  public:                                                                                          \
    inline operator knowCore::Uri() const                                                          \
    {                                                                                              \
      static knowCore::Uri uri = knowCore::Uri(QStringLiteral(_URL_));                             \
      return uri;                                                                                  \
    }                                                                                              \
    inline operator QString() const { return knowCore::Uri(*this); }                               \
  };                                                                                               \
  static _NAME_##UriDefinition _NAME_;

#define __KNOWCORE_DECLARE_URI_2(_NS_, _URL_, _NAME_, _LABEL_)                                     \
  __KNOWCORE_DECLARE_URI_C(_NAME_, _URL_ _LABEL_)

#define __KNOWCORE_DECLARE_URI_1(_NS_, _URL_, _NAME_)                                              \
  __KNOWCORE_DECLARE_URI_C(_NAME_, _URL_ #_NAME_)

#define KNOWCORE_DECLARE_URI_CHOOSER(A, B, C, ...)                                                 \
  __KNOWCORE_GET_2ND_ARG(__VA_OPT__(, ) __VA_ARGS__, __KNOWCORE_DECLARE_URI_2,                     \
                         __KNOWCORE_DECLARE_URI_1, )

#define __KNOWCORE_DECLARE_URI(...) KNOWCORE_DECLARE_URI_CHOOSER(__VA_ARGS__)(__VA_ARGS__)

#define __KNOWCORE_DECLARE_URI_TRAITS_C(_NS_, _NAME_)                                              \
  template<>                                                                                       \
  struct knowCore::Uris::IsUriDefinition<_NS_::_NAME_##UriDefinition> : std::true_type             \
  {                                                                                                \
  };                                                                                               \
  template<>                                                                                       \
  inline QVariant QVariant::fromValue<_NS_::_NAME_##UriDefinition>(                                \
    const _NS_::_NAME_##UriDefinition& _ud)                                                        \
  {                                                                                                \
    return QVariant::fromValue(knowCore::Uri(_ud));                                                \
  }                                                                                                \
  template<>                                                                                       \
  inline knowCore::Value knowCore::Value::fromValue<_NS_::_NAME_##UriDefinition>(                  \
    const _NS_::_NAME_##UriDefinition& _ud)                                                        \
  {                                                                                                \
    return knowCore::Value::fromValue(knowCore::Uri(_ud));                                         \
  }

#define __KNOWCORE_DECLARE_URI_TRAITS_2(_NS_, _URL_, _NAME_, _LABEL_)                              \
  __KNOWCORE_DECLARE_URI_TRAITS_C(_NS_, _NAME_)

#define __KNOWCORE_DECLARE_URI_TRAITS_1(_NS_, _URL_, _NAME_)                                       \
  __KNOWCORE_DECLARE_URI_TRAITS_C(_NS_, _NAME_)

#define KNOWCORE_DECLARE_URI_TRAITS_CHOOSER(A, B, C, ...)                                          \
  __KNOWCORE_GET_2ND_ARG(__VA_OPT__(, ) __VA_ARGS__, __KNOWCORE_DECLARE_URI_TRAITS_2,              \
                         __KNOWCORE_DECLARE_URI_TRAITS_1, )

#define __KNOWCORE_DECLARE_URI_TRAITS(...)                                                         \
  KNOWCORE_DECLARE_URI_TRAITS_CHOOSER(__VA_ARGS__)(__VA_ARGS__)

#define __KNOWCORE_DEFINE_URI_C(_NS_, _NAME_) _NS_::_NAME_##UriDefinition _NS_::_NAME_;

#define __KNOWCORE_DEFINE_URI_2(_NS_, _URL_, _NAME_, _LABEL_) __KNOWCORE_DEFINE_URI_C(_NS_, _NAME_)

#define __KNOWCORE_DEFINE_URI_1(_NS_, _URL_, _NAME_) __KNOWCORE_DEFINE_URI_C(_NS_, _NAME_)

#define KNOWCORE_DEFINE_URI_CHOOSER(A, B, C, ...)                                                  \
  __KNOWCORE_GET_2ND_ARG(__VA_OPT__(, ) __VA_ARGS__, __KNOWCORE_DEFINE_URI_2,                      \
                         __KNOWCORE_DEFINE_URI_1, )

#define __KNOWCORE_DEFINE_URI(...) KNOWCORE_DEFINE_URI_CHOOSER(__VA_ARGS__)(__VA_ARGS__)

#define __KNOWCORE_DECLARE_ONTOLOGY_URIS(_CPP_NAMESPACE_, _NSE_, _CNS_, _URL_, _PREFIX_)           \
  namespace _CPP_NAMESPACE_                                                                        \
  {                                                                                                \
    class _CNS_                                                                                    \
    {                                                                                              \
      _CNS_();                                                                                     \
      ~_CNS_();                                                                                    \
    public:                                                                                        \
      class UriDefinition                                                                          \
      {                                                                                            \
      public:                                                                                      \
        inline operator knowCore::Uri() const                                                      \
        {                                                                                          \
          static knowCore::Uri uri = knowCore::Uri(QStringLiteral(_URL_));                         \
          return uri;                                                                              \
        }                                                                                          \
        inline operator std::optional<knowCore::Uri>() const                                       \
        {                                                                                          \
          static knowCore::Uri uri = knowCore::Uri(QStringLiteral(_URL_));                         \
          return uri;                                                                              \
        }                                                                                          \
        inline operator QString() const { return knowCore::Uri(*this); }                           \
        knowCore::Uri resolved(const knowCore::Uri& _url) const                                    \
        {                                                                                          \
          return knowCore::Uri(*this).resolved(_url);                                              \
        }                                                                                          \
      };                                                                                           \
      static const UriDefinition base;                                                             \
      class PrefixDefinition                                                                       \
      {                                                                                            \
      public:                                                                                      \
        inline operator QString() const { return QString(_PREFIX_); }                              \
      };                                                                                           \
      static const PrefixDefinition prefix;                                                        \
      _NSE_(__KNOWCORE_DECLARE_URI, _CNS_, _URL_)                                                  \
    };                                                                                             \
  }                                                                                                \
  _NSE_(__KNOWCORE_DECLARE_URI_TRAITS, _CPP_NAMESPACE_::_CNS_, _URL_)

#ifdef KNOWCORE_ENABLE_ONTOLOGY_URIS_DEFINITION

#define __KNOWCORE_ONTOLOGY_URIS(_CPP_NAMESPACE_, _NSE_, _CNS_, _URL_, _PREFIX_)                   \
  __KNOWCORE_DECLARE_ONTOLOGY_URIS(_CPP_NAMESPACE_, _NSE_, _CNS_, _URL_, _PREFIX_)                 \
  namespace _CPP_NAMESPACE_                                                                        \
  {                                                                                                \
    _NSE_(__KNOWCORE_DEFINE_URI, _CNS_, _URL_)                                                     \
    const _CNS_::UriDefinition _CNS_::base;                                                        \
    const _CNS_::PrefixDefinition _CNS_::prefix;                                                   \
    struct _CNS_##_Registration                                                                    \
    {                                                                                              \
      _CNS_##_Registration()                                                                       \
      {                                                                                            \
        knowCore::UrisRegistry::manager()->addPrefix(_CNS_::prefix, knowCore::Uri(_CNS_::base));   \
      }                                                                                            \
    };                                                                                             \
    _CNS_##_Registration _CNS_##_registration;                                                     \
  }

#else

#define __KNOWCORE_ONTOLOGY_URIS(_CPP_NAMESPACE_, _NSE_, _CNS_, _URL_, _PREFIX_)                   \
  __KNOWCORE_DECLARE_ONTOLOGY_URIS(_CPP_NAMESPACE_, _NSE_, _CNS_, _URL_, _PREFIX_)

#endif

#define _KNOWCORE_ONTOLOGY_URIS_2(_CPP_NAMESPACE_, _NSE_, _CNS_, _URL_, _PREFIX_)                  \
  __KNOWCORE_ONTOLOGY_URIS(_CPP_NAMESPACE_, _NSE_, _CNS_, _URL_, _PREFIX_)
#define _KNOWCORE_ONTOLOGY_URIS_1(_CPP_NAMESPACE_, _NSE_, _CNS_, _URL_)                            \
  __KNOWCORE_ONTOLOGY_URIS(_CPP_NAMESPACE_, _NSE_, _CNS_, _URL_, #_CNS_)

#define _KNOWCORE_ONTOLOGY_URIS_CHOOSER(A, B, C, D, ...)                                           \
  __KNOWCORE_GET_2ND_ARG(__VA_OPT__(, ) __VA_ARGS__, _KNOWCORE_ONTOLOGY_URIS_2,                    \
                         _KNOWCORE_ONTOLOGY_URIS_1, )

#define ___KNOWCORE_ONTOLOGY_URIS(...) _KNOWCORE_ONTOLOGY_URIS_CHOOSER(__VA_ARGS__)(__VA_ARGS__)

#define KNOWCORE_ONTOLOGY_URIS(_CPP_NAMESPACE_, _NSE_, _CNS_, _URL_, ...)                          \
  ___KNOWCORE_ONTOLOGY_URIS(_CPP_NAMESPACE_, _NSE_, _CNS_, _URL_ __VA_OPT__(, ) __VA_ARGS__)
