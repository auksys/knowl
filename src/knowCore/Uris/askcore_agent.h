#include "Uris.h"

#define KNOWCORE_ASKCORE_AGENT_URIS(F, ...)                                                        \
  F(__VA_ARGS__, agent)                                                                            \
  F(__VA_ARGS__, agent_type)                                                                       \
  F(__VA_ARGS__, agent_uri)                                                                        \
  F(__VA_ARGS__, collection)                                                                       \
  F(__VA_ARGS__, frame)                                                                            \
  F(__VA_ARGS__, self)                                                                             \
  F(__VA_ARGS__, uav)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_AGENT_URIS, askcore_agent,
                       "http://askco.re/agent#")

#define KNOWCORE_ASKCORE_STREAM_URIS(F, ...)                                                       \
  F(__VA_ARGS__, content_type)                                                                     \
  F(__VA_ARGS__, data_type)                                                                        \
  F(__VA_ARGS__, has_stream)                                                                       \
  F(__VA_ARGS__, identifier)                                                                       \
  F(__VA_ARGS__, stream)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_STREAM_URIS, askcore_stream,
                       "http://askco.re/agent/stream#")
