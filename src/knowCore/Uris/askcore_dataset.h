#include "Uris.h"

#define KDBSENSING_ASKCORE_DATASET_URIS(F, ...)                                                    \
  F(__VA_ARGS__, collection)                                                                       \
  F(__VA_ARGS__, content_type)                                                                     \
  F(__VA_ARGS__, dataset)                                                                          \
  F(__VA_ARGS__, dataset_include)                                                                  \
  F(__VA_ARGS__, dataset_status)                                                                   \
  F(__VA_ARGS__, dataset_type)                                                                     \
  F(__VA_ARGS__, dataset_uri)                                                                      \
  F(__VA_ARGS__, document_of_datasets)                                                             \
  F(__VA_ARGS__, completed)                                                                        \
  F(__VA_ARGS__, has_dataset)                                                                      \
  F(__VA_ARGS__, in_progress)                                                                      \
  F(__VA_ARGS__, in_preparation)                                                                   \
  F(__VA_ARGS__, point_cloud_dataset)                                                              \
  F(__VA_ARGS__, scheduled)                                                                        \
  F(__VA_ARGS__, unknown)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KDBSENSING_ASKCORE_DATASET_URIS, askcore_dataset,
                       "http://askco.re/dataset#")
