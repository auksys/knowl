#pragma once

#include "Uris.h"

#define KNOWCORE_ASKCORE_DATATYPE_URIS(F, ...)                                                     \
  F(__VA_ARGS__, binarydata)                                                                       \
  F(__VA_ARGS__, binarydatalist)                                                                   \
  F(__VA_ARGS__, blanknode)                                                                        \
  F(__VA_ARGS__, bytesarray)                                                                       \
  F(__VA_ARGS__, colored_octree)                                                                   \
  F(__VA_ARGS__, doublelist)                                                                       \
  F(__VA_ARGS__, decimallist)                                                                      \
  F(__VA_ARGS__, decimalRange)                                                                     \
  F(__VA_ARGS__, empty)                                                                            \
  F(__VA_ARGS__, floatlist)                                                                        \
  F(__VA_ARGS__, geopose)                                                                          \
  F(__VA_ARGS__, integer8list)                                                                     \
  F(__VA_ARGS__, integer16list)                                                                    \
  F(__VA_ARGS__, integer32list)                                                                    \
  F(__VA_ARGS__, integer64list)                                                                    \
  F(__VA_ARGS__, unsignedinteger8list)                                                             \
  F(__VA_ARGS__, unsignedinteger16list)                                                            \
  F(__VA_ARGS__, unsignedinteger32list)                                                            \
  F(__VA_ARGS__, unsignedinteger64list)                                                            \
  F(__VA_ARGS__, integer8array)                                                                    \
  F(__VA_ARGS__, integer16array)                                                                   \
  F(__VA_ARGS__, integer32array)                                                                   \
  F(__VA_ARGS__, integer64array)                                                                   \
  F(__VA_ARGS__, unsignedinteger8array)                                                            \
  F(__VA_ARGS__, unsignedinteger16array)                                                           \
  F(__VA_ARGS__, unsignedinteger32array)                                                           \
  F(__VA_ARGS__, unsignedinteger64array)                                                           \
  F(__VA_ARGS__, floatarray)                                                                       \
  F(__VA_ARGS__, doublearray)                                                                      \
  F(__VA_ARGS__, uriList)                                                                          \
  F(__VA_ARGS__, image)                                                                            \
  F(__VA_ARGS__, json)                                                                             \
  F(__VA_ARGS__, quaternion)                                                                       \
  F(__VA_ARGS__, literal)                                                                          \
  F(__VA_ARGS__, object)                                                                           \
  F(__VA_ARGS__, octree)                                                                           \
  F(__VA_ARGS__, point_cloud)                                                                      \
  F(__VA_ARGS__, pose)                                                                             \
  F(__VA_ARGS__, quantityDecimal)                                                                  \
  F(__VA_ARGS__, quantityDecimalRange)                                                             \
  F(__VA_ARGS__, range)                                                                            \
  F(__VA_ARGS__, salientRegion)                                                                    \
  F(__VA_ARGS__, stringarray)                                                                      \
  F(__VA_ARGS__, stringlist)                                                                       \
  F(__VA_ARGS__, triple)                                                                           \
  F(__VA_ARGS__, value)                                                                            \
  F(__VA_ARGS__, valuehash)                                                                        \
  F(__VA_ARGS__, valuelist)                                                                        \
  F(__VA_ARGS__, vector2d)                                                                         \
  F(__VA_ARGS__, vector3d)                                                                         \
  F(__VA_ARGS__, vector4d)                                                                         \
  F(__VA_ARGS__, vector5d)                                                                         \
  F(__VA_ARGS__, vector6d)                                                                         \
  F(__VA_ARGS__, vector7d)                                                                         \
  F(__VA_ARGS__, vector8d)                                                                         \
  F(__VA_ARGS__, vector9d)                                                                         \
  F(__VA_ARGS__, vector2dlist)                                                                     \
  F(__VA_ARGS__, vector3dlist)                                                                     \
  F(__VA_ARGS__, vector4dlist)                                                                     \
  F(__VA_ARGS__, vector5dlist)                                                                     \
  F(__VA_ARGS__, vector6dlist)                                                                     \
  F(__VA_ARGS__, vector7dlist)                                                                     \
  F(__VA_ARGS__, vector8dlist)                                                                     \
  F(__VA_ARGS__, vector9dlist)                                                                     \
  F(__VA_ARGS__, vector2f)                                                                         \
  F(__VA_ARGS__, vector3f)                                                                         \
  F(__VA_ARGS__, vector4f)                                                                         \
  F(__VA_ARGS__, vector5f)                                                                         \
  F(__VA_ARGS__, vector6f)                                                                         \
  F(__VA_ARGS__, vector7f)                                                                         \
  F(__VA_ARGS__, vector8f)                                                                         \
  F(__VA_ARGS__, vector9f)                                                                         \
  F(__VA_ARGS__, vector2flist)                                                                     \
  F(__VA_ARGS__, vector3flist)                                                                     \
  F(__VA_ARGS__, vector4flist)                                                                     \
  F(__VA_ARGS__, vector5flist)                                                                     \
  F(__VA_ARGS__, vector6flist)                                                                     \
  F(__VA_ARGS__, vector7flist)                                                                     \
  F(__VA_ARGS__, vector8flist)                                                                     \
  F(__VA_ARGS__, vector9flist)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_DATATYPE_URIS, askcore_datatype,
                       "http://askco.re/datatype#")
