#pragma once

#include "Uris.h"

// This is ued to represent blank node (combined with a UUID)
#define KNOWCORE_ASKCORE_BLANK_URIS(F, ...)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_BLANK_URIS, askcore_db_blank,
                       "http://askco.re/db/blank#")

// This is used to create a unique URI for a server (combining it with a UUID)
#define KNOWCORE_ASKCORE_SERVER_URIS(F, ...)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_SERVER_URIS, askcore_db_server,
                       "http://askco.re/db/server#")

#define KNOWCORE_ASKCORE_FUNCTIONS_URIS(F, ...)                                                    \
  F(__VA_ARGS__, bound)                                                                            \
  F(__VA_ARGS__, coalesce)                                                                         \
  F(__VA_ARGS__, count)                                                                            \
  F(__VA_ARGS__, contains)                                                                         \
  F(__VA_ARGS__, datatype)                                                                         \
  F(__VA_ARGS__, extendedLangMatches)                                                              \
  F(__VA_ARGS__, iri)                                                                              \
  F(__VA_ARGS__, isBlank)                                                                          \
  F(__VA_ARGS__, isLiteral)                                                                        \
  F(__VA_ARGS__, isURI)                                                                            \
  F(__VA_ARGS__, isIRI)                                                                            \
  F(__VA_ARGS__, lang)                                                                             \
  F(__VA_ARGS__, langMatches)                                                                      \
  F(__VA_ARGS__, sameTerm)                                                                         \
  F(__VA_ARGS__, str)                                                                              \
  F(__VA_ARGS__, strends)                                                                          \
  F(__VA_ARGS__, strstarts)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_FUNCTIONS_URIS, askcore_sparql_functions,
                       "http://askco.re/sparql/functions#")

#define KNOWCORE_ASKCORE_FUNCTIONS_EXTRA_URIS(F, ...) F(__VA_ARGS__, convertQuantityValue)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_FUNCTIONS_EXTRA_URIS,
                       askcore_sparql_functions_extra, "http://askco.re/sparql/functions_extra#")

#define KNOWCORE_ASKCORE_INFO_URIS(F, ...)                                                         \
  F(__VA_ARGS__, self)                                                                             \
  F(__VA_ARGS__, uuid)                                                                             \
  F(__VA_ARGS__, hasSignature)                                                                     \
  F(__VA_ARGS__, RSA)                                                                              \
  F(__VA_ARGS__, hasUUID)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_INFO_URIS, askcore_db_info,
                       "http://askco.re/db/info#")

#define KNOWCORE_ASKCORE_DB_RDF_URIS(F, ...)                                                       \
  F(__VA_ARGS__, dataset)                                                                          \
  F(__VA_ARGS__, triple_store)                                                                     \
  F(__VA_ARGS__, union_, "union")                                                                  \
  F(__VA_ARGS__, view)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_DB_RDF_URIS, askcore_db_rdf,
                       "http://askco.re/db/rdf#")

#define KNOWCORE_ASKCORE_DB_QUERY_LANGUAGE_URIS(F, ...)                                            \
  F(__VA_ARGS__, kdQL)                                                                             \
  F(__VA_ARGS__, krQL)                                                                             \
  F(__VA_ARGS__, pl_sparql, "pl/sparql")                                                           \
  F(__VA_ARGS__, scQL)                                                                             \
  F(__VA_ARGS__, SPARQL)                                                                           \
  F(__VA_ARGS__, SQL)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_DB_QUERY_LANGUAGE_URIS,
                       askcore_db_query_language, "http://askco.re/db/query_language#")

#define KNOWCORE_ASKCORE_DB_QUERIES_URIS(F, ...)                                                   \
  F(__VA_ARGS__, binding)                                                                          \
  F(__VA_ARGS__, graph)                                                                            \
  F(__VA_ARGS__, optional)                                                                         \
  F(__VA_ARGS__, query)                                                                            \
  F(__VA_ARGS__, result)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_DB_QUERIES_URIS, askcore_db_queries,
                       "http://askco.re/db/queries#")
