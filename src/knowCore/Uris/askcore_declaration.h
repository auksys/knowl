#include "Uris.h"

#define KDBSENSING_ASKCORE_DECLARATION_URIS(F, ...)                                                \
  F(__VA_ARGS__, constant)                                                                         \
  F(__VA_ARGS__, default_value)                                                                    \
  F(__VA_ARGS__, focus_node_shape)                                                                 \
  F(__VA_ARGS__, property_map_shape)                                                               \
  F(__VA_ARGS__, quantiy_value_shape)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KDBSENSING_ASKCORE_DECLARATION_URIS, askcore_declaration,
                       "http://askco.re/declaration#")
