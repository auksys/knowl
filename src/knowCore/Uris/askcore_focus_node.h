#include "Uris.h"

#define KNOWCORE_ASKCORE_FOCUS_NODE_URIS(F, ...)                                                   \
  F(__VA_ARGS__, collection)                                                                       \
  F(__VA_ARGS__, collections_collection)                                                           \
  F(__VA_ARGS__, collection_type)                                                                  \
  F(__VA_ARGS__, contains)                                                                         \
  F(__VA_ARGS__, dataset_type)                                                                     \
  F(__VA_ARGS__, focus_node)                                                                       \
  F(__VA_ARGS__, focus_nodes_collection)                                                           \
  F(__VA_ARGS__, type)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_FOCUS_NODE_URIS, askcore_focus_node,
                       "http://askco.re/focus_node#")
