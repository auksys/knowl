#include "Uris.h"

/**
 * \ref all all RDF graphs
 * \ref all_focus_nodes is a union of all the RDF document with datasets
 * \ref local_datasets is a graph with local datasets (that graph should not be shared with other
 * platform, and can be used to store private information) \ref default_ reference the default graph
 * in a repository \ref info can be used by the server implementation to store meta-information
 * about the server
 */
#define KNOWCORE_ASKCORE_GRAPHS_URIS(F, ...)                                                       \
  F(__VA_ARGS__, agents_collections)                                                               \
  F(__VA_ARGS__, all)                                                                              \
  F(__VA_ARGS__, all_agents)                                                                       \
  F(__VA_ARGS__, all_datasets)                                                                     \
  F(__VA_ARGS__, all_simple_features)                                                              \
  F(__VA_ARGS__, all_focus_nodes)                                                                  \
  F(__VA_ARGS__, all_missions)                                                                     \
  F(__VA_ARGS__, all_salient_regions)                                                              \
  F(__VA_ARGS__, collections)                                                                      \
  F(__VA_ARGS__, datasets_collections)                                                             \
  F(__VA_ARGS__, private_agents)                                                                   \
  F(__VA_ARGS__, private_datasets)                                                                 \
  F(__VA_ARGS__, default_, "default")                                                              \
  F(__VA_ARGS__, info)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_GRAPHS_URIS, askcore_graph,
                       "http://askco.re/graph#")
