#include "Uris.h"

#define KNOWCORE_ASKCORE_THING_URIS(F, ...)                                                        \
  F(__VA_ARGS__, building)                                                                         \
  F(__VA_ARGS__, box)                                                                              \
  F(__VA_ARGS__, depot)                                                                            \
  F(__VA_ARGS__, medication)                                                                       \
  F(__VA_ARGS__, human)                                                                            \
  F(__VA_ARGS__, robot)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_THING_URIS, askcore_thing,
                       "http://askco.re/thing#")

#define KNOWCORE_ASKCORE_THING_PROPERTY_URIS(F, ...)                                               \
  F(__VA_ARGS__, contains)                                                                         \
  F(__VA_ARGS__, needs)                                                                            \
  F(__VA_ARGS__, visible)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_THING_PROPERTY_URIS, askcore_thing_property,
                       "http://askco.re/thing/property#")
