#include "Uris.h"

#define KNOWCORE_ASKCORE_UNIT_URIS(F, ...)                                                         \
  F(__VA_ARGS__, no_unit)                                                                          \
  F(__VA_ARGS__, point)                                                                            \
  F(__VA_ARGS__, point_per_m2)                                                                     \
  F(__VA_ARGS__, point_per_millim2)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_UNIT_URIS, askcore_unit,
                       "http://askco.re/unit#")
