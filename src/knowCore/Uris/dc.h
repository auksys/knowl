#include "Uris.h"

#define KNOWCORE_DC_URIS(F, ...) F(__VA_ARGS__, description)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_DC_URIS, dc, "http://purl.org/dc/elements/1.1/")
