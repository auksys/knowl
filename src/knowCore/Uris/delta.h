#include "Uris.h"

#define KNOWCORE_DIFF_URIS(F, ...)                                                                 \
  F(__VA_ARGS__, insertion)                                                                        \
  F(__VA_ARGS__, deleteion)                                                                        \
  F(__VA_ARGS__, replacement)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_DIFF_URIS, diff, "http://www.w3.org/2004/delta#")

#define KNOWCORE_DIFF_EXT_URIS(F, ...) F(__VA_ARGS__, context)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_DIFF_EXT_URIS, diff_ext,
                       "http://www.ida.liu.se/divisions/aiics/diff_ext#")
