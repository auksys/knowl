#include "Uris.h"

#define KNOWCORE_DUL_URIS(F, ...)                                                                  \
  F(__VA_ARGS__, hasDataValue)                                                                     \
  F(__VA_ARGS__, isClassifiedBy)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_DUL_URIS, dul,
                       "http://www.loa-cnr.it/ontologies/DUL.owl#")
