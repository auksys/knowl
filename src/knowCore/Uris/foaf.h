#include "Uris.h"

#define KNOWCORE_FOAF_URIS(F, ...) F(__VA_ARGS__, name)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_FOAF_URIS, foaf, "http://xmlns.com/foaf/0.1/")
