#include "Uris.h"

#define KNOWCORE_MF_URIS(F, ...)                                                                   \
  F(__VA_ARGS__, Manifest)                                                                         \
  F(__VA_ARGS__, QueryEvaluationTest)                                                              \
  F(__VA_ARGS__, PositiveSyntaxTest)                                                               \
  F(__VA_ARGS__, PositiveUpdateSyntaxTest11)                                                       \
  F(__VA_ARGS__, NegativeSyntaxTest)                                                               \
  F(__VA_ARGS__, NegativeSyntaxTest11)                                                             \
  F(__VA_ARGS__, NegativeUpdateSyntaxTest11)                                                       \
  F(__VA_ARGS__, action)                                                                           \
  F(__VA_ARGS__, include)                                                                          \
  F(__VA_ARGS__, result)                                                                           \
  F(__VA_ARGS__, name)                                                                             \
  F(__VA_ARGS__, entries)                                                                          \
  F(__VA_ARGS__, UpdateEvaluationTest)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_MF_URIS, mf,
                       "http://www.w3.org/2001/sw/DataAccess/tests/test-manifest#")
