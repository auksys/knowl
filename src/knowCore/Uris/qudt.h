#pragma once

#include "Uris.h"

#define KNOWCORE_QUDT_URIS(F, ...)                                                                 \
  F(__VA_ARGS__, unit)                                                                             \
  F(__VA_ARGS__, value)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_QUDT_URIS, qudt, "http://qudt.org/schema/qudt/")
