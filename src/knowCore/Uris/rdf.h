#include "Uris.h"

#define KNOWCORE_RDF_URIS(F, ...)                                                                  \
  F(__VA_ARGS__, a, "type")                                                                        \
  F(__VA_ARGS__, first)                                                                            \
  F(__VA_ARGS__, langString)                                                                       \
  F(__VA_ARGS__, rest)                                                                             \
  F(__VA_ARGS__, nil)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_RDF_URIS, rdf,
                       "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
