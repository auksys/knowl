#include "Uris.h"

#define KNOWCORE_RDFS_URIS(F, ...)                                                                 \
  F(__VA_ARGS__, label)                                                                            \
  F(__VA_ARGS__, comment)                                                                          \
  F(__VA_ARGS__, seeAlso)                                                                          \
  F(__VA_ARGS__, subClassOf)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_RDFS_URIS, rdfs,
                       "http://www.w3.org/2000/01/rdf-schema#")
