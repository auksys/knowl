#include "Uris.h"

#define KNOWCORE_RS_URIS(F, ...)                                                                   \
  F(__VA_ARGS__, ResultSet)                                                                        \
  F(__VA_ARGS__, value)                                                                            \
  F(__VA_ARGS__, variable)                                                                         \
  F(__VA_ARGS__, binding)                                                                          \
  F(__VA_ARGS__, solution)                                                                         \
  F(__VA_ARGS__, resultVariable)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_RS_URIS, rs,
                       "http://www.w3.org/2001/sw/DataAccess/tests/result-set#")
