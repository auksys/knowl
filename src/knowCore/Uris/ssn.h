#include "Uris.h"

#define KNOWCORE_SSN_URIS(F, ...)                                                                  \
  F(__VA_ARGS__, SensorOutput)                                                                     \
  F(__VA_ARGS__, hasProperty)                                                                      \
  F(__VA_ARGS__, hasValue)                                                                         \
  F(__VA_ARGS__, observationResult)                                                                \
  F(__VA_ARGS__, OperatingProperty)                                                                \
  F(__VA_ARGS__, observationSamplingTime)                                                          \
  F(__VA_ARGS__, observedBy)                                                                       \
  F(__VA_ARGS__, Property)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_SSN_URIS, ssn, "http://www.w3.org/ns/ssn/")
