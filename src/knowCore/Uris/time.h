#include "Uris.h"

#define KNOWCORE_ASKCORE_TIME_URIS(F, ...)                                                         \
  F(__VA_ARGS__, hasBeginning)                                                                     \
  F(__VA_ARGS__, hasTime)                                                                          \
  F(__VA_ARGS__, hasEnd)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_ASKCORE_TIME_URIS, time,
                       "http://www.w3.org/2006/time#")
