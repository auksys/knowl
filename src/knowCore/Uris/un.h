#include "Uris.h"

#define KNOWCORE_UN_URIS(F, ...)                                                                   \
  F(__VA_ARGS__, m)                                                                                \
  F(__VA_ARGS__, kg)                                                                               \
  F(__VA_ARGS__, radian)                                                                           \
  F(__VA_ARGS__, s)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_UN_URIS, un, "http://www.w3.org/2007/ont/unit#")
