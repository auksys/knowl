#include "Uris.h"

#define KNOWCORE_UNIT_URIS(F, ...)                                                                 \
  F(__VA_ARGS__, GM)                                                                               \
  F(__VA_ARGS__, KiloW)                                                                            \
  F(__VA_ARGS__, M)                                                                                \
  F(__VA_ARGS__, M_PER_SEC, "M-PER-SEC")                                                           \
  F(__VA_ARGS__, M2)                                                                               \
  F(__VA_ARGS__, MilliM)                                                                           \
  F(__VA_ARGS__, MilliM_PER_SEC, "MilliM-PER-SEC")                                                 \
  F(__VA_ARGS__, MilliM2)                                                                          \
  F(__VA_ARGS__, MilliSEC)                                                                         \
  F(__VA_ARGS__, NanoSEC)                                                                          \
  F(__VA_ARGS__, SEC)                                                                              \
  F(__VA_ARGS__, W)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_UNIT_URIS, unit, "http://qudt.org/vocab/unit/")
