#pragma once

#include <knowCore/Uri.h>

#include "Uris.h"
// Primitive datatypes are defined at
// https://www.w3.org/TR/2004/REC-xmlschema-2-20041028/#built-in-primitive-datatypes

#define KNOWCORE_XSD_URIS(F, ...)                                                                  \
  F(__VA_ARGS__, anyURI)                                                                           \
  F(__VA_ARGS__, atomic)                                                                           \
  F(__VA_ARGS__, boolean)                                                                          \
  F(__VA_ARGS__, dateTime)                                                                         \
  F(__VA_ARGS__, decimal)                                                                          \
  F(__VA_ARGS__, float32, "float")                                                                 \
  F(__VA_ARGS__, float64, "double")                                                                \
  F(__VA_ARGS__, integer)                                                                          \
  F(__VA_ARGS__, integer8, "byte")                                                                 \
  F(__VA_ARGS__, integer16, "short")                                                               \
  F(__VA_ARGS__, integer32, "int")                                                                 \
  F(__VA_ARGS__, integer64, "long")                                                                \
  F(__VA_ARGS__, language)                                                                         \
  F(__VA_ARGS__, list)                                                                             \
  F(__VA_ARGS__, nonNegativeInteger)                                                               \
  F(__VA_ARGS__, unsignedInteger8, "unsignedByte")                                                 \
  F(__VA_ARGS__, unsignedInteger16, "unsignedShort")                                               \
  F(__VA_ARGS__, unsignedInteger32, "unsignedInt")                                                 \
  F(__VA_ARGS__, unsignedInteger64, "unsignedLong")                                                \
  F(__VA_ARGS__, string)                                                                           \
  F(__VA_ARGS__, union_, "union")                                                                  \
  F(__VA_ARGS__, variety)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_XSD_URIS, xsd, "http://www.w3.org/2001/XMLSchema#")

namespace knowCore::Uris
{
  inline bool isNumericType(const knowCore::Uri& _uri)
  {
    return _uri == xsd::decimal or _uri == xsd::integer or _uri == xsd::integer8
           or _uri == xsd::integer16 or _uri == xsd::integer32 or _uri == xsd::integer64
           or _uri == xsd::nonNegativeInteger or _uri == xsd::unsignedInteger8
           or _uri == xsd::unsignedInteger16 or _uri == xsd::unsignedInteger32
           or _uri == xsd::unsignedInteger64 or _uri == xsd::float32 or _uri == xsd::float64;
  }
} // namespace knowCore::Uris
