#include "Uris.h"

#define KNOWCORE_XSI_URIS(F, ...)                                                                  \
  F(__VA_ARGS__, ref)                                                                              \
  F(__VA_ARGS__, type)

KNOWCORE_ONTOLOGY_URIS(knowCore::Uris, KNOWCORE_XSI_URIS, xsi,
                       "http://www.w3.org/2001/XMLSchema-instance#")
