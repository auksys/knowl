#include "UrisRegistry.h"

#include <knowCore/UriManager.h>

using namespace knowCore;

struct UrisRegistry::Private
{
  UriManager manager;
  static Private* s_instance;
  static Private* instance();
};

UrisRegistry::Private* UrisRegistry::Private::s_instance = nullptr;

UrisRegistry::Private* UrisRegistry::Private::instance()
{
  if(not s_instance)
  {
    s_instance = new Private;
  }
  return s_instance;
}

UriManager* UrisRegistry::manager() { return &Private::instance()->manager; }
