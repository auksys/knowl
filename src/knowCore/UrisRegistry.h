#pragma once

#include <knowCore/Forward.h>

namespace knowCore
{
  class UrisRegistry
  {
    UrisRegistry();
    ~UrisRegistry();
  public:
    static UriManager* manager();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowCore
