#include "Value.h"

#include <QCborMap>

#include "MetaTypeImplementation.h"
#include "MetaType_p.h"
#include "ValueHash.h"
#include "ValueList.h"

#include "Uris/askcore_datatype.h"

using namespace knowCore;

struct Value::Private : public QSharedData
{
  Private() {}
  Private(const Private& _rhs) : QSharedData(_rhs)
  {
    datatype = _rhs.datatype;
    if(_rhs.definition)
    {
      definition = _rhs.definition;
      data = definition->duplicate(_rhs.data);
    }
  }
  ~Private()
  {
    if(definition)
    {
      definition->release(data);
    }
  }

  Uri datatype;
  void* data = nullptr;

  const AbstractMetaTypeDefinition* definition = nullptr;
};

namespace
{
  class EmptyDataType : public AbstractMetaTypeDefinition
  {
  public:
    Uri uri() const override { return Uris::askcore_datatype::empty; }
    int qMetaTypeId() const override { return QMetaType::Void; }
    void* allocate() const override { return nullptr; }
    void* duplicate(const void*) const override { return nullptr; }
    void release(void*) const override {}
    bool compareEquals(const void*, const void*) const override { return true; }
    cres_qresult<QByteArray> md5(const void*) const override
    {
      QCryptographicHash hash(QCryptographicHash::Md5);
      return cres_success(hash.result());
    }
    cres_qresult<QJsonValue> toJsonValue(const void*, const SerialisationContexts&) const override
    {
      return cres_success(QJsonValue());
    }
    cres_qresult<void> fromJsonValue(void*, const QJsonValue&,
                                     const DeserialisationContexts&) const override
    {
      return cres_success();
    }
    /**
     * @return serialise to cbor
     */
    cres_qresult<QCborValue> toCborValue(const void*, const SerialisationContexts&) const override
    {
      return cres_success(QCborValue());
    }
    /**
     * Deserialise from cbor
     */
    cres_qresult<void> fromCborValue(void*, const QCborValue&,
                                     const DeserialisationContexts&) const override
    {
      return cres_success();
    }
    /**
     * Convert to a string representation for display purposes
     */
    cres_qresult<QString> printable(const void*) const override
    {
      return cres_success(QString("<empty>"));
    }
    /**
     * Serialise according to RDF.
     */
    cres_qresult<QString> toRdfLiteral(const void*, const SerialisationContexts&) const override
    {
      return cres_success(QString());
    }
    /**
     * Serialise according to RDF.
     */
    cres_qresult<void> fromRdfLiteral(void*, const QString&,
                                      const DeserialisationContexts&) const override
    {
      return cres_success();
    }
    static EmptyDataType* instance()
    {
      static EmptyDataType* s_instance = new EmptyDataType();
      return s_instance;
    }
  };
} // namespace

// BEGIN constructors
Value::Value() : d(new Private)
{
  d->datatype = Uris::askcore_datatype::empty;
  d->definition = EmptyDataType::instance();
}

Value::Value(const Uri& _datatype, const AbstractMetaTypeDefinition* _definition, void* _data)
    : d(new Private)
{
  d->definition = _definition;
  d->data = _data;
  d->datatype = _datatype;
}

Value::Value(const Value& _rhs) : d(_rhs.d) {}

Value& Value::operator=(const Value& _rhs)
{
  d = _rhs.d;
  return *this;
}

Value::~Value() {}

cres_qresult<Value> Value::fromVariant(const Uri& _datatype, const QVariant& _variant,
                                       TypeCheckingMode _conversion)
{
  const AbstractMetaTypeDefinition* def_1
    = MetaTypeRegistryPrivate::instance()->id_2_definitions.value(_variant.userType(), nullptr);
  const AbstractMetaTypeDefinition* def_2 = getDefinition(_datatype);
  if(not def_2)
  {
    return cres_failure("Datatype '{}' is not known", _datatype);
  }
  if(def_1 != def_2 and def_1)
  {

    if(const AbstractTypeConversion* converter
       = Value::converter(def_1->uri(), _datatype, _conversion))
    {
      void* data = def_2->allocate();
      cres_try(cres_ignore, converter->convert(_variant.data(), data));
      return cres_success(Value(_datatype, def_2, data));
    }
    else if(const AbstractTypeConversion* converter
            = Value::converter(def_1->uri(), Uris::askcore_datatype::value, _conversion))
    {
      // If the QVariant holds a knowRDF::Literal or knowRDF::Value, we need to convert it to a
      // value first and then we can convert it to the destination type
      Value val;
      cres_try(cres_ignore, converter->convert(_variant.data(), &val));
      return val.convert(_datatype, _conversion);
    }
    return cres_failure(
      "{} is not alias of type '{}' with uri '{}' and no conversion from '{}' to '{}'", _datatype,
      clog_safe_string(QMetaType(_variant.userType()).name(), "invalid variant"), def_1->uri(),
      def_1->uri(), def_2->uri());
  }
  if(def_1)
  {
    return cres_success(Value(_datatype, def_1, def_1->duplicate(_variant.data())));
  }
  else
  {
    return cres_failure(
      "Creating value from QVariant of type '{}' to a value of datatype '{}' is not defined!",
      clog_safe_string(_variant.typeName(), "invalid variant"), _datatype);
  }
}

cres_qresult<Value> Value::fromVariant(const QVariant& _variant)
{
  if(_variant.canConvert<knowCore::Value>())
  {
    return cres_success(_variant.value<knowCore::Value>());
  }
  const AbstractMetaTypeDefinition* def
    = MetaTypeRegistryPrivate::instance()->id_2_definitions.value(_variant.userType(), nullptr);
  if(def)
  {
    return cres_success(Value(def->uri(), def, def->duplicate(_variant.data())));
  }
  else if(_variant.canConvert<QVariantHash>())
  {
    ValueHash b;
    cres_try(cres_ignore, b.insert(_variant.value<QVariantHash>()));
    return cres_success(b);
  }
  else if(_variant.canConvert<QVariantList>())
  {
    return ValueList::fromVariants(_variant.value<QVariantList>());
  }
  else if(_variant.isNull())
  {
    return cres_success(Value());
  }
  else
  {
    return cres_failure("Creating value from QVariant of type  '{}' is not defined!",
                        clog_safe_string(_variant.typeName(), "invalid variant"));
  }
}

cres_qresult<Value> Value::parseVariant(const QVariant& _value)
{
  if(_value.canConvert<QVariantMap>())
  {
    QVariantMap m = _value.toMap();
    if(m.contains("type") and m.contains("datatype") and m.contains("value")
       and m.value("type") == "literal")
    {
      return fromVariant(m.value("datatype").toString(), m.value("value"));
    }
  }
  return fromValue(_value);
}

// END constructors

Uri Value::datatype() const { return d->datatype; }

const void* Value::data() const { return d->data; }

bool Value::isEmpty() const { return d->datatype == Uris::askcore_datatype::empty; }

const AbstractMetaTypeDefinition* Value::definition() const { return d->definition; }

QVariant Value::toVariant() const
{
  AbstractToVariantMarshal* marshal
    = MetaTypeRegistryPrivate::instance()->to_variant_marshals.value(d->definition->uri());
  QVariant r;
  if(marshal)
  {
    r = marshal->toQVariant(d->data);
  }
  else
  {
    r = QVariant(QMetaType(d->definition->qMetaTypeId()), d->data);
  }
  return r;
}

cres_qresult<ValueList> Value::toList() const { return value<ValueList>(); }

cres_qresult<ValueHash> Value::toHash() const { return value<ValueHash>(); }

namespace
{
  QList<ComparisonOperator> getOperators(ComparisonOperators _cos)
  {
    QList<ComparisonOperator> l;
    if(_cos.testFlag(ComparisonOperator::Equal))
    {
      l.append(ComparisonOperator::Equal);
    }
    if(_cos.testFlag(ComparisonOperator::AlmostEqual))
    {
      l.append(ComparisonOperator::AlmostEqual);
    }
    if(_cos.testFlag(ComparisonOperator::Inferior))
    {
      l.append(ComparisonOperator::Inferior);
    }
    if(_cos.testFlag(ComparisonOperator::GeoOverlaps))
    {
      l.append(ComparisonOperator::GeoOverlaps);
    }
    if(_cos.testFlag(ComparisonOperator::GeoIntersects))
    {
      l.append(ComparisonOperator::GeoIntersects);
    }
    if(_cos.testFlag(ComparisonOperator::GeoWithin))
    {
      l.append(ComparisonOperator::GeoWithin);
    }
    if(_cos.testFlag(ComparisonOperator::GeoContains))
    {
      l.append(ComparisonOperator::GeoContains);
    }
    if(_cos.testFlag(ComparisonOperator::GeoTouches))
    {
      l.append(ComparisonOperator::GeoTouches);
    }
    if(_cos.testFlag(ComparisonOperator::In))
    {
      l.append(ComparisonOperator::In);
    }
    switch(ComparisonOperator::Equal)
    { // Do nothing, just to trigger compilation error/warnings when update to list of operators
    case ComparisonOperator::Equal:
    case ComparisonOperator::AlmostEqual:
    case ComparisonOperator::Inferior:
    case ComparisonOperator::GeoOverlaps:
    case ComparisonOperator::GeoIntersects:
    case ComparisonOperator::GeoWithin:
    case ComparisonOperator::GeoContains:
    case ComparisonOperator::GeoTouches:
    case ComparisonOperator::In:
      break;
    }
    return l;
  }
} // namespace

cres_qresult<bool> Value::compare(const Value& _rhs, ComparisonOperators _operators) const
{
  if(const AbstractTypeConversion* atc = converterTo<Value>(TypeCheckingMode::Safe))
  {
    Value val;
    cres_try(cres_ignore, atc->convert(data(), &val));
    return val.compare(_rhs, _operators);
  }
  if(const AbstractTypeConversion* atc = _rhs.converterTo<Value>(TypeCheckingMode::Safe))
  {
    Value val;
    cres_try(cres_ignore, atc->convert(_rhs.data(), &val));
    return compare(val, _operators);
  }
  for(ComparisonOperator co : getOperators(_operators))
  {
    if(co == ComparisonOperator::Equal and d->datatype == _rhs.d->datatype)
    {
      if(d->definition->compareEquals(d->data, _rhs.d->data))
      {
        return cres_success(true);
      }
    }
    else
    {
      const AbstractTypeComparator* comparator
        = MetaTypeRegistryPrivate::instance()->comparator(co, d->datatype, _rhs.datatype());
      if(comparator)
      {
        cres_try(bool result, comparator->compare(d->data, _rhs.d->data));
        if(result)
        {
          return cres_success(true);
        }
      }
      else
      {
        return cres_failure("'{}' and '{}' are not comparable with '{}'", *this, _rhs, co);
      }
    }
  }
  return cres_success(false);
}

cres_qresult<Value> Value::convert(const Uri& _uri, TypeCheckingMode _conversion) const
{
  const AbstractMetaTypeDefinition* definition = getDefinition(_uri);
  if(definition == nullptr)
  {
    return cres_failure("No meta type definition for '{}'", _uri);
  }
  if(definition == d->definition)
  { // just an alias, then duplicate
    return cres_success(Value(_uri, definition, definition->duplicate(d->data)));
  }
  else if(const AbstractTypeConversion* atc = converterTo(_uri, _conversion))
  { // Can convert, maybe.
    void* data = definition->allocate();
    cres_try(cres_ignore, atc->convert(d->data, data));
    return cres_success(Value(_uri, definition, data));
  }
  else if(const AbstractTypeConversion* atc = converterTo<Value>(_conversion))
  {
    Value val;
    cres_try(cres_ignore, atc->convert(data(), &val));
    return val.convert(_uri, _conversion);
  }
  else
  {
    return cres_failure("{0} is not alias of type '{1}' and no conversion from '{0}' to '{1}'",
                        _uri, d->datatype);
  }
}

bool Value::canConvert(const Uri& _uri, TypeCheckingMode _conversion) const
{
  return getDefinition(_uri) == d->definition or converterTo(_uri, _conversion)
         or converterBetween(Uris::askcore_datatype::value, _uri, _conversion);
}

cres_qresult<Value> Value::compute(const Value& _rhs, ArithmeticOperator _operator) const
{
  const AbstractArithmeticOperator* aOperator
    = MetaTypeRegistryPrivate::instance()->arithmeticOperator(_operator, d->datatype,
                                                              _rhs.datatype());
  if(aOperator)
  {
    cres_try(Value result, aOperator->compute(d->data, _rhs.d->data));
    return cres_success(result);
  }
  else
  {
    return cres_failure("'{}' and '{}' are not operable with '{}'", *this, _rhs, _operator);
  }
}

// BEGIN operators
bool Value::operator<(const Value& _rhs) const
{
  return printable().expect_success() < _rhs.printable().expect_success();
  ;
}

namespace
{
  inline bool checkComparisonResult(const cres_qresult<bool>& _value)
  {
    return _value.is_successful() and _value.get_value();
  }
} // namespace

bool Value::operator==(const Value& _rhs) const
{
  auto const [s, r, m] = compare(_rhs, ComparisonOperator::Equal);
  return s and r.value();
}
// END operators

// BEGIN access to MetatType stuff
const AbstractTypeConversion* Value::converterTo(const Uri& _datatype,
                                                 TypeCheckingMode _conversion) const
{
  return MetaTypeRegistryPrivate::instance()->converter(d->definition->uri(), _datatype,
                                                        _conversion);
}

const AbstractTypeConversion* Value::converterFrom(const Uri& _datatype,
                                                   TypeCheckingMode _conversion) const
{
  return MetaTypeRegistryPrivate::instance()->converter(_datatype, d->definition->uri(),
                                                        _conversion);
}

const AbstractTypeConversion* Value::converterBetween(const Uri& _from, const Uri& _to,
                                                      TypeCheckingMode _conversion)
{
  return MetaTypeRegistryPrivate::instance()->converter(_from, _to, _conversion);
}

const AbstractMetaTypeDefinition* Value::getDefinition(const Uri& _datatype)
{
  return MetaTypeRegistryPrivate::instance()->uri_2_definitions.value(_datatype, nullptr);
}

const AbstractTypeConversion* Value::converter(const Uri& _from, const Uri& _to,
                                               TypeCheckingMode _conversion)
{
  return MetaTypeRegistryPrivate::instance()->converter(_from, _to, _conversion);
}

// END access to MetatType stuff

// BEGIN MetatType forwarding

cres_qresult<QByteArray> Value::md5() const
{
  if(d->data)
  {
    return d->definition->md5(d->data);
  }
  else
  {
    return cres_failure("Undefined value!");
  }
}

cres_qresult<QJsonValue> Value::toJsonValue(const SerialisationContexts& _contexts) const
{
  if(d->data)
  {
    return d->definition->toJsonValue(d->data, _contexts);
  }
  else if(d->datatype == Uris::askcore_datatype::empty)
  {
    return cres_success(QJsonValue());
  }
  else
  {
    return cres_failure("Undefined value!");
  }
}

cres_qresult<Value> Value::fromJsonValue(const Uri& _datatype, const QJsonValue& _value,
                                         const DeserialisationContexts& _contexts)
{
  const AbstractMetaTypeDefinition* definition = getDefinition(_datatype);
  if(definition)
  {
    Value r(_datatype, definition, definition->allocate());
    cres_try(cres_ignore, definition->fromJsonValue(r.d->data, _value, _contexts));
    return cres_success(r);
  }
  else if(_datatype == Uris::askcore_datatype::empty)
  {
    return cres_success(Value());
  }
  else
  {
    return cres_failure("Unknown type '{}'!", _datatype);
  }
}

cres_qresult<QCborValue> Value::toCborValue(const SerialisationContexts& _contexts) const
{
  if(d->data)
  {
    return d->definition->toCborValue(d->data, _contexts);
  }
  else
  {
    return cres_failure("Undefined value!");
  }
}

cres_qresult<Value> Value::fromCborValue(const Uri& _datatype, const QCborValue& _value,
                                         const DeserialisationContexts& _contexts)
{
  const AbstractMetaTypeDefinition* definition = getDefinition(_datatype);
  if(definition)
  {
    Value r(_datatype, definition, definition->allocate());
    cres_try(cres_ignore, definition->fromCborValue(r.d->data, _value, _contexts));
    return cres_success(r);
  }
  else if(_datatype == Uris::askcore_datatype::empty)
  {
    return cres_success(Value());
  }
  else
  {
    return cres_failure("Unknown type '{}'!", _datatype);
  }
}

cres_qresult<QString> Value::toRdfLiteral(const SerialisationContexts& _contexts) const
{
  if(d->data)
  {
    return d->definition->toRdfLiteral(d->data, _contexts);
  }
  else
  {
    return cres_failure("Undefined value!");
  }
}

cres_qresult<Value> Value::fromRdfLiteral(const Uri& _datatype, const QString& _value,
                                          const DeserialisationContexts& _contexts)
{
  const AbstractMetaTypeDefinition* definition = getDefinition(_datatype);
  if(definition)
  {
    Value r(_datatype, definition, definition->allocate());
    cres_try(cres_ignore, definition->fromRdfLiteral(r.d->data, _value, _contexts));
    return cres_success(r);
  }
  else if(_datatype == Uris::askcore_datatype::empty)
  {
    return cres_success(Value());
  }
  else
  {
    return cres_failure("Unknown type '{}'!", _datatype);
  }
}

cres_qresult<QString> Value::printable() const
{
  if(d->data)
  {
    cres_try(QString value, d->definition->printable(d->data));
    return cres_success(value);
  }
  else
  {
    return cres_failure("Undefined value!");
  }
}

uint knowCore::qHash(const Value& key, uint seed)
{
  auto const [success, hash, errorMessage] = key.md5();
  if(success)
  {

    return qHash(hash.value(), seed);
  }
  else
  {
    clog_warning("Failed to computed md5 for value: {}", errorMessage.value());
    return ::qHash(0, seed);
  }
}

// END MetatType forwarding

#define DATATYPE_KEY u8"datatype"_kCs
#define VALUE_KEY u8"value"_kCs

cres_qresult<QJsonObject> Value::toJsonObject(const SerialisationContexts& _contexts) const
{
  QJsonObject obj;
  obj[DATATYPE_KEY] = QString(datatype());
  cres_try(obj[VALUE_KEY], toJsonValue(_contexts));
  return cres_success(obj);
}

cres_qresult<Value> Value::fromJsonObject(const QJsonObject& _value,
                                          const DeserialisationContexts& _context)
{
  return fromJsonValue(_value[DATATYPE_KEY].toString(), _value[VALUE_KEY], _context);
}

cres_qresult<QCborMap> Value::toCborMap(const SerialisationContexts& _contexts) const
{
  QCborMap obj;
  obj[DATATYPE_KEY] = QString(datatype());
  cres_try(obj[VALUE_KEY], toCborValue(_contexts));
  return cres_success(obj);
}

cres_qresult<Value> Value::fromCborMap(const QCborMap& _value,
                                       const DeserialisationContexts& _context)
{
  return fromCborValue(_value[DATATYPE_KEY].toString(), _value[VALUE_KEY], _context);
}

#if 0

#define DATATYPE_KEY u8"datatype"_kCs
#define VALUE_KEY u8"value"_kCs

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(Value)
  cres_qresult<QByteArray> md5(const Value& _value) const override
  {
    return _value.md5();
  }
  cres_qresult<QJsonValue> toJsonValue(const Value& _value) const override
  {
    cres_try(value_json, _value.toJsonValue());
    QJsonObject object;
    object[DATATYPE_KEY] = (QString)_value.datatype();
    object[VALUE_KEY] = value_json;
    return cres_success(object);
  }
  cres_qresult<void> fromJsonValue(Value* _value, const QJsonValue& _json_value) const override
  {
    if(_json_value.isObject())
    {
      QJsonObject obj = _json_value.toObject();
      Uri datatype = obj.value(DATATYPE_KEY).toString();
      cres_try(value, Value::fromJsonValue(datatype, obj.value(VALUE_KEY)));
      *_value = value;
      return cres_success();
    } else {
      return cres_failure("Expected object got '{}'", _json_value);
    }
  }
  cres_qresult<QCborValue> toCborValue(const Value& _value) const override
  {
    cres_try(value_cbor, _value.toCborValue());
    QCborMap map;
    map[DATATYPE_KEY] = (QString)_value.datatype();
    map[VALUE_KEY] = value_cbor;
    return cres_success(map);
  }
  cres_qresult<void> fromCborValue(Value* _value, const QCborValue& _cbor_value) const override
  {
    if(_cbor_value.isMap())
    {
      QCborMap obj = _cbor_value.toMap();
      Uri datatype = obj.value(DATATYPE_KEY).toString();
      cres_try(value, Value::fromCborValue(datatype, obj.value(VALUE_KEY)));
      *_value = value;
      return cres_success();
    } else {
      return cres_failure("Expected object got '{}'", _cbor_value);
    }
  }
  cres_qresult<QString> printable(const Value& _value) const override
  {
    return _value.printable();
  }
  cres_qresult<QString> toRdfLiteral(const Value& _value) const override
  {
    return toJsonString(_value);
  }
  cres_qresult<void> fromRdfLiteral(Value* _value, const QString& _serialised) const override
  {
    return fromJsonString(_value, _serialised);
  }
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(Value)

#endif

KNOWCORE_DEFINE_QT_METATYPE(Value, MetaTypeTraits::None)
KNOWCORE_DEFINE_METATYPE_URI(Value, Uris::askcore_datatype::value)
