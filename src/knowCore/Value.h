#pragma once

#include <clog_qt>
#include <cres_qt>

#include <QSharedDataPointer>

#include <knowCore/Forward.h>

#include "MetaType.h"

namespace knowCore
{
  namespace details
  {
    template<typename _T_>
    struct Value_value;
  }
  class Value
  {
    template<typename _T_>
    friend struct details::Value_value;
    Value(const Uri& _datatype, const AbstractMetaTypeDefinition* _definition, void* _data);
    Value(const Uri& _datatype, const Uri& _maindatatype,
          const AbstractMetaTypeDefinition* _definition, void* _data);
  public:
    /**
     * Construct an empty value
     */
    Value();
    Value(const Value& _rhs);
    Value& operator=(const Value& _rhs);
    ~Value();

    /**
     * Create a \ref Value from any value \p _value. The type of \p _value needs to be register in
     * knowCore's metatype system. This function will guess the data type uri.
     */
    template<typename _T_>
    static inline Value fromValue(const _T_& _value);
    /**
     * Attempt to create a \ref Value from a variant \p _value. The type of \p _value needs to be
     * register in knowCore's metatype system, if it is not, the function return an error.
     */
    static inline cres_qresult<Value> fromValue(const QVariant& _value);
    /**
     * Create a \ref Value from any value \p _value. The type of \p _value needs to be register in
     * knowCore's metatype system. This function will use the datatype \p _datatype and attempt to
     * convert \p _value into \p _datatype.
     */
    template<typename _T_>
    static inline cres_qresult<Value> fromValue(const Uri& _datatype, const _T_& _value,
                                                TypeCheckingMode _conversion
                                                = TypeCheckingMode::Safe);
    /**
     * Construct a literal from a variant
     * @param _datatype the type of the literal
     * @param _value a variant with the value
     *
     * This attempt to convert the variant content to the dataype, if it fails, it returns
     * an invalid value.
     */
    static cres_qresult<Value> fromVariant(const Uri& _datatype, const QVariant& _value,
                                           TypeCheckingMode _conversion = TypeCheckingMode::Safe);
    /**
     * Construct a value from a variant. If the type of the variant is not defined
     * as a knowCore MetaType, it will return an invalid value.
     */
    static cres_qresult<Value> fromVariant(const QVariant& _variant);
    /**
     * Attempt to parse a variant as a value. If the value is a hash,
     * it might be parsed as a literal.
     * So, a hash such as { "type": "literal", "datatype": "...", "value": "..."} will
     * be returned as a value with the given value and datatype.
     */
    static cres_qresult<Value> parseVariant(const QVariant& _value);
    /**
     * @return convert to a variant
     */
    QVariant toVariant() const;
    /**
     * @return the URI corresponding to the datatype
     */
    Uri datatype() const;
    /**
     * @return true if it is empty
     */
    bool isEmpty() const;
    /**
     * This operator use the \ref printable result, it should only be used for storing values
     * in a container.
     * If you want proper comparison between the value you should use the \ref compare function.
     */
    bool operator<(const Value& _rhs) const;
    /**
     * Equality comparison, it can only compare value of the same type.
     */
    bool operator==(const Value& _rhs) const;
    /**
     * Inequality comparison, it can only compare value of the same type.
     */
    bool operator!=(const Value& _rhs) const { return not(*this == _rhs); }
    /**
     * Compare two values. With different operators, if one returns true then compare return true.
     */
    cres_qresult<bool> compare(const Value& _value, ComparisonOperators _operators) const;
    cres_qresult<Value> convert(const Uri& _uri,
                                TypeCheckingMode _conversion = TypeCheckingMode::Safe) const;
    /**
     * Compute the arithmetic operation between this value and \ref _value.
     */
    cres_qresult<Value> compute(const Value& _value, ArithmeticOperator _operator) const;
    /**
     * Function to access the value stored .
     *
     * @return the value if successfull, or an error message if the value cannot be converted
     */
    template<typename _T_>
    inline cres_qresult<_T_> value(TypeCheckingMode _conversion = TypeCheckingMode::Safe) const;
    /**
     * Convenient function to access the value or a default one if the stored value and \p _T_ are
     * not compatible.
     *
     * @return the value contained in the literal (no error is triggered if the type is wrong,
     * instead a default value is returned)
     */
    template<typename _T_>
    inline _T_ value(const _T_& _default,
                     TypeCheckingMode _conversion = TypeCheckingMode::Safe) const;
    /**
     * @return true if it is possible to convert
     */
    template<typename _T_>
    inline bool canConvert(TypeCheckingMode _conversion = TypeCheckingMode::Safe) const;
    /**
     * @return true if this value can be converted to the type identified by \param _uri.
     */
    bool canConvert(const Uri& _uri, TypeCheckingMode _conversion = TypeCheckingMode::Safe) const;
    template<typename _T_>
    inline bool is() const;
    cres_qresult<QByteArray> md5() const;
    cres_qresult<QJsonValue> toJsonValue(const SerialisationContexts& _contexts
                                         = defaultSerialisationContext()) const;
    static cres_qresult<Value> fromJsonValue(const Uri& _datatype, const QJsonValue& _value,
                                             const DeserialisationContexts& _context
                                             = defaultDeserialisationContext());
    template<typename _T_>
    static cres_qresult<_T_> fromJsonValue(const QJsonValue& _value,
                                           const DeserialisationContexts& _context
                                           = defaultDeserialisationContext());
    cres_qresult<QCborValue> toCborValue(const SerialisationContexts& _contexts
                                         = defaultSerialisationContext()) const;
    static cres_qresult<Value> fromCborValue(const Uri& _datatype, const QCborValue& _value,
                                             const DeserialisationContexts& _context
                                             = defaultDeserialisationContext());
    cres_qresult<QString> toRdfLiteral(const SerialisationContexts& _contexts
                                       = defaultSerialisationContext()) const;
    static cres_qresult<Value> fromRdfLiteral(const Uri& _datatype, const QString& _value,
                                              const DeserialisationContexts& _context
                                              = defaultDeserialisationContext());
    cres_qresult<QString> printable() const;

    /**
     * @return a json object that contains the datatype and the value.
     */
    cres_qresult<QJsonObject> toJsonObject(const SerialisationContexts& _contexts
                                           = defaultSerialisationContext()) const;
    /**
     * @return a value from a json object that contains the datatype and the value (i.e. one created
     * with @ref toJsonObject).
     */
    static cres_qresult<Value> fromJsonObject(const QJsonObject& _value,
                                              const DeserialisationContexts& _context
                                              = defaultDeserialisationContext());
    /**
     * @return a cbor map that contains the datatype and the value.
     */
    cres_qresult<QCborMap> toCborMap(const SerialisationContexts& _contexts
                                     = defaultSerialisationContext()) const;
    /**
     * @return a value from a cbor map that contains the datatype and the value (i.e. one created
     * with @ref toCborMap).
     */
    static cres_qresult<Value> fromCborMap(const QCborMap& _value,
                                           const DeserialisationContexts& _context
                                           = defaultDeserialisationContext());

    /**
     * This is a convenient function for \ref value<ValueList>() .
     */
    cres_qresult<ValueList> toList() const;
    /**
     * This is a convenient function for \ref value<ValueHash>() .
     */
    cres_qresult<ValueHash> toHash() const;
  private:
    const AbstractMetaTypeDefinition* definition() const;
  private:
    // Access to metatype stuff
    template<typename _T_>
    static const AbstractMetaTypeDefinition* getDefinition()
    {
      return MetaTypeInformation<_T_>::definition();
    }
    static const AbstractMetaTypeDefinition* getDefinition(const Uri& _datatype);
    static const AbstractMetaTypeDefinition* getDefinition(int _id);
    const void* data() const;
    template<typename _T_>
    const AbstractTypeConversion* converterTo(TypeCheckingMode _conversion) const
    {
      return converterTo(MetaTypeInformation<_T_>::uri(), _conversion);
    }
    const AbstractTypeConversion* converterTo(const Uri& _datatype,
                                              TypeCheckingMode _conversion) const;
    template<typename _T_>
    const AbstractTypeConversion* converterFrom(TypeCheckingMode _conversion) const
    {
      return converterFrom(MetaTypeInformation<_T_>::uri());
    }
    const AbstractTypeConversion* converterFrom(const Uri& _datatype,
                                                TypeCheckingMode _conversion) const;
    template<typename _TFrom_, typename _TTo_>
    static const AbstractTypeConversion* converterBetween(TypeCheckingMode _conversion)
    {
      return converterBetween(MetaTypeInformation<_TFrom_>::uri(),
                              MetaTypeInformation<_TTo_>::uri(), _conversion);
    }
    static const AbstractTypeConversion* converterBetween(const Uri& _from, const Uri& _to,
                                                          TypeCheckingMode _conversion);

    static const AbstractTypeConversion* converter(const Uri& _from, const Uri& _to,
                                                   TypeCheckingMode _conversion);
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
  uint qHash(const Value& key, uint seed = 0);

  template<typename _T_>
  inline Value Value::fromValue(const _T_& _value)
  {
    const AbstractMetaTypeDefinition* def = getDefinition<_T_>();
    clog_assert(def);
    return Value(def->uri(), def, def->duplicate(&_value));
  }
  template<typename _T_>
  inline cres_qresult<Value> Value::fromValue(const Uri& _datatype, const _T_& _value,
                                              TypeCheckingMode _conversion)
  {
    const AbstractMetaTypeDefinition* def_1 = getDefinition<_T_>();
    const AbstractMetaTypeDefinition* def_2 = getDefinition(_datatype);
    if(def_1 != def_2)
    {
      const AbstractTypeConversion* converter
        = Value::converter(MetaTypeInformation<_T_>::uri(), _datatype, _conversion);
      if(converter)
      {
        void* data = def_2->allocate();
        cres_try(cres_ignore, converter->convert(&_value, data), );
        return cres_success(Value(_datatype, def_2, data));
      }
      return cres_failure(
        "{0} is not alias of type '{1}' with uri '{2}' and no conversion from '{2}' to '{0}'.",
        _datatype, prettyTypename<_T_>(), MetaTypeInformation<_T_>::uri());
    }
    if(def_1)
    {
      return cres_success(Value(_datatype, def_1, def_1->duplicate(&_value)));
    }
    else
    {
      return cres_failure("Type '{}' with datatype '{}' is not defined!", prettyTypename<_T_>(),
                          _datatype);
    }
  }
  template<>
  inline cres_qresult<Value> Value::fromValue<knowCore::Value>(const Uri& _datatype,
                                                               const knowCore::Value& _value,
                                                               TypeCheckingMode _conversion)
  {
    return _value.convert(_datatype, _conversion);
  }
  inline cres_qresult<Value> Value::fromValue(const QVariant& _value)
  {
    return fromVariant(_value);
  }
  template<>
  inline cres_qresult<Value> Value::fromValue<QVariant>(const Uri& _datatype,
                                                        const QVariant& _value,
                                                        TypeCheckingMode _conversion)
  {
    return fromVariant(_datatype, _value, _conversion);
  }

  namespace details
  {
    template<typename _T_>
    struct Value_value
    {
      static cres_qresult<_T_> value(const Value* _self, TypeCheckingMode _conversion)
      {
        if(MetaTypeInformation<_T_>::uri() == _self->datatype()
           or _self->getDefinition<_T_>() == _self->definition())
        {
          return cres_success(_T_(*reinterpret_cast<const _T_*>(_self->data())));
        }
        // First: try to convert directly
        if(const AbstractTypeConversion* converter = _self->converterTo<_T_>(_conversion))
        {
          _T_ r;
          cres_try(cres_ignore, converter->convert(_self->data(), &r));
          return cres_success(r);
        } // Second: some types can be converted to a value (mostly usefull for knowRDF::Literal)
          // and then from that value to _T_
        else if(const AbstractTypeConversion* converter = _self->converterTo<Value>(_conversion))
        {
          Value val;
          cres_try(cres_ignore, converter->convert(_self->data(), &val));
          return val.value<_T_>();
        }
        // Third some types have custom converters from Value (mostly knowRDF::Literal)
        else if(const AbstractTypeConversion* converter
                = _self->converterBetween<knowCore::Value, _T_>(_conversion))
        {
          _T_ val;
          cres_try(cres_ignore, converter->convert(_self, &val));
          return cres_success(val);
        }
        return cres_failure("cannot convert from '{}' to '{}'", _self->datatype(),
                            MetaTypeInformation<_T_>::uri());
      }
    };
    template<typename _T_>
    struct Value_value<std::optional<_T_>>
    {
      static cres_qresult<std::optional<_T_>> value(const Value* _self,
                                                    TypeCheckingMode _conversion)
      {
        if(_self->isEmpty())
        {
          return cres_success<std::optional<_T_>>(std::nullopt);
        }
        else
        {
          return _self->value<_T_>();
        }
      }
    };
  } // namespace details

  template<typename _T_>
  inline cres_qresult<_T_> Value::value(TypeCheckingMode _conversion) const
  {
    return details::Value_value<_T_>::value(this, _conversion);
  }
  template<typename _T_>
  inline _T_ Value::value(const _T_& _default, TypeCheckingMode _conversion) const
  {
    auto const& [s, v, m] = value<_T_>(_conversion);
    return s ? v.value() : _default;
  }
  template<typename _T_>
  inline bool Value::canConvert(TypeCheckingMode _conversion) const
  {
    return canConvert(MetaTypeInformation<_T_>::uri(), _conversion);
  }
  template<typename _T_>
  inline bool Value::is() const
  {
    return definition()->uri() == MetaTypeInformation<_T_>::uri();
  }
  template<typename _T_>
  inline cres_qresult<_T_> Value::fromJsonValue(const QJsonValue& _value,
                                                const DeserialisationContexts& _context)
  {
    cres_try(knowCore::Value val, fromJsonValue(MetaTypeInformation<_T_>::uri(), _value, _context));
    return val.template value<_T_>();
  }

  /**
   * Convenience class to convert a \ref knowCore::Value to an arbitrary type for'
   * use as part of an if expression.
   *
   * @code
   * knowCore::Value akcvalue = knowCore::Value::fromValue(SomeKlass());
   * if(knowCore::ValueCast<SomeKlass> vc = akcvalue)
   * {
   *   SomeKlass sv = vc;
   *   sv.func() == vc->func();
   *   return vc;
   * }
   * @endcode
   */
  template<typename _T_>
  class ValueCast
  {
  public:
    ValueCast(const knowCore::Value& _val, TypeCheckingMode _conversion = TypeCheckingMode::Safe)
    {
      auto const& [s, t, m] = _val.value<_T_>(_conversion);
      m_success = s;
      m_t = t;
    }
    operator bool() { return m_success; }
    const _T_& value() const
    {
      clog_assert(m_success);
      return m_t.value();
    }
    _T_& value()
    {
      clog_assert(m_success);
      return m_t.value();
    }
    operator const _T_&() const { return value(); }
    _T_* operator->() { return &value(); }
    _T_* operator->() const { return &value(); }
    _T_&& moveValue()
    {
      clog_assert(m_success);
      return std::move(m_t.value());
    }
  private:
    bool m_success;
    std::optional<_T_> m_t;
  };
  /**
   * Convenience class to convert a \ref knowCore::Value to an arbitrary type for'
   * use as part of an if expression. Unlike \ref knowCore::ValueCast, no
   * conversion is allowed.
   *
   * @code
   * knowCore::Value akcvalue = knowCore::Value::fromValue(SomeKlass());
   * if(knowCore::ValueIs<SomeKlass> vc = akcvalue)
   * {
   *   SomeKlass sv = vc;
   *   sv.func() == vc->func();
   *   return vc;
   * }
   * @endcode
   */
  template<typename _T_>
  class ValueIs
  {
  public:
    ValueIs(const knowCore::Value& _val)
    {
      if(_val.datatype() == MetaTypeInformation<_T_>::uri())
      {
        m_success = true;
        m_t = _val.value<_T_>().expect_success();
      }
      else
      {
        m_success = false;
      }
    }
    operator bool() { return m_success; }
    const _T_& value() const
    {
      clog_assert(m_success);
      return m_t;
    }
    _T_& value()
    {
      clog_assert(m_success);
      return m_t;
    }
    operator const _T_&() const { return value(); }
    _T_* operator->() { return &value(); }
    _T_* operator->() const { return &value(); }
    _T_&& moveValue() { return std::move(m_t); }
  private:
    bool m_success;
    _T_ m_t;
  };
} // namespace knowCore

clog_format_declare_formatter(knowCore::Value)
{
  auto const [success, value, message] = p.printable();
  if(success)
  {
    return format_to(ctx.out(), "{}({})", p.datatype(), value.value());
  }
  else
  {
    return format_to(ctx.out(), "{}(error: '{}')", p.datatype(), message.value());
  }
}

template<typename _T_>
inline cres_qresult<_T_> cres_success(knowCore::ValueCast<_T_>& _t)
{
  return cres_success(_t.moveValue());
}

template<typename _T_>
inline cres_qresult<_T_> cres_success(knowCore::ValueIs<_T_>& _t)
{
  return cres_success(_t.moveValue());
}

#include <knowCore/MetaType.h>
KNOWCORE_DECLARE_FULL_METATYPE_NO_DEFINITION(knowCore, Value);
