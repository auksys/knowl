#include "ValueHash.h"

#include <QCborArray>
#include <QCborMap>
#include <QCryptographicHash>
#include <QJsonArray>
#include <QJsonObject>

using namespace knowCore;

struct ValueHash::Private : public QSharedData
{
  QHash<QString, Value> values;
};

ValueHash::ValueHash() : d(new Private) {}

// ValueHash::ValueHash(std::initializer_list<std::pair<QString, Value>> _list)
//     : ValueHash(QHash<QString, Value>(_list))
// {
// }

ValueHash::ValueHash(const QHash<QString, Value>& _values) : d(new Private) { d->values = _values; }

ValueHash::ValueHash(const ValueHash& _rhs) : d(_rhs.d) {}

ValueHash::ValueHash(ValueHash&& _rhs) : d(std::move(_rhs.d)) {}

ValueHash ValueHash::operator=(const ValueHash& _rhs)
{
  d = _rhs.d;
  return *this;
}

ValueHash::~ValueHash() {}

cres_qresult<ValueHash> ValueHash::parseVariantMap(const QVariantMap& _value)
{
  ValueHash r;

  for(auto it = _value.begin(); it != _value.end(); ++it)
  {
    cres_try(Value val, Value::parseVariant(it.value()));
    r.insert(it.key(), val);
  }

  return cres_success(r);
}

int ValueHash::size() const { return d->values.size(); }

QStringList ValueHash::keys() const { return d->values.keys(); }

Value ValueHash::value(const QString& _key) const { return d->values.value(_key); }

bool ValueHash::contains(const QString& _key) const { return d->values.contains(_key); }

bool ValueHash::contains(const QString& _key, const knowCore::Uri& _datatype) const
{
  auto it = d->values.find(_key);
  if(it != d->values.end())
  {
    return it.value().canConvert(_datatype);
  }
  else
  {
    return false;
  }
}

cres_qresult<void> ValueHash::ensureContains(const QString& _key,
                                             const knowCore::Uri& _datatype) const
{
  auto it = d->values.find(_key);
  if(it != d->values.end())
  {
    if(it.value().canConvert(_datatype))
    {
      return cres_success();
    }
    else
    {
      return cres_failure("Cannot convert '{}' to '{}'", it.value(), _datatype);
    }
  }
  else
  {
    return cres_failure("Missing '{}' in value hash.", _key);
  }
}

bool ValueHash::operator==(const ValueHash& _rhs) const { return d->values == _rhs.d->values; }

bool ValueHash::operator!=(const ValueHash& _rhs) const { return d->values != _rhs.d->values; }

bool ValueHash::operator==(const QHash<QString, Value>& _rhs) const { return d->values == _rhs; }

cres_qresult<QByteArray> ValueHash::md5() const
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  for(const Value& value : d->values)
  {
    cres_try(QByteArray value_md5, value.md5());
    hash.addData(value_md5);
  }
  return cres_success(hash.result());
}

cres_qresult<QJsonValue> ValueHash::toJsonValue(const SerialisationContexts& _contexts) const
{
  QJsonObject object;
  for(auto it = d->values.begin(); it != d->values.end(); ++it)
  {
    cres_try(QJsonObject value_json, it.value().toJsonObject(_contexts));
    object[it.key()] = value_json;
  }
  return cres_success(object);
}

cres_qresult<ValueHash> ValueHash::fromJsonValue(const QJsonValue& _json_value,
                                                 const DeserialisationContexts& _context)
{
  if(_json_value.isObject())
  {
    QHash<QString, Value> values;
    QJsonObject json_object = _json_value.toObject();
    for(auto it = json_object.begin(); it != json_object.end(); ++it)
    {
      QJsonValue json_value = it.value();
      if(json_value.isObject())
      {
        QJsonObject obj = json_value.toObject();
        cres_try(Value value, Value::fromJsonObject(obj, _context));
        values[obj.value(it.key()).toString()] = value;
      }
      else
      {
        return cres_failure("Expected object got '{}' when parsing ValueHash '{}'", json_value,
                            _json_value);
      }
    }
    return cres_success(values);
  }
  else
  {
    return cres_failure("Expect objject got '{}'", _json_value);
  }
}

cres_qresult<QCborValue> ValueHash::toCborValue(const SerialisationContexts& _contexts) const
{
  QCborMap map;
  for(auto it = d->values.begin(); it != d->values.end(); ++it)
  {
    cres_try(QCborMap value_cbor, it.value().toCborMap(_contexts));
    map[it.key()] = value_cbor;
  }
  return cres_success(map);
}

cres_qresult<ValueHash> ValueHash::fromCborValue(const QCborValue& _cbor_value,
                                                 const DeserialisationContexts& _contexts)
{
  if(_cbor_value.isMap())
  {
    QHash<QString, Value> values;
    QCborMap cbor_map = _cbor_value.toMap();
    for(auto it = cbor_map.begin(); it != cbor_map.end(); ++it)
    {
      QCborValue cbor_value = it.value();
      if(cbor_value.isMap())
      {
        QCborMap obj = cbor_value.toMap();
        cres_try(Value value, Value::fromCborMap(obj, _contexts));
        values[it.key().toString()] = value;
      }
      else
      {
        return cres_failure("Expected object got '{}' when parsing ValueHash '{}'", cbor_value,
                            _cbor_value);
      }
    }
    return cres_success(values);
  }
  else
  {
    return cres_failure("Expect array got '{}'", _cbor_value);
  }
}

cres_qresult<QString> ValueHash::printable() const
{
  return cres_success(clog_qt::qformat("{}", d->values));
}

QVariantHash ValueHash::toVariantHash() const
{
  QVariantHash values;
  QHash<QString, Value> values_ = hash();
  for(auto it = values_.begin(); it != values_.end(); ++it)
  {
    values[it.key()] = it.value().toVariant();
  }
  return values;
}

QVariantMap ValueHash::toVariantMap() const
{
  QVariantMap values;
  QHash<QString, Value> values_ = hash();
  for(auto it = values_.begin(); it != values_.end(); ++it)
  {
    values[it.key()] = it.value().toVariant();
  }
  return values;
}

QVariant ValueHash::toVariant() const { return toVariantHash(); }

QHash<QString, Value> ValueHash::hash() const { return d->values; }

cres_qresult<void> ValueHash::checkContainsOnly(const knowCore::Uri& _uri) const
{
  for(const Value& value : d->values)
  {
    if(value.datatype() != _uri)
    {
      return cres_failure("Invalid type '{}' in list, expected: '{}'", value.datatype(), _uri);
    }
  }
  return cres_success();
}

ValueHash& ValueHash::insert(const QString& _key, const knowCore::Value& _value)
{
  d->values[_key] = _value;
  return *this;
}

cres_qresult<void> ValueHash::insert(const QString& _key, const QVariant& _value)
{
  cres_try(Value value, Value::fromVariant(_value));
  d->values[_key] = value;
  return cres_success();
}

cres_qresult<void> ValueHash::insert(const QVariantHash& _value) { return insertHashMap(_value); }

cres_qresult<void> ValueHash::insert(const QVariantMap& _value) { return insertHashMap(_value); }

ValueHash& ValueHash::insert(const ValueHash& _map)
{
  for(auto it = _map.d->values.begin(); it != _map.d->values.end(); ++it)
  {
    insert(it.key(), it.value());
  }
  return *this;
}

cres_qresult<void> ValueHash::insert(const QJsonObject& _value)
{
  return insert(_value.toVariantHash());
}

ValueHash& ValueHash::remove(const QString& _key)
{
  d->values.remove(_key);
  return *this;
}

template<typename _T_>
cres_qresult<void> ValueHash::insertHashMap(const _T_& _map)
{
  for(auto it = _map.begin(); it != _map.end(); ++it)
  {
    cres_try(cres_ignore, insert(it.key(), it.value()));
  }
  return cres_success();
}

void ValueHash::clear() { d->values.clear(); }

// END ValueHash

#include "MetaTypeImplementation.h"
#include <knowCore/Uris/askcore_datatype.h>

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(ValueHash)
cres_qresult<QByteArray> md5(const ValueHash& _value) const override { return _value.md5(); }
cres_qresult<QJsonValue> toJsonValue(const ValueHash& _value,
                                     const SerialisationContexts& _contexts) const override
{
  return _value.toJsonValue(_contexts);
}
cres_qresult<void> fromJsonValue(ValueHash* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts& _contexts) const override
{
  return cres_try_assign(_value, ValueHash::fromJsonValue(_json_value, _contexts));
}
cres_qresult<QCborValue> toCborValue(const ValueHash& _value,
                                     const SerialisationContexts& _contexts) const override
{
  return _value.toCborValue(_contexts);
}
cres_qresult<void> fromCborValue(ValueHash* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts& _contexts) const override
{
  return cres_try_assign(_value, ValueHash::fromCborValue(_cbor_value, _contexts));
}
cres_qresult<QString> printable(const ValueHash& _value) const override
{
  return _value.printable();
}
cres_qresult<QString> toRdfLiteral(const ValueHash& _value,
                                   const SerialisationContexts& _contexts) const override
{
  return toJsonString(_value, _contexts);
}
cres_qresult<void> fromRdfLiteral(ValueHash* _value, const QString& _serialised,
                                  const DeserialisationContexts& _contexts) const override
{
  return fromJsonString(_value, _serialised, _contexts);
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(ValueHash)

KNOWCORE_DEFINE_METATYPE(ValueHash, Uris::askcore_datatype::valuehash, MetaTypeTraits::None)

template<>
struct knowCore::Comparator<ComparisonOperator::In, QString, ValueHash>
{
  static inline cres_qresult<bool> compare(const QString& _left, const ValueHash& _right)
  {
    return cres_success(_right.contains(_left));
  }
};

KNOWCORE_REGISTER_COMPARATORS_FROM((In), QString, ValueHash)
