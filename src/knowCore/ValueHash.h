#pragma once

#include "Value.h"

namespace knowCore
{
  // BEGIN ValueHash
  /**
   * \ingroup knowCore
   * Represent a (key, value) where key are strings for integration with \ref knowCore::Value.
   */
  class ValueHash
  {
  public:
    ValueHash();
    template<typename _T_, typename... _TOther_>
    ValueHash(const QString& _key, const _T_& _t, const _TOther_&...);
    ValueHash(const QHash<QString, Value>& _value);
    ValueHash(const ValueHash& _list);
    ValueHash(ValueHash&& _list);
    ValueHash operator=(const ValueHash& _list);
    ~ValueHash();
    static cres_qresult<ValueHash> fromVariantMap(const QVariantMap& _map);
    static cres_qresult<ValueHash> fromVariantHash(const QVariantHash& _map);
    static cres_qresult<ValueHash> parseVariantMap(const QVariantMap& _value);
  private:
    template<typename _T_>
    static cres_qresult<ValueHash> fromVariantHashMap(const _T_& _map);
  public:
    /**
     * @return the number of elements in the hash
     */
    int size() const;

    /**
     * @return the list of keys
     */
    QStringList keys() const;

    /**
     * @return the value for the given @arg _key or an empty value
     */
    Value value(const QString& _key) const;

    /**
     * @return attempt to return the value for the given @arg _key if convertible to @arg _T_
     */
    template<typename _T_>
    cres_qresult<_T_> value(const QString& _key) const;
    /**
     * @return attempt to return the value for the given @arg _key if convertible to @arg _T_
     */
    template<typename _T_>
    cres_qresult<_T_> value(const QString& _key, const _T_& _default) const;
    /**
     * @return if the value is contained in the hash.
     */
    bool contains(const QString& _key) const;
    /**
     * @return if the value is contained in the hash and has the given \p _datatype.
     */
    bool contains(const QString& _key, const knowCore::Uri& _datatype) const;
    /**
     * @return if the value is contained in the hash and is of type \p _T_.
     */
    template<typename _T_>
    bool contains(const QString& _key) const;
    /**
     * @return if the value is contained in the hash and has the given \p _datatype.
     */
    cres_qresult<void> ensureContains(const QString& _key, const knowCore::Uri& _datatype) const;
    /**
     * @return if the value is contained in the hash and is of type \p _T_.
     */
    template<typename _T_>
    cres_qresult<void> ensureContains(const QString& _key) const;
    /**
     * @return attempt to return as a hash of a specific type @arg _T_ if they are convertible to
     * @arg _T_
     */
    template<typename _T_>
    cres_qresult<QHash<QString, _T_>> hash() const;
    /**
     * @return the underlying `QHash<QString, Value>`
     */
    QHash<QString, Value> hash() const;
    /**
     * @return attempt to return as a map of a specific type @arg _T_ if they are convertible to
     * @arg _T_
     */
    template<typename _T_>
    cres_qresult<QMap<QString, _T_>> map() const;
    /**
     * @return convert the underlying hash into a `QMap<QString, Value>`
     */
    QMap<QString, Value> map() const;
    /**
     * @return attempt to return the values if they are convertible to @arg _T_
     */
    template<typename _T_>
    cres_qresult<QList<_T_>> values() const;
    /**
     * @return return the list of values
     */
    QList<Value> values() const { return hash().values(); }

    bool operator==(const ValueHash& _rhs) const;
    bool operator!=(const ValueHash& _rhs) const;
    bool operator==(const QHash<QString, Value>& _rhs) const;

    cres_qresult<QByteArray> md5() const;
    cres_qresult<QJsonValue> toJsonValue(const SerialisationContexts& _contexts) const;
    static cres_qresult<ValueHash> fromJsonValue(const QJsonValue& _value,
                                                 const DeserialisationContexts& _contexts);
    cres_qresult<QCborValue> toCborValue(const SerialisationContexts& _contexts) const;
    static cres_qresult<ValueHash> fromCborValue(const QCborValue& _value,
                                                 const DeserialisationContexts& _contexts);
    cres_qresult<QString> printable() const;
    /**
     * @return a variant hash representation
     */
    QVariantHash toVariantHash() const;
    /**
     * @return a variant map representation
     */
    QVariantMap toVariantMap() const;
    QVariant toVariant() const;
    /**
     * Check that the list only contains value of type \ref _uri.
     * It returns success if that is true.
     * It returns a failure with an error message in case a value has a
     * different type in the list.
     */
    cres_qresult<void> checkContainsOnly(const Uri& _uri) const;
    template<typename _T_>
    cres_qresult<void> checkContainsOnly() const;

    /**
     * Allow direct casting to \ref Value.
     */
    operator Value() const { return Value::fromValue(*this); }

    /**
     * Add a value to the hash with the given key.
     */
    template<typename _T_>
    ValueHash& insert(const QString& _key, const _T_& _value)
    {
      return insert(_key, Value::fromValue(_value));
    }
    /**
     * Add a set of keys/values to the hash.
     */
    template<typename _T_, typename... _TOther_>
    ValueHash& insert(const QString& _key, const _T_& _value, const QString& _key_n,
                      const _TOther_&... _other)
    {
      insert(_key, _value);
      return insert(_key_n, _other...);
    }
    /**
     * Add a value to the hash with the given key.
     */
    ValueHash& insert(const QString& _key, const knowCore::Value& _value);
    /**
     * Add a value from variant. This can potentially trigger a failure that is detected at create.
     */
    cres_qresult<void> insert(const QString& _key, const QVariant& _value);
    /**
     * Merge a \ref QVariantHash into the hash.
     */
    cres_qresult<void> insert(const QVariantHash& _value);
    /**
     * Merge a \ref QVariantHash into the hash.
     */
    ValueHash& insert(const knowCore::ValueHash& _value);
    /**
     * Merge a \ref QVariantMap into the hash.
     */
    cres_qresult<void> insert(const QVariantMap& _value);
    /**
     * Attempt to insert the content of a \ref QJsonObject to the hash.
     */
    cres_qresult<void> insert(const QJsonObject& _value);
    /**
     * Remove the \p _key.
     */
    ValueHash& remove(const QString& _key);
    /**
     * Clear the hash.
     */
    void clear();
  private:
    template<typename _T_>
    cres_qresult<void> insertHashMap(const _T_& _map);
    struct Private;
    QSharedDataPointer<Private> d;
  };

  template<typename _T_, typename... _TOther_>
  ValueHash::ValueHash(const QString& _key, const _T_& _t, const _TOther_&... _other) : ValueHash()
  {
    insert(_key, _t, _other...);
  }

  template<typename _T_>
  bool ValueHash::contains(const QString& _key) const
  {
    return contains(_key, MetaTypeInformation<_T_>::uri());
  }
  template<typename _T_>
  cres_qresult<void> ValueHash::ensureContains(const QString& _key) const
  {
    return ensureContains(_key, MetaTypeInformation<_T_>::uri());
  }

  inline cres_qresult<ValueHash> ValueHash::fromVariantMap(const QVariantMap& _map)
  {
    return fromVariantHashMap(_map);
  }
  inline cres_qresult<ValueHash> ValueHash::fromVariantHash(const QVariantHash& _map)
  {
    return fromVariantHashMap(_map);
  }

  template<typename _T_>
  inline cres_qresult<ValueHash> ValueHash::fromVariantHashMap(const _T_& _map)
  {
    ValueHash h;
    cres_try(cres_ignore, h.insert(_map));
    return cres_success(h);
  }
  template<typename _T_>
  inline cres_qresult<_T_> ValueHash::value(const QString& _key) const
  {
    return value(_key).template value<_T_>();
  }
  template<typename _T_>
  inline cres_qresult<_T_> ValueHash::value(const QString& _key, const _T_& _default) const
  {
    if(contains(_key))
    {
      return value<_T_>(_key);
    }
    else
    {
      return cres_success(_default);
    }
  }

  template<typename _T_>
  inline cres_qresult<QHash<QString, _T_>> ValueHash::hash() const
  {
    QHash<QString, _T_> values;
    QHash<QString, Value> values_ = hash();
    for(auto it = values_.begin(); it != values_.end(); ++it)
    {
      cres_try(value, it.value().value<_T_>());
      values[it.key()] = value;
    }
    return cres_success(values);
  }
  template<typename _T_>
  inline cres_qresult<QMap<QString, _T_>> ValueHash::map() const
  {
    QMap<QString, _T_> values;
    QHash<QString, Value> values_ = hash();
    for(auto it = values_.begin(); it != values_.end(); ++it)
    {
      cres_try(value, it.value().value<_T_>());
      values[it.key()] = value;
    }
    return cres_success(values);
  }
  inline QMap<QString, Value> ValueHash::map() const
  {
    QMap<QString, Value> values;
    QHash<QString, Value> values_ = hash();
    for(auto it = values_.begin(); it != values_.end(); ++it)
    {
      values[it.key()] = it.value();
    }
    return values;
  }
  template<typename _T_>
  inline cres_qresult<void> ValueHash::checkContainsOnly() const
  {
    return checkContainsOnly(MetaTypeInformation<_T_>::uri());
  }
} // namespace knowCore

KNOWCORE_CORE_DECLARE_FORMATTER_PRINTABLE(knowCore::ValueHash);

KNOWCORE_DECLARE_FULL_METATYPE(knowCore, ValueHash);
