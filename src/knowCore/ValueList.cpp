#include "ValueList.h"

#include <QCborArray>
#include <QCborMap>
#include <QCryptographicHash>
#include <QJsonArray>
#include <QJsonObject>

using namespace knowCore;

struct ValueList::Private : public QSharedData
{
  QList<Value> values;
};

ValueList::ValueList() : d(new Private) {}

ValueList::ValueList(const QList<Value>& _values) : d(new Private) { d->values = _values; }

ValueList::ValueList(const ValueList& _rhs) : d(_rhs.d) {}

ValueList ValueList::operator=(const ValueList& _rhs)
{
  d = _rhs.d;
  return *this;
}

ValueList::~ValueList() {}

ValueList::const_iterator ValueList::begin() const { return d->values.begin(); }

ValueList::const_iterator ValueList::end() const { return d->values.end(); }

int ValueList::size() const { return d->values.size(); }

Value ValueList::at(int _index) const { return d->values.at(_index); }

bool ValueList::operator==(const ValueList& _rhs) const { return d->values == _rhs.d->values; }

bool ValueList::operator==(const QList<Value>& _rhs) const { return d->values == _rhs; }

Value ValueList::operator[](std::size_t _index) const { return d->values[_index]; }

bool ValueList::contains(const Value& _value) const { return d->values.contains(_value); }

cres_qresult<QByteArray> ValueList::md5() const
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  for(const Value& value : d->values)
  {
    cres_try(QByteArray value_md5, value.md5());
    hash.addData(value_md5);
  }
  return cres_success(hash.result());
}

cres_qresult<QJsonValue> ValueList::toJsonValue(const SerialisationContexts& _contexts) const
{
  QJsonArray array;
  for(const Value& value : d->values)
  {
    cres_try(QJsonObject value_json, value.toJsonObject(_contexts));
    array.append(value_json);
  }
  return cres_success(array);
}

cres_qresult<ValueList> ValueList::fromJsonValue(const QJsonValue& _json_value,
                                                 const DeserialisationContexts& _contexts)
{
  if(_json_value.isArray())
  {
    QList<Value> values;
    QJsonArray json_array = _json_value.toArray();
    for(int i = 0; i < json_array.size(); ++i)
    {
      QJsonValue json_value = json_array.at(i);
      if(json_value.isObject())
      {
        QJsonObject obj = json_value.toObject();
        cres_try(Value value, Value::fromJsonObject(obj, _contexts));
        values.append(value);
      }
      else
      {
        return cres_failure("Expected object got '{}' when parsing ValueList '{}'", json_value,
                            _json_value);
      }
    }
    return cres_success(values);
  }
  else
  {
    return cres_failure("Expect array got '{}'", _json_value);
  }
}

cres_qresult<QCborValue> ValueList::toCborValue(const SerialisationContexts& _contexts) const
{
  QCborArray array;
  for(const Value& value : d->values)
  {
    cres_try(QCborValue value_cbor, value.toCborMap(_contexts));
    array.append(value_cbor);
  }
  return cres_success(array);
}

cres_qresult<ValueList> ValueList::fromCborValue(const QCborValue& _cbor_value,
                                                 const DeserialisationContexts& _contexts)
{
  if(_cbor_value.isArray())
  {
    QList<Value> values;
    QCborArray cbor_array = _cbor_value.toArray();
    for(int i = 0; i < cbor_array.size(); ++i)
    {
      QCborValue cbor_value = cbor_array.at(i);
      if(cbor_value.isMap())
      {
        QCborMap obj = cbor_value.toMap();
        cres_try(Value value, Value::fromCborMap(obj, _contexts));
        values.append(value);
      }
      else
      {
        return cres_failure("Expected object got '{}' when parsing ValueList '{}'", cbor_value,
                            _cbor_value);
      }
    }
    return cres_success(values);
  }
  else
  {
    return cres_failure("Expect array got '{}'", _cbor_value);
  }
}

cres_qresult<QString> ValueList::printable() const
{
  QStringList strings;
  for(const Value& v : d->values)
  {
    cres_try(QString str, v.printable());
    strings.append(str);
  }
  return cres_success(clog_qt::qformat("[{}]", strings));
}

QList<Value> ValueList::values() const { return d->values; }

cres_qresult<void> ValueList::checkContainsOnly(const Uri& _uri) const
{
  for(const Value& value : d->values)
  {
    if(value.datatype() != _uri)
    {
      return cres_failure("Invalid type '{}' in list, expected: '{}'", value.datatype(), _uri);
    }
  }
  return cres_success();
}

bool ValueList::canConvertAllTo(const Uri& _uri) const
{
  for(const Value& value : d->values)
  {
    if(not value.canConvert(_uri))
    {
      return false;
    }
  }
  return true;
}

cres_qresult<ValueList> ValueList::fromVariants(const QVariantList& _t)
{
  QList<Value> values;
  for(const QVariant& var : _t)
  {
    cres_try(Value val, Value::fromVariant(var));
    values.append(val);
  }
  return cres_success(ValueList(values));
}

QVariantList ValueList::toVariantList() const
{
  QVariantList l;
  std::transform(d->values.begin(), d->values.end(), std::back_inserter(l),
                 [](const Value& v) { return v.toVariant(); });
  return l;
}

QVariant ValueList::toVariant() const { return toVariantList(); }

#include "MetaTypeImplementation.h"
#include <knowCore/Uris/askcore_datatype.h>

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(ValueList)
cres_qresult<QByteArray> md5(const ValueList& _value) const override { return _value.md5(); }
cres_qresult<QJsonValue> toJsonValue(const ValueList& _value,
                                     const SerialisationContexts& _contexts) const override
{
  return _value.toJsonValue(_contexts);
}
cres_qresult<void> fromJsonValue(ValueList* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts& _contexts) const override
{
  return cres_try_assign(_value, ValueList::fromJsonValue(_json_value, _contexts));
}
cres_qresult<QCborValue> toCborValue(const ValueList& _value,
                                     const SerialisationContexts& _contexts) const override
{
  return _value.toCborValue(_contexts);
}
cres_qresult<void> fromCborValue(ValueList* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts& _contexts) const override
{
  return cres_try_assign(_value, ValueList::fromCborValue(_cbor_value, _contexts));
}
cres_qresult<QString> printable(const ValueList& _value) const override
{
  return _value.printable();
}
cres_qresult<QString> toRdfLiteral(const ValueList& _value,
                                   const SerialisationContexts& _contexts) const override
{
  return toJsonString(_value, _contexts);
}
cres_qresult<void> fromRdfLiteral(ValueList* _value, const QString& _serialised,
                                  const DeserialisationContexts& _contexts) const override
{
  return fromJsonString(_value, _serialised, _contexts);
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(ValueList)

KNOWCORE_DEFINE_METATYPE(ValueList, Uris::askcore_datatype::valuelist, MetaTypeTraits::None)

template<>
struct knowCore::Comparator<ComparisonOperator::In, Value, ValueList>
{
  static inline cres_qresult<bool> compare(const Value& _left, const ValueList& _right)
  {
    return cres_success(_right.contains(_left));
  }
};

KNOWCORE_REGISTER_COMPARATORS_FROM((In), Value, ValueList)
