#pragma once

#include "Value.h"

namespace knowCore
{
  class ValueList
  {
  public:
    using const_iterator = QList<Value>::const_iterator;
  public:
    ValueList();
    ValueList(const QList<Value>& _value);
    ValueList(const ValueList& _list);
    ValueList operator=(const ValueList& _list);
    ~ValueList();
    int size() const;
    Value at(int _index) const;
    template<typename _T_>
    cres_qresult<_T_> at(int _index) const;
    template<typename _T_>
    cres_qresult<QList<_T_>> values() const;

    bool operator==(const ValueList& _rhs) const;
    bool operator==(const QList<Value>& _rhs) const;
    Value operator[](std::size_t _index) const;

    const_iterator begin() const;
    const_iterator end() const;

    /**
     * @return true if \p _value is part of \ref ValueList.
     */
    bool contains(const knowCore::Value& _value) const;
    template<typename _T_>
    bool contains(const _T_& _value) const
    {
      return contains(knowCore::Value::fromValue(_value));
    }

    cres_qresult<QByteArray> md5() const;
    cres_qresult<QJsonValue> toJsonValue(const SerialisationContexts& _contexts) const;
    static cres_qresult<ValueList> fromJsonValue(const QJsonValue& _value,
                                                 const DeserialisationContexts& _contexts);
    cres_qresult<QCborValue> toCborValue(const SerialisationContexts& _contexts) const;
    static cres_qresult<ValueList> fromCborValue(const QCborValue& _value,
                                                 const DeserialisationContexts& _contexts);
    cres_qresult<QString> printable() const;
    QList<Value> values() const;
    /**
     * Check that the list only contains value of type \ref _uri.
     * It returns success if that is true.
     * It returns a failure with an error message in case a value has a
     * different type in the list.
     */
    cres_qresult<void> checkContainsOnly(const Uri& _uri) const;
    template<typename _T_>
    inline cres_qresult<void> checkContainsOnly() const;
    /**
     * Allow direct casting to \ref Value.
     */
    operator Value() const { return Value::fromValue(*this); }
    /**
     * Check that the list only contains value that can be casted to type \ref _uri.
     * It returns success if that is true.
     * It returns a failure with an error message in case a value has a
     * different type in the list.
     */
    bool canConvertAllTo(const Uri& _uri) const;
    template<typename _T_>
    bool canConvertAllTo() const;
    /**
     * Create a list from a QList. \p _T_ needs to be registered in knowCore MetaType.
     */
    template<typename _T_>
    inline static const ValueList fromValues(const QList<_T_>& _t);
    static cres_qresult<ValueList> fromVariants(const QVariantList& _t);
    /**
     * @return a list of variant
     */
    QVariantList toVariantList() const;
    /**
     * @return a variant
     */
    QVariant toVariant() const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };

  template<typename _T_>
  cres_qresult<_T_> ValueList::at(int _index) const
  {
    return at(_index).template value<_T_>();
  }
  template<typename _T_>
  cres_qresult<QList<_T_>> ValueList::values() const
  {
    QList<_T_> list;
    for(int i = 0; i < size(); ++i)
    {
      cres_try(_T_ value, at<_T_>(i));
      list.append(value);
    }
    return cres_success(list);
  }
  template<typename _T_>
  cres_qresult<void> ValueList::checkContainsOnly() const
  {
    return checkContainsOnly(MetaTypeInformation<_T_>::uri());
  }
  template<typename _T_>
  bool ValueList::canConvertAllTo() const
  {
    return canConvertAllTo(MetaTypeInformation<_T_>::uri());
  }
  template<typename _T_>
  const ValueList ValueList::fromValues(const QList<_T_>& _t)
  {
    QList<Value> l;
    std::transform(_t.begin(), _t.end(), std::back_inserter(l),
                   [](const _T_& _t) -> Value { return Value::fromValue(_t); });
    return l;
  }

} // namespace knowCore

KNOWCORE_CORE_DECLARE_FORMATTER_PRINTABLE(knowCore::ValueList);
KNOWCORE_DECLARE_FULL_METATYPE(knowCore, ValueList);
