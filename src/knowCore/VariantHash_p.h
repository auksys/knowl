#include <QHash>
#include <QVariant>

#include <clog_qt>

inline uint qHash(const QVariant& var, uint seed = 0)
{
  switch(var.userType())
  {
  case QVariant::Int:
    return qHash(var.toInt(), seed);
  case QVariant::UInt:
    return qHash(var.toUInt(), seed);
  case QVariant::Bool:
    return qHash(var.toUInt(), seed);
  case QVariant::Double:
    return qHash(var.toUInt(), seed);
  case QVariant::LongLong:
    return qHash(var.toLongLong(), seed);
  case QVariant::ULongLong:
    return qHash(var.toULongLong(), seed);
  case QVariant::String:
    return qHash(var.toString(), seed);
  case QVariant::Char:
    return qHash(var.toChar(), seed);
  case QVariant::StringList:
    return qHash(var.toString(), seed);
  case QVariant::ByteArray:
    return qHash(var.toByteArray(), seed);
  case QVariant::Date:
  case QVariant::Time:
  case QVariant::DateTime:
  case QVariant::Url:
  case QVariant::Locale:
  case QVariant::RegExp:
    return qHash(var.toString(), seed);
  case QVariant::Map:
  case QVariant::List:
  case QVariant::BitArray:
  case QVariant::Size:
  case QVariant::SizeF:
  case QVariant::Rect:
  case QVariant::LineF:
  case QVariant::Line:
  case QVariant::RectF:
  case QVariant::Point:
  case QVariant::PointF:
  default:
    // not supported yet
    break;
  case QVariant::UserType:
  case QVariant::Invalid:
    return -1;
  }
  clog_warning("qHash(QVariant) unkwown type: {}", var.userType());
  // could not generate a hash for the given variant
  return -1;
}
