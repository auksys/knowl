#ifndef _KNOW_CORE_VECTOR_H_
#define _KNOW_CORE_VECTOR_H_

#include <clog_qt>
#include <cres_qt>

#include <QSharedDataPointer>

namespace knowCore
{
  template<typename _T_, std::size_t _dimension>
  class Vector
  {
  public:
    typedef _T_ Scalar;
    static constexpr std::size_t Dimension = _dimension;
  public:
    Vector() : d(new Private) {}
    Vector(const _T_ _v[]) : d(new Private) { memcpy(d->data, _v, _dimension * sizeof(_T_)); }
    template<typename... Params>
      requires std::conjunction_v<std::is_convertible<Params, _T_>...>
    Vector(Params... _vs) : d(new Private)
    {
      setValue(_vs...);
    }
    template<template<typename, std::size_t> class _C_>
    Vector(const _C_<_T_, _dimension>& _v) : d(new Private)
    {
      copyFrom(_v);
    }
    template<typename... Params>
    void setValue(Params... _vs)
    {
      static_assert(sizeof...(Params) == _dimension);
      setValue<0>(_vs...);
    }
    template<template<typename> class _C_>
    cres_qresult<void> copyFrom(const _C_<_T_>& _v)
    {
      if(_v.size() == _dimension)
      {
        for(std::size_t i = 0; i < _dimension; ++i)
        {
          d->data[i] = _v[i];
        }
        return cres_success();
      }
      else
      {
        return cres_failure("Invalid size of container got {} expected {}", _v.size(), _dimension);
      }
    }
    template<template<typename, std::size_t> class _C_>
    void copyFrom(const _C_<_T_, _dimension>& _v)
    {
      for(std::size_t i = 0; i < _dimension; ++i)
      {
        d->data[i] = _v[i];
      }
    }
    operator QList<_T_>() const
    {
      QList<_T_> r;
      for(std::size_t i = 0; i < _dimension; ++i)
      {
        r.append(d->data[i]);
      }
      return r;
    }
  public:
    _T_ operator[](std::size_t _idx) const { return d->data[_idx]; }
    _T_& operator[](std::size_t _idx) { return d->data[_idx]; }
  public:
    bool operator==(const Vector<_T_, _dimension>& _rhs) const
    {
      return memcmp(d->data, _rhs.d->data, _dimension * sizeof(_T_)) == 0;
    }
    bool operator!=(const Vector<_T_, _dimension>& _rhs) const { return not(*this == _rhs); }
    bool operator<(const Vector<_T_, _dimension>& _rhs) const
    {
      for(std::size_t i = 0; i < _dimension; ++i)
      {
        if(d->data[i] < _rhs.d->data[i])
          return true;
      }
      return false;
    }
    const _T_* data() const { return d->data; }
  private:
    template<std::size_t _idx, typename... Params>
    void setValue(_T_ _v, Params... _vs)
    {
      d->data[_idx] = _v;
      setValue<_idx + 1>(_vs...);
    }
    template<std::size_t _idx>
    void setValue(_T_ _v)
    {
      d->data[_idx] = _v;
    }
  private:
    struct Private : public QSharedData
    {
      Private() {}
      Private(const Private& _rhs) : QSharedData()
      {
        memcpy(data, _rhs.data, _dimension * sizeof(_T_));
      }
      _T_ data[_dimension];
    };
    QSharedDataPointer<Private> d;
  };
} // namespace knowCore

// #include <QDebug>
//
// template<typename _T_, std::size_t _dimension>
// QDebug operator<<(QDebug dbg, const knowCore::Vector<_T_, _dimension> & v)
// {
//   dbg << "[";
//   for(std::size_t i = 0; i < _dimension; ++i)
//   {
//     if(i!= 0)
//       dbg << ", ";
//     dbg << v[i];
//   }
//   dbg << "]";
//   return dbg;
// }

#endif
