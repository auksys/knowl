#include <QWeakPointer>

namespace knowCore
{
  template<typename _TOwner_>
  class WeakReference;
  template<typename _TOwner_>
  inline uint qHash(const WeakReference<_TOwner_>& key, uint seed = 0);
  /**
   * @ingroup knowCore
   * This class allows to hold a weak reference to a private class that is shared between
   * instances using a QSharedPointer.
   *
   * Example of use is \ref kDB::Repository::Connection.
   */
  template<typename _TOwner_>
  class WeakReference
  {
    friend _TOwner_;
    friend uint qHash<>(const WeakReference<_TOwner_>&, uint);
  public:
    WeakReference() {}
    WeakReference(const _TOwner_& _t) : d(_t.d) {}
    bool operator==(const WeakReference<_TOwner_>& _rhs) const { return d == _rhs.d; }
  private:
    QWeakPointer<typename _TOwner_::Private> d;
  };
  template<typename _TOwner_>
#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
  inline uint qHash(const WeakReference<_TOwner_>& key, uint seed)
  {
    return ::qHash(key.d.data(), seed);
  }
#else
  inline uint qHash(const WeakReference<_TOwner_>& key, uint seed)
  {
    return ::qHash(key.d.toStrongRef().data(), seed);
  }
#endif
} // namespace knowCore
