#include "WhoAmI.h"

#include <QString>

#ifdef Q_OS_WIN
#include <Windows.h>
#elif defined(Q_OS_UNIX)
#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>
#else
#error "Not implemented for your OS"
#endif

QString knowCore::whoAmI()
{

#ifdef Q_OS_WIN
  char acUserName[MAX_USERNAME];
  DWORD nUserName = sizeof(acUserName);
  if(GetUserName(acUserName, &nUserName))
    return acUserName);
  return "";
#elif defined(Q_OS_UNIX)
  QString result;

  uid_t uid = geteuid();
  struct passwd* pw = getpwuid(uid); // static data, no need to free
  if(pw)
  {
    result = pw->pw_name;
  }
  return result;
#else
#error "Not implemented for your OS"
#endif
}
