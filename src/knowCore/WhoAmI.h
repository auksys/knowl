class QString;

namespace knowCore
{
  /**
   * @return the user name
   *
   * That function is *not* reentrent on unix.
   */
  QString whoAmI();
} // namespace knowCore
