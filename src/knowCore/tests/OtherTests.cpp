#include "OtherTests.h"

#include <cext_nt>
#include <clog_qt>

#include "../Global.h"
#include "../MonitoredValue.h"

namespace
{
  using Name = cext_named_type<QString, struct NameTag>;
}

void OtherTests::testNamedType()
{
  Name hello_n("hello"_kCs);

  QCOMPARE(clog_qt::qformat("{}", hello_n), "hello");
}

void OtherTests::testMonitoredValue()
{
  knowCore::MonitoredValueController<int> mvc(0);
  knowCore::MonitoredValue<int> mv = mvc.value();
  QCOMPARE(mv.value(), 0);
  mvc.setValue(1);
  QCOMPARE(mv.value(), 1);
  QCOMPARE(mvc.value().value(), 1);

  // Test monitors
  int value_f = 0;
  int value_g = 0;
  mv.addMonitor([&value_f](const int& _value) { value_f = _value; });
  mvc.setValue(2);
  QCOMPARE(mv.value(), 2);
  QCOMPARE(mvc.value().value(), 2);
  QCOMPARE(value_f, 2);
  QCOMPARE(value_g, 0);

  mvc.value().addMonitor([&value_g](const int& _value) { value_g = _value; });
  mvc.setValue(3);
  QCOMPARE(mv.value(), 3);
  QCOMPARE(mvc.value().value(), 3);
  QCOMPARE(value_f, 3);
  QCOMPARE(value_g, 3);
}

QTEST_MAIN(OtherTests)
