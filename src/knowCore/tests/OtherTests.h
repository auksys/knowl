#include <QtTest/QtTest>

class OtherTests : public QObject
{
  Q_OBJECT
private slots:
  void testNamedType();
  void testMonitoredValue();
};
