#include "TestArray.h"

#include "../TypeDefinitions.h"

void TestArray::testAccess()
{
  using namespace knowCore;
  Int32Array array2({2, 3});
  array2(0, 0) = 10;
  array2(1, 0) = 12;
  array2(0, 1) = 8;
  array2(1, 1) = 9;
  array2(0, 2) = 3;
  array2(1, 2) = 5;
  QVector<qint32> expected_data{10, 12, 8, 9, 3, 5};
  QCOMPARE(array2.data(), expected_data);

  Int32Array array3({1, 2, 3});
  array3(0, 0, 0) = 10;
  array3(0, 1, 0) = 12;
  array3(0, 0, 1) = 8;
  array3(0, 1, 1) = 9;
  array3(0, 0, 2) = 3;
  array3(0, 1, 2) = 5;
  QCOMPARE(array3.data(), expected_data);
}

QTEST_MAIN(TestArray)
