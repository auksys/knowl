#include "TestBigNumber.h"

#include <knowCore/BigNumberList.h>
#include <knowCore/Test.h>

#include "../Numeric_p.h"

#define COMPARE_BN(_EXPR_, _EXPECT_)                                                               \
  {                                                                                                \
    const auto [__success__, __value__, __error__] = _EXPR_;                                       \
    QVERIFY2(__success__, qPrintable(__error__.value().get_message()));                            \
    QCOMPARE(__value__.value(), _EXPECT_);                                                         \
  }

void TestBigNumber::do_test(qint16 weight, knowCore::BigNumber::Sign sign, qint16 dscale,
                            const QList<quint16>& digits, bool has_int64, qint64 _int64,
                            bool has_uint64, quint64 _uint64, bool has_double, double _double,
                            const QString& _string)
{
  BigNumber b1(weight, sign, dscale, digits);
  Q_UNUSED(_string);
  QCOMPARE(b1.toString(), _string);
  BigNumber b3 = CRES_QVERIFY(BigNumber::fromString(_string));
  QCOMPARE(b3, b1);

  if(has_int64)
  {
    COMPARE_BN(b1.toInt64(), _int64);
    BigNumber b2 = BigNumber(_int64);
    QCOMPARE(b1, b2);
  }
  else
  {
    QVERIFY(not b1.toInt64().is_successful());
  }
  if(has_uint64)
  {
    COMPARE_BN(b1.toUInt64(), _uint64);
    BigNumber b2 = BigNumber(_uint64);
    QCOMPARE(b1, b2);
  }
  else
  {
    QVERIFY(not b1.toUInt64().is_successful());
  }
  if(has_double)
  {
    QCOMPARE(b1.toDouble(), _double);
    BigNumber b2 = BigNumber(_double);
    QCOMPARE(b1, b2);
  }
}

void TestBigNumber::testCreation()
{
  auto n = BigNumber::fromString("915249906").expect_success();

  //  weight,                      sign, dscale,                    digits, has_int64 _int64,
  //  has_uint64, _uint64,       has_double,           double,                 string
  do_test(0, BigNumber::Sign::Positive, 0, {10}, true, 10, true, 10ull, true, 10.0, "10");
  do_test(0, BigNumber::Sign::Negative, 0, {10}, true, -10, false, 0ull, true, -10.0, "-10");
  do_test(0, BigNumber::Sign::Positive, 2, {10, 200}, false, 0, false, 0ull, true, 10.02, "10.02");
  do_test(0, BigNumber::Sign::Positive, 4, {10, 2}, false, 0, false, 0ull, true, 10.0002,
          "10.0002");
  do_test(0, BigNumber::Sign::Positive, 5, {10, 0, 2000}, false, 0, false, 0ull, true, 10.00002,
          "10.00002");
  do_test(1, BigNumber::Sign::Positive, 5, {1, 0, 0, 2000}, false, 0, false, 0ull, true,
          10000.00002, "10000.00002");
  do_test(-2, BigNumber::Sign::Positive, 5, {2000}, false, 0, false, 0ull, true, 0.00002,
          "0.00002");
  do_test(1, BigNumber::Sign::Positive, 0, {1}, true, 10000, true, 10000ull, true, 10000, "10000");
  do_test(1, BigNumber::Sign::Positive, 4, {1}, false, 0, false, 0ull, true, 10000, "10000.0000");
  do_test(0, BigNumber::Sign::Positive, 0, {}, true, 0, true, 0ull, true, 0, "0");
  do_test(0, BigNumber::Sign::Positive, 2, {}, false, 0, false, 0ull, true, 0, "0.00");
  do_test(1, BigNumber::Sign::Positive, 0, {1, 1}, true, 10001, true, 10001ull, true, 10001,
          "10001");
  do_test(2, BigNumber::Sign::Positive, 0, {1}, true, 100000000, true, 100000000ull, true,
          100000000, "100000000");
  do_test(2, BigNumber::Sign::Positive, 0, {1, 0, 1}, true, 100000001, true, 100000001ull, true,
          100000001, "100000001");
  do_test(2, BigNumber::Sign::Positive, 0, {1, 0, 1}, true, 100000001, true, 100000001ull, true,
          100000001, "100000001");
  do_test(2, BigNumber::Sign::Positive, 4, {1, 0, 0, 1}, false, 0, false, 0ull, true,
          100000000.0001, "100000000.0001");
  do_test(4, BigNumber::Sign::Positive, 0, {1844, 6744, 737, 955, 1615}, false, 0, true,
          18446744073709551615ull, false, 0.0, "18446744073709551615");
  do_test(2, BigNumber::Sign::Positive, 6, {9, 1524, 9906}, false, 0, false, 0, true, 915249906.0,
          "915249906.000000");
  do_test(1, BigNumber::Sign::Positive, 6, {9152, 4990}, false, 0, false, 0, true, 91524990.0,
          "91524990.000000");
}

void TestBigNumber::testNaN()
{
  BigNumber n = BigNumber::nan();
  QVERIFY(n.isNaN());
  BigNumber n1 = BigNumber(NAN);
  QVERIFY(n1.isNaN());
  QCOMPARE(n, n1);
  BigNumber n2 = CRES_QVERIFY(BigNumber::fromString("NaN"));
  QVERIFY(n2.isNaN());
  QCOMPARE(n, n2);
  QCOMPARE(n1, n2);
}

void TestBigNumber::testVariant()
{
  BigNumber n = BigNumber(10ull);
  QVariant v = n.toVariant();
  QCOMPARE(v.typeId(), QVariant::ULongLong);
  QCOMPARE(v.value<quint64>(), 10ull);
  n = BigNumber(-10ll);
  v = n.toVariant();
  QCOMPARE(v.typeId(), QVariant::LongLong);
  QCOMPARE(v.value<qint64>(), -10ll);
  n = BigNumber(18446744073709551615ull);
  v = n.toVariant();
  QCOMPARE(v.typeId(), QVariant::ULongLong);
  QCOMPARE(v.value<quint64>(), 18446744073709551615ull);
  n = CRES_QVERIFY(BigNumber::fromString("18446744073709551615"));
  v = n.toVariant();
  QCOMPARE(v.typeId(), QVariant::ULongLong);
  QCOMPARE(v.value<quint64>(), 18446744073709551615ull);
  n = CRES_QVERIFY(BigNumber::fromString("18446744073709551616"));
  v = n.toVariant();
  QCOMPARE(v.userType(), knowCore::MetaTypeInformation<knowCore::BigNumber>::id());
  QCOMPARE(v.value<BigNumber>(), n);
  n = BigNumber(std::numeric_limits<qint64>::min());
  v = n.toVariant();
  QCOMPARE(v.typeId(), QVariant::LongLong);
  QCOMPARE(v.value<qint64>(), std::numeric_limits<qint64>::min());
  n = CRES_QVERIFY(BigNumber::fromString("-9223372036854775808"));
  v = n.toVariant();
  QCOMPARE(v.typeId(), QVariant::LongLong);
  QCOMPARE(v.value<qint64>(), std::numeric_limits<qint64>::min());
  n = CRES_QVERIFY(BigNumber::fromString("-9223372036854775809"));
  v = n.toVariant();
  QCOMPARE(v.userType(), knowCore::MetaTypeInformation<knowCore::BigNumber>::id());
  QCOMPARE(v.value<BigNumber>(), n);
  n = CRES_QVERIFY(BigNumber::fromString("1.123"));
  v = n.toVariant();
  QCOMPARE(v.typeId(), QVariant::Double);
  QCOMPARE(v.value<double>(), 1.123);
  n = CRES_QVERIFY(BigNumber::fromString("-922337203685.4775808"));
  v = n.toVariant();
  QCOMPARE(v.userType(), knowCore::MetaTypeInformation<knowCore::BigNumber>::id());
  QCOMPARE(v.value<BigNumber>(), n);
  n = CRES_QVERIFY(BigNumber::fromString("-18"));
  v = n.toVariant();
  QCOMPARE(v.typeId(), QVariant::LongLong);
  QCOMPARE(v.value<qint64>(), -18);
}

void TestBigNumber::testString()
{
  BigNumber n = CRES_QVERIFY(BigNumber::fromString("-0.00002"));
  QCOMPARE(n, BigNumber(-0.00002));
  QCOMPARE(n.toDouble(), -0.00002);
}

void TestBigNumber::testList()
{
  QList<qint64> list_int = {1, 2, 3};
  knowCore::BigNumberList bl(list_int.begin(), list_int.end());
}

#define TEST_OP(a, op, b, r) QCOMPARE(bn(a) op bn(b), bn(r))

#define TEST_COP(a, op, b, r) QCOMPARE(bn(a) op bn(b), r)

void TestBigNumber::testOperators()
{
  using bn = knowCore::BigNumber;
  TEST_OP(1ll, +, 1ll, 2ll);
  TEST_OP(10000ll, +, 1ll, 10001ll);
  TEST_OP(10000ll, +, -1ll, 9999ll);
  TEST_OP(1ll, +, -10000ll, -9999ll);
  TEST_OP(1ll, -, 1ll, 0ll);
  TEST_OP(-1ll, -, 1ll, -2ll);
  TEST_OP(1ll, -, -1ll, 2ll);
  TEST_OP(10000ll, -, 1ll, 9999ll);
  TEST_OP(10000ll, -, -1ll, 10001ll);
  TEST_OP(1ll, -, -10000ll, 10001ll);
  TEST_OP(1ll, *, -2ll, -2ll);
  TEST_OP(2ll, /, -2ll, -1ll);
  TEST_OP(2ll, /, 2ll, 1ll);
  TEST_OP(10000ll, /, 2ll, 5000ll);
  TEST_OP(5000ll, *, 2ll, 10000ll);

  TEST_COP(1ll, <, 1ll, false);
  TEST_COP(0ll, <, 1ll, true);
  TEST_COP(2ll, <, 1ll, false);
  TEST_COP(1ll, <=, 1ll, true);
  TEST_COP(0ll, <=, 1ll, true);
  TEST_COP(2ll, <=, 1ll, false);
  TEST_COP(1ll, >, 1ll, false);
  TEST_COP(0ll, >, 1ll, false);
  TEST_COP(2ll, >, 1ll, true);
  TEST_COP(1ll, >=, 1ll, true);
  TEST_COP(0ll, >=, 1ll, false);
  TEST_COP(2ll, >=, 1ll, true);
}

#define TEST_NOT_OVERFLOW(_expr_, _value_)                                                         \
  {                                                                                                \
    auto [overflow, result] = (_expr_);                                                            \
    QVERIFY(not overflow);                                                                         \
    QCOMPARE(result, _value_);                                                                     \
  }

#define TEST_OVERFLOW(_expr_)                                                                      \
  {                                                                                                \
    auto [overflow, result] = (_expr_);                                                            \
    QVERIFY(overflow);                                                                             \
  }

void TestBigNumber::testOverflow()
{
  using namespace knowCore;
  TEST_NOT_OVERFLOW(mul_overflow(1, 100), 100);
  TEST_NOT_OVERFLOW(mul_overflow(0, 100), 0);
  TEST_OVERFLOW(mul_overflow(std::numeric_limits<quint64>::max(), 100));
  TEST_NOT_OVERFLOW(add_overflow(1, 100), 101);
  TEST_NOT_OVERFLOW(add_overflow(0, 100), 100);
  TEST_OVERFLOW(add_overflow(std::numeric_limits<quint64>::max(), 100));
}

void TestBigNumber::testInfinity()
{
  using namespace knowCore;
  QVERIFY(BigNumber(INFINITY).isInfinite());
  QVERIFY(BigNumber(INFINITY) > 0);
  QVERIFY(BigNumber(-INFINITY) < 0);
  QVERIFY(BigNumber(INFINITY) == BigNumber::positiveInfinite());
  QVERIFY(BigNumber(-INFINITY) == BigNumber::negativeInfinite());
  QCOMPARE(1 + BigNumber::positiveInfinite(), BigNumber::positiveInfinite());
  QCOMPARE(BigNumber::positiveInfinite() - BigNumber::positiveInfinite(), BigNumber::nan());
  QCOMPARE(BigNumber::positiveInfinite() / BigNumber::positiveInfinite(), BigNumber::nan());
  QCOMPARE(1 / BigNumber::positiveInfinite(), 0);
}

void TestBigNumber::testTruncate()
{
  using namespace knowCore;
  CRES_QCOMPARE(BigNumber(10.02).toInt64(true), 10);
  CRES_QVERIFY_FAILURE(BigNumber(10.02).toInt64());
  CRES_QCOMPARE(BigNumber(-10.02).toInt64(true), -10);
  CRES_QVERIFY_FAILURE(BigNumber(-10.02).toInt64());
  CRES_QVERIFY_FAILURE(BigNumber(-10.02).toUInt64(true));
  CRES_QVERIFY_FAILURE(BigNumber(-10.02).toUInt64());
}

QTEST_MAIN(TestBigNumber)
