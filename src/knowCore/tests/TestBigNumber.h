#include <QtTest/QtTest>

#include "../BigNumber.h"

class TestBigNumber : public QObject
{
  Q_OBJECT
  using BigNumber = knowCore::BigNumber;
private slots:
  void testCreation();
  void testNaN();
  void testVariant();
  void testString();
  void testList();
  void testOperators();
  void testOverflow();
  void testInfinity();
  void testTruncate();
private:
  void do_test(qint16 weight, BigNumber::Sign sign, qint16 dscale, const QList<quint16>& digits,
               bool has_int64, qint64 _int64, bool has_uint64, quint64 _uint64, bool has_double,
               double _double, const QString& _string);
};
