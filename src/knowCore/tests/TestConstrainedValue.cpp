#include "TestConstrainedValue.h"

#include <knowCore/ConstrainedValue.h>
#include <knowCore/Test.h>
#include <knowCore/TypeDefinitions.h>

using ConstrainedValue = knowCore::ConstrainedValue;

void TestConstrainedValue::testEquality()
{
  ConstrainedValue cv;
  cv.apply(10, ConstrainedValue::Type::Equal);
  CRES_QCOMPARE(cv.check(10), true);
  CRES_QCOMPARE(cv.check(11), false);
}

void TestConstrainedValue::testBounds()
{
  ConstrainedValue cv;
  cv.apply(10, ConstrainedValue::Type::InferiorEqual);
  CRES_QCOMPARE(cv.check(7), true);
  CRES_QCOMPARE(cv.check(8), true);
  CRES_QCOMPARE(cv.check(9), true);
  CRES_QCOMPARE(cv.check(10), true);
  CRES_QCOMPARE(cv.check(11), false);
  cv.apply(10, ConstrainedValue::Type::Inferior);
  CRES_QCOMPARE(cv.check(7), true);
  CRES_QCOMPARE(cv.check(8), true);
  CRES_QCOMPARE(cv.check(9), true);
  CRES_QCOMPARE(cv.check(10), false);
  CRES_QCOMPARE(cv.check(11), false);
  cv.apply(8, ConstrainedValue::Type::SuperiorEqual);
  CRES_QCOMPARE(cv.check(7), false);
  CRES_QCOMPARE(cv.check(8), true);
  CRES_QCOMPARE(cv.check(9), true);
  CRES_QCOMPARE(cv.check(10), false);
  CRES_QCOMPARE(cv.check(11), false);
  cv.apply(8, ConstrainedValue::Type::Superior);
  CRES_QCOMPARE(cv.check(7), false);
  CRES_QCOMPARE(cv.check(8), false);
  CRES_QCOMPARE(cv.check(9), true);
  CRES_QCOMPARE(cv.check(10), false);
  CRES_QCOMPARE(cv.check(11), false);
}

void TestConstrainedValue::testEqualityBounds()
{
  ConstrainedValue cv;
  cv.apply(10, ConstrainedValue::Type::Equal);
  cv.apply(8, ConstrainedValue::Type::Superior);
  cv.apply(12, ConstrainedValue::Type::Inferior);
  CRES_QCOMPARE(cv.check(8), false);
  CRES_QCOMPARE(cv.check(9), false);
  CRES_QCOMPARE(cv.check(10), true);
  CRES_QCOMPARE(cv.check(11), false);
  CRES_QCOMPARE(cv.check(12), false);
  cv.apply(10, ConstrainedValue::Type::Inferior);
  CRES_QCOMPARE(cv.check(8), false);
  CRES_QCOMPARE(cv.check(9), false);
  CRES_QCOMPARE(cv.check(10), false);
  CRES_QCOMPARE(cv.check(11), false);
  CRES_QCOMPARE(cv.check(12), false);
}

QTEST_MAIN(TestConstrainedValue)
