#include <QtTest/QtTest>

class TestConstrainedValue : public QObject
{
  Q_OBJECT
private slots:
  void testEquality();
  void testBounds();
  void testEqualityBounds();
};
