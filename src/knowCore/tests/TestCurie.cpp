#include "TestCurie.h"

#include "../Curie.h"
#include "../Test.h"
#include "../Uri.h"
#include "../UriManager.h"

void TestCurie::testCreation()
{
  using namespace knowCore;
  Curie curie("prefix", "suffix");
  QCOMPARE(curie.prefix(), QStringLiteral("prefix"));
  QCOMPARE(curie.suffix(), QStringLiteral("suffix"));

  Curie curie2("prefix", "suffix");
  QVERIFY(curie == curie2);

  Curie curie3(curie);
  QVERIFY(curie == curie3);
  QVERIFY(curie2 == curie3);
  QCOMPARE(curie3.prefix(), QStringLiteral("prefix"));
  QCOMPARE(curie3.suffix(), QStringLiteral("suffix"));

  Curie curie4("prefix4", "suffix4");
  QCOMPARE(curie4.prefix(), QStringLiteral("prefix4"));
  QCOMPARE(curie4.suffix(), QStringLiteral("suffix4"));
  QVERIFY(not(curie == curie4));

  curie4 = curie;
  QCOMPARE(curie4.prefix(), QStringLiteral("prefix"));
  QCOMPARE(curie4.suffix(), QStringLiteral("suffix"));
  QVERIFY(curie == curie4);
}

void TestCurie::testResolution()
{
  using namespace knowCore;

  Curie curie1("prefix1", "suffix");
  Curie curie2("prefix2", "suffix");
  Curie curie3("prefix3", "suffix");

  UriManager manager;

  QVERIFY(not curie1.canResolve(&manager));
  QVERIFY(not curie2.canResolve(&manager));
  QVERIFY(not curie3.canResolve(&manager));

  manager.addPrefix("prefix1", "http://example.org/some/prefix1/");
  manager.addPrefix("prefix2", "http://example.org/some/prefix2");
  manager.addPrefix("prefix3", "http://example.org/some/prefix3#");

  QVERIFY(curie1.canResolve(&manager));
  QVERIFY(curie2.canResolve(&manager));
  QVERIFY(curie3.canResolve(&manager));

  QCOMPARE(curie1.resolve(&manager), Uri("http://example.org/some/prefix1/suffix"));
  QCOMPARE(curie2.resolve(&manager), Uri("http://example.org/some/prefix2suffix"));
  QCOMPARE(curie3.resolve(&manager), Uri("http://example.org/some/prefix3#suffix"));
}

QTEST_MAIN(TestCurie)
