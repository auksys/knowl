#include <QtTest/QtTest>

class TestCurie : public QObject
{
  Q_OBJECT
private slots:
  void testCreation();
  void testResolution();
};
