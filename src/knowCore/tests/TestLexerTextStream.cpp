#include "TestLexerTextStream.h"

#include "../Global.h"
#include "../LexerTextStream.h"
#include <clog_qt>

#include <QBuffer>

#define VERIFY_ELT(_ELT_, _CONTENT_, _LINE_, _COLUMN_)                                             \
  QCOMPARE(_ELT_.content, QStringLiteral(_CONTENT_));                                              \
  QCOMPARE(_ELT_.line, _LINE_);                                                                    \
  QCOMPARE(_ELT_.column, _COLUMN_);                                                                \
  QCOMPARE(_ELT_.eof, false)

#define VERIFY_NEXT_ELT(_CONTENT_, _LINE_, _COLUMN_)                                               \
  {                                                                                                \
    knowCore::LexerTextStream::Element elt = _stream->getNextNonSeparatorChar();                   \
    VERIFY_ELT(elt, _CONTENT_, _LINE_, _COLUMN_);                                                  \
  }

#define VERIFY_NEXT_STRING(_CONTENT_, _LINE_, _COLUMN_, _FINISHED_, _TERM_)                        \
  {                                                                                                \
    auto [elt, finished] = _stream->getString(_TERM_);                                             \
    QCOMPARE(finished, _FINISHED_);                                                                \
    VERIFY_ELT(elt, _CONTENT_, _LINE_, _COLUMN_);                                                  \
  }

#define VERIFY_NEXT_DIGIT(_CONTENT_, _LINE_, _COLUMN_, _INTEGER_)                                  \
  {                                                                                                \
    auto [elt, integer] = _stream->getDigit(_stream->getNextNonSeparatorChar());                   \
    QCOMPARE(integer, _INTEGER_);                                                                  \
    QCOMPARE(elt.content, QStringLiteral(_CONTENT_));                                              \
    QCOMPARE(elt.line, _LINE_);                                                                    \
    QCOMPARE(elt.column, _COLUMN_);                                                                \
  }

QString TestLexerTextStream::string(bool _remove_last_return)
{
  QString str = R"SSS(ab
c "hello" 13 14e-3 d"u
" "x\"" "x\"a" """l
asd
"" f""" f "c" "d" ""
"e"g 14)
)SSS";
  if(_remove_last_return)
  {
    str.chop(1);
  }
  return str;
}

void TestLexerTextStream::testStream(knowCore::LexerTextStream* _stream, bool _remove_last_return,
                                     bool* _completed)
{
  VERIFY_NEXT_ELT("a", 1, 1);
  VERIFY_NEXT_ELT("b", 1, 2);
  VERIFY_NEXT_ELT("c", 2, 1);
  VERIFY_NEXT_ELT("\"", 2, 3);
  VERIFY_NEXT_STRING("hello", 2, 3, true, "\"");
  VERIFY_NEXT_DIGIT("13", 2, 11, true);
  VERIFY_NEXT_DIGIT("14e-3", 2, 14, false);
  VERIFY_NEXT_ELT("d", 2, 20);
  VERIFY_NEXT_ELT("\"", 2, 21);
  knowCore::LexerTextStream::Element e1 = _stream->getNextChar();
  _stream->unget(_stream->getNextChar());
  _stream->unget(e1);
  QCOMPARE(e1.content, "u");
  QCOMPARE(e1.column, 22);
  VERIFY_NEXT_STRING("u", 2, 21, false, "\"");
  VERIFY_NEXT_ELT("\"", 3, 1);
  VERIFY_NEXT_ELT("\"", 3, 3);
  VERIFY_NEXT_STRING("x\"", 3, 3, true, "\"");
  VERIFY_NEXT_ELT("\"", 3, 9);
  VERIFY_NEXT_STRING("x\"a", 3, 9, true, "\"");
  VERIFY_NEXT_ELT("\"", 3, 16);
  VERIFY_NEXT_ELT("\"", 3, 17);
  VERIFY_NEXT_ELT("\"", 3, 18);
  VERIFY_NEXT_STRING(R"lll(l
asd
"" f)lll",
                     3, 16, true, "\"\"\"");
  VERIFY_NEXT_ELT("f", 5, 9);
  VERIFY_NEXT_ELT("\"", 5, 11);
  VERIFY_NEXT_STRING("c", 5, 11, true, "\"");
  VERIFY_NEXT_ELT("\"", 5, 15);
  VERIFY_NEXT_STRING("d", 5, 15, true, "\"");
  VERIFY_NEXT_ELT("\"", 5, 19);
  {
    knowCore::LexerTextStream::Element elt = _stream->getNextNonSeparatorChar();
    VERIFY_ELT(elt, "\"", 5, 20);
    _stream->unget(elt);
  }
  VERIFY_NEXT_STRING("", 5, 19, true, "\"");
  VERIFY_NEXT_ELT("\"", 6, 1);
  VERIFY_NEXT_STRING("e", 6, 1, true, "\"");
  VERIFY_NEXT_ELT("g", 6, 4);
  VERIFY_NEXT_DIGIT("14", 6, 6, true);
  VERIFY_NEXT_ELT(")", 6, 9);
  if(not _remove_last_return)
  {
    QCOMPARE(_stream->getNextChar().content, QStringLiteral("\n"));
  }
  QVERIFY(_stream->eof());
  *_completed = true;
}

void TestLexerTextStream::testString()
{
  knowCore::LexerTextStream stream(string(false));
  bool completed = false;
  testStream(&stream, false, &completed);
  QVERIFY(completed);
  stream.setString(string(false));
  completed = false;
  testStream(&stream, false, &completed);
  QVERIFY(completed);
  stream.setString(string(true));
  completed = false;
  testStream(&stream, true, &completed);
  QVERIFY(completed);
}

void TestLexerTextStream::testDevice()
{
  QByteArray data = string(false).toUtf8();
  QBuffer buffer(&data);

  knowCore::LexerTextStream stream(&buffer);
  bool completed = false;
  testStream(&stream, false, &completed);
  QVERIFY(completed);
  buffer.seek(0);
  stream.setDevice(&buffer);
  completed = false;
  testStream(&stream, false, &completed);
  QVERIFY(completed);
}

void TestLexerTextStream::testTripleQuoteString()
{
  QByteArray data = R"SSS("""hello world""")SSS";
  QBuffer buffer(&data);

  // Test when the buffer ends on """, there was a bug where it would not detect the thiple """

  knowCore::LexerTextStream stream(&buffer);
  stream.setBufferSize(15);
  QCOMPARE(stream.currentBufferSize(), 0);
  knowCore::LexerTextStream* _stream = &stream;
  VERIFY_NEXT_ELT("\"", 1, 1);
  VERIFY_NEXT_ELT("\"", 1, 2);
  VERIFY_NEXT_ELT("\"", 1, 3);
  VERIFY_NEXT_STRING("hello world", 1, 1, true, "\"\"\"");
  QCOMPARE(stream.currentBufferSize(), 16);
}

void TestLexerTextStream::testDigits()
{
  knowCore::LexerTextStream stream("12"_kCs);
  auto [elt, integer] = stream.getDigit({"", 0, 0, false});
  QVERIFY(integer);
  QCOMPARE(elt.content, "12");
  QVERIFY(stream.eof());
}

QTEST_MAIN(TestLexerTextStream)
