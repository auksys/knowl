#include <QtTest/QtTest>

#include <knowCore/Forward.h>

class TestLexerTextStream : public QObject
{
  Q_OBJECT
  void testStream(knowCore::LexerTextStream* _stream, bool _remove_last_return, bool* _completed);
  QString string(bool _remove_last_return);
private slots:
  void testString();
  void testDevice();
  void testTripleQuoteString();
  void testDigits();
};
