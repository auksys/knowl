#include "TestMetaType.h"

#include "../Test.h"
#include "../TypeDefinitions.h"

#include <knowCore/BigNumber.h>
#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Uris/xsd.h>

void TestMetaType::testNumericTypes()
{
  using namespace knowCore;

  QCOMPARE(MetaTypeInformation<BigNumber>::uri(), Uris::xsd::decimal);
  QVERIFY(MetaTypeInformation<BigNumber>::isNumericType());
  QVERIFY(MetaTypeInformation<BigNumber>::aliases().contains(Uris::xsd::integer));
  QVERIFY(MetaTypeInformation<BigNumber>::aliases().contains(Uris::xsd::nonNegativeInteger));
  QVERIFY(MetaTypeInformation<QString>::canConvertTo<qint64>());
  QVERIFY(MetaTypeInformation<qint64>::canConvertFrom<QString>());
}

void TestMetaType::testStringTypes()
{
  using namespace knowCore;
  QCOMPARE(MetaTypeInformation<QString>::uri(), Uris::xsd::string);
  QCOMPARE(MetaTypeInformation<QStringList>::uri(), Uris::askcore_datatype::stringlist);
  QCOMPARE(MetaTypeInformation<QByteArray>::uri(), Uris::askcore_datatype::binarydata);
  QCOMPARE(MetaTypeInformation<QList<QByteArray>>::uri(), Uris::askcore_datatype::binarydatalist);
}

QTEST_MAIN(TestMetaType)
