#include <QtTest/QtTest>

class TestMetaType : public QObject
{
  Q_OBJECT
private slots:
  void testNumericTypes();
  void testStringTypes();
};
