#include "TestQuantityValue.h"

#include <knowCore/QuantityValue.h>
#include <knowCore/Test.h>

using QuantityNumber = knowCore::QuantityNumber;
using BigNumber = knowCore::BigNumber;
using Unit = knowCore::Unit;

void TestQuantityValue::testQuantityNumber()
{
  Unit mu = CRES_QVERIFY(Unit::bySymbol("m"));
  Unit mmu = CRES_QVERIFY(Unit::bySymbol("mm"));
  Unit m2u = CRES_QVERIFY(Unit::bySymbol("m^2"));
  Unit su = CRES_QVERIFY(Unit::bySymbol("s"));
  Unit msu = CRES_QVERIFY(Unit::bySymbol("m/s"));

  QuantityNumber un1(BigNumber(10.0), mu);
  QuantityNumber un2(11.0, mmu);
  QuantityNumber un3(11.0, su);

  QuantityNumber m1pm2 = CRES_QVERIFY(un1 + un2);
  QuantityNumber m1nm2 = CRES_QVERIFY(un1 - un2);
  QuantityNumber m1mm2 = CRES_QVERIFY(un1 * un2);
  QuantityNumber m1dm2 = CRES_QVERIFY(un1 / un2);

  QCOMPARE(m1pm2.value().toDouble(), 10 + 11e-3);
  QCOMPARE(m1pm2.unit(), mu);
  QCOMPARE(m1nm2.value().toDouble(), 10 - 11e-3);
  QCOMPARE(m1nm2.unit(), mu);
  QCOMPARE(m1mm2.value().toDouble(), 10 * 11e-3);
  QCOMPARE(m1mm2.unit(), m2u);
  QCOMPARE(m1dm2.value().toDouble(), 10 / 11e-3);
  QCOMPARE(m1dm2.unit(), Unit::empty());

  CRES_QVERIFY_FAILURE(un1 + un3);
  CRES_QVERIFY_FAILURE(un1 - un3);
  CRES_QVERIFY_FAILURE(un1 * un3);
  QuantityNumber m1dm3 = CRES_QVERIFY(un1 / un3);

  QCOMPARE(m1dm3.value().toDouble(), 0.909);
  QCOMPARE(m1dm3.unit(), msu);
}

void TestQuantityValue::testQuantityNumberSerialisation()
{
  Unit msu = CRES_QVERIFY(Unit::bySymbol("m/s"));
  QuantityNumber un1(BigNumber(10.0), msu);

  QJsonValue val1 = CRES_QVERIFY(un1.toJsonValue());
  QCborValue val2 = CRES_QVERIFY(un1.toCborValue());

  QuantityNumber un2 = CRES_QVERIFY(QuantityNumber::fromJsonValue(val1));
  QuantityNumber un3 = CRES_QVERIFY(QuantityNumber::fromCborValue(val2));

  QCOMPARE(un1, un2);
  QCOMPARE(un1, un3);

  QString lit1 = CRES_QVERIFY(un1.toRdfLiteral());
  QuantityNumber un4 = CRES_QVERIFY(QuantityNumber::fromRdfLiteral(lit1));

  QCOMPARE(lit1, "10 m/s");
  QCOMPARE(un1, un4);

  QuantityNumber un5 = CRES_QVERIFY(QuantityNumber::fromRdfLiteral("10"));

  QCOMPARE(un5.value().toString(), "10");
  QCOMPARE(un5.value().dscale(), 0);
  QCOMPARE(un5.value().toDouble(), 10.0);
  QCOMPARE(un5.unit(), Unit::empty());

  CRES_QVERIFY_FAILURE(QuantityNumber::fromRdfLiteral("10 s/m"));
}

QTEST_MAIN(TestQuantityValue)
