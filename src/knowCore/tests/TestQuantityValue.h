#include <QtTest/QtTest>

class TestQuantityValue : public QObject
{
  Q_OBJECT
private slots:
  void testQuantityNumber();
  void testQuantityNumberSerialisation();
};
