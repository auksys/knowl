#include "TestTimestamp.h"

#include <knowCore/Test.h>
#include <knowCore/Timestamp.h>

void TestDateTime::testFromRdfLiteral()
{
  knowCore::Timestamp dt
    = CRES_QVERIFY(knowCore::Timestamp::fromRdfLiteral("2002-10-10T12:00:00-05:00"));
  QDateTime qdt = CRES_QVERIFY(dt.toQDateTime());
  QCOMPARE(qdt, QDateTime({2002, 10, 10}, {12, 0, 0}, QTimeZone(-5 * 3600)));

  dt = CRES_QVERIFY(knowCore::Timestamp::fromRdfLiteral("2002-10-09T12:00:00-05:00"));
  qdt = CRES_QVERIFY(dt.toQDateTime());
  QCOMPARE(qdt, QDateTime({2002, 10, 9}, {12, 0, 0}, QTimeZone(-5 * 3600)));

  dt = CRES_QVERIFY(knowCore::Timestamp::fromRdfLiteral("2002-10-09T12:00:00Z"));
  qdt = CRES_QVERIFY(dt.toQDateTime());
  QCOMPARE(qdt, QDateTime({2002, 10, 9}, {12, 0, 0}, QTimeZone(0)));

  dt = CRES_QVERIFY(knowCore::Timestamp::fromRdfLiteral("2002-10-09T12:00:00"));
  qdt = CRES_QVERIFY(dt.toQDateTime());
  QCOMPARE(qdt, QDateTime({2002, 10, 9}, {12, 0, 0}));
}

void TestDateTime::testComparison()
{
  knowCore::Timestamp dt1
    = CRES_QVERIFY(knowCore::Timestamp::fromRdfLiteral("2002-10-10T12:00:00-05:00"));
  knowCore::Timestamp dt2
    = CRES_QVERIFY(knowCore::Timestamp::fromRdfLiteral("2002-10-10T12:00:01-05:00"));
  knowCore::Timestamp dt3
    = CRES_QVERIFY(knowCore::Timestamp::fromRdfLiteral("2002-10-09T12:00:00-05:00"));
  knowCore::Timestamp dt4
    = CRES_QVERIFY(knowCore::Timestamp::fromRdfLiteral("2002-10-10T12:00:00"));

  QVERIFY(dt1 < dt2);
  QVERIFY(dt3 < dt1);
  QVERIFY(dt4 < dt1);

  QVERIFY(dt2 > dt1);
  QVERIFY(dt1 > dt3);
  QVERIFY(dt1 > dt4);
}

QTEST_MAIN(TestDateTime)
