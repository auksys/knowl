#include <QtTest/QtTest>

class TestDateTime : public QObject
{
  Q_OBJECT
private slots:
  void testFromRdfLiteral();
  void testComparison();
};
