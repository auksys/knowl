#include "TestUnit.h"

#include <knowCore/Test.h>
#include <knowCore/Unit.h>

#include <knowCore/Uris/askcore_unit.h>
#include <knowCore/Uris/unit.h>

using askcore_unit = knowCore::Uris::askcore_unit;
using unit = knowCore::Uris::unit;

using Unit = knowCore::Unit;

void TestUnit::testBasicUnits()
{
  Unit mu = CRES_QVERIFY(Unit::bySymbol("m"));
  Unit mmu = CRES_QVERIFY(Unit::bySymbol("mm"));
  CRES_QVERIFY(Unit::byUri(mu.uri()));
  CRES_QVERIFY(Unit::byUri(mmu.uri()));

  QVERIFY(mu.isValid());
  QVERIFY(mmu.isValid());

  QCOMPARE(mu.symbol(), "m");
  QCOMPARE(mu.name(), "metre");
  QCOMPARE(mu.uri(), unit::M);
  QCOMPARE(mu.scale(), 1.0);
  QCOMPARE(mu.base(), mu);
  QCOMPARE(mmu.symbol(), "mm");
  QCOMPARE(mmu.name(), "millimetre");
  QCOMPARE(mmu.uri(), unit::MilliM);
  QCOMPARE(mmu.base(), mu);
  QCOMPARE(mmu.scale(), 1e-3);
  QCOMPARE(mu.derivedUnits().size(), 9);
  QCOMPARE(mmu.derivedUnits().size(), 0);

  Unit su = CRES_QVERIFY(Unit::bySymbol("s"));
  Unit msu = CRES_QVERIFY(Unit::bySymbol("ms"));
  QVERIFY(su.isValid());

  QVERIFY(su.isValid());
  QVERIFY(msu.isValid());

  QCOMPARE(su.symbol(), "s");
  QCOMPARE(su.name(), "second");
  QCOMPARE(su.uri(), unit::SEC);
  QCOMPARE(su.scale(), 1.0);
  QCOMPARE(msu.symbol(), "ms");
  QCOMPARE(msu.name(), "millisecond");
  QCOMPARE(msu.uri(), unit::MilliSEC);
  QCOMPARE(msu.base(), su);
  QCOMPARE(msu.scale(), 1e-3);
}

void TestUnit::testCompositeUnits()
{
  Unit mu = CRES_QVERIFY(Unit::bySymbol("m"));
  Unit mmu = CRES_QVERIFY(Unit::bySymbol("mm"));
  Unit su = CRES_QVERIFY(Unit::bySymbol("s"));
  Unit pu = CRES_QVERIFY(Unit::bySymbol("point"));

  Unit m2u = CRES_QVERIFY(mu * mu);
  Unit mm2u = CRES_QVERIFY(mmu * mmu);
  Unit msu = CRES_QVERIFY(mu / su);
  Unit mmsu = CRES_QVERIFY(mmu / su);
  Unit pmm2u = CRES_QVERIFY(pu / mm2u);
  Unit pm2u = CRES_QVERIFY(pu / m2u);
  Unit e = CRES_QVERIFY(pu / pu);
  CRES_QVERIFY(Unit::byUri(pmm2u.uri()));
  CRES_QVERIFY(Unit::byUri(e.uri()));

  QCOMPARE(m2u.symbol(), "m^2");
  QCOMPARE(m2u.name(), "square metre");
  QCOMPARE(m2u.uri(), unit::M2);
  QCOMPARE(m2u.scale(), 1.0);

  QCOMPARE(mm2u.symbol(), "mm^2");
  QCOMPARE(mm2u.name(), "square millimetre");
  QCOMPARE(mm2u.uri(), unit::MilliM2);
  QCOMPARE(mm2u.scale(), 1.0e-6);

  QCOMPARE(msu.symbol(), "m/s");
  QCOMPARE(msu.name(), "metre per second");
  QCOMPARE(msu.uri(), unit::M_PER_SEC);
  QCOMPARE(msu.scale(), 1.0);
  QCOMPARE(msu.derivedUnits().size(), 39);

  QCOMPARE(mmsu.symbol(), "mm/s");
  QCOMPARE(mmsu.name(), "millimetre per second");
  QCOMPARE(mmsu.uri(), unit::MilliM_PER_SEC);
  QCOMPARE(mmsu.base(), msu);
  QCOMPARE(mmsu.scale(), 1.0e-3);

  QCOMPARE(pm2u.symbol(), "point/m^2");
  QCOMPARE(pm2u.name(), "point per square metre");
  QCOMPARE(pm2u.uri(), askcore_unit::point_per_m2);
  QCOMPARE(pm2u.scale(), 1.0);
  QCOMPARE(pm2u.derivedUnits().size(), 9);

  QCOMPARE(pmm2u.symbol(), "point/mm^2");
  QCOMPARE(pmm2u.name(), "point per square millimetre");
  QCOMPARE(pmm2u.uri(), askcore_unit::point_per_millim2);
  QCOMPARE(pmm2u.base(), pm2u);
  QCOMPARE(pmm2u.scale(), 1.0e6);

  QCOMPARE(e, Unit::empty());
}

QTEST_MAIN(TestUnit)
