#include <QtTest/QtTest>

class TestUnit : public QObject
{
  Q_OBJECT
private slots:
  void testBasicUnits();
  void testCompositeUnits();
};
