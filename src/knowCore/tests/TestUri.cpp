#include "TestUri.h"

#include <knowCore/TypeDefinitions.h>
#include <knowCore/Uri.h>
#include <knowCore/UriList.h>
#include <knowCore/ValueList.h>

#include <knowCore/Test.h>

using kUri = knowCore::Uri;

void TestUri::testResolution()
{
  kUri b1("http://askco.re/test/base.ttl");
  kUri b2("http://askco.re/test/");
  kUri b3("http://askco.re/test");
  kUri b4("http://askco.re/test#");
  kUri b5("http://askco.re/test?");

  QCOMPARE(b1.resolved("resolved.ttl"), "http://askco.re/test/resolved.ttl"_kCu);
  QCOMPARE(b2.resolved("resolved.ttl"), "http://askco.re/test/resolved.ttl"_kCu);
  QCOMPARE(b3.resolved("resolved.ttl"), "http://askco.re/resolved.ttl"_kCu);
  QCOMPARE(b4.resolved("resolved.ttl"), "http://askco.re/test#resolved.ttl"_kCu);
  QCOMPARE(b5.resolved("resolved.ttl"), "http://askco.re/resolved.ttl"_kCu);

  QCOMPARE(b1.resolved(""), b1);
  QCOMPARE(b2.resolved(""), b2);
  QCOMPARE(b3.resolved(""), b3);
  QCOMPARE(b4.resolved(""), b4);
  QCOMPARE(b5.resolved(""), b5);
}

void TestUri::testUriList()
{
  kUri b1("http://askco.re/test/base.ttl");
  kUri b2("http://askco.re/test/");

  knowCore::ValueList values({knowCore::Value::fromValue(b1), knowCore::Value::fromValue(b2)});

  knowCore::Value val = knowCore::Value::fromValue(values);

  knowCore::UriList uris = val.value<knowCore::UriList>().expect_success();

  QCOMPARE(uris.size(), 2);

  QCOMPARE(uris[0], b1);
  QCOMPARE(uris[1], b2);

  knowCore::ValueList values_2({knowCore::Value::fromValue(4), knowCore::Value::fromValue(b2)});

  knowCore::Value val2 = knowCore::Value::fromValue(values_2);

  QCOMPARE(val2.value<knowCore::UriList>().is_successful(), false);

  QCOMPARE(values, knowCore::Value::fromValue(uris).toList().expect_success());
}

QTEST_MAIN(TestUri)
