#include <QtTest/QtTest>

class TestUri : public QObject
{
  Q_OBJECT
private slots:
  void testResolution();
  void testUriList();
};
