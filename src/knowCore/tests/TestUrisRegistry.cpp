#include "TestUrisRegistry.h"

#include <knowCore/Curie.h>
#include <knowCore/Uri.h>
#include <knowCore/UriManager.h>
#include <knowCore/UrisRegistry.h>

void TestUrisRegistry::testRegistration()
{
  QCOMPARE((QString)knowCore::UrisRegistry::manager()->uri("rdfs"),
           QString("http://www.w3.org/2000/01/rdf-schema#"));
}

void TestUrisRegistry::testCurification()
{
  knowCore::Curie c;
  QVERIFY(knowCore::UrisRegistry::manager()->curify(
    knowCore::Uri("http://www.w3.org/2000/01/rdf-schema#label"), &c));
  QCOMPARE(c.prefix(), QString("rdfs"));
  QCOMPARE(c.suffix(), QString("label"));
}

QTEST_MAIN(TestUrisRegistry)
