#include <QtTest/QtTest>

class TestUrisRegistry : public QObject
{
  Q_OBJECT
private slots:
  void testRegistration();
  void testCurification();
};
