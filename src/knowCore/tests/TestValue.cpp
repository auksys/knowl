#include "TestValue.h"

#include "../Test.h"
#include "../TypeDefinitions.h"
#include "../Value.h"

#include <knowCore/BigNumber.h>
#include <knowCore/Timestamp.h>
#include <knowCore/Uris/xsd.h>

void TestValue::testFromValue()
{
  using namespace knowCore;

  Value val = Value::fromValue(u8"test"_kCs);

  QVERIFY(val.canConvert<QString>());
  CRES_QCOMPARE(val.value<QString>(), QStringLiteral("test"));

  val = Value::fromValue(1432716988641976118ull);
  CRES_QCOMPARE(val.value<unsigned long long>(), 1432716988641976118ull);

  val = CRES_QVERIFY(Value::fromValue(Uris::xsd::integer, 1432716988641976118ull));

  BigNumber val_bn = CRES_QVERIFY(val.value<BigNumber>());
  CRES_QCOMPARE(val_bn.toUInt64(), 1432716988641976118);
  CRES_QCOMPARE(val.value<unsigned long long>(), 1432716988641976118ull);

  CRES_QVERIFY_FAILURE(Value::fromValue(Uris::xsd::integer32, 1432716988641976118ull));
  val = CRES_QVERIFY(Value::fromValue(Uris::xsd::integer32, 14327169ull));
  CRES_QCOMPARE(val.value<qint32>(), 14327169);
}

void TestValue::testFromVariant()
{
  using namespace knowCore;

  CRES_QVERIFY_FAILURE(Value::fromVariant("url"_kCu, QVariant("hello")));
  CRES_QVERIFY_FAILURE(Value::fromVariant("url"_kCu, QVariant(1432716988641976118ull)));

  Value val
    = CRES_QVERIFY(Value::fromVariant(Uris::xsd::integer, QVariant(1432716988641976118ull)));

  BigNumber val_bn = CRES_QVERIFY(val.value<BigNumber>());
  quint64 val_uint64 = CRES_QVERIFY(val_bn.toUInt64());
  QCOMPARE(val_uint64, 1432716988641976118);
  val_uint64 = CRES_QVERIFY(val.value<unsigned long long>());
  QCOMPARE(val_uint64, 1432716988641976118ull);

  val = CRES_QVERIFY(Value::fromVariant(u8"test"_kCs));
  QString val_string = CRES_QVERIFY(val.value<QString>());
  QCOMPARE(val_string, u8"test"_kCs);
  val = CRES_QVERIFY(Value::fromVariant("test"));
  val_string = CRES_QVERIFY(val.value<QString>());
  QCOMPARE(val_string, u8"test"_kCs);
}

void TestValue::testToVariant()
{
  using namespace knowCore;
  Value val = Value::fromValue(knowCore::BigNumber(10ull));
  QCOMPARE(val.toVariant().userType(), QMetaType::ULongLong);
  val = Value::fromValue(10);
  QCOMPARE(val.toVariant().userType(), QMetaType::Int);
  val = CRES_QVERIFY(Value::fromValue(Uris::xsd::integer, knowCore::BigNumber(10ull)));
  QCOMPARE(val.toVariant().userType(), QMetaType::ULongLong);
}

void TestValue::testFromString()
{
  using namespace knowCore;

  Value val = CRES_QVERIFY(Value::fromVariant(Uris::xsd::integer, QVariant("10")));
  QCOMPARE(val.datatype(), Uris::xsd::integer);
  knowCore::BigNumber bn = CRES_QVERIFY(val.value<knowCore::BigNumber>());
  QCOMPARE(bn.toString(), u8"10"_kCs);
  qint64 val_int64 = CRES_QVERIFY(bn.toInt64());
  QCOMPARE(val_int64, 10ll);
  val_int64 = CRES_QVERIFY(val.value<qint64>());
  QCOMPARE(val_int64, 10ll);
  val = CRES_QVERIFY(Value::fromValue(Uris::xsd::integer, u8"10"_kCs));
  val_int64 = CRES_QVERIFY(val.value<qint64>());
  QCOMPARE(val_int64, 10ll);
  val = CRES_QVERIFY(Value::fromValue(Uris::xsd::integer32, u8"10"_kCs));
  qint32 val_int32 = CRES_QVERIFY(val.value<qint32>());
  QCOMPARE(val_int32, 10ll);
}

void TestValue::testRdfSerialisation()
{
  using namespace knowCore;
  Value val = CRES_QVERIFY(Value::fromRdfLiteral(Uris::xsd::string, "test"));

  QCOMPARE(val.value<QString>(), QStringLiteral("test"));
}

void TestValue::testNumberConversion()
{
  using namespace knowCore;
  Value val = Value::fromValue(10ull);
  CRES_QCOMPARE(val.value<knowCore::BigNumber>(), knowCore::BigNumber(10ull));
  val = Value::fromValue(knowCore::BigNumber(10ull));
  CRES_QCOMPARE(val.value<quint64>(), 10ull);
}

void TestValue::testComparison()
{
  using namespace knowCore;
  QCOMPARE(Value(), Value());

  Value val1 = CRES_QVERIFY(Value::fromRdfLiteral(Uris::xsd::string, "test"));
  Value val2 = CRES_QVERIFY(Value::fromRdfLiteral(Uris::xsd::string, "test"));
  QCOMPARE(val1, val2);
  QVERIFY(val1 == val2);

  val1 = Value::fromValue("test"_kCs);
  val2 = Value::fromValue("test"_kCs);
  QCOMPARE(val1, val2);
  QVERIFY(val1 == val2);

  val1 = Value::fromValue("test"_kCs);
  val2 = Value::fromValue("test"_kCu);
  QCOMPARE(val1, val2);
  QVERIFY(val1 == val2);

  Value val3 = Value::fromValue(10ll);
  Value val4 = Value::fromValue(10ull);
  QCOMPARE(val3, val4);
  Value val4a = Value::fromValue(knowCore::BigNumber(10ull));
  QCOMPARE(val4, val4a);

  Value val5 = Value::fromValue(11ull);
  Value val5a = Value::fromValue(knowCore::BigNumber(11ull));
  QCOMPARE_NE(val3, val5);
  QCOMPARE_NE(val4, val5);
  QVERIFY(val4 < val5);
  QVERIFY(CRES_QVERIFY(val4.compare(val5, ComparisonOperator::Inferior)));
  QVERIFY(CRES_QVERIFY(val4.compare(
    val5, ComparisonOperator((int)ComparisonOperator::Inferior | (int)ComparisonOperator::Equal))));
  CRES_QVERIFY_FAILURE(val1.compare(
    val5, ComparisonOperator((int)ComparisonOperator::Inferior | (int)ComparisonOperator::Equal)));

  QVERIFY(CRES_QVERIFY(val4a.compare(val4a, ComparisonOperator::Equal)));
  QVERIFY(CRES_QVERIFY(val4a.compare(val5a, ComparisonOperator((int)ComparisonOperator::Inferior
                                                               | (int)ComparisonOperator::Equal))));
  QVERIFY(CRES_QVERIFY(val4a.compare(
    val5, ComparisonOperator((int)ComparisonOperator::Inferior | (int)ComparisonOperator::Equal))));
  QVERIFY(CRES_QVERIFY(val4.compare(val5a, ComparisonOperator((int)ComparisonOperator::Inferior
                                                              | (int)ComparisonOperator::Equal))));

  Value val4b = CRES_QVERIFY(Value::fromValue(Uris::xsd::integer, knowCore::BigNumber(10ull)));
  Value val5b = CRES_QVERIFY(Value::fromValue(Uris::xsd::integer, 11ull));
  QVERIFY(CRES_QVERIFY(val4b.compare(val5b, ComparisonOperator((int)ComparisonOperator::Inferior
                                                               | (int)ComparisonOperator::Equal))));
  CRES_QVERIFY_FAILURE(val1.compare(
    val5b, ComparisonOperator((int)ComparisonOperator::Inferior | (int)ComparisonOperator::Equal)));

  Value val6 = CRES_QVERIFY(Value::fromRdfLiteral(Uris::xsd::integer, "1"));
  Value val7 = CRES_QVERIFY(Value::fromRdfLiteral(Uris::xsd::integer, "1"));
  QCOMPARE(val6, val6);
  QVERIFY(val7 == val7);
}

void TestValue::testCborSerialisation()
{
  using namespace knowCore;
  Value val1 = CRES_QVERIFY(Value::fromValue(Uris::xsd::integer, 10));
  QCborValue val = CRES_QVERIFY(val1.toCborValue());
  Value val2 = CRES_QVERIFY(Value::fromCborValue(Uris::xsd::integer, val));
  CRES_QCOMPARE(val2.value<qint64>(), 10);

  val1 = CRES_QVERIFY(Value::fromValue(Uris::xsd::integer, 10ull));
  val = CRES_QVERIFY(val1.toCborValue());
  val2 = CRES_QVERIFY(Value::fromCborValue(Uris::xsd::integer, val));
  CRES_QCOMPARE(val2.value<quint64>(), 10ull);

  val1 = knowCore::Value::fromValue(quint64(10));
  QCOMPARE(val1.datatype(), Uris::xsd::unsignedInteger64);
  val = CRES_QVERIFY(val1.toCborValue());
  val2 = CRES_QVERIFY(Value::fromCborValue(Uris::xsd::integer, val));
  CRES_QCOMPARE(val2.value<quint64>(), 10ull);
}

void TestValue::testStrongWeakConversion()
{
  using namespace knowCore;
  Value val1 = Value::fromValue(10ull);
  Timestamp dt = CRES_QVERIFY(val1.value<Timestamp>(TypeCheckingMode::Force));
  QCOMPARE(dt.toEpoch<knowCore::NanoSeconds>().count(), 10ull);
  CRES_QVERIFY_FAILURE(val1.value<Timestamp>());
  CRES_QVERIFY_FAILURE(val1.convert(MetaTypeInformation<Timestamp>::uri()));
  Value val2
    = CRES_QVERIFY(val1.convert(MetaTypeInformation<Timestamp>::uri(), TypeCheckingMode::Force));
  CRES_QCOMPARE(val2.value<Timestamp>(), Timestamp::fromEpoch(NanoSeconds(10)));
}

void TestValue::testArrayListConversion()
{
  using namespace knowCore;
  Int32Array array({1, 2, 3}, {3});
  Value val1 = Value::fromValue(array);
  QList<qint32> l1 = CRES_QVERIFY(val1.value<QList<qint32>>());
  QCOMPARE(array.toList(), QList<qint32>{KNOWCORE_LIST(1, 2, 3)});
  QCOMPARE(l1, QList<qint32>{KNOWCORE_LIST(1, 2, 3)});
  QList<int> l2 = CRES_QVERIFY(val1.value<QList<int>>());
  QCOMPARE(l2, QList<int>{KNOWCORE_LIST(1, 2, 3)});

  StringArray str_array({"a"_kCs, "b"_kCs, "c"_kCs}, {3});
  Value val_str = Value::fromValue(str_array);
  QStringList l1_str = CRES_QVERIFY(val_str.value<QStringList>());
  QCOMPARE(str_array.toList(), QList<QString>{KNOWCORE_LIST("a"_kCs, "b"_kCs, "c"_kCs)});
  QCOMPARE(l1_str, QStringList{KNOWCORE_LIST("a"_kCs, "b"_kCs, "c"_kCs)});
}

template<>
bool operator==(const cres_qresult<knowCore::BigNumber>& _lhs, const knowCore::BigNumber& _rhs)
{
  return _lhs.is_successful() and _lhs.get_value() == _rhs;
}

void TestValue::testArithmetic()
{
  // Test with bignumber
  knowCore::Value v1bn = knowCore::Value::fromValue<knowCore::BigNumber>(2);
  knowCore::Value v2bn = knowCore::Value::fromValue<knowCore::BigNumber>(3);

  knowCore::Value v3bn = CRES_QVERIFY(v1bn.compute(v2bn, knowCore::ArithmeticOperator::Addition));
  QCOMPARE(v3bn.value<knowCore::BigNumber>(), knowCore::BigNumber(5));

  v3bn = CRES_QVERIFY(v1bn.compute(v2bn, knowCore::ArithmeticOperator::Multiplication));
  QCOMPARE(v3bn.value<knowCore::BigNumber>(), knowCore::BigNumber(6));

  // Test with just number
  knowCore::Value v1 = knowCore::Value::fromValue(2);
  knowCore::Value v2 = knowCore::Value::fromValue(3);

  knowCore::Value v3 = CRES_QVERIFY(v1.compute(v2, knowCore::ArithmeticOperator::Addition));
  QCOMPARE(v3.value<int>(), 5);
  v3 = CRES_QVERIFY(v1.compute(v2, knowCore::ArithmeticOperator::Multiplication));
  QCOMPARE(v3.value<int>(), 6);

  // Test number to bignumber
  knowCore::Value v4 = CRES_QVERIFY(v1.compute(v2bn, knowCore::ArithmeticOperator::Addition));
  QCOMPARE(v4.value<int>(), 5);
  v4 = CRES_QVERIFY(v1.compute(v2bn, knowCore::ArithmeticOperator::Multiplication));
  QCOMPARE(v4.value<int>(), 6);

  // Test bignumber to number
  knowCore::Value v5 = CRES_QVERIFY(v1bn.compute(v2, knowCore::ArithmeticOperator::Addition));
  QCOMPARE(v5.value<int>(), 5);
  v5 = CRES_QVERIFY(v1bn.compute(v2, knowCore::ArithmeticOperator::Multiplication));
  QCOMPARE(v5.value<int>(), 6);

  // Test int to float
  knowCore::Value v2f = knowCore::Value::fromValue(0.1);
  knowCore::Value v6 = CRES_QVERIFY(v1.compute(v2f, knowCore::ArithmeticOperator::Addition));
  QCOMPARE(v6.value<double>(), 2.1);
  v6 = CRES_QVERIFY(v1.compute(v2f, knowCore::ArithmeticOperator::Multiplication));
  QCOMPARE(v6.value<double>(), 0.2);
}

QTEST_MAIN(TestValue)
