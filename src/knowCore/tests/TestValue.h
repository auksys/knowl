#include <QtTest/QtTest>

class TestValue : public QObject
{
  Q_OBJECT
private slots:
  void testFromValue();
  void testFromVariant();
  void testToVariant();
  void testFromString();
  void testNumberConversion();
  void testComparison();
  void testRdfSerialisation();
  void testCborSerialisation();
  void testStrongWeakConversion();
  void testArrayListConversion();
  void testArithmetic();
};
