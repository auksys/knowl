#include "TestValueHash.h"

#include <QJsonObject>

#include "../Global.h"
#include "../Test.h"
#include "../TypeDefinitions.h"
#include "../ValueHash.h"

using ValueHash = knowCore::ValueHash;

void TestValueHash::testInitialiser()
{
  ValueHash vh;
  vh.insert("a", 1);
  QCOMPARE(vh.size(), 1);
  CRES_QCOMPARE(vh.value<int>("a"), 1);
  vh.insert("a", 1).insert("b", u8"c"_kCs);
  QCOMPARE(vh.size(), 2);
  CRES_QCOMPARE(vh.value<int>("a"), 1);
  CRES_QCOMPARE(vh.value<QString>("b"), u8"c"_kCs);

  ValueHash vh2("oh"_kCs, 2, "ohoh"_kCs, u8"hmm"_kCs);
  CRES_QCOMPARE(vh2.value<int>("oh"), 2);
  CRES_QCOMPARE(vh2.value<QString>("ohoh"), u8"hmm"_kCs);
}

void TestValueHash::testVariantBuilder()
{
  QVariantHash v;
  v["hello"] = 10;
  v["ah"] = v;

  ValueHash vh = CRES_QVERIFY(ValueHash::fromVariantHash(v));

  CRES_QCOMPARE(vh.value("hello").value<int>(), 10);
  ValueHash vh_ah = CRES_QVERIFY(vh.value("ah").value<ValueHash>());
  CRES_QCOMPARE(vh_ah.value("hello").value<int>(), 10);
}

void TestValueHash::testJsonBuilder()
{
  QVariantHash v;
  v["hello"] = 10;
  v["ah"] = v;
  QJsonObject obj = QJsonObject::fromVariantHash(v);
  ValueHash vh;
  CRES_QVERIFY(vh.insert(obj));
  QString vh_s = clog_qt::to_qstring(vh);
  qDebug() << vh_s;
  QVERIFY(
    vh_s
      == "{hello: http://www.w3.org/2001/XMLSchema#long(10), ah: "
         "http://askco.re/datatype#valuehash({hello: http://www.w3.org/2001/XMLSchema#long(10)})}"
    or vh_s
         == "{ah: http://askco.re/datatype#valuehash({hello: "
            "http://www.w3.org/2001/XMLSchema#long(10)}), hello: "
            "http://www.w3.org/2001/XMLSchema#long(10)}");

  CRES_QCOMPARE(vh.value("hello").value<int>(), 10);
  ValueHash vh_ah = CRES_QVERIFY(vh.value("ah").value<ValueHash>());
  CRES_QCOMPARE(vh_ah.value("hello").value<int>(), 10);
}

void TestValueHash::testParseFromVariant()
{
  QVariantMap map;
  map["print"] = QVariantMap({{"type", "literal"},
                              {"datatype", "http://www.w3.org/2001/XMLSchema#string"},
                              {"value", "Hello"}});
  knowCore::ValueHash vh = knowCore::ValueHash::parseVariantMap(map).expect_success();
  QCOMPARE(vh.keys(), QStringList{"print"});
  knowCore::Value val = vh.value("print");
  QCOMPARE(val.datatype(), "http://www.w3.org/2001/XMLSchema#string"_kCu);
  CRES_QCOMPARE(val.value<QString>(), "Hello");
}

QTEST_MAIN(TestValueHash)
