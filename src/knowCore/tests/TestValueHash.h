#include <QtTest/QtTest>

class TestValueHash : public QObject
{
  Q_OBJECT
private slots:
  void testInitialiser();
  void testVariantBuilder();
  void testJsonBuilder();
  void testParseFromVariant();
};
