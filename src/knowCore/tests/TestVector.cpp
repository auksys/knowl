#include "TestVector.h"

#include "../TypeDefinitions.h"

void TestVector::testComparison()
{
  using namespace knowCore;

  Vector3d v1(0.1, -0.2, 0.3);
  Vector3d v2(0.1, -0.2, 0.3);
  Vector3d v3(-0.1, -0.2, 0.3);

  QVERIFY(v1 == v1);
  QVERIFY(v1 == v2);
  QVERIFY(v1 != v3);
  QVERIFY(v2 != v3);
}

QTEST_MAIN(TestVector)
