#include <QImage>

#include <cstring>

#include "Image.h"

namespace knowCore
{
  /**
   * Convert @p _img to a \ref QImage. This function is available as header-only,
   * because QImage is part of QtGUI, and knowCore only depends on QtCore.
   */
  inline QImage toQImage(const knowCore::Image& _img)
  {
    knowCore::Image imageu8 = _img.convert(knowCore::Image::Type::UnsignedInteger8);
    QImage qi;
    switch(imageu8.channels())
    {
    case 1:
      qi = QImage(imageu8.width(), imageu8.height(), QImage::Format_Grayscale8);
      break;
    case 3:
      qi = QImage(imageu8.width(), imageu8.height(), QImage::Format_RGB888);
      break;
    case 4:
      qi = QImage(imageu8.width(), imageu8.height(), QImage::Format_ARGB32);
      break;
    default:
      qi = QImage();
      qWarning() << "Unsupported number of channels: " << imageu8.channels();
      break;
    }
    if(not qi.isNull())
    {
      std::memcpy(qi.bits(), imageu8.dataPtr(), imageu8.size());
    }
    return qi;
  }
} // namespace knowCore
