#include <knowCore/Reference.h>

#include "Interfaces/Connection.h"

#include "Query.h"

namespace knowDBC
{
  namespace details
  {
    KNOWCORE_DEFINE_REFERENCE(Connection, knowDBC::Interfaces::Connection,
                              KNOWCORE_FORWARD_CONST_FUNCTIONS(isConnected)
                                KNOWCORE_FORWARD_CONST_FUNCTION(supportQuery, const knowCore::Uri&)
                                  KNOWCORE_FORWARD_CONST_FUNCTION(createQuery, const knowCore::Uri&,
                                                                  const knowCore::ValueHash&));
  }
  class Connection : public details::Connection
  {
  public:
    using details::Connection::Connection;
  };
} // namespace knowDBC

Q_DECLARE_METATYPE(knowDBC::Connection);
