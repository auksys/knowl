#include <knowRDF/Forward.h>

namespace knowDBC
{
  namespace Interfaces
  {
    class Connection;
    class QueryExecutor;
    class Result;
  } // namespace Interfaces
  class Connection;
  class Query;
  class Result;
} // namespace knowDBC
