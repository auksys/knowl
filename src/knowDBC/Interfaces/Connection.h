#include <knowDBC/Forward.h>

namespace knowDBC::Interfaces
{
  /**
   * @ingroup knowDBC
   *
   * An interface for connection to a knowledge store.
   */
  class Connection
  {
  public:
    virtual ~Connection();
    /**
     * @return true if the connection support the given query.
     */
    virtual bool supportQuery(const knowCore::Uri& _type) const = 0;
    /**
     * Create a query with the given type. May return an error if the type is not supported.
     */
    virtual cres_qresult<knowDBC::Query> createQuery(const knowCore::Uri& _type,
                                                     const knowCore::ValueHash& _environment) const
      = 0;
    /**
     * @return true if is connected.
     */
    virtual bool isConnected() const = 0;
  };
} // namespace knowDBC::Interfaces
