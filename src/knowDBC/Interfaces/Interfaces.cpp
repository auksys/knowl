#include "Connection.h"
#include "QueryExecutor.h"
#include "Result.h"

using namespace knowDBC::Interfaces;

Connection::~Connection() {}

QueryExecutor::~QueryExecutor() {}

Result::~Result() {}
