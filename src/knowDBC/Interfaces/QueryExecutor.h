#pragma once

#include <knowDBC/Forward.h>

namespace knowDBC::Interfaces
{
  class QueryExecutor
  {
  public:
    virtual ~QueryExecutor();
    /**
     * Execute the query and return the result.
     */
    virtual knowDBC::Result execute(const QString& _query, const knowCore::ValueHash& _options,
                                    const knowCore::ValueHash& _bindings)
      = 0;
    /**
     * @return the query language used by this executor.
     */
    virtual knowCore::Uri queryLanguage() const = 0;
  };
} // namespace knowDBC::Interfaces
