#pragma once

#include <knowDBC/Forward.h>

namespace knowDBC::Interfaces
{
  class Result
  {
  public:
    enum class Type
    {
      Invalid,
      Failed,
      VariableBinding,
      Boolean
    };
  public:
    virtual ~Result();
    virtual Type type() const = 0;
    /**
     * @return the number of tuples (i.e. rows)
     */
    virtual int tuples() const = 0;
    /**
     * @return the number of fields (i.e. columns)
     */
    virtual int fields() const = 0;
    /**
     * @return the list of fields
     */
    virtual QStringList fieldNames() const = 0;
    /**
     * @return the value at given \p _tuple (row) and \p _field (column)
     */
    virtual knowCore::Value value(int _tuple, int _field) const = 0;
    /**
     * @return an error message if \ref status() is set to \ref Status::Failed.
     */
    virtual QString error() const = 0;
    /**
     * @return the text of the query used for obtaining the results.
     */
    virtual QString query() const = 0;
  };
} // namespace knowDBC::Interfaces
