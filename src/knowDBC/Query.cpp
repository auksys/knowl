#include "Query.h"

#include <QFile>

#include <knowCore/ValueHash.h>

#include "Interfaces/QueryExecutor.h"
#include "Result.h"

using namespace knowDBC;

struct Query::Private : QSharedData
{
  ~Private() { delete queryEngine; }
  QString query;
  knowCore::ValueHash options, namedBindings;
  Interfaces::QueryExecutor* queryEngine;
};

QString Query::OptionsKeys::InlineArguments = QStringLiteral("InlineArguments");
QString Query::OptionsKeys::MultiQueries = QStringLiteral("MultiQueries");

Query::Query() : d(nullptr) {}

Query::Query(Interfaces::QueryExecutor* _query) : d(new Private) { d->queryEngine = _query; }

Query::Query(const Query& _query) : d(_query.d) {}

Query& Query::operator=(const Query& _Query)
{
  d = _Query.d;
  return *this;
}

Query::~Query() {}

void Query::bindValue(const QString& _name, const knowCore::Value& _variant)
{
  d->namedBindings.insert(_name, _variant);
}

void Query::bindValues(const knowCore::ValueHash& _values) { d->namedBindings.insert(_values); }

void Query::bindValues(const QMap<QString, knowCore::Value>& _values)
{
  for(QMap<QString, knowCore::Value>::const_iterator it = _values.begin(); it != _values.end();
      ++it)
  {
    d->namedBindings.insert(it.key(), it.value());
  }
}

void Query::clearBindings() { d->namedBindings.clear(); }

QString Query::query() const { return d->query; }

void Query::setQuery(const QString& _text, bool _keepBindings)
{
  d->query = _text;
  if(not _keepBindings)
  {
    clearBindings();
  }
}

cres_qresult<void> Query::loadFromFile(const knowCore::Uri& _source)
{
  return loadFromFile(_source.toLocalFile());
}

cres_qresult<void> Query::loadFromFile(const QString& _filename)
{
  QFile file(_filename);
  if(file.open(QIODevice::ReadOnly))
  {
    setQuery(file.readAll());
    return cres_success();
  }
  else
  {
    return cres_failure("Could not open file: {}", _filename);
  }
}

knowDBC::Result Query::execute()
{
  return d->queryEngine->execute(d->query, d->options, d->namedBindings);
}

void Query::clearOptions() { d->options.clear(); }

void Query::setOption(const QString& _key, const knowCore::Value& _value)
{
  d->options.insert(_key, _value);
}

void Query::setOptions(const knowCore::ValueHash& _values) { d->options.insert(_values); }

void Query::unsetOption(const QString& _key) { d->options.remove(_key); }
