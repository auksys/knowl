#pragma once

#include <knowDBC/Forward.h>

#include <QExplicitlySharedDataPointer>

#include <knowCore/Value.h>

namespace knowDBC
{
  class Query
  {
  public:
    struct OptionsKeys
    {
      static QString InlineArguments; //< Set to true to inline the argument of the Query
      static QString MultiQueries;    //< set this option if executing multiple query (does not work
                                      // with SELECT and might inline the arguments)
    };
  public:
    Query();
    Query(Interfaces::QueryExecutor* _query);
    Query(const Query& _sqlQuery);
    Query& operator=(const Query& _sqlQuery);
    ~Query();
  public:
    QString query() const;
    void setQuery(const QString& _text, bool _keepBindings = false);

    /**
     * Attempt to load a query from a file.
     */
    cres_qresult<void> loadFromFile(const knowCore::Uri& _source);
    /**
     * Attempt to load a query from a file.
     */
    cres_qresult<void> loadFromFile(const QString& _filename);

    void clearOptions();
    knowCore::Value option(const QString& _key);
    void unsetOption(const QString& _key);
    void setOption(const QString& _key, const knowCore::Value& _value);
    template<typename _T_>
    void setOption(const QString& _key, const _T_& _value)
    {
      setOption(_key, knowCore::Value::fromValue(_value));
    }
    void setOptions(const knowCore::ValueHash& _values);

    void bindValue(const QString& _name, const knowCore::Value& _variant);
    void bindValue(const QString& _name, const char* _literal)
    {
      bindValue(_name, knowCore::Value::fromValue(QString::fromUtf8(_literal)));
    }
    template<typename _T_>
    void bindValue(const QString& _name, const _T_& _value)
    {
      bindValue(_name, knowCore::Value::fromValue<_T_>(_value));
    }
    void bindValues(const knowCore::ValueHash& _values);
    void bindValues(const QMap<QString, knowCore::Value>& _values);
    template<typename _T_, typename... _TOther_>
    void bindValues(const QString& _name, const _T_& _value, _TOther_... _other)
    {
      bindValue(_name, _value);
      bindValues(_other...);
    }
    void clearBindings();
    knowDBC::Result execute();
  private:
    void bindValues() {}
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };

} // namespace knowDBC
