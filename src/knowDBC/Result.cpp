#include "Result.h"

#include <knowCore/TypeDefinitions.h>
#include <knowCore/ValueList.h>

using namespace knowDBC;

namespace knowDBC
{
  struct DefaultResult : public Interfaces::Result
  {
    DefaultResult(const QString& _error, const QString& _query, Type _type,
                  const QStringList& _fields, const QList<knowCore::ValueList>& _data)
        : m_error(_error), m_query(_query), m_type(_type), m_fields(_fields), m_data(_data)
    {
    }
    virtual ~DefaultResult() = default;
    Type type() const override { return m_type; }
    int tuples() const override { return m_data.size(); }
    int fields() const override { return m_fields.size(); }
    QStringList fieldNames() const override { return m_fields; }
    knowCore::Value value(int _tuple, int _field) const override { return m_data[_tuple][_field]; }
    QString error() const override { return m_error; }
    QString query() const override { return m_query; }
  public:
    QString m_error, m_query;
    Type m_type;

    QStringList m_fields;
    QList<knowCore::ValueList> m_data;
  };
} // namespace knowDBC

Result::Result() : Result(Result::create(QString(), "Invalid result."_kCs)) {}

Result::~Result() {}

Result Result::create(const QString& _query, const char* _error)
{
  return create(_query, QString(_error));
}

Result Result::create(const QString& _query, const QString& _error)
{
  return new DefaultResult{_error, _query, Type::Failed, QStringList(),
                           QList<knowCore::ValueList>()};
}

Result Result::create(const QString& _query, QStringList _fields,
                      const QList<knowCore::ValueList>& _data)
{
  return new DefaultResult{QString(), _query, Type::VariableBinding, _fields, _data};
}

Result Result::create(const QString& _query, bool _boolean)
{
  return new DefaultResult{QString(),
                           _query,
                           Type::Boolean,
                           {"boolean"},
                           {knowCore::ValueList({knowCore::Value::fromValue(_boolean)})}};
}

knowCore::Value Result::value(int _tuple, const QByteArray& _field) const
{
  return value(_tuple, fieldIndex(_field));
}

knowCore::Value Result::value(int _tuple, const char* _field) const
{
  return value(_tuple, QByteArray::fromStdString(_field));
}

bool Result::boolean() const
{
  return type() == Type::Boolean ? value<bool>(0, 0).expect_success() : false;
}

int Result::fieldIndex(const QString& _name) const { return fieldNames().indexOf(_name); }

Result::operator bool() const { return type() == Type::Boolean or type() == Type::VariableBinding; }

cres_qresult<Result> Result::read(QIODevice* _device, const QString& _format,
                                  const knowCore::Uri& _base)
{
  if(_format == knowCore::FileFormat::JSON)
  {
    return readJson(_device, _base);
  }
  else if(_format == knowCore::FileFormat::SRX or _format == knowCore::FileFormat::XML)
  {
    return readSRX(_device, _base);
  }
  else if(_format == knowCore::FileFormat::Turtle)
  {
    return readRDF(_device, _format, _base);
  }
  else
  {
    return cres_failure("unsupported file format: '{}', supported are: '{}'", _format,
                        QStringList({knowCore::FileFormat::JSON, knowCore::FileFormat::SRX,
                                     knowCore::FileFormat::XML, knowCore::FileFormat::Turtle}));
  }
}

cres_qresult<void> Result::write(QIODevice* _device, const QString& _format) const
{
  if(_format == knowCore::FileFormat::JSON or _format == "application/sparql-results+json")
  {
    return writeJson(_device);
  }
  else if(_format == knowCore::FileFormat::CSV)
  {
    return writeCSV(_device);
  }
  else if(_format == knowCore::FileFormat::SRX or _format == knowCore::FileFormat::XML
          or _format == "application/sparql-results+xml")
  {
    return writeSRX(_device);
  }
  else
  {
    return cres_failure("unsupported file format: '{}', supported are: '{}'", _format,
                        QStringList({knowCore::FileFormat::JSON, knowCore::FileFormat::SRX,
                                     knowCore::FileFormat::XML}));
  }
}
