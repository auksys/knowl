#pragma once

#include <knowCore/FileFormat.h>
#include <knowCore/Reference.h>
#include <knowRDF/Literal.h>

#include "Interfaces/Result.h"

namespace knowDBC
{
  namespace details
  {
    KNOWCORE_DEFINE_REFERENCE(ResultRef, Interfaces::Result,
                              KNOWCORE_FORWARD_CONST_FUNCTIONS(type, tuples, fields, fieldNames,
                                                               error, query),
                              KNOWCORE_FORWARD_CONST_FUNCTION(value, int, int))
  }
  /**
   * Hold an \ref Interfaces::Result and provide convenient functionnalities common to all type of
   * query results.
   */
  class Result : public details::ResultRef
  {
  public:
    using Type = Interfaces::Result::Type;
  public:
    static Result create(const QString& _query, const char* _error);
    static Result create(const QString& _query, const QString& _error);
    static Result create(const QString& _query, QStringList _fields,
                         const QList<knowCore::ValueList>& _data);
    static Result create(const QString& _query, bool _boolean);
  public:
    Result(Interfaces::Result* _result) : details::ResultRef(_result) {}
    Result();
    Result(const Result&) = default;
    Result& operator=(const Result&) = default;
    ~Result();
    using details::ResultRef::value;
    knowCore::Value value(int _tuple, const QByteArray& _field) const;
    knowCore::Value value(int _tuple, const char* _field) const;
    template<typename _T_>
    cres_qresult<_T_> value(int _tuple, int _field,
                            knowCore::TypeCheckingMode _conversion
                            = knowCore::TypeCheckingMode::Safe) const;
    template<typename _T_>
    cres_qresult<_T_> value(int _tuple, const QByteArray& _field,
                            knowCore::TypeCheckingMode _conversion
                            = knowCore::TypeCheckingMode::Safe) const;
    template<typename _T_>
    cres_qresult<_T_> value(int _tuple, const char* _field,
                            knowCore::TypeCheckingMode _conversion
                            = knowCore::TypeCheckingMode::Safe) const;
    /**
     * If the result is of type \ref Type::Boolean return the boolean value, otherwise result is
     * undefined.
     */
    bool boolean() const;
    /**
     * @return the index of the field \p _name.
     */
    int fieldIndex(const QString& _name) const;
    /**
     * @return the name of the field at index \ref _index.
     */
    QString fieldName(int _index) const { return fieldNames()[_index]; }
  public:
    operator bool() const;
  public:
    static cres_qresult<Result> read(QIODevice* _device,
                                     const QString& _format = knowCore::FileFormat::JSON,
                                     const knowCore::Uri& _base = knowCore::Uri());
    cres_qresult<void> write(QIODevice* _device,
                             const QString& _format = knowCore::FileFormat::JSON) const;
  private:
    static cres_qresult<Result> readJson(QIODevice* _device, const knowCore::Uri& _base);
    cres_qresult<void> writeJson(QIODevice* _device) const;
    static cres_qresult<Result> readRDF(QIODevice* _device, const QString& _format,
                                        const knowCore::Uri& _base);
    static cres_qresult<Result> readSRX(QIODevice* _device, const knowCore::Uri& _base);
    cres_qresult<void> writeSRX(QIODevice* _device) const;
    cres_qresult<void> writeCSV(QIODevice* _device) const;
  };
  template<typename _T_>
  cres_qresult<_T_> Result::value(int _tuple, int _field,
                                  knowCore::TypeCheckingMode _conversion) const
  {
    return value(_tuple, _field).value<_T_>(_conversion);
  }
  template<typename _T_>
  cres_qresult<_T_> Result::value(int _tuple, const QByteArray& _field,
                                  knowCore::TypeCheckingMode _conversion) const
  {
    return value(_tuple, _field).value<_T_>(_conversion);
  }
  template<typename _T_>
  cres_qresult<_T_> Result::value(int _tuple, const char* _field,
                                  knowCore::TypeCheckingMode _conversion) const
  {
    return value(_tuple, QByteArray::fromStdString(_field)).value<_T_>(_conversion);
  }
} // namespace knowDBC
