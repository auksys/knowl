#include "Result.h"

#include <QIODevice>

#include <knowCore/CSVWriter.h>

using knowDBC::Result;

cres_qresult<void> Result::writeCSV(QIODevice* _device) const
{

  knowCore::CSVWriter stream(_device, fields());
  for(int i = 0; i < fields(); ++i)
  {
    stream << fieldNames()[i];
  }
  for(int k = 0; k < tuples(); ++k)
  {
    for(int i = 0; i < fields(); ++i)
    {
      cres_try(QString serialised, value(k, i).toRdfLiteral());
      stream << serialised;
    }
  }
  _device->close();
  return cres_success();
}
