#include "Result.h"

#include <knowCore/Uri.h>
#include <knowCore/ValueList.h>

#include <knowRDF/BlankNode.h>
#include <knowRDF/Literal.h>

#include <QIODevice>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

using namespace knowDBC;

cres_qresult<Result> Result::readJson(QIODevice* _device, const knowCore::Uri& _base)
{
  if(not _device->open(QIODevice::ReadOnly))
  {
    return cres_failure("Failed to open device for reading.");
  }

  QJsonParseError err;
  QJsonDocument doc = QJsonDocument::fromJson(_device->readAll(), &err);
  if(err.error != QJsonParseError::ParseError::NoError)
  {
    return cres_failure("Parse error in json '{}'", err.errorString());
  }

  QStringList fields;
  QList<knowCore::ValueList> data;
  bool success = true;
  QString error;
  QString query;

  QJsonObject root_object = doc.object();
  QJsonObject kdb_object
    = root_object.value(root_object.contains("__kdb") ? "__kdb" : "__knowL").toObject();
  QJsonObject head_object = root_object.value("head").toObject();
  QJsonObject results_object = root_object.value("results").toObject();

  // read head
  QJsonArray vars_array = head_object.value("vars").toArray();
  for(const QJsonValue& value : vars_array)
  {
    fields.append(value.toString());
  }

  QHash<QString, knowCore::Value> blankNodes;

  // read results
  QJsonArray bindings_array = results_object.value("bindings").toArray();
  for(const QJsonValue& value : bindings_array)
  {
    QJsonObject row_object = value.toObject();
    QList<knowCore::Value> list;
    for(const QString& field : fields)
    {
      QJsonObject cell_object = row_object.value(field).toObject();
      QString type = cell_object.value("type").toString();
      QJsonValue value_object = cell_object.value("value");
      knowCore::Value var;
      if(type == "uri")
      {
        var = knowCore::Value::fromValue<knowCore::Uri>(_base.resolved(value_object.toString()));
      }
      else if(type == "literal")
      {
        QString uri = cell_object.value("datatype").toString();
        QString lang = cell_object.value("lang").toString();

        if(uri.isEmpty())
        {
          cres_try(var, knowRDF::Literal::fromVariant(value_object.toVariant(), lang));
        }
        else if(value_object.isString())
        {
          cres_try(var, knowRDF::Literal::fromRdfLiteral(uri, value_object.toString(), lang));
        }
        else
        {
          cres_try(var, knowRDF::Literal::fromVariant(uri, value_object.toVariant(), lang));
        }
      }
      else if(type == "bnode")
      {
        QString label = value_object.toString();
        if(blankNodes.contains(label))
        {
          var = blankNodes[label];
        }
        else
        {
          var = knowCore::Value::fromValue(knowRDF::BlankNode(label));
          blankNodes[label] = var;
        }
      }
      else
      {
        cres_try(var, knowCore::Value::fromVariant(value_object.toVariant()));
      }
      list.append(var);
    }
    data.append(list);
  }

  // read __kdb

  if(kdb_object.contains("success"))
    success = kdb_object.value("success").toBool();
  if(kdb_object.contains("query"))
    query = kdb_object.value("query").toString();
  if(kdb_object.contains("error"))
    error = kdb_object.value("error").toString();

  if(success)
  {
    return cres_success(Result::create(query, fields, data));
  }
  else
  {
    return cres_success(Result::create(query, error));
  }
}

cres_qresult<void> Result::writeJson(QIODevice* _device) const
{
  QJsonArray vars_array;
  for(const QString& var : fieldNames())
  {
    vars_array.append(var);
  }
  QJsonObject head_object;
  head_object.insert("vars", vars_array);

  QHash<knowRDF::BlankNode, QString> blankNodes;

  QJsonArray bindings_array;
  for(int t = 0; t < tuples(); ++t)
  {
    QJsonObject row_object;
    for(int i = 0; i < fields(); ++i)
    {
      knowCore::Value val = value(t, i);

      QJsonObject value_object;

      if(knowCore::ValueIs<knowCore::Uri> val_uri = val)
      {
        value_object.insert("type", QString("uri"));
        value_object.insert("value", (QString)val_uri.value());
      }
      else if(knowCore::ValueIs<QString> val_uri = val)
      {
        value_object.insert("type", QString("uri"));
        value_object.insert("value", (QString)val_uri);
      }
      else if(knowCore::ValueIs<knowRDF::Literal> lit = val)
      {
        QString serialised;
        value_object.insert("type", QString("literal"));
        cres_try(QString value_r_l, lit->toRdfLiteral());
        value_object.insert("value", value_r_l);
        value_object.insert("datatype", (QString)lit->datatype());
        if(lit->lang().size() > 0)
        {
          value_object.insert("lang", lit->lang());
        }
      }
      else if(knowCore::ValueIs<knowRDF::BlankNode> bn = val)
      {
        QString label;
        if(blankNodes.contains(bn))
        {
          label = blankNodes[bn];
        }
        else
        {
          label = QString::number(blankNodes.size());
          blankNodes[bn] = label;
        }
        value_object.insert("type", QString("bnode"));
        value_object.insert("value", label);
      }
      else
      {
        value_object.insert("type", QString("literal"));
        cres_try(QJsonValue json_val, val.toJsonValue());
        value_object.insert("datatype", (QString)val.datatype());
        value_object.insert("value", json_val);
      }
      row_object.insert(fieldNames()[i], value_object);
    }
    bindings_array.append(row_object);
  }
  QJsonObject results_object;
  results_object.insert("bindings", bindings_array);

  QJsonObject kdb_data;
  kdb_data["success"] = type() == Type::VariableBinding or type() == Type::Boolean;
  kdb_data["query"] = query();
  kdb_data["error"] = error();

  QJsonObject root_object;
  root_object.insert("head", head_object);
  root_object.insert("results", results_object);
  root_object.insert("__knowL", kdb_data);

  QJsonDocument doc;
  doc.setObject(root_object);
  if(not _device->open(QIODevice::WriteOnly))
  {
    return cres_failure("Failed to open device for writting.");
  }
  _device->write(doc.toJson());
  return cres_success();
}
