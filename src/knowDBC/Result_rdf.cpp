#include "Result.h"

#include <knowCore/Uri.h>
#include <knowCore/ValueList.h>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include <clog_qt>
#include <knowCore/Messages.h>

#include <knowRDF/BlankNode.h>
#include <knowRDF/Graph.h>
#include <knowRDF/Literal.h>
#include <knowRDF/Node.h>
#include <knowRDF/TripleStream.h>

#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/rs.h>

using namespace knowDBC;

cres_qresult<Result> Result::readRDF(QIODevice* _device, const QString& _format,
                                     const knowCore::Uri& _base)
{
  QStringList fields;
  QList<knowCore::ValueList> data;
  bool success = true;
  QString error;
  QString query;

  knowRDF::TripleStream testfilestream;
  testfilestream.setBase(_base);
  knowRDF::Graph manifest_graph;
  testfilestream.addListener(&manifest_graph);

  cres_try(cres_ignore, testfilestream.start(_device, nullptr, _format));

  const knowRDF::Node* result_node = nullptr;

  for(const knowRDF::Node* n : manifest_graph.nodes())
  {
    const knowRDF::Node* type = n->getFirstChild(knowCore::Uris::rdf::a);
    if(type and type->uri() == knowCore::Uris::rs::ResultSet)
    {
      result_node = n;
      break;
    }
  }

  if(not result_node)
  {
    return cres_failure("Invalid file, does not countain a dataset!");
  }

  // Fill fields
  QList<const knowRDF::Node*> result_variable_node_children
    = result_node->children(knowCore::Uris::rs::resultVariable);
  for(const knowRDF::Node* result_variable : result_variable_node_children)
  {
    cres_try(QString field_name, result_variable->literal().toRdfLiteral());
    fields.append(field_name);
  }

  QList<const knowRDF::Node*> solution_node_children
    = result_node->children(knowCore::Uris::rs::solution);
  for(const knowRDF::Node* solution_node : solution_node_children)
  {
    QList<knowCore::Value> line;
    std::fill_n(std::back_inserter(line), fields.size(), knowCore::Value());
    for(const knowRDF::Node* binding_node : solution_node->children(knowCore::Uris::rs::binding))
    {
      const knowRDF::Node* value_node = binding_node->getFirstChild(knowCore::Uris::rs::value);
      const knowRDF::Node* variable_node
        = binding_node->getFirstChild(knowCore::Uris::rs::variable);
      if(value_node and variable_node)
      {
        cres_try(QString field_name, variable_node->literal().toRdfLiteral());
        int index = fields.indexOf(field_name);
        clog_assert(index >= 0);
        switch(value_node->type())
        {
        case knowRDF::Node::Type::Literal:
          line[index] = value_node->literal();
          break;
        case knowRDF::Node::Type::Uri:
          line[index] = knowCore::Value::fromValue(value_node->uri());
          break;
        case knowRDF::Node::Type::BlankNode:
          line[index] = knowCore::Value::fromValue(knowRDF::BlankNode());
          break;
        default:
          return cres_failure("Unsupported node type");
        }
      }
      else
      {
        clog_warning("Missing value or variable");
      }
    }
    data.append(line);
  }

  if(success)
  {
    return cres_success(Result::create(query, fields, data));
  }
  else
  {
    return cres_success(Result::create(query, error));
  }
}
