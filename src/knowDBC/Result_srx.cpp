#include "Result.h"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include <knowCore/TypeDefinitions.h>
#include <knowCore/Uri.h>
#include <knowCore/Uris/xsd.h>
#include <knowCore/ValueList.h>
#include <knowRDF/Literal.h>

using namespace knowDBC;

cres_qresult<Result> Result::readSRX(QIODevice* _device, const knowCore::Uri& _base)
{
  if(not _device->open(QIODevice::ReadOnly))
  {
    return cres_failure("Failed to open device for reading.");
  }

  QStringList fields;
  QList<knowCore::ValueList> data;
  bool success = true;
  bool boolean = false;
  QString error;
  QString query;
  Type type = Type::Failed;

  QXmlStreamReader reader(_device);
  if(not reader.readNextStartElement() and reader.name() != QStringLiteral("sparql"))
  {
    return cres_failure("Missing <sparql>.");
  }

  if(not reader.readNextStartElement() and reader.name() != QStringLiteral("head"))
  {
    return cres_failure("Missing <head>.");
  }
  while(reader.readNextStartElement())
  {
    if(reader.name() == QStringLiteral("variable"))
    {
      fields.append(reader.attributes().value("name").toString());
      reader.skipCurrentElement();
    }
    else
    {
      return cres_failure("Unexpected <{}>.", reader.name().toString());
    }
  }

  if(not reader.readNextStartElement())
  {
    return cres_failure("Missing <results> or <boolean>.");
  }

  if(reader.name() == QStringLiteral("results"))
  {
    type = Type::VariableBinding;
    while(reader.readNextStartElement())
    {
      if(reader.name() == QStringLiteral("result"))
      {
        QHash<QString, knowCore::Value> fields2value;
        while(reader.readNextStartElement())
        {
          if(reader.name() == QStringLiteral("binding"))
          {
            QString name = reader.attributes().value("name").toString();
            reader.readNextStartElement();
            if(reader.name() == QStringLiteral("literal"))
            {
              QString uri = reader.attributes().value("datatype").toString();
              QString lang = reader.attributes()
                               .value("http://www.w3.org/XML/1998/namespace", "lang")
                               .toString(); // http://www.w3.org/2005/sparql-results#
              QString value_text = reader.readElementText();
              if(uri.isEmpty())
              {
                uri = knowCore::Uris::xsd::string;
              }
              cres_try(fields2value[name], knowRDF::Literal::fromRdfLiteral(uri, value_text, lang));
            }
            else if(reader.name() == QStringLiteral("uri"))
            {
              fields2value[name]
                = knowCore::Value::fromValue(_base.resolved(reader.readElementText()));
            }
            else
            {
              return cres_failure("Unexpected <{}>.", reader.name().toString());
            }
          }
          else
          {
            return cres_failure("Unexpected <{}>.", reader.name().toString());
          }
          reader.skipCurrentElement();
        }
        QList<knowCore::Value> list;
        for(const QString& field : fields)
        {
          list.append(fields2value[field]);
        }
        data.append(list);
      }
      else
      {
        return cres_failure("Unexpected <{}>.", reader.name().toString());
      }
    }
  }
  else if(reader.name() == QStringLiteral("boolean"))
  {
    QString vaue = reader.readElementText();
    boolean = vaue == "true";
    type = Type::Boolean;
  }
  else
  {
    type = Type::Failed;
  }

  if(reader.readNextStartElement()
     and (reader.name() == QStringLiteral("__kdb") or reader.name() == QStringLiteral("__knowL")))
  {
    while(reader.readNextStartElement())
    {
      if(reader.name() == QStringLiteral("success"))
      {
        success = reader.attributes().value("value") == QStringLiteral("true");
      }
      else if(reader.name() == QStringLiteral("query"))
      {
        query = reader.attributes().value("value").toString();
      }
      if(reader.name() == QStringLiteral("error"))
      {
        error = reader.attributes().value("value").toString();
        if(not error.isEmpty())
        {
          type = Type::Failed;
        }
      }
    }
  }
  if(not success and type != Type::Failed)
  {
    return cres_failure("Invalid result file.");
  }
  switch(type)
  {
  case Type::VariableBinding:
    return cres_success(Result::create(query, fields, data));
  case Type::Boolean:
    return cres_success(Result::create(query, boolean));
  case Type::Failed:
    return cres_success(Result::create(query, error));
  case Type::Invalid:
    return cres_failure("Invalid result");
  }
  return cres_failure("Unknown type.");
}

cres_qresult<void> Result::writeSRX(QIODevice* _device) const
{
  if(not _device->open(QIODevice::WriteOnly))
  {
    return cres_failure("Failed to open device for writting.");
  }

  QXmlStreamWriter writer(_device);
  writer.writeStartDocument("1.0");
  // sparql
  writer.writeStartElement("sparql");
  writer.writeAttribute("xmlns", "http://www.w3.org/2005/sparql-results#");

  // head
  writer.writeStartElement("head");
  for(const QString& var : fieldNames())
  {
    writer.writeStartElement("variable");
    writer.writeAttribute("name", var);
    writer.writeEndElement();
  }
  writer.writeEndElement(); // head

  // results
  switch(type())
  {
  case Type::Invalid:
    break;
  case Type::VariableBinding:
  {
    writer.writeStartElement("results");
    for(int t = 0; t < tuples(); ++t)
    {
      writer.writeStartElement("result");
      for(int i = 0; i < fields(); ++i)
      {
        writer.writeStartElement("binding");
        writer.writeAttribute("name", fieldNames()[i]);
        knowCore::Value val = value(t, i);

        if(knowCore::ValueIs<knowCore::Uri> val_uri = val)
        {
          writer.writeTextElement("uri", (QString)val_uri.value());
        }
        else if(knowCore::ValueIs<QString> val_uri = val)
        {
          writer.writeTextElement("uri", val_uri);
        }
        else if(knowCore::ValueIs<knowRDF::Literal> lit = val)
        {
          cres_try(QString serialised, lit->toRdfLiteral());

          writer.writeStartElement("literal");
          writer.writeAttribute("datatype", (QString)lit->datatype());
          if(lit->lang().size() > 0)
          {
            writer.writeAttribute("xml:lang", lit->lang());
          }
          writer.writeCharacters(serialised);
          writer.writeEndElement();
        }
        else
        {
          cres_try(QString serialised, val.toRdfLiteral());

          writer.writeStartElement("literal");
          writer.writeAttribute("datatype", (QString)val.datatype());
          writer.writeCharacters(serialised);
          writer.writeEndElement();
        }
        writer.writeEndElement(); // binding
      }
      writer.writeEndElement(); // result
    }
    writer.writeEndElement(); // results
    break;
  }
  case Type::Boolean:
  {
    cres_try(bool boolean, value(0, 0).value<bool>());
    writer.writeTextElement("boolean", boolean ? "true" : "false");
    break;
  }
  case Type::Failed:
    break;
  }

  // __knowL
  writer.writeStartElement("__knowL");
#define KNOWDBC_ELEMENT(_name_, _value_)                                                           \
  writer.writeStartElement(_name_);                                                                \
  writer.writeAttribute("value", _value_);                                                         \
  writer.writeEndElement()
  KNOWDBC_ELEMENT("success",
                  (type() != Type::Failed and type() != Type::Invalid) ? "true" : "false");
  KNOWDBC_ELEMENT("query", query());
  KNOWDBC_ELEMENT("error", error());
  writer.writeEndElement(); // __knowL
  // Finish document
  writer.writeEndElement(); // sparql
  writer.writeEndDocument();
  return cres_success();
}
