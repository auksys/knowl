#include "AbstractCommunicationInterfaces.h"

#include <QList>
#include <QString>

using namespace knowDataTransfert;

//////////////////// AbstractCommunicationInterface ////////////////////

struct AbstractCommunicationInterface::Private
{
  QString communicationNamespace;
  QList<std::function<void(const DataControl&)>> receivers;
};

AbstractCommunicationInterface::AbstractCommunicationInterface(
  const QString& _communicationNamespace, Private* _d)
    : d(_d)
{
  d->communicationNamespace = _communicationNamespace;
}

AbstractCommunicationInterface::~AbstractCommunicationInterface() { delete d; }

QString AbstractCommunicationInterface::communicationNamespace() const
{
  return d->communicationNamespace;
}

void AbstractCommunicationInterface::addReceiver(
  const std::function<void(const DataControl&)>& _receiver)
{
  d->receivers.append(_receiver);
}

void AbstractCommunicationInterface::handle(const DataControl& _data)
{
  for(const std::function<void(const DataControl&)>& _f : d->receivers)
  {
    _f(_data);
  }
}

//////////////////// AbstractSendingCommunicationInterface ////////////////////

AbstractSendingCommunicationInterface::AbstractSendingCommunicationInterface(
  const QString& _communicationNamespace)
    : AbstractCommunicationInterface(_communicationNamespace, new Private)
{
}

AbstractSendingCommunicationInterface::~AbstractSendingCommunicationInterface() {}

//////////////////// AbstractReceivingCommunicationInterface ////////////////////

struct AbstractReceivingCommunicationInterface::Private : AbstractCommunicationInterface::Private
{
  QList<std::function<void(const Data&)>> receivers;
};

KNOWCORE_D_FUNC_DEF(AbstractReceivingCommunicationInterface);

AbstractReceivingCommunicationInterface::AbstractReceivingCommunicationInterface(
  const QString& _communicationNamespace)
    : AbstractCommunicationInterface(_communicationNamespace, new Private)
{
}

AbstractReceivingCommunicationInterface::~AbstractReceivingCommunicationInterface() {}

void AbstractReceivingCommunicationInterface::addReceiver(
  const std::function<void(const Data&)>& _receiver)
{
  D()->receivers.append(_receiver);
}

void AbstractReceivingCommunicationInterface::handle(const Data& _data)
{
  for(const std::function<void(const Data&)>& _f : D()->receivers)
  {
    _f(_data);
  }
}

//////////////////// AbstractCommunicationInterfaceFactory ////////////////////

struct AbstractCommunicationInterfaceFactory::Private
{
};

AbstractCommunicationInterfaceFactory::~AbstractCommunicationInterfaceFactory() { delete d; }
