#include <functional>

#include <knowCore/Global.h>

#include "Forward.h"

namespace knowDataTransfert
{
  class AbstractCommunicationInterface
  {
  protected:
    struct Private;
    AbstractCommunicationInterface(const QString& _communicationNamespace, Private* _d);
  public:
    virtual ~AbstractCommunicationInterface();
    QString communicationNamespace() const;

    virtual void initialise() = 0;
    virtual void send(const DataControl& _dataControl) = 0;
    void addReceiver(const std::function<void(const DataControl& /*_data*/)>& _receiver);
  protected:
    void handle(const DataControl& _data);
    Private* const d;
  };

  class AbstractSendingCommunicationInterface : public AbstractCommunicationInterface
  {
  public:
    AbstractSendingCommunicationInterface(const QString& _communicationNamespace);
    virtual ~AbstractSendingCommunicationInterface();
    using AbstractCommunicationInterface::send;
    virtual void send(const Data& _data) = 0;
  };
  class AbstractReceivingCommunicationInterface : public AbstractCommunicationInterface
  {
  public:
    AbstractReceivingCommunicationInterface(const QString& _communicationNamespace);
    virtual ~AbstractReceivingCommunicationInterface();
    using AbstractCommunicationInterface::addReceiver;
    void addReceiver(const std::function<void(const Data& /*_data*/)>& _receiver);
  protected:
    using AbstractCommunicationInterface::handle;
    void handle(const Data& _data);
    KNOWCORE_D_DECL();
  };
  class AbstractCommunicationInterfaceFactory
  {
  public:
    virtual ~AbstractCommunicationInterfaceFactory();
    virtual AbstractSendingCommunicationInterface*
      createSendingInterface(const QString& _communicationNamespace)
      = 0;
    virtual AbstractReceivingCommunicationInterface*
      createReceivingInterface(const QString& _communicationNamespace)
      = 0;
  private:
    struct Private;
    Private* const d = nullptr;
  };
} // namespace knowDataTransfert
