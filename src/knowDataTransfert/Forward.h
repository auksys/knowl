#include <knowCore/Forward.h>

namespace knowDataTransfert
{
  namespace Interfaces
  {
    class SendIterator;
    class ReceiveIterator;
  } // namespace Interfaces
  class AbstractCommunicationInterfaceFactory;
  class AbstractReceivingCommunicationInterface;
  class AbstractSendingCommunicationInterface;
  struct Data;
  struct DataControl;
  class SendJob;
  class ReceiveJob;
} // namespace knowDataTransfert
