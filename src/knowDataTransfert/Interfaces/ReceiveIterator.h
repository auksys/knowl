#include <knowCore/Forward.h>

namespace knowDataTransfert::Interfaces
{
  class ReceiveIterator
  {
  public:
    virtual ~ReceiveIterator() {}

    virtual cres_qresult<void> next(const QCborValue& _data) = 0;
    virtual cres_qresult<void> finalise() = 0;
  };
} // namespace knowDataTransfert::Interfaces
