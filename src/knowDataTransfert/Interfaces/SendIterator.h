#include <knowCore/Forward.h>
#include <knowCore/Interfaces/Iterable.h>

namespace knowDataTransfert::Interfaces
{
  class SendIterator : public knowCore::Interfaces::Iterable<cres_qresult<QCborValue>>
  {
  public:
    virtual ~SendIterator() {}
    virtual bool hasNext() const = 0;
  };
} // namespace knowDataTransfert::Interfaces
