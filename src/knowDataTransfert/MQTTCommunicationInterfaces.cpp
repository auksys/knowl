#include "MQTTCommunicationInterfaces.h"

#include <QVariantMap>

#include <Cyqlops/MQTT/Client.h>
#include <Cyqlops/MQTT/Message.h>

#include <clog_qt>

#include "Messages.h"

namespace knowDataTransfert::MQTTCommunicationInterfaces
{
  DataControl toDataControl(const QVariant& _variant)
  {
    QVariantMap m = _variant.toMap();
    DataControl dc;
    dc.name = m["name"].toString();
    dc.errorMessage = m["errorMessage"].toString();
    dc.command = DataControl::Command(m["command"].toInt());
    for(const QVariant& var : m["seqs"].toList())
    {
      dc.seqs.append(var.toInt());
    }
    return dc;
  }
  Data toData(const QVariant& _variant)
  {
    QVariantMap m = _variant.toMap();
    Data da;
    da.seq = m["seq"].toInt();
    da.data = m["data"].toByteArray();
    return da;
  }
  QVariant toVariant(const DataControl& _dc)
  {
    QVariantMap m;
    m["name"] = _dc.name;
    m["errorMessage"] = _dc.errorMessage;
    m["command"] = int(_dc.command);
    QVariantList list;
    for(qint32 s : _dc.seqs)
    {
      list.append(s);
    }
    m["seqs"] = list;
    return m;
  }
  QVariant toVariant(const Data& _dc)
  {
    QVariantMap m;
    m["seq"] = _dc.seq;
    m["data"] = _dc.data;
    return m;
  }
  class Sending : public AbstractSendingCommunicationInterface
  {
  public:
    Sending(const QString& _serverAddress, const QString& _communicationNamespace);
    virtual ~Sending();
    void initialise() override;
    void send(const Data& _data) override;
    void send(const DataControl& _dataControl) override;
  private:
    QString m_serverAddress;
    Cyqlops::MQTT::Client* m_client = nullptr;
    QString m_communicationNamespace;
  };
  class Receiving : public AbstractReceivingCommunicationInterface
  {
  public:
    Receiving(const QString& _serverAddress, const QString& _communicationNamespace);
    virtual ~Receiving();
    void initialise() override;
    void send(const DataControl& _dataControl) override;
  private:
    QString m_serverAddress;
    Cyqlops::MQTT::Client* m_client = nullptr;
    QString m_communicationNamespace;
  };
} // namespace knowDataTransfert::MQTTCommunicationInterfaces

using namespace knowDataTransfert::MQTTCommunicationInterfaces;

Sending::Sending(const QString& _serverAddress, const QString& _communicationNamespace)
    : AbstractSendingCommunicationInterface(_serverAddress), m_serverAddress(_serverAddress),
      m_communicationNamespace(_communicationNamespace)
{
}

Sending::~Sending() { delete m_client; }

void Sending::initialise()
{
  m_client = new Cyqlops::MQTT::Client();
  if(not m_serverAddress.isEmpty())
  {
    m_client->setServerAddress(m_serverAddress);
  }

  QObject::connect(m_client, &Cyqlops::MQTT::Client::messageReceived,
                   [this](const QString& _topic, const Cyqlops::MQTT::Message& _message)
                   {
                     if(_topic == m_communicationNamespace + "/data_control")
                     {
                       handle(toDataControl(_message.parsed()));
                     }
                   });

  m_client->subscribe(m_communicationNamespace + "/data_control");
  m_client->start();
}

void Sending::send(const Data& _data)
{
  m_client->publish(m_communicationNamespace + "/data", toVariant(_data),
                    Cyqlops::MQTT::Client::SerializationMode::Cbor);
}

void Sending::send(const DataControl& _dataControl)
{
  m_client->publish(m_communicationNamespace + "/data_control", toVariant(_dataControl));
}

Receiving::Receiving(const QString& _serverAddress, const QString& _communicationNamespace)
    : AbstractReceivingCommunicationInterface(_serverAddress), m_serverAddress(_serverAddress),
      m_communicationNamespace(_communicationNamespace)
{
}

Receiving::~Receiving() { delete m_client; }

void Receiving::initialise()
{
  m_client = new Cyqlops::MQTT::Client();
  if(not m_serverAddress.isEmpty())
  {
    m_client->setServerAddress(m_serverAddress);
  }

  QObject::connect(m_client, &Cyqlops::MQTT::Client::messageReceived,
                   [this](const QString& _topic, const Cyqlops::MQTT::Message& _message)
                   {
                     if(_topic == m_communicationNamespace + "/data_control")
                     {
                       handle(toDataControl(_message.parsed()));
                     }
                     else if(_topic == m_communicationNamespace + "/data")
                     {
                       handle(toData(_message.parsed()));
                     }
                   });

  m_client->subscribe(m_communicationNamespace + "/data_control");
  m_client->subscribe(m_communicationNamespace + "/data");
  m_client->start();
}

void Receiving::send(const DataControl& _dataControl)
{
  m_client->publish(m_communicationNamespace + "/data_control", toVariant(_dataControl));
}

struct Factory::Private
{
  QString serverAddress;
};

Factory::Factory(const QString& _serverAddress) : d(new Private)
{
  d->serverAddress = _serverAddress;
}

Factory::~Factory() { delete d; }

knowDataTransfert::AbstractSendingCommunicationInterface*
  Factory::createSendingInterface(const QString& _communicationNamespace)
{
  return new Sending(d->serverAddress, _communicationNamespace);
}

knowDataTransfert::AbstractReceivingCommunicationInterface*
  Factory::createReceivingInterface(const QString& _communicationNamespace)
{
  return new Receiving(d->serverAddress, _communicationNamespace);
}
