#include "AbstractCommunicationInterfaces.h"

namespace Cyqlops::MQTT
{
  class Client;
}

namespace knowDataTransfert::MQTTCommunicationInterfaces
{
  class Factory : public AbstractCommunicationInterfaceFactory
  {
  public:
    /**
     * Create a MQTT Factory using the broker at \p _serverAddress (an empty string will use the
     * default localhost broker).
     */
    Factory(const QString& _serverAddress = QString());
    virtual ~Factory();
    AbstractSendingCommunicationInterface*
      createSendingInterface(const QString& _communicationNamespace) override;
    AbstractReceivingCommunicationInterface*
      createReceivingInterface(const QString& _communicationNamespace) override;
  private:
    struct Private;
    Private* const d = nullptr;
  };
} // namespace knowDataTransfert::MQTTCommunicationInterfaces
