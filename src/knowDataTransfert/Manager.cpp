#include "Manager.h"

#include <QUuid>

#include <cres_qt>

#include "AbstractCommunicationInterfaces.h"
#include "ReceiveJob_p.h"
#include "SendJob_p.h"

using namespace knowDataTransfert;

struct Manager::Private
{
  QString name;
  AbstractCommunicationInterfaceFactory* communicationInterfaceFactory;
  QStringList communicationNamespaces;
  QList<ReceiveJob*> receiveJobs;
  QList<SendJob*> sendJobs;
};

Manager::Manager(const QString& _name,
                 AbstractCommunicationInterfaceFactory* _communicationInterfaceFactory)
    : d(new Private)
{
  d->name = _name;
  d->communicationInterfaceFactory = _communicationInterfaceFactory;
}

Manager::~Manager()
{
  stopTransfert();
  waitForFinished();
  delete d->communicationInterfaceFactory;
  delete d;
}

cres_qresult<void> Manager::setupSendJob(Interfaces::SendIterator* _send_iterator,
                                         const QString& _communicationNamespace,
                                         const QStringList& _receivers)
{
  SendJob* newJob = new SendJob(
    d->name, _receivers, _send_iterator,
    d->communicationInterfaceFactory->createSendingInterface(_communicationNamespace));
  d->sendJobs.append(newJob);
  return cres_success();
}

cres_qresult<void> Manager::setupReceiveJob(Interfaces::ReceiveIterator* _receive_iterator,
                                            const QString& _communicationNamespace,
                                            const QString& _sender)
{
  ReceiveJob* receiveJob = new ReceiveJob(
    d->name, _sender, _receive_iterator,
    d->communicationInterfaceFactory->createReceivingInterface(_communicationNamespace));
  d->receiveJobs.append(receiveJob);
  return cres_success();
}

void Manager::stopTransfert()
{
  for(ReceiveJob* job : d->receiveJobs)
  {
    job->abort();
  }
  for(SendJob* job : d->sendJobs)
  {
    job->abort();
  }
}

void Manager::waitForFinished()
{
  for(ReceiveJob* job : d->receiveJobs)
  {
    job->waitForFinished();
  }
  for(SendJob* job : d->sendJobs)
  {
    job->waitForFinished();
  }
}
