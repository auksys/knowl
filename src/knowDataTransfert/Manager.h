#include "Forward.h"

namespace knowDataTransfert
{
  class Manager
  {
  public:
    Manager(const QString& _name, AbstractCommunicationInterfaceFactory* _communicationInterface);
    ~Manager();
    cres_qresult<void> setupSendJob(Interfaces::SendIterator* _send_iterator,
                                    const QString& _communicationNamespace,
                                    const QStringList& _receivers);
    cres_qresult<void> setupReceiveJob(Interfaces::ReceiveIterator* _receive_iterator,
                                       const QString& _communicationNamespace,
                                       const QString& _sender);
    void stopTransfert();
    void waitForFinished();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowDataTransfert
