#include "Messages.h"

using namespace knowDataTransfert;

DataControl DataControl::ready(const QString& _name)
{
  DataControl dc;
  dc.name = _name;
  dc.command = Command::Ready;
  return dc;
}

DataControl DataControl::started(const QString& _name)
{
  DataControl dc;
  dc.name = _name;
  dc.command = Command::Started;
  return dc;
}

DataControl DataControl::finished(const QString& _name, quint32 _last_seq)
{
  DataControl dc;
  dc.name = _name;
  dc.command = Command::Finished;
  dc.seqs.append(_last_seq);
  return dc;
}

DataControl DataControl::failed(const QString& _name, const QString& _errorMesssage)
{
  DataControl dc;
  dc.name = _name;
  dc.errorMessage = _errorMesssage;
  dc.command = Command::Failed;
  return dc;
}

DataControl DataControl::speedUp(const QString& _name)
{
  DataControl dc;
  dc.name = _name;
  dc.command = Command::SpeedUp;
  return dc;
}

DataControl DataControl::slowDown(const QString& _name)
{
  DataControl dc;
  dc.name = _name;
  dc.command = Command::SlowDown;
  return dc;
}

DataControl DataControl::seqMissing(const QString& _name, const QList<quint32>& _seqs)
{
  DataControl dc;
  dc.name = _name;
  dc.command = Command::SeqMissing;
  dc.seqs = _seqs;
  return dc;
}

DataControl DataControl::seqReceived(const QString& _name, quint32 _seq)
{
  DataControl dc;
  dc.name = _name;
  dc.command = Command::SeqReceived;
  dc.seqs.append(_seq);
  return dc;
}
