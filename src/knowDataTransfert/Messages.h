#pragma once

#include <QList>
#include <QString>

namespace knowDataTransfert
{
  struct Data
  {
    quint32 seq;
    QByteArray data;
  };
  struct DataControl
  {
    enum class Command : quint32
    {
      Ready = 0,
      Started = 1,
      Finished = 2,
      SlowDown = 3,
      SpeedUp = 4,
      Failed = 5,
      SeqMissing = 6,
      SeqReceived = 7,
    };

    QString name;
    QString errorMessage;
    Command command;
    QList<quint32> seqs;

    static DataControl ready(const QString& _name);
    static DataControl started(const QString& _name);
    static DataControl finished(const QString& _name, quint32 _last_seq);
    static DataControl failed(const QString& _name, const QString& _errorMesssage);
    static DataControl speedUp(const QString& _name);
    static DataControl slowDown(const QString& _name);
    static DataControl seqMissing(const QString& _name, const QList<quint32>& _seqs);
    static DataControl seqReceived(const QString& _name, quint32 _seq);
  };
} // namespace knowDataTransfert
