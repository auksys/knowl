#include "ReceiveJob_p.h"

#include <QCborMap>
#include <QMutex>
#include <QString>
#include <QThread>
#include <QWaitCondition>

#include <clog_qt>
#include <cres_qt>

#include "AbstractCommunicationInterfaces.h"
#include "Interfaces/ReceiveIterator.h"
#include "Messages.h"

using namespace knowDataTransfert;

struct ReceiveJob::Private : public QThread
{
  QString name, sender;
  AbstractReceivingCommunicationInterface* communicationInterface;
  Interfaces::ReceiveIterator* iterator;

  quint32 next_seq = 0;
  quint32 last_seq = 0;
  QHash<quint32, QByteArray> data;

  int current_message_index = 0;
  int current_message_count = 0;
  bool current_message_finished = true;
  QByteArray current_message;

  void run() override;
  void handle(const DataControl& _control);
  void handle(const Data& _data);

  bool sync_started;
  QMutex mutex;
  QWaitCondition waitCondition;

  bool finished = false;
  bool started = false;
  bool aborted = false;
};

void ReceiveJob::Private::run()
{
  while(not started)
  {
    communicationInterface->send(DataControl::ready(name));
    QMutexLocker l(&mutex);
    waitCondition.wait(&mutex, 1000); // Send message at 1Hz
  }
  clog_info("Receving data from '{}' to '{}' has started", sender, name);
  while(not aborted)
  {
    QHash<quint32, QByteArray> dataCopy;

    {
      QMutexLocker l(&mutex);
      dataCopy = data;
    }
    quint32 original_seq = next_seq;
    if(not dataCopy.isEmpty())
    {
      if(dataCopy.contains(next_seq))
      {
        while(dataCopy.contains(next_seq))
        {
          QCborParserError cbor_error;
          QCborValue msg = QCborValue::fromCbor(dataCopy[next_seq], &cbor_error);
          ++next_seq;

          if(cbor_error.error == QCborError::NoError)
          {
            QCborMap msg_map = msg.toMap();
            int message_index = msg_map["index"_kCs].toInteger();
            int message_count = msg_map["count"_kCs].toInteger();
            QByteArray message_data = msg_map["data"_kCs].toByteArray();

            if(current_message_finished)
            {
              if(message_index == 0)
              {
                current_message_index = 0;
                current_message_count = message_count;
                current_message_finished = false;
                current_message = message_data;
              }
              else
              {
                clog_error("First message should be index 0 but got: {} from sender {}",
                           message_index, sender);
                communicationInterface->send(
                  DataControl::failed(name, "Protocol error, wrong first index"));
                aborted = true;
              }
            }
            else
            {
              if(message_index == current_message_index + 1)
              {
                current_message_index = message_index;
                current_message += message_data;
              }
              else
              {
                clog_error(
                  "Discountinuity in message index got: {} should have gotten {} from sender {}",
                  message_index, current_message_index + 1, sender);
                communicationInterface->send(
                  DataControl::failed(name, "Protocol error, wrong index"));
                aborted = true;
              }
            }
            // Check if we got a full message now
            if(current_message_index + 1 == current_message_count)
            {
              current_message_finished = true;
              QCborParserError cbor_error;
              QCborValue data_v = QCborValue::fromCbor(current_message, &cbor_error);
              if(cbor_error.error == QCborError::NoError)
              {
                auto const [success, errorMessage] = iterator->next(data_v);
                if(not success)
                {
                  clog_error("While receiving from '{}', failed to handle data: {}", sender,
                             errorMessage.value());
                  communicationInterface->send(
                    DataControl::failed(name, errorMessage.value().get_message()));
                  aborted = true;
                }
              }
              else
              {
                clog_error("While receiving from '{}', failed to parse CBOR: {}", sender,
                           cbor_error.errorString());
                communicationInterface->send(DataControl::failed(name, cbor_error.errorString()));
                aborted = true;
              }
            }
          }
          else
          {
            clog_error("While receiving from '{}', failed to parse message CBOR: {}", sender,
                       cbor_error.errorString());
            communicationInterface->send(DataControl::failed(name, cbor_error.errorString()));
            aborted = true;
          }
        }
      }
      else
      {
        clog_info("Missing '{}' has '{}'", next_seq, dataCopy.keys().size());
        communicationInterface->send(DataControl::slowDown(name));
        communicationInterface->send(DataControl::seqMissing(name, {next_seq}));
      }
    }

    {
      QMutexLocker l(&mutex);
      bool not_enough_data = data.size() <= 1;
      bool too_much_data = data.size() > 1;

      l.unlock();
      // Throttle the communication
      if(not_enough_data)
      {
        communicationInterface->send(DataControl::speedUp(name));
      }
      else if(too_much_data)
      {
        communicationInterface->send(DataControl::slowDown(name));
      }
      l.relock();
      // Remove the processed data from

      for(quint32 i = original_seq; i < next_seq; ++i)
      {
        data.remove(i);
      }

      if(data.isEmpty())
      {
        if(finished)
        {
          if(last_seq == next_seq - 1)
          {
            break;
          }
          else
          {
            QList<quint32> seqs;
            for(quint32 i = next_seq; i < last_seq + 1; ++i)
            {
              seqs.append(i);
            }
            clog_info("Missing '{}' has '{}'", seqs.size(), dataCopy.keys().size());
            l.unlock();
            communicationInterface->send(DataControl::slowDown(name));
            communicationInterface->send(DataControl::seqMissing(name, seqs));
            l.relock();
          }
        }
      }
      waitCondition.wait(&mutex, 1000);
    }
  }
  if(finished and not aborted)
  {
    auto const [success, errorMessage] = iterator->finalise();
    if(success)
    {
      communicationInterface->send(DataControl::finished(name, next_seq - 1));
    }
    else
    {
      communicationInterface->send(DataControl::failed(name, errorMessage.value().get_message()));
    }
  }
  clog_info("Data transfer from '{}' to '{}' has finished: aborted: {} or finished: {}", sender,
            name, aborted, finished);
}

void ReceiveJob::Private::handle(const DataControl& _control)
{
  QMutexLocker l(&mutex);
  if(_control.name == sender)
  {
    if(not started and _control.command == DataControl::Command::Started)
    {
      started = true;
      waitCondition.wakeOne();
    }
    else
    {
      switch(_control.command)
      {
      case DataControl::Command::Started:
        // Ignore, can happen to be repeated
        break;
      case DataControl::Command::Finished:
      {
        finished = true;
        last_seq = _control.seqs.first();
        waitCondition.wakeOne();
        break;
      }
      case DataControl::Command::Failed:
      {
        clog_error("Synchronisation has failed with agent '{}' with error '{}'", _control.name,
                   _control.errorMessage);
        aborted = true;
        waitCondition.wakeOne();
        break;
      }
      default:
        clog_error("Unsupported control command: {}", quint32(_control.command));
      }
    }
  }
}

void ReceiveJob::Private::handle(const Data& _data)
{
  QMutexLocker l(&mutex);
  if(_data.seq >= next_seq)
  {
    data[_data.seq] = _data.data;
  }
  waitCondition.wakeOne();
}

ReceiveJob::ReceiveJob(const QString& _name, const QString& _sender,
                       Interfaces::ReceiveIterator* _iterator,
                       AbstractReceivingCommunicationInterface* _receivingInterface)
    : d(new Private)
{
  clog_info("Starting receive job of from {} to {}", _sender, _name);
  d->name = _name;
  d->sender = _sender;
  d->communicationInterface = _receivingInterface;
  d->iterator = _iterator;

  d->communicationInterface->addReceiver(
    std::bind<void (Private::*)(const DataControl&)>(&Private::handle, d, std::placeholders::_1));
  d->communicationInterface->addReceiver(
    std::bind<void (Private::*)(const Data&)>(&Private::handle, d, std::placeholders::_1));

  d->communicationInterface->initialise();
  d->start();
}

ReceiveJob::~ReceiveJob() {}

QString ReceiveJob::communicationNamespace() const
{
  return d->communicationInterface->communicationNamespace();
}

void ReceiveJob::abort() { d->aborted = true; }

void ReceiveJob::waitForFinished() { d->wait(); }
