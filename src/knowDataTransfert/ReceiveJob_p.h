#include "Forward.h"

namespace knowDataTransfert
{
  class ReceiveJob
  {
  public:
    ReceiveJob(const QString& _name, const QString& _sender, Interfaces::ReceiveIterator* _iterator,
               AbstractReceivingCommunicationInterface* _receivingInterface);
    ~ReceiveJob();
    QString communicationNamespace() const;
    void abort();
    void waitForFinished();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowDataTransfert
