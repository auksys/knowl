#include "SendJob_p.h"

#include <QCborMap>
#include <QCborValue>
#include <QHash>
#include <QMutex>
#include <QThread>

#include <clog_qt>
#include <cres_qt>

#include "AbstractCommunicationInterfaces.h"
#include "Interfaces/SendIterator.h"
#include "Messages.h"

using namespace knowDataTransfert;

struct SendJob::Private : public QThread
{
  QString name, dataset;
  QStringList receivers;
  AbstractSendingCommunicationInterface* sendingInterface;
  Interfaces::SendIterator* iterator;

  bool running = false;
  int sleep = 200;
  double sleep_average = 200;
  int sleep_count_update = 1;
  quint32 next_seq = 0;

  QMutex statusMutex;
  struct Status
  {
    bool ready;
  };

  QHash<QString, Status> statuses;

  int seq;

  void run() override;
  void handle(const DataControl& _control);

  struct DataInfo
  {
    QByteArray data;
    QStringList names;
  };

  QHash<quint32, DataInfo> seq2data;
};

void SendJob::Private::run()
{
  sendingInterface->send(DataControl::started(name));
  while(running and iterator->hasNext())
  {
    const auto [success, data, errorMessage] = iterator->next();
    if(success)
    {
      QByteArray arr = QCborValue(data.value()).toCbor();
      constexpr int slice_size = 1000;
      int count_msg = arr.size() / slice_size + 1;

      for(int i = 0; i < count_msg; ++i)
      {
        quint32 seq = next_seq++;
        QCborMap msg;
        msg["index"_kCs] = i;
        msg["count"_kCs] = count_msg;
        msg["data"_kCs] = QByteArray(arr.data() + i * slice_size,
                                     i == count_msg - 1 ? arr.size() % slice_size : slice_size);
        QByteArray msg_data = QCborValue(msg).toCbor();
        {
          QMutexLocker l(&statusMutex);
          seq2data[seq] = {msg_data, QStringList()};
        }
        sendingInterface->send(Data{seq, msg_data});
        msleep(sleep);
      }
    }
    else
    {
      sendingInterface->send(DataControl::failed(name, errorMessage.value().get_message()));
    }
    msleep(sleep);
  }
  sendingInterface->send(DataControl::finished(name, next_seq - 1));
  clog_info("Send job is finished.");
}

void SendJob::Private::handle(const DataControl& _control)
{
  QMutexLocker l(&statusMutex);
  if(receivers.contains(_control.name))
  {
    switch(_control.command)
    {
    case DataControl::Command::Ready:
    {
      statuses[_control.name].ready = true;
      break;
    }
    case DataControl::Command::Failed:
    {
      clog_error("Synchronisation has failed with agent '{}' with error '{}'", _control.name,
                 _control.errorMessage);
      break;
    }
    case DataControl::Command::SlowDown:
      sleep = sleep + (15 * sleep) / 100;
      sleep_average = (sleep_average * sleep_count_update + sleep) / (sleep_count_update + 1);
      ++sleep_count_update;
      break;
    case DataControl::Command::SpeedUp:
      sleep = std::max(20, sleep - (5 * sleep) / 100);
      sleep_average = (sleep_average * sleep_count_update + sleep) / (sleep_count_update + 1);
      ++sleep_count_update;
      if(sleep > sleep_average)
      {
        sleep = sleep_average;
      }
      else if(sleep < sleep_average / 2)
      {
        sleep = sleep_average / 2;
      }
      break;
    case DataControl::Command::Finished:
      clog_info("Agent '{}' has finished", _control.name);
      break;
    case DataControl::Command::SeqMissing:
    {
      clog_info("Missing: {} from {}", _control.seqs, _control.name);
      for(quint32 seq : _control.seqs)
      {
        if(seq2data.contains(seq))
        {
          sendingInterface->send(Data{seq, seq2data[seq].data});
        }
        else
        {
          sendingInterface->send(
            DataControl::failed(name, clog_qt::qformat("Unknown seq '{}'", seq)));
          running = false;
        }
      }
    }
    break;
    case DataControl::Command::SeqReceived:
      for(quint32 seq : _control.seqs)
      {
        DataInfo& data = seq2data[seq];
        if(not data.names.contains(_control.name))
        {
          data.names.append(_control.name);
        }
        if(data.names.size() == receivers.size())
        {
          seq2data.remove(seq);
        }
      }
      break;
    default:
      clog_error("Unsupported control command: {}", quint32(_control.command));
    }
    // Check if ready to start
    if(not running)
    {
      if(statuses.size() == receivers.size())
      {
        bool all_ready = true;
        for(QHash<QString, Status>::iterator it = statuses.begin(); it != statuses.end(); ++it)
        {
          if(it.value().ready == false)
          {
            clog_info("{} is not ready yet!", it.key());
            all_ready = false;
          }
        }
        if(all_ready)
        {
          clog_info("Start transfert to {}", receivers);
          running = true;
          start();
        }
      }
      else
      {
        clog_info("{} receivers are ready, waiting for {}", statuses.size(), receivers.size());
      }
    }
  }
}

SendJob::SendJob(const QString& _name, const QStringList& _receivers,
                 Interfaces::SendIterator* _iterator,
                 AbstractSendingCommunicationInterface* _sendingInterface)
    : d(new Private)
{
  clog_info("Starting send job from {} to {}", _name, _receivers);
  d->name = _name;
  d->receivers = _receivers;
  d->sendingInterface = _sendingInterface;
  d->iterator = _iterator;

  d->sendingInterface->addReceiver(std::bind(&Private::handle, d, std::placeholders::_1));
  d->sendingInterface->initialise();
}

SendJob::~SendJob() {}

QString SendJob::communicationNamespace() const
{
  return d->sendingInterface->communicationNamespace();
}

void SendJob::abort() { d->running = false; }

void SendJob::waitForFinished() { d->wait(); }
