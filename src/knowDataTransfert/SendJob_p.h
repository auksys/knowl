#include "Forward.h"

namespace knowDataTransfert
{
  class SendJob
  {
  public:
    SendJob(const QString& _name, const QStringList& _receivers,
            Interfaces::SendIterator* _iterator,
            AbstractSendingCommunicationInterface* _sendingInterface);
    ~SendJob();
    QString communicationNamespace() const;
    void abort();
    void waitForFinished();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowDataTransfert
