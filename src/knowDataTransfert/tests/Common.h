#include <clog_qt>
#include <cres_qt>
#include <knowCore/Test.h>

#include <knowDataTransfert/Interfaces/ReceiveIterator.h>
#include <knowDataTransfert/Interfaces/SendIterator.h>
#include <knowDataTransfert/Manager.h>

class TestSendIterator : public knowDataTransfert::Interfaces::SendIterator
{
public:
  bool hasNext() const override { return m_seq < 10; }
  cres_qresult<QCborValue> next() override
  {
    QCborMap m;
    m[QStringLiteral("value")] = m_seq++;
    return cres_success(m);
  }
private:
  quint32 m_seq = 0;
};

class TestReceiveIterator : public knowDataTransfert::Interfaces::ReceiveIterator
{
public:
  TestReceiveIterator(QList<quint32>* _destination) : m_destination(_destination) {}
  virtual cres_qresult<void> next(const QCborValue& _data)
  {
    m_tmp.append(_data.toMap()[QStringLiteral("value")].toInteger());
    return cres_success();
  }
  virtual cres_qresult<void> finalise()
  {
    *m_destination = m_tmp;
    return cres_success();
  }
private:
  QList<quint32> m_tmp;
  QList<quint32>* m_destination;
};

class TestSendBigDataIterator : public knowDataTransfert::Interfaces::SendIterator
{
public:
  TestSendBigDataIterator(const QList<QByteArray>& _data) : m_data(_data) {}
  bool hasNext() const override { return m_seq < m_data.size(); }
  cres_qresult<QCborValue> next() override
  {
    QCborMap m;
    m[QStringLiteral("value")] = m_data[m_seq++];
    return cres_success(m);
  }
private:
  qint32 m_seq = 0;
  QList<QByteArray> m_data;
};

class TestReceiveBigDataIterator : public knowDataTransfert::Interfaces::ReceiveIterator
{
public:
  TestReceiveBigDataIterator(QList<QByteArray>* _destination) : m_destination(_destination) {}
  virtual cres_qresult<void> next(const QCborValue& _data)
  {
    m_tmp.append(_data.toMap()[QStringLiteral("value")].toByteArray());
    return cres_success();
  }
  virtual cres_qresult<void> finalise()
  {
    *m_destination = m_tmp;
    return cres_success();
  }
private:
  QList<QByteArray> m_tmp;
  QList<QByteArray>* m_destination;
};

void runDataExchange(knowDataTransfert::Manager* manager_src,
                     knowDataTransfert::Manager* manager_dst_1,
                     knowDataTransfert::Manager* manager_dst_2)
{
  QList<quint32> output_1, output_2;
  ;

  // Test with 1
  output_1.clear();
  output_2.clear();
  manager_src->setupSendJob(new TestSendIterator, "com1", {"dst_1"});
  manager_dst_1->setupReceiveJob(new TestReceiveIterator(&output_1), "com1", "src");
  KNOWCORE_TEST_WAIT_FOR(output_1.size() == 10);
  QCOMPARE(output_1, QList<quint32>{KNOWCORE_LIST(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)});

  // Test with 2
  output_1.clear();
  output_2.clear();
  manager_src->setupSendJob(new TestSendIterator, "com2", {"dst_1", "dst_2"});
  manager_dst_1->setupReceiveJob(new TestReceiveIterator(&output_1), "com2", "src");
  manager_dst_2->setupReceiveJob(new TestReceiveIterator(&output_2), "com2", "src");
  KNOWCORE_TEST_WAIT_FOR(output_1.size() == 10 and output_2.size() == 10);
  QCOMPARE(output_1, QList<quint32>{KNOWCORE_LIST(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)});
  QCOMPARE(output_2, QList<quint32>{KNOWCORE_LIST(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)});
}

void runBigDataExchange(knowDataTransfert::Manager* manager_src,
                        knowDataTransfert::Manager* manager_dst_1,
                        knowDataTransfert::Manager* manager_dst_2)
{
  QList<QByteArray> input;

  int next = 0;
  for(int i = 0; i < 3; ++i)
  {
    QByteArray data;
    data.resize(1000 * sizeof(quint32));
    quint32* data_ptr = reinterpret_cast<quint32*>(data.data());
    for(int j = 0; j < 1000; ++j)
    {
      data_ptr[j] = next++;
    }
    input.append(data);
  }

  QList<QByteArray> output_1, output_2;
  ;

  // Test with 1
  output_1.clear();
  output_2.clear();
  manager_src->setupSendJob(new TestSendBigDataIterator(input), "bigcom1", {"dst_1"});
  manager_dst_1->setupReceiveJob(new TestReceiveBigDataIterator(&output_1), "bigcom1", "src");
  KNOWCORE_TEST_WAIT_FOR(output_1.size() == input.size());
  QCOMPARE(output_1, input);

  // Test with 2
  output_1.clear();
  output_2.clear();
  manager_src->setupSendJob(new TestSendBigDataIterator(input), "bigcom2", {"dst_1", "dst_2"});
  manager_dst_1->setupReceiveJob(new TestReceiveBigDataIterator(&output_1), "bigcom2", "src");
  manager_dst_2->setupReceiveJob(new TestReceiveBigDataIterator(&output_2), "bigcom2", "src");
  KNOWCORE_TEST_WAIT_FOR(output_1.size() == input.size() and output_2.size() == input.size());
  QCOMPARE(output_1, input);
  QCOMPARE(output_2, input);
}
