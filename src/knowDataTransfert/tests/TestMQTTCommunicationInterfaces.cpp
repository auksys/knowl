#include "TestMQTTCommunicationInterfaces.h"

#include "Common.h"

#include <knowDataTransfert/MQTTCommunicationInterfaces.h>

void TestMQTTCommunicationInterfaces::testDataExchange()
{
  QByteArray mqtt_test_server = qgetenv("KNOWL_MQTT_TEST_SERVER");

  knowDataTransfert::Manager manager_src(
    "src", new knowDataTransfert::MQTTCommunicationInterfaces::Factory(mqtt_test_server));
  knowDataTransfert::Manager manager_dst_1(
    "dst_1", new knowDataTransfert::MQTTCommunicationInterfaces::Factory(mqtt_test_server));
  knowDataTransfert::Manager manager_dst_2(
    "dst_2", new knowDataTransfert::MQTTCommunicationInterfaces::Factory(mqtt_test_server));

  runDataExchange(&manager_src, &manager_dst_1, &manager_dst_2);
}

void TestMQTTCommunicationInterfaces::testBigDataExchange()
{
  QByteArray mqtt_test_server = qgetenv("KNOWL_MQTT_TEST_SERVER");

  knowDataTransfert::Manager manager_src(
    "src", new knowDataTransfert::MQTTCommunicationInterfaces::Factory(mqtt_test_server));
  knowDataTransfert::Manager manager_dst_1(
    "dst_1", new knowDataTransfert::MQTTCommunicationInterfaces::Factory(mqtt_test_server));
  knowDataTransfert::Manager manager_dst_2(
    "dst_2", new knowDataTransfert::MQTTCommunicationInterfaces::Factory(mqtt_test_server));

  runBigDataExchange(&manager_src, &manager_dst_1, &manager_dst_2);
}

QTEST_MAIN(TestMQTTCommunicationInterfaces)
