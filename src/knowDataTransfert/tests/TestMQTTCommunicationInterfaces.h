#include <QtTest/QtTest>

class TestMQTTCommunicationInterfaces : public QObject
{
  Q_OBJECT
private slots:
  void testDataExchange();
  void testBigDataExchange();
};
