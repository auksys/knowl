#include "TestManager.h"

#include <knowDataTransfert/Test.h>

#include "Common.h"

void TestManager::testDataExchange()
{
  knowDataTransfert::Test::CommunicationCenter commCenter;
  knowDataTransfert::Manager manager_src(
    "src", new knowDataTransfert::Test::CommunicationInterfaceFactory(&commCenter));
  knowDataTransfert::Manager manager_dst_1(
    "dst_1", new knowDataTransfert::Test::CommunicationInterfaceFactory(&commCenter));
  knowDataTransfert::Manager manager_dst_2(
    "dst_2", new knowDataTransfert::Test::CommunicationInterfaceFactory(&commCenter));

  runDataExchange(&manager_src, &manager_dst_1, &manager_dst_2);
}

void TestManager::testBigDataExchange()
{
  knowDataTransfert::Test::CommunicationCenter commCenter;
  knowDataTransfert::Manager manager_src(
    "src", new knowDataTransfert::Test::CommunicationInterfaceFactory(&commCenter));
  knowDataTransfert::Manager manager_dst_1(
    "dst_1", new knowDataTransfert::Test::CommunicationInterfaceFactory(&commCenter));
  knowDataTransfert::Manager manager_dst_2(
    "dst_2", new knowDataTransfert::Test::CommunicationInterfaceFactory(&commCenter));

  runBigDataExchange(&manager_src, &manager_dst_1, &manager_dst_2);
}

QTEST_MAIN(TestManager)
