#include <QtTest/QtTest>

class TestManager : public QObject
{
  Q_OBJECT
private slots:
  void testDataExchange();
  void testBigDataExchange();
};
