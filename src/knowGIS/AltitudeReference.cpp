#include "AltitudeReference.h"

#include <GeographicLib/Geoid.hpp>

#include <clog_qt>

#include "GeoPoint.h"

namespace
{
  GeographicLib::Geoid* getEgm84()
  {
    static GeographicLib::Geoid* egm84 = nullptr;

    if(not egm84)
    {
      egm84 = new GeographicLib::Geoid("EGM84", "", true, true);
    }
    return egm84;
  }

  GeographicLib::Geoid* getEgm96()
  {
    static GeographicLib::Geoid* egm96 = nullptr;

    if(not egm96)
    {
      egm96 = new GeographicLib::Geoid("EGM96", "", true, true);
    }
    return egm96;
  }

  GeographicLib::Geoid* getEgm2008()
  {
    static GeographicLib::Geoid* egm2008 = nullptr;

    if(not egm2008)
    {
      egm2008 = new GeographicLib::Geoid("EGM2008", "", true, true);
    }
    return egm2008;
  }
} // namespace

qreal knowGIS::AltitudeReference::get(knowGIS::AltitudeReference::Type _type,
                                      const GeoPoint& _geopoint)
{
  try
  {
    switch(_type)
    {
    case knowGIS::AltitudeReference::Type::WGS84_Ellipsoid:
      return 0.0;
    case knowGIS::AltitudeReference::Type::EGM84_Geoid:
      return (*getEgm84())(_geopoint.latitude(), _geopoint.longitude());
    case knowGIS::AltitudeReference::Type::EGM96_Geoid:
      return (*getEgm96())(_geopoint.latitude(), _geopoint.longitude());
    case knowGIS::AltitudeReference::Type::EGM2008_Geoid:
      return (*getEgm2008())(_geopoint.latitude(), _geopoint.longitude());
    }
  }
  catch(const GeographicLib::GeographicErr& err)
  {
    clog_error("Ellipsoid data is not available: {}", err.what());
  }
  return std::numeric_limits<double>::quiet_NaN();
}
