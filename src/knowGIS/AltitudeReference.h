#pragma once

#include <QtGlobal>

namespace knowGIS
{
  class GeoPoint;
  class AltitudeReference
  {
  public:
    enum class Type
    {
      WGS84_Ellipsoid,
      EGM84_Geoid,
      EGM96_Geoid,
      EGM2008_Geoid
    };
  public:
    static qreal get(Type _type, const GeoPoint& _geopoint);
  };
} // namespace knowGIS
