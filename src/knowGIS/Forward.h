#pragma once

#include <Cartography/Forward.h>
#include <knowCore/Forward.h>

namespace knowGIS
{
  class GeometryObject;
  class GeoPose;
  class GeoPoint;
  class Point;
  class Pose;
} // namespace knowGIS
