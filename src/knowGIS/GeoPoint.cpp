#include "GeoPoint.h"

#include <QCborMap>

#include <Cyqlops/Crypto/Hash.h>

#include <knowCore/MetaTypeImplementation.h>

#include "GeometryObject.h"
#include "NamedType.h"
#include "Uris/askcore_gis.h"

using namespace knowGIS;

#define LONGITUDE_KEY QStringLiteral("longitude")
#define LATITUDE_KEY QStringLiteral("latitude")
#define ALTITUDE_KEY QStringLiteral("altitude")

QByteArray GeoPoint::md5() const
{
  return Cyqlops::Crypto::Hash::md5(longitude(), latitude(), altitude());
}

QJsonValue GeoPoint::toJsonValue() const
{
  QJsonObject object;
  object[LONGITUDE_KEY] = double(longitude());
  object[LATITUDE_KEY] = double(latitude());
  object[ALTITUDE_KEY] = double(altitude());
  return object;
}

cres_qresult<GeoPoint> GeoPoint::fromJsonValue(const QJsonValue& _json_value)
{
  if(_json_value.isObject())
  {
    QJsonObject object = _json_value.toObject();
    return cres_success(GeoPoint(Cartography::Longitude(object.value(LONGITUDE_KEY).toDouble()),
                                 Cartography::Latitude(object.value(LATITUDE_KEY).toDouble()),
                                 Cartography::Altitude(object.value(ALTITUDE_KEY).toDouble())));
  }
  else
  {
    return cres_failure("Expected object got {}", _json_value);
  }
}

QCborValue GeoPoint::toCborValue() const
{
  QCborMap object;
  object[LONGITUDE_KEY] = double(longitude());
  object[LATITUDE_KEY] = double(latitude());
  object[ALTITUDE_KEY] = double(altitude());
  return object;
}

cres_qresult<GeoPoint> GeoPoint::fromCborValue(const QCborValue& _cbor_value)
{
  if(_cbor_value.isMap())
  {
    QCborMap cbor_map = _cbor_value.toMap();
    return cres_success(GeoPoint(Cartography::Longitude(cbor_map.value(LONGITUDE_KEY).toDouble()),
                                 Cartography::Latitude(cbor_map.value(LATITUDE_KEY).toDouble()),
                                 Cartography::Altitude(cbor_map.value(ALTITUDE_KEY).toDouble())));
  }
  else
  {
    return cres_failure("Expected map got {}", _cbor_value);
  }
}

KNOWCORE_DEFINE_CONVERT(GeoPoint, _from, QString, _to, Safe)
{
  *_to = clog_qt::qformat("({} {} {})@{}", _from->x(), _from->y(), _from->z(),
                          _from->coordinateSystem().srid());
  return cres_success();
}

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(GeoPoint)
cres_qresult<QByteArray> md5(const GeoPoint& _value) const override
{
  return cres_success(_value.md5());
}
cres_qresult<QJsonValue> toJsonValue(const GeoPoint& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(_value.toJsonValue());
}
cres_qresult<void> fromJsonValue(GeoPoint* _value, const QJsonValue& _json_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  cres_try(*_value, GeoPoint::fromJsonValue(_json_value));
  return cres_success();
}
cres_qresult<QCborValue> toCborValue(const GeoPoint& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(_value.toCborValue());
}
cres_qresult<void> fromCborValue(GeoPoint* _value, const QCborValue& _cbor_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  cres_try(*_value, GeoPoint::fromCborValue(_cbor_value));
  return cres_success();
}
cres_qresult<QString> printable(const GeoPoint& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const GeoPoint& _value,
                                   const SerialisationContexts&) const override
{
  EuclidSystem::point pt(_value.longitude(), _value.latitude(), _value.altitude());
  return cres_success(QString::fromStdString(euclid::io::to_wkt(pt)));
}
cres_qresult<void> fromRdfLiteral(GeoPoint* _value, const QString& _serialised,
                                  const knowCore::DeserialisationContexts&) const override
{
  EuclidSystem::geometry geometry
    = euclid::io::from_wkt<EuclidSystem::geometry>(_serialised.toStdString());
  if(EuclidSystem::try_cast_to_point point = geometry)
  {
    *_value = GeoPoint(Cartography::Longitude(point->x()), Cartography::Latitude(point->y()),
                       Cartography::Altitude(point->z()));
    return cres_success();
  }
  else
  {
    return cres_failure("Invalid wkt '{}' expected a point", _serialised);
  }
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(GeoPoint)

KNOWCORE_DEFINE_METATYPE(knowGIS::GeoPoint, knowGIS::Uris::askcore_gis::geopoint,
                         knowCore::MetaTypeTraits::OnlyEqualComparable
                           | knowCore::MetaTypeTraits::ToString)
