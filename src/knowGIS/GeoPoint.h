#pragma once

#include <Cartography/GeoPoint.h>

#include <clog_qt>
#include <cres_qt>

namespace knowGIS
{
  /**
   * GeoPoint is a @ref Point whose coordinateSystem is wgs84
   */
  class GeoPoint : public Cartography::GeoPoint
  {
  public:
    using Cartography::GeoPoint::GeoPoint;
    GeoPoint(const Cartography::GeoPoint& _rhs) : Cartography::GeoPoint(_rhs) {}
    QByteArray md5() const;
    QJsonValue toJsonValue() const;
    static cres_qresult<GeoPoint> fromJsonValue(const QJsonValue& _value);
    QCborValue toCborValue() const;
    static cres_qresult<GeoPoint> fromCborValue(const QCborValue& _value);
  };
} // namespace knowGIS

clog_format_declare_formatter(knowGIS::GeoPoint)
{
  return format_to(ctx.out(), "[lon: {} lat: {} alt: {}]", p.longitude(), p.latitude(),
                   p.altitude());
}

#include <knowCore/MetaType.h>
KNOWCORE_DECLARE_FULL_METATYPE(knowGIS, GeoPoint);
