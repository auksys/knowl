#include "GeoPose.h"

#include <QCborMap>
#include <QJsonObject>

#include <Cyqlops/Crypto/Hash.h>

#include "Pose.h"

using namespace knowGIS;

Pose GeoPose::transform(const Cartography::CoordinateSystem& _coordinateSystem) const
{
  return Pose(position().transform(_coordinateSystem), orientation());
}

GeoPose::operator Pose() const { return Pose(position(), orientation()); }

#include <knowCore/MetaTypeImplementation.h>
#include <knowCore/Uris/askcore_datatype.h>

KNOWCORE_DEFINE_CONVERT(knowGIS::GeoPose, _from, QString, _to, Safe)
{
  *_to = clog_qt::to_qstring(*_from);
  return cres_success();
}

#define POSITION_KEY u8"position"_kCs
#define ORIENTATION_KEY u8"orientation"_kCs

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(GeoPose)
cres_qresult<QByteArray> md5(const GeoPose& _value) const override
{
  return cres_success(
    Cyqlops::Crypto::Hash::md5(_value.position().md5(), _value.orientation().md5()));
}
cres_qresult<QJsonValue> toJsonValue(const GeoPose& _value,
                                     const SerialisationContexts&) const override
{
  QJsonObject object;
  object[POSITION_KEY] = _value.position().toJsonValue();
  object[ORIENTATION_KEY] = _value.orientation().toJsonValue();
  return cres_success(object);
}
cres_qresult<void> fromJsonValue(GeoPose* _value, const QJsonValue& _json_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  if(_json_value.isObject())
  {
    QJsonObject object = _json_value.toObject();
    cres_try(GeoPoint position, GeoPoint::fromJsonValue(object.value(POSITION_KEY)));
    cres_try(Quaternion orientation, Quaternion::fromJsonValue(object.value(ORIENTATION_KEY)));

    *_value = GeoPose(position, orientation);
    return cres_success();
  }
  else
  {
    return expectedError("object", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const GeoPose& _value,
                                     const SerialisationContexts&) const override
{
  QCborMap object;
  object[POSITION_KEY] = _value.position().toCborValue();
  object[ORIENTATION_KEY] = _value.orientation().toCborValue();
  return cres_success(object);
}
cres_qresult<void> fromCborValue(GeoPose* _value, const QCborValue& _cbor_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  if(_cbor_value.isMap())
  {
    QCborMap cbor_map = _cbor_value.toMap();
    cres_try(GeoPoint position, GeoPoint::fromCborValue(cbor_map.value(POSITION_KEY)));
    cres_try(Quaternion orientation, Quaternion::fromCborValue(cbor_map.value(ORIENTATION_KEY)));

    *_value = GeoPose(position, orientation);
    return cres_success();
  }
  else
  {
    return expectedError("map", _cbor_value);
  }
}
cres_qresult<QString> printable(const GeoPose& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const GeoPose& _value,
                                   const SerialisationContexts& _contexts) const override
{
  return toJsonString(_value, _contexts);
}
cres_qresult<void> fromRdfLiteral(GeoPose* _value, const QString& _serialised,
                                  const knowCore::DeserialisationContexts& _contexts) const override
{
  return fromJsonString(_value, _serialised, _contexts);
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(GeoPose)

KNOWCORE_DEFINE_METATYPE(knowGIS::GeoPose, knowCore::Uris::askcore_datatype::geopose,
                         knowCore::MetaTypeTraits::OnlyEqualComparable
                           | knowCore::MetaTypeTraits::ToString)

#include <knowGIS/GeometryObject.h>

template<>
struct knowCore::Comparator<knowCore::ComparisonOperator::GeoWithin, knowGIS::GeoPose,
                            knowGIS::GeometryObject>
{
  static inline cres_qresult<bool> compare(const knowGIS::GeoPose& _left,
                                           const knowGIS::GeometryObject& _right)
  {
    return cres_success(_right.contains(_left.position()));
  }
};

KNOWCORE_REGISTER_COMPARATORS_FROM((GeoWithin), knowGIS::GeoPose, knowGIS::GeometryObject)
