#include "GeometryObject.h"

#include <QJsonDocument>
#include <QString>

#include <QJsonArray>
#include <QJsonObject>

#include <Cartography/CoordinateSystem.h>
#include <Cartography/Interface/Gdal.h>

#include "Point.h"
#include "Uris/geo.h"

#include <euclid/convert>
#include <euclid/ops>
#include <euclid/proj>

using namespace knowGIS;

GeometryObject::~GeometryObject() {}

GeometryObject& GeometryObject::operator=(const Cartography::GeometryObject& _rhs)
{
  Cartography::GeometryObject::operator=(_rhs);
  return *this;
}

QList<GeometryObject> GeometryObject::load(const QString& _filename)
{
  QList<Cartography::GeometryObject> c_objects = Cartography::GeometryObject::load(_filename);

  QList<GeometryObject> objects;

  for(const Cartography::GeometryObject& c_object : c_objects)
  {
    objects.push_back(c_object);
  }
  return objects;
}

Point GeometryObject::toPoint() const
{
  EuclidSystem::geometry this_geometry = geometry();
  if(EuclidSystem::try_cast_to_point point = this_geometry)
  {
    return Point(point, coordinateSystem());
  }
  else
  {
    return Point();
  }
}

Point GeometryObject::centroid() const
{
  return Point(euclid::ops::centroid(geometry()), coordinateSystem());
}

#include <knowCore/MetaTypeImplementation.h>

KNOWCORE_DEFINE_CONVERT(GeometryObject, _from, QString, _to, Safe)
{
  if(_from->isValid())
  {
    *_to = _from->toEWKT();
    return cres_success();
  }
  else
  {
    return cres_failure("invalid geometry");
  }
}

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(GeometryObject)
cres_qresult<QByteArray> md5(const GeometryObject& _value) const override
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  hash.addData(_value.toWKB());
  return cres_success(hash.result());
}
cres_qresult<QJsonValue> toJsonValue(const GeometryObject& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(_value.toJsonValue());
}
cres_qresult<void> fromJsonValue(GeometryObject* _value, const QJsonValue& _json_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  if(_json_value.isString())
  {
    return cres_try_assign(_value, GeometryObject::fromJsonValue(_json_value.toString()));
    return cres_success();
  }
  else
  {
    return expectedError("string", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const GeometryObject& _value,
                                     const SerialisationContexts&) const override
{
  if(_value.isValid())
  {
    return cres_success(_value.toEWKB());
  }
  else
  {
    return cres_success(QCborValue("GEOMETRY EMPTY"));
  }
}
cres_qresult<void> fromCborValue(GeometryObject* _value, const QCborValue& _cbor_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  if(_cbor_value.isByteArray())
  {
    return cres_try_assign(_value, GeometryObject::fromEWKB(_cbor_value.toByteArray()));
  }
  else if(_cbor_value.isString() and _cbor_value.toString() == "GEOMETRY EMPTY")
  {
    *_value = GeometryObject();
    return cres_success();
  }
  else
  {
    return expectedError("bytearray", _cbor_value);
  }
}
cres_qresult<QString> printable(const GeometryObject& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const GeometryObject& _value,
                                   const SerialisationContexts&) const override
{
  return cres_success(_value.toEWKT());
}
cres_qresult<void> fromRdfLiteral(GeometryObject* _value, const QString& _serialised,
                                  const knowCore::DeserialisationContexts&) const override
{
  return cres_try_assign(_value, GeometryObject::fromWKT(_serialised));
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(GeometryObject)

KNOWCORE_DEFINE_METATYPE(GeometryObject, knowGIS::Uris::geo::Geometry,
                         knowCore::MetaTypeTraits::DebugStreamOperator
                           | knowCore::MetaTypeTraits::OnlyEqualComparable
                           | knowCore::MetaTypeTraits::ToString)
KNOWCORE_DEFINE_METATYPE_ALIASES(Uris::geo::wktLiteral, Uris::geo::gmlLiteral, Uris::geo::Geometry)

template<>
struct knowCore::Comparator<knowCore::ComparisonOperator::AlmostEqual, knowGIS::GeometryObject,
                            knowGIS::GeometryObject>
{
  static inline cres_qresult<bool> compare(const knowGIS::GeometryObject& _left,
                                           const knowGIS::GeometryObject& _right)
  {
    return cres_success(_left.difference(_right).area()
                        < 0.001 * std::min(_left.area(), _right.area()));
  }
};

template<>
struct knowCore::Comparator<knowCore::ComparisonOperator::GeoContains, knowGIS::GeometryObject,
                            knowGIS::GeometryObject>
{
  static inline cres_qresult<bool> compare(const knowGIS::GeometryObject& _left,
                                           const knowGIS::GeometryObject& _right)
  {
    return cres_success(_right.within(_left)); // Contains is symetric of within
  }
};

template<>
struct knowCore::Comparator<knowCore::ComparisonOperator::GeoWithin, knowGIS::GeometryObject,
                            knowGIS::GeometryObject>
{
  static inline cres_qresult<bool> compare(const knowGIS::GeometryObject& _left,
                                           const knowGIS::GeometryObject& _right)
  {
    return cres_success(_left.within(_right));
  }
};
template<>
struct knowCore::Comparator<knowCore::ComparisonOperator::GeoOverlaps, knowGIS::GeometryObject,
                            knowGIS::GeometryObject>
{
  static inline cres_qresult<bool> compare(const knowGIS::GeometryObject& _left,
                                           const knowGIS::GeometryObject& _right)
  {
    return cres_success(_left.overlaps(_right));
  }
};

template<>
struct knowCore::Comparator<knowCore::ComparisonOperator::GeoIntersects, knowGIS::GeometryObject,
                            knowGIS::GeometryObject>
{
  static inline cres_qresult<bool> compare(const knowGIS::GeometryObject& _left,
                                           const knowGIS::GeometryObject& _right)
  {
    return cres_success(_left.intersects(_right));
  }
};

KNOWCORE_REGISTER_COMPARATORS((AlmostEqual, GeoContains, GeoWithin, GeoOverlaps, GeoIntersects),
                              knowGIS::GeometryObject)
