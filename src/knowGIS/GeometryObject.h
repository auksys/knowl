#pragma once

#include <QSharedDataPointer>
#include <QVariant>

#include <Cartography/Forward.h>

#include <clog_qt>
#include <cres_qt>

#include "EuclidSystem.h"
#include "Forward.h"

#include <Cartography/GeometryObject.h>

class QString;

namespace knowGIS
{
  /**
   * Extend and integrate a \ref Cartography::GeometryObject into knowCore MetaType system.
   */
  class GeometryObject : public Cartography::GeometryObject
  {
  public:
    using Cartography::GeometryObject::GeometryObject;
    using Cartography::GeometryObject::operator=;
    GeometryObject(const Cartography::GeometryObject& _rhs) : Cartography::GeometryObject(_rhs) {}
    ~GeometryObject();
    GeometryObject&
      operator=(const Cartography::GeometryObject& _rhs); // has to be explicitly defined because
                                                          // of deprecation of generated ones
    /**
     * Convert the geometry to a Point
     */
    Point toPoint() const;
    /**
     * @return the center point of a geometry
     */
    Point centroid() const;
  public:
    static cres_qresult<GeometryObject> fromEWKB(const QByteArray& _data)
    {
      return Cartography::GeometryObject::fromEWKB(_data);
    }
    static cres_qresult<GeometryObject> fromWKB(const QByteArray& _data, bool _extented = false)
    {
      return Cartography::GeometryObject::fromWKB(_data, _extented);
    }
    static cres_qresult<GeometryObject> fromWKT(const QString& _data)
    {
      return Cartography::GeometryObject::fromWKT(_data);
    }
    static cres_qresult<GeometryObject> fromGML(const QString& _data)
    {
      return Cartography::GeometryObject::fromGML(_data);
    }
    static cres_qresult<GeometryObject> fromJson(const QString& _data)
    {
      return Cartography::GeometryObject::fromJson(_data);
    }
    static cres_qresult<GeometryObject> fromJsonValue(const QJsonValue& _data)
    {
      return Cartography::GeometryObject::fromJsonValue(_data);
    }
    /**
     * Convenient function that return a GeometryObject from a variant following the GeoJSON
     * convension
     */
    static cres_qresult<GeometryObject> fromGeoVariant(const QVariant& _data)
    {
      return Cartography::GeometryObject::fromGeoVariant(_data);
    }
    static cres_qresult<GeometryObject> polygon(const QVariantList& _exteriorRing,
                                                const QVariantList& _interiorRings = QVariantList())
    {
      return Cartography::GeometryObject::polygon(_exteriorRing, _interiorRings);
    }
  public:
    static QList<GeometryObject> load(const QString& _filename);
  };
} // namespace knowGIS

#include <knowCore/Formatter.h>
clog_format_declare_formatter(knowGIS::GeometryObject)
{
  if(p.isValid())
  {
    return std::format_to(ctx.out(), "{}", p.toWKT());
  }
  else
  {
    return std::format_to(ctx.out(), "[invalid geometry]");
  }
}

#include <knowCore/MetaType.h>
KNOWCORE_DECLARE_FULL_METATYPE(knowGIS, GeometryObject);

#include "Operators.h"

namespace knowGIS
{
  template<>
  struct intersects<GeometryObject>
  {
    bool operator()(const GeometryObject& _lhs, const GeometryObject& _rhs) const
    {
      return _lhs.intersects(_rhs);
    }
  };
  template<>
  struct overlaps<GeometryObject>
  {
    bool operator()(const GeometryObject& _lhs, const GeometryObject& _rhs) const
    {
      return _lhs.overlaps(_rhs);
    }
  };
  template<>
  struct contains<GeometryObject>
  {
    bool operator()(const GeometryObject& _lhs, const GeometryObject& _rhs) const
    {
      return _lhs.contains(_rhs);
    }
  };
  template<>
  struct within<GeometryObject>
  {
    bool operator()(const GeometryObject& _lhs, const GeometryObject& _rhs) const
    {
      return _lhs.within(_rhs);
    }
  };
  template<>
  struct touches<GeometryObject>
  {
    bool operator()(const GeometryObject& _lhs, const GeometryObject& _rhs) const
    {
      return _lhs.touches(_rhs);
    }
  };
  template<>
  struct disjoint<GeometryObject>
  {
    bool operator()(const GeometryObject& _lhs, const GeometryObject& _rhs) const
    {
      return _lhs.disjoint(_rhs);
    }
  };
} // namespace knowGIS
