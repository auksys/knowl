#pragma once

#include <cext_nt>

#include <Cyqlops/Crypto/Hash.h>

namespace Cyqlops::Crypto::Hash
{
  template<typename _T_, typename _Tag_, int _Options_>
  struct Compute<cext_named_type<_T_, _Tag_, _Options_>>
  {
    Compute(QCryptographicHash& _algorithm, const cext_named_type<_T_, _Tag_, _Options_>& _v)
    {
      compute(_algorithm, _v.get_value());
    }
  };
} // namespace Cyqlops::Crypto::Hash
