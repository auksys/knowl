namespace knowGIS
{
  template<typename _T_>
  struct overlaps;
  template<typename _T_>
  struct intersects;
  template<typename _T_>
  struct contains;
  template<typename _T_>
  struct within;
  template<typename _T_>
  struct touches;
  template<typename _T_>
  struct disjoint;
} // namespace knowGIS
