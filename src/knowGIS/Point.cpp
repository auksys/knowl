#include "Point.h"

#include <QCborMap>
#include <QJsonObject>

#include <Cyqlops/Crypto/Hash.h>

#include <clog_qt>

#include "GeometryObject.h"

using namespace knowGIS;

QByteArray Point::md5() const
{
  return Cyqlops::Crypto::Hash::md5(x(), y(), z(), coordinateSystem().srid());
}

#define X_KEY QStringLiteral("x")
#define Y_KEY QStringLiteral("y")
#define Z_KEY QStringLiteral("z")
#define SRID_KEY QStringLiteral("srid")

QJsonValue Point::toJsonValue() const
{
  QJsonObject object;
  object[X_KEY] = x();
  object[Y_KEY] = y();
  object[Z_KEY] = z();
  object[SRID_KEY] = qint64(coordinateSystem().srid());
  return object;
}

cres_qresult<Point> Point::fromJsonValue(const QJsonValue& _json_value)
{
  if(_json_value.isObject())
  {
    QJsonObject object = _json_value.toObject();
    return cres_success(Point(object.value(X_KEY).toDouble(), object.value(Y_KEY).toDouble(),
                              object.value(Z_KEY).toDouble(),
                              Cartography::CoordinateSystem(object.value(SRID_KEY).toInt())));
  }
  else
  {
    return cres_failure("Expected object got {}", _json_value);
  }
}

QCborValue Point::toCborValue() const
{
  QCborMap object;
  object[X_KEY] = x();
  object[Y_KEY] = y();
  object[Z_KEY] = z();
  object[SRID_KEY] = coordinateSystem().srid();
  return object;
}

cres_qresult<Point> Point::fromCborValue(const QCborValue& _cbor_value)
{
  if(_cbor_value.isMap())
  {
    QCborMap cbor_map = _cbor_value.toMap();
    return cres_success(Point(cbor_map.value(X_KEY).toDouble(), cbor_map.value(Y_KEY).toDouble(),
                              cbor_map.value(Z_KEY).toDouble(),
                              Cartography::CoordinateSystem(cbor_map.value(SRID_KEY).toInteger())));
  }
  else
  {
    return cres_failure("Expected map got {}", _cbor_value);
  }
}

#include "Uris/askcore_gis.h"
#include <knowCore/MetaTypeImplementation.h>

KNOWCORE_DEFINE_CONVERT(Point, _from, QString, _to, Safe)
{
  *_to = clog_qt::qformat("({} {} {})@{}", _from->x(), _from->y(), _from->z(),
                          _from->coordinateSystem().srid());
  return cres_success();
}

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(Point)
cres_qresult<QByteArray> md5(const Point& _value) const override
{
  return cres_success(_value.md5());
}
cres_qresult<QJsonValue> toJsonValue(const Point& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(_value.toJsonValue());
}
cres_qresult<void> fromJsonValue(Point* _value, const QJsonValue& _json_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  cres_try(*_value, Point::fromJsonValue(_json_value));
  return cres_success();
}
cres_qresult<QCborValue> toCborValue(const Point& _value,
                                     const knowCore::SerialisationContexts&) const override
{
  return cres_success(_value.toCborValue());
}
cres_qresult<void> fromCborValue(Point* _value, const QCborValue& _cbor_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  cres_try(*_value, Point::fromCborValue(_cbor_value));
  return cres_success();
}
cres_qresult<QString> printable(const Point& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const Point& _value, const SerialisationContexts&) const override
{
  EuclidSystem::point pt(_value.x(), _value.y(), _value.z());
  return cres_success(
    clog_qt::qformat("SRID={};{}", _value.coordinateSystem().srid(), euclid::io::to_wkt(pt)));
}
cres_qresult<void> fromRdfLiteral(Point* _value, const QString& _serialised,
                                  const knowCore::DeserialisationContexts&) const override
{
  if(not _serialised.startsWith("SRID="))
  {
    return cres_failure("Missing SRID in '{}'", _serialised);
  }
  int pos = _serialised.indexOf(';');
  if(pos < 0)
  {
    return cres_failure("Missing ';' in '{}'", _serialised);
  }
  int srid = _serialised.mid(5, pos - 5).toInt();

  EuclidSystem::geometry geometry = euclid::io::from_wkt<EuclidSystem::geometry>(
    _serialised.right(_serialised.size() - pos - 1).toStdString());
  if(EuclidSystem::try_cast_to_point point = geometry)
  {
    *_value = Point(point->x(), point->y(), point->z(), Cartography::CoordinateSystem(srid));
    return cres_success();
  }
  else
  {
    return cres_failure("Invalid wkt '{}' expected a point", _serialised);
  }
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(Point)

KNOWCORE_DEFINE_METATYPE(knowGIS::Point, knowGIS::Uris::askcore_gis::point,
                         knowCore::MetaTypeTraits::OnlyEqualComparable
                           | knowCore::MetaTypeTraits::ToString)
