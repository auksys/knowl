#pragma once

#include <clog_qt>
#include <cres_qt>

#include "EuclidSystem.h"
#include <Cartography/Point.h>

namespace knowGIS
{
  class Point : public Cartography::Point
  {
  public:
    using Cartography::Point::Point;
    Point(const Cartography::Point& _rhs) : Cartography::Point(_rhs) {}
    QByteArray md5() const;
    QJsonValue toJsonValue() const;
    static cres_qresult<Point> fromJsonValue(const QJsonValue& _value);
    QCborValue toCborValue() const;
    static cres_qresult<Point> fromCborValue(const QCborValue& _value);
  };
} // namespace knowGIS

#include <Cartography/CoordinateSystem.h>
#include <knowCore/Formatter.h>
clog_format_declare_formatter(knowGIS::Point)
{
  if(p.isValid())
  {
    int srid = p.coordinateSystem().isValid() ? p.coordinateSystem().srid() : -1;
    return std::format_to(ctx.out(), "[{} {} {}]@{}", p.x(), p.y(), p.z(), srid);
  }
  else
  {
    return std::format_to(ctx.out(), "[invalid point]");
  }
}

#include <knowCore/MetaType.h>
KNOWCORE_DECLARE_FULL_METATYPE(knowGIS, Point);
