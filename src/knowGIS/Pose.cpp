#include "Pose.h"

#include <QCborMap>

#include <Cyqlops/Crypto/Hash.h>

#include <Cartography/CoordinateSystem.h>

#include <clog_qt>

#include <knowCore/MetaTypeImplementation.h>
#include <knowCore/Uris/askcore_datatype.h>

using namespace knowGIS;

KNOWCORE_DEFINE_CONVERT(knowGIS::Pose, _from, QString, _to, Safe)
{
  *_to = clog_qt::to_qstring(*_from);
  return cres_success();
}

Pose Pose::transform(const Cartography::CoordinateSystem& _coordinateSystem) const
{
  return Pose{m_position.transform(_coordinateSystem), m_orientation};
}

#define POSITION_KEY u8"position"_kCs
#define ORIENTATION_KEY u8"orientation"_kCs

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(Pose)
cres_qresult<QByteArray> md5(const Pose& _value) const override
{
  return cres_success(
    Cyqlops::Crypto::Hash::md5(_value.position().md5(), _value.orientation().md5()));
}
cres_qresult<QJsonValue> toJsonValue(const Pose& _value,
                                     const SerialisationContexts&) const override
{
  QJsonObject object;
  object[POSITION_KEY] = _value.position().toJsonValue();
  object[ORIENTATION_KEY] = _value.orientation().toJsonValue();
  return cres_success(object);
}
cres_qresult<void> fromJsonValue(Pose* _value, const QJsonValue& _json_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  if(_json_value.isObject())
  {
    QJsonObject object = _json_value.toObject();
    cres_try(Point position, Point::fromJsonValue(object.value(POSITION_KEY)));
    cres_try(Quaternion orientation, Quaternion::fromJsonValue(object.value(ORIENTATION_KEY)));

    *_value = Pose(position, orientation);
    return cres_success();
  }
  else
  {
    return expectedError("object", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const Pose& _value,
                                     const SerialisationContexts&) const override
{
  QCborMap object;
  object[POSITION_KEY] = _value.position().toCborValue();
  object[ORIENTATION_KEY] = _value.orientation().toCborValue();
  return cres_success(object);
}
cres_qresult<void> fromCborValue(Pose* _value, const QCborValue& _cbor_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  if(_cbor_value.isMap())
  {
    QCborMap cbor_map = _cbor_value.toMap();
    cres_try(Point position, Point::fromCborValue(cbor_map.value(POSITION_KEY)));
    cres_try(Quaternion orientation, Quaternion::fromCborValue(cbor_map.value(ORIENTATION_KEY)));

    *_value = Pose(position, orientation);
    return cres_success();
  }
  else
  {
    return expectedError("map", _cbor_value);
  }
}
cres_qresult<QString> printable(const Pose& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const Pose& _value,
                                   const SerialisationContexts& _contexts) const override
{
  return toJsonString(_value, _contexts);
}
cres_qresult<void> fromRdfLiteral(Pose* _value, const QString& _serialised,
                                  const knowCore::DeserialisationContexts& _contexts) const override
{
  return fromJsonString(_value, _serialised, _contexts);
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(Pose)

KNOWCORE_DEFINE_METATYPE(knowGIS::Pose, knowCore::Uris::askcore_datatype::pose,
                         knowCore::MetaTypeTraits::OnlyEqualComparable
                           | knowCore::MetaTypeTraits::ToString)
