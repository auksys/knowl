#include "Quaternion.h"

#include <Cyqlops/Crypto/Hash.h>

#include <QCborMap>
#include <QJsonObject>

#include <knowCore/Uri.h>

using namespace knowGIS;

QByteArray Quaternion::md5() const { return Cyqlops::Crypto::Hash::md5(x(), y(), z(), w()); }

#define X_KEY QStringLiteral("x")
#define Y_KEY QStringLiteral("y")
#define Z_KEY QStringLiteral("z")
#define W_KEY QStringLiteral("w")

QJsonValue Quaternion::toJsonValue() const
{
  QJsonObject object;
  object[X_KEY] = x();
  object[Y_KEY] = y();
  object[Z_KEY] = z();
  object[W_KEY] = w();
  return object;
}

cres_qresult<Quaternion> Quaternion::fromJsonValue(const QJsonValue& _json_value)
{
  if(_json_value.isObject())
  {
    QJsonObject object = _json_value.toObject();
    return cres_success(Quaternion(object.value(X_KEY).toDouble(), object.value(Y_KEY).toDouble(),
                                   object.value(Z_KEY).toDouble(), object.value(W_KEY).toDouble()));
  }
  else
  {
    return cres_failure("Expected object got {}", _json_value);
  }
}

QCborValue Quaternion::toCborValue() const
{
  QCborMap object;
  object[X_KEY] = x();
  object[Y_KEY] = y();
  object[Z_KEY] = z();
  object[W_KEY] = w();
  return object;
}

cres_qresult<Quaternion> Quaternion::fromCborValue(const QCborValue& _cbor_value)
{
  if(_cbor_value.isMap())
  {
    QCborMap cbor_map = _cbor_value.toMap();
    return cres_success(
      Quaternion(cbor_map.value(X_KEY).toDouble(), cbor_map.value(Y_KEY).toDouble(),
                 cbor_map.value(Z_KEY).toDouble(), cbor_map.value(W_KEY).toDouble()));
  }
  else
  {
    return cres_failure("Expected map got {}", _cbor_value);
  }
}

#include <knowCore/MetaTypeImplementation.h>
#include <knowCore/Uris/askcore_datatype.h>

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(Quaternion)
cres_qresult<QByteArray> md5(const Quaternion& _value) const override
{
  return cres_success(_value.md5());
}
cres_qresult<QJsonValue> toJsonValue(const Quaternion& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(_value.toJsonValue());
}
cres_qresult<void> fromJsonValue(Quaternion* _value, const QJsonValue& _json_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  cres_try(*_value, Quaternion::fromJsonValue(_json_value));
  return cres_success();
}
cres_qresult<QCborValue> toCborValue(const Quaternion& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(_value.toCborValue());
}
cres_qresult<void> fromCborValue(Quaternion* _value, const QCborValue& _cbor_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  cres_try(*_value, Quaternion::fromCborValue(_cbor_value));
  return cres_success();
}
cres_qresult<QString> printable(const Quaternion& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const Quaternion& _value,
                                   const SerialisationContexts& _contexts) const override
{
  return toJsonString(_value, _contexts);
}
cres_qresult<void> fromRdfLiteral(Quaternion* _value, const QString& _serialised,
                                  const knowCore::DeserialisationContexts& _contexts) const override
{
  return fromJsonString(_value, _serialised, _contexts);
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(Quaternion)

KNOWCORE_DEFINE_METATYPE(knowGIS::Quaternion, knowCore::Uris::askcore_datatype::quaternion,
                         knowCore::MetaTypeTraits::None)
