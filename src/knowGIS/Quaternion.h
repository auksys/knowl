#pragma once

#include <QtGlobal>

#include <clog_qt>
#include <cres_qt>

#include <knowCore/Forward.h>

namespace knowGIS
{
  class Quaternion
  {
  public:
    Quaternion() : Quaternion(identity()) {}
    Quaternion(qreal _x, qreal _y, qreal _z, qreal _w) : m_x(_x), m_y(_y), m_z(_z), m_w(_w) {}
    static Quaternion identity() { return Quaternion(0.0, 0.0, 0.0, 1.0); }
    ~Quaternion() {}
    qreal x() const { return m_x; }
    qreal y() const { return m_y; }
    qreal z() const { return m_z; }
    qreal w() const { return m_w; }
    bool operator==(const Quaternion& _rhs) const
    {
      return m_x == _rhs.m_x and m_y == _rhs.m_y and m_z == _rhs.m_z and m_w == _rhs.m_w;
    }
    QByteArray md5() const;
    QJsonValue toJsonValue() const;
    static cres_qresult<Quaternion> fromJsonValue(const QJsonValue& _value);
    QCborValue toCborValue() const;
    static cres_qresult<Quaternion> fromCborValue(const QCborValue& _value);
  private:
    qreal m_x, m_y, m_z, m_w;
  };
} // namespace knowGIS

#include <knowCore/Formatter.h>
clog_format_declare_formatter(knowGIS::Quaternion)
{
  return format_to(ctx.out(), "(x={} y={} z={} w={})", p.x(), p.y(), p.z(), p.w());
}

#include <knowCore/MetaType.h>
KNOWCORE_DECLARE_FULL_METATYPE(knowGIS, Quaternion);
