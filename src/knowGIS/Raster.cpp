#include "Raster.h"

#include <QBuffer>
#include <QDataStream>
#include <QPointF>
#include <QString>

#include <Cartography/Raster/AbstractImageWritter.h>

#include <clog_qt>
#include <knowCore/Image.h>

#include "Point.h"

using namespace knowGIS;

knowCore::Image Raster::toImage() const
{
  struct ImageWriter : public Cartography::Raster::AbstractImageWritter
  {
    quint8* prepare(quint64 _width, quint64 _height, quint64 _channels,
                    Raster::ChannelType _type) override
    {
      knowCore::Image::Type t;
      switch(_type)
      {
#define RASTER_TYPE_TO_KNOWCORE_TYPE(X, I)                                                         \
  case Raster::ChannelType::X:                                                                     \
    t = knowCore::Image::Type::X;                                                                  \
    break;
        KNOWCORE_FOREACH(RASTER_TYPE_TO_KNOWCORE_TYPE, UnsignedInteger8, Integer8,
                         UnsignedInteger16, Integer16, UnsignedInteger32, Integer32, Float32,
                         Float64)
      default:
        return nullptr;
      }
      img = knowCore::Image(_width, _height, _channels, t);
      return img.dataPtr();
    }
    std::size_t pixelSize() const override { return img.pixelSize(); }
    std::size_t channelPos(std::size_t _index) const override
    {
      return _index * img.scalarSize(img.type());
    }
    knowCore::Image img;
  };
  ImageWriter iw;
  if(toImage(&iw))
  {
    return iw.img;
  }
  else
  {
    return knowCore::Image();
  }
}

#include "Uris/askcore_gis.h"
#include <knowCore/MetaTypeImplementation.h>

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(Raster)
cres_qresult<QByteArray> md5(const Raster& _value) const override
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  hash.addData(_value.toWKBRaster());
  return cres_success(hash.result());
}
cres_qresult<QJsonValue> toJsonValue(const Raster& _value,
                                     const knowCore::SerialisationContexts&) const override
{
  return cres_success(QString::fromLatin1(_value.toWKBRaster().toBase64()));
}
cres_qresult<void> fromJsonValue(Raster* _value, const QJsonValue& _json_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  if(_json_value.isString())
  {
    *_value = Raster::fromWKBRaster(QByteArray::fromBase64(_json_value.toString().toLatin1()));
    return cres_success();
  }
  else
  {
    return expectedError("bytearray", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const Raster& _value,
                                     const knowCore::SerialisationContexts&) const override
{
  return cres_success(_value.toWKBRaster());
}
cres_qresult<void> fromCborValue(Raster* _value, const QCborValue& _cbor_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  if(_cbor_value.isByteArray())
  {
    *_value = Raster::fromWKBRaster(_cbor_value.toByteArray());
    return cres_success();
  }
  else
  {
    return expectedError("bytearray", _cbor_value);
  }
}
cres_qresult<QString> printable(const Raster& _value) const override
{
  Q_UNUSED(_value);
  return cres_success(QStringLiteral("Raster"));
}
cres_qresult<QString> toRdfLiteral(const Raster& _value,
                                   const knowCore::SerialisationContexts&) const override
{
  return cres_success(QString::fromLatin1(_value.toWKBRaster().toBase64()));
}
cres_qresult<void> fromRdfLiteral(Raster* _value, const QString& _serialised,
                                  const knowCore::DeserialisationContexts&) const override
{
  *_value = Raster::fromWKBRaster(QByteArray::fromBase64(_serialised.toLatin1()));
  return cres_success();
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(Raster)

KNOWCORE_DEFINE_METATYPE(Raster, knowGIS::Uris::askcore_gis::raster, knowCore::MetaTypeTraits::None)
