#pragma once

#include <QSharedDataPointer>

class QPointF;
class QString;

#include <knowCore/Forward.h>

#include <Cartography/Raster/Raster.h>

namespace knowGIS
{
  /**
   * Wrap a raster from GDAL and provides convenient functions to access the data.
   *
   * The actual data is shared between multiple instance of this class and only deleted once the
   * last instance goes out of scope.
   */
  class Raster : public Cartography::Raster::Raster
  {
  public:
    using Cartography::Raster::Raster::Raster;
    Raster(const Cartography::Raster::Raster& _rhs) : Cartography::Raster::Raster(_rhs) {}
    using Cartography::Raster::Raster::toImage;
    knowCore::Image toImage() const;
  };
} // namespace knowGIS

#include <knowCore/MetaType.h>
KNOWCORE_DECLARE_FULL_METATYPE(knowGIS, Raster);
