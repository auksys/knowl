#include <QtTest/QtTest>

#include "GeometryObject.h"

namespace knowGIS
{
  template<typename _T_>
    requires(std::has_formatter<_T_, std::format_context>::value)
  char* toString(const _T_& p)
  {
    return QTest::toString(clog_qt::to_qstring(p));
  }
} // namespace knowGIS
