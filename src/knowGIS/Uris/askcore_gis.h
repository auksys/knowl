#include <knowCore/Uris/Uris.h>

#define KNOWGIS_ASKCORE_GIS_URIS(F, ...)                                                           \
  F(__VA_ARGS__, building)                                                                         \
  F(__VA_ARGS__, geopoint)                                                                         \
  F(__VA_ARGS__, point)                                                                            \
  F(__VA_ARGS__, raster)

KNOWCORE_ONTOLOGY_URIS(knowGIS::Uris, KNOWGIS_ASKCORE_GIS_URIS, askcore_gis, "http://askco.re/gis#")
