#include <knowCore/Uris/Uris.h>

#define KNOWGIS_ASKCORE_SGF_URIS(F, ...)                                                           \
  F(__VA_ARGS__, collection)                                                                       \
  F(__VA_ARGS__, feature)                                                                          \
  F(__VA_ARGS__, has_property)

KNOWCORE_ONTOLOGY_URIS(knowGIS::Uris, KNOWGIS_ASKCORE_SGF_URIS, askcore_sgf, "http://askco.re/sgf#")
