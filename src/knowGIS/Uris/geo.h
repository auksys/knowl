#include <knowCore/Uris/Uris.h>

#define KNOWGIS_GEO_URIS(F, ...)                                                                   \
  F(__VA_ARGS__, wktLiteral)                                                                       \
  F(__VA_ARGS__, gmlLiteral)                                                                       \
  F(__VA_ARGS__, hasGeometry)                                                                      \
  F(__VA_ARGS__, Geometry)

KNOWCORE_ONTOLOGY_URIS(knowGIS::Uris, KNOWGIS_GEO_URIS, geo,
                       "http://www.opengis.net/ont/geosparql#")
