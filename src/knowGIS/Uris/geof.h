#include <knowCore/Uris/Uris.h>

#define KNOWGIS_GEOF_URIS(F, ...)                                                                  \
  F(__VA_ARGS__, difference)                                                                       \
  F(__VA_ARGS__, getSRID)                                                                          \
  F(__VA_ARGS__, sfIntersects)                                                                     \
  F(__VA_ARGS__, sfDisjoint)                                                                       \
  F(__VA_ARGS__, sfOverlaps)                                                                       \
  F(__VA_ARGS__, sfTouches)                                                                        \
  F(__VA_ARGS__, sfWithin)                                                                         \
  F(__VA_ARGS__, union_, "union")

KNOWCORE_ONTOLOGY_URIS(knowGIS::Uris, KNOWGIS_GEOF_URIS, geof,
                       "http://www.opengis.net/def/function/geosparql/")
