#include "TestCoordinateSystem.h"

#include "../GeoPoint.h"
#include "../Point.h"
#include <Cartography/CoordinateSystem.h>

// Use https://www.latlong.net/lat-long-utm.html to convert lat/long to UTM
// Use qDebug() << qSetRealNumberPrecision(12) for higher accuracy

void TestCoordinateSystem::testConversion()
{
  knowGIS::GeoPoint gp(Cartography::Longitude(15.1017), Cartography::Latitude(58.4949));
  Cartography::CoordinateSystem utm_33N = Cartography::CoordinateSystem::utm(gp);
  knowGIS::Point pt = gp.transform(utm_33N);
  QCOMPARE(pt.coordinateSystem(), utm_33N);
  QCOMPARE(pt.x(), 505928.239751);
  QCOMPARE(pt.y(), 6483815.54676);
}

void TestCoordinateSystem::testUtm()
{
  knowGIS::GeoPoint gp(Cartography::Longitude(15.1017), Cartography::Latitude(58.4949));
  Cartography::CoordinateSystem utm_33N = Cartography::CoordinateSystem::utm(gp);
  QCOMPARE(utm_33N.srid(), 32633u); // https://spatialreference.org/ref/epsg/wgs-84-utm-zone-33n/
}

void TestCoordinateSystem::testWgs84()
{
  Cartography::CoordinateSystem cs_wgs84 = Cartography::CoordinateSystem::wgs84;
  QCOMPARE(cs_wgs84.srid(), 4326u);
  QVERIFY(cs_wgs84.isValid());
  QVERIFY(cs_wgs84.isWgs84());
}

QTEST_MAIN(TestCoordinateSystem)
