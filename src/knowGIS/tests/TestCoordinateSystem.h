#include <QtTest/QtTest>

class TestCoordinateSystem : public QObject
{
  Q_OBJECT
private slots:
  void testWgs84();
  void testUtm();
  void testConversion();
};
