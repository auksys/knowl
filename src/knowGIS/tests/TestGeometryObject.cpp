#include "TestGeometryObject.h"

#include <knowCore/Test.h>
#include <knowCore/Uri.h>
#include <knowRDF/Literal.h>

#include "../GeometryObject.h"
#include "../Uris/geo.h"

#include <QJsonDocument>

#define COMPARE_S(__KEY__, __VALUE__)                                                              \
  QCOMPARE(geobject[__KEY__].toString(), QStringLiteral(__VALUE__))

#define COMPARE_PT(__INDEX__, __X__, __Y__)                                                        \
  QCOMPARE(coordinates[__INDEX__].toList()[0].toInt(), __X__);                                     \
  QCOMPARE(coordinates[__INDEX__].toList()[1].toInt(), __Y__)

void TestGeometryObject::testMetaInfo()
{
  QCOMPARE(knowCore::MetaTypes::type(knowGIS::Uris::geo::Geometry),
           knowGIS::GeometryObjectMetaTypeInformation::id());
  QCOMPARE(knowCore::MetaTypes::uri(knowGIS::GeometryObjectMetaTypeInformation::id()),
           knowGIS::Uris::geo::Geometry);
}

void TestGeometryObject::testJson()
{
  using namespace knowGIS;

  QString wkt = "POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10))";

  GeometryObject obj = CRES_QVERIFY(GeometryObject::fromWKT(wkt));
  QVERIFY(obj.isValid());
  QString json = obj.toJson();

  QJsonDocument document = QJsonDocument::fromJson(json.toLatin1());
  QVariantMap geobject = document.toVariant().toMap();
  COMPARE_S("type", "Polygon");
  QVariantList coordinates = geobject["coordinates"].toList().first().toList();
  QCOMPARE(coordinates.size(), 5);

  COMPARE_PT(0, 30, 10);
  COMPARE_PT(1, 40, 40);
  COMPARE_PT(2, 20, 40);
  COMPARE_PT(3, 10, 20);
  COMPARE_PT(4, 30, 10);

  GeometryObject obj2 = CRES_QVERIFY(GeometryObject::fromJson(json));
  QCOMPARE(obj2.toWKT(2), wkt);
}

void TestGeometryObject::testRdfLiteral()
{
  knowRDF::Literal point_literal = CRES_QVERIFY(knowRDF::Literal::fromRdfLiteral(
    knowGIS::Uris::geo::wktLiteral, QStringLiteral("POINT(10 10)")));
  CRES_QCOMPARE(point_literal.value<QString>(), QStringLiteral("SRID=4326;POINT (10 10)"));
  CRES_QCOMPARE(point_literal.toRdfLiteral(), QStringLiteral("SRID=4326;POINT (10 10)"));
  knowRDF::Literal polygon_literal = CRES_QVERIFY(knowRDF::Literal::fromRdfLiteral(
    knowGIS::Uris::geo::wktLiteral,
    QStringLiteral(
      "POLYGON ((16.682642698287967 57.759813973085713, 16.683061122894294 57.759999986629374, "
      "16.682610511779792 57.760715414412353, 16.681650280952457 57.760489340763655, "
      "16.682025790214542 57.759942751794839, 16.682642698287967 57.759813973085713))")));
  CRES_QCOMPARE(
    polygon_literal.value<QString>(),
    QStringLiteral("SRID=4326;POLYGON ((16.682642698287967 57.75981397308571, 16.683061122894294 "
                   "57.759999986629374, 16.682610511779792 57.76071541441235, 16.681650280952457 "
                   "57.760489340763655, 16.682025790214542 57.75994275179484, 16.682642698287967 "
                   "57.75981397308571))"));
  CRES_QCOMPARE(
    polygon_literal.toRdfLiteral(),
    QStringLiteral("SRID=4326;POLYGON ((16.682642698287967 57.75981397308571, 16.683061122894294 "
                   "57.759999986629374, 16.682610511779792 57.76071541441235, 16.681650280952457 "
                   "57.760489340763655, 16.682025790214542 57.75994275179484, 16.682642698287967 "
                   "57.75981397308571))"));
}

void TestGeometryObject::testWktLiteral()
{
  knowRDF::Literal lit = CRES_QVERIFY(
    knowRDF::Literal::fromValue(knowGIS::Uris::geo::wktLiteral,
                                CRES_QVERIFY(knowGIS::GeometryObject::fromWKT("POINT (30 10)"))));
  QCOMPARE(lit.datatype(), knowGIS::Uris::geo::wktLiteral);
  QVERIFY(lit.canConvert<knowGIS::GeometryObject>());
  CRES_QCOMPARE(lit.value<knowGIS::GeometryObject>(),
                CRES_QVERIFY(knowGIS::GeometryObject::fromWKT("POINT (30 10)")));

  lit = CRES_QVERIFY(
    knowRDF::Literal::fromValue(knowGIS::Uris::geo::wktLiteral, "POINT (30 10)"_kCs));
  QCOMPARE(lit.datatype(), knowGIS::Uris::geo::wktLiteral);
  QVERIFY(lit.canConvert<knowGIS::GeometryObject>());
  CRES_QCOMPARE(lit.value<knowGIS::GeometryObject>(),
                CRES_QVERIFY(knowGIS::GeometryObject::fromWKT("POINT (30 10)")));
}

QTEST_MAIN(TestGeometryObject)
