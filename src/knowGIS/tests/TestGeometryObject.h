#include <QtTest/QtTest>

class TestGeometryObject : public QObject
{
  Q_OBJECT
private slots:
  void testMetaInfo();
  void testJson();
  void testRdfLiteral();
  void testWktLiteral();
};
