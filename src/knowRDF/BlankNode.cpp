#include "BlankNode.h"

#include <QString>
#include <QUuid>

using namespace knowRDF;

struct BlankNode::Private : public QSharedData
{
  QString label;
  QUuid uuid;
};

BlankNode::BlankNode() : d(new Private) { d->uuid = QUuid::createUuid(); }

BlankNode::BlankNode(const QString& _label) : d(new Private)
{
  d->label = _label;
  d->uuid = QUuid::createUuid();
}

BlankNode::BlankNode(const QUuid& _uuid, const QString& _label) : d(new Private)
{
  d->uuid = _uuid;
  d->label = _label;
}

BlankNode::BlankNode(const BlankNode& _rhs) : d(_rhs.d) {}

BlankNode& BlankNode::operator=(const BlankNode& _rhs)
{
  d = _rhs.d;
  return *this;
}

BlankNode::~BlankNode() {}

QString BlankNode::label() const { return d->label; }

QUuid BlankNode::uuid() const { return d->uuid; }

bool BlankNode::operator==(const BlankNode& _rhs) const
{
  return d == _rhs.d /* faster and more common */ or d->uuid == _rhs.d->uuid;
}

bool BlankNode::operator<(const BlankNode& _rhs) const { return d->uuid < _rhs.d->uuid; }

const void* BlankNode::displayId() const { return d.data(); }

#include <knowCore/MetaTypeImplementation.h>
#include <knowCore/Uris/askcore_datatype.h>

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(knowRDF::BlankNode)
cres_qresult<QByteArray> md5(const knowRDF::BlankNode& _value) const override
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  hash.addData(_value.uuid().toRfc4122());
  return cres_success(hash.result());
}
cres_qresult<QJsonValue> toJsonValue(const knowRDF::BlankNode& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(QJsonValue(_value.uuid().toString()));
}
cres_qresult<void> fromJsonValue(knowRDF::BlankNode* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts&) const override
{
  if(_json_value.isString())
  {
    *_value = QUuid::fromString(_json_value.toString());
    return cres_success();
  }
  else
  {
    return expectedError("string", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const knowRDF::BlankNode& _value,
                                     const SerialisationContexts&) const override
{
  return cres_success(QCborValue(_value.uuid().toRfc4122()));
}
cres_qresult<void> fromCborValue(knowRDF::BlankNode* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts&) const override
{
  if(_cbor_value.isByteArray())
  {
    *_value = QUuid::fromRfc4122(_cbor_value.toByteArray());
    return cres_success();
  }
  else
  {
    return expectedError("bytearray", _cbor_value);
  }
}
cres_qresult<QString> printable(const knowRDF::BlankNode& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const knowRDF::BlankNode& _value,
                                   const SerialisationContexts&) const override
{
  return cres_success(_value.uuid().toString());
}
cres_qresult<void> fromRdfLiteral(knowRDF::BlankNode* _value, const QString& _serialised,
                                  const DeserialisationContexts&) const override
{
  _value->d->uuid = QUuid::fromString(_serialised);
  return cres_success();
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(knowRDF::BlankNode)

KNOWCORE_DEFINE_METATYPE(knowRDF::BlankNode, knowCore::Uris::askcore_datatype::blanknode,
                         knowCore::MetaTypeTraits::Comparable)
