#ifndef _KNOWCORE_RDF_BLANKNODE_H_
#define _KNOWCORE_RDF_BLANKNODE_H_

#include <QHash>
#include <QSharedDataPointer>

#include <knowCore/Formatter.h>
#include <knowCore/Forward.h>

class QUuid;

namespace knowRDF
{
  /**
   * @ingroup knowRDF
   *
   * This class represents a BlankNode in RDF (i.e. an unspecific uri). Each blank node is uniquely
   * identified by an UUID.
   *
   * The blank node maybe associated with a label. However the label is not considered to check if
   * two blank nodes are equals, only the UUID is used. The label is mainly used for display
   * purposes and as a hint when serializing documents.
   */
  class BlankNode
  {
    friend uint qHash(const BlankNode& key, uint seed);
    friend struct std::formatter<BlankNode>;
    friend class knowCore::MetaTypeDefinition<BlankNode>;
  public:
    /**
     * Create a blank node with a random UUID.
     */
    BlankNode();
    /**
     * Create a blank node with a given label, labels are often used to identify a blank node in an
     * RDF document.
     */
    BlankNode(const QString& _label);
    /**
     * Create a blank node with a given UUID and label.
     */
    BlankNode(const QUuid& _uuid, const QString& _label = QString());
    BlankNode(const BlankNode& _rhs);
    BlankNode& operator=(const BlankNode& _rhs);
    ~BlankNode();
    /**
     * @return the label that was used when creating this blank node.
     */
    QString label() const;
    bool operator==(const BlankNode& _rhs) const;
    bool operator!=(const BlankNode& _rhs) const { return not(*this == _rhs); }
    bool operator<(const BlankNode& _rhs) const;
    QUuid uuid() const;
  private:
    const void* displayId() const;
    struct Private;
    QSharedDataPointer<Private> d;
  };
  inline uint qHash(const BlankNode& key, uint seed = 0)
  {
    return ::qHash(key.d.constData(), seed);
  }

  uint qHash(const BlankNode& key, uint seed);
} // namespace knowRDF

clog_format_declare_formatter(knowRDF::BlankNode)
{
  QString ret;
  if(p.label().isEmpty())
  {
    ret = "EmptyBlankNode";
  }
  else
  {
    ret = "?" + p.label();
  }
  return format_to(ctx.out(), "{}({},{})", ret, p.displayId(), p.uuid());
}

#include <knowCore/MetaType.h>

KNOWCORE_DECLARE_FULL_METATYPE(knowRDF, BlankNode);

#endif
