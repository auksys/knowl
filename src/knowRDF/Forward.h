#include <knowCore/Forward.h>

namespace knowRDF
{
  class BlankNode;
  class Literal;
  class Graph;
  class Node;
  class Subject;
  class Triple;
  class TripleStreamListener;
  class Object;
} // namespace knowRDF
