#include "Graph.h"

#include <knowCore/Cast.h>
#include <knowCore/Uri.h>

#include "BlankNode.h"
#include "Node.h"
#include "Object.h"
#include "Subject.h"
#include "Triple.h"

using namespace knowRDF;

struct Graph::Private
{
  QHash<BlankNode, Node*> nodes_blank;
  QHash<knowCore::Uri, Node*> nodes_uris;
  QList<Node*> nodes;
};

Graph::Graph() : d(new Private) {}

Graph::~Graph()
{
  clear();
  delete d;
}

void Graph::clear()
{
  for(Node* n : d->nodes)
  {
    delete n;
  }
  d->nodes_blank.clear();
  d->nodes_uris.clear();
  d->nodes.clear();
}

Node* Graph::createOrGetNode(const Object& _object)
{
  switch(_object.type())
  {
  case Object::Type::Uri:
  {
    return createOrGetNode(_object.uri());
  }
  case Object::Type::BlankNode:
  {
    return createOrGetNode(_object.blankNode());
  }
  default:
  {
    Node* n = new Node(_object);
    for(Node* no : d->nodes) // TODO index this, if possible
    {
      if(no->equal(n))
      {
        clog_assert(no->literal() == _object.literal());
        delete n;
        return no;
      }
    }
    d->nodes.append(n);
    return n;
  }
  }
}

Node* Graph::createOrGetNode(const Subject& _subject)
{
  switch(_subject.type())
  {
  case Subject::Type::Uri:
  {
    return createOrGetNode(_subject.uri());
  }
  case Subject::Type::BlankNode:
  {
    return createOrGetNode(_subject.blankNode());
  }
  default:
  {
    Node* n = new Node(_subject);
    d->nodes.append(n);
    return n;
  }
  }
}

Node* Graph::createOrGetNode(const knowCore::Uri& _uri)
{
  Node* n = d->nodes_uris.value(_uri, nullptr);
  if(n)
    return n;
  n = new Node(_uri);
  d->nodes.append(n);
  d->nodes_uris[_uri] = n;
  return n;
}

Node* Graph::createOrGetNode(const BlankNode& _blankNode)
{
  Node* n = d->nodes_blank.value(_blankNode, nullptr);
  if(n)
    return n;
  n = new Node(_blankNode);
  d->nodes.append(n);
  d->nodes_blank[_blankNode] = n;
  return n;
}

const Node* Graph::getNode(const knowCore::Uri& _uri) const
{
  return d->nodes_uris.value(_uri, nullptr);
}

const Node* Graph::getNode(const BlankNode& _blankNode) const
{
  return d->nodes_blank.value(_blankNode, nullptr);
}

const Node* Graph::getNode(const Subject& _subject) const
{
  switch(_subject.type())
  {
  case Subject::Type::Uri:
  {
    return getNode(_subject.uri());
  }
  case Subject::Type::BlankNode:
  {
    return getNode(_subject.blankNode());
  }
  default:
  {
    return nullptr;
  }
  }
}

QList<QPair<const Node*, const Node*>> Graph::getNodes(const knowCore::Uri& _predicate) const
{
  QList<QPair<const Node*, const Node*>> r;
  for(const Node* n : d->nodes)
  {
    QList<const Node*> cs = n->children(_predicate);
    std::transform(
      cs.begin(), cs.end(), std::back_inserter(r), [n](const Node* _c)
      { return std::make_pair<const Node*, const Node*>((const Node*)n, (const Node*)_c); });
    // std::transform(cs.begin(), cs.end(), std::back_inserter(r), std::bind(std::make_pair<const
    // Node*, const Node*>, n, std::placeholders::_1));
  }
  return r;
}

QList<const Node*> Graph::getSubjects(const knowCore::Uri& _predicate, const Object& _object) const
{
  Node nobject(_object);
  QList<const Node*> r;
  for(Node* n : d->nodes)
  {
    for(const Node* c : n->children(_predicate))
    {
      if(c->equal(&nobject))
      {
        r.append(n);
      }
    }
  }
  return r;
}

void Graph::removeTriple(const Triple& _triple)
{
  Node* subject = getNCNode(_triple.subject());
  if(subject)
  {
    subject->disconnect(_triple.predicate(), _triple.object());
  }
}

void Graph::triple(const Triple& _triple)
{
  createOrGetNode(_triple.subject())
    ->connect(_triple.predicate(), createOrGetNode(_triple.object()));
}

void Graph::addTriples(const QList<Triple>& _triples)
{
  for(const Triple& t : _triples)
  {
    triple(t);
  }
}

QList<const Node*> Graph::nodes() const { return knowCore::listCast<const Node*>(d->nodes); }
/*
QList< Node* > Graph::nodes()
{
  return d->nodes;
}*/
