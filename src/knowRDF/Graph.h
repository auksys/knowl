#include "TripleStreamListener.h"

#include <QList>

#include <knowRDF/Forward.h>

namespace knowRDF
{
  class Graph : public TripleStreamListener
  {
  public:
    Graph();
    virtual ~Graph();
  public:
    void clear();
    const Node* getNode(const knowCore::Uri& _uri) const;
    const Node* getNode(const BlankNode& _blankNode) const;
    const Node* getNode(const Subject& _subject) const;
    /**
     * Get the nodes connected by the given predicate
     */
    QList<QPair<const Node*, const Node*>> getNodes(const knowCore::Uri& _predicate) const;
    QList<const Node*> getSubjects(const knowCore::Uri& _predicate, const Object& _object) const;
    QList<const Node*> nodes() const;
    void removeTriple(const Triple& _triple);
  public:
    virtual void triple(const Triple& _triple);
    void addTriples(const QList<Triple>& _triples);
  private:
    Node* createOrGetNode(const knowCore::Uri& _uri);
    Node* createOrGetNode(const BlankNode& _blankNode);
    Node* createOrGetNode(const Subject& _subject);
    Node* createOrGetNode(const Object& _object);
    template<typename _T_>
    Node* getNCNode(const _T_& _arg)
    {
      return const_cast<Node*>(getNode(_arg));
    }
    struct Private;
    Private* const d;
  };
} // namespace knowRDF
