#include "Literal.h"

#include <QCborMap>
#include <QCryptographicHash>
#include <QJsonObject>

#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Uris/xsd.h>

using namespace knowRDF;

struct Literal::Private : public QSharedData
{
  QString lang;
  std::optional<knowCore::Uri> datatype;
};

#define DATATYPE_KEY u8"datatype"_kCs
#define VALUE_KEY u8"value"_kCs
#define LANG_KEY u8"lang"_kCs

Literal::Literal() : d(new Private) {}

Literal::Literal(const knowCore::Uri& _datatype, const knowCore::Value& _rhs, const QString& _lang)
    : knowCore::Value(_rhs), d(new Private)
{
  clog_assert(_rhs.datatype() == knowCore::Uris::xsd::string
              or _rhs.datatype() == knowCore::Uris::askcore_datatype::binarydata);
  d->datatype = _datatype;
  d->lang = _lang;
}

Literal::Literal(const knowCore::Value& _rhs, const QString& _lang)
    : knowCore::Value(_rhs), d(new Private)
{
  clog_assert(_rhs.datatype()
              != knowCore::Uris::askcore_datatype::literal); // Storing a literal inside a literal
                                                             // does not make sense
  d->lang = _lang;
}

Literal::Literal(const Literal& _rhs) : Value(_rhs) { d = _rhs.d; }

Literal& Literal::operator=(const Literal& _rhs)
{
  Value::operator=(_rhs);
  d = _rhs.d;
  return *this;
}

Literal::~Literal() {}

knowCore::Uri Literal::datatype() const
{
  if(d->datatype)
  {
    return *d->datatype;
  }
  else
  {
    return Value::datatype();
  }
}

Literal Literal::fromValue(const knowCore::Value& _rhs)
{
  if(knowCore::ValueIs<Literal> vc = _rhs)
  {
    return vc;
  }
  else
  {
    return _rhs;
  }
}

cres_qresult<Literal> Literal::fromValue(const knowCore::Value& _value, const Lang& _lang)
{
  if(_value.template is<Literal>())
  {
    cres_try(Literal lit, _value.template value<Literal>());
    return fromValue(lit, _lang);
  }
  else
  {
    return cres_success(Literal(_value, _lang));
  }
}

cres_qresult<Literal> Literal::fromValue(const Literal& _value, const Lang& _lang)
{
  if(_lang.get_value().isEmpty() or _lang == _value.lang())
  {
    return cres_success(_value);
  }
  else
  {
    return cres_failure("Lang mismatched: {} != {}", _value.lang(), _lang.get_value());
  }
}

cres_qresult<Literal> Literal::fromVariant(const knowCore::Uri& _datatype, const QVariant& _value,
                                           const QString& _lang,
                                           knowCore::TypeCheckingMode _conversion)
{
  const auto [success, value, message] = Value::fromValue(_datatype, _value, _conversion);
  if(success)
  {
    return cres_success(Literal(value.value(), _lang));
  }
  else if(_value.userType() == qMetaTypeId<QByteArray>())
  { // Give precedence to QByteArray over QString, only if the QVariant is storing a QByteArray
    return cres_success(Literal(_datatype, Value::fromValue(_value.toByteArray()), _lang));
  }
  else if(_value.userType() == qMetaTypeId<QString>())
  {
    return cres_success(Literal(_datatype, Value::fromValue(_value.toString()), _lang));
  }
  else
  {
    return cres_forward_failure(message);
  }
}

cres_qresult<Literal> Literal::fromVariant(const QVariant& _value, const QString& _lang)
{
  cres_try(knowCore::Value v, knowCore::Value::fromVariant(_value));
  if(knowCore::ValueIs<Literal> lit = v)
  {
    clog_assert(_lang.isEmpty());
    return cres_success(lit);
  }
  else
  {
    return cres_success(Literal(v, Lang(_lang)));
  }
}

cres_qresult<Literal> Literal::fromRdfLiteral(const knowCore::Uri& _datatype, const QString& _value,
                                              const QString& _lang)
{
  if(knowCore::MetaTypes::isDefined(_datatype))
  {
    cres_try(knowCore::Value value, Value::fromRdfLiteral(_datatype, _value));
    return cres_success(Literal(value, _lang));
  }
  else
  {
    return cres_success(Literal(_datatype, Value::fromValue(_value), _lang));
  }
}

QString Literal::lang() const { return d->lang; }

bool Literal::operator<(const Literal& _rhs) const
{
  return Value::operator<(_rhs) and d->lang < _rhs.lang();
}

bool Literal::operator==(const Literal& _rhs) const
{
  return datatype() == _rhs.datatype() and Value::operator==(_rhs) and d->lang == _rhs.lang();
}

cres_qresult<QByteArray> Literal::md5() const
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  cres_try(QByteArray value_md5, Value::md5());
  hash.addData(value_md5);
  hash.addData(lang().toUtf8());
  return cres_success(hash.result());
}

cres_qresult<QJsonValue>
  Literal::toJsonValue(const knowCore::SerialisationContexts& _contexts) const
{
  QJsonObject obj;
  obj[DATATYPE_KEY] = (QString)datatype();
  cres_try(QJsonValue value_json, Value::toJsonValue(_contexts));
  obj[VALUE_KEY] = value_json;
  obj[LANG_KEY] = lang();
  return cres_success(obj);
}

cres_qresult<Literal> Literal::fromJsonValue(const QJsonValue& _value,
                                             const knowCore::DeserialisationContexts& _contexts)
{
  if(_value.isObject())
  {
    QJsonObject obj = _value.toObject();
    knowCore::Uri datatype = obj.value(DATATYPE_KEY).toString();
    QString lang = obj.value(LANG_KEY).toString();
    cres_try(knowCore::Value value,
             knowCore::Value::fromJsonValue(datatype, obj.value(VALUE_KEY), _contexts));
    return cres_success(Literal(value, lang));
  }
  else
  {
    return cres_failure("Expected object got '{}'", _value);
  }
}

cres_qresult<QCborValue>
  Literal::toCborValue(const knowCore::SerialisationContexts& _contexts) const
{
  QCborMap obj;
  obj[DATATYPE_KEY] = (QString)datatype();
  cres_try(QCborValue value_cbor, Value::toCborValue(_contexts));
  obj[VALUE_KEY] = value_cbor;
  obj[LANG_KEY] = lang();
  return cres_success(obj);
}

cres_qresult<Literal> Literal::fromCborValue(const QCborValue& _cbor_value,
                                             const knowCore::DeserialisationContexts& _contexts)
{
  if(_cbor_value.isMap())
  {
    QCborMap obj = _cbor_value.toMap();
    QString datatype = obj.value(DATATYPE_KEY).toString();
    QString lang = obj.value(LANG_KEY).toString();
    cres_try(knowCore::Value value,
             knowCore::Value::fromCborValue(datatype, obj.value(VALUE_KEY), _contexts));
    return cres_success(Literal(value, lang));
  }
  else
  {
    return cres_failure("Expected map got '{}'");
  }
}

namespace knowRDF
{
  struct HashCombine
  {
    uint seed;
    template<typename T>
    void operator()(const T& t) Q_DECL_NOEXCEPT_EXPR(noexcept(::qHash(t)))
    // combiner taken from N3876 / boost::hash_combine
    {
      seed = seed ^ (::qHash(t) + 0x9e3779b9 + (seed << 6) + (seed >> 2));
    }
  };
} // namespace knowRDF

uint knowRDF::qHash(const Literal& key, uint seed)
{
  HashCombine combine;
  combine.seed = seed;
  combine(qHash((const knowCore::Value&)key));
  combine(key.lang());
  return combine.seed;
}

#include <knowCore/MetaTypeImplementation.h>

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(Literal)
cres_qresult<QByteArray> md5(const Literal& _value) const override { return _value.md5(); }
cres_qresult<QJsonValue> toJsonValue(const Literal& _value,
                                     const SerialisationContexts& _contexts) const override
{
  return _value.toJsonValue(_contexts);
}
cres_qresult<void> fromJsonValue(Literal* _value, const QJsonValue& _json_value,
                                 const knowCore::DeserialisationContexts& _contexts) const override
{
  return cres_try_assign(_value, Literal::fromJsonValue(_json_value, _contexts));
}
cres_qresult<QCborValue> toCborValue(const Literal& _value,
                                     const SerialisationContexts& _contexts) const override
{
  return _value.toCborValue(_contexts);
}
cres_qresult<void> fromCborValue(Literal* _value, const QCborValue& _cbor_value,
                                 const knowCore::DeserialisationContexts& _contexts) const override
{
  return cres_try_assign(_value, Literal::fromCborValue(_cbor_value, _contexts));
}
cres_qresult<QString> printable(const Literal& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const Literal& _value,
                                   const SerialisationContexts& _contexts) const override
{
  return toJsonString(_value, _contexts);
}
cres_qresult<void> fromRdfLiteral(Literal* _value, const QString& _serialised,
                                  const knowCore::DeserialisationContexts& _contexts) const override
{
  return fromJsonString(_value, _serialised, _contexts);
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(Literal)

KNOWCORE_DEFINE_METATYPE(knowRDF::Literal, knowCore::Uris::askcore_datatype::literal,
                         knowCore::MetaTypeTraits::Comparable
                           | knowCore::MetaTypeTraits::DebugStreamOperator)

KNOWCORE_DEFINE_CONVERT(knowRDF::Literal, _from, knowCore::Value, _to, Safe)
{
  *_to = *_from;
  return cres_success();
}

KNOWCORE_REGISTER_CONVERSION_FROM(knowRDF::Literal, knowCore::Value)
KNOWCORE_REGISTER_CONVERSION_TO(knowRDF::Literal, knowCore::Value)
