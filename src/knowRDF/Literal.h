#pragma once

#include <cext_nt>
#include <knowCore/Value.h>

namespace knowRDF
{
  using Lang = cext_named_type<QString, struct LangTag>;
  /**
   * \ingroup knowRDF
   * Represents a RDF Literal, as \ref knowCore::Value with a lang tag.
   */
  class Literal : public knowCore::Value
  {
    Literal(const knowCore::Uri& _datatype, const knowCore::Value& _rhs,
            const QString& _lang = QString());
  public:
    /**
     * Construct an empty Literal
     */
    Literal();
    Literal(const knowCore::Value& _rhs, const QString& _lang = QString());
    Literal(const Literal& _rhs);
    Literal& operator=(const Literal& _rhs);
    ~Literal();
    template<typename _T_>
    static Literal fromValue(const _T_& _value, const Lang& _lang = Lang(QString()));
    static cres_qresult<Literal> fromValue(const knowCore::Value& _value, const Lang& _lang);
    static cres_qresult<Literal> fromValue(const Literal& _value,
                                           const Lang& _lang = Lang(QString()));
    static cres_qresult<Literal> fromValue(const QVariant& _value,
                                           const Lang& _lang = Lang(QString()))
    {
      return fromVariant(_value, _lang);
    }
    template<typename _T_>
    static cres_qresult<Literal>
      fromValue(const knowCore::Uri& _datatype, const _T_& _value, const QString& _lang = QString(),
                knowCore::TypeCheckingMode _conversion = knowCore::TypeCheckingMode::Safe);
    /**
     * @return the URI corresponding to the datatype, this might differ from \ref Value::datatype in
     * case Value::datatype is set to \ref knowCore::Uris::xsd::string and the literal hold a value
     * as a \ref QString for an unregistered type.
     */
    knowCore::Uri datatype() const;
    /**
     * Construct a literal from a value. If \ref _rhs contains a \ref Literal, it returns it,
     * otherwise, it call the \ref Literal constructor with the value and an empty lang.
     */
    static Literal fromValue(const knowCore::Value& _rhs);
    /**
     * Construct a literal from its full definition
     * @param _datatype the type of the literal
     * @param _value a variant with the value
     * @param _lang the language tag (ie 'en')
     * @param _normalise whether the QVariant should be casted to the default type for @p _datatype
     */
    static cres_qresult<Literal> fromVariant(const knowCore::Uri& _datatype, const QVariant& _value,
                                             const QString& _lang = QString(),
                                             knowCore::TypeCheckingMode _conversion
                                             = knowCore::TypeCheckingMode::Safe);
    /**
     * Create a Literal from a variant.
     */
    static cres_qresult<Literal> fromVariant(const QVariant& _value,
                                             const QString& _lang = QString());
    /**
     * Create a Literal from a string and datatype.
     * If the type is not available in knowCore MetaType system, it is stored as a string.
     * If the type is available, attemps a conversion, which may fails.
     */
    static cres_qresult<Literal> fromRdfLiteral(const knowCore::Uri& _datatype,
                                                const QString& _value,
                                                const QString& _lang = QString());
    /**
     * @return the lang tag
     */
    QString lang() const;
    bool operator<(const Literal& _rhs) const;
    bool operator==(const Literal& _rhs) const;
    cres_qresult<QByteArray> md5() const;
    cres_qresult<QJsonValue> toJsonValue(const knowCore::SerialisationContexts& _contexts) const;
    static cres_qresult<Literal> fromJsonValue(const QJsonValue& _value,
                                               const knowCore::DeserialisationContexts& _contexts);
    cres_qresult<QCborValue> toCborValue(const knowCore::SerialisationContexts& _contexts) const;
    static cres_qresult<Literal> fromCborValue(const QCborValue& _value,
                                               const knowCore::DeserialisationContexts& _contexts);
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
  template<typename _T_>
  Literal Literal::fromValue(const _T_& _value, const Lang& _lang)
  {
    return Literal(knowCore::Value::fromValue(_value), _lang);
  }
  template<typename _T_>
  inline cres_qresult<Literal> Literal::fromValue(const knowCore::Uri& _datatype, const _T_& _value,
                                                  const QString& _lang,
                                                  knowCore::TypeCheckingMode _conversion)
  {
    const auto [success, value, message] = Value::fromValue(_datatype, _value, _conversion);
    Q_UNUSED(message);
    if(success)
    {
      return cres_success(Literal(value.value(), _lang));
    }
    else if constexpr(std::is_same_v<QString, _T_> or std::is_same_v<QByteArray, _T_>)
    {
      const auto [success_2, value_2, message_2] = Value::fromRdfLiteral(_datatype, _value);
      if(success_2)
      {
        clog_assert(value_2.value().datatype() == _datatype);
        return cres_success(Literal(value_2.value(), _lang));
      }
      else
      {
        knowCore::Value value = Value::fromValue(_value);
        return cres_success(Literal(_datatype, value, _lang));
      }
    }
    else
    {
      return cres_forward_failure(message);
    }
  }
  template<>
  inline cres_qresult<Literal>
    Literal::fromValue<QVariant>(const knowCore::Uri& _datatype, const QVariant& _value,
                                 const QString& _lang, knowCore::TypeCheckingMode _conversion)
  {
    return fromVariant(_datatype, _value, _lang, _conversion);
  }
  uint qHash(const Literal& key, uint seed = 0);
} // namespace knowRDF

clog_format_declare_formatter(knowRDF::Literal)
{
  if(p.datatype() != p.Value::datatype())
  {
    if(p.lang().isEmpty())
    {
      return format_to(ctx.out(), "{}({})", p.datatype(), (const knowCore::Value&)p);
    }
    else
    {
      return format_to(ctx.out(), "{}({})@{}", p.datatype(), (const knowCore::Value&)p, p.lang());
    }
  }
  else if(p.lang().isEmpty())
  {
    return format_to(ctx.out(), "{}", (const knowCore::Value&)(p));
  }
  else
  {
    return format_to(ctx.out(), "{}@{}", (const knowCore::Value&)p, p.lang());
  }
}

#include <knowCore/MetaType.h>
KNOWCORE_DECLARE_FULL_METATYPE(knowRDF, Literal);

inline knowRDF::Lang operator"" _kRDFlang(const char* _text, std::size_t _size)
{
  return knowRDF::Lang(QString::fromUtf8(_text, _size));
}

#if __cplusplus > 201703L
inline knowRDF::Lang operator"" _kRDFlang(const char8_t* _text, std::size_t _size)
{
  return knowRDF::Lang(QString::fromUtf8((const char*)_text, _size));
}
#endif
