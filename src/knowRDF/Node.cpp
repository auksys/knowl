#include "Node.h"

#include <clog_qt>
#include <knowCore/Uri.h>
#include <knowCore/UriList.h>
#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/rdfs.h>

#include "BlankNode.h"
#include "Object.h"
#include "Subject.h"
#include "Triple.h"

using namespace knowRDF;

struct Node::Private
{
  Type type;
  knowCore::Uri uri;
  BlankNode blankNode;
  Literal literal;
  QString variable;

  QMultiHash<knowCore::Uri, const Node*> children, parents;
  QHash<const Node*, int> childRanks;
  QList<const Node*> sortChildren(QList<const Node*> _children);

  void getCollection(QList<const Node*>* list, const Node* _current);
};

QList<const Node*> Node::Private::sortChildren(QList<const Node*> _children)
{
  std::sort(_children.begin(), _children.end(), [this](const Node* _a, const Node* _b)
            { return childRanks.value(_a) < childRanks.value(_b); });
  return _children;
}

void Node::Private::getCollection(QList<const Node*>* _list, const Node* _current)
{
  const Node* rdfFirst = _current->getFirstChild(knowCore::Uris::rdf::first);
  const Node* rdfRest = _current->getFirstChild(knowCore::Uris::rdf::rest);

  if(rdfFirst)
  {
    _list->append(rdfFirst);
  }
  if(rdfRest)
  {
    getCollection(_list, rdfRest);
  }
}

Node::Node() : d(new Private) { d->type = Type::Undefined; }

Node::Node(const Object& _object) : d(new Private)
{
  switch(_object.type())
  {
  case Object::Type::Undefined:
    d->type = Type::Undefined;
    break;
  case Object::Type::BlankNode:
    d->type = Type::BlankNode;
    d->blankNode = _object.blankNode();
    break;
  case Object::Type::Literal:
    d->type = Type::Literal;
    d->literal = _object.literal();
    break;
  case Object::Type::Uri:
    d->type = Type::Uri;
    d->uri = _object.uri();
    break;
  case Object::Type::Variable:
    d->type = Type::Variable;
    d->variable = _object.variableName();
    break;
  }
}

Node::Node(const Subject& _subject) : d(new Private)
{
  switch(_subject.type())
  {
  case Subject::Type::Undefined:
    d->type = Type::Undefined;
    break;
  case Subject::Type::BlankNode:
    d->type = Type::BlankNode;
    d->blankNode = _subject.blankNode();
    break;
  case Subject::Type::Uri:
    d->type = Type::Uri;
    d->uri = _subject.uri();
    break;
  case Subject::Type::Variable:
    d->type = Type::Variable;
    d->variable = _subject.variableName();
    break;
  }
}

Node::Node(const knowCore::Uri& _uri) : d(new Private)
{
  d->type = Type::Uri;
  d->uri = _uri;
}

Node::Node(const BlankNode& _blankNode) : d(new Private)
{
  d->type = Type::BlankNode;
  d->blankNode = _blankNode;
}

Node::~Node() { delete d; }

BlankNode Node::blankNode() const { return d->blankNode; }

QString Node::variable() const { return d->variable; }

knowCore::Uri Node::uri() const { return d->uri; }

Literal Node::literal() const { return d->literal; }

Node::Type Node::type() const { return d->type; }

void Node::connect(const knowCore::Uri& _predicate, Node* _node)
{
  d->childRanks[_node] = d->childRanks.size();
  d->children.insert(_predicate, _node);
  _node->d->parents.insert(_predicate, this);
}

void Node::disconnect(const knowCore::Uri& _predicate, const Object& _object)
{
  Node nodeObject(_object);
  for(QMultiHash<knowCore::Uri, const Node*>::iterator it = d->children.begin();
      it != d->children.end();)
  {
    if(it.value()->equal(&nodeObject))
    {
      it.value()->d->parents.remove(_predicate, this);
      it = d->children.erase(it);
    }
    else
    {
      ++it;
    }
  }
}

QList<const Node*> Node::children(const knowCore::Uri& _predicate) const
{
  return d->sortChildren(d->children.values(_predicate));
}

QMultiHash<knowCore::Uri, const Node*> Node::children() const { return d->children; }

bool Node::hasChildren(const knowCore::Uri& _predicate) const
{
  return d->children.contains(_predicate);
}

bool Node::hasChildren(const knowCore::UriList& _predicates) const
{
  for(const knowCore::Uri& p : _predicates)
  {
    if(hasChildren(p))
      return true;
  }
  return false;
}

const Node* Node::getFirstChild(const knowCore::Uri& name) const
{
  return d->children.value(name, nullptr);
}

QList<const Node*> Node::parents(const knowCore::Uri& _predicate) const
{
  return d->parents.values(_predicate);
}

QMultiHash<knowCore::Uri, const Node*> Node::parents() const { return d->parents; }

bool Node::equal(const Node* _otherNode) const
{
  if(d->type == _otherNode->type())
  {
    switch(d->type)
    {
    case Type::Undefined:
      return true;
    case Type::Uri:
      return d->uri == _otherNode->d->uri;
    case Type::BlankNode:
      return d->blankNode == _otherNode->d->blankNode;
    case Type::Literal:
      return d->literal == _otherNode->d->literal;
    case Type::Variable:
      return d->variable == _otherNode->d->variable;
    }
    clog_fatal("Impossible");
  }
  else
  {
    return false;
  }
}

Triple Node::triple(const Node* _subject, const knowCore::Uri& _predicate, const Node* _object)
{
  Subject s;
  switch(_subject->type())
  {
  case Type::Undefined:
    break;
  case Type::BlankNode:
    s = Subject(_subject->d->blankNode);
    break;
  case Type::Literal:
    clog_fatal("Subject cannot be a literal");
    break;
  case Type::Uri:
    s = Subject(_subject->d->uri);
    break;
  case Type::Variable:
    s = Subject(_subject->d->variable, Subject::Type::Variable);
    break;
  }
  Object o;
  switch(_object->type())
  {
  case Type::Undefined:
    break;
  case Type::BlankNode:
    o = Object(_object->d->blankNode);
    break;
  case Type::Literal:
    o = Object(_object->d->literal);
    break;
  case Type::Uri:
    o = Object(_object->d->uri);
    break;
  case Type::Variable:
    o = Object(_object->d->variable, Object::Type::Variable);
    break;
  }
  return Triple(s, _predicate, o);
}

QList<const Node*> Node::getCollection(const knowCore::Uri& _predicate) const
{
  const Node* node = getFirstChild(_predicate);
  if(node)
  {
    return node->getCollection();
  }
  else
  {
    return QList<const Node*>();
  }
}

QList<const Node*> Node::getCollection() const
{
  QList<const Node*> nodes;
  d->getCollection(&nodes, this);
  return nodes;
}

bool Node::isCollection() const
{
  return d->children.contains(knowCore::Uris::rdf::first)
         or d->children.contains(knowCore::Uris::rdf::rest);
}

bool Node::belongsToClass(const knowCore::Uri& _class) const
{
  for(const Node* n : children(knowCore::Uris::rdf::a))
  {
    if(n->uri() == _class or n->subClassOf(_class))
    {
      return true;
    }
  }
  return false;
}

bool Node::subClassOf(const knowCore::Uri& _class) const
{
  for(const Node* n : children(knowCore::Uris::rdfs::subClassOf))
  {
    if(n->uri() == _class or n->subClassOf(_class))
    {
      return true;
    }
  }
  return false;
}
