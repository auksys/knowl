
#include <QMultiHash>
#include <knowCore/Forward.h>

namespace knowRDF
{
  class BlankNode;
  class Subject;
  class Object;
  class Graph;
  class Literal;
  class Triple;
  /**
   * Represent a subject or an object in the graph.
   */
  class Node
  {
    friend class Graph;
  public:
    enum class Type
    {
      Undefined,
      Uri,
      BlankNode,
      Literal,
      Variable
    };
  private:
    /// Create a @ref Type::Undefined node.
    Node();
    /// Create a @ref Type::Uri node.
    Node(const knowCore::Uri& _uri);
    /// Create a @ref Type::BlankNode node.
    Node(const BlankNode& _blankNode);
    /// Create a @ref Type::BlankNode or @ref Type::Uri or @ref Type::Variable node (depending on
    /// the subject).
    Node(const Subject& _subject);
    /// Create a @ref Type::BlankNode or @ref Type::Uri or @ref Type::Literal or @ref Type::Variable
    /// node (depending on the object).
    Node(const Object& _object);
    ~Node();
    /**
     * Connect this node as a subject to a node @p _node which will be the object using the
     * predicate @p _predicate. This node will be the parent in the connection while @p _node will
     * be the child.
     */
    void connect(const knowCore::Uri& _predicate, Node* _node);
    void disconnect(const knowCore::Uri& _predicate, const Object& _object);
  public:
    /**
     * @return the type of the node
     */
    Type type() const;
    /**
     * @return a literal if the node is of type @ref Type::Literal otherwise it is invalid
     */
    Literal literal() const;
    /**
     * @return a uri if the node is of type @ref Type::Uri otherwise it is invalid
     */
    knowCore::Uri uri() const;
    /**
     * @return a blank node if the node is of type @ref Type::BlankNode otherwise it is invalid
     */
    BlankNode blankNode() const;
    /**
     * @return a variable name if the node is of type @ref Type::Variable otherwise it is invalid
     */
    QString variable() const;
  public:
    /**
     * @return if this node is equal to the other node
     */
    bool equal(const Node* _otherNode) const;
    /**
     * @return all the children and the associated predicate
     */
    QMultiHash<knowCore::Uri, const Node*> children() const;
    /**
     * @return all the parents and the associated predicate
     */
    QMultiHash<knowCore::Uri, const Node*> parents() const;
    /**
     * @return true if the node belongs to class \p _class (i.e. one of the rdf::a child is equal to
     * \p _class or recursively belongs to the class).
     */
    bool belongsToClass(const knowCore::Uri& _class) const;
    /**
     * @return true if the node is a subclass of \p _class (i.e. one of the rdfs::subClassOf child
     * is equal to \p _class or recursively belongs to the class).
     */
    bool subClassOf(const knowCore::Uri& _class) const;
    /**
     * @return all the parents in the graph that are connected with the uri @p _predicate
     */
    QList<const Node*> parents(const knowCore::Uri& _predicate) const;
    /**
     * @return all the children in the graph that are connected with the uri @p _predicate
     */
    QList<const Node*> children(const knowCore::Uri& _predicate) const;
    /**
     * @return true if it has a child of uri \ref _predicate
     */
    bool hasChildren(const knowCore::Uri& _predicate) const;
    bool hasChildren(const knowCore::UriList& _predicates) const;
    /**
     * @return the first child in the graph which is connected by uri @p _predicate.
     */
    const Node* getFirstChild(const knowCore::Uri& _predicate) const;
    /**
     * Function that will create a triple reprsentation from a subject and an object defined as a
     * @ref Node.
     */
    static Triple triple(const Node* _subject, const knowCore::Uri& _predicate,
                         const Node* _object);
    /**
     * @return the nodes forming a RDF collection for the first child pointed by @p _predicate.
     */
    QList<const Node*> getCollection(const knowCore::Uri& _predicate) const;
    /**
     * @return the nodes forming a RDF collection starting from that node
     */
    QList<const Node*> getCollection() const;
    /**
     * @return if this element is a collection
     */
    bool isCollection() const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowRDF

#include <knowCore/Formatter.h>

clog_format_declare_enum_formatter(knowRDF::Node::Type, Undefined, Uri, BlankNode, Literal,
                                   Variable);
