#include "Object.h"

#include <variant>

#include <QCborMap>
#include <QJsonObject>
#include <QUuid>

#include <Cyqlops/Crypto/Hash.h>

#include <clog_qt>
#include <knowCore/Uri.h>
#include <knowCore/Uris/xsd.h>

#include "BlankNode.h"
#include "Skolemisation.h"

using namespace knowRDF;

struct Object::Private : public QSharedData
{
  Type type;

  knowCore::Uri uri() const { return std::get<1>(value); }
  Literal literal() const { return std::get<2>(value); }
  BlankNode blankNode() const { return std::get<3>(value); }
  QString variableName() const { return std::get<4>(value); }

  std::variant<std::nullptr_t, knowCore::Uri, Literal, BlankNode, QString> value;
};

Object::Object() : d(new Private) { d->type = Type::Undefined; }

Object::Object(const QString& _string, Object::Type _type) : d(new Private)
{
  d->type = _type;
  switch(d->type)
  {
  case Type::Uri:
    d->value = knowCore::Uri(_string);
    break;
  case Type::Literal:
    d->value = Literal::fromValue(_string);
    break;
  case Type::Variable:
    d->value = _string;
    break;
  default:
    clog_fatal("Object::Object: invalid type");
  }
}

Object::Object(const knowCore::Uri& _uri) : d(new Private)
{
  if(isBlankNodeSkolemisation(_uri))
  {
    QPair<QString, QString> uuid_label = parseBlankNodeUri(_uri);
    d->type = Type::BlankNode;
    d->value = knowRDF::BlankNode(QUuid(uuid_label.first), uuid_label.second);
  }
  else
  {
    d->value = _uri;
    d->type = Type::Uri;
  }
}

Object::Object(const BlankNode& _blankNodeRef) : d(new Private)
{
  d->type = Type::BlankNode;
  d->value = _blankNodeRef;
}

Object::Object(const Literal& _literal) : d(new Private)
{
  if(knowCore::ValueIs<knowCore::Uri> val_uri = _literal)
  {
    d->type = Type::Uri;
    d->value = val_uri;
  }
  else
  {
    d->type = Type::Literal;
    d->value = _literal;
  }
}

Object::Object(const Object& _rhs) : d(_rhs.d) {}

Object& Object::operator=(const Object& _rhs)
{
  d = _rhs.d;
  return *this;
}

Object::~Object() {}

bool Object::operator==(const Object& _rhs) const
{
  return d->type == _rhs.d->type
         and ((d->type == Type::Uri and d->uri() == _rhs.d->uri())
              or (d->type == Type::BlankNode and d->blankNode() == _rhs.d->blankNode())
              or (d->type == Type::Literal and d->literal() == _rhs.d->literal())
              or (d->type == Type::Variable and d->variableName() == _rhs.d->variableName()));
}

Object::Type Object::type() const { return d->type; }

BlankNode Object::blankNode() const
{
  clog_assert(d->type == Type::BlankNode);
  return d->blankNode();
}

Literal Object::literal() const
{
  clog_assert(d->type == Type::Literal);
  return d->literal();
}

knowCore::Uri Object::uri() const
{
  clog_assert(d->type == Type::Uri);
  return d->uri();
}

QString Object::variableName() const
{
  clog_assert(d->type == Type::Variable);
  return d->variableName();
}

cres_qresult<QByteArray> Object::md5() const
{
  QByteArray data;
  switch(d->type)
  {
  case Object::Type::Undefined:
    break;
  case Object::Type::Uri:
    data = ((QString)d->uri()).toUtf8();
    break;
  case Object::Type::BlankNode:
    data = d->blankNode().uuid().toByteArray();
    break;
  case Object::Type::Variable:
    data = d->variableName().toUtf8();
    break;
  case Object::Type::Literal:
    return d->literal().md5();
  }
  return cres_success(Cyqlops::Crypto::Hash::md5(data));
}

#define TYPE_KEY QStringLiteral("type")
#define UNDEFINED_TYPE_KEY QStringLiteral("undefined")
#define URI_TYPE_KEY QStringLiteral("uri")
#define BLANKNODE_TYPE_KEY QStringLiteral("blanknode")
#define VARIABLE_TYPE_KEY QStringLiteral("variable")
#define LITERAL_TYPE_KEY QStringLiteral("literal")
#define VALUE_KEY u8"value"_kCs

cres_qresult<QJsonValue> Object::toJsonValue(const knowCore::SerialisationContexts& _contexts) const
{
  QJsonObject json_object;
  switch(d->type)
  {
  case Object::Type::Undefined:
    json_object[TYPE_KEY] = UNDEFINED_TYPE_KEY;
    break;
  case Object::Type::Uri:
    json_object[TYPE_KEY] = URI_TYPE_KEY;
    json_object[VALUE_KEY] = (QString)d->uri();
    break;
  case Object::Type::BlankNode:
    json_object[TYPE_KEY] = BLANKNODE_TYPE_KEY;
    json_object[VALUE_KEY] = d->blankNode().uuid().toString();
    break;
  case Object::Type::Variable:
    json_object[TYPE_KEY] = VARIABLE_TYPE_KEY;
    json_object[VALUE_KEY] = d->variableName();
    break;
  case Object::Type::Literal:
  {
    json_object[TYPE_KEY] = LITERAL_TYPE_KEY;
    cres_try(QJsonValue literal, d->literal().toJsonValue(_contexts));
    json_object[VALUE_KEY] = literal;
    break;
  }
  }
  return cres_success(json_object);
}

cres_qresult<Object> Object::fromJsonValue(const QJsonValue& _value,
                                           const knowCore::DeserialisationContexts& _contexts)
{
  if(_value.isObject())
  {
    QJsonObject json_object = _value.toObject();
    QString type = json_object.value(TYPE_KEY).toString();

    if(type == UNDEFINED_TYPE_KEY)
    {
      return cres_success(Object());
    }
    else if(type == URI_TYPE_KEY)
    {
      return cres_success(Object(json_object.value(VALUE_KEY).toString(), Type::Uri));
    }
    else if(type == BLANKNODE_TYPE_KEY)
    {
      return cres_success(
        Object(BlankNode(QUuid::fromString(json_object.value(VALUE_KEY).toString()))));
    }
    else if(type == VARIABLE_TYPE_KEY)
    {
      return cres_success(Object(json_object.value(VALUE_KEY).toString(), Type::Variable));
    }
    else if(type == LITERAL_TYPE_KEY)
    {
      cres_try(Literal literal, Literal::fromJsonValue(json_object.value(VALUE_KEY), _contexts));
      return cres_success(Object(literal));
    }

    return cres_failure("Unknow type '{}'", type);
  }
  else
  {
    return cres_failure("Expected object got '{}'", _value);
  }
}

cres_qresult<QCborValue> Object::toCborValue(const knowCore::SerialisationContexts& _contexts) const
{
  QCborMap cbor_map;
  switch(d->type)
  {
  case Object::Type::Undefined:
    cbor_map[TYPE_KEY] = UNDEFINED_TYPE_KEY;
    break;
  case Object::Type::Uri:
    cbor_map[TYPE_KEY] = URI_TYPE_KEY;
    cbor_map[VALUE_KEY] = (QString)d->uri();
    break;
  case Object::Type::BlankNode:
    cbor_map[TYPE_KEY] = BLANKNODE_TYPE_KEY;
    cbor_map[VALUE_KEY] = d->blankNode().uuid().toRfc4122();
    break;
  case Object::Type::Variable:
    cbor_map[TYPE_KEY] = VARIABLE_TYPE_KEY;
    cbor_map[VALUE_KEY] = d->variableName();
    break;
  case Object::Type::Literal:
  {
    cbor_map[TYPE_KEY] = LITERAL_TYPE_KEY;
    cres_try(QCborValue literal, d->literal().toCborValue(_contexts));
    cbor_map[VALUE_KEY] = literal;
    break;
  }
  }
  return cres_success(cbor_map);
}

cres_qresult<Object> Object::fromCborValue(const QCborValue& _value,
                                           const knowCore::DeserialisationContexts& _contexts)
{
  if(_value.isMap())
  {
    QCborMap cbor_map = _value.toMap();
    QString type = cbor_map.value(TYPE_KEY).toString();

    if(type == UNDEFINED_TYPE_KEY)
    {
      return cres_success(Object());
    }
    else if(type == URI_TYPE_KEY)
    {
      return cres_success(Object(cbor_map.value(VALUE_KEY).toString(), Type::Uri));
    }
    else if(type == BLANKNODE_TYPE_KEY)
    {
      return cres_success(
        Object(BlankNode(QUuid::fromString(cbor_map.value(VALUE_KEY).toString()))));
    }
    else if(type == VARIABLE_TYPE_KEY)
    {
      return cres_success(Object(cbor_map.value(VALUE_KEY).toString(), Type::Variable));
    }
    else if(type == LITERAL_TYPE_KEY)
    {
      cres_try(Literal literal, Literal::fromCborValue(cbor_map.value(VALUE_KEY), _contexts));
      return cres_success(Object(literal));
    }

    return cres_failure("Unknow type '{}'", type);
  }
  else
  {
    return cres_failure("Expected object got '{}'", _value);
  }
}

uint knowRDF::qHash(const Object& key, uint seed)
{
  switch(key.type())
  {
  case Object::Type::Undefined:
    return 0;
  case Object::Type::Uri:
    return qHash(key.uri(), seed);
  case Object::Type::BlankNode:
    return qHash(key.blankNode(), seed);
  case Object::Type::Literal:
    return qHash(key.literal(), seed);
  case Object::Type::Variable:
    return qHash("?" + key.variableName(), seed);
  }
  return 0;
}

#include <knowCore/MetaTypeImplementation.h>
#include <knowCore/Uris/askcore_datatype.h>

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(Object)
cres_qresult<QByteArray> md5(const Object& _value) const override { return _value.md5(); }
cres_qresult<QJsonValue> toJsonValue(const Object& _value,
                                     const SerialisationContexts& _contexts) const override
{
  return _value.toJsonValue(_contexts);
}
cres_qresult<void> fromJsonValue(Object* _value, const QJsonValue& _json_value,
                                 const knowCore::DeserialisationContexts& _contexts) const override
{
  return cres_try_assign(_value, Object::fromJsonValue(_json_value, _contexts));
}
cres_qresult<QCborValue> toCborValue(const Object& _value,
                                     const SerialisationContexts& _contexts) const override
{
  return _value.toCborValue(_contexts);
}
cres_qresult<void> fromCborValue(Object* _value, const QCborValue& _cbor_value,
                                 const knowCore::DeserialisationContexts& _contexts) const override
{
  return cres_try_assign(_value, Object::fromCborValue(_cbor_value, _contexts));
}
cres_qresult<QString> printable(const Object& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const Object& _value,
                                   const SerialisationContexts& _contexts) const override
{
  return toJsonString(_value, _contexts);
}
cres_qresult<void> fromRdfLiteral(Object* _value, const QString& _serialised,
                                  const knowCore::DeserialisationContexts& _contexts) const override
{
  return fromJsonString(_value, _serialised, _contexts);
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(Object)

KNOWCORE_DEFINE_METATYPE(knowRDF::Object, knowCore::Uris::askcore_datatype::object,
                         knowCore::MetaTypeTraits::DebugStreamOperator)
