#pragma once

#include <knowCore/Uris/Uris.h>

#include "BlankNode.h"
#include "Literal.h"

namespace knowRDF
{
  class Object
  {
  public:
    enum class Type
    {
      Undefined,
      Uri,
      BlankNode,
      Literal,
      Variable
    };
  public:
    Object();
    Object(const QString& _string, Type _type);
    Object(const knowCore::Uri& _uri);
    Object(const BlankNode& _blankNodeRef);
    Object(const Literal& _literal);
    template<typename _T_>
      requires(knowCore::Uris::IsUriDefinitionV<_T_>)
    Object(const _T_& _t) : Object(knowCore::Uri(_t))
    {
    }
    template<typename _T_>
    static Object fromValue(const _T_& _value, const Lang& _lang = Lang(QString()));
    template<typename _T_>
    static cres_qresult<Object> fromValue(const knowCore::Uri& _dataTypeUri, const _T_& _value,
                                          const QString& _lang = QString(),
                                          knowCore::TypeCheckingMode _conversion
                                          = knowCore::TypeCheckingMode::Safe);
    //       Object(const knowCore::Uri& _uri, const char* _value, const QString& _lang =
    //       QString());
    Object(const Object& _rhs);
    Object& operator=(const Object& _rhs);
    ~Object();
    bool operator==(const Object& _rhs) const;
    Type type() const;
    knowCore::Uri uri() const;
    BlankNode blankNode() const;
    Literal literal() const;
    QString variableName() const;
    cres_qresult<QByteArray> md5() const;
    cres_qresult<QJsonValue> toJsonValue(const knowCore::SerialisationContexts&) const;
    static cres_qresult<Object> fromJsonValue(const QJsonValue& _value,
                                              const knowCore::DeserialisationContexts& _context);
    cres_qresult<QCborValue> toCborValue(const knowCore::SerialisationContexts&) const;
    static cres_qresult<Object> fromCborValue(const QCborValue& _value,
                                              const knowCore::DeserialisationContexts& _context);
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
  template<typename _T_>
  Object Object::fromValue(const _T_& _value, const Lang& _lang)
  {
    return Literal::fromValue<_T_>(_value, _lang);
  }
  template<typename _T_>
  cres_qresult<Object> Object::fromValue(const knowCore::Uri& _dataTypeUri, const _T_& _value,
                                         const QString& _lang,
                                         knowCore::TypeCheckingMode _conversion)
  {
    return Literal::fromValue(_dataTypeUri, _value, _lang, _conversion);
  }
  uint qHash(const Object& key, uint seed = 0);
} // namespace knowRDF

clog_format_declare_formatter(knowRDF::Object)
{
  switch(p.type())
  {
  case knowRDF::Object::Type::Undefined:
    break;
  case knowRDF::Object::Type::Uri:
    return format_to(ctx.out(), "uri({})", p.uri());
  case knowRDF::Object::Type::BlankNode:
    return format_to(ctx.out(), "blank_node({})", p.blankNode());
  case knowRDF::Object::Type::Literal:
    return format_to(ctx.out(), "literal({})", p.literal());
  case knowRDF::Object::Type::Variable:
    return format_to(ctx.out(), "variable(?{})", p.variableName());
  }
  return format_to(ctx.out(), "undefined()");
}

clog_format_declare_enum_formatter(knowRDF::Object::Type, Undefined, Uri, BlankNode, Literal,
                                   Variable);

#include <knowCore/MetaType.h>
KNOWCORE_DECLARE_FULL_METATYPE(knowRDF, Object);
