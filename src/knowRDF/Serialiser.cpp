#include "Serialiser.h"

#include <QIODevice>
#include <QTextStream>

#include <knowCore/Curie.h>
#include <knowCore/Uri.h>

#include "Object.h"
#include "Skolemisation.h"
#include "Subject.h"
#include "Triple.h"

using namespace knowRDF;

struct Serialiser::Private
{
  QIODevice* output;
  QTextStream* stream;
  knowCore::UriManager uriManager;
  bool delete_output;
  QHash<BlankNode, QString> blanknodes;
  QString blankNode(const BlankNode& _bn);
  void printUri(const knowCore::Uri& _uri);

  bool saveBlankNodeAsUri = false;
};

QString Serialiser::Private::blankNode(const BlankNode& _bn)
{
  if(blanknodes.contains(_bn))
  {
    return blanknodes.value(_bn);
  }
  QString v = QString::number(blanknodes.size());
  blanknodes[_bn] = v;
  return v;
}

void Serialiser::Private::printUri(const knowCore::Uri& _uri)
{
  knowCore::Curie c;
  if(uriManager.curify(_uri, &c))
  {
    (*stream) << c.prefix() << ":" << c.suffix();
  }
  else
  {
    QString t = _uri;
    if(t.startsWith(uriManager.base()))
    {
      t = t.right(t.length() - ((QString)uriManager.base()).length());
    }
    (*stream) << "<" << t << ">";
  }
}

Serialiser::Serialiser(QIODevice* _output, const knowCore::UriManager& _uriManager,
                       bool _delete_device, const QString& _format)
    : d(new Private)
{
  clog_assert(_format == knowCore::FileFormat::Turtle);
  d->output = _output;
  d->stream = new QTextStream(d->output);
  d->uriManager = _uriManager;
  d->delete_output = _delete_device;
}

Serialiser::Serialiser(QTextStream* _stream, const knowCore::UriManager& _uriManager,
                       const QString& _format)
    : d(new Private)
{
  clog_assert(_format == knowCore::FileFormat::Turtle);
  d->output = nullptr;
  d->stream = _stream;
  d->uriManager = _uriManager;
  d->delete_output = false;
}

Serialiser::~Serialiser()
{
  if(d->output)
    delete d->stream;
  if(d->delete_output)
    delete d->output;
  delete d;
}

void Serialiser::setSaveBlankNodeAsUri(bool _v) { d->saveBlankNodeAsUri = _v; }

void Serialiser::serialise(const Triple& _triple)
{
  Subject s = _triple.subject();
  switch(s.type())
  {
  case Subject::Type::BlankNode:
    if(d->saveBlankNodeAsUri)
    {
      d->printUri(blankNodeSkolemisation(s.blankNode()));
    }
    else
    {
      (*d->stream) << "_:" << d->blankNode(s.blankNode());
    }
    break;
  case Subject::Type::Undefined:
    (*d->stream) << "(undefined)";
    break;
  case Subject::Type::Uri:
  {
    d->printUri(s.uri());
    break;
  }
  case Subject::Type::Variable:
  {
    (*d->stream) << "?" << s.variableName();
    break;
  }
  }
  (*d->stream) << " ";
  d->printUri(_triple.predicate());
  (*d->stream) << " ";
  Object o = _triple.object();
  switch(o.type())
  {
  case Object::Type::BlankNode:
    if(d->saveBlankNodeAsUri)
    {
      d->printUri(blankNodeSkolemisation(o.blankNode()));
    }
    else
    {
      (*d->stream) << "_:" << d->blankNode(o.blankNode());
    }
    break;
  case Object::Type::Literal:
  {
    Literal l = o.literal();
    QString serialised;
    knowCore::Uri serialised_uri = l.datatype();
    const auto [success, serialised_src, error] = l.toRdfLiteral();

    if(success)
    {
      // Escape '\n' and '"'
      QString serialised = serialised_src.value();
      serialised = serialised.replace('\n', "\\n").replace('"', "\\\"");
      (*d->stream) << "\"" << serialised << "\"^^";
      d->printUri(serialised_uri);
      if(l.lang().isEmpty())
      {
      }
      else
      {
        (*d->stream) << "@" << l.lang();
      }
    }
    else
    {
      clog_fatal("Implement reporting of error during serialisation!");
    }
  }
  break;
  case Object::Type::Undefined:
    (*d->stream) << "(undefined)";
    break;
  case Object::Type::Uri:
    d->printUri(o.uri());
    break;
  case Object::Type::Variable:
    (*d->stream) << "?" << o.variableName();
    break;
  }
  (*d->stream) << " .\n";
}

void Serialiser::serialise(const QList<Triple>& _triples)
{
  for(const Triple& t : _triples)
  {
    serialise(t);
  }
  d->stream->flush();
}
