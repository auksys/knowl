#include <knowCore/FileFormat.h>
#include <knowCore/Forward.h>

class QIODevice;
class QTextStream;

namespace knowRDF
{
  class Triple;
  class Serialiser
  {
  public:
    Serialiser(QIODevice* _output, const knowCore::UriManager& _uriManager,
               bool _delete_device = false, const QString& _format = knowCore::FileFormat::Turtle);
    Serialiser(QTextStream* _stream, const knowCore::UriManager& _uriManager,
               const QString& _format = knowCore::FileFormat::Turtle);
    ~Serialiser();
    void setSaveBlankNodeAsUri(bool _v);
    void serialise(const Triple& _triple);
    void serialise(const QList<knowRDF::Triple>& _triples);
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowRDF
