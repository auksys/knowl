#include <QUuid>

#include "BlankNode.h"
#include <knowCore/Uris/askcore_db.h>

namespace knowRDF
{
  inline QString blankNodeSkolemisation(const BlankNode& _blankNode)
  {
    QString uuidString = _blankNode.uuid().toString();
    uuidString = uuidString.mid(1, uuidString.length() - 2); // Remove the {}
    if(not _blankNode.label().isEmpty())
    {
      uuidString += "?" + _blankNode.label();
    }
    return knowCore::Uris::askcore_db_blank::base.resolved(uuidString);
  }
  inline bool isBlankNodeSkolemisation(const knowCore::Uri& _uri)
  {
    return QString(_uri).startsWith(knowCore::Uris::askcore_db_blank::base);
  }
  /**
   * Parse a blank node uri and return the UUID and the Label (label is optional and after '?')
   */
  inline QPair<QString, QString> parseBlankNodeUri(const knowCore::Uri& _uri)
  {
    QString str = _uri;
    int index_question = str.indexOf('?');
    QString label;
    if(index_question > 0)
    {
      label = str.right(str.length() - index_question - 1);
      str = str.left(index_question - 1);
    }
    return {str.right(str.size() - ((QString)knowCore::Uris::askcore_db_blank::base).size()),
            label};
  }
} // namespace knowRDF
