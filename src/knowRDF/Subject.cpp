#include "Subject.h"

#include <variant>

#include <QCborMap>
#include <QJsonObject>
#include <QUuid>

#include <Cyqlops/Crypto/Hash.h>

#include <clog_qt>
#include <knowCore/Uri.h>

#include "BlankNode.h"
#include "Skolemisation.h"

using namespace knowRDF;

struct Subject::Private : public QSharedData
{
  Type type;
  std::variant<std::nullptr_t, knowCore::Uri, BlankNode, QString> value;

  knowCore::Uri uri() const { return std::get<1>(value); }
  BlankNode blankNode() const { return std::get<2>(value); }
  QString variableName() const { return std::get<3>(value); }
};

Subject::Subject() : d(new Private) { d->type = Type::Undefined; }

Subject::Subject(const knowCore::Uri& _uri) : d(new Private)
{
  if(isBlankNodeSkolemisation(_uri))
  {
    QPair<QString, QString> uuid_label = parseBlankNodeUri(_uri);
    d->type = Type::BlankNode;
    d->value = knowRDF::BlankNode(QUuid(uuid_label.first), uuid_label.second);
  }
  else
  {
    d->value = _uri;
    d->type = Type::Uri;
  }
}

Subject::Subject(const knowCore::Uri& _base, const QString& _suffix)
    : Subject(knowCore::Uri(_base, _suffix))
{
}

Subject::Subject(const QString& _string, Subject::Type _type) : d(new Private)
{
  d->type = _type;
  switch(d->type)
  {
  case Type::Uri:
    d->value = knowCore::Uri(_string);
    break;
  case Type::Variable:
    d->value = _string;
    break;
  default:
    clog_fatal("Subject::Subject: invalid type");
  }
}

Subject::Subject(const BlankNode& _blankNodeRef) : d(new Private)
{
  d->type = Type::BlankNode;
  d->value = _blankNodeRef;
}

Subject::Subject(const Subject& _rhs) : d(_rhs.d) {}

Subject& Subject::operator=(const Subject& _rhs)
{
  d = _rhs.d;
  return *this;
}

Subject::~Subject() {}

bool Subject::operator==(const Subject& _rhs) const
{
  return d->type == _rhs.d->type
         and ((d->type == Type::Uri and d->uri() == _rhs.d->uri())
              or (d->type == Type::BlankNode and d->blankNode() == _rhs.d->blankNode())
              or (d->type == Type::Variable and d->variableName() == _rhs.d->variableName()));
}

Subject::Type Subject::type() const { return d->type; }

BlankNode Subject::blankNode() const { return d->blankNode(); }

knowCore::Uri Subject::uri() const { return d->uri(); }

QString Subject::variableName() const { return d->variableName(); }

QByteArray Subject::md5() const
{
  QByteArray data;
  switch(d->type)
  {
  case Subject::Type::Undefined:
    break;
  case Subject::Type::Uri:
    data = ((QString)d->uri()).toUtf8();
    break;
  case Subject::Type::BlankNode:
    data = d->blankNode().uuid().toByteArray();
    break;
  case Subject::Type::Variable:
    data = d->variableName().toUtf8();
    break;
  }
  return Cyqlops::Crypto::Hash::md5(data);
}

#define TYPE_KEY QStringLiteral("type")
#define UNDEFINED_TYPE_KEY QStringLiteral("undefined")
#define URI_TYPE_KEY QStringLiteral("uri")
#define BLANKNODE_TYPE_KEY QStringLiteral("blanknode")
#define VARIABLE_TYPE_KEY QStringLiteral("variable")
#define VALUE_KEY QStringLiteral("value")

QJsonValue Subject::toJsonValue() const
{
  QJsonObject json_object;
  switch(d->type)
  {
  case Subject::Type::Undefined:
    json_object[TYPE_KEY] = UNDEFINED_TYPE_KEY;
    break;
  case Subject::Type::Uri:
    json_object[TYPE_KEY] = URI_TYPE_KEY;
    json_object[VALUE_KEY] = (QString)d->uri();
    break;
  case Subject::Type::BlankNode:
    json_object[TYPE_KEY] = BLANKNODE_TYPE_KEY;
    json_object[VALUE_KEY] = d->blankNode().uuid().toString();
    break;
  case Subject::Type::Variable:
    json_object[TYPE_KEY] = VARIABLE_TYPE_KEY;
    json_object[VALUE_KEY] = d->variableName();
    break;
  }
  return json_object;
}

cres_qresult<Subject> Subject::fromJsonValue(const QJsonValue& _value)
{
  if(_value.isObject())
  {
    QJsonObject json_object = _value.toObject();
    QString type = json_object.value(TYPE_KEY).toString();

    if(type == UNDEFINED_TYPE_KEY)
    {
      return cres_success(Subject());
    }
    else if(type == URI_TYPE_KEY)
    {
      return cres_success(Subject(json_object.value(VALUE_KEY).toString(), Type::Uri));
    }
    else if(type == BLANKNODE_TYPE_KEY)
    {
      return cres_success(
        Subject(BlankNode(QUuid::fromString(json_object.value(VALUE_KEY).toString()))));
    }
    else if(type == VARIABLE_TYPE_KEY)
    {
      return cres_success(Subject(json_object.value(VALUE_KEY).toString(), Type::Variable));
    }

    return cres_failure("Unknow type '{}'", type);
  }
  else
  {
    return cres_failure("Expected object got '{}'", _value);
  }
}

QCborValue Subject::toCborValue() const
{
  QCborMap cbor_object;
  switch(d->type)
  {
  case Subject::Type::Undefined:
    cbor_object[TYPE_KEY] = UNDEFINED_TYPE_KEY;
    break;
  case Subject::Type::Uri:
    cbor_object[TYPE_KEY] = URI_TYPE_KEY;
    cbor_object[VALUE_KEY] = (QString)d->uri();
    break;
  case Subject::Type::BlankNode:
    cbor_object[TYPE_KEY] = BLANKNODE_TYPE_KEY;
    cbor_object[VALUE_KEY] = d->blankNode().uuid().toRfc4122();
    break;
  case Subject::Type::Variable:
    cbor_object[TYPE_KEY] = VARIABLE_TYPE_KEY;
    cbor_object[VALUE_KEY] = d->variableName();
    break;
  }
  return cbor_object;
}

cres_qresult<Subject> Subject::fromCborValue(const QCborValue& _value)
{
  if(_value.isMap())
  {
    QCborMap cbor_map = _value.toMap();
    QString type = cbor_map.value(TYPE_KEY).toString();

    if(type == UNDEFINED_TYPE_KEY)
    {
      return cres_success(Subject());
    }
    else if(type == URI_TYPE_KEY)
    {
      return cres_success(Subject(cbor_map.value(VALUE_KEY).toString(), Type::Uri));
    }
    else if(type == BLANKNODE_TYPE_KEY)
    {
      return cres_success(
        Subject(BlankNode(QUuid::fromRfc4122(cbor_map.value(VALUE_KEY).toByteArray()))));
    }
    else if(type == VARIABLE_TYPE_KEY)
    {
      return cres_success(Subject(cbor_map.value(VALUE_KEY).toString(), Type::Variable));
    }

    return cres_failure("Unknow type '{}'", type);
  }
  else
  {
    return cres_failure("Expected object got '{}'", _value);
  }
}

uint knowRDF::qHash(const knowRDF::Subject& key, uint seed)
{
  switch(key.type())
  {
  case Subject::Type::Undefined:
    return seed;
  case Subject::Type::Uri:
    return qHash(key.uri(), seed);
  case Subject::Type::BlankNode:
    return qHash(key.blankNode(), seed);
  case Subject::Type::Variable:
    return qHash("?" + key.variableName(), seed);
  }
  return seed;
}
