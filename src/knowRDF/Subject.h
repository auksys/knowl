#pragma once

#include <QSharedDataPointer>

#include <clog_qt>
#include <cres_qt>

#include <knowCore/Uris/Uris.h>

#include "Forward.h"

class QString;

namespace knowRDF
{
  /**
   * Represents a \ref Subject (either \ref knowCore::Uri or \ref BlankNode) in an RDF triple
   * definition.
   */
  class Subject
  {
  public:
    enum class Type
    {
      Undefined,
      Uri,
      BlankNode,
      Variable
    };
  public:
    Subject();
    Subject(const QString& _string, Type _type);
    Subject(const knowCore::Uri& _uri);
    Subject(const knowCore::Uri& _base, const QString& _suffix);
    Subject(const BlankNode& _blankNodeRef);
    Subject(const Subject& _rhs);
    /**
     * Create a \ref Subject from a Uri Ontology Definition.
     */
    template<typename _T_>
      requires(knowCore::Uris::IsUriDefinitionV<_T_>)
    Subject(const _T_& _t) : Subject(knowCore::Uri(_t))
    {
    }
    Subject& operator=(const Subject& _rhs);
    ~Subject();
    bool operator==(const Subject& _rhs) const;
    Type type() const;
    knowCore::Uri uri() const;
    BlankNode blankNode() const;
    QString variableName() const;
    QByteArray md5() const;
    QJsonValue toJsonValue() const;
    static cres_qresult<Subject> fromJsonValue(const QJsonValue& _value);
    QCborValue toCborValue() const;
    static cres_qresult<Subject> fromCborValue(const QCborValue& _value);
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
  uint qHash(const Subject& key, uint seed = 0);
} // namespace knowRDF

clog_format_declare_formatter(knowRDF::Subject)
{
  switch(p.type())
  {
  case knowRDF::Subject::Type::Undefined:
    break;
  case knowRDF::Subject::Type::Uri:
    return format_to(ctx.out(), "uri({})", p.uri());
  case knowRDF::Subject::Type::BlankNode:
    return format_to(ctx.out(), "blank_node({})", p.blankNode());
  case knowRDF::Subject::Type::Variable:
    return format_to(ctx.out(), "variable(?{})", p.variableName());
  }
  return format_to(ctx.out(), "undefined()");
}
