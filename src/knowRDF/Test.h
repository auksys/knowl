#include "Literal.h"
#include "Object.h"
#include "Subject.h"
#include "Triple.h"

#include <knowCore/Test.h>

namespace knowRDF
{
  inline char* toString(const knowRDF::Literal& t)
  {
    return QTest::toString(clog_qt::to_qstring(t));
  }
  inline char* toString(const knowRDF::Object& t)
  {
    return QTest::toString(clog_qt::to_qstring(t));
  }
  inline char* toString(const knowRDF::Subject& t)
  {
    return QTest::toString(clog_qt::to_qstring(t));
  }
  inline char* toString(const knowRDF::Triple& t)
  {
    return QTest::toString(clog_qt::to_qstring(t));
  }
} // namespace knowRDF
