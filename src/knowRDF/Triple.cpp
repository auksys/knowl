/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "Triple.h"

#include <Cyqlops/Crypto/Hash.h>
#include <QCborMap>
#include <QJsonObject>
#include <QVariant>

#include <knowCore/Uri.h>

#include "BlankNode.h"
#include "Object.h"
#include "Subject.h"

using namespace knowRDF;

struct Triple::Private : public QSharedData
{
  Subject subject;
  knowCore::Uri predicate;
  Object object;
  mutable QByteArray md5;
};

Triple::Triple() : d(new Private) {}

Triple::Triple(const Subject& _subject, const knowCore::Uri& _predicate, const Object& _value)
    : d(new Private)
{
  d->subject = _subject;
  d->predicate = _predicate;
  d->object = _value;
}

Triple::Triple(const Triple& _rhs) : d(_rhs.d) {}

Triple& Triple::operator=(const Triple& _rhs)
{
  d = _rhs.d;
  return *this;
}

Triple::~Triple() {}

knowCore::Uri Triple::predicate() const { return d->predicate; }

Subject Triple::subject() const { return d->subject; }

Object Triple::object() const { return d->object; }

bool Triple::operator==(const Triple& _rhs) const
{
  return d->predicate == _rhs.d->predicate and d->subject == _rhs.d->subject
         and d->object == _rhs.d->object;
}

cres_qresult<QByteArray> Triple::md5() const
{
  if(d->md5.isEmpty())
  {
    QCryptographicHash hash(QCryptographicHash::Md5);
    hash.addData(subject().md5());
    hash.addData(((QString)predicate()).toUtf8());
    cres_try(QByteArray object_md5, object().md5());
    hash.addData(object_md5);
    d->md5 = hash.result();
  }
  return cres_success(d->md5);
}

uint knowRDF::qHash(const knowRDF::Triple& key, uint seed)
{
  auto const [success, hash, errorMessage] = key.md5();
  if(success)
  {
    return qHash(hash.value(), seed);
  }
  else
  {
    clog_warning("Failed to computed md5 for triple: {}", errorMessage.value());
    return ::qHash(0, seed);
  }
}

#include <knowCore/MetaTypeImplementation.h>
#include <knowCore/Uris/askcore_datatype.h>

#define SUBJECT_KEY u8"subject"_kCs
#define PREDICATE_KEY u8"predicate"_kCs
#define OBJECT_KEY u8"object"_kCs

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(Triple)
cres_qresult<QByteArray> md5(const Triple& _value) const override { return _value.md5(); }
cres_qresult<QJsonValue> toJsonValue(const Triple& _value,
                                     const SerialisationContexts& _contexts) const override
{
  QJsonObject json_object;
  json_object[SUBJECT_KEY] = _value.subject().toJsonValue();
  json_object[PREDICATE_KEY] = (QString)_value.predicate();
  cres_try(QJsonValue object, _value.object().toJsonValue(_contexts));
  json_object[OBJECT_KEY] = object;
  return cres_success(json_object);
}
cres_qresult<void> fromJsonValue(Triple* _value, const QJsonValue& _json_value,
                                 const knowCore::DeserialisationContexts& _contexts) const override
{
  if(_json_value.isObject())
  {
    QJsonObject json_object = _json_value.toObject();
    cres_try(Subject subject, Subject::fromJsonValue(json_object[SUBJECT_KEY]));
    knowCore::Uri predicate = json_object[PREDICATE_KEY].toString();
    cres_try(Object object, Object::fromJsonValue(json_object[OBJECT_KEY], _contexts));
    *_value = Triple(subject, predicate, object);
    return cres_success();
  }
  else
  {
    return expectedError("object", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const Triple& _value,
                                     const SerialisationContexts& _contexts) const override
{
  QCborMap cbor_map;
  cbor_map[SUBJECT_KEY] = _value.subject().toCborValue();
  cbor_map[PREDICATE_KEY] = (QString)_value.predicate();
  cres_try(QCborValue object, _value.object().toCborValue(_contexts));
  cbor_map[OBJECT_KEY] = object;
  return cres_success(cbor_map);
}
cres_qresult<void> fromCborValue(Triple* _value, const QCborValue& _cbor_value,
                                 const knowCore::DeserialisationContexts& _contexts) const override
{
  if(_cbor_value.isMap())
  {
    QCborMap cbor_map = _cbor_value.toMap();
    cres_try(Subject subject, Subject::fromCborValue(cbor_map[SUBJECT_KEY]));
    knowCore::Uri predicate = cbor_map[PREDICATE_KEY].toString();
    cres_try(Object object, Object::fromCborValue(cbor_map[OBJECT_KEY], _contexts));
    *_value = Triple(subject, predicate, object);
    return cres_success();
  }
  else
  {
    return expectedError("map", _cbor_value);
  }
}
cres_qresult<QString> printable(const Triple& _value) const override
{
  return cres_success(clog_qt::to_qstring(_value));
}
cres_qresult<QString> toRdfLiteral(const Triple& _value,
                                   const SerialisationContexts& _contexts) const override
{
  return toJsonString(_value, _contexts);
}
cres_qresult<void> fromRdfLiteral(Triple* _value, const QString& _serialised,
                                  const knowCore::DeserialisationContexts& _contexts) const override
{
  return fromJsonString(_value, _serialised, _contexts);
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(Triple)

KNOWCORE_DEFINE_METATYPE(knowRDF::Triple, knowCore::Uris::askcore_datatype::triple,
                         knowCore::MetaTypeTraits::DebugStreamOperator)
