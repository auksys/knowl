/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#ifndef _KNOWCORE_RDF_TRIPLE_H_
#define _KNOWCORE_RDF_TRIPLE_H_

#include <clog_qt>
#include <cres_qt>

#include <QSharedDataPointer>

#include "Forward.h"

namespace knowRDF
{
  class Triple
  {
  public:
    Triple();
    Triple(const Subject& _subject, const knowCore::Uri& _predicate, const Object& _value);
    Triple(const Triple& _rhs);
    Triple& operator=(const Triple& _rhs);
    ~Triple();
    Subject subject() const;
    knowCore::Uri predicate() const;
    Object object() const;
    bool operator==(const Triple& _triple) const;
    cres_qresult<QByteArray> md5() const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
  uint qHash(const Triple& key, uint seed);
} // namespace knowRDF

#include <knowCore/Formatter.h>

#include "Object.h"
#include "Subject.h"

clog_format_declare_formatter(knowRDF::Triple)
{
  return format_to(ctx.out(), "[{} {} {}]", p.subject(), p.predicate(), p.object());
}

#include <knowCore/MetaType.h>
KNOWCORE_DECLARE_FULL_METATYPE(knowRDF, Triple);

#endif
