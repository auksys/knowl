#include "TripleStore.h"

#include <QSet>

#include "Triple.h"

using namespace knowRDF;

struct TripleStore::Private
{
  QSet<Triple> triples;
};

TripleStore::TripleStore() : d(new Private) {}

TripleStore::~TripleStore() { delete d; }

TripleStore::TripleStore(const TripleStore& _rhs) : d(new Private(*_rhs.d)) {}

TripleStore& TripleStore::operator=(const TripleStore& _rhs)
{
  *d = *_rhs.d;
  return *this;
}

bool TripleStore::isEmpty() const { return d->triples.isEmpty(); }

void TripleStore::addTriples(const QList<Triple>& _triples)
{
  for(const Triple& t : _triples)
  {
    triple(t);
  }
}

void TripleStore::triple(const Triple& _triple) { d->triples.insert(_triple); }

void TripleStore::removeTriple(const Triple& _triple) { d->triples.remove(_triple); }

bool TripleStore::hasTriple(const Triple& _triple) { return d->triples.contains(_triple); }

QList<Triple> TripleStore::triples() const
{
#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
  return d->triples.toList();
#else
  return d->triples.values();
#endif
}
