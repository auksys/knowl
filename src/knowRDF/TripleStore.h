#include "TripleStreamListener.h"

#include <QList>

namespace knowRDF
{
  class Triple;
  class TripleStore : public TripleStreamListener
  {
  public:
    TripleStore();
    ~TripleStore();
    TripleStore(const TripleStore& _rhs);
    TripleStore& operator=(const TripleStore& _rhs);
  public:
    bool isEmpty() const;
    void triple(const Triple& _triple) override;
    void addTriples(const QList<Triple>& _triples);
    void removeTriple(const Triple& _triple);
    QList<Triple> triples() const;
    bool hasTriple(const Triple& _triple);
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowRDF
