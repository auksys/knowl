/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "TripleStream.h"

#include <knowCore/Messages.h>
#include <knowCore/Uri.h>

#include "TripleStreamAdaptor_p.h"
#include "Turtle/Lexer_p.h"
#include "Turtle/Parser_p.h"

using namespace knowRDF;

struct TripleStream::Private
{
  TripleStreamAdaptor adaptor;
  knowCore::Uri base;
};

TripleStream::TripleStream() : d(new Private) {}

TripleStream::~TripleStream() { delete d; }

void TripleStream::addListener(TripleStreamListener* _adaptor) { d->adaptor.addListener(_adaptor); }

void TripleStream::setBase(const knowCore::Uri& _uri) { d->base = _uri; }

cres_qresult<void> TripleStream::start(QIODevice* _device, knowCore::Messages* _messages,
                                       const QString& _format)
{
  Q_UNUSED(_format)
  Turtle::Lexer l(_device);
  Turtle::Parser p(&l);
  p.setBase(d->base);
  bool v = p.parse(&d->adaptor);

  if(_messages)
  {
    *_messages = p.messages();
  }
  if(v)
  {
    return cres_success();
  }
  else
  {
    return cres_failure(p.messages().toString());
  }
}
