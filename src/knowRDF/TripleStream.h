#pragma once

/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include <knowCore/FileFormat.h>
#include <knowRDF/Forward.h>

class QIODevice;

namespace knowRDF
{
  /**
   * @ingroup knowRDF
   *
   * This class is used to parse a RDF graph from a \ref QIODevice using \ref start function and
   * forwarding the triples to a listener \ref TripleStreamListener.
   */
  class TripleStream
  {
  public:
    TripleStream();
    ~TripleStream();
    void setBase(const knowCore::Uri& _uri);
    void addListener(TripleStreamListener* _listener);
    /**
     * Start parsing triples from \p _device.
     * @return false if parsing failed
     */
    cres_qresult<void> start(QIODevice* _device, knowCore::Messages* _messages = nullptr,
                             const QString& _format = knowCore::FileFormat::Turtle);
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowRDF
