/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "TripleStreamAdaptor_p.h"
#include "TripleStreamListener.h"

using namespace knowRDF;

TripleStreamAdaptor::TripleStreamAdaptor() {}

void TripleStreamAdaptor::addListener(TripleStreamListener* _listener)
{
  m_listeners.append(_listener);
}

void TripleStreamAdaptor::streamTriple(const Triple& _triple)
{
  for(TripleStreamListener* l : m_listeners)
  {
    l->triple(_triple);
  }
}
