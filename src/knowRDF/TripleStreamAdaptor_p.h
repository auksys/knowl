/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include <QList>

namespace knowRDF
{
  class Triple;
  class TripleStreamListener;
  class TripleStreamAdaptor
  {
  public:
    TripleStreamAdaptor();
    void addListener(TripleStreamListener* _listener);
    void streamTriple(const Triple& _triple);
  private:
    QList<TripleStreamListener*> m_listeners;
  };
} // namespace knowRDF
