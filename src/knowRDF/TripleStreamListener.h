/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#ifndef _KNOWCORE_RDF_TRIPLESTREAMLISTENER_H_
#define _KNOWCORE_RDF_TRIPLESTREAMLISTENER_H_

namespace knowRDF
{
  class Triple;
  class TripleStreamListener
  {
  public:
    virtual ~TripleStreamListener();
    virtual void triple(const Triple& _triple) = 0;
  };
} // namespace knowRDF

#endif
