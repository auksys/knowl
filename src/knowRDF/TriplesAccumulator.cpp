#include "TriplesAccumulator.h"

#include <QList>

#include "Triple.h"

using namespace knowRDF;

struct TriplesAccumulator::Private : public QList<Triple>
{
};

TriplesAccumulator::TriplesAccumulator() : d(new Private) {}

TriplesAccumulator::~TriplesAccumulator() { delete d; }

void TriplesAccumulator::triple(const Triple& _triple) { d->append(_triple); }

QList<Triple> TriplesAccumulator::triples() { return *d; }
