#pragma once

#include "TripleStreamListener.h"

template<typename T>
class QList;

namespace knowRDF
{
  /**
   * This class implements a @ref TripleStreamListener which accumulates triples in a list.
   */
  class TriplesAccumulator : public TripleStreamListener
  {
  public:
    TriplesAccumulator();
    ~TriplesAccumulator();
    QList<Triple> triples();
    void triple(const Triple& _triple) override;
  private:
    struct Private;
    Private* const d;
  };

} // namespace knowRDF
