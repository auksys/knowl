#include "TriplesSerialiser.h"

#include "Turtle/Writer_p.h"

using namespace knowRDF;

struct TriplesSerialiser::Private
{
  Turtle::Writter* writter = nullptr;
};

TriplesSerialiser::TriplesSerialiser() : d(new Private) {}

TriplesSerialiser::~TriplesSerialiser() {}

bool TriplesSerialiser::start(QIODevice* _device, const QString& _format)
{
  if(_format != knowCore::FileFormat::Turtle)
    return false;
  d->writter = new Turtle::Writter(_device);
  return true;
}

void TriplesSerialiser::triple(const Triple& _triple) { d->writter->write(_triple); }

void TriplesSerialiser::finish() { delete d->writter; }
