#include "TripleStreamListener.h"

#include <knowCore/FileFormat.h>

class QIODevice;

namespace knowRDF
{
  class TriplesSerialiser : public TripleStreamListener
  {
  public:
    TriplesSerialiser();
    ~TriplesSerialiser();
    bool start(QIODevice* _device, const QString& _format = knowCore::FileFormat::Turtle);
    void triple(const knowRDF::Triple& _triple) override;
    void finish();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowRDF
