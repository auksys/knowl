#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QUrl>

#include <knowCore/BigNumber.h>
#include <knowCore/Messages.h>
#include <knowCore/Uri.h>
#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/xsd.h>
#include <knowCore/ValueHash.h>

#include "../BlankNode.h"
#include "../Object.h"
#include "../Subject.h"
#include "../Triple.h"

namespace knowRDF
{
  namespace Turtle
  {
    /**
     * Base parser shared between Turtle and kDB::RDFView. It is private API, that is subject to
     * changes, do not use outside of knowL and kDB.
     * @internal
     */
    template<typename _Base_, typename _Lexer_, typename _Token_, bool _has_variable_>
    struct BaseParser : public _Base_
    {
      _Lexer_* lexer;
      knowCore::Messages messages;
      knowCore::UriManager urlManager;

      QHash<QString, BlankNode> blankNodes;
      knowCore::ValueHash bindings;

      _Token_ currentToken;
      QList<_Token_> backToken;

      void reportError(const _Token_& _token, const QString& _errorMsg)
      {
        messages.reportError(_errorMsg, _token.line);
      }

      void reportUnexpected(const _Token_& _token)
      {
        reportError(_token, clog_qt::qformat("Unexpected '{}'.", _token.toString()));
      }

      void reportUnexpected(const _Token_& _token, typename _Token_::Type _expectedType)
      {
        reportError(_token,
                    clog_qt::qformat("Expected '{}' before '{}'.",
                                     _Token_::typeToString(_expectedType), _token.toString()));
      }

      bool isOfType(const _Token_& _token, typename _Token_::Type _type)
      {
        if(_token.type == _type)
          return true;
        reportUnexpected(_token, _type);
        return false;
      }

      const _Token_& getNextToken()
      {
        if(backToken.isEmpty())
        {
          return (currentToken = lexer->nextToken());
        }
        else
        {
          return (currentToken = backToken.takeLast());
        }
      }
      void pushBackToken(const _Token_& _new_current)
      {
        backToken.append(currentToken);
        currentToken = _new_current;
      }

      knowCore::Uri parseIri()
      {
        knowCore::Uri r;
        switch(currentToken.type)
        {
        case _Token_::CURIE_CONSTANT:
        {
          if(currentToken.curie.canResolve(&urlManager))
          {
            r = currentToken.curie.resolve(&urlManager);
          }
          else
          {
            reportError(currentToken,
                        clog_qt::qformat("Unknown curie prefix '{}'", currentToken.curie.prefix()));
          }
          getNextToken();
          break;
        }
        case _Token_::URI_CONSTANT:
          r = urlManager.base().resolved(currentToken.string);
          getNextToken();
          break;
        default:
          reportUnexpected(currentToken);
          getNextToken();
        }
        return r;
      }

      BlankNode parseBlankNode()
      {
        if(isOfType(currentToken, _Token_::UNDERSCORECOLON))
        {
          getNextToken();

          QString label;

          switch(currentToken.type)
          {
          case _Token_::IDENTIFIER:
            label = currentToken.string;
            getNextToken();
            break;
          case _Token_::A:
            label = "a";
            getNextToken();
            break;
          case _Token_::INTEGER_CONSTANT:
          case _Token_::FLOAT_CONSTANT:
            label = currentToken.string;
            getNextToken();
            if(currentToken.type == _Token_::IDENTIFIER)
            {
              label += currentToken.string;
              getNextToken();
            }
            break;
          default:
            reportUnexpected(currentToken);
            return BlankNode();
          }
          if(blankNodes.contains(label))
          {
            return blankNodes.value(label);
          }
          else
          {
            BlankNode bn(label);
            blankNodes[label] = bn;
            return bn;
          }
        }
        return BlankNode();
      }

      Subject parseSubject()
      {
        switch(currentToken.type)
        {
        case _Token_::STARTBOXBRACKET:
        {
          getNextToken();
          isOfType(currentToken, _Token_::ENDBOXBRACKET);
          getNextToken();
          return Subject(BlankNode());
        }
        case _Token_::QUESTION:
          if(_has_variable_)
          {
            getNextToken();
            if(currentToken.type != _Token_::IDENTIFIER and currentToken.type != _Token_::A)
            {
              reportUnexpected(currentToken);
            }
            QString name = currentToken.type == _Token_::IDENTIFIER ? currentToken.string : "a";
            getNextToken();
            return Subject(name, Subject::Type::Variable);
          }
          else
          {
            return parseIri();
          }
        case _Token_::UNDERSCORECOLON:
          return parseBlankNode();
        default:
          return parseIri();
        }
      }

      knowCore::Uri parsePredicate()
      {
        switch(currentToken.type)
        {
        case _Token_::A:
          getNextToken();
          return knowCore::Uris::rdf::a;
        default:
          return parseIri();
        }
      }

      void parseObject(const Subject& _subject, const knowCore::Uri& _predicate)
      {
        switch(currentToken.type)
        {
        case _Token_::QUESTION:
          if(_has_variable_)
          {
            getNextToken();
            if(currentToken.type != _Token_::IDENTIFIER and currentToken.type != _Token_::A)
            {
              reportUnexpected(currentToken);
            }
            QString name = currentToken.type == _Token_::IDENTIFIER ? currentToken.string : "a";
            getNextToken();
            appendTriple(_subject, _predicate, Object(name, Object::Type::Variable));
          }
          else
          {
            reportUnexpected(currentToken);
            getNextToken();
          }
          break;
        case _Token_::UNDERSCORECOLON:
        {
          appendTriple(_subject, _predicate, parseBlankNode());
          break;
        }
        case _Token_::CURIE_CONSTANT:
        case _Token_::URI_CONSTANT:
        {
          appendTriple(_subject, _predicate, parseIri());
          break;
        }
        case _Token_::STRING_CONSTANT:
        {
          QString str = currentToken.string;
          getNextToken();
          knowCore::Uri literal_uri = knowCore::Uris::xsd::string;
          if(currentToken.type == _Token_::CIRCUMFLEXCIRCUMFLEX)
          {
            getNextToken();
            literal_uri = parseIri();
          }
          switch(currentToken.type)
          {
          case _Token_::LANG_TAG:
          {
            appendTriple(_subject, _predicate, literal_uri, str, currentToken.string);
            getNextToken();
            break;
          }
          default:
          {
            appendTriple(_subject, _predicate, literal_uri, str, QString());
            break;
          }
          }
          break;
        }
        case _Token_::TRUE:
          appendTriple(_subject, _predicate, knowCore::Uris::xsd::boolean, true);
          getNextToken();
          break;
        case _Token_::FALSE:
          appendTriple(_subject, _predicate, knowCore::Uris::xsd::boolean, false);
          getNextToken();
          break;
        case _Token_::INTEGER_CONSTANT:
          appendTriple(_subject, _predicate, knowCore::Uris::xsd::integer,
                       knowCore::BigNumber::fromString(currentToken.string).expect_success());
          getNextToken();
          break;
        case _Token_::FLOAT_CONSTANT:
          appendTriple(_subject, _predicate,
                       knowCore::Uris::select(currentToken.string.indexOf('e') == -1,
                                              knowCore::Uris::xsd::decimal,
                                              knowCore::Uris::xsd::float64),
                       knowCore::BigNumber::fromString(currentToken.string).expect_success());
          getNextToken();
          break;
        case _Token_::STARTBOXBRACKET:
        {
          getNextToken();

          BlankNode bn;

          if(currentToken.type != _Token_::ENDBOXBRACKET)
          {
            parseSingleSubject(bn, _Token_::ENDBOXBRACKET);
            appendTriple(_subject, _predicate, bn);
          }
          else
          {
            appendTriple(_subject, _predicate, bn);
            getNextToken();
          }

          break;
        }
        case _Token_::STARTBRACKET:
        {
          getNextToken();

          if(currentToken.type == _Token_::ENDBRACKET)
          {
            appendTriple(_subject, _predicate, knowCore::Uris::rdf::nil);
          }
          else
          {

            BlankNode bn;

            appendTriple(_subject, _predicate, bn);

            while(currentToken.type != _Token_::END_OF_FILE)
            {
              parseObject(bn, knowCore::Uris::rdf::first);

              if(currentToken.type == _Token_::ENDBRACKET)
              {
                appendTriple(bn, knowCore::Uris::rdf::rest, knowCore::Uris::rdf::nil);
                break;
              }
              else
              {
                BlankNode nbn;
                appendTriple(bn, knowCore::Uris::rdf::rest, nbn);
                bn = nbn;
              }
            }
          }
          getNextToken();
          break;
        }
        case _Token_::LOAD_FILE:
        {
          getNextToken();
          if(not isOfType(currentToken, _Token_::STARTBRACKET))
            return;
          getNextToken(); // eats '('
          if(not isOfType(currentToken, _Token_::STRING_CONSTANT))
            return;
          QString filename = currentToken.string;

          QUrl base = QUrl(urlManager.base());
          QString base_fn = base.scheme() == "qrc" ? ":" + base.path() : base.toLocalFile();
          QString fn = QFileInfo(base_fn).dir().absolutePath() + "/" + filename;
          QFile f(fn);
          if(f.open(QIODevice::ReadOnly))
          {
            appendTriple<QString>(_subject, _predicate, knowCore::Uris::xsd::string,
                                  QString::fromUtf8(f.readAll()), QString());
          }
          else
          {
            reportError(currentToken, clog_qt::qformat("Failed to open file '{}'", f.fileName()));
          }

          getNextToken(); // eats filename
          if(not isOfType(currentToken, _Token_::ENDBRACKET))
            return;
          getNextToken(); // eats ')'
          break;
        }
        default:
          reportUnexpected(currentToken);
          getNextToken();
        }
      }

      void parseSingleSubject(const Subject& subject, const typename _Token_::Type& _endType)
      {
        while(currentToken.type != _Token_::END_OF_FILE)
        {
          knowCore::Uri predicate = parsePredicate();

          while(currentToken.type != _Token_::END_OF_FILE)
          {
            parseObject(subject, predicate);

            if(currentToken.type == _Token_::COMA)
            {
              getNextToken();
            }
            else
            {
              break;
            }
          }
          if(currentToken.type == _endType)
          {
            getNextToken();
            break;
          }
          else if(isOfType(currentToken, _Token_::SEMI))
          {
            getNextToken();
            if(currentToken.type == _endType)
            {
              getNextToken();
              break;
            }
          }
        }
      }
      using _Base_::appendTriple;
      template<typename _T_>
      void appendTriple(const Subject& _subject, const knowCore::Uri& _predicate,
                        const knowCore::Uri& _dataTypeUri, const _T_& _value,
                        const QString& _lang = QString())
      {
        const auto [success, object, message] = Object::fromValue(_dataTypeUri, _value, _lang);
        if(success)
        {
          this->appendTriple(Triple(_subject, _predicate, object.value()));
        }
        else
        {
          reportError(currentToken,
                      clog_qt::qformat("Failed to create object for value '{}' with error '{}'",
                                       _value, message.value()));
        }
      }
      void appendTriple(const Subject& _subject, const knowCore::Uri& _predicate,
                        const Object& _value)
      {
        this->appendTriple(Triple(_subject, _predicate, _value));
      }
      void appendTriple(const Subject& _subject, const knowCore::Uri& _predicate,
                        const knowCore::Uri& _dataTypeUri, const QString& _value,
                        const QString& _lang)
      {
        const auto [success, lit, message] = Literal::fromRdfLiteral(_dataTypeUri, _value, _lang);
        if(success)
        {
          appendTriple(_subject, _predicate, lit.value());
        }
        else
        {
          reportError(
            currentToken,
            clog_qt::qformat("Failed to parse RDF literal '{}' of type '{}' with error '{}'",
                             _value, _dataTypeUri, message.value()));
        }
      }
    };
  } // namespace Turtle
} // namespace knowRDF
