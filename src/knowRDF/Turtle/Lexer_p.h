/*
 *  Copyright (c) 2008,2010,2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#ifndef _TURTLE_LEXER_BASE_H_
#define _TURTLE_LEXER_BASE_H_

class QIODevice;

#include "Token_p.h"
#include <knowCore/LexerTextStream.h>

namespace knowRDF
{
  namespace Turtle
  {
    /**
     * @internal
     * This class provide some base functions to lexers. Such as eating spaces,
     * or managing the flow.
     * @ingroup Cauchy
     */
    class Lexer
    {
    public:
      Lexer(QIODevice* _sstream);
      Lexer(const QString& _string);
      ~Lexer();
    public:
      void setCurieLexingEnabled(bool _v);
      bool isCurieLexingEnabled() const;
      Token nextToken();
    protected:
      /**
       * Get an identifier (or keyword) in the current flow of character.
       */
      QString getIdentifier(knowCore::LexerTextStream::Element lastChar);
      Token getDigit(knowCore::LexerTextStream::Element firstChar);
      Token getString(int terminator, Token::Type _type, bool _tripleEnding);
      bool isTriple(knowCore::LexerTextStream::Element _char);
    private:
      struct Private;
      Private* const d;
    };
  } // namespace Turtle
} // namespace knowRDF

#endif
