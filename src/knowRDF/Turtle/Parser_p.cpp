/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "Parser_p.h"

#include <iostream>

#include <QVariant>

#include <knowCore/TypeDefinitions.h>

#include "../TripleStreamAdaptor_p.h"

#include "BaseParser_p.h"
#include "Lexer_p.h"

using namespace knowRDF::Turtle;

struct Parser::PrivateBase
{
  TripleStreamAdaptor* stream;
  void appendTriple(const Triple& _triple) { stream->streamTriple(_triple); }
};

struct Parser::Private : public BaseParser<PrivateBase, Lexer, Token, false>
{
};

Parser::Parser(Lexer* _lexer) : d(new Private) { d->lexer = _lexer; }

Parser::~Parser() { delete d; }

const knowCore::Messages& Parser::messages() const { return d->messages; }

void Parser::setBase(const knowCore::Uri& _uri) { d->urlManager.setBase(_uri); }

bool Parser::parse(knowRDF::TripleStreamAdaptor* _stream)
{
  d->lexer->setCurieLexingEnabled(false);
  d->stream = _stream;
  d->getNextToken();

  if(d->currentToken.type == Token::BASE)
  {
    if(d->isOfType(d->getNextToken(), Token::URI_CONSTANT))
    {
      d->urlManager.setBase(d->currentToken.string);
      d->getNextToken();
    }
    else
    {
      return false;
    }
    if(d->currentToken.type == Token::DOT)
    {
      d->getNextToken();
    }
  }

  while(d->currentToken.type == Token::PREFIX)
  {
    switch(d->getNextToken().type)
    {
    case Token::IDENTIFIER:
    {
      QString ns = d->currentToken.string;
      d->isOfType(d->getNextToken(), Token::COLON);
      if(d->isOfType(d->getNextToken(), Token::URI_CONSTANT))
      {
        d->urlManager.addPrefix(ns, d->currentToken.string);
      }
      else
      {
        return false;
      }
      break;
    }
    case Token::COLON:
    {
      if(d->isOfType(d->getNextToken(), Token::URI_CONSTANT))
      {
        d->urlManager.addPrefix(QString(), d->currentToken.string);
        d->getNextToken();
      }
      else
      {
        return false;
      }
      break;
    }
    default:
      d->reportUnexpected(d->currentToken);
      return false;
    }
    d->lexer->setCurieLexingEnabled(true);
    d->getNextToken();
    if(d->currentToken.type == Token::DOT)
    {
      d->getNextToken();
    }
    d->lexer->setCurieLexingEnabled(false);
  }
  d->lexer->setCurieLexingEnabled(true);
  while(d->currentToken.type != Token::END_OF_FILE)
  {
    switch(d->currentToken.type)
    {
    case Token::STARTBOXBRACKET:
    {
      Token currentTokenBack = d->currentToken;
      if(d->getNextToken().type != Token::ENDBOXBRACKET)
      {
        d->parseSingleSubject(BlankNode(), Token::ENDBOXBRACKET);
        d->isOfType(d->currentToken, Token::DOT);
        d->getNextToken();
        break;
      }
      else
      {
        d->pushBackToken(currentTokenBack);
      }
    }
      Q_FALLTHROUGH();
    case Token::CURIE_CONSTANT:
    case Token::URI_CONSTANT:
    case Token::IDENTIFIER:
    case Token::UNDERSCORECOLON:
    case Token::COLON:
      d->parseSingleSubject(d->parseSubject(), Token::DOT);
      break;
    default:
      d->reportUnexpected(d->currentToken);
      d->getNextToken();
    }
  }
  d->isOfType(d->currentToken, Token::END_OF_FILE);
  return not d->messages.hasErrors();
}
