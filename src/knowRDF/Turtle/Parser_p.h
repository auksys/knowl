/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include <knowCore/Forward.h>

namespace knowRDF
{
  class TripleStreamAdaptor;
  namespace Turtle
  {
    class Lexer;
    class Parser
    {
    public:
      Parser(Lexer* _lexer);
      ~Parser();
      bool parse(TripleStreamAdaptor* _stream);
      const knowCore::Messages& messages() const;
      void setBase(const knowCore::Uri& _uri);
    private:
      struct PrivateBase;
      struct Private;
      Private* const d;
    };
  } // namespace Turtle
} // namespace knowRDF
