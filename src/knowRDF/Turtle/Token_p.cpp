/*
 *  Copyright (c) 2008,2010,2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "Token_p.h"

#include <clog_qt>

using namespace knowRDF::Turtle;

QString Token::typeToString(Type type)
{
  switch(type)
  {
    // Not really token
  case END_OF_FILE:
    return "end of file";
  case END_OF_LINE:
    return "end of line";
  case UNKNOWN:
    return "unknown token";
  case UNFINISHED_STRING:
    return "unfinished string";
    // Special characters
  case Token::SEMI:
    return ";";
  case Token::COLON:
    return ":";
  case Token::COMA:
    return ",";
  case Token::DOT:
    return ".";
  case Token::STARTBRACKET:
    return "(";
  case Token::ENDBRACKET:
    return ")";
  case Token::STARTBOXBRACKET:
    return "[";
  case Token::ENDBOXBRACKET:
    return "]";
  case Token::QUESTION:
    return "?";
  case Token::UNDERSCORECOLON:
    return "_:";
  case Token::CIRCUMFLEXCIRCUMFLEX:
    return "^^";
    // Constants
  case FLOAT_CONSTANT:
    return "float constant";
  case INTEGER_CONSTANT:
    return "integer constant";
  case STRING_CONSTANT:
    return "string constant";
  case URI_CONSTANT:
    return "uri constant";
  case IDENTIFIER:
    return "identifier";
  case LANG_TAG:
    return "language tag";
  case CURIE_CONSTANT:
    return "curie constant";
    // Keywords
  case BASE:
    return "@base";
  case PREFIX:
    return "@prefix";
  case A:
    return "a";
  case TRUE:
    return "true";
  case FALSE:
    return "false";
  case LOAD_FILE:
    return "@load_file";
  }
  return clog_qt::qformat("[Unknown token] ({})", (int)type);
}

Token::Token() : type(UNKNOWN), line(-1), column(-1) {}

Token::Token(const knowCore::Curie& _curie, int _line, int _column)
    : type(Type::CURIE_CONSTANT), line(_line), column(_column), curie(_curie)
{
}

Token::Token(Type _type, int _line, int _column) : type(_type), line(_line), column(_column) {}

Token::Token(Type _type, const QString& _string, int _line, int _column)
    : type(_type), line(_line), column(_column), string(_string)
{
  clog_assert(_type == STRING_CONSTANT or _type == IDENTIFIER or _type == INTEGER_CONSTANT
              or _type == FLOAT_CONSTANT or _type == LANG_TAG or _type == URI_CONSTANT
              or _type == UNFINISHED_STRING);
}

QString Token::toString() const
{
  QString str = typeToString(type);
  switch(type)
  {
  case Token::FLOAT_CONSTANT:
  case Token::STRING_CONSTANT:
  case Token::INTEGER_CONSTANT:
  case Token::URI_CONSTANT:
  case Token::IDENTIFIER:
  case Token::UNFINISHED_STRING:
    str += "(" + string + ")";
    break;
  default:
    break;
  }
  return str;
}
