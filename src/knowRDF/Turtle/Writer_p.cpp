#include "Writer_p.h"

#include <QIODevice>

#include "../Literal.h"
#include "../Object.h"
#include "../Subject.h"
#include "../Triple.h"

using namespace knowRDF::Turtle;

Writter::Writter(QIODevice* _device) : m_device(_device) {}

cres_qresult<void> Writter::write(const knowRDF::BlankNode& _blankNode)
{
  if(m_blankNodes.contains(_blankNode))
  {
    m_device->write(m_blankNodes[_blankNode].toUtf8());
  }
  else
  {
    QString label = clog_qt::qformat("_:{}", m_blankNodes.size());
    m_blankNodes[_blankNode] = label;
    m_device->write(label.toUtf8());
  }
  return cres_success();
}

cres_qresult<void> Writter::write(const knowRDF::Literal& _literal)
{
  m_device->write("\"");
  cres_try(QString str, _literal.toRdfLiteral());
  m_device->write(str.toUtf8());
  m_device->write("\"");
  m_device->write("^^");
  write(_literal.datatype());
  if(not _literal.lang().isEmpty())
  {
    m_device->write("@");
    m_device->write(_literal.lang().toUtf8());
  }
  return cres_success();
}

cres_qresult<void> Writter::write(const knowRDF::Subject& _subject)
{
  switch(_subject.type())
  {
  case knowRDF::Subject::Type::BlankNode:
    return write(_subject.blankNode());
  case knowRDF::Subject::Type::Uri:
    return write(_subject.uri());
  case knowRDF::Subject::Type::Undefined:
  case knowRDF::Subject::Type::Variable:
    return cres_failure("Cannot write undefined or variable subject.");
  }
  return cres_failure("Cannot write unknown type of subject.");
}

cres_qresult<void> Writter::write(const knowRDF::Object& _object)
{
  switch(_object.type())
  {
  case knowRDF::Object::Type::BlankNode:
    return write(_object.blankNode());
  case knowRDF::Object::Type::Uri:
    return write(_object.uri());
  case knowRDF::Object::Type::Literal:
    return write(_object.literal());
  case knowRDF::Object::Type::Undefined:
  case knowRDF::Object::Type::Variable:
    return cres_failure("Cannot write undefined or variable object.");
  }
  return cres_failure("Cannot write unknown type of subject.");
}

cres_qresult<void> Writter::write(const knowRDF::Triple& _triple)
{
  write(_triple.subject());
  m_device->write(" ");
  write(_triple.predicate());
  m_device->write(" ");
  write(_triple.object());
  m_device->write(" .\n");
  return cres_success();
}

cres_qresult<void> Writter::write(const knowCore::Uri& _uri)
{
  m_device->write("<");
  m_device->write(((QString)_uri).toUtf8());
  m_device->write(">");
  return cres_success();
}
