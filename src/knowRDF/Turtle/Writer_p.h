#include <knowCore/Uri.h>

#include "../BlankNode.h"

class QIODevice;

namespace knowRDF
{
  class Literal;
  class Object;
  class Subject;
  class Triple;
  namespace Turtle
  {
    class Writter
    {
    public:
      Writter(QIODevice* _device);
      cres_qresult<void> write(const BlankNode& _blankNode);
      cres_qresult<void> write(const Subject& _subject);
      cres_qresult<void> write(const Literal& _literal);
      cres_qresult<void> write(const Object& _object);
      cres_qresult<void> write(const Triple& _triple);
      cres_qresult<void> write(const knowCore::Uri& _uri);
    private:
      QIODevice* m_device;
      QHash<knowRDF::BlankNode, QString> m_blankNodes;
    };
  } // namespace Turtle
} // namespace knowRDF
