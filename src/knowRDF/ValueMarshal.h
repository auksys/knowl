#pragma once

#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Value.h>
#include <knowCore/ValueList.h>
#include <knowRDF/Literal.h>

namespace knowRDF
{
  template<typename _T_>
  struct ValueMarshal;

  template<typename _T_, typename _DO_NOT_SPECIALISED_ = void>
  cres_qresult<knowCore::Value> toValue(const _T_& _t)
  {
    return ValueMarshal<_T_>::convert(_t);
  }

  template<typename _T_>
  cres_qresult<_T_> fromValue(const knowCore::Value& _t)
  {
    return ValueMarshal<_T_>::convert(_t);
  }

  template<typename _T_>
  struct ValueMarshal
  {
    static cres_qresult<knowCore::Value> convert(const _T_& _t)
    {
      return cres_success(knowCore::Value::fromValue(_t));
    }
    static cres_qresult<_T_> convert(const knowCore::Value& _t) { return _t.value<_T_>(); }
  };
  template<>
  struct ValueMarshal<knowCore::Value>
  {
    static cres_qresult<knowCore::Value> convert(const knowCore::Value& _t)
    {
      return cres_success(_t);
    }
  };
  template<typename _T_>
  struct ValueMarshal<QList<_T_>>
  {
    static cres_qresult<knowCore::Value> convert(const QList<_T_>& _t)
    {
      QList<knowCore::Value> values;
      for(const _T_& st : _t)
      {
        cres_try(knowCore::Value value, toValue(st));
        values.append(value);
      }
      return cres_success(knowCore::Value::fromValue(knowCore::ValueList(values)));
    }
    static cres_qresult<QList<_T_>> convert(const knowCore::Value& _t)
    {
      QList<_T_> list;
      cres_try(knowCore::ValueList value_list, _t.toList());
      for(const knowCore::Value val : value_list.values())
      {
        cres_try(_T_ val_t, fromValue<_T_>(val));
        list.append(val_t);
      }
      return cres_success(list);
    }
  };
} // namespace knowRDF
