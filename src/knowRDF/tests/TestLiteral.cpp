#include "TestLiteral.h"

template<typename T1, typename T2>
inline bool tqCompare(const T1& t1, const T2& t2, const char* actual, const char* expected,
                      const char* file, int line)
{
  return QTest::compare_helper(
    t1 == t2, "Compared values are not the same", [&t1] { return QTest::toString(t1); },
    [&t2] { return QTest::toString(t2); }, actual, expected, file, line);
}

#include <knowCore/BigNumber.h>
#include <knowCore/Test.h>
#include <knowCore/Timestamp.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/Uris/xsd.h>

#include "../Literal.h"

// template<>
bool operator==(const cres_qresult<unsigned long long>& _lhs, const unsigned long long& _rhs)
{
  return _lhs.is_successful() and _lhs.get_value() == _rhs;
}

void TestLiteral::testFromValue()
{
  using namespace knowRDF;

  Literal lit = Literal::fromValue(u8"test"_kCs);

  QVERIFY(lit.value<QString>() == QStringLiteral("test"));

  lit = Literal::fromValue(1432716988641976118ull);
  lit.value<unsigned long long>() == 1432716988641976118ull;

  QCOMPARE(lit.value<unsigned long long>(), 1432716988641976118ull);

  lit = CRES_QVERIFY(Literal::fromValue(knowCore::Uris::xsd::integer, 1432716988641976118ull));

  knowCore::BigNumber val_bn = CRES_QVERIFY(lit.value<knowCore::BigNumber>());
  quint64 val_uint64 = CRES_QVERIFY(val_bn.toUInt64());
  QCOMPARE(val_uint64, 1432716988641976118);
  val_uint64 = CRES_QVERIFY(lit.value<unsigned long long>());
  QCOMPARE(val_uint64, 1432716988641976118ull);
}

void TestLiteral::testDateLiteral()
{
  knowRDF::Literal lit_dt_1 = CRES_QVERIFY(
    knowRDF::Literal::fromRdfLiteral(knowCore::Uris::xsd::dateTime, "1001 ns"_kCs, "en"));
  knowRDF::Literal lit_dt_1_s
    = CRES_QVERIFY(knowRDF::Literal::fromValue(knowCore::Uris::xsd::dateTime, "1001"_kCs, "en"));
  knowRDF::Literal lit_dt_2 = CRES_QVERIFY(knowRDF::Literal::fromValue(
    knowCore::Uris::xsd::dateTime, quint64(1001), "en", knowCore::TypeCheckingMode::Force));

  QCOMPARE(lit_dt_1.toRdfLiteral().expect_success(), lit_dt_2.toRdfLiteral().expect_success());
  QCOMPARE(lit_dt_1, lit_dt_2);
  QVERIFY(lit_dt_1_s != lit_dt_2);
  QVERIFY(lit_dt_1_s != lit_dt_1);

  knowCore::Timestamp dt = CRES_QVERIFY(lit_dt_1.value<knowCore::Timestamp>());
  QCOMPARE(dt.toEpoch<knowCore::NanoSeconds>().count(), 1001);
}

void TestLiteral::testFromVariant()
{
  using namespace knowRDF;

  Literal lit = CRES_QVERIFY(Literal::fromVariant("url"_kCu, QVariant("hello")));
  QVERIFY(not lit.isEmpty());
  CRES_QVERIFY_FAILURE(Literal::fromVariant("url"_kCu, QVariant(1432716988641976118ull)));

  lit = CRES_QVERIFY(
    Literal::fromVariant(knowCore::Uris::xsd::integer, QVariant(1432716988641976118ull)));

  knowCore::BigNumber val_bn = CRES_QVERIFY(lit.value<knowCore::BigNumber>());
  quint64 val_uint64 = CRES_QVERIFY(val_bn.toUInt64());
  QCOMPARE(val_uint64, 1432716988641976118);
  val_uint64 = CRES_QVERIFY(lit.value<unsigned long long>());
  QCOMPARE(val_uint64, 1432716988641976118ull);

  lit = CRES_QVERIFY(Literal::fromVariant(u8"test"_kCs));
  QString val_string = CRES_QVERIFY(lit.value<QString>());
  QCOMPARE(val_string, u8"test"_kCs);
  lit = CRES_QVERIFY(Literal::fromVariant("test"));
  val_string = CRES_QVERIFY(lit.value<QString>());
  QCOMPARE(val_string, u8"test"_kCs);
}

void TestLiteral::testFromString()
{
  using namespace knowRDF;

  Literal lit = CRES_QVERIFY(Literal::fromVariant(knowCore::Uris::xsd::integer, QVariant("10")));
  QCOMPARE(lit.datatype(), knowCore::Uris::xsd::integer);
  knowCore::BigNumber bn = CRES_QVERIFY(lit.value<knowCore::BigNumber>());
  QCOMPARE(bn.toString(), u8"10"_kCs);
  qint64 val_int64 = CRES_QVERIFY(bn.toInt64());
  QCOMPARE(val_int64, 10ll);
  val_int64 = CRES_QVERIFY(lit.value<qint64>());
  QCOMPARE(val_int64, 10ll);
  lit = CRES_QVERIFY(Literal::fromValue(knowCore::Uris::xsd::integer, u8"10"_kCs));
  val_int64 = CRES_QVERIFY(lit.value<qint64>());
  QCOMPARE(val_int64, 10ll);
  lit = CRES_QVERIFY(Literal::fromValue(knowCore::Uris::xsd::integer32, u8"10"_kCs));
  qint32 val_int32 = CRES_QVERIFY(lit.value<qint32>());
  QCOMPARE(val_int32, 10ll);
}

void TestLiteral::testFromRDFLiteral()
{
  using namespace knowRDF;
  Literal val = CRES_QVERIFY(Literal::fromRdfLiteral(knowCore::Uris::xsd::string, "test"));
  QCOMPARE(val.value<QString>(), QStringLiteral("test"));
  val = CRES_QVERIFY(Literal::fromRdfLiteral("dataType"_kCu, "test"));
  QCOMPARE(val.datatype(), "dataType"_kCu);
  QCOMPARE(val.Value::datatype(), knowCore::Uris::xsd::string);
  QCOMPARE(val.value<QString>(), QStringLiteral("test"));

  val = CRES_QVERIFY(Literal::fromRdfLiteral(knowCore::Uris::xsd::boolean, "true"));
  QCOMPARE(val.value<bool>(), true);
  val = CRES_QVERIFY(Literal::fromRdfLiteral(knowCore::Uris::xsd::boolean, "false"));
  QCOMPARE(val.value<bool>(), false);

  CRES_QVERIFY_FAILURE(Literal::fromRdfLiteral(knowCore::Uris::xsd::boolean, "falsafse"));
}

void TestLiteral::testConvertFromValue()
{
  using namespace knowRDF;
  // Create a date from a string
  Literal lit = CRES_QVERIFY(Literal::fromValue(knowCore::Uris::xsd::dateTime, u8"10"_kCs));
  QString val_string = CRES_QVERIFY(lit.value<QString>());
  QCOMPARE(val_string, u8"10"_kCs);
  QCOMPARE(lit.datatype(), knowCore::Uris::xsd::dateTime);

  // Create a date from a number
  lit = CRES_QVERIFY(Literal::fromValue(knowCore::Uris::xsd::dateTime, quint64(10), QString(),
                                        knowCore::TypeCheckingMode::Force));
  knowCore::Timestamp val_dt = CRES_QVERIFY(lit.value<knowCore::Timestamp>());
  QCOMPARE(lit.knowCore::Value::datatype(), knowCore::Uris::xsd::dateTime);
  QCOMPARE(val_dt.toEpoch<knowCore::NanoSeconds>().count(), quint64(10));
  QCOMPARE(lit.datatype(), knowCore::Uris::xsd::dateTime);

  // Create a literal for an unsupported datate
  lit = CRES_QVERIFY(Literal::fromValue("test"_kCu, u8"10"_kCs));
  val_string = CRES_QVERIFY(lit.value<QString>());
  QCOMPARE(lit.knowCore::Value::datatype(), knowCore::Uris::xsd::string);
  QCOMPARE(val_string, u8"10"_kCs);
  QCOMPARE(lit.datatype(), "test"_kCu);
}

void TestLiteral::testLiteralValueToCppValue()
{
  using namespace knowRDF;
  using namespace knowCore;

  // Check access to the QString when the value contains the RDF literal
  Literal lit = CRES_QVERIFY(Literal::fromRdfLiteral(Uris::xsd::string, "test"));
  Value val = Value::fromValue(lit);
  CRES_QCOMPARE(val.value<QString>(), u8"test"_kCs);
  val = CRES_QVERIFY(Value::fromVariant(QVariant::fromValue(lit)));
  ;
  CRES_QCOMPARE(val.value<QString>(), u8"test"_kCs);

  // Check when the value is created from the literal and hold the string directly
  Value str_val = CRES_QVERIFY(val.convert(Uris::xsd::string));
  QCOMPARE(str_val.datatype(), Uris::xsd::string);
  CRES_QCOMPARE(str_val.value<QString>(), u8"test"_kCs);
  str_val = CRES_QVERIFY(Value::fromVariant(Uris::xsd::string, QVariant::fromValue(lit)));
  ;
  QCOMPARE(str_val.datatype(), Uris::xsd::string);
  CRES_QCOMPARE(str_val.value<QString>(), u8"test"_kCs);
}

void TestLiteral::testValueToLiteral()
{
  knowCore::Value value_uri = knowCore::Value::fromValue("test"_kCu);
  QVERIFY(value_uri.canConvert<knowRDF::Literal>());
  knowRDF::Literal literal_uri = CRES_QVERIFY(value_uri.value<knowRDF::Literal>());
  CRES_QCOMPARE(literal_uri.value<knowCore::Uri>(), "test"_kCu);
}

QTEST_MAIN(TestLiteral)
