#include <QtTest/QtTest>

class TestLiteral : public QObject
{
  Q_OBJECT
private slots:
  void testFromValue();
  void testDateLiteral();
  void testFromVariant();
  void testFromString();
  void testFromRDFLiteral();
  /**
   * Test when a literal is given as a value that needs to be converted
   */
  void testConvertFromValue();
  /**
   * This test the conversion from a Literal contained in a Value and converting that value to a C++
   * type
   */
  void testLiteralValueToCppValue();
  /**
   * test getting a literal from value
   */
  void testValueToLiteral();
};
