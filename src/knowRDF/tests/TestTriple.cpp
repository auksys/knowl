#include "TestTriple.h"

#include <knowCore/BigNumber.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/Uri.h>

#include <knowCore/Uris/xsd.h>

#include "../BlankNode.h"
#include "../Object.h"
#include "../Subject.h"
#include "../Triple.h"

void TestTriple::testLiteral()
{
  knowRDF::Literal l1
    = knowRDF::Literal::fromValue(knowCore::Uris::xsd::decimal, -0.00002).expect_success();
  QCOMPARE(l1.value<double>().expect_success(), -0.00002);

  knowRDF::Literal l2
    = knowRDF::Literal::fromValue<QString>(knowCore::Uris::xsd::string, u8"-0.00002"_kCs)
        .expect_success();
  QVERIFY(knowCore::MetaTypes::isNumericType(l1.datatype()));
  QVERIFY(not knowCore::MetaTypes::isNumericType(l2.datatype()));
  QVERIFY(not(l1 == l2));
}

void TestTriple::testTriple()
{
  knowRDF::BlankNode bn2;
  knowRDF::Triple triple(
    bn2, "http://test/path/2/is_then"_kCu,
    knowRDF::Literal::fromValue(knowCore::Uris::xsd::decimal, -0.00002).expect_success());

  QCOMPARE(triple.object().literal().value<double>().expect_success(), -0.00002);
}

QTEST_MAIN(TestTriple)
