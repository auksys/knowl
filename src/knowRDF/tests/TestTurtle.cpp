/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "TestTurtle.h"

#include <clog_qt>
#include <knowCore/Messages.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/Uri.h>

#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/xsd.h>

#include "../BlankNode.h"
#include "../Subject.h"
#include "../Test.h"
#include "../Triple.h"
#include "../TripleStream.h"
#include "../TriplesAccumulator.h"

namespace
{
  knowRDF::Subject createSubject(const QString& _uri)
  {
    return knowRDF::Subject(_uri, knowRDF::Subject::Type::Uri);
  }
  knowRDF::Subject createSubject(const knowRDF::BlankNode& _bn) { return knowRDF::Subject(_bn); }
} // namespace

#define COMPARE_SPO(__idx__, __subject_url__, __predicate_url__, __object__)                       \
  {                                                                                                \
    knowRDF::Triple t1 = tsl.triples()[__idx__];                                                   \
    QCOMPARE(t1.subject(), createSubject(__subject_url__));                                        \
    QCOMPARE(t1.predicate(), knowCore::Uri(__predicate_url__));                                    \
    QCOMPARE(t1.object(), __object__);                                                             \
  }

#define COMPARE_PO(__idx__, __predicate_url__, __object__)                                         \
  {                                                                                                \
    knowRDF::Triple t1 = tsl.triples()[__idx__];                                                   \
    QCOMPARE(t1.predicate(), knowCore::Uri(__predicate_url__));                                    \
    QCOMPARE(t1.object(), __object__);                                                             \
  }

#define START_PARSER(__STREAM__, __FILE__)                                                         \
  {                                                                                                \
    cres_qresult<void> __r__ = __STREAM__.start(__FILE__, nullptr, knowCore::FileFormat::Turtle);  \
    if(__r__.is_successful())                                                                      \
    {                                                                                              \
      clog_error(__r__.get_error().get_message().toStdString());                                   \
      QVERIFY(__r__.is_successful());                                                              \
    }                                                                                              \
  }

#define START_PARSER_EXPECT_FAILURE(__STREAM__, __FILE__)                                          \
  {                                                                                                \
    QVERIFY(                                                                                       \
      not __STREAM__.start(__FILE__, nullptr, knowCore::FileFormat::Turtle).is_successful());      \
  }

void TestTurtle::testExample1()
{
  knowRDF::TripleStream stream;
  knowRDF::TriplesAccumulator tsl;
  stream.addListener(&tsl);
  QFile file(":/example1.ttl");
  file.open(QIODevice::ReadOnly);
  START_PARSER(stream, &file);
  COMPARE_SPO(0, "http://example.org/#green-goblin",
              "http://www.perceive.net/schemas/relationship/enemyOf",
              knowCore::Uri("http://example.org/#spiderman"));
  COMPARE_SPO(1, "http://example.org/#green-goblin",
              "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
              knowCore::Uri("http://xmlns.com/foaf/0.1/Person"));
  COMPARE_SPO(2, "http://example.org/#green-goblin", "http://xmlns.com/foaf/0.1/name",
              knowRDF::Object::fromValue<QString>(knowCore::Uris::xsd::string, u8"Green Goblin"_kCs)
                .expect_success());
  COMPARE_SPO(3, "http://example.org/#spiderman",
              "http://www.perceive.net/schemas/relationship/enemyOf",
              knowCore::Uri("http://example.org/#green-goblin"));
  COMPARE_SPO(4, "http://example.org/#spiderman", "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
              knowCore::Uri("http://xmlns.com/foaf/0.1/Person"));
  COMPARE_SPO(5, "http://example.org/#spiderman", "http://xmlns.com/foaf/0.1/name",
              knowRDF::Object::fromValue<QString>(knowCore::Uris::xsd::string, u8"Spiderman"_kCs)
                .expect_success());
  COMPARE_SPO(6, "http://example.org/#spiderman", "http://xmlns.com/foaf/0.1/name",
              knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"Человек-паук"_kCs, "ru")
                .expect_success());
}

void TestTurtle::testExample2()
{
  knowRDF::TripleStream stream;
  knowRDF::TriplesAccumulator tsl;
  stream.addListener(&tsl);
  QFile file(":/example2.ttl");
  file.open(QIODevice::ReadOnly);
  START_PARSER(stream, &file);

  COMPARE_SPO(0, "http://data.lenka.no/geo/inndeling/01/0101",
              "http://www.geonames.org/ontology#officialName",
              knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"Halden"_kCs, "no")
                .expect_success());
  COMPARE_SPO(1, "http://data.lenka.no/geo/inndeling/01/0101",
              "http://www.geonames.org/ontology#officialName",
              knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"Halden"_kCs, "se")
                .expect_success());
  COMPARE_SPO(2, "http://data.lenka.no/geo/inndeling/01/0101",
              "http://www.geonames.org/ontology#officialName",
              knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"Halden"_kCs, "fkv")
                .expect_success());
  COMPARE_SPO(
    3, "http://data.lenka.no/geo/inndeling/01/0104",
    "http://www.geonames.org/ontology#officialName",
    knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"Moss"_kCs, "no").expect_success());
  COMPARE_SPO(
    4, "http://data.lenka.no/geo/inndeling/01/0104",
    "http://www.geonames.org/ontology#officialName",
    knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"Moss"_kCs, "se").expect_success());
  COMPARE_SPO(
    5, "http://data.lenka.no/geo/inndeling/01/0104",
    "http://www.geonames.org/ontology#officialName",
    knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"Moss"_kCs, "fkv").expect_success());
}

void TestTurtle::testExample3()
{
  knowRDF::TripleStream stream;
  knowRDF::TriplesAccumulator tsl;
  stream.addListener(&tsl);
  QFile file(":/example3.ttl");
  file.open(QIODevice::ReadOnly);
  START_PARSER(stream, &file);

  QCOMPARE(tsl.triples().at(0).subject(), tsl.triples().at(1).subject());
  QCOMPARE(tsl.triples().at(0).subject().blankNode().label(),
           tsl.triples().at(1).object().blankNode().label());
  QCOMPARE(tsl.triples().at(0).subject().blankNode(), tsl.triples().at(1).object().blankNode());
  QCOMPARE(tsl.triples().at(0).subject().type(), knowRDF::Subject::Type::BlankNode);

  COMPARE_PO(0, "http://www.loa-cnr.it/ontologies/DUL.owl#hasDataValue",
             knowRDF::Literal::fromValue(knowCore::Uris::xsd::integer, 1432716988641976118LL)
               .expect_success());
  COMPARE_PO(1, knowCore::Uris::rdf::a, tsl.triples().at(0).subject().blankNode());
}

void TestTurtle::testExample4()
{
  knowRDF::TripleStream stream;
  knowRDF::TriplesAccumulator tsl;
  stream.addListener(&tsl);
  QFile file(":/example4.ttl");
  file.open(QIODevice::ReadOnly);
  START_PARSER(stream, &file);

  QCOMPARE(tsl.triples().at(2).subject().type(), knowRDF::Subject::Type::BlankNode);
  QCOMPARE(tsl.triples().at(6).subject().type(), knowRDF::Subject::Type::BlankNode);

  knowRDF::BlankNode bn1 = tsl.triples().at(2).subject().blankNode();
  knowRDF::BlankNode bn2 = tsl.triples().at(6).subject().blankNode();

  COMPARE_SPO(0, "http://data.semanticweb.org/conference/dc/2010",
              "http://xmlns.com/foaf/0.1/based_near",
              knowCore::Uri("http://dbpedia.org/resource/Pittsburgh"));
  COMPARE_SPO(1, "http://data.semanticweb.org/conference/dc/2010",
              "http://xmlns.com/foaf/0.1/based_near",
              knowCore::Uri("http://sws.geonames.org/5206379/"));
  COMPARE_SPO(2, bn1, knowCore::Uris::rdf::a,
              knowCore::Uri("http://www.w3.org/2003/01/geo/wgs84_pos#Point"));
  COMPARE_SPO(3, bn1, "http://www.w3.org/2003/01/geo/wgs84_pos#lat",
              knowRDF::Literal::fromValue<QString>(knowCore::Uris::xsd::string, u8"40.44"_kCs)
                .expect_success());
  COMPARE_SPO(4, bn1, "http://www.w3.org/2003/01/geo/wgs84_pos#long",
              knowRDF::Literal::fromValue<QString>(knowCore::Uris::xsd::string, u8"-80.00"_kCs)
                .expect_success());
  COMPARE_SPO(5, "http://data.semanticweb.org/conference/dc/2010",
              "http://xmlns.com/foaf/0.1/based_near", bn1);
  COMPARE_SPO(6, bn2, knowCore::Uris::rdf::a,
              knowCore::Uri("http://www.w3.org/2003/01/geo/wgs84_pos#Point"));
  COMPARE_SPO(7, bn2, "http://www.w3.org/2003/01/geo/wgs84_pos#lat",
              knowRDF::Literal::fromValue<QString>(knowCore::Uris::xsd::string, u8"-40.44"_kCs)
                .expect_success());
  COMPARE_SPO(8, bn2, "http://www.w3.org/2003/01/geo/wgs84_pos#long",
              knowRDF::Literal::fromValue<QString>(knowCore::Uris::xsd::string, u8"80.00"_kCs)
                .expect_success());
  COMPARE_SPO(9, "http://data.semanticweb.org/conference/dc/2010",
              "http://xmlns.com/foaf/0.1/based_near", bn2);
  COMPARE_SPO(10, "http://data.semanticweb.org/conference/dc/2010",
              "http://xmlns.com/foaf/0.1/homepage", knowCore::Uri("http://dc-2010.org/"));
  COMPARE_SPO(
    11, "http://data.semanticweb.org/conference/dc/2010",
    "http://swrc.ontoware.org/ontology#abstract",
    knowRDF::Literal::fromValue<QString>(
      knowCore::Uris::xsd::string,
      u8"The University of North Texas (UNT) Libraries recently revised their Metadata Input "
      u8"Guidelines in order to improve usability and accessibility for metadata writers, and "
      u8"to enhance the quality of metadata that drives new features in their digital systems. "
      u8" This paper describes important considerations in the revision process and also "
      u8"demonstrates the relationship between quality metadata and system functionality that "
      u8"ultimately benefits both metadata creators and system end-users.\n"
      " Keywords: metadata; input guidelines; schemas; system functionality; quality control; faceted searching"_kCs)
      .expect_success());
}

void TestTurtle::testExample4a()
{
  knowRDF::TripleStream stream;
  knowRDF::TriplesAccumulator tsl;
  stream.addListener(&tsl);
  QFile file(":/example4a.ttl");
  file.open(QIODevice::ReadOnly);
  START_PARSER_EXPECT_FAILURE(stream, &file);
}

void TestTurtle::testExample5()
{
  knowRDF::TripleStream stream;
  knowRDF::TriplesAccumulator tsl;
  stream.addListener(&tsl);
  QFile file(":/example5.ttl");
  file.open(QIODevice::ReadOnly);
  START_PARSER(stream, &file);
  COMPARE_SPO(
    0, "http://example.org/#", "b",
    knowRDF::Literal::fromValue<QString>(knowCore::Uris::xsd::string, u8"1"_kCs).expect_success());
  COMPARE_SPO(1, "http://example.org/#", "http://example.org/#example",
              knowCore::Uri("http://example.org/#"));
  COMPARE_SPO(2, "http://example.org/#", "http://example.org/#",
              knowCore::Uri("http://example.org/#"));
  COMPARE_SPO(
    3, "http://example.org/#", "http://example.org/#",
    knowRDF::Literal::fromValue<QString>(knowCore::Uris::xsd::string, QString()).expect_success());
  COMPARE_SPO(
    4, "http://example.org/#x3", "p3",
    knowRDF::Literal::fromValue<QString>("http://example.org/#someType"_kCu, QString("x\ny"))
      .expect_success());
}

void TestTurtle::testExampleBN()
{
  knowRDF::TripleStream stream;
  knowRDF::TriplesAccumulator tsl;
  stream.addListener(&tsl);
  QFile file(":/example_bn.ttl");
  file.open(QIODevice::ReadOnly);
  START_PARSER(stream, &file);

  COMPARE_PO(0, "b", "c"_kCu);
  COMPARE_PO(1, "d", "e"_kCu);
}

void TestTurtle::testExample_load_file()
{
  knowRDF::TripleStream stream;
  stream.setBase("qrc:/example_load_file.ttl"_kCu);
  knowRDF::TriplesAccumulator tsl;
  stream.addListener(&tsl);
  QFile file(":/example_load_file.ttl");
  file.open(QIODevice::ReadOnly);
  START_PARSER(stream, &file);
  COMPARE_SPO(0, "qrc:/a", "qrc:/b",
              knowRDF::Literal::fromValue<QString>(knowCore::Uris::xsd::string, u8"c\n"_kCs)
                .expect_success());
}

QTEST_MAIN(TestTurtle)
