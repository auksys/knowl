/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include <QtTest/QtTest>

class TestTurtle : public QObject
{
  Q_OBJECT
private slots:
  void testExample1();
  void testExample2();
  void testExample3();
  void testExample4();
  void testExample4a();
  void testExample5();
  void testExampleBN();
  void testExample_load_file();
};
