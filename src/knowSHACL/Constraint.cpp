#include "Constraint_p.h"

#include "NodeShape_p.h"
#include "PropertyShape_p.h"

using namespace knowSHACL;

Constraint::Constraint() : d(nullptr) {}

Constraint::Constraint(const Constraint& _rhs) : d(_rhs.d) {}

Constraint& Constraint::operator=(const Constraint& _rhs)
{
  d = _rhs.d;
  return *this;
}

Constraint::~Constraint() {}

Constraint::Type Constraint::type() const { return d->type; }

PropertyShape Constraint::property() const
{
  if(d->property_ref)
  {
    PropertyShape ps;
    ps.d = d->property_ref;
    return ps;
  }
  else
  {
    return d->property;
  }
}

knowCore::Value Constraint::value() const { return d->value; }

QList<NodeShape> Constraint::nodes() const { return d->nodes; }

NodeShape Constraint::node() const
{
  if(d->node_ref)
  {
    NodeShape ns;
    ns.d = d->node_ref;
    return ns;
  }
  else
  {
    return d->node;
  }
}

QRegularExpression Constraint::pattern() const { return d->pattern; }

std::optional<int> Constraint::qualifiedMinCount() const { return d->qualifiedMinCount; }

std::optional<int> Constraint::qualifiedMaxCount() const { return d->qualifiedMaxCount; }

bool Constraint::qualifiedDisjoint() const { return d->qualifiedDisjoint; }
