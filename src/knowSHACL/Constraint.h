#pragma once

#include "Forward.h"

#include <knowCore/Value.h>

#include <QExplicitlySharedDataPointer>

namespace knowSHACL
{
  /**
   * @ingroup knowSHACL
   * Represents a Constraint
   */
  class Constraint
  {
    friend class details::DefinitionParser;
  public:
    enum class Type
    {
      Empty,
      Class,        //< has a given class
      Datatype,     //< has a given datatype
      DefaultValue, //< has a default value
      Disjoint,     //< is disjoint from the current node
      Equals,       //< is equals to the current node
      HasValue,     //< is equal to a value
      In,           //< is one of the value
      LanguageIn,   //< the node is a string with specified language
      LessThan,
      LessThanOrEquals,
      MaxCount,
      MinCount,
      MaxLength,    //< the maximum length of a string
      MinLength,    //< the maximum length of a string
      MaxExclusive, //< the maximum exclusive accepted value
      MinExclusive, //< the minimum exclusive accepted value
      MaxInclusive, //< the maximum inclusive accepted value
      MinInclusive, //< the minimum inclusive accepted value
      NodeKind,     //< is a kind of node
      Node,         //< is an other node constraint
      QualifiedValue,
      Pattern,  //< Is a pattern
      Property, //< Is a property
      UniqueLang,
      // Agrregates
      And,
      Or,
      Not,
      XOne
    };
  public:
    Constraint();
    Constraint(const Constraint& _rhs);
    Constraint& operator=(const Constraint& _rhs);
    ~Constraint();
    /**
     * @return the type of Constraint
     */
    Type type() const;
    /**
     * @return the property (only valid if type() == Property)
     */
    PropertyShape property() const;
    /**
     * @return the property (only valid if type() == Property)
     */
    knowCore::Value value() const;
    /**
     * @return the children Constraints (only valid if type() != Property)
     */
    QList<NodeShape> nodes() const;
    NodeShape node() const;
    template<typename _T_>
    cres_qresult<_T_> value() const
    {
      return value().value<_T_>();
    }
    QRegularExpression pattern() const;
    std::optional<int> qualifiedMinCount() const;
    std::optional<int> qualifiedMaxCount() const;
    bool qualifiedDisjoint() const;
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace knowSHACL
