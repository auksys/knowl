#include "Constraint.h"

#include <knowCore/Value.h>

#include "NodeShape.h"
#include "PropertyShape.h"

struct knowSHACL::Constraint::Private : public QSharedData
{
  Type type = Type::Empty;

  knowCore::Value value;
  PropertyShape property;
  PropertyShape::Private* property_ref = nullptr;
  QList<NodeShape> nodes;
  NodeShape node;
  NodeShape::Private* node_ref = nullptr;
  QRegularExpression pattern;
  std::optional<int> qualifiedMinCount, qualifiedMaxCount;
  bool qualifiedDisjoint = false;
};
