#include "Definition_p.h"

#include <QBuffer>
#include <QFile>

#include <cres_qt>
#include <knowCore/Uri.h>

#include <knowRDF/Literal.h>
#include <knowRDF/Node.h>

#include "DefinitionParser_p.h"
#include "Target.h"

using namespace knowSHACL;

Definition::Definition() : d(nullptr) {}

Definition::Definition(const Definition& _rhs) : d(_rhs.d) {}

Definition& Definition::operator=(const Definition& _rhs)
{
  d = _rhs.d;
  return *this;
}

Definition::~Definition() {}

QList<NodeShape> Definition::nodes() const { return d->nodes; }

bool Definition::isValid() const { return d; }

cres_qresult<Definition> Definition::create(const QString& _Definition_def,
                                            const knowCore::Uri& _base, const QString& _format)
{
  QBuffer buffer;
  buffer.setData(_Definition_def.toUtf8());
  buffer.open(QIODevice::ReadOnly);

  return create(&buffer, _base, _format);
}

cres_qresult<Definition> Definition::create(QIODevice* _definition_device,
                                            const knowCore::Uri& _base, const QString& _format)
{
  return details::DefinitionParser::parse(_definition_device, _base, _format);
}

cres_qresult<Definition> Definition::create(const QUrl& _Definition_url, const QString& _format)
{
  QString fn = _Definition_url.toLocalFile();

  if(fn.isEmpty())
  {
    if(_Definition_url.scheme() == "qrc")
    {
      fn = ":" + _Definition_url.path();
    }
    else if(_Definition_url.scheme().isEmpty())
    {
      fn = _Definition_url.path();
    }
  }

  if(fn.isEmpty())
  {
    return cres_failure("Cannot access definition at URL {} only local files are supported",
                        _Definition_url);
  }

  QFile file(fn);

  if(not file.open(QIODevice::ReadOnly))
  {
    return cres_failure("Failed to open {} with local name {}", _Definition_url, fn);
  }

  return create(&file, _Definition_url, _format);
}

cres_qresult<Definition> Definition::create(const knowRDF::Graph& _graph)
{
  return details::DefinitionParser::parse(_graph);
}

QList<NodeShape> Definition::shapesFor(const knowRDF::Node* _node)
{
  QList<NodeShape> shapes;
  for(const NodeShape& ns : d->nodes)
  {
    bool apply = false;
    for(const Target& target : ns.targets())
    {
      switch(target.type())
      {
      case Target::Type::NodeUri:
        apply = _node->uri() == target.uri();
        break;
      case Target::Type::NodeLiteral:
        apply = _node->literal() == target.literal();
        break;
      case Target::Type::Class:
        apply = _node->belongsToClass(target.uri());
        break;
      case Target::Type::SubjectsOf:
        apply = _node->children().keys().contains(target.uri());
        break;
      case Target::Type::ObjectsOf:
        apply = _node->parents().keys().contains(target.uri());
        break;
      }
      if(apply)
        break;
    }
    if(apply)
    {
      shapes.append(ns);
    }
  }
  return shapes;
}
