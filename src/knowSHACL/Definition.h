#include "Forward.h"

#include <QExplicitlySharedDataPointer>

#include <knowCore/FileFormat.h>

namespace knowSHACL
{
  /**
   * @ingroup knowSHACL
   * Definition for SHACL as defined in https://www.w3.org/TR/shacl/ plus support for some
   * extensions.
   */
  class Definition
  {
    friend class details::DefinitionParser;
  public:
    Definition();
    Definition(const Definition& _rhs);
    Definition& operator=(const Definition& _rhs);
    ~Definition();
  public:
    /**
     * @return the nodes definition for this definition
     */
    QList<NodeShape> nodes() const;
    /**
     * @return true if the definition is valid
     */
    bool isValid() const;
  public:
    /**
     * Create a \ref Definition according to the definition given in \ref _Definition_def.
     */
    static cres_qresult<Definition> create(const QString& _Definition_def,
                                           const knowCore::Uri& _base,
                                           const QString& _format = knowCore::FileFormat::Turtle);
    /**
     * Create a \ref Definition according to the definition given in \ref _definition_device.
     */
    static cres_qresult<Definition> create(QIODevice* _definition_device,
                                           const knowCore::Uri& _base,
                                           const QString& _format = knowCore::FileFormat::Turtle);
    /**
     * Create a \ref Definition according to the definition given in \ref _Definition_def.
     */
    static cres_qresult<Definition> create(const QUrl& _Definition_url,
                                           const QString& _format = knowCore::FileFormat::Turtle);
    static cres_qresult<Definition> create(const knowRDF::Graph& _graph);
  public:
    /**
     * @return the list of shapes that applies to a give RDF node \p _node
     */
    QList<NodeShape> shapesFor(const knowRDF::Node* _node);
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace knowSHACL
