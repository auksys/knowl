#include "DefinitionParser_p.h"

#include <clog_qt>
#include <cres_qt>

#include <knowCore/Messages.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/ValueList.h>

#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/rdfs.h>

#include <knowRDF/BlankNode.h>
#include <knowRDF/Graph.h>
#include <knowRDF/Literal.h>
#include <knowRDF/TripleStream.h>

#include "Constraint_p.h"
#include "Definition_p.h"
#include "NodeShape_p.h"
#include "Path_p.h"
#include "PropertyShape_p.h"
#include "Target_p.h"
#include "Utils_p.h"

#include "Uris/sh.h"

using namespace knowSHACL;
using namespace knowSHACL::details;

using u_rdf = knowCore::Uris::rdf;
using u_rdfs = knowCore::Uris::rdfs;
using u_sh = Uris::sh;

#define CHECK_NODE_TYPE(__URI__, __NODE__, __TYPE__)                                               \
  if(__NODE__->type() != knowRDF::Node::Type::__TYPE__)                                            \
  {                                                                                                \
    return cres_failure("Invalid node type got {} expected {} for {}", __NODE__->type(),           \
                        knowRDF::Node::Type::__TYPE__, __URI__);                                   \
  }

struct DefinitionParser::Private
{
  QHash<knowCore::Uri, NodeShape> nodeShapes;
  QHash<knowCore::Uri, PropertyShape> propertyShapes;
  QList<std::function<cres_qresult<void>()>> delayed;
};

DefinitionParser::DefinitionParser() : d(new Private) {}

DefinitionParser::~DefinitionParser() { delete d; }

cres_qresult<knowCore::Value> DefinitionParser::getValue(const knowRDF::Node* node)
{
  switch(node->type())
  {
  case knowRDF::Node::Type::Uri:
    return cres_success(knowCore::Value::fromValue(node->uri()));
  case knowRDF::Node::Type::Literal:
    return cres_success(node->literal());
  case knowRDF::Node::Type::BlankNode:
  case knowRDF::Node::Type::Variable:
  case knowRDF::Node::Type::Undefined:
    break;
  }
  return cres_failure("Invalid node type got {} expected Uri or Literal for {}", node->type(),
                      u_sh::hasValue);
}

cres_qresult<Path> DefinitionParser::parsePath(const knowRDF::Node* node)
{
  Path p;
  p.d = new Path::Private;
  switch(node->type())
  {
  case knowRDF::Node::Type::Uri:
  {
    p.d->type = Path::Type::Predicate;
    p.d->predicate = node->uri();
    return cres_success(p);
  }
  case knowRDF::Node::Type::BlankNode:
  {
    if(node->isCollection())
    {
      QList<const knowRDF::Node*> nodes = node->getCollection();
      if(nodes.size() > 0)
      {
        cres_try(p, parsePath(nodes[0]));
        Path t = p;
        for(int i = 1; i < nodes.size(); ++i)
        {
          cres_try(Path np, parsePath(nodes[i]));
          t.d->next = np;
          t = np;
        }
      }
      return cres_success(p);
    }
    else
    {
      QMultiHash<knowCore::Uri, const knowRDF::Node*> children = node->children();
      if(children.size() != 1)
      {
        return cres_failure("Path element should have exactly one child, got {}", children.size());
      }
      auto const [uri_c, node_c] = *children.keyValueBegin();
      if(uri_c == u_sh::alternativePath)
      {
        p.d->type = Path::Type::Alternative;
        CHECK_NODE_TYPE(uri_c, node_c, BlankNode);
        for(const knowRDF::Node* node_uri : node_c->getCollection())
        {
          CHECK_NODE_TYPE(uri_c, node_uri, Uri);
          p.d->alternatives.append(node_uri->uri());
        }
      }
      else if(uri_c == u_sh::zeroOrMorePath)
      {
        p.d->type = Path::Type::ZeroOrMore;
        if(node_c->type() == knowRDF::Node::Type::BlankNode
           and node_c->blankNode().label() == "PathPath")
        {
          p.d->type = Path::Type::PropertyPath;
          return cres_success(p);
        }
        else
        {
          CHECK_NODE_TYPE(uri_c, node_c, Uri);
          p.d->predicate = node_c->uri();
        }
      }
      else
      {
        CHECK_NODE_TYPE(uri_c, node_c, Uri);
        p.d->predicate = node_c->uri();
        if(uri_c == u_sh::inversePath)
        {
          p.d->type = Path::Type::Inverse;
        }
        else if(uri_c == u_sh::oneOrMorePath)
        {
          p.d->type = Path::Type::OneOrMore;
        }
        else if(uri_c == u_sh::zeroOrOnePath)
        {
          p.d->type = Path::Type::ZeroOrOne;
        }
        else
        {
          return cres_failure("Path element of unknown type {}", uri_c);
        }
      }
      return cres_success(p);
    }
  }
  case knowRDF::Node::Type::Variable:
  case knowRDF::Node::Type::Literal:
  case knowRDF::Node::Type::Undefined:
    break;
  }
  return cres_failure("Invalid path, cannot be a node of type {}", node->type());
}

cres_qresult<PropertyShape> DefinitionParser::parsePropertyShape(const knowRDF::Node* _node)
{
  PropertyShape p;
  p.d = new PropertyShape::Private;
  p.d->uri = Utils::nodeUri(_node);

  knowCore::UriList ignoredUris = std::initializer_list<knowCore::Uri>{
    u_sh::qualifiedMinCount, u_sh::qualifiedMaxCount, u_sh::qualifiedValueShapesDisjoint};

  QMultiHash<knowCore::Uri, const knowRDF::Node*> children = _node->children();
  for(QMultiHash<knowCore::Uri, const knowRDF::Node*>::iterator child_it = children.begin();
      child_it != children.end(); ++child_it)
  {
    if(ignoredUris.contains(child_it.key()))
    {
    }
    else if(child_it.key() == u_rdf::a)
    {
      CHECK_NODE_TYPE(u_rdf::a, child_it.value(), Uri);
      p.d->types.append(child_it.value()->uri());
    }
    else if(child_it.key() == u_sh::path)
    {
      cres_try(p.d->path, parsePath(child_it.value()));
    }
    else if(child_it.key() == u_rdfs::label)
    {
      CHECK_NODE_TYPE(u_rdfs::label, child_it.value(), Literal);
      cres_try(p.d->label, child_it.value()->literal().value<QString>());
    }
    else if(child_it.key() == u_rdfs::comment)
    {
      CHECK_NODE_TYPE(u_rdfs::comment, child_it.value(), Literal);
      cres_try(p.d->comment, child_it.value()->literal().value<QString>());
    }
    else if(child_it.key() == u_sh::name)
    {
      CHECK_NODE_TYPE(u_sh::name, child_it.value(), Literal);
      cres_try(p.d->name, child_it.value()->literal().value<QString>());
    }
    else if(child_it.key() == u_sh::description)
    {
      CHECK_NODE_TYPE(u_sh::description, child_it.value(), Literal);
      cres_try(p.d->description, child_it.value()->literal().value<QString>());
    }
    else
    {
      cres_try(Constraint pc, parseConstraint(child_it.key(), child_it.value(), true, _node));
      if(pc.d->type != Constraint::Type::Empty)
      {
        p.d->constraints.append(pc);
      }
      if(pc.d->type == Constraint::Type::QualifiedValue)
      {
        p.d->hasQualifiedValue = true;
      }
    }
  }
  //   clog_assert(p.d->path.isValid());
  return cres_success(p);
}

cres_qresult<Constraint> DefinitionParser::parseConstraint(const knowCore::Uri& _type,
                                                           const knowRDF::Node* _node,
                                                           bool _property,
                                                           const knowRDF::Node* _parentNode)
{
  Constraint c;
  c.d = new Constraint::Private;
  if(_type == u_sh::property)
  {
    c.d->type = Constraint::Type::Property;

    knowCore::Uri node_uri = Utils::nodeUri(_node);
    if(node_uri.isEmpty())
    {
      cres_try(c.d->property, parsePropertyShape(_node));
    }
    else
    {
      d->delayed.append(
        [this, node_uri, c]() -> cres_qresult<void>
        {
          if(d->propertyShapes.contains(node_uri))
          {
            c.d->property_ref = d->propertyShapes[node_uri].d.data();
            return cres_success();
          }
          else
          {
            return cres_failure("Unknown property {} referenced in property.", node_uri);
          }
        });
    }
  }
  else if(_type == u_sh::hasValue)
  {
    c.d->type = Constraint::Type::HasValue;
    cres_try(c.d->value, getValue(_node));
  }
  else if(_type == u_sh::defaultValue)
  {
    c.d->type = Constraint::Type::DefaultValue;
    cres_try(c.d->value, getValue(_node));
  }
#define GET_NODE_CONSTRAINT_URI(_C_URI_, _C_TYPE_)                                                 \
  else if(_type == u_sh::_C_URI_)                                                                  \
  {                                                                                                \
    c.d->type = Constraint::Type::_C_TYPE_;                                                        \
    CHECK_NODE_TYPE(u_sh::_C_URI_, _node, Uri);                                                    \
    c.d->value = knowCore::Value::fromValue(_node->uri());                                         \
  }
#define GET_NODE_CONSTRAINT_VALUE(_C_URI_, _C_TYPE_)                                               \
  else if(_type == u_sh::_C_URI_)                                                                  \
  {                                                                                                \
    c.d->type = Constraint::Type::_C_TYPE_;                                                        \
    CHECK_NODE_TYPE(u_sh::_C_URI_, _node, Literal);                                                \
    c.d->value = knowCore::Value::fromValue(_node->literal());                                     \
  }

#define GET_NODE_CONSTRAINT_LIST(_C_URI_, _C_TYPE_)                                                \
  else if(_type == u_sh::_C_URI_)                                                                  \
  {                                                                                                \
    c.d->type = Constraint::Type::_C_TYPE_;                                                        \
    QList<knowCore::Value> list;                                                                   \
    for(const knowRDF::Node* list_node : _node->getCollection())                                   \
    {                                                                                              \
      cres_try(knowCore::Value v, getValue(list_node));                                            \
      list.append(v);                                                                              \
    }                                                                                              \
    c.d->value = knowCore::Value::fromValue(knowCore::ValueList(list));                            \
  }
  GET_NODE_CONSTRAINT_URI(datatype, Datatype)
  GET_NODE_CONSTRAINT_URI(equals, Equals)
  GET_NODE_CONSTRAINT_URI(disjoint, Disjoint)
  GET_NODE_CONSTRAINT_URI(klass, Class)
  GET_NODE_CONSTRAINT_URI(nodeKind, NodeKind)
  GET_NODE_CONSTRAINT_LIST(in, In)
  GET_NODE_CONSTRAINT_LIST(languageIn, LanguageIn)
  GET_NODE_CONSTRAINT_VALUE(minExclusive, MinExclusive)
  GET_NODE_CONSTRAINT_VALUE(maxExclusive, MaxExclusive)
  GET_NODE_CONSTRAINT_VALUE(minInclusive, MinInclusive)
  GET_NODE_CONSTRAINT_VALUE(maxInclusive, MaxInclusive)
  GET_NODE_CONSTRAINT_VALUE(maxLength, MaxLength)
  GET_NODE_CONSTRAINT_VALUE(minLength, MinLength)
  else if(_type == u_sh::pattern)
  {
    c.d->type = Constraint::Type::Pattern;
    CHECK_NODE_TYPE(u_sh::pattern, _node, Literal);
    cres_try(QString pattern_str, _node->literal().value<QString>());
    c.d->pattern = QRegularExpression(pattern_str);
    if(not c.d->pattern.isValid())
    {
      return cres_failure("Failed to compile regular expression {} with error {}", pattern_str,
                          c.d->pattern.errorString());
    }
    for(const knowRDF::Node* flag_node : _parentNode->children(u_sh::flags))
    {
      CHECK_NODE_TYPE(u_sh::flags, flag_node, Literal);
      cres_try(QString flags_str, flag_node->literal().value<QString>());
      for(int i = 0; i < flags_str.length(); ++i)
      {
        if(flags_str[i] == 'i')
        {
          c.d->pattern.setPatternOptions(c.d->pattern.patternOptions()
                                         | QRegularExpression::CaseInsensitiveOption);
        }
        else
        {
          return cres_failure("Unhandled flags {} at character {}", flags_str, i);
        }
      }
    }
  }
  else if(_type == u_sh::qualifiedValueShape)
  {
    const knowRDF::Node* minNode = _parentNode->getFirstChild(u_sh::qualifiedMinCount);
    if(minNode)
    {
      CHECK_NODE_TYPE(u_sh::qualifiedMinCount, minNode, Literal);
      cres_try(c.d->qualifiedMinCount, minNode->literal().value<int>());
    }
    const knowRDF::Node* maxNode = _parentNode->getFirstChild(u_sh::qualifiedMaxCount);
    if(maxNode)
    {
      CHECK_NODE_TYPE(u_sh::qualifiedMaxCount, maxNode, Literal);
      cres_try(c.d->qualifiedMaxCount, maxNode->literal().value<int>());
    }
    const knowRDF::Node* disjoinNode
      = _parentNode->getFirstChild(u_sh::qualifiedValueShapesDisjoint);
    if(disjoinNode)
    {
      CHECK_NODE_TYPE(u_sh::qualifiedValueShapesDisjoint, disjoinNode, Literal);
      cres_try(c.d->qualifiedDisjoint, disjoinNode->literal().value<bool>());
    }
    c.d->type = Constraint::Type::QualifiedValue;

    knowCore::Uri node_uri = Utils::nodeUri(_node);
    if(not node_uri.isEmpty())
    {
      d->delayed.append(
        [this, node_uri, c]() -> cres_qresult<void>
        {
          if(d->nodeShapes.contains(node_uri))
          {
            c.d->node_ref = d->nodeShapes[node_uri].d.data();
            return cres_success();
          }
          else
          {
            return cres_failure("Unknown node {} referenced in property.", node_uri);
          }
        });
    }
    else if(_node->type() == knowRDF::Node::Type::BlankNode)
    {
      cres_try(c.d->node, parseNodeShape(_node));
    }
    else
    {
      return cres_failure("Expected BlankNode or Uri for {}", u_sh::node);
    }
  }
  else if(_type == u_sh::or_ or _type == u_sh::and_ or _type == u_sh::xone)
  {
    if(_type == u_sh::or_)
    {
      c.d->type = Constraint::Type::Or;
    }
    else if(_type == u_sh::and_)
    {
      c.d->type = Constraint::Type::And;
    }
    else
    {
      clog_assert(_type == u_sh::xone);
      c.d->type = Constraint::Type::XOne;
    }
    for(const knowRDF::Node* node_C : _node->getCollection())
    {
      cres_try(NodeShape ns, parseNodeShape(node_C));
      c.d->nodes.append(ns);
    }
  }
  else if(_type == u_sh::not_)
  {
    CHECK_NODE_TYPE(u_sh::not_, _node, BlankNode);
    cres_try(NodeShape ns, parseNodeShape(_node));
    c.d->type = Constraint::Type::Not;
    c.d->nodes.append(ns);
  }
  else if(_type == u_sh::node)
  {
    knowCore::Uri node_uri = Utils::nodeUri(_node);
    if(not node_uri.isEmpty())
    {
      c.d->type = Constraint::Type::Node;
      d->delayed.append(
        [this, node_uri, c]() -> cres_qresult<void>
        {
          if(d->nodeShapes.contains(node_uri))
          {
            c.d->node_ref = d->nodeShapes[node_uri].d.data();
            return cres_success();
          }
          else
          {
            return cres_failure("Unknown node {} referenced in property.", node_uri);
          }
        });
    }
    else if(_node->type() == knowRDF::Node::Type::BlankNode)
    {
      c.d->type = Constraint::Type::Node;
      cres_try(c.d->node, parseNodeShape(_node));
    }
    else
    {
      return cres_failure("Expected BlankNode or Uri for {}", u_sh::node);
    }
  }
  else if(_property)
  {
    if(false)
    {
    }
    GET_NODE_CONSTRAINT_VALUE(maxCount, MaxCount)
    GET_NODE_CONSTRAINT_VALUE(minCount, MinCount)
    GET_NODE_CONSTRAINT_URI(lessThan, LessThan)
    GET_NODE_CONSTRAINT_URI(lessThanOrEquals, LessThanOrEquals)
    GET_NODE_CONSTRAINT_VALUE(uniqueLang, UniqueLang)
    else
    {
      knowCore::UriList ignoredUris
        = std::initializer_list<knowCore::Uri>{u_sh::targetNode, u_rdf::a};
      if(not ignoredUris.contains(_type))
      {
        clog_warning("{} is ignnored in property constraint definition", _type);
      }
    }
  }
  else
  {
    if(_type == u_sh::maxCount or _type == u_sh::minCount or _type == u_sh::lessThan
       or _type == u_sh::lessThanOrEquals or _type == u_sh::uniqueLang)
    {
      return cres_failure("{} is not supported for node shapes", _type);
    }
    clog_warning("{} is ignnored in node constraint definition", _type);
  }
  return cres_success(c);
}

bool DefinitionParser::isTargetUri(const knowCore::Uri& _type)
{
  return _type == u_sh::targetSubjectsOf or _type == u_sh::targetClass
         or _type == u_sh::targetObjectsOf or _type == u_sh::targetNode;
}

cres_qresult<Target> DefinitionParser::parseTarget(const knowCore::Uri& _type,
                                                   const knowRDF::Node* _node)
{
#define HANDLE_TARGET(_URI_, _TYPE_)                                                               \
  else if(_type == u_sh::_URI_)                                                                    \
  {                                                                                                \
    CHECK_NODE_TYPE(u_sh::_URI_, _node, Uri);                                                      \
    Target t;                                                                                      \
    t.d = new Target::Private;                                                                     \
    t.d->type = Target::Type::_TYPE_;                                                              \
    t.d->uri = _node->uri();                                                                       \
    return cres_success(t);                                                                        \
  }
  if(false)
    ;
  HANDLE_TARGET(targetSubjectsOf, SubjectsOf)
  HANDLE_TARGET(targetClass, Class)
  HANDLE_TARGET(targetObjectsOf, ObjectsOf)
  else if(_type == u_sh::targetNode)
  {
    Target t;
    t.d = new Target::Private;
    switch(_node->type())
    {
    case knowRDF::Node::Type::Uri:
      t.d->type = Target::Type::NodeUri;
      t.d->uri = _node->uri();
      break;
    case knowRDF::Node::Type::Literal:
      t.d->type = Target::Type::NodeLiteral;
      t.d->literal = _node->literal();
      break;
    case knowRDF::Node::Type::BlankNode:
      return cres_failure("BlankNode cannot be targetNode");
    case knowRDF::Node::Type::Variable:
      return cres_failure("BlankNode cannot be variable");
    case knowRDF::Node::Type::Undefined:
      return cres_failure("BlankNode cannot be undefined");
    }
    return cres_success(t);
  }
  return cres_failure("Invalid target");
}

cres_qresult<NodeShape> DefinitionParser::parseNodeShape(const knowRDF::Node* _node)
{
  NodeShape s;
  s.d = new NodeShape::Private;

  if(_node->type() == knowRDF::Node::Type::Uri)
  {
    Target t;
    t.d = new Target::Private;
    t.d->type = Target::Type::Class;
    t.d->uri = _node->uri();
    s.d->targets.append(t);
    s.d->uri = _node->uri();
  }

  knowCore::UriList ignoredUris = std::initializer_list<knowCore::Uri>{u_sh::flags};

  QMultiHash<knowCore::Uri, const knowRDF::Node*> children = _node->children();
  for(QMultiHash<knowCore::Uri, const knowRDF::Node*>::iterator child_it = children.begin();
      child_it != children.end(); ++child_it)
  {
    if(ignoredUris.contains(child_it.key()))
    {
      // skip
    }
    else if(child_it.key() == u_rdf::a)
    {
      CHECK_NODE_TYPE(u_rdf::a, child_it.value(), Uri);
      s.d->types.append(child_it.value()->uri());
    }
    else if(child_it.key() == u_rdfs::comment)
    {
      CHECK_NODE_TYPE(u_rdfs::comment, child_it.value(), Literal);
      cres_try(s.d->comment, child_it.value()->literal().value<QString>());
    }
    else if(child_it.key() == u_rdfs::label)
    {
      CHECK_NODE_TYPE(u_rdfs::label, child_it.value(), Literal);
      cres_try(s.d->label, child_it.value()->literal().value<QString>());
    }
    else if(child_it.key() == u_rdfs::seeAlso)
    {
      CHECK_NODE_TYPE(u_rdfs::seeAlso, child_it.value(), Uri);
      s.d->seeAlso = child_it.value()->uri();
    }
    else if(child_it.key() == u_rdfs::subClassOf)
    {
      CHECK_NODE_TYPE(u_rdfs::subClassOf, child_it.value(), Uri);
      s.d->parentClasses.append(child_it.value()->uri());
    }
    else if(child_it.key() == u_sh::closed)
    {
      CHECK_NODE_TYPE(u_sh::closed, child_it.value(), Literal);
      cres_try(s.d->isClosed, child_it.value()->literal().value<bool>());
    }
    else if(child_it.key() == u_sh::ignoredProperties)
    {
      if(child_it.value()->isCollection())
      {
        for(const knowRDF::Node* n : child_it.value()->getCollection())
        {
          CHECK_NODE_TYPE(u_sh::ignoredProperties, n, Uri);
          s.d->ignoredProperties.append(n->uri());
        }
      }
      else
      {
        return cres_failure("ignoredProperties should be a list of uri");
      }
    }
    else if(isTargetUri(child_it.key()))
    {
      cres_try(Target t, parseTarget(child_it.key(), child_it.value()));
      s.d->targets.append(t);
    }
    else
    {
      cres_try(Constraint c, parseConstraint(child_it.key(), child_it.value(), false, _node));
      if(c.d->type != Constraint::Type::Empty)
      {
        s.d->constraints.append(c);
      }
    }
  }

  return cres_success(s);
}

cres_qresult<Definition> DefinitionParser::parse(QIODevice* _definition_device,
                                                 const knowCore::Uri& _base, const QString& _format)
{
  knowRDF::TripleStream testfilestream;
  testfilestream.setBase(_base);
  knowRDF::Graph graph;
  testfilestream.addListener(&graph);

  cres_try(cres_ignore, testfilestream.start(_definition_device, nullptr, _format));

  return parse(graph);
}

bool DefinitionParser::isNodeShape(const knowRDF::Node* node)
{
  return node->belongsToClass(u_sh::NodeShape);
}

bool DefinitionParser::isPropertyShape(const knowRDF::Node* node)
{
  return node->belongsToClass(u_sh::PropertyShape) or node->children(u_sh::path).size() >= 1;
}

cres_qresult<Definition> DefinitionParser::parse(const knowRDF::Graph& _graph)
{
  DefinitionParser dp;

  Definition def;
  def.d = new Definition::Private;

  for(const knowRDF::Node* node : _graph.nodes())
  {
    knowCore::Uri node_uri = Utils::nodeUri(node);

    if(not node_uri.isEmpty())
    {
      if(dp.isNodeShape(node))
      {
        cres_try(NodeShape shape, dp.parseNodeShape(node));
        def.d->nodes.append(shape);
        dp.d->nodeShapes[node_uri] = shape;
      }
      else if(dp.isPropertyShape(node))
      {
        cres_try(PropertyShape shape, dp.parsePropertyShape(node));
        def.d->properties.append(shape);
        dp.d->propertyShapes[node_uri] = shape;

        if(node->hasChildren(QList<QString>{u_sh::targetSubjectsOf, u_sh::targetClass,
                                            u_sh::targetObjectsOf, u_sh::targetNode}))
        {
          NodeShape ns;
          ns.d = new NodeShape::Private;
          Constraint c;
          c.d = new Constraint::Private;
          c.d->type = Constraint::Type::Property;
          c.d->property_ref = shape.d.data();
          ns.d->constraints.append(c);

          QMultiHash<knowCore::Uri, const knowRDF::Node*> children = node->children();
          for(QMultiHash<knowCore::Uri, const knowRDF::Node*>::iterator it = children.begin();
              it != children.end(); ++it)
          {
            if(dp.isTargetUri(it.key()))
            {
              cres_try(Target t, dp.parseTarget(it.key(), it.value()));
              ns.d->targets.append(t);
            }
          }
          def.d->nodes.append(ns);
        }
      }
    }
  }

  for(const std::function<cres_qresult<void>()>& f : dp.d->delayed)
  {
    cres_try(cres_ignore, f());
  }

  for(const NodeShape& ns : def.d->nodes)
  {
    QList<PropertyShape> sibblings;
    for(const Constraint& c : ns.constraints())
    {
      if(c.type() == Constraint::Type::Property and c.property().hasQualifiedValue())
      {
        sibblings.append(c.property());
      }
    }
    for(const PropertyShape& c1 : sibblings)
    {
      for(const PropertyShape& c2 : sibblings)
      {
        if(c1.d != c2.d)
        {
          c1.d->sibblings.append(c2.d.data());
        }
      }
    }
  }

  return cres_success(def);
}
