#include "Forward.h"

namespace knowSHACL::details
{
  class DefinitionParser
  {
    DefinitionParser();
    ~DefinitionParser();
  public:
    static cres_qresult<Definition> parse(QIODevice* _definition_device, const knowCore::Uri& _base,
                                          const QString& _format);
    static cres_qresult<Definition> parse(const knowRDF::Graph& _graph);
  private:
    cres_qresult<NodeShape> parseNodeShape(const knowRDF::Node* node);
    cres_qresult<PropertyShape> parsePropertyShape(const knowRDF::Node* node);
    cres_qresult<Constraint> parseConstraint(const knowCore::Uri& _type, const knowRDF::Node* node,
                                             bool _property, const knowRDF::Node* _parentNode);
    cres_qresult<Path> parsePath(const knowRDF::Node* node);
    cres_qresult<knowCore::Value> getValue(const knowRDF::Node* node);
    bool isNodeShape(const knowRDF::Node* node);
    bool isPropertyShape(const knowRDF::Node* node);
    cres_qresult<Target> parseTarget(const knowCore::Uri& _type, const knowRDF::Node* node);
    bool isTargetUri(const knowCore::Uri& _type);
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowSHACL::details
