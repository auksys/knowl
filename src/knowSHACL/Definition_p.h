#include "Definition.h"

#include <QList>

#include "NodeShape.h"
#include "PropertyShape.h"

struct knowSHACL::Definition::Private : public QSharedData
{
  QList<NodeShape> nodes;
  QList<PropertyShape> properties;
};
