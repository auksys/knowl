#include <knowRDF/Forward.h>

namespace knowSHACL
{
  class Constraint;
  class Definition;
  class Path;
  class NodeShape;
  class PropertyShape;
  class Target;
  class Validator;
  class ValidationResult;
  class ValidationResults;
  namespace details
  {
    class DefinitionParser;
  }
} // namespace knowSHACL
