#include "NodeShape_p.h"

using namespace knowSHACL;

NodeShape::NodeShape() : d(nullptr) {}

NodeShape::NodeShape(const NodeShape& _rhs) : d(_rhs.d) {}

NodeShape& NodeShape::operator=(const NodeShape& _rhs)
{
  d = _rhs.d;
  return *this;
}

NodeShape::~NodeShape() {}

bool NodeShape::isValid() const { return d; }

knowCore::Uri NodeShape::uri() const { return d->uri; }

QList<Target> NodeShape::targets() const { return d->targets; }

QList<knowCore::Uri> NodeShape::parentClasses() const { return d->parentClasses; }

QList<Constraint> NodeShape::constraints() const { return d->constraints; }

bool NodeShape::isClosed() const { return d->isClosed; }

knowCore::UriList NodeShape::ignoredProperties() const { return d->ignoredProperties; }

QList<knowCore::Uri> NodeShape::types() const { return d->types; }
