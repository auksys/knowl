#pragma once

#include "Forward.h"

#include <QExplicitlySharedDataPointer>

namespace knowSHACL
{
  /**
   * @ingroup knowSHACL
   * Represents a NodeShape
   */
  class NodeShape
  {
    friend class details::DefinitionParser;
    friend class Constraint;
  public:
    NodeShape();
    NodeShape(const NodeShape& _rhs);
    NodeShape& operator=(const NodeShape& _rhs);
    ~NodeShape();
    /**
     * @return if the shape is valid
     */
    bool isValid() const;
    /**
     * @return the uri that address this shape, or an empty string if the shape was defined as a
     * blank node
     */
    knowCore::Uri uri() const;
    QList<Target> targets() const;
    /**
     * @return the list of parent classes, i.e. anything that was refered with 'rdfs:subClassOf'
     */
    QList<knowCore::Uri> parentClasses() const;
    /**
     * @return the list of types (i.e. rdf:type/a that were used to define this node)
     */
    QList<knowCore::Uri> types() const;
    /**
     * @return the validating constraints
     */
    QList<Constraint> constraints() const;
    /**
     * @return true if the shape is closed, i.e. if only the property listed are allowed
     */
    bool isClosed() const;
    /**
     * @return list of properties that are not specified but are allowed, needed if isClosed is set
     * to true.
     */
    knowCore::UriList ignoredProperties() const;
    bool operator!=(const NodeShape& _rhs) const { return d != _rhs.d; }
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace knowSHACL
