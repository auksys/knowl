#include "NodeShape.h"

#include <knowCore/UriList.h>

#include "Constraint.h"
#include "Target.h"

struct knowSHACL::NodeShape::Private : public QSharedData
{
  QString comment, label;
  knowCore::Uri seeAlso, uri;
  QList<Target> targets;
  QList<knowCore::Uri> parentClasses;
  QList<Constraint> constraints;
  bool isClosed = false;
  QList<knowCore::Uri> ignoredProperties;
  QList<knowCore::Uri> types;
};
