#include "Path_p.h"

#include <knowCore/UriList.h>

using namespace knowSHACL;

Path::Path() : d(nullptr) {}

Path::Path(const Path& _rhs) : d(_rhs.d) {}

Path& Path::operator=(const Path& _rhs)
{
  d = _rhs.d;
  return *this;
}

Path::~Path() {}

bool Path::isValid() const { return d; }

Path::Type Path::type() const { return d->type; }

knowCore::Uri Path::predicate() const { return d->predicate; }

knowCore::UriList Path::alternatives() const { return d->alternatives; }

std::optional<Path> Path::next() const { return d->next; }

bool Path::match(const knowCore::Uri& _uri) const
{
  switch(d->type)
  {
  case Type::Predicate:
  case Type::ZeroOrMore:
  case Type::OneOrMore:
  case Type::ZeroOrOne:
    return d->predicate == _uri;
  case Type::Alternative:
    return d->alternatives.contains(_uri);
  case Type::Inverse:
    return d->predicate != _uri;
  case Type::PropertyPath:
    return false;
  }
  return false;
}

bool Path::operator==(const Path& _rhs) const
{
  return d == _rhs.d
         or (d != nullptr and _rhs.d != nullptr and d->type == _rhs.d->type
             and d->predicate == _rhs.d->predicate
             and ((d->next and _rhs.d->next and *d->next == *_rhs.d->next)
                  or (not d->next and not _rhs.d->next)));
}
