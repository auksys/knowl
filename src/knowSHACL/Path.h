#pragma once

#include "Forward.h"

#include <QExplicitlySharedDataPointer>
#include <optional>

namespace knowSHACL
{
  /**
   * @ingroup knowSHACL
   * Represents a property path
   */
  class Path
  {
    friend class details::DefinitionParser;
    friend class Validator;
    friend class ValidationResults;
  public:
    Path();
    Path(const Path& _rhs);
    Path& operator=(const Path& _rhs);
    ~Path();
    enum class Type
    {
      Predicate,
      Alternative,
      Inverse,
      ZeroOrMore,
      OneOrMore,
      ZeroOrOne,
      PropertyPath //< work around ill-formation of the standard SHACL validator
    };
    Type type() const;
    /**
     * @return true if the path is valid.
     */
    bool isValid() const;
    knowCore::Uri predicate() const;
    knowCore::UriList alternatives() const;
    std::optional<Path> next() const;
    bool match(const knowCore::Uri&) const;
    /**
     * @return true if two paths are equals
     */
    bool operator==(const Path& _rhs) const;
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace knowSHACL

#include <knowCore/Formatter.h>

clog_format_declare_enum_formatter(knowSHACL::Path::Type, Predicate, Alternative, Inverse,
                                   ZeroOrMore, OneOrMore, ZeroOrOne, PropertyPath);

clog_format_declare_formatter(knowSHACL::Path)
{
  if(p.isValid())
  {
    if(p.next())
    {
      return format_to(ctx.out(), "{} {} {}", p.type(), p.predicate(), *p.next());
    }
    else
    {
      return format_to(ctx.out(), "{} {}", p.type(), p.predicate());
    }
  }
  else
  {
    return format_to(ctx.out(), "invalid path");
  }
}
