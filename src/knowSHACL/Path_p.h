#include "Path.h"

#include <knowCore/Uri.h>

struct knowSHACL::Path::Private : public QSharedData
{
  Type type;
  knowCore::Uri predicate;
  QList<knowCore::Uri> alternatives;
  std::optional<Path> next;
};
