#include "PropertyShape_p.h"

using namespace knowSHACL;

PropertyShape::PropertyShape() : d(nullptr) {}

PropertyShape::PropertyShape(const PropertyShape& _rhs) : d(_rhs.d) {}

PropertyShape& PropertyShape::operator=(const PropertyShape& _rhs)
{
  d = _rhs.d;
  return *this;
}

PropertyShape::~PropertyShape() {}

knowCore::Uri PropertyShape::uri() const { return d->uri; }

QList<knowCore::Uri> PropertyShape::types() const { return d->types; }

Path PropertyShape::path() const { return d->path; }

QList<Constraint> PropertyShape::constraints() const { return d->constraints; }

bool PropertyShape::hasQualifiedValue() const { return d->hasQualifiedValue; }

QList<PropertyShape> PropertyShape::sibblings() const
{
  QList<PropertyShape> ss;

  for(PropertyShape::Private* d : d->sibblings)
  {
    PropertyShape ps;
    ps.d = d;
    ss.append(ps);
  }

  return ss;
}
