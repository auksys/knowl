#pragma once

#include "Forward.h"

#include <QExplicitlySharedDataPointer>

namespace knowSHACL
{
  /**
   * @ingroup knowSHACL
   * Represents a PropertyShape
   */
  class PropertyShape
  {
    friend class details::DefinitionParser;
    friend class Constraint;
  public:
    PropertyShape();
    PropertyShape(const PropertyShape& _rhs);
    PropertyShape& operator=(const PropertyShape& _rhs);
    ~PropertyShape();
    /**
     * @return the list of types (i.e. rdf:type/a that were used to define this node)
     */
    QList<knowCore::Uri> types() const;
    knowCore::Uri uri() const;
    Path path() const;
    QList<Constraint> constraints() const;
    /**
     * @return true if it has qualified values
     */
    bool hasQualifiedValue() const;
    /**
     * @return the sibblings, with respects to
     */
    QList<PropertyShape> sibblings() const;
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace knowSHACL
