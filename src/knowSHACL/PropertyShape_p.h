#include "PropertyShape.h"

#include "Constraint.h"
#include "Path.h"

struct knowSHACL::PropertyShape::Private : public QSharedData
{
  QString label, comment;
  QString name, description;
  knowCore::Uri uri;
  Path path;
  QList<Constraint> constraints;
  bool hasQualifiedValue = false;
  QList<PropertyShape::Private*> sibblings;
  QList<knowCore::Uri> types;
};
