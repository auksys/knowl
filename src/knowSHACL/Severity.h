#pragma once

namespace knowSHACL
{
  enum class Severity
  {
    Violation
  };
}

#include <knowCore/Formatter.h>

clog_format_declare_enum_formatter(knowSHACL::Severity, Violation);
