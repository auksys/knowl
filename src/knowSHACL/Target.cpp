#include "Target_p.h"

using namespace knowSHACL;

Target::Target() : d(nullptr) {}

Target::Target(const Target& _rhs) : d(_rhs.d) {}

Target& Target::operator=(const Target& _rhs)
{
  d = _rhs.d;
  return *this;
}

Target::~Target() {}

Target::Type Target::type() const { return d->type; }

knowCore::Uri Target::uri() const { return d->uri; }

knowRDF::Literal Target::literal() const { return d->literal; }
