#pragma once

#include "Forward.h"

#include <QExplicitlySharedDataPointer>

namespace knowSHACL
{
  /**
   * @ingroup knowSHACL
   * Represents a target of a shape
   */
  class Target
  {
    friend class details::DefinitionParser;
  public:
    Target();
    Target(const Target& _rhs);
    Target& operator=(const Target& _rhs);
    ~Target();
    enum class Type
    {
      NodeLiteral,
      NodeUri,
      Class,
      SubjectsOf,
      ObjectsOf
    };
    Type type() const;
    knowRDF::Literal literal() const;
    knowCore::Uri uri() const;
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace knowSHACL
