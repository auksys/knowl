#include "Target.h"

#include <knowCore/Uri.h>
#include <knowRDF/Literal.h>

struct knowSHACL::Target::Private : public QSharedData
{
  Type type;
  knowRDF::Literal literal;
  knowCore::Uri uri;
};
