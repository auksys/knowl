#include <knowCore/Uris/Uris.h>

#define KNOWSHACL_SH_URIS(F, ...)                                                                  \
  F(__VA_ARGS__, AndConstraintComponent)                                                           \
  F(__VA_ARGS__, BlankNode)                                                                        \
  F(__VA_ARGS__, BlankNodeOrIRI)                                                                   \
  F(__VA_ARGS__, BlankNodeOrLiteral)                                                               \
  F(__VA_ARGS__, ClassConstraintComponent)                                                         \
  F(__VA_ARGS__, ClosedConstraintComponent)                                                        \
  F(__VA_ARGS__, DatatypeConstraintComponent)                                                      \
  F(__VA_ARGS__, DisjointConstraintComponent)                                                      \
  F(__VA_ARGS__, EqualsConstraintComponent)                                                        \
  F(__VA_ARGS__, HasValueConstraintComponent)                                                      \
  F(__VA_ARGS__, InConstraintComponent)                                                            \
  F(__VA_ARGS__, IRI)                                                                              \
  F(__VA_ARGS__, IRIOrLiteral)                                                                     \
  F(__VA_ARGS__, LanguageInConstraintComponent)                                                    \
  F(__VA_ARGS__, LessThanConstraintComponent)                                                      \
  F(__VA_ARGS__, LessThanOrEqualsConstraintComponent)                                              \
  F(__VA_ARGS__, Literal)                                                                          \
  F(__VA_ARGS__, MaxCountConstraintComponent)                                                      \
  F(__VA_ARGS__, MaxExclusiveConstraintComponent)                                                  \
  F(__VA_ARGS__, MaxInclusiveConstraintComponent)                                                  \
  F(__VA_ARGS__, MaxLengthConstraintComponent)                                                     \
  F(__VA_ARGS__, MinCountConstraintComponent)                                                      \
  F(__VA_ARGS__, MinExclusiveConstraintComponent)                                                  \
  F(__VA_ARGS__, MinInclusiveConstraintComponent)                                                  \
  F(__VA_ARGS__, MinLengthConstraintComponent)                                                     \
  F(__VA_ARGS__, NodeConstraintComponent)                                                          \
  F(__VA_ARGS__, NodeKindConstraintComponent)                                                      \
  F(__VA_ARGS__, NodeShape)                                                                        \
  F(__VA_ARGS__, NotConstraintComponent)                                                           \
  F(__VA_ARGS__, OrConstraintComponent)                                                            \
  F(__VA_ARGS__, PatternConstraintComponent)                                                       \
  F(__VA_ARGS__, PropertyShape)                                                                    \
  F(__VA_ARGS__, QualifiedMaxCountConstraintComponent)                                             \
  F(__VA_ARGS__, QualifiedMinCountConstraintComponent)                                             \
  F(__VA_ARGS__, UniqueLangConstraintComponent)                                                    \
  F(__VA_ARGS__, ValidationReport)                                                                 \
  F(__VA_ARGS__, Violation)                                                                        \
  F(__VA_ARGS__, XoneConstraintComponent)                                                          \
  F(__VA_ARGS__, alternativePath)                                                                  \
  F(__VA_ARGS__, and_, "and")                                                                      \
  F(__VA_ARGS__, closed)                                                                           \
  F(__VA_ARGS__, conforms)                                                                         \
  F(__VA_ARGS__, datatype)                                                                         \
  F(__VA_ARGS__, defaultValue)                                                                     \
  F(__VA_ARGS__, description)                                                                      \
  F(__VA_ARGS__, disjoint)                                                                         \
  F(__VA_ARGS__, equals)                                                                           \
  F(__VA_ARGS__, flags)                                                                            \
  F(__VA_ARGS__, focusNode)                                                                        \
  F(__VA_ARGS__, hasValue)                                                                         \
  F(__VA_ARGS__, ignoredProperties)                                                                \
  F(__VA_ARGS__, in)                                                                               \
  F(__VA_ARGS__, languageIn)                                                                       \
  F(__VA_ARGS__, inversePath)                                                                      \
  F(__VA_ARGS__, klass, "class")                                                                   \
  F(__VA_ARGS__, lessThan)                                                                         \
  F(__VA_ARGS__, lessThanOrEquals)                                                                 \
  F(__VA_ARGS__, maxCount)                                                                         \
  F(__VA_ARGS__, maxLength)                                                                        \
  F(__VA_ARGS__, maxExclusive)                                                                     \
  F(__VA_ARGS__, maxInclusive)                                                                     \
  F(__VA_ARGS__, minCount)                                                                         \
  F(__VA_ARGS__, minExclusive)                                                                     \
  F(__VA_ARGS__, minInclusive)                                                                     \
  F(__VA_ARGS__, minLength)                                                                        \
  F(__VA_ARGS__, name)                                                                             \
  F(__VA_ARGS__, node)                                                                             \
  F(__VA_ARGS__, nodeKind)                                                                         \
  F(__VA_ARGS__, not_, "not")                                                                      \
  F(__VA_ARGS__, oneOrMorePath)                                                                    \
  F(__VA_ARGS__, or_, "or")                                                                        \
  F(__VA_ARGS__, path)                                                                             \
  F(__VA_ARGS__, pattern)                                                                          \
  F(__VA_ARGS__, property)                                                                         \
  F(__VA_ARGS__, qualifiedMaxCount)                                                                \
  F(__VA_ARGS__, qualifiedMinCount)                                                                \
  F(__VA_ARGS__, qualifiedValueShape)                                                              \
  F(__VA_ARGS__, qualifiedValueShapesDisjoint)                                                     \
  F(__VA_ARGS__, result)                                                                           \
  F(__VA_ARGS__, resultPath)                                                                       \
  F(__VA_ARGS__, resultSeverity)                                                                   \
  F(__VA_ARGS__, sourceConstraintComponent)                                                        \
  F(__VA_ARGS__, sourceShape)                                                                      \
  F(__VA_ARGS__, targetClass)                                                                      \
  F(__VA_ARGS__, targetNode)                                                                       \
  F(__VA_ARGS__, targetObjectsOf)                                                                  \
  F(__VA_ARGS__, targetSubjectsOf)                                                                 \
  F(__VA_ARGS__, uniqueLang)                                                                       \
  F(__VA_ARGS__, value)                                                                            \
  F(__VA_ARGS__, xone)                                                                             \
  F(__VA_ARGS__, zeroOrMorePath)                                                                   \
  F(__VA_ARGS__, zeroOrOnePath)

KNOWCORE_ONTOLOGY_URIS(knowSHACL::Uris, KNOWSHACL_SH_URIS, sh, "http://www.w3.org/ns/shacl#")
