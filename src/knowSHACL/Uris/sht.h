#include <knowCore/Uris/Uris.h>

#define KNOWSHACL_SHT_URIS(F, ...)                                                                 \
  F(__VA_ARGS__, dataGraph)                                                                        \
  F(__VA_ARGS__, shapesGraph)

KNOWCORE_ONTOLOGY_URIS(knowSHACL::Uris, KNOWSHACL_SHT_URIS, sht, "http://www.w3.org/ns/shacl-test#")
