#include <knowRDF/Node.h>
#include <knowRDF/Skolemisation.h>

namespace knowSHACL::Utils
{

  inline knowCore::Uri nodeUri(const knowRDF::Node* node)
  {
    switch(node->type())
    {
    case knowRDF::Node::Type::Uri:
      return node->uri();
    case knowRDF::Node::Type::BlankNode:
      if(not node->blankNode().label().isEmpty())
      {
        return knowRDF::blankNodeSkolemisation(node->blankNode());
      }
    case knowRDF::Node::Type::Variable:
    case knowRDF::Node::Type::Literal:
    case knowRDF::Node::Type::Undefined:
      break;
    }
    return knowCore::Uri();
  }
} // namespace knowSHACL::Utils
