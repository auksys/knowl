#include "ValidationResult_p.h"

#include <knowRDF/Node.h>

using namespace knowSHACL;

cres_qresult<void> ValidationResult::Private::setFocusNode(const knowRDF::Node* _node)
{
  switch(_node->type())
  {
  case knowRDF::Node::Type::Uri:
    focusNode = _node->uri();
    break;
  case knowRDF::Node::Type::Literal:
    focusNode = _node->literal();
    break;
  case knowRDF::Node::Type::BlankNode:
    focusNode = _node->blankNode();
    break;
  case knowRDF::Node::Type::Undefined:
    return cres_failure("Cannot assigned Undefined to focusNode");
  case knowRDF::Node::Type::Variable:
    return cres_failure("Cannot assigned Variable to focusNode");
  }
  return cres_success();
}

cres_qresult<void> ValidationResult::Private::setValue(const knowRDF::Node* _node)
{
  switch(_node->type())
  {
  case knowRDF::Node::Type::Uri:
    value = knowCore::Value::fromValue(_node->uri());
    break;
  case knowRDF::Node::Type::Literal:
    value = _node->literal();
    break;
  case knowRDF::Node::Type::BlankNode:
    value = knowCore::Value::fromValue("_:" + _node->blankNode().label());
    break;
  case knowRDF::Node::Type::Undefined:
    return cres_failure("Cannot assigned Undefined to value");
  case knowRDF::Node::Type::Variable:
    return cres_failure("Cannot assigned Variable to value");
  }
  return cres_success();
}

ValidationResult::ValidationResult() : d(nullptr) {}

ValidationResult::ValidationResult(const ValidationResult& _rhs) : d(_rhs.d) {}

ValidationResult& ValidationResult::operator=(const ValidationResult& _rhs)
{
  d = _rhs.d;
  return *this;
}

ValidationResult::~ValidationResult() {}

Severity ValidationResult::severity() const { return d->severity; }

knowRDF::Object ValidationResult::focusNode() const { return d->focusNode; }

Path ValidationResult::resultPath() const { return d->resultPath; }

knowCore::Uri ValidationResult::sourceConstraintComponent() const
{
  return d->sourceConstraintComponent;
}

knowCore::Uri ValidationResult::sourceShape() const { return d->sourceShape; }

knowCore::Value ValidationResult::value() const { return d->value; }
