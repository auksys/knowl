#pragma once

#include <QExplicitlySharedDataPointer>

#include "Forward.h"
#include "Severity.h"

namespace knowSHACL
{
  class ValidationResult
  {
    friend class Validator;
    friend class ValidationResults;
  public:
    ValidationResult();
    ValidationResult(const ValidationResult& _rhs);
    ValidationResult& operator=(const ValidationResult& _rhs);
    ~ValidationResult();
    /**
     * @return the severity of the validation
     */
    Severity severity() const;
    /**
     * The name of the node that failed
     */
    knowRDF::Object focusNode() const;
    Path resultPath() const;
    /**
     * Type of constrained that failed
     */
    knowCore::Uri sourceConstraintComponent() const;
    /**
     * The source shape that has triggered the result
     */
    knowCore::Uri sourceShape() const;
    /**
     * The value that triggered the error
     */
    knowCore::Value value() const;
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace knowSHACL
