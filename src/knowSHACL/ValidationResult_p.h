#include "ValidationResult.h"

#include <knowCore/Uri.h>
#include <knowCore/Value.h>
#include <knowRDF/Object.h>

#include "Path.h"

struct knowSHACL::ValidationResult::Private : public QSharedData
{
  Severity severity;
  knowRDF::Object focusNode;
  knowCore::Uri sourceShape;
  knowCore::Uri sourceConstraintComponent;
  knowCore::Value value;
  Path resultPath;

  cres_qresult<void> setFocusNode(const knowRDF::Node* _node);
  cres_qresult<void> setValue(const knowRDF::Node* _node);
};
