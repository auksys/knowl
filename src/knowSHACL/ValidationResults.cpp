#include "ValidationResults_p.h"

#include <cres_qt>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/Uri.h>
#include <knowCore/UriList.h>

#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/rdfs.h>

#include <knowRDF/Literal.h>

#include "Path_p.h"
#include "Utils_p.h"
#include "ValidationResult_p.h"

#include "Uris/sh.h"

using u_rdf = knowCore::Uris::rdf;
using u_rdfs = knowCore::Uris::rdfs;
using u_sh = knowSHACL::Uris::sh;

using namespace knowSHACL;

ValidationResults::ValidationResults() : d(nullptr) {}

ValidationResults::ValidationResults(const ValidationResults& _rhs) : d(_rhs.d) {}

ValidationResults& ValidationResults::operator=(const ValidationResults& _rhs)
{
  d = _rhs.d;
  return *this;
}

ValidationResults::~ValidationResults() {}

bool ValidationResults::isSuccessful() const { return d->success; }

QList<ValidationResult> ValidationResults::results() const { return d->results; }

cres_qresult<Path> ValidationResults::parsePath(const knowCore::Uri& _uri,
                                                const knowRDF::Node* _node)
{
  Path p;
  p.d = new Path::Private;
  if(_uri == u_sh::alternativePath)
  {
    p.d->type = Path::Type::Alternative;
    for(const knowRDF::Node* cnode : _node->getCollection())
    {
      p.d->alternatives.append(cnode->uri());
    }
  }
#define HANDLE_PATH_ELT(_URI_, _TYPE_)                                                             \
  else if(_uri == u_sh::_URI_)                                                                     \
  {                                                                                                \
    p.d->type = Path::Type::_TYPE_;                                                                \
    p.d->predicate = _node->uri();                                                                 \
  }
  HANDLE_PATH_ELT(zeroOrMorePath, ZeroOrMore)
  HANDLE_PATH_ELT(zeroOrOnePath, ZeroOrOne)
  HANDLE_PATH_ELT(oneOrMorePath, OneOrMore)
  HANDLE_PATH_ELT(inversePath, Inverse)
  else
  {
    return cres_failure("Unsupported uri '{}' in path", _uri);
  }
  return cres_success(p);
}

cres_qresult<ValidationResults> ValidationResults::load(const knowRDF::Node* _node)
{
  ValidationResults vr;
  vr.d = new ValidationResults::Private;

  QMultiHash<knowCore::Uri, const knowRDF::Node*> children = _node->children();

  knowCore::UriList ignoredUris
    = std::initializer_list<knowCore::Uri>{u_rdfs::subClassOf, u_rdf::a};

  for(QMultiHash<knowCore::Uri, const knowRDF::Node*>::iterator it = children.begin();
      it != children.end(); ++it)
  {
    if(ignoredUris.contains(it.key()))
    {
    }
    else if(it.key() == Uris::sh::result)
    {
      QMultiHash<knowCore::Uri, const knowRDF::Node*> result_children = it.value()->children();
      ValidationResult r;
      r.d = new ValidationResult::Private;

      for(QMultiHash<knowCore::Uri, const knowRDF::Node*>::iterator it = result_children.begin();
          it != result_children.end(); ++it)
      {
        if(ignoredUris.contains(it.key()))
        {
        }
        else if(it.key() == Uris::sh::focusNode)
        {
          r.d->setFocusNode(it.value());
        }
        else if(it.key() == Uris::sh::resultPath)
        {
          r.d->resultPath.d = new Path::Private;
          switch(it.value()->type())
          {
          case knowRDF::Node::Type::Uri:
            r.d->resultPath.d->type = Path::Type::Predicate;
            r.d->resultPath.d->predicate = it.value()->uri();
            break;
          case knowRDF::Node::Type::BlankNode:
          {
            if(it.value()->isCollection())
            {
              bool first = true;
              Path previous_path;
              for(const knowRDF::Node* n : it.value()->getCollection())
              {
                Path p;
                p.d = new Path::Private;
                switch(n->type())
                {
                case knowRDF::Node::Type::Uri:
                  p.d->type = Path::Type::Predicate;
                  p.d->predicate = n->uri();
                  break;
                case knowRDF::Node::Type::BlankNode:
                {
                  auto const [uri, node] = *n->children().constKeyValueBegin();
                  cres_try(p, parsePath(uri, node));
                  break;
                }
                case knowRDF::Node::Type::Variable:
                case knowRDF::Node::Type::Literal:
                case knowRDF::Node::Type::Undefined:
                  return cres_failure("Variable, Literal or Undefined are not supported in Path");
                }
                if(first)
                {
                  r.d->resultPath = p;
                  previous_path = p;
                  first = false;
                }
                else
                {
                  previous_path.d->next = p;
                  previous_path = p;
                }
              }
            }
            else
            {
              auto const [uri, node] = *it.value()->children().constKeyValueBegin();
              cres_try(r.d->resultPath, parsePath(uri, node));
            }
            break;
          }
          case knowRDF::Node::Type::Literal:
          case knowRDF::Node::Type::Variable:
          case knowRDF::Node::Type::Undefined:
            return cres_failure("Variable or Undefined are not supported in Path");
          }
        }
        else if(it.key() == Uris::sh::sourceConstraintComponent)
        {
          r.d->sourceConstraintComponent = it.value()->uri();
        }
        else if(it.key() == Uris::sh::sourceShape)
        {
          r.d->sourceShape = Utils::nodeUri(it.value());
        }
        else if(it.key() == Uris::sh::resultSeverity)
        {
          if(it.value()->uri() == Uris::sh::Violation)
          {
            r.d->severity = Severity::Violation;
          }
        }
        else if(it.key() == Uris::sh::value)
        {
          r.d->setValue(it.value());
        }
        else
        {
          return cres_failure("Unhandled key in result {}", it.key());
        }
      }
      vr.d->results.append(r);
    }
    else if(it.key() == Uris::sh::conforms)
    {
      cres_try(vr.d->success, it.value()->literal().value<bool>());
    }
    else
    {
      return cres_failure("Unhandled key in results {}", it.key());
    }
  }

  return cres_success(vr);
}
