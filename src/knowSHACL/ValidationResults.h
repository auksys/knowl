#include "Forward.h"

#include <QExplicitlySharedDataPointer>

namespace knowSHACL
{
  class ValidationResults
  {
    friend class Validator;
  public:
    ValidationResults();
    ValidationResults(const ValidationResults& _rhs);
    ValidationResults& operator=(const ValidationResults& _rhs);
    ~ValidationResults();
    /**
     * @return true if the validation
     */
    bool isSuccessful() const;
    /**
     * @return full list of validation results
     */
    QList<ValidationResult> results() const;
  public:
    /**
     * Load a \ref ValidationResults from a RDF Node
     */
    static cres_qresult<ValidationResults> load(const knowRDF::Node* _node);
  private:
    static cres_qresult<Path> parsePath(const knowCore::Uri& _uri, const knowRDF::Node* _node);
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace knowSHACL
