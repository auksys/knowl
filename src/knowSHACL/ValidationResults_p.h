#include "ValidationResults.h"

#include "ValidationResult.h"

struct knowSHACL::ValidationResults::Private : public QSharedData
{
  bool success;
  QList<ValidationResult> results;
};
