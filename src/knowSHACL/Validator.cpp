#include "Validator.h"

#include <QFile>

#include <cres_qt>
#include <knowCore/BigNumber.h>
#include <knowCore/Messages.h>
#include <knowCore/Timestamp.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/UriList.h>
#include <knowCore/ValueList.h>

#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/xsd.h>

#include <knowRDF/Graph.h>
#include <knowRDF/Node.h>
#include <knowRDF/TripleStream.h>

#include "Constraint.h"
#include "Definition.h"
#include "NodeShape.h"
#include "Path_p.h"
#include "PropertyShape.h"
#include "Target.h"
#include "ValidationResult_p.h"
#include "ValidationResults_p.h"

#include "Uris/sh.h"

using namespace knowSHACL;

using u_rdf = knowCore::Uris::rdf;
using u_xsd = knowCore::Uris::xsd;
using u_sh = knowSHACL::Uris::sh;

struct Validator::Private : public QSharedData
{
  Definition def;
  bool extendedResults = false;
};

Validator::Validator() : d(nullptr) {}

Validator::Validator(const Validator& _rhs) : d(_rhs.d) {}

Validator& Validator::operator=(const Validator& _rhs)
{
  d = _rhs.d;
  return *this;
}

Validator::~Validator() {}

cres_qresult<ValidationResults>
  Validator::validate(QIODevice* _graph_data, const knowCore::Uri& _base, const QString& _format)
{
  knowRDF::TripleStream testfilestream;
  testfilestream.setBase(_base);
  knowRDF::Graph graph;
  testfilestream.addListener(&graph);

  cres_try(cres_ignore, testfilestream.start(_graph_data, nullptr, _format));

  return validate(graph);
}

cres_qresult<ValidationResults> Validator::validate(const QUrl& _graph_uri, const QString& _format)
{
  QString fn = _graph_uri.toLocalFile();

  if(fn.isEmpty())
  {
    if(_graph_uri.scheme() == "qrc")
    {
      fn = ":" + _graph_uri.path();
    }
    else if(_graph_uri.scheme().isEmpty())
    {
      fn = _graph_uri.path();
    }
  }

  if(fn.isEmpty())
  {
    return cres_failure("Cannot access definition at URL {} only local files are supported",
                        _graph_uri);
  }

  QFile file(fn);

  if(not file.open(QIODevice::ReadOnly))
  {
    return cres_failure("Failed to open {} with local name {}", _graph_uri, fn);
  }

  return validate(&file, _graph_uri, _format);
}

cres_qresult<ValidationResults> Validator::validate(const knowCore::Uri& _graph_uri,
                                                    const QString& _format)
{
  return validate(QUrl(_graph_uri), _format);
}

ValidationResult Validator::createValidationResult(const knowRDF::Node* _focusNode,
                                                   const Path& _path, Severity _severity,
                                                   const knowCore::Uri& _sourceConstraintComponent,
                                                   const knowCore::Uri& _sourceShape,
                                                   const knowRDF::Node* _value)
{
  ValidationResult vr;
  vr.d = new ValidationResult::Private;
  vr.d->setFocusNode(_focusNode);
  vr.d->resultPath = _path;
  vr.d->severity = _severity;
  vr.d->sourceConstraintComponent = _sourceConstraintComponent;
  vr.d->sourceShape = _sourceShape;
  vr.d->setValue(_value);
  return vr;
}

template<typename _T_>
ValidationResult Validator::createValidationResult(const knowRDF::Node* _focusNode,
                                                   const Path& _path, Severity _severity,
                                                   const knowCore::Uri& _sourceConstraintComponent,
                                                   const knowCore::Uri& _sourceShape,
                                                   const _T_& _value)
{
  return createValidationResult(_focusNode, _path, _severity, _sourceConstraintComponent,
                                _sourceShape, knowCore::Value::fromValue(_value));
}

ValidationResult Validator::createValidationResult(const knowRDF::Node* _focusNode,
                                                   const Path& _path, Severity _severity,
                                                   const knowCore::Uri& _sourceConstraintComponent,
                                                   const knowCore::Uri& _sourceShape,
                                                   const knowCore::Value& _value)
{
  ValidationResult vr;
  vr.d = new ValidationResult::Private;
  vr.d->setFocusNode(_focusNode);
  vr.d->resultPath = _path;
  vr.d->severity = _severity;
  vr.d->sourceConstraintComponent = _sourceConstraintComponent;
  vr.d->sourceShape = _sourceShape;
  vr.d->value = _value;
  return vr;
}

QList<const knowRDF::Node*> Validator::follow(const knowRDF::Node* _node, const Path& _path)
{
  QList<const knowRDF::Node*> nodes;
  using PT = Path::Type;
  switch(_path.type())
  {
  case PT::Predicate:
    nodes = _node->children(_path.predicate());
    break;
  case PT::Alternative:
  {
    QMultiHash<knowCore::Uri, const knowRDF::Node*> children = _node->children();
    for(QMultiHash<knowCore::Uri, const knowRDF::Node*>::iterator it = children.begin();
        it != children.end(); ++it)
    {
      if(_path.alternatives().contains(it.key()))
      {
        nodes.append(it.value());
      }
    }
    break;
  }
  case PT::Inverse:
    nodes = _node->parents(_path.predicate());
    break;
  case PT::ZeroOrMore:
    nodes.append(_node);
    Q_FALLTHROUGH();
  case PT::OneOrMore:
  {
    QList<const knowRDF::Node*> investigates;
    investigates.append(_node);
    while(not investigates.isEmpty())
    {
      const knowRDF::Node* node = investigates.takeFirst();
      QList<const knowRDF::Node*> children = node->children(_path.predicate());
      investigates.append(children);
      nodes.append(children);
    }
    break;
  }
  case PT::ZeroOrOne:
  {
    nodes.append(_node);
    nodes.append(_node->children(_path.predicate()));
    break;
  }
  case PT::PropertyPath:
    clog_fatal("wip {}", (int)_path.type());
  }
  if(_path.next())
  {
    QList<const knowRDF::Node*> realnodes;
    for(const knowRDF::Node* n : nodes)
    {
      realnodes.append(follow(n, *_path.next()));
    }
    return realnodes;
  }
  return nodes;
}

namespace
{
  knowCore::Uri coalesce(const knowCore::Uri& _a, const knowCore::Uri& _b)
  {
    return _a.isEmpty() ? _b : _a;
  }

  void collectPath(QList<Path>* _paths, const NodeShape& _shape)
  {
    for(const Constraint& c : _shape.constraints())
    {
      using CT = Constraint::Type;
      switch(c.type())
      {
      case CT::Empty:
        break;
      case CT::Property:
        _paths->append(c.property().path());
        break;
      case CT::Class:
      case CT::Datatype:
      case CT::DefaultValue:
      case CT::Disjoint:
      case CT::Equals:
      case CT::HasValue:
      case CT::In:
      case CT::LanguageIn:
      case CT::LessThan:
      case CT::LessThanOrEquals:
      case CT::MaxCount:
      case CT::MinCount:
      case CT::MaxExclusive:
      case CT::MinExclusive:
      case CT::MaxInclusive:
      case CT::MinInclusive:
      case CT::MaxLength:
      case CT::MinLength:
      case CT::NodeKind:
      case CT::Node:
      case CT::QualifiedValue:
      case CT::Pattern:
      case CT::UniqueLang:
        break;
      case CT::And:
      case CT::Or:
      case CT::Not:
      case CT::XOne:
        for(const NodeShape& ss : c.nodes())
        {
          collectPath(_paths, ss);
        }
        break;
      }
    }
  }
  bool checkDataType(const knowRDF::Node* _node, const knowCore::Uri& _datatype_uri)
  {
    return _node->type() == knowRDF::Node::Type::Literal
           and ((_datatype_uri != u_xsd::string and _node->literal().datatype() == _datatype_uri)
                or (_datatype_uri == u_rdf::langString
                    and _node->literal().datatype() == u_xsd::string
                    and not _node->literal().lang().isEmpty())
                or (_datatype_uri == u_xsd::string and _node->literal().datatype() == u_xsd::string
                    and _node->literal().lang().isEmpty()));
  }
  cres_qresult<bool> compare(const knowRDF::Node* _node, const knowCore::Value& _value)
  {
    switch(_node->type())
    {
    case knowRDF::Node::Type::Uri:
    {
      cres_try(knowCore::Uri uri, _value.value<knowCore::Uri>());
      return cres_success(_node->uri() == uri);
    }
    case knowRDF::Node::Type::Literal:
    {
      return cres_success(_node->literal() == _value);
    }
    case knowRDF::Node::Type::BlankNode:
    {
      cres_try(QString label, _value.value<QString>());
      return cres_success(_node->blankNode().label() == label);
    }
    case knowRDF::Node::Type::Variable:
    case knowRDF::Node::Type::Undefined:
      return cres_failure("Cannot compare Variable or Undefined");
    }
    return cres_failure("Invalid node type");
  }
  cres_qresult<knowCore::Value> getValue(const knowRDF::Node* _node)
  {
    switch(_node->type())
    {
    case knowRDF::Node::Type::Literal:
      return cres_success(knowCore::Value(_node->literal()));
    case knowRDF::Node::Type::Uri:
      return cres_success(knowCore::Value::fromValue(_node->uri()));
    case knowRDF::Node::Type::BlankNode:
      return cres_failure("BlankNode are not valid value");
    case knowRDF::Node::Type::Undefined:
      return cres_failure("Undefined are not valid value");
    case knowRDF::Node::Type::Variable:
      return cres_failure("Variable are not valid value");
    }
    return cres_failure("Unknown node type");
  }
} // namespace

cres_qresult<bool> Validator::validate(const knowRDF::Node* _node, const Constraint& _constraint,
                                       QList<ValidationResult>* _results,
                                       const knowRDF::Node* _focus_node, const Path& _path,
                                       const knowCore::Uri& _source_shape)
{
  using CT = Constraint::Type;
  switch(_constraint.type())
  {
  case CT::Empty:
    break;
  case CT::MaxCount:
    return cres_failure("MaxCount constraint are not allowed for individual node constraints.");
  case CT::MinCount:
    return cres_failure("MinCount constraint are not allowed for individual node constraints.");
  case CT::LessThan:
    return cres_failure("LessThan constraint are not allowed for individual node constraints.");
  case CT::LessThanOrEquals:
    return cres_failure(
      "LessThanOrEquals constraint are not allowed for individual node constraints.");
  case CT::UniqueLang:
    return cres_failure("UniqueLang constraint are not allowed for individual node constraints.");
  case CT::QualifiedValue:
    return cres_failure(
      "QualifiedValue constraint are not allowed for individual node constraints.");
  case CT::Property:
    return validate(_node, _constraint.property(), _results, _source_shape);
  case CT::Datatype:
  {
    cres_try(knowCore::Uri datatype_uri, _constraint.value<knowCore::Uri>());
    if(not checkDataType(_node, datatype_uri))
    {
      _results->append(createValidationResult(_focus_node, _path, Severity::Violation,
                                              u_sh::DatatypeConstraintComponent, _source_shape,
                                              _node));
      return cres_success(false);
    }
    return cres_success(true);
  }
  case CT::DefaultValue:
  {
    // Default value are non-validating
    return cres_success(true);
  }
  case CT::Disjoint:
  {
    cres_try(knowCore::Uri child_uri, _constraint.value<knowCore::Uri>());
    for(const knowRDF::Node* cnode : _focus_node->children(child_uri))
    {
      if(cnode->equal(_node))
      {
        _results->append(createValidationResult(_focus_node, _path, Severity::Violation,
                                                u_sh::DisjointConstraintComponent, _source_shape,
                                                cnode));
        return cres_success(false);
      }
    }
    return cres_success(true);
  }
  case CT::Equals:
  {
    return cres_failure("Internal error: equals should be checked at a specific level.");
  }
  case CT::Class:
  {
    cres_try(knowCore::Uri uri, _constraint.value<knowCore::Uri>());
    if(not _node->belongsToClass(uri))
    {
      _results->append(createValidationResult(_focus_node, _path, Severity::Violation,
                                              u_sh::ClassConstraintComponent, _source_shape,
                                              _node));
      return cres_success(false);
    }
    return cres_success(true);
  }
  case CT::HasValue:
  {
    cres_try(bool s, compare(_node, _constraint.value()));
    if(not s)
    {
      _results->append(createValidationResult(
        _focus_node, _path, Severity::Violation, u_sh::HasValueConstraintComponent, _source_shape,
        knowCore::Value())); // somehow the spec says no value should be added
      return cres_success(false);
    }
    return cres_success(true);
  }
  case CT::In:
  {
    cres_try(knowCore::ValueList list, _constraint.value<knowCore::ValueList>());
    bool found = false;
    for(const knowCore::Value& value : list)
    {
      cres_try(bool s, compare(_node, value));
      if(s)
      {
        found = true;
        break;
      }
    }
    if(not found)
    {
      _results->append(createValidationResult(_focus_node, _path, Severity::Violation,
                                              u_sh::InConstraintComponent, _source_shape, _node));
      return cres_success(false);
    }
    return cres_success(true);
  }
  case CT::LanguageIn:
  {
    cres_try(knowCore::ValueList list, _constraint.value<knowCore::ValueList>());
    bool found = false;
    knowRDF::Literal lit = _node->literal();
    for(const knowCore::Value& value : list)
    {
      cres_try(QString lang, value.value<QString>());
      if(lit.lang() == lang or lit.lang().startsWith(lang))
      {
        found = true;
        break;
      }
    }
    if(not found)
    {
      _results->append(createValidationResult(_focus_node, _path, Severity::Violation,
                                              u_sh::LanguageInConstraintComponent, _source_shape,
                                              _node));
      return cres_success(false);
    }
    return cres_success(true);
  }
  case CT::MaxExclusive:
  case CT::MinExclusive:
  case CT::MaxInclusive:
  case CT::MinInclusive:
  {
    bool valid = false;
    if(knowCore::Uris::isNumericType(_node->literal().datatype()))
    {
      cres_try(knowCore::BigNumber bound, _constraint.value<knowCore::BigNumber>());
      cres_try(knowCore::BigNumber value, _node->literal().value<knowCore::BigNumber>());
      switch(_constraint.type())
      {
      case CT::MaxExclusive:
        valid = value < bound;
        break;
      case CT::MinExclusive:
        valid = value > bound;
        break;
      case CT::MaxInclusive:
        valid = value <= bound;
        break;
      case CT::MinInclusive:
        valid = value >= bound;
        break;
      default:
        clog_fatal("Impossible");
      }
    }
    else if(_node->literal().datatype() == u_xsd::dateTime)
    {
      cres_try(knowCore::Timestamp bound, _constraint.value<knowCore::Timestamp>());
      cres_try(knowCore::Timestamp value, _node->literal().value<knowCore::Timestamp>());
      switch(_constraint.type())
      {
      case CT::MaxExclusive:
        valid = value < bound;
        break;
      case CT::MinExclusive:
        valid = value > bound;
        break;
      case CT::MaxInclusive:
        valid = value <= bound;
        break;
      case CT::MinInclusive:
        valid = value >= bound;
        break;
      default:
        clog_fatal("Impossible");
      }
    }
    if(not valid)
    {
      knowCore::Uri constraint;
      switch(_constraint.type())
      {
      case CT::MaxExclusive:
        constraint = u_sh::MaxExclusiveConstraintComponent;
        break;
      case CT::MinExclusive:
        constraint = u_sh::MinExclusiveConstraintComponent;
        break;
      case CT::MaxInclusive:
        constraint = u_sh::MaxInclusiveConstraintComponent;
        break;
      case CT::MinInclusive:
        constraint = u_sh::MinInclusiveConstraintComponent;
        break;
      default:
        clog_fatal("Impossible");
      }
      _results->append(createValidationResult(_focus_node, _path, Severity::Violation, constraint,
                                              _source_shape, _node));
      return cres_success(false);
    }
    return cres_success(true);
  }
  case CT::MaxLength:
  case CT::MinLength:
  case CT::Pattern:
  {
    bool valid = true;
    QString string;
    if(_node->type() == knowRDF::Node::Type::Uri)
    {
      string = _node->uri();
    }
    else
    {
      auto const& [success, value, errorMessage] = _node->literal().value<QString>();
      if(success)
      {
        string = value.value();
      }
      else
      {
        valid = false;
      }
    }
    if(valid)
    {
      switch(_constraint.type())
      {
      case CT::MaxLength:
      {
        cres_try(int bound, _constraint.value<int>());
        valid = string.length() <= bound;
      }
      break;
      case CT::MinLength:
      {
        cres_try(int bound, _constraint.value<int>());
        valid = string.length() >= bound;
      }
      break;
      case CT::Pattern:
        valid = _constraint.pattern().match(string).hasMatch();
        break;
      default:
        clog_fatal("Impossible");
      }
    }
    if(not valid)
    {
      knowCore::Uri constraint;
      switch(_constraint.type())
      {
      case CT::MaxLength:
        constraint = u_sh::MaxLengthConstraintComponent;
        break;
      case CT::MinLength:
        constraint = u_sh::MinLengthConstraintComponent;
        break;
      case CT::Pattern:
        constraint = u_sh::PatternConstraintComponent;
        break;
      default:
        clog_fatal("Impossible");
      }
      _results->append(createValidationResult(_focus_node, _path, Severity::Violation, constraint,
                                              _source_shape, _node));
      return cres_success(false);
    }
    return cres_success(true);
  }
  case CT::NodeKind:
  {
    cres_try(knowCore::Uri uri_kind, _constraint.value<knowCore::Uri>());
    bool valid = false;
    switch(_node->type())
    {
    case knowRDF::Node::Type::Uri:
      valid = (uri_kind == u_sh::IRI or uri_kind == u_sh::BlankNodeOrIRI
               or uri_kind == u_sh::IRIOrLiteral);
      break;
    case knowRDF::Node::Type::Literal:
      valid = (uri_kind == u_sh::Literal or uri_kind == u_sh::BlankNodeOrLiteral
               or uri_kind == u_sh::IRIOrLiteral);
      break;
    case knowRDF::Node::Type::BlankNode:
      valid = (uri_kind == u_sh::BlankNode or uri_kind == u_sh::BlankNodeOrIRI
               or uri_kind == u_sh::BlankNodeOrLiteral);
      break;
    case knowRDF::Node::Type::Variable:
    case knowRDF::Node::Type::Undefined:
      return cres_failure("Variable or Undefined are not supported.");
    }

    if(not valid)
    {
      _results->append(createValidationResult(_focus_node, _path, Severity::Violation,
                                              u_sh::NodeKindConstraintComponent, _source_shape,
                                              _node));
      return cres_success(false);
    }
    return cres_success(true);
  }
  case CT::Node:
  {
    QList<ValidationResult> results;
    cres_try(bool valid, validate(_node, _constraint.node(), &results, _source_shape));
    if(d->extendedResults)
    {
      _results->append(results);
    }
    if(not valid)
    {
      _results->append(createValidationResult(_focus_node, _path, Severity::Violation,
                                              u_sh::NodeConstraintComponent, _source_shape, _node));
      return cres_success(false);
    }
    return cres_success(true);
  }
  case CT::And:
  {
    QList<ValidationResult> results;
    bool s = true;
    for(const NodeShape& ss : _constraint.nodes())
    {
      cres_try(bool vr, validate(_node, ss, &results, _source_shape));
      s = s and vr;
      if(d->extendedResults)
      {
        _results->append(results);
      }
    }
    if(not s)
    {
      _results->append(createValidationResult(_focus_node, _path, Severity::Violation,
                                              u_sh::AndConstraintComponent, _source_shape, _node));
      return cres_success(false);
    }
    return cres_success(true);
  }
  case CT::Or:
  {
    QList<ValidationResult> results;
    bool s = false;
    for(const NodeShape& ss : _constraint.nodes())
    {
      QList<ValidationResult> tmp_results;
      cres_try(bool vr, validate(_node, ss, &tmp_results, _source_shape));
      if(vr)
      {
        results = tmp_results;
        s = true;
        break;
      }
      else
      {
        if(d->extendedResults)
        {
          results.append(tmp_results);
        }
      }
    }
    if(d->extendedResults)
    {
      _results->append(results);
    }
    if(not s)
    {
      _results->append(createValidationResult(_focus_node, _path, Severity::Violation,
                                              u_sh::OrConstraintComponent, _source_shape, _node));
      return cres_success(false);
    }
    return cres_success(true);
  }
  case CT::Not:
  {
    QList<ValidationResult> results;
    cres_try(bool vr, validate(_node, _constraint.nodes().first(), &results, _source_shape));
    if(d->extendedResults)
    {
      _results->append(results);
    }
    if(vr)
    {
      _results->append(createValidationResult(_focus_node, _path, Severity::Violation,
                                              u_sh::NotConstraintComponent, _source_shape, _node));
      return cres_success(false);
    }
    return cres_success(true);
  }
  case CT::XOne:
  {
    QList<ValidationResult> results;
    int count = 0;
    for(const NodeShape& ss : _constraint.nodes())
    {
      cres_try(bool vr, validate(_node, ss, &results, _source_shape));
      if(vr)
      {
        ++count;
      }
    }
    if(d->extendedResults)
    {
      _results->append(results);
    }
    if(count != 1)
    {
      _results->append(createValidationResult(_focus_node, _path, Severity::Violation,
                                              u_sh::XoneConstraintComponent, _source_shape, _node));
      return cres_success(false);
    }
    return cres_success(true);
  }
  }

  return cres_failure("Unknown constraint type");
}

cres_qresult<bool> Validator::validate(const knowRDF::Node* _node, const PropertyShape& _shape,
                                       QList<ValidationResult>* _results,
                                       const knowCore::Uri& _source_shape)
{
  knowCore::Uri source_shape = coalesce(_shape.uri(), _source_shape);
  bool valid = true;
  QList<const knowRDF::Node*> nodes = follow(_node, _shape.path());
  for(const Constraint& c : _shape.constraints())
  {
    using CT = Constraint::Type;

    switch(c.type())
    {
    case CT::DefaultValue:
    {
      // Default value are non-validating
      break;
    }
    case CT::Empty:
      break;
    case CT::MaxCount:
    {
      cres_try(int mc, c.value<int>());
      if(nodes.size() > mc)
      {
        valid = false;
        _results->append(createValidationResult(_node, _shape.path(), Severity::Violation,
                                                u_sh::MaxCountConstraintComponent, source_shape,
                                                knowCore::Value()));
      }
      break;
    }
    case CT::MinCount:
    {
      cres_try(int mc, c.value<int>());
      if(nodes.size() < mc)
      {
        valid = false;
        _results->append(createValidationResult(
          _node, _shape.path(), Severity::Violation, u_sh::MinCountConstraintComponent,
          source_shape, knowCore::Value())); // Somehow count value is not needed by the spec
      }
      break;
    }
    case CT::LessThan:
    case CT::LessThanOrEquals:
    {
      cres_try(knowCore::Uri uri, c.value<knowCore::Uri>());
      QList<const knowRDF::Node*> rhs_nodes = _node->children(uri);

      knowCore::ComparisonOperators co(knowCore::ComparisonOperator::Inferior);
      if(c.type() == CT::LessThanOrEquals)
      {
        co |= knowCore::ComparisonOperator::Equal;
      }

      for(const knowRDF::Node* lhs_node : nodes)
      {
        for(const knowRDF::Node* rhs_node : rhs_nodes)
        {
          cres_try(knowCore::Value lhs_value, getValue(lhs_node));
          cres_try(knowCore::Value rhs_value, getValue(rhs_node));
          auto const& [success, cr, errorMessage] = lhs_value.compare(rhs_value, co);
          if(not cr.value() or not success)
          {
            valid = false;
            knowCore::Uri validationUri;
            switch(c.type())
            {
            case CT::LessThan:
              validationUri = u_sh::LessThanConstraintComponent;
              break;
            case CT::LessThanOrEquals:
              validationUri = u_sh::LessThanOrEqualsConstraintComponent;
              break;
            default:
              clog_fatal("Impossible: internal error");
            }
            _results->append(createValidationResult(_node, _shape.path(), Severity::Violation,
                                                    validationUri, source_shape, lhs_node));
          }
        }
      }
      break;
    }
    case CT::QualifiedValue:
    {
      int count = 0;
      NodeShape validatingShape = c.node();
      QList<ValidationResult> results;
      for(const knowRDF::Node* node : nodes)
      {
        cres_try(bool v, validate(node, validatingShape, &results, source_shape));
        if(v)
        {
          if(c.qualifiedDisjoint())
          {
            for(const PropertyShape& sibbling_ps : _shape.sibblings())
            {
              NodeShape other_validatingShape;
              for(const Constraint& other_c : sibbling_ps.constraints())
              {
                if(other_c.type() == Constraint::Type::QualifiedValue)
                {
                  other_validatingShape = other_c.node();
                }
              }
              QList<ValidationResult> ignoredResults;
              cres_try(bool vo,
                       validate(node, other_validatingShape, &ignoredResults, node->uri()));
              if(vo)
              {
                v = false;
                break;
              }
            }
          }
          if(v)
          {
            ++count;
          }
        }
      }
      bool violateMin = (c.qualifiedMinCount() and count < *c.qualifiedMinCount());
      bool violateMax = (c.qualifiedMaxCount() and count > *c.qualifiedMaxCount());
      if(violateMin or violateMax)
      {
        if(d->extendedResults)
        {
          _results->append(results);
        }
        if(violateMin)
        {
          _results->append(createValidationResult(_node, _shape.path(), Severity::Violation,
                                                  u_sh::QualifiedMinCountConstraintComponent,
                                                  source_shape, knowCore::Value()));
        }
        if(violateMax)
        {
          _results->append(createValidationResult(_node, _shape.path(), Severity::Violation,
                                                  u_sh::QualifiedMaxCountConstraintComponent,
                                                  source_shape, knowCore::Value()));
        }
        valid = false;
      }
      break;
    }
    case CT::UniqueLang:
    {
      QStringList langs, duplicated;
      for(const knowRDF::Node* lhs_node : nodes)
      {
        if(lhs_node->type() == knowRDF::Node::Type::Literal)
        {
          QString lang = lhs_node->literal().lang();
          if(langs.contains(lang) and lang != "")
          {
            if(not duplicated.contains(lang))
            {
              duplicated.append(lang);
              valid = false;
              _results->append(createValidationResult(_node, _shape.path(), Severity::Violation,
                                                      u_sh::UniqueLangConstraintComponent,
                                                      source_shape, knowCore::Value()));
            }
          }
          else
          {
            langs.append(lang);
          }
        }
      }
      break;
    }
    case CT::Equals:
    {
      cres_try(knowCore::Uri child_uri, c.value<knowCore::Uri>());
      QList<const knowRDF::Node*> set_1 = nodes;
      QList<const knowRDF::Node*> set_2 = _node->children(child_uri);

      for(int i = 0; i < 2; ++i)
      {
        for(const knowRDF::Node* node : set_1)
        {
          if(set_2.isEmpty())
          {
            _results->append(createValidationResult(_node, _shape.path(), Severity::Violation,
                                                    u_sh::EqualsConstraintComponent, source_shape,
                                                    node));
            valid = false;
          }
          else
          {
            QList<ValidationResult> results;
            bool hasOneValid = false;
            for(const knowRDF::Node* cnode : set_2)
            {
              if(cnode->equal(node))
              {
                hasOneValid = true;
                break;
              }
              else
              {
                results.append(createValidationResult(_node, _shape.path(), Severity::Violation,
                                                      u_sh::EqualsConstraintComponent, source_shape,
                                                      node));
              }
            }
            if(not hasOneValid)
            {
              _results->append(results);
              valid = false;
            }
          }
        }
        std::swap(set_1, set_2);
      }
      break;
    }
    case CT::HasValue:
    {
      bool oneIsValid = false;
      QList<ValidationResult> tmp_results;
      for(const knowRDF::Node* node : nodes)
      {
        cres_try(bool v, validate(node, c, &tmp_results, _node, _shape.path(), source_shape));
        if(v)
        {
          oneIsValid = true;
          break;
        }
      }
      if(not oneIsValid)
      {
        if(d->extendedResults)
        {
          _results->append(tmp_results);
        }
        _results->append(createValidationResult(
          _node, _shape.path(), Severity::Violation, u_sh::HasValueConstraintComponent,
          source_shape, knowCore::Value())); // somehow the spec says no value should be added

        valid = false;
      }
    }
    break;
    case CT::Class:
    case CT::Datatype:
    case CT::Disjoint:
    case CT::Node:
    case CT::In:
    case CT::LanguageIn:
    case CT::NodeKind:
    case CT::MaxExclusive:
    case CT::MinExclusive:
    case CT::MaxInclusive:
    case CT::MinInclusive:
    case CT::MaxLength:
    case CT::MinLength:
    case CT::Pattern:
    case CT::Property:
    case CT::And:
    case CT::Or:
    case CT::Not:
    case CT::XOne:
      for(const knowRDF::Node* node : nodes)
      {
        cres_try(bool v, validate(node, c, _results, _node, _shape.path(), source_shape));
        valid = valid and v;
      }
      break;
    }
  }
  return cres_success(valid);
}

cres_qresult<bool> Validator::validate(const knowRDF::Node* _node, const NodeShape& _shape,
                                       QList<ValidationResult>* _results,
                                       const knowCore::Uri& _source_shape)
{
  bool valid = true;
  for(const Constraint& c : _shape.constraints())
  {
    using CT = Constraint::Type;
    switch(c.type())
    {
    case CT::Equals:
    {
      cres_try(knowCore::Uri child_uri, c.value<knowCore::Uri>());
      for(const knowRDF::Node* cnode : _node->children(child_uri))
      {
        if(not cnode->equal(_node))
        {
          _results->append(createValidationResult(_node, Path(), Severity::Violation,
                                                  u_sh::EqualsConstraintComponent, _source_shape,
                                                  cnode));
          valid = false;
        }
      }
      if(_node->children(child_uri).isEmpty())
      {
        _results->append(createValidationResult(_node, Path(), Severity::Violation,
                                                u_sh::EqualsConstraintComponent, _source_shape,
                                                _node));
        valid = false;
      }
      break;
    }
    default:
    {
      cres_try(bool cv, validate(_node, c, _results, _node, Path(), _source_shape));
      valid = valid and cv;
      break;
    }
    }
  }
  if(_shape.isClosed())
  {
    QMultiHash<knowCore::Uri, const knowRDF::Node*> children = _node->children();
    for(QMultiHash<knowCore::Uri, const knowRDF::Node*>::iterator it = children.begin();
        it != children.end(); ++it)
    {
      if(not _shape.ignoredProperties().contains(it.key()))
      {
        bool hasMatch = false;
        QList<Path> paths;
        collectPath(&paths, _shape);
        for(const Path& p : paths)
        {
          if(p.match(it.key()))
          {
            hasMatch = true;
            break;
          }
        }
        if(not hasMatch)
        {
          valid = false;
          Path p;
          p.d = new Path::Private;
          p.d->type = Path::Type::Predicate;
          p.d->predicate = it.key();
          _results->append(createValidationResult(_node, p, Severity::Violation,
                                                  u_sh::ClosedConstraintComponent, _shape.uri(),
                                                  it.value()));
        }
      }
    }
  }
  return cres_success(valid);
}

cres_qresult<ValidationResults> Validator::validate(const knowRDF::Graph& _graph)
{
  ValidationResults vrs;
  vrs.d = new ValidationResults::Private;
  vrs.d->success = true;

  for(const knowRDF::Node* node : _graph.nodes())
  {
    QList<NodeShape> shapes = d->def.shapesFor(node);
    if(shapes.size() > 0)
    {
      // Try to validate
      for(const NodeShape& shape : shapes)
      {
        cres_try(bool succ, validate(node, shape, &vrs.d->results, shape.uri()));
        vrs.d->success = vrs.d->success and succ;
      }
    }
  }

  return cres_success(vrs);
}

cres_qresult<Validator> Validator::create(const knowRDF::Graph& _graph)
{
  Validator v;
  v.d = new Validator::Private;
  cres_try(v.d->def, Definition::create(_graph));

  return cres_success(v);
}

cres_qresult<Validator> Validator::create(const QUrl& _validator_url, const QString& _format)
{
  Validator v;
  v.d = new Validator::Private;
  cres_try(v.d->def, Definition::create(_validator_url, _format));

  return cres_success(v);
}

cres_qresult<Validator> Validator::create(const knowCore::Uri& _validator_url,
                                          const QString& _format)
{
  return create(QUrl(_validator_url), _format);
}
