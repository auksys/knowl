#ifndef VALIDATOR_H_INCLUDED
#define VALIDATOR_H_INCLUDED

#include <QExplicitlySharedDataPointer>

#include <knowCore/FileFormat.h>
#include <knowCore/Value.h>

#include "Forward.h"
#include "Severity.h"

namespace knowSHACL
{
  /**
   * @ingroup knowSHACL
   * Validator for SHACL as defined in https://www.w3.org/TR/shacl/ plus support for some
   * extensions.
   */
  class Validator
  {
  public:
    Validator();
    Validator(const Validator& _rhs);
    Validator& operator=(const Validator& _rhs);
    ~Validator();
    /**
     * @return the definition
     */
    Definition definition() const;
    /**
     * @return the result of validating \p _graph
     */
    cres_qresult<ValidationResults> validate(const knowRDF::Graph& _graph);
    /**
     * @return the result of validating \p _graph
     */
    cres_qresult<ValidationResults> validate(const QUrl& _graph_uri,
                                             const QString& _format = knowCore::FileFormat::Turtle);
    /**
     * @return the result of validating \p _graph
     */
    cres_qresult<ValidationResults> validate(QIODevice* _graph_data, const knowCore::Uri& _base,
                                             const QString& _format = knowCore::FileFormat::Turtle);
    /**
     * @return the result of validating \p _graph
     */
    cres_qresult<ValidationResults> validate(const knowCore::Uri& _graph_uri,
                                             const QString& _format = knowCore::FileFormat::Turtle);
  public:
    /**
     * Create a \ref Validator according to the definition given in \ref _validator_def.
     */
    static cres_qresult<Validator> create(const QString& _validator_def,
                                          const QString& _format = knowCore::FileFormat::Turtle);
    /**
     * Create a \ref Validator according to the definition loaded from \ref _validator_url.
     */
    static cres_qresult<Validator> create(const QUrl& _validator_url,
                                          const QString& _format = knowCore::FileFormat::Turtle);
    /**
     * Create a \ref Validator according to the definition loaded from \ref _validator_url.
     */
    static cres_qresult<Validator> create(const knowCore::Uri& _validator_url,
                                          const QString& _format = knowCore::FileFormat::Turtle);
    /**
     * Create a validator from a RDF graph
     */
    static cres_qresult<Validator> create(const knowRDF::Graph& _graph);
  private:
    QList<const knowRDF::Node*> follow(const knowRDF::Node* _node, const Path& _path);
    cres_qresult<bool> validate(const knowRDF::Node* _node, const Constraint& _shape,
                                QList<ValidationResult>* _results, const knowRDF::Node* _focus_node,
                                const Path& _path, const knowCore::Uri& _source_shape);
    cres_qresult<bool> validate(const knowRDF::Node* _node, const PropertyShape& _shape,
                                QList<ValidationResult>* _results,
                                const knowCore::Uri& _source_shape);
    cres_qresult<bool> validate(const knowRDF::Node* _node, const NodeShape& _shape,
                                QList<ValidationResult>* _results,
                                const knowCore::Uri& _source_shape);
    ValidationResult createValidationResult(const knowRDF::Node* _node, const Path& _path,
                                            Severity _severity,
                                            const knowCore::Uri& _sourceConstraintComponent,
                                            const knowCore::Uri& _sourceShape,
                                            const knowRDF::Node* _value);
    ValidationResult createValidationResult(const knowRDF::Node* _node, const Path& _path,
                                            Severity _severity,
                                            const knowCore::Uri& _sourceConstraintComponent,
                                            const knowCore::Uri& _sourceShape,
                                            const knowCore::Value& _value);
    template<typename _T_>
    ValidationResult createValidationResult(const knowRDF::Node* _node, const Path& _path,
                                            Severity _severity,
                                            const knowCore::Uri& _sourceConstraintComponent,
                                            const knowCore::Uri& _sourceShape, const _T_& _value);
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace knowSHACL

#endif // VALIDATOR_H_INCLUDED
