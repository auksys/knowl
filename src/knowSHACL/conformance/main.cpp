#include <clog_print>

#include <QCommandLineParser>
#include <QFile>

#include <knowCore/Uris/mf.h>

#include <clog_qt>
#include <knowCore/Messages.h>
#include <knowCore/Uri.h>

#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/rdfs.h>
#include <knowRDF/Graph.h>
#include <knowRDF/Node.h>
#include <knowRDF/Object.h>
#include <knowRDF/Skolemisation.h>
#include <knowRDF/TripleStream.h>

#include <knowSHACL/Path.h>
#include <knowSHACL/ValidationResult.h>
#include <knowSHACL/ValidationResults.h>
#include <knowSHACL/Validator.h>

#include <knowSHACL/Uris/sht.h>

bool compareSourceShape(const knowCore::Uri& _a, const knowCore::Uri& _b)
{
  return (_a == _b)
         or (knowRDF::isBlankNodeSkolemisation(_a) and knowRDF::isBlankNodeSkolemisation(_b)
             and knowRDF::parseBlankNodeUri(_a).second == knowRDF::parseBlankNodeUri(_b).second);
}

bool compareFocusNode(const knowRDF::Object& _a, const knowRDF::Object& _b)
{
  return _a == _b
         or (_a.type() == knowRDF::Object::Type::Uri and _b.type() == knowRDF::Object::Type::Uri
             and compareSourceShape(_a.uri(), _b.uri()))
         or (_a.type() == knowRDF::Object::Type::BlankNode
             and _b.type() == knowRDF::Object::Type::BlankNode
             and _a.blankNode().label() == _b.blankNode().label());
}

int main(int _argc, char** _argv)
{

  QStringList args;
  for(int i = 0; i < _argc; ++i)
  {
    args.append(_argv[i]);
  }

  QCommandLineParser parser;
  parser.setApplicationDescription("knowSHACL Conformance tool");
  parser.addHelpOption();
  parser.addOption({"skip", "file with test case to skip.", "skip"});
  parser.addPositionalArgument("root", "root directory of the conformance suite (required)");
  parser.addPositionalArgument("testfile", "name of the test file (required)");

  parser.process(args);

  if(parser.positionalArguments().length() != 2)
  {
    parser.showHelp(-1);
  }

  // BEGIN read skip files
  QStringList ignorelist;
  for(const QString& skipfile : parser.values("skip"))
  {
    QFile ignorefile(skipfile);
    if(not ignorefile.open(QIODevice::ReadOnly))
    {
      clog_fatal("Failed to open {}", _argv[2]);
    }

    while(not ignorefile.atEnd())
    {
      QString str = ignorefile.readLine();
      if(str.right(1) == "\n")
      {
        str.chop(1);
      }
      ignorelist << str;
    }
  }
  // END read skip files

  //
  QString root = clog_qt::qformat("{}/", parser.positionalArguments()[0]);

  // Parse the test file
  QString manifest_filename = root + parser.positionalArguments()[1];
  knowCore::Uri manifest_uri(manifest_filename);

  QFile manifest_file(manifest_filename);

  if(not manifest_file.open(QIODevice::ReadOnly))
  {
    clog_fatal("Failed to open {}", manifest_filename);
  }

  knowRDF::TripleStream manifest_filestream;
  manifest_filestream.setBase(manifest_uri);
  knowRDF::Graph manifest_graph;
  manifest_filestream.addListener(&manifest_graph);

  knowCore::Messages messages;

  if(not manifest_filestream.start(&manifest_file, &messages).is_successful())
  {
    clog_fatal("Failed to parse: {}: {}", manifest_filename, messages.toString());
  }

  // Get the manifest node
  const knowRDF::Node* manifest_node = manifest_graph.getNode(manifest_uri);

  if(not manifest_node)
  {
    clog_fatal("No manifest node for {}", manifest_uri);
  }

  // Get the test suite name
  const knowRDF::Node* manifest_name_node
    = manifest_node->getFirstChild(knowCore::Uris::rdfs::label);

  if(not manifest_name_node)
  {
    manifest_name_node = manifest_node->getFirstChild(knowCore::Uris::rdfs::comment);
  }
  if(not manifest_name_node)
  {
    clog_fatal("No name for manifest node");
  }

  clog_print("Running tests for '{}{}{}'", clog_print_flag::green,
             manifest_name_node->literal().value<QString>().expect_success(),
             clog_print_flag::reset);

  QList<QPair<knowRDF::Graph*, const knowRDF::Node*>> test_cases;

  // BEGIN go through included files to extract test cases
  for(const knowRDF::Node* included_node : manifest_node->children(knowCore::Uris::mf::include))
  {
    knowCore::Uri test_case_uri = included_node->uri();

    bool skip = false;
    for(const QString& ignore : ignorelist)
    {
      if(QString(test_case_uri).endsWith(ignore))
      {
        skip = true;
      }
    }
    if(skip)
    {
      clog_print<clog_print_flag::yellow>("Skipping '{}'", test_case_uri);
      continue;
    }
    // Open test case
    QFile test_case_file(test_case_uri);

    if(not test_case_file.open(QIODevice::ReadOnly))
    {
      clog_fatal("Failed to open {}", test_case_uri);
    }

    // BEGIN Parse included file
    knowRDF::TripleStream test_case_filestream;
    test_case_filestream.setBase(test_case_uri);
    knowRDF::Graph* test_case_graph = new knowRDF::Graph();
    test_case_filestream.addListener(test_case_graph);

    knowCore::Messages messages;

    if(not test_case_filestream.start(&test_case_file, &messages).is_successful())
    {
      clog_fatal("Failed to parse: {}: {}", test_case_uri, messages.toString());
    }
    // END Parse included file

    // BEGIN Get the manifest node
    const knowRDF::Node* test_case_node = test_case_graph->getNode(test_case_uri);

    if(not test_case_node)
    {
      clog_fatal("No test case node for {}", test_case_uri);
    }
    // END Get the manifest node

    QList<const knowRDF::Node*> entries
      = test_case_node->getFirstChild(knowCore::Uris::mf::entries)->getCollection();
    for(const knowRDF::Node* entry : entries)
    {
      test_cases.append({test_case_graph, entry});
    }
  }

  // END go through included files to extract test cases

  // BEGIN Execute the test cases
  int successfullCount = 0;
  int failedCount = 0;
  QStringList failed_tests;
  for(int i = 0; i < test_cases.size(); ++i)
  {
    const knowRDF::Node* test_case_node = test_cases[i].second;
    const knowCore::Uri test_case_uri = test_case_node->uri();

    // Get the test suite name
    const knowRDF::Node* test_case_name_node
      = test_case_node->getFirstChild(knowCore::Uris::rdfs::label);

    if(not test_case_name_node)
    {
      test_case_name_node = test_case_node->getFirstChild(knowCore::Uris::rdfs::comment);
    }
    if(not test_case_name_node)
    {
      clog_fatal("No name for test_case node in {}", test_case_uri);
    }

    QString test_case_name = test_case_name_node->literal().value<QString>().expect_success();

    if(ignorelist.contains(test_case_name))
    {
      clog_print<clog_print_flag::bold>(
        "- Skip test '{}{}{}' @ '{}' ({}/{})", clog_print_flag::yellow, test_case_name,
        clog_print_flag::default_color, test_case_uri, i + 1, test_cases.size());
      continue;
    }
    else
    {
      clog_print<clog_print_flag::bold>(
        "- Running test '{}{}{}' @ '{}' ({}/{})", clog_print_flag::green, test_case_name,
        clog_print_flag::default_color, test_case_uri, i + 1, test_cases.size());
    }
    // BEGIN get the expected result
    const knowRDF::Node* test_case_result_node
      = test_case_node->getFirstChild(knowCore::Uris::mf::result);
    if(not test_case_result_node)
    {
      clog_fatal("Missing result node");
    }
    // END get the expected result

    // BEGIN get the action
    knowCore::Uri dataGraph, shapesGraph;

    const knowRDF::Node* test_case_action_node
      = test_case_node->getFirstChild(knowCore::Uris::mf::action);
    if(not test_case_action_node)
    {
      clog_fatal("Missing action node");
    }
    {
      const knowRDF::Node* test_case_data_graph_node
        = test_case_action_node->getFirstChild(knowSHACL::Uris::sht::dataGraph);
      if(not test_case_data_graph_node)
      {
        clog_fatal("Missing data graph");
      }
      dataGraph = test_case_data_graph_node->uri();
      const knowRDF::Node* test_case_shapes_graph_node
        = test_case_action_node->getFirstChild(knowSHACL::Uris::sht::shapesGraph);
      if(not test_case_shapes_graph_node)
      {
        clog_fatal("Missing shapes graph");
      }
      shapesGraph = test_case_shapes_graph_node->uri();
    }

    // END get the action
    //  Load test case
    knowSHACL::Validator validator = knowSHACL::Validator::create(shapesGraph).expect_success();
    knowSHACL::ValidationResults know_result = validator.validate(dataGraph).expect_success();
    knowSHACL::ValidationResults expected_result
      = knowSHACL::ValidationResults::load(test_case_result_node).expect_success();

    // BEGIN compare results

    bool successfull = true;

    if(know_result.isSuccessful() != expected_result.isSuccessful())
    {
      clog_print<clog_print_flag::red>("Expected '{}' but got '{}'",
                                       (expected_result.isSuccessful() ? "successfull" : "failed"),
                                       (know_result.isSuccessful() ? "successfull" : "failed"));
      successfull = false;
    }
    if(know_result.results().size() != expected_result.results().size())
    {
      clog_print<clog_print_flag::red>("Expected {} results but got {} results.",
                                       expected_result.results().size(),
                                       know_result.results().size());
      successfull = false;
    }
    for(const knowSHACL::ValidationResult& know_vr : know_result.results())
    {
      bool found = false;
      for(const knowSHACL::ValidationResult& expected_vr : expected_result.results())
      {
        if(compareFocusNode(know_vr.focusNode(), expected_vr.focusNode())
           and know_vr.resultPath() == expected_vr.resultPath()
           and know_vr.severity() == expected_vr.severity()
           and know_vr.sourceConstraintComponent() == expected_vr.sourceConstraintComponent()
           and compareSourceShape(know_vr.sourceShape(), expected_vr.sourceShape())
           and know_vr.value() == expected_vr.value())
        {
          found = true;
          break;
        }
      }
      if(not found)
      {
        clog_print<clog_print_flag::red>(R"(Could not find result for actual result:
 - focusNode: {}
 - resultPath: {}
 - severity: {}
 - sourceConstraintComponent: {}
 - sourceShape: {}
 - value: {})",
                                         know_vr.focusNode(), know_vr.resultPath(),
                                         know_vr.severity(), know_vr.sourceConstraintComponent(),
                                         know_vr.sourceShape(), know_vr.value());
        successfull = false;
      }
    }
    for(const knowSHACL::ValidationResult& expected_vr : expected_result.results())
    {
      bool found = false;
      for(const knowSHACL::ValidationResult& know_vr : know_result.results())
      {
        if(compareFocusNode(know_vr.focusNode(), expected_vr.focusNode())
           and know_vr.resultPath() == expected_vr.resultPath()
           and know_vr.severity() == expected_vr.severity()
           and know_vr.sourceConstraintComponent() == expected_vr.sourceConstraintComponent()
           and compareSourceShape(know_vr.sourceShape(), expected_vr.sourceShape())
           and know_vr.value() == expected_vr.value())
        {
          found = true;
          break;
        }
      }
      if(not found)
      {
        clog_print<clog_print_flag::red>(R"(Could not find result for expected result:
 - focusNode: {}
 - resultPath: {}
 - severity: {}
 - sourceConstraintComponent: {}
 - sourceShape: {}
 - value: {})",
                                         expected_vr.value(), expected_vr.focusNode(),
                                         expected_vr.resultPath(), expected_vr.severity(),
                                         expected_vr.sourceConstraintComponent(),
                                         expected_vr.sourceShape(), expected_vr.value());
        successfull = false;
      }
    }

    if(successfull)
    {
      ++successfullCount;
    }
    else
    {
      ++failedCount;
      failed_tests.append(test_case_name);
    }
    // END compare results
  }
  // END Execute the test cases

  if(failedCount == 0)
  {
    clog_print<clog_print_flag::green>("All {} tests have succeed", successfullCount);
  }
  else
  {
    clog_print<clog_print_flag::red | clog_print_flag::bold>(
      "{} tests have failed on a total of {}", failedCount, (failedCount + successfullCount));
    clog_print<clog_print_flag::red>("Failed tests: {}", failed_tests.join(", "));
  }

  return failedCount;
}
