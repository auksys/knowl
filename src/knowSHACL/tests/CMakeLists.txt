
add_executable(TestDefinition TestDefinition.cpp)
target_link_libraries(TestDefinition knowSHACL Qt6::Test)
add_test(NAME TEST-SHACL-TestDefinition COMMAND TestDefinition)
