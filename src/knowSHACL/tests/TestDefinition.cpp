#include "TestDefinition.h"

#include "../Definition.h"

#include <knowCore/Test.h>

void TestDefinition::testStandardDefinition()
{
  knowSHACL::Definition def = CRES_QVERIFY(
    knowSHACL::Definition::create(QUrl("qrc:///knowshacl/data/standard_validator.ttl")));
  QVERIFY(def.isValid());
}

QTEST_MAIN(TestDefinition)
