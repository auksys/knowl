#include <knowCore/Global.h>
#include <knowGIS/Forward.h>

#define KNOWVALUES_VALUES_LIST                                                                     \
  Value, PoseValue, RegionValue, LidarScan, PointCloudField, PointCloud, Lidar3DScan, CameraInfo,  \
    Image, SalientRegion

namespace knowValues::Values
{
  namespace details
  {
    template<typename _T_>
    class Value;
  }
#define __KNOWVALUES_DECLARE_VALUE(_VALUE_, _I_)                                                   \
  namespace Definitions                                                                            \
  {                                                                                                \
    class _VALUE_;                                                                                 \
  }                                                                                                \
  using _VALUE_ = details::Value<Definitions::_VALUE_>;

#define __KNOWVALUES_DECLARE_VALUES(...) KNOWCORE_FOREACH(__KNOWVALUES_DECLARE_VALUE, __VA_ARGS__)

  __KNOWVALUES_DECLARE_VALUES(KNOWVALUES_VALUES_LIST)

#undef __KNOWVALUES_DECLARE_VALUES
#undef __KNOWVALUES_DECLARE_VALUE
} // namespace knowValues::Values
