#include <knowCore/Uris/Uris.h>

#define KNOWVALUES_ASKCORE_SENSING_URIS(F, ...)                                                    \
  F(__VA_ARGS__, images)                                                                           \
  F(__VA_ARGS__, image_frame)                                                                      \
  F(__VA_ARGS__, lidar_scan)                                                                       \
  F(__VA_ARGS__, lidar3d_scan)                                                                     \
  F(__VA_ARGS__, point_cloud)                                                                      \
  F(__VA_ARGS__, point_density)                                                                    \
  F(__VA_ARGS__, pose_value)                                                                       \
  F(__VA_ARGS__, region_value)                                                                     \
  F(__VA_ARGS__, salient_region)                                                                   \
  F(__VA_ARGS__, salient_region_collection)                                                        \
  F(__VA_ARGS__, value)

KNOWCORE_ONTOLOGY_URIS(knowValues::Uris, KNOWVALUES_ASKCORE_SENSING_URIS, askcore_sensing,
                       "http://askco.re/sensing#")
