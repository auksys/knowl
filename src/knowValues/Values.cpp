#include "Values.h"

#include <knowValues/Uris/askcore_sensing.h>

#include <knowCore/MetaTypeImplementation.h>

namespace knowValues::Values::Definitions
{

  ::Cartography::CoordinateSystem PointCloud::pointsCoordinateSystem() const
  {
    if(coordinateSystem().isValid())
    {
      return coordinateSystem();
    }
    else if(transformation().isSet())
    {
      return ::Cartography::CoordinateSystem::utm(transformation().position());
    }
    else
    {
      clog_warning("No coordinate system set for point cloud, assuming WGS84.");
      return Cartography::CoordinateSystem::wgs84;
    }
  }
  ::Cartography::CoordinateSystem Lidar3DScan::pointsCoordinateSystem() const
  {
    return ::Cartography::CoordinateSystem::utm(pose().position());
  }

} // namespace knowValues::Values::Definitions

template<typename _TValue_>
KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION_TEMPLATE(_TValue_)
cres_qresult<QByteArray> md5(const _TValue_& _value) const override
{
  return cres_success(_value->hash(QCryptographicHash::Md5));
}
cres_qresult<QJsonValue> toJsonValue(const _TValue_& _value,
                                     const SerialisationContexts& _contexts) const override
{
  Q_UNUSED(_contexts);
  return cres_success(_value->toJson());
}
cres_qresult<void> fromJsonValue(_TValue_* _value, const QJsonValue& _json_value,
                                 const DeserialisationContexts& _contexts) const override
{
  Q_UNUSED(_contexts);
  return cres_try_assign(_value, _TValue_::fromJsonValue(_json_value));
}
cres_qresult<QCborValue> toCborValue(const _TValue_& _value,
                                     const SerialisationContexts& _contexts) const override
{
  Q_UNUSED(_contexts);
  return cres_success(_value->toCbor());
}
cres_qresult<void> fromCborValue(_TValue_* _value, const QCborValue& _cbor_value,
                                 const DeserialisationContexts& _contexts) const override
{
  Q_UNUSED(_contexts);
  return cres_try_assign(_value, _TValue_::fromCborValue(_cbor_value));
}
cres_qresult<QString> printable(const _TValue_& _value) const override
{
  return cres_success(QString::fromUtf8(QJsonDocument(_value->toJson()).toJson()));
}
cres_qresult<QString> toRdfLiteral(const _TValue_& _value,
                                   const SerialisationContexts& _contexts) const override
{
  return toJsonString(_value, _contexts);
}
cres_qresult<void> fromRdfLiteral(_TValue_* _value, const QString& _serialised,
                                  const knowCore::DeserialisationContexts& _contexts) const override
{
  return fromJsonString(_value, _serialised, _contexts);
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(_TValue_)

template<typename _TFrom_>
struct knowCore::Converter<_TFrom_, knowValues::Values::Value, knowCore::TypeCheckingMode::Safe>
{
  static inline cres_qresult<void> convert(const _TFrom_* _from, knowValues::Values::Value* _to)
  {
    *_to = _from->template s_cast<knowValues::Values::Value::Definition>();
    return cres_success();
  }
};

KNOWCORE_DEFINE_METATYPE(knowValues::Values::Value, knowValues::Uris::askcore_sensing::value,
                         knowCore::MetaTypeTraits::None)
KNOWCORE_DEFINE_METATYPE(knowValues::Values::LidarScan,
                         knowValues::Uris::askcore_sensing::lidar_scan,
                         knowCore::MetaTypeTraits::None)
KNOWCORE_DEFINE_METATYPE(knowValues::Values::Lidar3DScan,
                         knowValues::Uris::askcore_sensing::lidar3d_scan,
                         knowCore::MetaTypeTraits::None)
KNOWCORE_DEFINE_METATYPE(knowValues::Values::PointCloud,
                         knowValues::Uris::askcore_sensing::point_cloud,
                         knowCore::MetaTypeTraits::None)
KNOWCORE_DEFINE_METATYPE(knowValues::Values::Image, knowValues::Uris::askcore_sensing::image_frame,
                         knowCore::MetaTypeTraits::None)
KNOWCORE_DEFINE_METATYPE(knowValues::Values::SalientRegion,
                         knowValues::Uris::askcore_sensing::salient_region,
                         knowCore::MetaTypeTraits::None)
KNOWCORE_REGISTER_CONVERSION_TO(knowValues::Values::Value, knowValues::Values::LidarScan,
                                knowValues::Values::PointCloud, knowValues::Values::Lidar3DScan,
                                knowValues::Values::SalientRegion, knowValues::Values::Image)
