#pragma once

#include <knowCore/ObjectSharedRef.h>
#include <knowCore/TypeDefinitions.h>
#include <knowValues/ValuesDef.h>

#include "Forward.h"

namespace knowValues::Values
{
  namespace details
  {
    template<typename _T_>
    class Value : public knowCore::ObjectSharedRef<_T_>
    {
    public:
      using Builder = typename _T_::Builder;
    public:
      using Definition = _T_;
      Value(_T_* _t = nullptr) : knowCore::ObjectSharedRef<_T_>(_t, true) {}
      Value(const knowCore::ObjectSharedRef<_T_>& _rhs) : knowCore::ObjectSharedRef<_T_>(_rhs) {}
      Value(const Value& _rhs) : knowCore::ObjectSharedRef<_T_>(_rhs) {}
      Value& operator=(const Value& _rhs)
      {
        knowCore::ObjectSharedRef<_T_>::operator=(_rhs);
        return *this;
      }
      Value(Builder&& _builder) : Value((_T_*)(_builder)) {}
      static Builder create() { return _T_::create(); }
    public:
      static cres_qresult<Value<_T_>> fromJsonValue(const QJsonValue& _value)
      {
        QString errMsg;
        Value value = Value(Definition::fromJson(_value.toObject(), &errMsg));
        if(value)
        {
          return cres_success(value);
        }
        else
        {
          return cres_failure("Failed to parse from Json: {}", errMsg);
        }
      }
      static cres_qresult<Value<_T_>> fromCborValue(const QCborValue& _value)
      {
        QString errMsg;
        Value value = Value(Definition::fromCbor(_value.toMap(), &errMsg));
        if(value)
        {
          return cres_success(value);
        }
        else
        {
          return cres_failure("Failed to parse from Cbor: {}", errMsg);
        }
      }
    };
    template<>
    class Value<Definitions::Value> : public knowCore::ObjectSharedRef<Definitions::Value>
    {
    public:
      using Definition = Definitions::Value;
      Value(Definitions::Value* _t = nullptr)
          : knowCore::ObjectSharedRef<Definitions::Value>(_t, true)
      {
      }
      Value(const knowCore::ObjectSharedRef<Definitions::Value>& _rhs)
          : knowCore::ObjectSharedRef<Definitions::Value>(_rhs)
      {
      }
      Value(const Value& _rhs) : knowCore::ObjectSharedRef<Definitions::Value>(_rhs) {}
      Value& operator=(const Value& _rhs)
      {
        knowCore::ObjectSharedRef<Definitions::Value>::operator=(_rhs);
        return *this;
      }
    public:
      static cres_qresult<Value<Definitions::Value>> fromJsonValue(const QJsonValue& _value)
      {
        QString errMsg;
        Value value = Value(Definition::fromJson(_value.toObject(), &errMsg));
        if(value)
        {
          return cres_success(value);
        }
        else
        {
          return cres_failure("Failed to parse from Json: {}", errMsg);
        }
      }
      static cres_qresult<Value<Definitions::Value>> fromCborValue(const QCborValue& _value)
      {
        QString errMsg;
        Value value = Value(Definition::fromCbor(_value.toMap(), &errMsg));
        if(value)
        {
          return cres_success(value);
        }
        else
        {
          return cres_failure("Failed to parse from Cbor: {}", errMsg);
        }
      }
    };
  } // namespace details
} // namespace knowValues::Values

#define __KNOWVALUES_DECLARE_VALUE(_VALUE_, _I_)                                                   \
  KNOWCORE_DECLARE_FULL_METATYPE(knowValues::Values, _VALUE_);

#define __KNOWVALUES_DECLARE_VALUES(...) KNOWCORE_FOREACH(__KNOWVALUES_DECLARE_VALUE, __VA_ARGS__)

__KNOWVALUES_DECLARE_VALUES(KNOWVALUES_VALUES_LIST)

#undef __KNOWVALUES_DECLARE_VALUES
#undef __KNOWVALUES_DECLARE_VALUE
