#include "ValuesDef.h"

using namespace knowValues::Values::Definitions;

bool PoseValue::intersects(const ::knowGIS::GeometryObject& _object)
{
  return _object.contains(pose().position());
}

bool RegionValue::intersects(const ::knowGIS::GeometryObject& _object)
{
  return geometry().intersects(_object);
}

int PointCloudField::size() const
{
  int s = 0;

  switch(dataType())
  {
  case DataType::UnsignedInteger8:
  case DataType::Integer8:
    s = 1;
    break;
  case DataType::UnsignedInteger16:
  case DataType::Integer16:
    s = 2;
    break;
  case DataType::UnsignedInteger32:
  case DataType::Integer32:
    s = 4;
    break;
  case DataType::UnsignedInteger64:
  case DataType::Integer64:
    s = 8;
    break;
  case DataType::Float32:
    s = sizeof(float);
    break;
  case DataType::Float64:
    s = sizeof(double);
    break;
  }
  return s * count();
}
