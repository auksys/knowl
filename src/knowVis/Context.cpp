#include "Context.h"

#include <Cartography/CoordinateSystem.h>
#include <knowGIS/GeoPose.h>

#include "Pose.h"

using namespace knowVis;

struct Context::Private
{
  Pose* origin;
};

Context::Context(QObject* _parent) : QObject(_parent), d(new Private) { d->origin = new Pose; }

Context::~Context() { delete d; }

Pose* Context::origin() const { return d->origin; }

Cartography::CoordinateSystem Context::coordinateSystem() const
{
  return Cartography::CoordinateSystem::utm(d->origin->geoPose().position());
}

#include "moc_Context.cpp"
