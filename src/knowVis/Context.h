#pragma once

#include <QObject>

#include "Forward.h"

namespace knowVis
{
  class Context : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(knowVis::Pose* origin READ origin CONSTANT)
  public:
    Context(QObject* _parent = nullptr);
    ~Context();
  public:
    Pose* origin() const;
    Cartography::CoordinateSystem coordinateSystem() const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowVis
