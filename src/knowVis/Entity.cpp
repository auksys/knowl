#include "Entity.h"

#include <knowVis/GeoTransform.h>

using namespace knowVis;

struct Entity::Private
{
  knowVis::GeoTransform* transform = nullptr;
};

Entity::Entity(Qt3DCore::QNode* _node) : Qt3DCore::QEntity(_node), d(new Private) {}

Entity::~Entity() {}

void Entity::setupTransform()
{
  d->transform = new GeoTransform(this);
  addComponent(d->transform);
  QObject::connect(d->transform, SIGNAL(contextChanged()), this, SIGNAL(contextChanged()));
}

GeoTransform* Entity::transform() { return d->transform; }

const GeoTransform* Entity::transform() const { return d->transform; }

knowVis::Context* Entity::context() const { return d->transform->context(); }

void Entity::setContext(knowVis::Context* _context)
{
  d->transform->setContext(_context);
  // no need to emit contextChanged, this is done in d->transform->setContext
}

#include "Context.h"

#include "moc_Entity.cpp"
