#pragma once

#include <Qt3DCore/QEntity>

#include <knowVis/Forward.h>

namespace knowVis
{
  class Entity : public Qt3DCore::QEntity
  {
    Q_OBJECT
    Q_PROPERTY(knowVis::Context* context READ context WRITE setContext NOTIFY contextChanged);
  public:
    Entity(Qt3DCore::QNode* _parent = nullptr);
    ~Entity();
    knowVis::Context* context() const;
    void setContext(knowVis::Context* _context);
  signals:
    void contextChanged();
  protected:
    GeoTransform* transform();
    const GeoTransform* transform() const;
    void setupTransform();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowVis
