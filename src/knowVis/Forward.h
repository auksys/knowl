#include <knowGIS/Forward.h>

namespace Qt3DCore
{
  class QEntity;
  class QTransform;
} // namespace Qt3DCore

namespace knowVis
{
  class Context;
  class GeoTransform;
  class Pose;
  class PointCloud;
  class PointCloudMaterial;
  class PointCloudRenderer;
} // namespace knowVis
