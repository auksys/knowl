#include "GeoTransform.h"

#include <knowGIS/Pose.h>

#include "Context.h"
#include "Pose.h"

using namespace knowVis;

struct GeoTransform::Private
{
  Pose* pose;
  Context* context = nullptr;
  QMetaObject::Connection originConnection;
  bool usePose = true;
};

GeoTransform::GeoTransform(Qt3DCore::QNode* _parent) : Qt3DCore::QTransform(_parent), d(new Private)
{
  d->pose = new Pose(this);
  QObject::connect(d->pose, SIGNAL(poseChanged()), this, SLOT(updateTransform()));
}

GeoTransform::~GeoTransform() { delete d; }

Pose* GeoTransform::pose() const { return d->pose; }

void GeoTransform::setUsePose(bool _value)
{
  d->usePose = _value;
  updateTransform();
}

Context* GeoTransform::context() const { return d->context; }

void GeoTransform::setContext(Context* _context)
{
  if(d->context)
  {
    QObject::disconnect(d->originConnection);
  }
  d->context = _context;
  if(d->context)
  {
    d->originConnection = QObject::connect(d->context->origin(), SIGNAL(poseChanged()), this,
                                           SLOT(updateTransform()));
  }
  updateTransform();
  emit(contextChanged());
}

void GeoTransform::updateTransform()
{
  QMatrix4x4 t;
  if(d->usePose)
  {
    knowGIS::Pose pose = d->pose->pose();
    if(d->context and d->context->origin()->isValid() and d->pose->isValid())
    {
      knowGIS::Pose origin_pose
        = d->context->origin()->pose().transform(d->context->coordinateSystem());
      pose = pose.transform(d->context->coordinateSystem());
      t.translate(pose.position().x() - origin_pose.position().x(),
                  pose.position().y() - origin_pose.position().y(),
                  pose.position().z() - origin_pose.position().z());
      t.rotate(QQuaternion(origin_pose.orientation().w(), origin_pose.orientation().x(),
                           origin_pose.orientation().y(), origin_pose.orientation().z())
                 .inverted()
                 .normalized());
    }
    t.rotate(QQuaternion(pose.orientation().w(), pose.orientation().x(), pose.orientation().y(),
                         pose.orientation().z())
               .normalized());
  }
  else
  {
    if(d->context and d->context->origin()->isValid())
    {
      knowGIS::Pose origin_pose
        = d->context->origin()->pose().transform(d->context->coordinateSystem());
      t.translate(-origin_pose.position().x(), -origin_pose.position().y(),
                  -origin_pose.position().z());
      // t.translate(-12.5,-5.0,0);
      t.rotate(QQuaternion(origin_pose.orientation().w(), origin_pose.orientation().x(),
                           origin_pose.orientation().y(), origin_pose.orientation().z())
                 .inverted()
                 .normalized());
    }
  }
  setMatrix(t);
}

#include "moc_GeoTransform.cpp"
