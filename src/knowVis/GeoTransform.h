#ifndef _KNOWVIS_GEOTRANSFORM_H_
#define _KNOWVIS_GEOTRANSFORM_H_

#include <Qt3DCore/QTransform>

#include "Forward.h"

namespace knowVis
{
  /**
   * Used to transform an \ref Qt3DCore::QEntity according to the \ref pose and the origin specified
   * in the \ref context.
   */
  class GeoTransform : public Qt3DCore::QTransform
  {
    Q_OBJECT
    Q_PROPERTY(knowVis::Pose* pose READ pose CONSTANT)
    Q_PROPERTY(knowVis::Context* context READ context WRITE setContext NOTIFY contextChanged)
  public:
    GeoTransform(Qt3DCore::QNode* _parent = nullptr);
    ~GeoTransform();
  public:
    Pose* pose() const;
    Context* context() const;
    void setContext(Context* _context);
    /**
     * Set to false to prevent the transform to use the \ref pose().
     */
    void setUsePose(bool _value);
  signals:
    void contextChanged();
  private slots:
    void updateTransform();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowVis

#endif
