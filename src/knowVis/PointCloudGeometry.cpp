#include "PointCloudGeometry.h"

#include <QHash>
#include <QSharedPointer>

#include <Qt3DCore/QAttribute>
#include <Qt3DCore/QBuffer>

using namespace knowVis;

struct PointCloudGeometry::Private
{
  Qt3DCore::QBuffer* m_vertexBuffer;
  Qt3DCore::QBuffer* m_indexBuffer;
  PointCloud pointcloud;
};

PointCloudGeometry::PointCloudGeometry(Qt3DCore::QNode* parent)
    : Qt3DCore::QGeometry(parent), d(new Private)
{
  d->m_vertexBuffer = new Qt3DCore::QBuffer(this);
  d->m_indexBuffer = new Qt3DCore::QBuffer(this);
}

PointCloudGeometry::~PointCloudGeometry() { delete d; }

PointCloud PointCloudGeometry::pointCloud() const { return d->pointcloud; }

void PointCloudGeometry::setPointCloud(const PointCloud& pointcloud)
{
  d->pointcloud = pointcloud;
  updateVertices();
}

void PointCloudGeometry::updateVertices()
{
  updateAttributes();
  d->m_vertexBuffer->setData(this->d->pointcloud.data);
  QByteArray indexBytes;

  indexBytes.resize(d->pointcloud.points * sizeof(quint16));
  quint16* indexPtr = reinterpret_cast<quint16*>(indexBytes.data());

  for(int i = 0; i < d->pointcloud.points; ++i)
  {
    *indexPtr++ = i;
  }
  d->m_indexBuffer->setData(indexBytes);
}

void PointCloudGeometry::updateAttributes()
{
  // completely rebuild attribute list and remove all previous attributes
  QVector<Qt3DCore::QAttribute*> atts = attributes();
  for(Qt3DCore::QAttribute* attr : atts)
  {
    removeAttribute(attr);
    attr->deleteLater();
  }

  {
    Qt3DCore::QAttribute* attrib = new Qt3DCore::QAttribute(this);
    attrib->setName(Qt3DCore::QAttribute::defaultPositionAttributeName());
    attrib->setVertexBaseType(Qt3DCore::QAttribute::Float);
    attrib->setVertexSize(3);
    attrib->setAttributeType(Qt3DCore::QAttribute::VertexAttribute);
    attrib->setBuffer(d->m_vertexBuffer);
    attrib->setByteStride(d->pointcloud.stride());
    attrib->setByteOffset(0);
    attrib->setCount(d->pointcloud.points);
    addAttribute(attrib);
    setBoundingVolumePositionAttribute(attrib);
  }
  if(d->pointcloud.hasColor)
  {
    Qt3DCore::QAttribute* attrib = new Qt3DCore::QAttribute(this);
    attrib->setName(Qt3DCore::QAttribute::defaultColorAttributeName());
    attrib->setVertexBaseType(Qt3DCore::QAttribute::Float);
    attrib->setVertexSize(4);
    attrib->setAttributeType(Qt3DCore::QAttribute::VertexAttribute);
    attrib->setBuffer(d->m_vertexBuffer);
    attrib->setByteStride(3 * sizeof(float));
    attrib->setByteOffset(0);
    attrib->setCount(d->pointcloud.points);
    addAttribute(attrib);
  }
  {
    // Add index attribute
    Qt3DCore::QAttribute* attrib = new Qt3DCore::QAttribute(this);
    attrib->setAttributeType(Qt3DCore::QAttribute::IndexAttribute);
    attrib->setVertexBaseType(Qt3DCore::QAttribute::UnsignedShort);
    attrib->setBuffer(d->m_indexBuffer);

    attrib->setCount(d->pointcloud.points);
  }
}
