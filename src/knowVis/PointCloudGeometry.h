#pragma once

#include <Qt3DCore/QGeometry>
#include <knowCore/Forward.h>

namespace knowVis
{
  struct PointCloud
  {
    QByteArray data;
    bool hasColor = false;
    int points = 0;
    void resize(int _points)
    {
      points = _points;
      data.resize(_points * stride());
    }
    int stride() const { return sizeof(float) * (hasColor ? 7 : 3); }
    void setValue(int _index, float _x, float _y, float _z, float _r = 0.0, float _g = 0.0,
                  float _b = 0.0, float _a = 1.0)
    {
      float* pt = reinterpret_cast<float*>(data.data() + _index * stride());
      pt[0] = _x;
      pt[1] = _y;
      pt[2] = _z;
      if(hasColor)
      {
        pt[3] = _r;
        pt[4] = _g;
        pt[5] = _b;
        pt[6] = _a;
      }
    }
  };
  class PointCloudGeometry : public Qt3DCore::QGeometry
  {
    Q_OBJECT
  public:
    explicit PointCloudGeometry(QNode* parent = NULL);
    ~PointCloudGeometry();
    void updateVertices();

    void setPointCloud(const PointCloud& _pc);
    PointCloud pointCloud() const;
  private:
    void updateAttributes();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowVis
