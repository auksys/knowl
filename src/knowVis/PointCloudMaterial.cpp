#include "PointCloudMaterial.h"

#include <QColor>

#include <Qt3DRender/QEffect>
#include <Qt3DRender/QGraphicsApiFilter>
#include <Qt3DRender/QParameter>
#include <Qt3DRender/QPointSize>
#include <Qt3DRender/QShaderProgram>
#include <Qt3DRender/QShaderProgramBuilder>
#include <Qt3DRender/QTechnique>

using namespace knowVis;

struct PointCloudMaterial::Private
{
  Qt3DRender::QEffect* effect;
  Qt3DRender::QTechnique* gl3Technique;
  Qt3DRender::QRenderPass* gl3Pass;
  Qt3DRender::QShaderProgram* glShader;
  Qt3DRender::QFilterKey* filterKey;

  Qt3DRender::QParameter* primaryColorParameter;
  Qt3DRender::QParameter* secondaryColorParameter;
  Qt3DRender::QParameter* gradientMinParameter;
  Qt3DRender::QParameter* gradientMaxParameter;
  Qt3DRender::QParameter* modeParameter;

  Qt3DRender::QPointSize* pointSize;
};

PointCloudMaterial::PointCloudMaterial(Qt3DCore::QNode* parent)
    : Qt3DRender::QMaterial(parent), d(new Private)
{
  d->effect = new Qt3DRender::QEffect();
  d->gl3Technique = new Qt3DRender::QTechnique();
  d->gl3Pass = new Qt3DRender::QRenderPass();
  d->glShader = new Qt3DRender::QShaderProgram();

  // Set the targeted GL version for the technique
  d->gl3Technique->graphicsApiFilter()->setApi(Qt3DRender::QGraphicsApiFilter::OpenGL);
  d->gl3Technique->graphicsApiFilter()->setMajorVersion(3);
  d->gl3Technique->graphicsApiFilter()->setMinorVersion(1);
  d->gl3Technique->graphicsApiFilter()->setProfile(Qt3DRender::QGraphicsApiFilter::CoreProfile);

  d->glShader->setVertexShaderCode(R"V0G0N(#version 150 core

in vec3 vertexPosition;
in vec3 vertexColor;

out vec4 color;

uniform mat4 modelMatrix;
uniform mat3 modelNormalMatrix;
uniform mat4 modelViewProjection;

uniform vec4 primaryColor;
uniform vec4 secondaryColor;
uniform float gradientMin;
uniform float gradientMax;
uniform int colorMode;

void main()
{
  // Calculate vertex position in clip coordinates
  gl_Position = modelViewProjection * vec4(vertexPosition, 1.0);

  float t = 1.0;
  float v;
  switch(colorMode)
  {
    case 1: // xgradient
    {
      v = vertexPosition.x;
      break;
    }
    case 2: // ygradient
    {
      v = vertexPosition.y;
      break;
    }
    case 3: // zgradient
    {
      v = vertexPosition.z;
      break;
    }
  }
  switch(colorMode)
  {
    case 1: // xgradient
    case 2: // ygradient
    case 3: // zgradient
    {
      v = min(max(v, gradientMin), gradientMax);
      t = (v - gradientMin) / (gradientMax - gradientMin);
      break;
    }
  }
  switch(colorMode)
  {
    case 0: // Flat color
      color = primaryColor;
      break;
    case 1:
    case 2:
    case 3:
      color = t * primaryColor + (1-t) * secondaryColor;
      break;
    case 4: // Vertex color
      color = vec4(vertexColor, 1.0);
      break;
  }
}
  )V0G0N");

  d->glShader->setFragmentShaderCode(R"V0G0N(#version 130

in vec3 normal;
in vec3 position;
in vec4 color;

out vec4 fragColor;

void main()
{
  fragColor = color;
}  
)V0G0N");

  // Set the shader on the render pass
  d->gl3Pass->setShaderProgram(d->glShader);
  d->pointSize = new Qt3DRender::QPointSize();
  d->pointSize->setSizeMode(Qt3DRender::QPointSize::Fixed);
  d->pointSize->setValue(8);
  d->gl3Pass->addRenderState(d->pointSize);

  // Add the pass to the technique
  d->gl3Technique->addRenderPass(d->gl3Pass);

  d->filterKey = new Qt3DRender::QFilterKey;
  d->filterKey->setParent(this);
  d->filterKey->setName(QStringLiteral("renderingStyle"));
  d->filterKey->setValue(QStringLiteral("forward"));
  d->gl3Technique->addFilterKey(d->filterKey);

  // Add the technique to the effect
  d->effect->addTechnique(d->gl3Technique);

  d->effect->addParameter(
    new Qt3DRender::QParameter(QStringLiteral("ka"), QColor::fromRgbF(0.05f, 0.05f, 0.05f, 1.0f)));
  d->effect->addParameter(
    new Qt3DRender::QParameter(QStringLiteral("kd"), QColor::fromRgbF(0.7f, 0.7f, 0.7f, 1.0f)));
  d->effect->addParameter(
    new Qt3DRender::QParameter(QStringLiteral("ks"), QColor::fromRgbF(0.01f, 0.01f, 0.01f, 1.0f)));
  d->effect->addParameter(new Qt3DRender::QParameter(QStringLiteral("shininess"), 150.0f));

  // Set the effect on the material
  setEffect(d->effect);

  // Set some parameters
  d->primaryColorParameter = new Qt3DRender::QParameter(QStringLiteral("primaryColor"),
                                                        QColor::fromRgbF(1.0f, 1.0f, 1.0f, 1.0f));
  addParameter(d->primaryColorParameter);
  d->secondaryColorParameter = new Qt3DRender::QParameter(QStringLiteral("secondaryColor"),
                                                          QColor::fromRgbF(1.0f, 1.0f, 1.0f, 1.0f));
  addParameter(d->secondaryColorParameter);
  d->gradientMinParameter = new Qt3DRender::QParameter(QStringLiteral("gradientMin"), 0.0);
  addParameter(d->gradientMinParameter);
  d->gradientMaxParameter = new Qt3DRender::QParameter(QStringLiteral("gradientMax"), 1.0);
  addParameter(d->gradientMaxParameter);
  d->modeParameter = new Qt3DRender::QParameter(QStringLiteral("colorMode"), 0);
  addParameter(d->modeParameter);
  //   addParameter(new Qt3DRender::QParameter(QStringLiteral("pointSize"), 0.7));
}

PointCloudMaterial::~PointCloudMaterial() { delete d; }

QColor PointCloudMaterial::color() const
{
  return d->primaryColorParameter->value().value<QColor>();
}

void PointCloudMaterial::setColor(const QColor& _color)
{
  d->primaryColorParameter->setValue(_color);
  emit(colorChanged());
}

QColor PointCloudMaterial::secondaryColor() const
{
  return d->secondaryColorParameter->value().value<QColor>();
}

void PointCloudMaterial::setSecondaryColor(const QColor& _color)
{
  d->secondaryColorParameter->setValue(_color);
  emit(secondaryColorChanged());
}

qreal PointCloudMaterial::gradientMin() const { return d->gradientMinParameter->value().toReal(); }

void PointCloudMaterial::setGradientMin(qreal _min)
{
  d->gradientMinParameter->setValue(_min);
  emit(gradientMinChanged());
}

qreal PointCloudMaterial::gradientMax() const { return d->gradientMaxParameter->value().toReal(); }

void PointCloudMaterial::setGradientMax(qreal _max)
{
  d->gradientMaxParameter->setValue(_max);
  emit(gradientMaxChanged());
}

PointCloudMaterial::ColorMode PointCloudMaterial::colorMode() const
{
  return PointCloudMaterial::ColorMode(d->modeParameter->value().toInt());
}

void PointCloudMaterial::setColorMode(ColorMode _mode)
{
  d->modeParameter->setValue(int(_mode));
  emit(colorModeChanged());
}

qreal PointCloudMaterial::pointSize() const { return d->pointSize->value(); }

void PointCloudMaterial::setPointSize(qreal _pointSize)
{
  d->pointSize->setValue(_pointSize);
  emit(pointSizeChanged());
}

#include "moc_PointCloudMaterial.cpp"
