#pragma once

#include <Qt3DRender/QMaterial>

namespace knowVis
{
  class PointCloudMaterial : public Qt3DRender::QMaterial
  {
    Q_OBJECT
  public:
    enum class ColorMode
    {
      Flat = 0,
      XGradient = 1,
      YGradient = 2,
      ZGradient = 3,
      Data = 4
    };
    Q_ENUM(ColorMode)
  public:
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(QColor secondaryColor READ secondaryColor WRITE setSecondaryColor NOTIFY
                 secondaryColorChanged)
    Q_PROPERTY(qreal gradientMin READ gradientMin WRITE setGradientMin NOTIFY gradientMinChanged)
    Q_PROPERTY(qreal gradientMax READ gradientMax WRITE setGradientMax NOTIFY gradientMaxChanged)
    Q_PROPERTY(ColorMode colorMode READ colorMode WRITE setColorMode NOTIFY colorModeChanged)
    Q_PROPERTY(qreal pointSize READ pointSize WRITE setPointSize NOTIFY pointSizeChanged)
  public:
    PointCloudMaterial(Qt3DCore::QNode* parent = nullptr);
    ~PointCloudMaterial();
    QColor color() const;
    void setColor(const QColor& _color);
    QColor secondaryColor() const;
    void setSecondaryColor(const QColor& _color);
    qreal gradientMin() const;
    void setGradientMin(qreal _min);
    qreal gradientMax() const;
    void setGradientMax(qreal _max);
    ColorMode colorMode() const;
    void setColorMode(ColorMode mode);
    qreal pointSize() const;
    void setPointSize(qreal _pointSize);
  signals:
    void colorChanged();
    void secondaryColorChanged();
    void gradientMinChanged();
    void gradientMaxChanged();
    void colorModeChanged();
    void pointSizeChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowVis

Q_DECLARE_METATYPE(knowVis::PointCloudMaterial*)
