#include "PointCloudRenderer.h"

#include "PointCloudGeometry.h"

using namespace knowVis;

struct PointCloudRenderer::Private
{
  PointCloudGeometry* geometry;
};

PointCloudRenderer::PointCloudRenderer(Qt3DCore::QNode* parent)
    : Qt3DRender::QGeometryRenderer(parent), d(new Private)
{
  d->geometry = new PointCloudGeometry(this);
  setPrimitiveType(PrimitiveType::Points);
  setGeometry(d->geometry);
  connect(this, &PointCloudRenderer::vertexCountChanged, [](int _c) { qDebug() << _c; });
}

PointCloudRenderer::~PointCloudRenderer() { delete d; }

void PointCloudRenderer::setPointCloud(const PointCloud& _pc) { d->geometry->setPointCloud(_pc); }

PointCloud PointCloudRenderer::pointCloud() const { return d->geometry->pointCloud(); }
