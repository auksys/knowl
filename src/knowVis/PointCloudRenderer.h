#pragma once

#include <Qt3DRender/QGeometryRenderer>
#include <knowVis/Forward.h>

namespace knowVis
{

  class PointCloudRenderer : public Qt3DRender::QGeometryRenderer
  {
    Q_OBJECT
  public:
    PointCloudRenderer(Qt3DCore::QNode* parent = nullptr);
    ~PointCloudRenderer();
  public:
    void setPointCloud(const PointCloud& _pc);
    PointCloud pointCloud() const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowVis
