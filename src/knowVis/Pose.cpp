#include "Pose.h"

#include <knowGIS/GeoPose.h>

using namespace knowVis;

struct Pose::Private
{
  knowGIS::Pose pose;
  knowGIS::GeoPose geoPose;
};

Pose::Pose(QObject* _parent) : QObject(_parent), d(new Private)
{
  d->geoPose = knowGIS::GeoPose(knowGIS::GeoPoint(), knowGIS::Quaternion::identity());
  d->pose = d->geoPose;
}

Pose::~Pose() {}

knowGIS::GeoPose Pose::geoPose() const { return d->geoPose; }

void Pose::setGeoPose(const knowGIS::GeoPose& _pose)
{
  d->pose = _pose;
  d->geoPose = _pose;
  emit(poseChanged());
}

knowGIS::GeoPoint Pose::geoPoint() const { return d->geoPose.position(); }

void Pose::setGeoPoint(const knowGIS::GeoPoint& _pose)
{
  setGeoPose({_pose, knowGIS::Quaternion()});
}

knowGIS::Pose Pose::pose() const { return d->pose; }

void Pose::setPose(const knowGIS::Pose& _pose)
{
  d->pose = _pose;
  d->geoPose = knowGIS::GeoPose::from(_pose);
  emit(poseChanged());
}

knowGIS::Point Pose::point() const { return d->pose.position(); }

void Pose::setPoint(const knowGIS::Point& _pose) { setPose({_pose, knowGIS::Quaternion()}); }

qreal Pose::longitude() const { return d->geoPose.position().longitude(); }

void Pose::setLongitude(qreal _l)
{
  setGeoPose({knowGIS::GeoPoint(Cartography::Longitude(_l), d->geoPose.position().latitude(),
                                d->geoPose.position().altitude()),
              d->geoPose.orientation()});
}

qreal Pose::latitude() const { return d->geoPose.position().latitude(); }

void Pose::setLatitude(qreal _l)
{
  setGeoPose({knowGIS::GeoPoint(d->geoPose.position().longitude(), Cartography::Latitude(_l),
                                d->geoPose.position().altitude()),
              d->geoPose.orientation()});
}

qreal Pose::altitude() const { return d->geoPose.position().altitude(); }
void Pose::setAltitude(qreal _l)
{
  setGeoPose({knowGIS::GeoPoint(d->geoPose.position().longitude(), d->geoPose.position().latitude(),
                                Cartography::Altitude(_l)),
              d->geoPose.orientation()});
}

bool Pose::isValid() const { return d->pose.position().isValid(); }

#include "moc_Pose.cpp"
