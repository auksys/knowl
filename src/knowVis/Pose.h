#pragma once

#include <QObject>

#include "Forward.h"

namespace knowVis
{
  class Pose : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(knowGIS::Pose pose READ pose WRITE setPose NOTIFY poseChanged)
    Q_PROPERTY(knowGIS::Point point READ point WRITE setPoint NOTIFY poseChanged)
    Q_PROPERTY(knowGIS::GeoPose geoPose READ geoPose WRITE setGeoPose NOTIFY poseChanged)
    Q_PROPERTY(knowGIS::GeoPoint geoPoint READ geoPoint WRITE setGeoPoint NOTIFY poseChanged)
    Q_PROPERTY(qreal longitude READ longitude WRITE setLongitude NOTIFY poseChanged)
    Q_PROPERTY(qreal latitude READ latitude WRITE setLatitude NOTIFY poseChanged)
    Q_PROPERTY(qreal altitude READ altitude WRITE setAltitude NOTIFY poseChanged)
    Q_PROPERTY(bool isValid READ isValid NOTIFY poseChanged)
  public:
    Pose(QObject* _parent = nullptr);
    ~Pose();
  public:
    knowGIS::GeoPose geoPose() const;
    void setGeoPose(const knowGIS::GeoPose& _pose);
    knowGIS::GeoPoint geoPoint() const;
    void setGeoPoint(const knowGIS::GeoPoint& _pose);
    knowGIS::Pose pose() const;
    void setPose(const knowGIS::Pose& _pose);
    knowGIS::Point point() const;
    void setPoint(const knowGIS::Point& _pose);
    qreal longitude() const;
    void setLongitude(qreal _l);
    qreal latitude() const;
    void setLatitude(qreal _l);
    qreal altitude() const;
    void setAltitude(qreal _l);
    /**
     * @return true if the pose was set and is valid
     */
    bool isValid() const;
  signals:
    void poseChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace knowVis
