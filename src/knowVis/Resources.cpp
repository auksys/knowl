#include "Resources.h"

#include <Qt3DCore/QEntity>
#include <Qt3DCore/QTransform>

#include <Qt3DExtras/QCylinderMesh>
#include <Qt3DExtras/QPhongMaterial>
#include <Qt3DExtras/QSphereMesh>

using namespace knowVis;

#define RF(_T_, _N_, _I_)                                                                          \
  ResourceFactory<_T_> _N_ = ResourceFactory<_T_>(this, [](_T_ * _resource) _I_)
#define RB(_T_, _N_, _I_)                                                                          \
  ResourceBuilder<_T_> _N_ = ResourceBuilder<_T_>([this](_T_ * _resource) _I_)

struct Resources::Private
{

  template<typename _T_>
  struct ResourceFactory
  {
    typedef typename std::function<void(_T_*)> InitF;
    ResourceFactory(Private* _self_p, const InitF& _init) : self_p(_self_p), init(_init) {}
    _T_* operator()()
    {
      if(not resource)
      {
        resource = new _T_();
        init(resource);
      }
      return resource;
    }
  private:
    Private* self_p;
    _T_* resource = nullptr;
    InitF init;
  };
  template<typename _T_>
  struct ResourceBuilder
  {
    typedef typename std::function<void(_T_*)> InitF;
    ResourceBuilder(const InitF& _init) : init(_init) {}
    _T_* operator()(Qt3DCore::QEntity* _parent)
    {
      _T_* resource = new _T_(_parent);
      init(resource);
      return resource;
    }
  private:
    InitF init;
  };

  RF(Qt3DExtras::QSphereMesh, pointMesh, { _resource->setRadius(0.1); });
  RF(Qt3DExtras::QCylinderMesh, axeMesh, {
    _resource->setRadius(0.1);
    _resource->setLength(1.0);
  });
  RF(Qt3DExtras::QPhongMaterial, orangeMaterial, { _resource->setAmbient(qRgb(255, 165, 0)); });
  RF(Qt3DExtras::QPhongMaterial, redMaterial, { _resource->setAmbient(Qt::red); });
  RF(Qt3DExtras::QPhongMaterial, greenMaterial, { _resource->setAmbient(Qt::green); });
  RF(Qt3DExtras::QPhongMaterial, blueMaterial, { _resource->setAmbient(Qt::blue); });
  RB(Qt3DCore::QTransform, xAxis, {
    _resource->setTranslation({axeMesh()->length() * 0.5f, 0.0, 0.0});
    _resource->setRotationZ(90);
  });
  RB(Qt3DCore::QTransform, yAxis,
     { _resource->setTranslation({0.0, axeMesh()->length() * 0.5f, 0.0}); });
  RB(Qt3DCore::QTransform, zAxis, {
    _resource->setTranslation({0.0, 0.0, axeMesh()->length() * 0.5f});
    _resource->setRotationX(90);
  });

  static Private* s_d;
};

Resources::Private* Resources::Private::s_d = new Resources::Private;

#define D Private::s_d

void Resources::addOrangeMaterial(Qt3DCore::QEntity* _entity)
{
  _entity->addComponent(D->orangeMaterial());
}

void Resources::addPoint(Qt3DCore::QEntity* _entity) { _entity->addComponent(D->pointMesh()); }

void Resources::addXAxe(Qt3DCore::QEntity* _entity)
{
  _entity->addComponent(D->axeMesh());
  _entity->addComponent(D->redMaterial());
  _entity->addComponent(D->xAxis(_entity));
}

void Resources::addYAxe(Qt3DCore::QEntity* _entity)
{
  _entity->addComponent(D->axeMesh());
  _entity->addComponent(D->greenMaterial());
  _entity->addComponent(D->yAxis(_entity));
}

void Resources::addZAxe(Qt3DCore::QEntity* _entity)
{
  _entity->addComponent(D->axeMesh());
  _entity->addComponent(D->blueMaterial());
  _entity->addComponent(D->zAxis(_entity));
}
