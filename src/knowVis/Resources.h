#pragma once

#include "Forward.h"

namespace knowVis
{
  class Resources
  {
    Resources();
    ~Resources();
  public:
    static void addOrangeMaterial(Qt3DCore::QEntity* _entity);
    static void addPoint(Qt3DCore::QEntity* _entity);
    static void addXAxe(Qt3DCore::QEntity* _entity);
    static void addYAxe(Qt3DCore::QEntity* _entity);
    static void addZAxe(Qt3DCore::QEntity* _entity);
  private:
    struct Private;
  };
} // namespace knowVis
